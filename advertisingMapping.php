<?php
include("includes/boot.php");
   
error_reporting(E_ERROR);
global $sales;
$sql="SELECT * FROM core_preferences";
$dbPrefs=dbselectsingle($sql);
$prefs=$dbPrefs['data'];

$sales[0]='All sales staff';

$sql="SELECT * FROM advertising_zones ORDER BY name";
$dbTypes=dbselectmulti($sql);
if($dbTypes['numrows']>0)
{
    $minZone=$dbTypes['data'][0]['id'];
    foreach($dbTypes['data'] as $loc)
    {
        $zones[$loc['id']]=stripslashes($loc['name']);
        $legend[]=array("color"=>$loc['color'],"name"=>$loc['name']);      
    }
} else {
   $minZone=0;
}
$zones['all']='All zones';

 
$sql="SELECT DISTINCT(category) FROM advertising_account_mapping ORDER BY category";
$dbCats=dbselectmulti($sql);
$categories['all']='Show all categories'; 
if($dbCats['numrows']>0)
{
    foreach($dbCats['data'] as $cat)
    {
        if(trim($cat['category'])!='')
        {
            $categories[$cat['category']]=$cat['category'];
        } else {
            $categories['blank']='No Category';
        }
    }    
}

$sql="SELECT * FROM naics_codes ORDER BY naics_name";
$dbNCodes=dbselectmulti($sql);
$codes[0]="Select NAICS code";
if($dbNCodes['numrows']>0)
{
    foreach($dbNCodes['data'] as $ncode)
    {
        $codes[$ncode['id']]=$ncode['naics_name'];   
        $niccodes[$ncode['id']]=$ncode['naics_code'];   
    }
}
     
$sql="SELECT * FROM sic_codes ORDER BY sic_name";
$dbCodes=dbselectmulti($sql);
$siccodes[0]="Select SIC code";
if($dbCodes['numrows']>0)
{
    foreach($dbCodes['data'] as $code)
    {
        $siccodes[$code['sic_code']]=$code['sic_name'];
    }
}

if($_POST['category'])
{
    $category=$_POST['category'];
} else {
    $category='all';
} 
if($_POST['revenue'])
{
    $revenue=$_POST['revenue'];
} else {
    $revenue='all';
} 
if($_POST['zone'])
{
    $zone=$_POST['zone'];
} else {
    $zone='all';
}
if($_POST['code'])
{
    $code=$_POST['code'];
} else {
    $code='0';
}
if($_POST['siccode'])
{
    $siccode=$_POST['siccode'];
} else {
    $siccode='0';
}
if($_POST['prospects'])
{
    $showprospects=1;
} 
if($_POST['propects_only'])
{
    $prospectsonly=1;
} 

$revenues=array("all"=>"Any revenue","0"=>"No revenue","1000"=>"Over \$1000","2500"=>"Over \$2500","5000"=>"Over \$5000","10000"=>"Over \$10000");

print "<div style='padding:10px;border: 1px solid black;background-coloe:#efefef;margin-bottom:10px;'>\n";
print "<form method=post class='form-horizontal'>\n";
    print "<div style='float:left;width:500px;'>\n";
        make_select('salesid',$sales[$_POST['salesid']],$sales,'Sales Rep');
        make_select('zone',$zones[$zone],$zones,'Territory');
        make_select('category',$categories[$category],$categories,'Category');
        make_checkbox('print',0,'Large size','Check for a large print-size map');
        
    print "</div>\n";
    
    print "<div style='float:left;width:500px;'>\n";
        make_select('revenue',$revenues[$revenue],$revenues,'YTD Revenue');
        make_checkbox('prospects',$showprospects,'Include Prospects','Include prospects from MSG Data');
        print "<div class='label'>NIC Code</div><div class='input'>";
        print "<small>Requires prospects to be checked</small><br />";
        print "<select id='code' name='code' multiple style='width:300px;'>";
        if(count($codes)>0)
        {
            foreach($codes as $cid=>$cname)
            {
                $selected=false;
                if(array_key_exists($cid,$_POST['code']))
                {
                    $selected=true;
                }
                print "<option value='$cid' ".($selected? 'selected':'').">$cname</option>\n";   
            }
        }
        print "</select>\n";
        print "</div><div class='clear'></div>\n";
        
        print "<div class='label'>SIC Code</div><div class='input'>";
        print "<small>Requires prospects to be checked</small><br />";
        print "<select id='siccode' name='siccode' multiple style='width:300px;'>";
        if(count($siccodes)>0)
        {
            foreach($siccodes as $sid=>$sname)
            {
                $selected=false;
                if(array_key_exists($sid,$_POST['siccode']))
                {
                    $selected=true;
                }
                print "<option value='$sid' ".($selected? 'selected':'').">$sname</option>\n";   
            }
        }
        print "</select>\n";
        print "</div><div class='clear'></div>\n";
        //make_select('code',$codes[$code],$codes,'NAICS Code', 'Requires prospects to be checked','',false,'',false,true);
        //make_select('siccode',$siccodes[$siccode],$siccodes,'SIC Code', 'Requires prospects to be checked','',false,'',false,true);
        make_checkbox('propects_only',$prospectsonly,'Prospects Only','ONLY show prospects');
        print "</div><div class='clear'></div>\n";
        make_submit('submit','Filter Results');
    print "</form>\n";
print "</div>\n";
$sql="SELECT * FROM advertising_map_defaults";
$dbSettings=dbselectsingle($sql);
$settings=$dbSettings['data'];


if($dbPrefs['data']['officeLat']!='' && $dbPrefs['data']['officeLat']!=0) { 
    $defaultLat=$dbPrefs['data']['officeLat'];
    $defaultLon=$dbPrefs['data']['officeLon'];
} else {
    $defaultLat=43.57939;
    $defaultLon=-116.55910;
}

if($_POST)
{
    if($_POST['salesid']!=0)
    {
        $salesid=" AND sales_id=".$_POST['salesid']; 
    }
    if($_POST['revenue']!='')
    {
        if($_POST['revenue']=='all')
        {
            $searchrevenue=" AND ytd_revenue>0";
        } elseif($_POST['revenue']=='0')
        {
            $searchrevenue=" AND ytd_revenue=0";
        } else {
            $searchrevenue=" AND ytd_revenue>".$_POST['revenue'];
        } 
    }
    if($_POST['category']!='all')
    {
        if($_POST['category']=='blank')
        {
            $searchcategory=" AND category=''";
        } else {
            $searchcategory=" AND category='".$_POST['category']."'";
        }
    }
    if($_POST['zone']!='all')
    {
        $searchzone=" AND zone_id='".$_POST['zone']."'";
    }
    $accountsql="SELECT * FROM advertising_account_mapping 
    WHERE lat<>'' AND lon<>'' $salesid $searchcategory $searchrevenue $searchzone";
} else {
    $searchcategory=" AND category<>''";
    $searchrevenue=" AND ytd_revenue>0";
    $accountsql="SELECT * FROM advertising_account_mapping WHERE lat<>'' AND lon<>'' $searchcategory $searchrevenue";
}


?>

<style type='text/css'>
<?php if ($_GET['mode']=='print' || $_POST['print'])
{
  ?>
  #mapCanvas {
    border: thin solid black;
    width: <?php echo $settings['map_width']; ?>px;
    height: <?php echo $settings['map_height']; ?>px;
}

  <?php  
} else {?>

#mapCanvas {
    border: thin solid black;
    width: 100%;
    height: 600px;
}
<?php } ?>
#legend {
    margin-top:20px;
    padding:10px;
    font-family:Trebuchet MS, Arial, sans-serif;
    font-size:10px;
}
</style>

<script type="text/javascript"> 
 
var center;
var map = null;
var currentPopup;
var icon = Array();
var overlay, image, selectedShape, 
      polys         = new Array(),
      poly          = new Array(),
      zonemapid     = 0,
      selectedColor = '',
      selectedShape = '',
      bounds        = new google.maps.LatLngBounds(),
      zoneid        = <?php echo $minZone ?>,
      zoom          = <?php echo  $settings['zoom_level']; ?>,
      drawingManager= null;
  
//need to create an icon for each rep
<?php
    
    $sales[0]="Not assigned";
    $i=1;
    print "icon[0] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=0|00CC99|000000';\n";
    print "icon[999999] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=P|037600|FFFFFF';\n";
    $reps.="0 - No rep assigned<br>";
    $reps.="P - Prospect<br>";
    foreach($sales as $key=>$s)
    {
        print "icon[$key] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=$i|00CC99|000000';\n";
        $reps.="$i - ".$s."<br>";
        $i++;
    }
    
?>

var bounds = new google.maps.LatLngBounds();
function addMarker(id, lat, lng, info, salesid) {
    var pt = new google.maps.LatLng(lat, lng);
    bounds.extend(pt);
    var marker = new google.maps.Marker({
        id: id,
        position: pt,
        icon: icon[salesid],
        map: map
    });
    //console.log('new marker '+id+' added for '+salesid+' at '+lat+' by '+lng);
    var popup = new google.maps.InfoWindow({
        content: info,
        maxWidth: 400
    });
    google.maps.event.addListener(marker, "click", function() {
        if (currentPopup != null) {
            currentPopup.close();
            currentPopup = null;
        }
        popup.open(map, marker);
        currentPopup = popup;
    });
    google.maps.event.addListener(popup, "closeclick", function() {
        currentPopup = null;
    });
};
$(function() {
    var lat=<?php echo $defaultLat ?>;
    var lon=<?php echo $defaultLon ?>;
    center = new google.maps.LatLng(lat,lon);
        
    //Basic
    var MapOptions = {
      zoom: zoom,
      center: center,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    // Init the map
    map = new google.maps.Map(document.getElementById("mapCanvas"),MapOptions);
    getPolygons();
    <?php
    //get the mapping stuff
    $i=1;
    if($_POST['propects_only'])
    {
    //skip this part
    } else {    
        $dbAccounts=dbselectmulti($accountsql);
        if($dbAccounts['numrows']>0)
        {
            foreach($dbAccounts['data'] as $item)
            {
                $ballooninfo=$item['category']."<br>";
                $ballooninfo=$item['account_name']."<br>";
                $ballooninfo.=$item['address']."<br>";
                $ballooninfo.=$item['city'].' '.$item['state'].' '.$item['zip']."<br>";
                $ballooninfo.=$item['phone']."<br>";
                $ballooninfo.="Contact: ".$item['contact']."<br>";
                $ballooninfo.="YTD Revenue: ".$item['ytd_revenue']."<br>";
                $ballooninfo.="Rep: ".$sales[$item['sales_id']];
                
                print "addMarker($i,$item[lat],$item[lon],'".addslashes($ballooninfo)."',$item[sales_id]);\n";
                $i++;
            }
        }
    }
    //now we'll get the prospects. need to filter on zone or SIC/NIC as needed
    //use 'prospect' as the key for the icon
    if($_POST['prospects'])
    {
        if($_POST['code'] && count($_POST['code'])>0)
        {
            foreach($_POST['code'] as $code)
            {
                $naicodes[]=$niccodes[$code];
                print "// $code<br />\n";
            }
            $naicodes=implode(",",$naicodes);
            if($naicodes!='' && $naicodes!=0)
            {
                $searchcode=" AND naics_code IN($naicodes)";
            }
        } else {
            $searchcode='';
        }
        if($_POST['siccode'] && count($_POST['siccode'])>0)
        {
            $siccode=implode(",",$_POST['siccode']);
            if($siccode!='' && $siccode!=0)
            {
                $searchsiccode=" AND sic_code IN($siccode)";
            }
        } else {
            $searchsiccode='';
        }
        if($_POST['revenue']!='')
        {
            if($_POST['revenue']=='all')
            {
                $searchrevenue="";
            } else {
                $searchrevenue=" AND revenue>=".$_POST['revenue'];
            } 
        }
        if($_POST['zone']!='all')
        {
            $searchzone=" AND territory_id=".$_POST['zone'];
        }
        $sql="SELECT * FROM prospects WHERE territory_tested=1 $searchcode $searchsiccode $searchrevenue $searchzone";
        print "//<!-- prospects sql is $sql -->\n";
        $dbProspects=dbselectmulti($sql);
        if($dbProspects['numrows']>0)
        {
            foreach($dbProspects['data'] as $prospect)
            {
                $ballooninfo=$prospect['category']."<br>";
                $ballooninfo=$prospect['account_name']."<br>";
                $ballooninfo.=$prospect['address']."<br>";
                $ballooninfo.=$prospect['city'].' '.$item['state'].' '.$item['zip']."<br>";
                $ballooninfo.="Phone: ".$prospect['phone']."<br>";
                if($prospect['website']!='')
                {
                    $ballooninfo.="Website: ".$prospect['website']."<br>";
                }
                $ballooninfo.="Contact: ".$prospect['contact_name']."<br>";
                $ballooninfo.="Revenue: ".$prospect['revenue'];
                print "addMarker($i,$prospect[lat],$prospect[lng],'".addslashes($ballooninfo)."',999999);\n";
                $i++; 
            }
        }
    }
    
    ?>
     
  }); 

  
  function drawPolygonOld(id, poly, zoneid, color) {
    // Construct the polygon
    // Note that we don't specify an array or arrays, but instead just
    // a simple array of LatLngs in the paths property
    var options = { paths: poly,
      strokeWeight: 0,
      fillColor: selectedColor,
      fillOpacity: 0.45,
      zIndex: 1
    };
      newPoly = new google.maps.Polygon(options);
      newPoly.id = id;
      newPoly.zoneid = zoneid;
      newPoly.color = color;
      newPoly.setMap(map);
      
      polys.push(newPoly);
                                               
  }
  function drawPolygon(poly, color) {
        // Construct the polygon
        // Note that we don't specify an array or arrays, but instead just
        // a simple array of LatLngs in the paths property
        var options = { paths: poly,
          strokeWeight: 0,
          fillColor: color,
          fillOpacity: 0.45,
          zIndex: 1};

          newPoly = new google.maps.Polygon(options);
          newPoly.setMap(map);
          
    }
    
  function getPolygons() {
    $.ajax({
      url: "includes/ajax_handlers/advertisingTerritoryHandler.php",
      type: "POST",
      data: ({action:'getzonemaps'}),
      dataType: "json",
      success: function(response){
          if(response.status=='success')
          {
              if(response.maps)
              {
                  jQuery.each(response.maps, function(i, zonemap) {
                     //now go through the points
                     poly   = new Array();
                     jQuery.each(zonemap.coords, function(i, coord) {
                        poly.push(new google.maps.LatLng(coord.lat, coord.lon));
                     });
                     drawPolygon(poly, zonemap.color );
                 
                 });
              }
          } else {
             //alert(response.message);
          }
          
      }
    })
  }
    
  
  
  
</script>

<div id="mapCanvas"><div style='margin-top:50px;text-align:center;margin-left:auto;margin-right:auto;'>Loading...<br><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div></div>
<div id='legend'>
<h3>Legend</h3>
<?php
    if(count($legend)>0)
    {
        foreach($legend as $key=>$entry)
        {
            print "<div style='margin-bottom:10px;width:30px;height30px;float:left;margin-right:4px;background-color:$entry[color]'>&nbsp;</div>
            <div style='margin-bottom:10px;height30px;float:left;'>$entry[name]</div>
            <div class='clear'></div>\n";
        }
    }
    print $reps;


$Page->footer();  
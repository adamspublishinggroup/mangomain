<?php
//<!--VERSION: .7 **||**-->

include("includes/boot.php") ;
if($GLOBALS['debug']){print "refer is ".$_SERVER['HTTP_REFERER']."<br>";}

if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}

/* get a list of all zones that have been configured, with the system_code as the key */
$sql="SELECT id, system_code FROM publications_insertzones";
$dbZones = dbselectmulti($sql);
$insertZones=array();
if($dbZones['numrows']>0)
{
    foreach($dbZones['data'] as $zone)
    {
        $insertZones[$zone['system_code']]=$zone['id'];
    }
}

    
switch ($action)
{
    case "import":
    import();
    break;
    
    case "Process Import":
    process_import();
    break;
    
}

function import()
{
    $sources = array('pioneer'=>'Pioneer','mcclatchy'=>'McClatchy');
    print "<form method=post enctype='multipart/form-data' class='form-horizontal'>\n";
    make_select('source','pioneer',$sources,'Select source','Specify manifest source type');
    make_file('manifest','Preprint Manifest','Please select the vision data manifest file to upload.');
    make_submit('submit','Process Import');
    print "</form>\n";
}

function process_import()
{
    global $pubs, $advertisers, $Prefs, $lockInsertBook;
    
    print "<p><a href='inserts.php?action=list'>Return to insert list</a></p>";
    print "<p>Inserted records are checked for duplicate entry and the main insert is updated with the current information.</p>";
    $currentDate=strtotime();
    if(isset($_FILES))
    {
        require_once 'includes/parsecsv.lib.php';
        
        $file=$_FILES['manifest']['tmp_name'];
        $contents=file_get_contents($file);
        
        $ads=array();
        $userAccounts=array();
        if($_POST['source']=='pioneer')
        {
            $csv = new parseCSV();
            $csv->heading = true;
            $result = $csv->parse_string($contents);
            
            $ads = processPioneer($result);
            $aSource = 'pioneer';
            
            
            //define $userAccounts array
            $sql="SELECT id, vision_data_sales_id FROM users WHERE site_id='".SITE_ID."'";
            $dbUserAccounts=dbselectmulti($sql);
            if($dbUserAccounts['numrows']>0)
            {
                foreach($dbUserAccounts['data'] as $uac)
                {
                    $userAccounts[$uac['vision_data_sales_id']]=$uac['id'];
                }
            }
        } elseif($_POST['source']=='mcclatchy')
        {
            $csv = new parseCSV();
            $csv->heading = true;
            $result = $csv->parse_string($contents);
            
            $ads = processMcClatchy($result);
            $aSource = 'mcclatchy';
            
            //define $userAccounts array
            $sql="SELECT id, email FROM users WHERE site_id='".SITE_ID."'";
            $dbUserAccounts=dbselectmulti($sql);
            if($dbUserAccounts['numrows']>0)
            {
                foreach($dbUserAccounts['data'] as $uac)
                {
                    $email = stripslashes($uac['email']);
                    $userAccounts[$email]=$uac['id'];
                }
            }
        }
        
        if($GLOBALS['debug']){ print "Ads found<br><pre>";print_r($ads);print "</pre>\n";}
   
        $dt=date("Y-m-d H:i");
        if(count($ads)>0)
        {
            
            //now insert them in the database
            foreach($ads as $key=>$record)
            {
                if(trim($record['account_number'])!='' || trim($record['account_name'])!='')
                {
                    $stickyNote = 0;
                    $singleSheet = 0;
                    $error='';
                    
                    //figure out the customer_id from the account number
                    $sql="SELECT A.id FROM accounts A, accounts_vd B WHERE (B.vd_account_number='$record[account_number]' OR A.account_name='$record[account_name]') AND A.id=B.account_id AND A.account_source='$aSource'";
                    //print "Checked account ID with $sql<br>";
                    $dbCustomer=dbselectsingle($sql);
                    if($dbCustomer['numrows']>0)
                    {
                        $accountid=$dbCustomer['data']['id'];
                        if($GLOBALS['debug']){ print "Found account id of $accountid for $record[account_number]<br>";}
                        
                    } else {
                        //that vision data advertiser does not exist in the account table. Lets add it
                        $sql="INSERT INTO accounts (site_id, account_name, account_advertiser, adsystem_account_name, account_source, created_by, created_datetime) VALUES 
                        ('".SITE_ID."', '".$record['account_name']."', '1', '".$record['account_name']."', '$aSource','".$_SESSION['userid']."','".date("Y-m-d H:i:s")."')";
                        $dbInsert=dbinsertquery($sql);
                        //print $sql."<br><br>";
                        $accountid=$dbInsert['insertid'];
                        //now add the account_vd record
                        $sql="INSERT INTO accounts_vd (account_id, vd_account_number) VALUES ('$accountid','$record[account_number]')";
                        $dbInsertVD=dbinsertquery($sql);
                        if($GLOBALS['debug']){ print "Created a new account record with id $accountid for $record[account_number]<br>&nbsp;&nbsp;&nbsp;&nbsp;The error, if any was: $dbInsert[error]<br />&nbsp;&nbsp;&nbsp;&nbsp;and $dbInsertVD[error]<br />";}
                        $newAccounts[]=array('account_name'=>$record['account_name'],'account_id'=>$accountid);
                    }
                    
                    $record['account_id']=$accountid;
                    if(array_key_exists($record['sales'],$userAccounts))
                    {
                        $salesid=$userAccounts[$record['sales']];
                    } else {
                        $salesid=0;
                    }
                    
                    //toggle from ad type
                    switch($record['adType'])
                    {
                       case "Tab":
                           $record['product']=1;
                           $record['standard_pages']=$record['pages']*1;
                           $singleSheet= 0;
                           $stickyNote = 0;
                       break;  
                       
                       case "P&D - Full Sheet":
                           $record['product']=2;
                           $record['standard_pages']=$record['pages']*1;
                           $singleSheet= 1;
                           $stickyNote = 0;
                       break; 
                       
                       case "Ad Notes":
                           $record['product']=2;
                           $record['standard_pages']=$record['pages']*1;
                           $singleSheet= 0;
                           $stickyNote = 1;
                       break;
                       
                       default:
                           $record['product']=0;
                           $record['standard_pages']=$record['pages']*2;
                           $singleSheet= 0;
                           $stickyNote = 0;
                       break;
                    }
                    
                    /* switching to FIND_IN_SET operation */
                    $pubsql="SELECT * FROM publications WHERE FIND_IN_SET('$record[publication]',booking_pub)";
                    if($record['edition']!=''){$pubsql.=" AND (FIND_IN_SET('$record[edition]',booking_edition) OR FIND_IN_SET('$record[edition]',booking_sticky))";}
                    
                    $dbPub=dbselectsingle($pubsql);
                    if($dbPub['numrows']>0)
                    {
                        $pubid=$dbPub['data']['id'];
                        $pressrunid=0;
                    } else {
                        $pubsql = "SELECT A.id, B.id as run_id FROM publications A, publications_runs B 
                        WHERE A.id=B.pub_id AND FIND_IN_SET('$record[publication]',B.booking_pub)";
                        if($record['edition']!=''){$pubsql.=" AND (FIND_IN_SET('$record[edition]',B.booking_edition) OR FIND_IN_SET('$record[edition]',A.booking_sticky))";}
                        $dbPub=dbselectsingle($pubsql);
                        if($dbPub['numrows']>0)
                        {
                            $pubid=$dbPub['data']['id'];
                            $pressrunid=$dbPub['data']['run_id'];
                        } else {
                            $pubid=0;
                            $pressrunid=0;
                        }
                    }
                    if(str_replace($record['edition'],"",$dbPub['data']['booking_sticky']) != $dbPub['data']['booking_sticky'] && $dbPub['data']['booking_sticky']!='')
                    {
                        //means that the edition code WAS found in the adsys_edition_sticky and that field wasn't blank, so this must be a sticky note so...
                        $stickyNote=1;
                        $record['pages']=1;
                    }
                    $record['pub_id']=$pubid;
                    $insertdate=date("Y-m-d",strtotime($record['run_date']));
                    $insertday=date("w",strtotime($record['run_date']));
                    
                    
                    //if the pubid is zero, let's skip creation because obviously something is wrong.
                    if($pubid!=0)
                    {
                        $runid=0;
                        
                        //lets see if the insertdate (publish) is in violation of our timelock
                        $minDate=strtotime($record['run_date']." -$lockInsertBook hours");
                        if($minDate<strtotime())
                        {
                            //we'll want to set a flag to send a message, an append this insert to that message
                            $late=true;
                            $latemessages[]="An insert for $ad[account_name] scheduled to publish on $insertdate has just been imported. Order # was $ad[ad_number]";
                        }
                        
                        $zoneInfo = $ads[$key]['zones'];
                        if(is_array($zoneInfo))
                        {
                            $zoneInfo = implode(", ",$zoneInfo);
                        }  
                        
                        //see if there is an ad with this number already existing. if there is, just update the details
                        $sql="SELECT id FROM inserts WHERE ad_number='$record[ad_number]'";
                        $dbCheck=dbselectsingle($sql);
                        
                        if($dbCheck['numrows']==0)
                        {
                            //create the insert record
                            $insertsql="INSERT INTO inserts (control_number, single_sheet, sticky_note, advertiser_id, insert_tagline, buy_count, sales_id, product_size, product_pages, received, insertion_order, insert_description, standard_pages, scheduled, created_by, created_datetime, site_id, ad_number) VALUES ('$record[control_number]', '$singleSheet', '$stickyNote', $accountid, 
                            '".addslashes($record['description']." ".$record['ad_number'])."',  '$record[quantity]','$salesid', '$record[product]', '$record[pages]', 0, '$record[po_number]', '".addslashes($record['description']." ".$record['misc'])."', '$record[standard_pages]', 0, 0, '$dt', '".SITE_ID."',  '$record[ad_number]')";
                            $dbInsert=dbinsertquery($insertsql);
                            $insertid=$dbInsert['insertid'];
                            if($GLOBALS['debug']){ print "Inserting a new insert  with $insertsql<br>";}
                            $error.=$dbInsert['error'];
                            if($dbInsert['error']=='' && $insertid!=0)
                            {
                                //now create the schedule record
                                //need to look up the pub_id from the publications based on the vision data pub
                                $schedsql="INSERT INTO inserts_schedule (insert_id, pub_id, run_id, pressrun_id, package_run_id, insert_quantity, 
                                insert_date, zoning) VALUES ('$insertid', '$pubid', '$runid', 0, '$pressrunid', '$record[quantity]', 
                                '$insertdate', '$zoneInfo')";
                                $dbInsert=dbinsertquery($schedsql);
                                $schedid=$dbInsert['insertid'];
                                $error.=$dbInsert['error'];
                                
                                $inserted++;
                            }
                            
                        } else {
                            $insertid=$dbCheck['data']['id'];
                            if($record['control_number']!='')
                            {
                                $cnum = "control_number='$record[control_number]', ";
                            }  else {
                                $cnum = '';
                            }
                            $insertsql="UPDATE inserts SET $cnum advertiser_id='$accountid', 
                            insert_tagline='".addslashes($record['description']." ".$record['ad_number'])."',  buy_count='$record[quantity]', sales_id='$salesid', product_pages='$record[pages]', product_size='$record[product]', insertion_order='$record[po_number]', insert_description='".addslashes($record['description']." ".$record['misc'])."', standard_pages='$record[standard_pages]', ad_number='$record[ad_number]', single_sheet='$singleSheet', sticky_note='$stickyNote' WHERE id=$insertid";
                            $dbUpdate=dbexecutequery($insertsql);
                            if($dbUpdate['error']=='')
                            {
                                $updated++;     
                            } else {
                                print "Failed update sql is $insertsql<br>";
                            }
                            $insertdate=date("Y-m-d",strtotime($record['run_date']));
                            if($GLOBALS['debug']){ print "Updating an existing insert  with $insertsql<br>";}
                            
                             
                            //see if there is an existing schedule for this pub and insert id and update accordingly
                            $sql="SELECT * FROM inserts_schedule WHERE insert_id=$insertid AND pub_id=$pubid AND pressrun_id=$pressrunid AND insert_date='$insertdate'";
                            $dbCheck=dbselectsingle($sql);
                            if($dbCheck['numrows']>0)
                            {
                                $schedid=$dbCheck['data']['id'];
                                $schedsql="UPDATE inserts_schedule SET insert_quantity='$record[quantity]', zoning='$zoneInfo' WHERE id=$schedid";
                                $dbUpdate=dbexecutequery($schedsql);
                                $error.=$dbUpdate['error'];
                                if($GLOBALS['debug']){ print "Updating an exisiting schedule for this insert with $schedsql<br>";}
                            } else {
                                $schedsql="INSERT INTO inserts_schedule (insert_id, pub_id, pressrun_id, run_id, insert_quantity, insert_date, zoning) 
                                VALUES ('$insertid', '$pubid', '$pressrunid', '$runid', '$record[quantity]', '$insertdate', '$zoneInfo')";
                                $dbInsert=dbinsertquery($schedsql);
                                $schedid=$dbInsert['insertid'];
                                $error.=$dbInsert['error'];
                                if($GLOBALS['debug']){ print "Inserting a new schedule for this insert with $schedsql<br>";}
                            }
                            
                        }
                        
                        
                        if($GLOBALS['debug'] && $error!=''){ print $error;}
                        
                        //create the zoning records
                        handleZones($insertid,$schedid,$record);
                            
                        //see if there is a matching insert_received record based on the control number
                        if($controlnumber!='')
                        {
                            tie_received($controlnumber,$insertid);
                        }
                        $found[]=array('record'=>$record,'pub_id'=>$pubid,'pressrun_id'=>$pressrunid,'pub_sql'=>$pubsql);
                    } else {
                        $unfound[]=$record;
                    }
                    //print "Ran $insertsql for the insert and $schedsql for the schedule and $pubsql for the pub and $runsql for the run<br>";
                } else {
                    if($GLOBALS['debug']){ print "Account number and account name were both blank for record $key<br>";}
                }
            }
            print "Inserted $inserted records and updated $updated.<br>";
            
            ?>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-primary">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#successful" aria-expanded="true" aria-controls="collapseOne">
                      Successful imports
                    </a>
                  </h4>
                </div>
                <div id="successful" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <?php
                       if(count($found)>0)
                        {
                            print "<h3>The following inserts were successfully imported:</h3>";
                            print "<table class='table table-striped table-bordered'>\n";
                            print "<tr><th>Account Name</th><th>Advertiser</th><th>Ad Number</th><th>Publication</th><th>Pub Date</th></tr>\n";
                            foreach($found as $fAd)
                            {
                                $f = $fAd['record'];
                                $pubid=$fAd['pub_id'];
                                $pressrunid=$fAd['pressrun_id'];
                                $pubsql=$fAd['pub_sql'];
                                $zones=implode(",",$ufa['zones']);
                                print "<tr><td>$f[account_name]<br>PubID: $pubid | PressrunID: $pressrunid</td><td>".$advertisers[$f['account_id']]."</td>
                               <td>$f[ad_number]</td><td>Pub: ".$f['publication'].' -  Secondary:'.$f['edition']."</td><td>$f[run_date]</td><td>$zones</td></tr>\n";  
                                 //print "<tr><td colspan=5>$pubsql</td></tr>\n";
                            }
                            print "</table>\n";
                        
                        }  
                    ?>
                  </div>
                </div>
              </div>
              
               <div class="panel panel-success">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#newaccounts" aria-expanded="true" aria-controls="collapseOne">
                        New advertising accounts created for the following  
                    </a>
                  </h4>
                </div>
                <div id="newaccounts" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <?php
                      //lets display everything that was imported with an issue
                        if(count($newAccounts)>0)
                        {
                            print "<h3>Inserts where new accounts created</h3>\n";
                            print "<table class='table table-striped table-bordered'>\n";
                            print "<tr><th>Account Name</th><th>New Account ID</th></tr>\n";
                            foreach($newAccounts as $ufa)
                            {
                               print "<tr><td>$ufa[account_name]</td><td>$ufa[account_id]</td></tr>\n";  
                            }
                            print "</table>\n";
                        }
                    ?>
                  </div>
                </div>
              </div>
              
              <div class="panel panel-danger">
                <div class="panel-heading" role="tab" id="headingOne">
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#unfound" aria-expanded="true" aria-controls="collapseOne">
                      Unable to find a matching publication
                    </a>
                  </h4>
                </div>
                <div id="unfound" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                  <div class="panel-body">
                    <?php
                       if(count($unfound)>0)
                        {
                            print "<h3>Unable to find publicationss for these:</h3>";
                            print "<table class='table table-striped table-bordered'>\n";
                            print "<tr><th>Account Name</th><th>Advertiser</th><th>Ad Number</th><th>Publication</th><th>Pub Date</th></tr>\n";
                            foreach($unfound as $ufa)
                            {
                               $zones=implode(",",$ufa['zones']);
                                print "<tr><td>$ufa[account_name]</td><td>".$advertisers[$ufa['account_id']]."</td>
                               <td>$ufa[account_number]</td><td>Pub: ".$ufa['publication'].' -  Secondary:'.$ufa['edition']."</td><td>$ufa[run_date]</td><td>$zones</td></tr>\n";  
                            }
                            print "</table>\n";
                        
                        } 
                    ?>
                  </div>
                </div>
              </div>
           <?php
            
        } else {
            print "Sorry, there were no ad records found in this file<br>";
        }
           
    }
    
    
    print "<p><a href='inserts.php?action=list'>Return to insert list</a></p>";
    
    if($late)
    {
        //send a message && also echo it out so the person who just ran the import knows.
        
        include ('/includes/mail/htmlMimeMail.php');
        $subject=htmlentities("Insert import process found several late inserts");
        $mail = new htmlMimeMail();
        $message="The following inserts where imported during the batch import process and were marked as being late by the system based on the insert lock preferences. They have been imported, this is just a courtesy notification.<br />";
        $message.=implode("<br />",$latemessages);
        $mail->setHtml($message);                  
        $mail->setFrom($GLOBALS['systemEmailFromAddress']);
        $mail->setSubject($subject);
        $mail->setHeader('Sender','Mango');
        
        if ($address!='')
        {
            $result = $mail->send(array($GLOBALS['lateInsertNotification']),'smtp');
        }
        
        //echo out these as well
        print "<h4>The following inserts are past deadline, but have been imported.</h4>";
        print "<ul>";
        foreach($latemessages as $key=>$late)
        {
            print "<li>$late</li>\n";
        }
        print "</ul>\n";
    }
    
    
    if($error!='')
    {
        print $error;
    } 
}

function processMcClatchy($csvFileData)
{
    $ads=array();
    $lastAd = '';
    $i=0;        
    foreach($csvFileData as $lineitems)
    {
        /*
        print "<pre>";
        print_r($line);
        print "</pre>";
        */        
        if($lineitems['IsPublishable']!='No')
        {
            $adnumber =trim($lineitems['AdNumber']);
            
            /*
            * DO A REPLACEMENT OF OLD IDAHO STATESMAN ZONES WITH NEW
            */
            $zone = $lineitems['DistUnit'];
            $zone = str_replace("BOI-WS-EVLY","BOI-IS-EVLY",$zone);
            $zone = str_replace("BOI-WS-WVLY","BOI-IS-WVLY",$zone);
            $zone = str_replace("BOI-TO-ADA","BOI-IS-EVLY",$zone);
            $zone = str_replace("BOI-TO-CC","BOI-IS-CC-STATE",$zone);
            $zone = str_replace("BOI-TO-STATE","BOI-IS-CC-STATE",$zone);
            $zone = str_replace("BOI-SS-EVLY","BOI-IS-EVLY",$zone);
            $zone = str_replace("BOI-SS-WVLY","BOI-IS-WVLY",$zone);
            
            if($lastAd!=$adnumber)
            {
                $lastAd = $adnumber;
                $ads[$i]['account_number']=addslashes($lineitems['AccountNumber']);    
                $ads[$i]['account_name']=addslashes($lineitems['Customer']);    
                $ads[$i]['agency_name']='';    
                $ads[$i]['telephone']='';    
                $ads[$i]['ad_number']=addslashes($lineitems['AdNumber']);    
                $ads[$i]['run_date']=addslashes($lineitems['StartDate']);    
                $ads[$i]['publication']=addslashes($lineitems['Product']);    
                $ads[$i]['sales']=addslashes($lineitems['SalesRep']);    
                $ads[$i]['adType']=addslashes($lineitems['AdType']);    
                $ads[$i]['zone']='';    
                $ads[$i]['edition']='';    
                $ads[$i]['section']='';    
                $ads[$i]['pages']=0;    
                $desc=str_replace(array("\"","\n","\r", "\r\n"),"",trim($lineitems['OrderNotes'].' '.$lineitems['ScheduleNotes']));
                $ads[$i]['description']=addslashes($desc);    
                //look for a C# or c# in the description, break it into words based on ' ' and look at each start
                $temp=explode(" ",$desc);
                $controlnumber='';
                if(count($temp)>0)
                {
                    $wordloop=0;
                    foreach($temp as $key=>$word)
                    {
                        if(substr($word,0,2)=='C#' || substr($word,0,2)=='c#')
                        {
                            //print "<b>FOUND!</b>";
                            $controlnumber=str_replace('c#','',$word);
                            $controlnumber=str_replace('C#','',$controlnumber);
                            if(strlen($controlnumber)<3 && $wordloop<count($temp))
                            {
                                //looks like this is too short, possible space between parts of control number,
                                //lets append the next word to it
                                $controlnumber.=$temp[$wordloop+1];    
                            }
                            if(strlen($controlnumber)<5 && $wordloop<(count($temp)-1))
                            {
                                //looks like this is still too short, possible space between parts of control number,
                                //lets append the next word to it
                                $controlnumber.=$temp[$wordloop+2];    
                            }
                            //now uppercase and get rid of hyphens
                            $controlnumber=strtoupper(str_replace("-","",$controlnumber));
                            //print "--><b>$controlnumber</b>";
                        }
                        $wordloop++;
                    }
                }
                $ads[$i]['control_number']=trim($controlnumber);
                
                $ads[$i]['po_number']='';    
                $ads[$i]['pages']=addslashes($lineitems['PageCount']);    
                $ads[$i]['quantity']=addslashes(str_replace(",","",$lineitems['QuantityOrdered']));    
                $ads[$i]['misc']='';
                if ($ads[$i]['pages']==''){$ads[$i]['pages']=0;}
                
                
                $ads[$i]['zones'][]=addslashes($zone); 
                $i++;
            } else {
                $ads[$i-1]['zones'][]=addslashes($zone); 
            }    
            
            
        }
    }
    /*
    print "<pre>";
    print_r($ads);
    print "</pre>";
    */
    return $ads;
}

function processPioneer($csvFileData)
{
    $ads=array();
    foreach($csvFileData as $lineitems)
    {
        if($GLOBALS['debug']){
        print "<pre>";
        print_r($lineitems);
        print "</pre>";
        }
        if($lineitems['Account #']!='Account #')
        {
            $ads[$i]['account_number']=addslashes(str_replace("\"","",trim($lineitems['Account #'])));    
            $ads[$i]['account_name']=addslashes(str_replace("\"","",trim($lineitems['Advertiser Name'])));    
            $ads[$i]['agency_name']=addslashes(str_replace("\"","",trim($lineitems['Agency Name'])));    
            $ads[$i]['telephone']=addslashes(str_replace("\"","",trim($lineitems['Telephone'])));    
            $ads[$i]['ad_number']=str_replace("\"","",trim($lineitems['Ad-Number']));    
            $ads[$i]['run_date']=str_replace("\"","",trim($lineitems['Run-Date']));    
            $ads[$i]['publication']=str_replace("\"","",trim($lineitems['Publication']));    
            $ads[$i]['zone']=str_replace("\"","",trim($lineitems['Zone']));    
            $ads[$i]['edition']=str_replace("\"","",trim($lineitems['Edition']));    
            $ads[$i]['section']=str_replace("\"","",trim($lineitems['Section']));    
            $ads[$i]['page']=str_replace("\"","",trim($lineitems['Page']));    
            $ads[$i]['sales']=str_replace("\"","",trim($lineitems['Salsp']));    
            $desc=str_replace("\"","",trim($lineitems['Description'])." ".trim($lineitems['Description-2']));
            $ads[$i]['description']=addslashes($desc);    
            $ads[$i]['adType']="Tab";    
            //look for a C# or c# in the description, break it into words based on ' ' and look at each start
            $temp=explode(" ",$desc);
            $controlnumber='';
            if(count($temp)>0)
            {
                $wordloop=0;
                foreach($temp as $key=>$word)
                {
                    if(substr($word,0,2)=='C#' || substr($word,0,2)=='c#')
                    {
                        //print "<b>FOUND!</b>";
                        $controlnumber=str_replace('c#','',$word);
                        $controlnumber=str_replace('C#','',$controlnumber);
                        if(strlen($controlnumber)<3 && $wordloop<count($temp))
                        {
                            //looks like this is too short, possible space between parts of control number,
                            //lets append the next word to it
                            $controlnumber.=$temp[$wordloop+1];    
                        }
                        if(strlen($controlnumber)<5 && $wordloop<(count($temp)-1))
                        {
                            //looks like this is still too short, possible space between parts of control number,
                            //lets append the next word to it
                            $controlnumber.=$temp[$wordloop+2];    
                        }
                        //now uppercase and get rid of hyphens
                        $controlnumber=strtoupper(str_replace("-","",$controlnumber));
                        //print "--><b>$controlnumber</b>";
                    }
                    $wordloop++;
                }
            }
            $ads[$i]['control_number']=trim($controlnumber);
            
            $ads[$i]['po_number']=addslashes(str_replace("\"","",trim($lineitems['Po-Number'])));    
            $ads[$i]['pages']=addslashes(str_replace("\"","",trim($lineitems['PrPg'])));    
            $ads[$i]['quantity']=addslashes(str_replace("\"","",trim($lineitems['Preprints'])));    
            $ads[$i]['misc']=addslashes(str_replace("\"","",trim($lineitems['Misc Instr'])));
            if ($ads[$i]['pages']==''){$ads[$i]['pages']=0;}
            $ads[$i]['zones']=$zones;
            
            $i++;
            
        }
    }
    return $ads;
}

function handleZones($insertID,$scheduleID,$record)
{
    /*
    * @TODO need to handle the zone record creation now.
    */
    global $insertZones;
    
    //clear any exinst inserts_zoning records
    $sql="DELETE FROM inserts_zoning WHERE schedule_id = $scheduleID AND insert_id = $insertID";
    $dbClear = dbexecutequery($sql);
    
    
    //pull zones list from record
    $zones = $record['zones'];
    if(count($zones)>0)
    {
        $values = array();
        foreach($zones as $zone)
        {
            //check to see if zone exists in insertZones
            if(array_key_exists($zone,$insertZones))
            {
                $zoneID = $insertZones[$zone];
                $values[]="($scheduleID, $insertID, $zoneID)";
            }
        }
        if(count($values)>0)
        {
            $values = implode(",",$values);
            $sql="INSERT INTO inserts_zoning (schedule_id, insert_id, zone_id) VALUES $values";
            $dbInsert=dbinsertquery($sql);
        }
    }
    return;
}


$Page->footer();

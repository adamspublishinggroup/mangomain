<?php
//<!--VERSION: .9 **||**-->
include("includes/boot.php");
if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}

switch ($action)
{
    case "Import Prospects":
        switch($_POST['import_type'])
        {
            case "msg":
                prospect_import_msg();
            break;
            case "vdata":
                prospect_import_vdata();
            break;
        }
        
    break;
    
    case "import":
        prospect_import_select();
    break;
    
    case "Save Prospect":
    save_prospect('insert');
    break;
    
    case "Update Prospect":
    save_prospect('update');
    break;
    
    case "add":
        setup_prospect('add');
    break;
    
    case "edit":
        setup_prospect('edit');
    break;
    
    case "delete":
        setup_prospect('delete');
    break;
    
    case "list":
        setup_prospect('list');
    break;
    
    case "checkterritory":
        map_to_zone();
    break;
        
    case "getlist":
        get_prospect_list();
    break;
        
    case "resetterritory":
        clear_zones();
    break;
        
    case "Show List":
        show_prospect_list();
    break;
    
        
    default:
    setup_prospect('list');
    break;
    
} 
    

function get_prospect_list()
{
    
    $sql="SELECT * FROM naics_codes ORDER BY naics_name";
    $dbNCodes=dbselectmulti($sql);
    $codes[0]="Select NAICS code";
    if($dbNCodes['numrows']>0)
    {
        foreach($dbNCodes['data'] as $ncode)
        {
            $codes[$ncode['id']]=$ncode['naics_name'];   
            $niccodes[$ncode['id']]=$ncode['naics_code'];   
        }
    }
         
    $sql="SELECT * FROM sic_codes ORDER BY sic_name";
    $dbCodes=dbselectmulti($sql);
    $siccodes[0]="Select SIC code";
    if($dbCodes['numrows']>0)
    {
        foreach($dbCodes['data'] as $code)
        {
            $siccodes[$code['sic_code']]=$code['sic_name'];
        }
    }
    
    $sql="SELECT * FROM advertising_zones ORDER BY name";
    $dbTypes=dbselectmulti($sql);
    if($dbTypes['numrows']>0)
    {
        $minZone=$dbTypes['data'][0]['id'];
        foreach($dbTypes['data'] as $loc)
        {
            $zones[$loc['id']]=stripslashes($loc['name']);
            $legend[]=array("color"=>$loc['color'],"name"=>$loc['name']);      
        }
    } else {
       $minZone=0;
    }
    $zones['all']='All zones';

    if($_POST['code'])
    {
        $code=$_POST['code'];
    } else {
        $code='0';
    }
    if($_POST['siccode'])
    {
        $siccode=$_POST['siccode'];
    } else {
        $siccode='0';
    }
    if($_POST['territory'])
    {
        $zone=$_POST['territory'];
    } else {
        $zone='0';
    }
    print "<form method=post class='form-horizontal'>";
    make_select('code',$codes[$code],$codes,'NAICS Code', 'Requires prospects to be checked','',false,'',false,true);
    make_select('siccode',$siccodes[$siccode],$siccodes,'SIC Code', 'Requires prospects to be checked','',false,'',false,true);
    make_select('territory',$zones[$zone],$zones,'Territory');
            
    
    make_submit('submit','Show List');
    
    print "</form>\n";
}   

function show_prospect_list()
{
    print "Winner is ".rand(1,8);
    print "<pre>\n";
    print_r($_POST);
    print "</pre>\n";
} 
  
function clear_zones()
{
     $sql="UPDATE prospects SET territory_tested=0, territory_id=0";
     $dbUpdate=dbexecutequery($sql);
     redirect("?action=list");
}  
    
function map_to_zone()
{
        
        //get all locations
        /*
        $sql="SELECT * FROM circ_racks";
        $dbRacks=dbselectmulti($sql);
        if($dbRacks['numrows']>0)
        {
            //geocode the starting address
            $mindistance=9999;
            $wid=0;
            foreach($dbRacks['data'] as $rack)
            {
                $R = 6371; // km
                $lat1=$map['lat'];
                $lat2=$rack['lat'];
                $lon1=$map['lon'];
                $lon2=$rack['lon'];
                $dist = acos(sin($lat1)*sin($lat2)+cos($lat1)*cos($lat2) * cos($lon2-$lon1)) * $R;
                $dist=$dist/1.609344; //convert miles to kilometers
                if($dist<$mindistance){$mindistance=$dist;$wid=$rack['id'];$crack=$rack;}
            }
            $json['startlat']=$map['lat'];
            $json['startlon']=$map['lon'];
            $json['endlat']=$rack['lat'];
            $json['endlon']=$rack['lon'];
            $json['distance']=$mindistance;   
            if($wid!=0)
            {
               $json['status']='success';
               $json['message']="The nearest location to pick up the paper will be at $crack[location_name], located at $crack[street] in $crack[city].";  
            } else {
               $json['status']='error';
               $json['message']='There are no nearby racks or stores. Please consider a digital only membership.'; 
            }
        } else {
            $json['status']='error';
            $json['message']='There are no racks or stores in your area. Please consider a digital only membership.';
        }
        */
        $pointLocation = new pointLocation();
        
        //get all prospects that have no territory_id
        $sql="SELECT id, account_name, lat, lng FROM prospects 
        WHERE lat<>'' AND territory_tested=0 ";
        $dbProspects=dbselectmulti($sql);
        print $dbProspects['numrows'].' total prospects available that have not been tested.<br />';
        
        //get all prospects that have no territory_id
        $sql="SELECT id, account_name, lat, lng FROM prospects 
        WHERE lat<>'' AND territory_id=0 AND territory_tested=0 LIMIT 500";
        $dbProspects=dbselectmulti($sql);
        print "Testing ".$dbProspects['numrows']." prospects in this batch.<br />";
        
        if($dbProspects['numrows']>0)
        {
            $inzones=array();
            $sql="SELECT id, zone_id FROM advertising_zone_map";
            $dbZoneMaps=dbselectmulti($sql);
            if($dbZoneMaps['numrows']>0)
            {
                $zonemaps=array();
                foreach($dbZoneMaps['data'] as $zonemap)
                {
                    $sql="SELECT * FROM advertising_zone_map_points WHERE zonemap_id=$zonemap[id]";
                    $dbPoints=dbselectmulti($sql);
                    if($dbPoints['numrows']>0)
                    {
                        $zonemap['points']=$dbPoints['data'];
                        $zonemaps[]=$zonemap;        
                    } else {
                        print "<br />No map points for territory with id of $zonemap[id]<br />";
                    }
                    
                }
                
                foreach($dbProspects['data'] as $prospect)
                {
                    $locPoint = array('y'=>$prospect['lat'],'x'=>$prospect['lng']);
                    $ids[]=$prospect['id'];
                
                    foreach($zonemaps as $zonemap)
                    {
                        $pointLocation = new pointLocation();
                        $polygon=array();
                        if(count($zonemap['points']>0))
                        {
                            foreach($zonemap['points'] as $point)
                            {
                                $polygon[]=array('y'=>$point['lat'],'x'=>$point['lon']);    
                            }
                        }
                        $test=$pointLocation->pointInPolygon($locPoint, $polygon);
                        if ($test)
                        {
                            $inzones[]=array('territory'=>$zonemap['zone_id'],
                                             'id'=>$prospect['id']);
                        }
                    } 
                }
            } else {
                print "Looks like no advertising territories have been set up yet.<br />";
            }
        }
        print "Findings:<br />";
        if(count($inzones)>0)
        {
            foreach($inzones as $inzone)
            {
                $sql="UPDATE prospects SET territory_id=$inzone[territory] WHERE id=$inzone[id]";
                $dbUpdate=dbexecutequery($sql);
                    
            }
            print "A total of ".count($inzones)." locations were in existing territories.<br />";
        } else {
            print "No locations were in any defined territories.<br />";
        }
        if(count($ids)>0)
        {
        $ids=implode(",",$ids);
        $sql="UPDATE prospects SET territory_tested=1 WHERE id IN ($ids)";
        $dbUpdate=dbexecutequery($sql);
        }
        print "<a href='?action=checkterritory'>Process another batch</a><br />";
        print "<a href='?action=list'>Return to list</a>";
}
    
function setup_prospect($action)
{
    $sources=array('manual'=>"Manually entered",
                   'import_msg'=>"Imported from MSG",
                   'import_vdata'=>"Imported from Vision Data"
                   );
    //get naics codes
    $sql="SELECT * FROM naics_codes ORDER BY naics_code";
    $dbCodes=dbselectmulti($sql);
    $codes[0]="Please select a code";
    if($dbCodes['numrows']>0)
    {
        foreach($dbCodes['data'] as $code)
        {
            $codes[$code['naics_code']]=$code['naics_name'];
        }
    }
    $id=intval($_GET['id']);
    if ($action=='add' || $action=='edit')
    {
        if ($action=='add')
        {
            $button="Save Prospect";
        } else {
            $button="Update Prospect";
            $sql="SELECT * FROM prospects WHERE id=$id";
            $dbProspect=dbselectsingle($sql);
            $prospect=$dbProspect['data'];
            $account_name=stripslashes($prospect['account_name']);
            $contact_name=stripslashes($prospect['contact_name']);
            $email=stripslashes($prospect['email']);
            $account_number=stripslashes($prospect['account_number']);
            $source=$sources[$prospect['source']];
            $county=stripslashes($prospect['county']);
            $address1=stripslashes($prospect['address1']);
            $address2=stripslashes($prospect['address2']);
            $city=stripslashes($prospect['city']);
            $state=stripslashes($prospect['state']);
            $zip=stripslashes($prospect['zip']);
            $phone=stripslashes($prospect['phone']);
            $fax=stripslashes($prospect['fax']);
            $tollfree=stripslashes($prospect['tollfree']);
            $website=stripslashes($prospect['website']);
            $code=$prospect['naics_code'];
            $branch=stripslashes($prospect['branch']);
            $female_owned=stripslashes($prospect['female_owned']);
            $do_not_call=stripslashes($prospect['do_not_call']);
            $out_of_business=stripslashes($prospect['out_of_business']);
            
        }
        print "<form method=post class='form-horizontal'>\n";
        if($id!=0){
            print "This record was created $sources[$source]<br />";
        }
        make_text('account_name',$account_name,'Account Name','',30);
        make_text('account_number',$account_number,'Account Number','',30);
        make_select('naics_code',$codes[$code],$codes,'NAICS Code');
        make_text('contact_name',$contact_name,'Contact Name','',30);
        make_text('email',$email,'Email Address','',30);
        make_text('phone',$phone,'Phone Number','',30);
        make_text('fax',$phone,'Fax Number','',30);
        make_text('tollfree',$phone,'Toll Free Number','',30);
        make_text('website',$website,'Website','',30);
        make_text('address1',$address1,'Address Line 1','',30);
        make_text('address2',$address2,'Address Line 2','',30);
        make_text('city',$city,'City','',30);
        make_text('county',$county,'County','',30);
        make_state('state',$state,'State');
        make_text('zip',$zip,'Zip','',30);
        make_checkbox('female_owned',$female_owned,'Female Owned Business');
        make_checkbox('branch',$branch,'Branch or Single','Check if this is a branch location / chain');
        make_checkbox('out_of_business',$out_of_business,'Out of business?','Check if this is a business has been closed.');
        make_checkbox('do_not_call',$do_not_call,'Do not contact?','Check if this is a business requests to not be contacted by us.');
        make_hidden('id',$id);
        make_submit('submit',$button);
        print "</form>\n";  
    } elseif($action=='delete') {
        $sql="UPDATE prospects SET deleted=1 WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
        if ($error!='')
        {
            setUserMessage('There was a problem deleting the prospect.<br>'.$error,'error');
        } else {
            //delete any cross references in carrer_groups_xref
            $sql="DELETE FROM prospect_campaign_xref WHERE prospect_id=$id";
            $dbDelete=dbexecutequery($sql);
            setUserMessage('Prospect successfully deleted.','success');
        }
        redirect("?action=list");
    } else {
        $sql="SELECT * FROM prospects WHERE deleted=0 ORDER BY account_name";
        $dbGroups=dbselectmulti($sql);
        tableStart("<a href='?action=add'>Add new prospect</a>,
        <a href='?action=import'>Import Prospects</a>,
        <a href='?action=checkterritory'>Find territory for prospects</a>,
        <a href='?action=resetterritory'>Reset territories for all prospects</a>,
        <a href='?action=getlist'>Generate a list</a>","Account Name, Account Number",4);
        if ($dbGroups['numrows']>0)
        {
            foreach($dbGroups['data'] as $group)
            {
                $aname=$group['account_name'];
                $cname=$group['contact_name'];
                $id=$group['id'];
                print "<tr><td>$aname</td><td>$cname</td>";
                print "<td><a href='?action=edit&id=$id'>Edit</a></td>\n";
                print "<td><a href='?action=delete&id=$id' class='delete'>Delete</a></td>\n";
                print "</tr>";
            }
        }
        tableEnd($dbGroups);
        
    }
}

function save_prospect($action)
{
    $prospectid=$_POST['id'];
    
    $account_name=addslashes($_POST['account_name']);
    $contact_name=addslashes($_POST['contact_name']);
    $email=addslashes($_POST['email']);
    $account_number=addslashes($_POST['account_number']);
    $county=addslashes($_POST['county']);
    $address1=addslashes($_POST['address1']);
    $address2=addslashes($_POST['address2']);
    $city=addslashes($_POST['city']);
    $state=addslashes($_POST['state']);
    $zip=addslashes($_POST['zip']);
    $phone=addslashes($_POST['phone']);
    $fax=addslashes($_POST['fax']);
    $tollfree=addslashes($_POST['tollfree']);
    
    $phone=preg_replace("[^0-9]","",$phone);
    $fax=preg_replace("[^0-9]","",$fax);
    $tollfree=preg_replace("[^0-9]","",$tollfree);
     
    $website=addslashes($_POST['website']);
    $naics_code=$_POST['naics_code'];
    if($_POST['branch']){$branch=1;}else{$branch=0;}
    if($_POST['female_owned']){$female_owned=1;}else{$female_owned=0;}
    if($_POST['do_not_call']){$do_not_call=1;}else{$do_not_call=0;}
    if($_POST['out_of_business']){$out_of_business=1;}else{$out_of_business=0;}
    $dt=date("Y-m-d H:i:s");
    if ($action=='insert')
    {
        $source='manual';
        $sql="INSERT INTO prospects (account_name, account_number, contact_name, email, phone, fax, tollfree, website, address1, address2, city, county, state, zip, naics_code, branch, female_owned, do_not_call, out_of_business, source,created_datetime) VALUES ('$account_name', '$account_name', '$account_number', '$contact_name', '$email', '$phone', '$fax', '$tollfree', '$website', '$address1', '$address2', '$city', '$county', '$state', '$zip', '$naics_code', '$branch', '$female_owned', '$do_not_call', '$out_of_business', '$source', '$dt')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $prospectid=$dbInsert['insertid'];
    } else {
        $sql="UPDATE prospect SET account_name='$accountname', account_number='$account_number', contact_name='$contact_name', email='$email', phone='$phone', fax='$fax', tollfree='$tollfree', website='$website', address1='$address1', address2='$address2', city='$city', county='$county', state='$state', zip='$zip', naics_code='$naics_code', branch='$branch', female_owned='$female_owned', do_not_call='$do_not_call', out_of_business='$out_of_business', updated_datetime='$dt' WHERE id=$prospectid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    
    if ($error!='')
    {
        setUserMessage('There was a problem saving the prospect.<br>'.$error,'error');
    } else {
        setUserMessage('The prospect was successfully saved.','success');
    }
    redirect("?action=list");
}



function prospect_import_select()
{
    $types=array('msg'=>"MSG",'vdata'=>"Vision Data");
    print "<form method=post enctype='multipart/form-data'>";
    make_select("import_type",'msg',$types,'Type of file to import');
    make_file('prospect','File', 'Select a file to import');
    make_submit('submit',"Import Prospects");
    print "</form>";    
}


function prospect_import_msg()
{
    $file=$_FILES['prospect']['tmp_name'];
    $contents=file_get_contents($file);
    //break it up by line
    $lines=explode("\n",$contents);
    
    //load naics codes
    $sql="SELECT * FROM naics_codes";
    $dbCodes=dbselectmulti($sql);
    if($dbCodes['numrows']>0)
    {
        foreach($dbCodes['data'] as $code)
        {
            $codes[$code['naics_code']]=$code['id']; // reverse it since we'll be looking up naics code, this will be easier
        }
    } else {
        $codes=array(); // initialize it blank
    }
    
    //load existing prospects by phone number so we don't import those
    $sql="SELECT id, phone FROM prospects";
    $dbProspects=dbselectmulti($sql);
    if($dbProspects['numrows']>0)
    {
        foreach($dbProspects['data'] as $p)
        {
            $prospects[$p['id']]=$p['phone']; // phone numbers should be unique
        }
    } else {
        $prospects=array(); // initialize it blank
    }
    $siccodes=array();
    $fullsic=array();
    $inserts='';   
    foreach($lines as $line)
    {
        $parts=convertCSVtoArray($line);
        
        
        $contact=addslashes(trim($parts[0]));
        $accountName=addslashes(trim($parts[1]));
        $address1=addslashes(trim($parts[55]));
        $city=addslashes(trim($parts[56]));
        $state=addslashes(trim($parts[57]));
        $zip=trim($parts[58]);
        $lat=trim($parts[60]);
        $lng=trim($parts[61]);
        $lat=substr($lat,0,strlen($lat)-6).".".substr($lat,strlen($lat)-6);
        $lng=substr($lng,0,strlen($lng)-6).".".substr($lng,strlen($lng)-6);
        $county=addslashes(trim($parts[2]));
        $phone=trim($parts[3]);
        $fax=trim($parts[4]);
        $tollfree=trim($parts[5]);
        $web=addslashes(trim($parts[6]));
        $naics=substr(trim($parts[26]),0,6);
        $naics_code=$codes[$naics];
        $web=addslashes(trim($parts[6]));
        $sic=trim($parts[7]);
        $sicname=addslashes(trim($parts[8]));
        
        $revenue=str_replace("\$","",$parts[30]);
        $employees=$parts[28];
        $credit=trim($parts[40]);
        
        if(trim($parts[36])=="SINGLE LOC"){$branch=0;}else{$branch=0;}
        if(trim($parts[49])=="Y"){$female=1;}else{$female=0;}
        
        if(!in_array($phone,$prospects))
        {
            $inserts.="('$accountName', 'import_msg', '".date("Y-m-d H:i:s")."', 
            '$contact', '$address1', '$city', '$county', '$state', '$zip', '$phone', 
            '$fax', '$tollfree', '$web', '$naics_code', $branch, $female, '$lat', '$lng',
            '$sic','$revenue','$employees', '$credit'),";
        } else {
            $sql="UPDATE prospects SET account_name='$accountName', contact_name='$contact', 
        address1='$address1', city='$city', county='$county', state='$state', zip='$zip',  
        fax='$fax', tollfree='$tollfree', website='$web', naics_code='$naics_code', 
        branch='$branch', female_owned='$female', lat='$lat', lng='$lng', sic_code='$sic', 
        revenue='$revenue', employees='$employees', credit_score='$credit' WHERE phone='$phone'";
            $dbUpdate=dbexecutequery($sql);
            $updates++;
        }
        if(!in_array($sic,$siccodes))
        {
            $siccodes[]=$sic;
            $fullsic[]=array('code'=>$sic,'name'=>$sicname);
        }
    }
    if($inserts!='')
    {
        $inserts=rtrim($inserts,',');
        $sql="INSERT INTO prospects (account_name, source, created_datetime, contact_name, 
        address1, city, county, state, zip, phone, fax, tollfree, website, naics_code, 
        branch, female_owned, lat, lng, sic_code, revenue, employees, credit_score) VALUES $inserts";
        $dbBulkInsert=dbinsertquery($sql);
        if($dbBulkInsert['error']!='')
        {
            print "There was a problem importing the file.<br /><br />";
            print $dbBulkInsert['error'];
        } else {
            print "Successfully imported ".$dbBulkInsert['numrows']." records.<br />";
        }
    } else {
        if($updates>0)
        {
            print "Updated $updates records with new information.<br />";
        } else {
            print "Nothing to insert, everything was already updated.";
        }
    }
    if(count($fullsic)>0)
    {
        $inserts='';
        foreach($fullsic as $sic)
        {
            if($sic['code']!='')
            {
                $inserts.="($sic[code],'$sic[name]'),";
            }
        }
        $inserts=rtrim($inserts,',');
        $sql="INSERT INTO sic_codes (sic_code, sic_name) VALUES $inserts";
        $dbInsert=dbinsertquery($sql);
    }
}

function prospect_import_vdata()
{
    $file=$_FILES['prospect']['tmp_name'];
    $contents=file_get_contents($file);
    //break it up by line
    $lines=explode("\n",$contents);
    
    //load existing prospects by phone number so we don't import those
    $sql="SELECT id, phone FROM prospects";
    $dbProspects=dbselectmulti($sql);
    if($dbProspects['numrows']>0)
    {
        foreach($dbProspects['data'] as $p)
        {
            $prospects[$p['id']]=$p['phone']; // phone numbers should be unique
        }
    } else {
        $prospects=array(); // initialize it blank
    }
    
    $inserts='';    
    foreach($lines as $line)
    {
        $parts=convertCSVtoArray($line);
        
        
        $salesperson=trim($parts[0]);
        $contact=trim($parts[15]);
        $phone=trim($parts[1]);
        $fax='';
        $tollfree='';
        $web='';
        $email='';
        $accountName=trim($parts[2]);
        $address1=trim($parts[3]);
        $address2=trim($parts[4]);
        $address3=trim($parts[5]);
        $address4=trim($parts[6]);
        
        
        $firstWords=trim($parts[7]);
        $publications=trim($parts[8]);
        $class=trim($parts[9]);
        $adNumber=trim($parts[10]);
        $wordCount=trim($parts[11]);
        $timesRun=trim($parts[12]);
        $start=trim($parts[13]);
        $stop=trim($parts[14]);
        $width=trim($parts[16]);
        $height=trim($parts[17]);
        $inches=trim($parts[18]);
        $rate=trim($parts[19]);
        $price=trim($parts[20]);
        $status=trim($parts[21]);
        $pubsRun=trim($parts[22]);
        
        
        $notes="This prospect came from Vision Data Classifieds<br />";
        $notes.="Sales Rep: $salesperson<br />";
        $notes.="Ad number: $adNumber<br />";
        $notes.="Partial ad text: $firstWords<br />";
        $notes.="Publications: $publications<br />";
        $notes.="Word Count: $wordCount<br />";
        $notes.="# Runs: $timesRun<br />";
        $notes.="Ad Status: $status<br />";
        $notes.="Started: $start<br />";
        $notes.="Stopped: $stop<br />";
        $notes.="Width: $width<br />";
        $notes.="Height: $height<br />";
        $notes.="Total Inches: $inches<br />";
        $notes.="Price: $price<br />";
        $notes.="Rate: $rate<br />";
        
        $naics_code='0';
        $cityline='';
        $street='';
        $accountOther='';
        
        //now parse the address lines
        if($address4!='')
        {
            $cityline=$address4;
        }
        
        if($address3!='' && $cityline=='')
        {
            $cityline=$address3;    
        }elseif($address3!='' && $cityline!='')
        {
            $street=$address3;
        }
        
        if($address2!='' && $cityline=='')
        {
            $cityline=$address2;    
        }elseif($address2!='' && $cityline!='' && $street=='')
        {
            $street=$address2;
        }
        if($street!='' && $address1!='')
        {
            $accountOther=$address1;
        } elseif($street=='' && $address1!='')
        {
            $accountOther='';
            $street=$address1;
            
        }
        if($cityline=='')
        {
            $cityline=$address1;
        }
        //chop cityline into parts
        if($cityline!='')
        {
            $cityParts=explode(" ",$cityline);
            $zip=array_pop($cityParts); //last element
            $state=array_pop($cityParts); //new last elementh
            $city=implode(" ",$cityParts); //stick the rest back together in case the city has a two word name like "New Plymouth"
        } else {
            $city='No city';
            $state='No state';
            $zip='No zip';
        }
        $lat='0.000';
        $lng='0.000';
        
        if(!in_array($phone,$prospects))
        {
            $inserts.="('$accountName', 'import_vdata', '".date("Y-m-d H:i:s")."', '$contact', '$address1', '$city', '', '$state', '$zip', '$phone', '$fax', '$tollfree', '$web', '$naics_code', 0, 0, '$lat', '$lng', '$notes'),";
        }
    }
    
    
    if($inserts!='')
    {
        $inserts=rtrim($inserts,',');
        $sql="INSERT INTO prospects (account_name, source, created_datetime, contact, address1, city, county, state, zip, phone, fax, tollfree, website, naics_code, branch, female_owned, lat, lng, notes) VALUES $inserts";
        $dbBulkInsert=dbinsertquery($sql);
    }
}


$Page->footer();
<?php
include '../includes/functions_db.php';
include '../includes/functions_common.php';
include '../includes/functions_formtoolsMobile.php';
include '../includes/functions_mobile.php';
include '../includes/config.php';
mobileHeader();
?>

      <!-- Main component for a primary marketing message or call to action -->
      <div class='col-xs-12'>
      <?php
          global $insertProducts, $users;
          if($_POST)
          {
            $code=addslashes($_POST['code']);    
          } else {
            $code=addslashes($_GET['code']);
          }
          $sql="SELECT A.*, B.account_name, C.pub_name FROM inserts_received A, accounts B, publications C  
          WHERE A.control_number='$code' AND A.advertiser_id = B.id AND A.insert_pub_id = C.id";
          $dbInsert=dbselectsingle($sql);
          $insert=$dbInsert['data'];
          
          if($dbInsert['numrows']==0)
          {
               print "No matching insert.";
          } else {
              print "<h4>Insert Details</h4>";
              print "<b>Advertiser: </b>".stripslashes($insert['account_name'])."<br />\n";
              print "<b>Publication: </b>".stripslashes($insert['pub_name'])."<br />\n";
              print "<b>Insert Date: </b>".date("D m/d/Y",strtotime($insert['scheduled_pubdate']))."<br />\n";
              print "<b>Tagline: </b>".stripslashes($insert['insert_tagline'])."<br />\n";
              print "<b>Received by: </b>".$users[$insert['receive_by']]."<br />\n";
              print "<b>Received on: </b>".date("D m/d/Y H:i",strtotime($insert['scheduled_pubdate']))."<br />\n";
              print "<b>Control #: </b>".stripslashes($insert['control_number'])."<br />\n";
              if($insert['ship_type']=='pallet')
              {
                  print "<b>Shipped on: </b>".stripslashes($insert['ship_quantity'])." pallets<br />\n";
              } else {
                   print "<b>Shipped in: </b>".stripslashes($insert['ship_quantity'])." boxes<br />\n";
              }
              if($GLOBALS['insertUseLocation'] && $insert['storage_location']!=0)
              {
                  $sql="SELECT * FROM insert_storage_location WHERE id=".$insert['storage_location'];
                  $dbLocation=dbselectsingle($sql);
                  $location=$dbLocation['data'];
                  print "<b>Stored in: </b>".stripslashes($location['location_name'])."<br />\n";
                  
              }
              print "<b>The product is a  ".$insert['pages'].' page '.$insertProducts[$insert['product_size']]."</b><br />\n";
              print "<b>Receive Count: </b>".stripslashes($insert['receive_count'])."<br />\n";
              
               if($tagColor!='')
               {
                   print "<b>Tag Color: </b>".stripslashes($insert['tag_color'])."<br />\n";
              
               }
               
               if($singleSheet)
               {
                   print "<b>Product is a single sheet.</b><br />\n";
               }
               if($slickSheet)
               {
                   print "<b>Product is slick which may cause production issues.</b><br />\n";
               }
               if($stickyNote)
               {
                   print "<b>Product is a sticky note.</b><br />\n";
               }
               if($damaged)
               {
                   print "<b>There was some damage noted on arrival:</b>".stripslashes($insert['damage'])."<br />\n";
               }
               if($filename!='')
               {
                   print "<b>Here is a photograph of the cover: <br />
                   <img src='$GLOBALS[serverIPaddress]/artwork/inserts/".$insert['insert_path']."/".$insert['insert_image']."' class='img img-responsive'> ";
               }
          }
      ?>
      
      
         
      </div>
      
<?php
    mobileFooter();
?>
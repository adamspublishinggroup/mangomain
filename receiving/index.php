<?php
include '../includes/functions_db.php';
include '../includes/functions_common.php';
include '../includes/functions_formtoolsMobile.php';
include '../includes/functions_mobile.php';
include '../includes/config.php';
mobileHeader();
?>
      <div class="row">
        <div class="col-xs-12">
            <h3>Recently Received Inserts</h3>
            <?php
                global $pubs;
                $lastDT=date("Y-m-d",strtotime("-1 week"));
                $sql="SELECT A.*, B.account_name FROM inserts_received A, accounts B  
                   WHERE A.advertiser_id=B.id AND A.site_id=$siteID AND A.advertiser_id=B.id  
                   AND A.receive_datetime>='$lastDT'  
                   ORDER BY B.account_name LIMIT 500";
                $dbInserts=dbselectmulti($sql);
                if($dbInserts['numrows']>0)
                {
                    tableStart("",
                   "Control#,Advertiser,Publication,Receive Date,Insert Date, Matched",11);
                       foreach($dbInserts['data'] as $insert)
                        {
                            $insertid=$insert['id'];
                            
                            //$advertisername=$advertisers[$insert['advertiser_id']];
                            $advertisername=stripslashes($insert['account_name']);
                            $tagline=stripslashes($insert['insert_tagline']);
                            $control=stripslashes($insert['control_number']);
                            if($insert['matched'])
                            {
                                $matched='Matched';
                            } else {
                                $matched='Not matched'; 
                            }
                            if($insert['insert_pub_id']==0)
                            {
                                $publication='Not specified';    
                            } else {
                                $publication=$pubs[$insert['insert_pub_id']];
                            }
                            $insertdate=date("D m/d/Y",strtotime($insert['scheduled_pubdate']));
                            $receivedate=date("D m/d/Y",strtotime($insert['receive_date']));
                            print "<tr>\n";
                            print "<td><a href='receivedInserts.php?action=edit&id=$insertid'>$control</a>";
                            print "<br>$tagline";
                            print "</td>\n";
                            print "<td>$advertisername</td>";
                            print "<td>$publication</td>";
                            print "<td>$receivedate</td>\n";
                            print "<td>$insertdate</td>\n";
                            print "<td>$matched</td>\n";
                            print "<td><a href='receivedInserts.php?action=edit&id=$insertid'>Edit</a></td>\n";
                            print "</tr>\n";
                        }
                   
                
                tableEnd($dbInserts);
                } 
                print "<hr><h3>Unmatched Inserts</h3>";
                $sql="SELECT A.*, B.account_name FROM inserts_received A, accounts B  
                   WHERE A.advertiser_id=B.id AND A.site_id=$siteID AND A.advertiser_id=B.id  
                   AND A.matched=0   
                   ORDER BY B.account_name LIMIT 500";
                $dbInserts=dbselectmulti($sql);
                if($dbInserts['numrows']>0)
                {
                    tableStart("",
                   "Control#,Advertiser,Publication,Receive Date,Insert Date",11);
                       foreach($dbInserts['data'] as $insert)
                        {
                            $insertid=$insert['id'];
                            
                            //$advertisername=$advertisers[$insert['advertiser_id']];
                            $advertisername=stripslashes($insert['account_name']);
                            $tagline=stripslashes($insert['insert_tagline']);
                            $control=stripslashes($insert['control_number']);
                            if($insert['matched'])
                            {
                                $matched='Matched';
                            } else {
                                $matched='Not matched'; 
                            }
                            if($insert['insert_pub_id']==0)
                            {
                                $publication='Not specified';    
                            } else {
                                $publication=$pubs[$insert['insert_pub_id']];
                            }
                            $insertdate=date("D m/d/Y",strtotime($insert['scheduled_pubdate']));
                            $receivedate=date("D m/d/Y",strtotime($insert['receive_date']));
                            print "<tr>\n";
                            print "<td><a href='receivedInserts.php?action=edit&id=$insertid'>$control</a>";
                            print "<br>$tagline";
                            print "</td>\n";
                            print "<td>$advertisername</td>";
                            print "<td>$publication</td>";
                            print "<td>$receivedate</td>\n";
                            print "<td>$insertdate</td>\n";
                            print "<td><a href='receivedInserts.php?action=edit&id=$insertid'>Edit</a></td>\n";
                            print "</tr>\n";
                        }
                   
                
                tableEnd($dbInserts);
                }   
            ?>
            
            
        </div>
      </div>
<?php
    mobileFooter();
?>
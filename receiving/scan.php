<?php

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../favicon.ico">

    <title>Mango Mobile Receiving Tool</title>

    <!-- Bootstrap core CSS -->
    <link href="../includes/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <!-- Fixed navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="../Index.php">Mango</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="receive.php">Receive</a></li>
            <li><a href="scan.php">Scan</a></li>
            <li><a href="index.php">Inserts</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      
        <div id="barcode"></div>
          
      
      </div>
    </nav>
    <style>
    body {
        padding-top:70px;
    }
    #interactive.viewport {
      width: 640px;
      height: 480px;
    }

    /* line 6, ../sass/_viewport.scss */
    #interactive.viewport canvas, video {
      float: left;
      width: 640px;
      height: 480px;
    }
    /* line 10, ../sass/_viewport.scss */
    #interactive.viewport canvas.drawingBuffer, video.drawingBuffer {
      margin-left: -640px;
    }
    .scanner-overlay {
      display: none;
      width: 640px;
      height: 510px;
      position: absolute;
      padding: 20px;
      top: 50%;
      margin-top: -275px;
      left: 50%;
      margin-left: -340px;
      background-color: #FFF;
      -moz-box-shadow: #333333 0px 4px 10px;
      -webkit-box-shadow: #333333 0px 4px 10px;
      box-shadow: #333333 0px 4px 10px;
    }
    /* line 20, ../sass/_overlay.scss */
    .scanner-overlay > .header {
      position: relative;
      margin-bottom: 14px;
    }
    /* line 23, ../sass/_overlay.scss */
    .scanner-overlay > .header h4, .scanner-overlay > .header .close {
      line-height: 16px;
    }
    /* line 26, ../sass/_overlay.scss */
    .scanner-overlay > .header h4 {
      margin: 0px;
      padding: 0px;
    }
    /* line 30, ../sass/_overlay.scss */
    .scanner-overlay > .header .close {
      position: absolute;
      right: 0px;
      top: 0px;
      height: 16px;
      width: 16px;
      text-align: center;
      font-weight: bold;
      font-size: 14px;
      cursor: pointer;
    }
    </style>
    <div class="container">

      <div class='row clearfix'>
          <div class='col-xs-12'>
              <form method=post action='scanResults.php' class="form-horizontal">
                  <div class="form-group">
                    <label for="code" class="col-sm-2 control-label">Code</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="code" name='code' placeholder="Enter Code">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button id="submit" type='submit' class='btn btn-success'>Lookup Insert</button>
                    </div>
                  </div>
                  <input type='hidden' name='action' value='manual' />
              </form>
                    
          </div>
          
      </div>
      
       <div id="interactive" class="viewport col-xs-12"></div> 
         

    </div> <!-- /container -->
    

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="../includes/bootstrap/js/bootstrap.min.js"></script> 
    <script src="quagga/quagga.min.js" type="text/javascript"></script>
    <script>
    $(function() {
        
        
        var resultCollector = Quagga.ResultCollector.create({
            capture: true,
            capacity: 20,
            blacklist: [{code: "2167361334", format: "i2of5"}],
            filter: function(codeResult) {
                // only store results which match this constraint
                // e.g.: codeResult
                return true;
            }
        });
        var App = {
            init : function() {
                var self = this;

                Quagga.init(this.state, function(err) {
                    if (err) {
                        return self.handleError(err);
                    }
                    Quagga.registerResultCollector(resultCollector);
                    App.attachListeners();
                    Quagga.start();
                });
            },
            handleError: function(err) {
                console.log(err);
            },
            attachListeners: function() {
                var self = this;
                /*
                $(".controls").on("click", "button.stop", function(e) {
                    e.preventDefault();
                    Quagga.stop();
                    self._printCollectedResults();
                });
                $(".controls").on("click", "button.start", function(e) {
                    App.init();              
                });
                */
            },
            _printCollectedResults: function() {
                var results = resultCollector.getResults();
                results.forEach(function(result) {
                    //$node = $('<p class="node"></p>').html(code);
                    //$("#barFound").prepend($node);
                    
                });
                
                Quagga.stop();
                
            },
            _accessByPath: function(obj, path, val) {
                var parts = path.split('.'),
                    depth = parts.length,
                    setter = (typeof val !== "undefined") ? true : false;

                return parts.reduce(function(o, key, i) {
                    if (setter && (i + 1) === depth) {
                        o[key] = val;
                    }
                    return key in o ? o[key] : {};
                }, obj);
            },
            _convertNameToState: function(name) {
                return name.replace("_", ".").split("-").reduce(function(result, value) {
                    return result + value.charAt(0).toUpperCase() + value.substring(1);
                });
            },
            detachListeners: function() {
                //$(".controls").off("click", "button.stop");                       
            },
            setState: function(path, value) {
                var self = this;

                if (typeof self._accessByPath(self.inputMapper, path) === "function") {
                    value = self._accessByPath(self.inputMapper, path)(value);
                }

                self._accessByPath(self.state, path, value);

                console.log(JSON.stringify(self.state));
                App.detachListeners();
                Quagga.stop();
                App.init();
            },
            inputMapper: {
                inputStream: {
                    constraints: function(value){
                        var values = value.split('x');
                        return {
                            width: parseInt(values[0]),
                            height: parseInt(values[1]),
                            facing: "environment"
                        }
                    }
                },
                numOfWorkers: function(value) {
                    return parseInt(value);
                },
                decoder: {
                    readers: function(value) {
                        return [value + "_reader"];
                    }
                }
            },
            state: {
                inputStream: {
                    type : "LiveStream",
                    constraints: {
                        width: 640,
                        height: 480,
                        facing: "environment" // or user
                    }
                },
                locator: {
                    patchSize: "medium",
                    halfSample: true
                },
                numOfWorkers: 4,
                decoder: {
                    readers : [ "code_128_reader"]
                },
                locate: true
            },
            lastResult : null
        };

        App.init();

        Quagga.onProcessed(function(result) {
            var drawingCtx = Quagga.canvas.ctx.overlay,
                drawingCanvas = Quagga.canvas.dom.overlay;

            if (result) {
                if (result.boxes) {
                    drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                    result.boxes.filter(function (box) {
                        return box !== result.box;
                    }).forEach(function (box) {
                        Quagga.ImageDebug.drawPath(box, {x: 0, y: 1}, drawingCtx, {color: "green", lineWidth: 2});
                    });
                }

                if (result.box) {
                    Quagga.ImageDebug.drawPath(result.box, {x: 0, y: 1}, drawingCtx, {color: "#00F", lineWidth: 2});
                }

                if (result.codeResult && result.codeResult.code) {
                    Quagga.ImageDebug.drawPath(result.line, {x: 'x', y: 'y'}, drawingCtx, {color: 'red', lineWidth: 3});
                }
            }
        });

        Quagga.onDetected(function(result) {
            var code = result.codeResult.code;

            if (App.lastResult !== code && code != '') {
                App.lastResult = code;
                var $node = null, canvas = Quagga.canvas.dom.image;

                //$node = $('<li><div class="thumbnail"><div class="imgWrapper"><img /></div><div class="caption"><h4 class="code"></h4></div></div></li>');
                $node = $('<p class="node"></p>').html("Found: "+code);
                $("#barFound").html(code);
                Quagga.stop();
                window.location="scanResults.php?code="+code;
                
            }
        });

    });
    </script>
     
  </body>
</html>

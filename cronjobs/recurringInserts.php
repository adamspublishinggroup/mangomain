<?php
  /*
  * This script is created to build recurring jobs, main function first is to build a set of dates for the jobs to be run for
  * next phase is to create the actual jobs.
  * We'll start with test cases, as defined in the jobRecurring.php script.
  
  $recurFrequencies:
  0 = "Every Week"
  1 = "Every Other Week"
  2 = "Every 3rd Week"
  3 = "Every 4th Week"
  4 = "On the first"
  5 = "On the second"
  6 = "On the third"
  7 = "On the fourth"
  8 = "On the last"
  */
  
  if($_GET['mode']=='test' || $_GET['mode']=='manual'){
      $systemMode=true;
      include("../includes/boot.php");
      if($_GET['jobid']){$jobid=intval($_GET['jobid']);}else{$jobid=0;}
      init_recurringJob($jobid);
  }
  
  function init_recurringJob($jobid=0)
  {
      if($_GET['mode']=='test'){$test = true;}else{$test = false;};
      
      if($test==true)
      {
          print "<h4>In test mode</h4>";
          
          
          $jobs[0]=array('id'=>0,'days_of_week'=>'0|4|6', 'days_out'=>365,'active'=>1,'recur_frequency'=>'0',
      'start_date'=>'2017-01-24', 'end_date'=>'2017-05-31','end_date_checked'=>0,'pub_id'=>1);
          
          $jobs[1]=array('id'=>1,'days_of_week'=>'1', 'days_out'=>365,'active'=>1,'recur_frequency'=>'1|2',
      'start_date'=>'2017-02-24', 'end_date'=>'2017-06-30','end_date_checked'=>1,'pub_id'=>7);
          
          $jobs[2]=array('id'=>2,'days_of_week'=>'3', 'days_out'=>90,'active'=>1,'recur_frequency'=>'3',
      'start_date'=>'2017-03-24', 'end_date'=>'2017-06-30','end_date_checked'=>1,'pub_id'=>2);
          
          $jobs[3]=array('id'=>3,'days_of_week'=>'3|5', 'days_out'=>90,'active'=>1,'recur_frequency'=>'4',
      'start_date'=>'2017-04-15', 'end_date'=>'2017-06-30','end_date_checked'=>1,'pub_id'=>13);
            
      } else {
          if ($jobid!=0)
          {
              $sql="SELECT * FROM inserts_recurring WHERE id=$jobid";
          } else {
              $sql="SELECT * FROM inserts_recurring WHERE active=1";
          }
          $dbRecurring=dbselectmulti($sql);
          $jobs = $dbRecurring['data'];
      }
      if(count($jobs)>0)
      {
          $inserted=0;
          ob_implicit_flush(true);
          ob_start();
          $foundJobs = array();
          foreach($jobs as $job)
          {
              //get starting date
              //if this is a new job, we default to start_date, otherwise set to last date of a job in the system
              $startDate = $job['start_date'];
              if($job['active']==0){continue;}
              if($test)
              {
                  $startDate = $job['start_date'];
              } else {
                  $sql="SELECT MAX(insert_date) as sdate FROM inserts_schedule WHERE recurring_id=$job[id]";
                  $dbMaxJob = dbselectsingle($sql);
                  if($dbMaxJob['numrows']>0)
                  {
                      //only here if this is not a new job
                      $startDate = date("Y-m-d",strtotime($dbMaxJob['data']['sdate']." +1 day")); //1 day older than the current max
                  }
              }
              if(strtotime($startDate)<time()){$startDate = date("Y-m-d");}
              //print "Checking for recurrences for $job[id] with startDate $startDate<br>";
              $dates = findRecurrences($job,$startDate);
              //now we would actually create the recurrences
              //print "Processing $job[id] and found ".count($dates)." dates<br>";
              //print " --- $sql<br>";
              if($test==false)
              {
                  if(count($dates)>0)
                  {
                      foreach($dates as $jdate)
                      {
                          //print "Inserting for $jdate for job $job[id] with a starting date of $startDate test is ".($test?"true":"false")."<br>";
                          $inserted++;
                          if($_GET['nosave'])
                          {
                              print "Would have added a new recurrence at $jdate for $job[id]<br>";
                          } else {
                              insertRecurring($jdate,$job); 
                          }
                      }
                      
                  }
              } else {
                  $foundJobs[$job['id']]['jobDates'] = $dates;
                  $foundJobs[$job['id']]['job']=$job;
              }
              ob_flush();
          }
          ob_end_flush();
      } else {
          print "No jobs found<br>";
      }
      print "Created a total of $inserted inserted<br>";
      if($test){showRecurringOutput($foundJobs);}
  }
  
  function showRecurringOutput($jobs){
  
      //lets show the dates we've found
      if(count($jobs)>0)
      {
          global $pubs;
          foreach($jobs as $key=>$j)
          {
              $foundDates = $j['jobDates'];
              $j = $j['job'];
              print "
              
              <table class='table table-striped table-bordered'>\n";
              print "<tr><th>Recurrence ID: $j[id] - Publication: ".$pubs[$j['pub_id']]."</th><th>Starting Date: $j[start_date] | Ending date: $j[end_date] | End checked: $j[end_date_checked]</tr>";
              print "<tr><th>DoW</th><th>Date</th></tr>\n";
              if(count($foundDates)>0)
              {
                  foreach($foundDates as $d)
                  {
                      print "<tr>";
                      print "<td>".date("w",strtotime($d))."</td>";
                      print "<td>$d</td>";
                      print "</tr>\n";
                  }
              }
              print "</table>
              ";
          }
      }
      
      print $GLOBALS['notes'];
  }
  
  
  
function insertRecurring($pubdate,$recJob)
{
    $dow=date("w",strtotime($pubdate));
    //lets first create the new insert record
    $sql="INSERT INTO inserts (advertiser_id, insert_tagline, product_size, product_pages, standard_pages, single_sheet, slick_sheet, 
    sticky_note, insert_notes, spawned, site_id, created_datetime, created_by, recurring_id) VALUES ('$recJob[account_id]', 
    '$recJob[insert_tagline]', '$recJob[product_size]', '$recJob[product_pages]', '$recJob[standard_pages]', '$recJob[single_sheet]', 
    '$recJob[slick_sheet]', '$recJob[sticky_note]','$recJob[notes]', 1, '$recJob[site_id]', '".date("Y-m-d H:i")."', 0, $recJob[id]";
    $dbInsert=dbinsertquery($sql);
    $insertID=$dbInsert['insertid'];
    if($dbInsert['error']=='')
    {
        // add the schedule and zoning
        $sql="INSERT INTO inserts_schedule (insert_id, pub_id, pressrun_id, package_run_id, run_id, insert_quantity, insert_date, recurrence_id) 
        VALUES ('$insertID', $recJob[pub_id], $recJob[pressrun_id], 0, $recJob[pressrun_id], $recJob[quantity], '$pubdate',$recJob[id])";
        $dbInsert=dbinsertquery($sql);
        $scheduleID=$dbInsert['insertid'];
        if($dbInsert['error']=='')
        {
            //get the zoning
            $sql="SELECT * FROM inserts_recurring_zoning WHERE recurrence_id=$recJob[id] AND dow=$dow";
            $dbZones = dbselectmulti($sql);
            if($dbZones['numrows']>0)
            {
                foreach($dbZones['data'] as $zone)
                {
                    $zoneInserts[]="($scheduleID,$insertID,$zone[id],$zone[zone_count])";
                    
                    //see if there are any trucks
                    $sql="SELECT * FROM inserts_recurring_zoning_trucks WHERE recurrence_id=$recJob[id] AND zone_id=$zone[id]";
                    $dbTrucks=dbselectmulti($sql);
                    if($dbTrucks['numrows']>0)
                    {
                        foreach($dbTrucks['data'] as $truck)
                        {
                            $truckInserts[]="($scheduleID,$insertID,$zone[id],$zone[zone_count],$truck[id])";
                        }
                    }
                }
                if(count($zoneInserts)>0)
                {
                    $sql="INSERT INTO inserts_zonine (schedule_id, insert_id, zone_id, zone_count) VALUES ".implode(",",$zoneInserts);
                    $dbZoneInserts=dbinsertquery($sql);
                    if($dbZoneInserts['error']=='')
                    {
                        $sql="INSERT INTO inserts_zoning_trucks (schedule_id, insert_id, zone_id, zone_count, truck_id) VALUES ".implode(",",$trucksInserts);
                        $dbTruckInserts=dbinsertquery($sql);
                    }
                }
            }
        }
    } 
    
    $GLOBALS['notes'].="Created an insert for $pubdate<br />\n";
    $now=date("Y-m-d H:i:s");
    $by='0';
    
       
    
}
   
<?php
  /*
  * This script is created to build recurring jobs, main function first is to build a set of dates for the jobs to be run for
  * next phase is to create the actual jobs.
  * We'll start with test cases, as defined in the jobRecurring.php script.
  
  $recurFrequencies:
  0 = "Every Week"
  1 = "Every Other Week"
  2 = "Every 3rd Week"
  3 = "Every 4th Week"
  4 = "On the first"
  5 = "On the second"
  6 = "On the third"
  7 = "On the fourth"
  8 = "On the last"
  */
  
  if($_GET['mode']=='test' || $_GET['mode']=='manual'){
      $systemMode=true;
      include("../includes/boot.php");
      if($_GET['jobid']){$jobid=intval($_GET['jobid']);}else{$jobid=0;}
      init_recurringJob($jobid);
  }
  
  function init_recurringJob($jobid=0)
  {
      if($_GET['mode']=='test'){$test = true;}else{$test = false;};
      
      if($test==true)
      {
          print "<h4>In test mode</h4>";
          if(isset($_GET['jobid']))
          {
              $sql="SELECT * FROM jobs_recurring WHERE id=$jobid";
              $dbRecurring=dbselectmulti($sql);
              $jobs = $dbRecurring['data']; 
          }  else {
          
          $jobs[0]=array('id'=>0,'days_of_week'=>'0|4|6', 'days_out'=>365,'active'=>1,'days_prev'=>0,'recur_frequency'=>'0',
      'start_date'=>'2017-01-24', 'end_date'=>'2017-05-31','end_date_checked'=>0,'pub_id'=>1);
          
          $jobs[1]=array('id'=>1,'days_of_week'=>'1', 'days_out'=>365,'active'=>1,'days_prev'=>0,'recur_frequency'=>'1|2',
      'start_date'=>'2017-02-24', 'end_date'=>'2017-06-30','end_date_checked'=>1,'pub_id'=>7);
          
          $jobs[2]=array('id'=>2,'days_of_week'=>'3', 'days_out'=>90,'active'=>1,'days_prev'=>0,'recur_frequency'=>'3',
      'start_date'=>'2017-03-24', 'end_date'=>'2017-06-30','end_date_checked'=>1,'pub_id'=>2);
          
          $jobs[3]=array('id'=>3,'days_of_week'=>'3|5', 'days_out'=>90,'active'=>1,'days_prev'=>0,'recur_frequency'=>'4',
      'start_date'=>'2017-04-15', 'end_date'=>'2017-06-30','end_date_checked'=>1,'pub_id'=>13);
      
          $jobs[4]=array('id'=>4,'days_of_week'=>'3', 'days_out'=>90,'active'=>1,'days_prev'=>0,'recur_frequency'=>'4',
      'start_date'=>'2017-04-15', 'end_date'=>'2017-10-30','end_date_checked'=>1,'pub_id'=>3, 'specified_date'=>10);
          }
      } else {
          if ($jobid!=0)
          {
              $sql="SELECT * FROM jobs_recurring WHERE id=$jobid";
          } else {
              $sql="SELECT * FROM jobs_recurring WHERE active=1";
          }
          $dbRecurring=dbselectmulti($sql);
          $jobs = $dbRecurring['data'];
      }
      if(count($jobs)>0)
      {
          $inserted=0;
          ob_implicit_flush(true);
          ob_start();
          $foundJobs = array();
          foreach($jobs as $job)
          {
              //get starting date
              //if this is a new job, we default to start_date, otherwise set to last date of a job in the system
              $startDate = $job['start_date'];
              if($job['active']==0){continue;}
              if($test)
              {
                  $startDate = $job['start_date'];
              } else {
                  $sql="SELECT MAX(pub_date) as sdate FROM jobs WHERE recurring_id=$job[id]";
                  $dbMaxJob = dbselectsingle($sql);
                  if($dbMaxJob['numrows']>0)
                  {
                      //only here if this is not a new job
                      $startDate = date("Y-m-d",strtotime($dbMaxJob['data']['sdate']." +1 day")); //1 day older than the current max
                  }
              }
              if(strtotime($startDate)<time()){$startDate = date("Y-m-d");}
              //print "Checking for recurrences for $job[id] with startDate $startDate<br>";
              $dates = findRecurrences($job,$startDate);
              //now we would actually create the recurrences
              //print "Processing $job[id] and found ".count($dates)." dates<br>";
              //print " --- $sql<br>";
              if($test==false)
              {
                  if(count($dates)>0)
                  {
                      foreach($dates as $jdate)
                      {
                          //print "Inserting for $jdate for job $job[id] with a starting date of $startDate test is ".($test?"true":"false")."<br>";
                          $inserted++;
                          if($_GET['nosave'])
                          {
                              print "Would have added a new recurrence at $jdate for $job[id]<br>";
                          } else {
                              insertRecurring($jdate,$job); 
                          }
                      }
                      clearCache('presscalendar');
                  }
              } else {
                  $foundJobs[$job['id']]['jobDates'] = $dates;
                  $foundJobs[$job['id']]['job']=$job;
              }
              ob_flush();
          }
          ob_end_flush();
      } else {
          print "No jobs found<br>";
      }
      print "Inserted a total of $inserted jobs<br>";
      if($test){showRecurringOutput($foundJobs);}
  }
  
  function showRecurringOutput($jobs){
  
      //lets show the dates we've found
      if(count($jobs)>0)
      {
          global $pubs;
          foreach($jobs as $key=>$j)
          {
              $foundDates = $j['jobDates'];
              $j = $j['job'];
              print "
              
              <table class='table table-striped table-bordered'>\n";
              print "<tr><th>Recurrence ID: $j[id] - Publication: ".$pubs[$j['pub_id']]."</th><th>Starting Date: $j[start_date] | Ending date: $j[end_date] | End checked: $j[end_date_checked]</tr>";
              print "<tr><th>DoW</th><th>Date</th></tr>\n";
              if(count($foundDates)>0)
              {
                  foreach($foundDates as $d)
                  {
                      print "<tr>";
                      print "<td>".date("w",strtotime($d))."</td>";
                      print "<td>$d</td>";
                      print "</tr>\n";
                  }
              }
              print "</table>
              ";
          }
      }
      
      print $GLOBALS['notes'];
  }
  
  
  
function insertRecurring($pubdate,$recJob)
{
    $GLOBALS['notes'].="Inserting a recurring job on $pubdate<br />\n";
    global $siteID;
    $daysprev=$recJob['days_prev'];
    $now=date("Y-m-d H:i:s");
    $by='0';
    $layoutid=$recJob['layout_id'];
    $insertpubid=$recJob['insert_pub_id'];
    if($insertpubid==0 || $insertpubid== ""){$insertpubid=$recJob['pub_id'];}
    $startdatetime=$pubdate." ".$recJob['start_time'];
    //print "Start date time is $startdatetime<br />\n";
    //this should mean that the job is running the day before pub
    $startdatetime=date("Y-m-d H:i",strtotime($startdatetime."-$daysprev days"));
    $runtime=$recJob['draw']/($GLOBALS['pressSpeed']/60); //this should give us a number of minutes;
    $runtime=round($runtime,0);
    $runtime+=$GLOBALS['pressSetup'];
    //print "Showing a runtime of $runtime<br />\n";
    $stopdatetime=date("Y-m-d H:i",strtotime($startdatetime."+$runtime minutes"));
    //print "Stop date time is $stopdatetime<br />\n";
    if ($recJob['use_draw'])
    {
        $draw=$recJob['draw'];   
    } else {
        $draw=0;
    }
    $recNotes=addslashes(htmlentities($recJob['notes']));
    $GLOBALS['notes'].= "We would are inserting a record right now with startdate of $startdatetime and enddate of $stopdatetime.<br />";
    if($pressid=0){$pressid=1;}
    
    $sql="INSERT INTO jobs (created_time, created_by, pub_id, run_id, startdatetime, enddatetime, 
    recurring_id, notes_job, papertype, papertype_cover, lap, folder, pub_date, site_id, insert_source, pagewidth, draw, 
    layout_id, insert_pub_id, slitter, folder_pin, job_type, quarterfold, rollSize, press_id, request_printdate) VALUES 
    ('$now', '$by', '$recJob[pub_id]', '$recJob[run_id]', '$startdatetime', '$stopdatetime', '$recJob[id]', 
    '$recNotes', '$recJob[papertype]', '$recJob[papertype_cover]', '$recJob[lap]', '$recJob[folder]', 
    '$pubdate', '".SITE_ID."', 'autorecurring', '$recJob[pagewidth]', '$draw', '$recJob[layout_id]', '$insertpubid', '$recJob[slitter]', 
    '$recJob[folder_pin]', '$recJob[job_type]', '$recJob[quarterfold]', '$recJob[rollSize]', '$recJob[press_id]', '$startdatetime')";
    $dbInsert=dbinsertquery($sql);
    $jobid=$dbInsert['insertid'];
    $GLOBALS['notes'].="We created a new record with: <br />$sql<br /><br />";
        
    
    if ($dbInsert['error']!='')
    {
        $GLOBALS['notes'].="Error inserting this one:<br>\n";
        $GLOBALS['notes'].=$dbInsert['error']."<br /><br />\n";
    } else {
        //if section_id is not 0, lets duplicate recurring_section to job_section
        if ($recJob['section_id']!=0)
        {
            $sql="SELECT * FROM jobs_recurring_sections WHERE id=$recJob[section_id]";
            $dbRS=dbselectsingle($sql);
            $rs=$dbRS['data'];
            $fields='';
            $values='';
            foreach($rs as $key=>$value)
            {
                if ($key=='job_id')
                {
                    $fields.="job_id,";
                    $values.="'$jobid',";
                } elseif($key=='id') {
                    //skip this one
                } else {
                    $fields.="$key,";
                    $values.="'$value',";
                }           
            }
            $fields=substr($fields,0,strlen($fields)-1);
            $values=substr($values,0,strlen($values)-1);
            $sql="INSERT INTO jobs_sections ($fields) VALUES ($values)";
            $dbInsert=dbinsertquery($sql);
            //print $sql;
        }
        
        //create a stat record
        $sql="INSERT INTO job_stats (job_id, added_by) VALUES ($jobid, 'recurringPressJobs.php insertRecurring')";
        $dbStat=dbinsertquery($sql);
        $statsid=$dbStat['insertid'];
        
        
        $sql="UPDATE jobs SET stats_id=$statsid, scheduled_time='$scheduledtime', scheduled_by='$scheduledby', 
        enddatetime='$stopdatetime' WHERE id=$jobid";
        //print "Update sql is $sql<br />\n";
        $dbUpdate=dbexecutequery($sql);
        
        saveRecLayout($layoutid,$jobid,$siteID);
        $GLOBALS['notes'].="Sucessfully inserted a run for $pubdate at $startdatetime, finishing at $stopdatetime<br />\n";
        $GLOBALS['notes'].="Passing $jobid over to the 2Inserter function<br />\n";
        printJob2Inserter($jobid);
        $GLOBALS['notes'].="Passing $jobid over to the 2Delivery function<br />\n";
        printJob2Delivery($jobid);
        $GLOBALS['notes'].="Passing $jobid over to the 2Bindery function<br />\n";
        printJob2Bindery($jobid);
        $GLOBALS['notes'].="Passing $jobid over to the 2Addressing function<br />\n";
        printJob2Addressing($jobid);
    }
    
}

function handleMissingInsertJobs()
{
    if ($_GET['mode']=='manual')
    {
        $debug=true;
    }
    //this function should look for jobs out for the next 90 days.
    $sql="SELECT id FROM jobs WHERE pub_date<='".date("Y-m-d",strtotime("+90 days"))."' AND pub_date>='".date("Y-m-d")."'";
    $dbJobs=dbselectmulti($sql);
    if($dbJobs['numrows']>0)
    {
        foreach($dbJobs['data'] as $job)
        {
            $jobid=$job['id'];
            printJob2Inserter($jobid,0,$debug);
            printJob2Delivery($jobid);
            printJob2Bindery($jobid);
            printJob2Addressing($jobid);
        }
    }
    
}

  
function saveRecLayout($layoutid,$jobid,$siteID)
{
    
    //get some pub info from the job
    $sql="SELECT A.pub_date, B.pub_code, A.pub_id FROM jobs A, publications B WHERE B.id=A.pub_id AND A.id=$jobid";
    $dbPubInfo=dbselectsingle($sql);
    $pubinfo=$dbPubInfo['data'];
    $pubcode=$pubinfo['pub_code'];
    $pubdate=date("Y-md",strtotime($pubinfo['pub_date']));
    $pubid=$pubinfo['pub_id'];
    
    
    //now, lets create the plates for this job
    //we'll need to get the pub code for the publication
    //also, pub date and section codes for all sections
    $jobsql="SELECT A.*, B.pub_code FROM jobs A, publications B WHERE A.id=$jobid AND A.pub_id=B.id";
    //print "Job select sql:<br>$jobsql<br>";
    $dbJob=dbselectsingle($jobsql);
    $job=$dbJob['data'];


    $jobsection="SELECT * FROM jobs_sections WHERE job_id=$jobid";
    //print "Job section sql:<br>$jobsection<br>";
    $dbJSection=dbselectsingle($jobsection);
    $jsection=$dbJSection['data'];
    $scode[1]=$jsection['section1_code'];
    $scode[2]=$jsection['section2_code'];
    $scode[3]=$jsection['section3_code'];
    
    //now get layout sections
    $lsql="SELECT * FROM layout_sections WHERE layout_id=$layoutid";
    //print "Job layout sections sql:<br>$lsql<br>";
    $dbLSections=dbselectmulti($lsql);


    //first, delete any potential existing job plates and pages
    $sql="DELETE FROM job_pages WHERE job_id=$jobid";
    $dbDelete=dbexecutequery($sql);
    $sql="DELETE FROM job_plates WHERE job_id=$jobid";
    $dbDelete=dbexecutequery($sql);
    $colorconfigs=$GLOBALS['colorconfigs'];
    if ($dbLSections['numrows']>0)
    {
        foreach ($dbLSections['data'] as $lsection)
        {
            
            $section_number=$lsection['section_number'];
            $towers=$lsection['towers'];
            $towers=explode("|",$towers);
            foreach ($towers as $tower)
            {
                $created=date("Y-m-d H:i:s");
                //lets look up the color for a tower
                $sql="SELECT color_config FROM press_towers WHERE id=$tower";
                $dbColor=dbselectsingle($sql);
                if ($dbColor['numrows']>0)
                {
                    if ($dbColor['data']['color_config']=='K')
                    {
                        $color=0;
                        $possiblecolor=0;
                    }else{
                        $color=1;
                        $possiblecolor=1;
                    }
                    $tcolor=array_search($dbColor['data']['color_config'],$colorconfigs,true);
                } else {
                    $color=0;
                    $possiblecolor=0;
                    $tcolor=0;
                }
                
                $plate1="";
                $plate2="";
                $pages1=array();
                $pages2=array();
                $lowpage1=9999; //set arbitrarily high so it gets set immediately to the new page
                $lowpage2=9999; //set arbitrarily high so it gets set immediately to the new page
                //now we need the pages for this layout & tower -- 10 side, then 13 side
                $psql="SELECT * FROM layout_page_config WHERE layout_id=$layoutid AND tower_id=$tower";
                $dbPages=dbselectmulti($psql);
                if ($dbPages['numrows']>0)
                {
                    foreach ($dbPages['data'] as $page)
                    {
                        $side=$page['side'];
                        $page_num=$page['page_number'];
                        if ($page_num!=0)
                        {
                            if ($side==10)
                            {
                                if ($page_num<$lowpage1 && $page_num!=0){$lowpage1=$page_num;}
                                $pages1[]="$pubid, $jobid,'$scode[$section_number]','$pubcode','$pubdate',$color,$possiblecolor,$tower, $tcolor,$page_num, 1,'$created', '$siteID'),";
                            } else {
                                if ($page_num<$lowpage2 && $page_num!=0){$lowpage2=$page_num;}
                                $pages2[]="$pubid, $jobid,'$scode[$section_number]','$pubcode','$pubdate',$color,$possiblecolor,$tower, $tcolor, $page_num, 1,'$created', '$siteID'),";
                            }
                        }            
                    
                    }
                    //now we should have 2 items, 2 arrays with pages and a low page number for each plate
                    $plate1="INSERT INTO job_plates (pub_id, job_id, section_code, pub_code, pub_date, low_page, color, version, created, site_id) VALUES
                    ($pubid,$jobid, '$scode[$section_number]','$pubcode', '$pubdate','$lowpage1',$color,1,'$created', '$siteID')";
                    $dbPlate1=dbinsertquery($plate1);                                             
                    //print "Plate save 1 sql:<br>$plate1<br>";

                    $plate1ID=$dbPlate1['numrows'];
                    $plate2="INSERT INTO job_plates (pub_id, job_id, section_code, pub_code, pub_date, low_page, color, version, created, site_id) VALUES
                    ($pubid,$jobid, '$scode[$section_number]','$pubcode', '$pubdate','$lowpage2',$color,1,'$created', '$siteID')";
                    $dbPlate2=dbinsertquery($plate2);
                    $plate2ID=$dbPlate2['numrows'];
                    //print "Plate save 2 sql:<br>$plate2<br>";

                    //now insert the pages
                    $values1="";
                    foreach($pages1 as $page)
                    {
                        $values1.="($plate1ID,$page";    
                    }
                    $values1=substr($values1,0,strlen($values1)-1);
                    $page1="INSERT INTO job_pages (plate_id, pub_id, job_id, section_code, pub_code, pub_date, color, possiblecolor, tower_id, tower_color, page_number, version, created, site_id) VALUES $values1";
                    $dbPage1=dbinsertquery($page1);
                    //print "Page save 1 sql:<br>$page1<br>";

                    //now insert the pages
                    $values2="";
                    foreach($pages2 as $page)
                    {
                        $values2.="($plate2ID,$page";    
                    }
                    $values2=substr($values2,0,strlen($values2)-1);
                    $page2="INSERT INTO job_pages (plate_id, pub_id, job_id, section_code, pub_code, pub_date, color, possiblecolor, tower_id, tower_color, page_number, version, created, site_id) VALUES $values2";
                    $dbPage2=dbinsertquery($page2);
                    //print "Page save 2 sql:<br>$page2<br>";

                }
            }
         }
    }
  }
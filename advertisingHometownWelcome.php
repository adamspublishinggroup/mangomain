<?php
include("includes/boot.php") ;
if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}
    
switch ($action)
{
    case "list":
    list_batches();
    break;
    
    case "import":
    import();
    break;
    
    case "geocode":
    geocodeBusinesses();
    break;
    
    case "checkgeocode":
    fetchGeocode();
    break;
    
    case "Process Import":
    process_import();
    break;
    
    default:
    list_batches();
    break;
    
} 


   
function import()
{
    print "<form method='post' enctype='multipart/form-data'>\n";
    make_text('batch',date("Y-m-d"),'Batch name','Enter a batch name for this file. It will be available for you to select later to view this data on a map.');
    make_file('addresses','Hometown Welcome File');
    make_submit('submit','Process Import');
    print "</form>\n";   
}   

function process_import()
{
    $batch=addslashes($_POST['batch']);
    print "Processing...<br>";
    set_time_limit(6000);
    if(isset($_FILES))
    {
        print "Ingesting file...<br>";
        $file=$_FILES['addresses']['tmp_name'];
        $contents=file_get_contents($file);
        $inserted=0;
        $updated=0;
        $lines=explode("\n",$contents);
        $i=0;
        $success=0;
        $accounts=array();
        if(strpos($lines[0],"Last Name")>0)
        {
            array_shift($lines);
        }
        $addednew=false; 
        print "A total of ".count($lines)." records are in the file<br>"; 
        $totalsuccess=0; 
        foreach($lines as $line)
        {
            $line=trim($line);
            $line=convertCSVtoArray($line);
            $dbInsert=array();
            if(trim($line[0])!='')
            {
                $accounts[$i]['first_name']=addslashes(trim($line[0]));    
                $accounts[$i]['last_name']=addslashes(trim($line[2]));    
                $accounts[$i]['address']=addslashes(trim($line[4]));    
                $accounts[$i]['city']=addslashes(trim($line[5]));    
                $accounts[$i]['state']=addslashes(trim($line[6]));    
                $accounts[$i]['zip']=addslashes(trim($line[7]));    
                $i++;
            }
            if($i>500)
            {
                $values='';
                foreach($accounts as $account)
                {
                    $values.="('$batch', '$account[first_name]','$account[last_name]','$account[address]','$account[city]','$account[state]','$account[zip]'),";
                    $addednew=true;
                }
                $values=substr($values,0,strlen($values)-1);
                $sql="INSERT INTO hometown_welcome (batch, first_name, last_name, address, city, state, zip) VALUES $values";
                $dbInsert=dbinsertquery($sql);
                if($dbInsert['error']!='')
                {
                    print "There was an error processing the database import batch:<br>".$dbInsert['error'];
                } else {
                    print "<br><br>";
                    print "Total of $i accounts were processed for this batch.<br>.";
                }
                $totalsuccess+=$i;
                $accounts=array();
                $i=0; 
            }
        }
                      
         
        if(count($accounts)>0)
        {
            $values='';
            foreach($accounts as $account)
            {
                $values.="('$batch', '$account[first_name]','$account[last_name]','$account[address]','$account[city]','$account[state]','$account[zip]'),";
                $addnew=true;
            }
            $values=substr($values,0,strlen($values)-1);
            if($values!='')
            {
                $sql="INSERT INTO hometown_welcome (batch, first_name, last_name, address, city, state, zip) VALUES $values";
                $dbInsert=dbinsertquery($sql);
                if($dbInsert['error']!='')
                {
                    print "There was an error processing the database import:<br>".$dbInsert['error'];
                } else {
                    $totalsuccess+=count($accounts);
                }
            }
        }
        print "Overall, $totalsuccess records were successfully inserted with $updated records being updated.<br>";
        if($addednew)
        {
            print "Since there were new accounts added, you will need to do batch geocode.<br>";
        }
    } else {
        print "No file was uploaded.";
    }
    print "<br><br><a href='?action=list' class='submit'>Return to menu</a>";
    print "<br><br><a href='?action=geocode' class='submit'>Batch Geocode</a>";
    
    /*
    ob_implicit_flush(true);
    print "Begining file processing...imported: <span id='importcount'></span><br>";
    print "<script>\$('#importcount').html('".($i-1)."')</script>";
    $i++;
        //only update every 25
        if($l==25)
        {
            $l=0;
            print "<script>\$('#importcount').html('$i')</script>";
            for($k = 0; $k < 320000; $k++){echo ' ';} // extra spaces to fill up browser buffer
        } else {
            $l++;
        }
        for($k = 0; $k < 320000; $k++)echo ' '; // extra spaces to fill up browser buffer
     $addresses=batch_geocode($addresses,'0','0',true,'curadd');
            print "<br><br>Integrating geocode information into the accounts...<br>";
            foreach($accounts as $key=>$account)
            {
                $accounts[$key]['lat']=$addresses[$key]['lat'];        
                $accounts[$key]['lon']=$addresses[$key]['lon'];
                if($addresses[$key]['status']=='success'){$success++;}
            }  
    */ 
} 


function list_batches()
{
    $sql="SELECT DISTINCT(batch) FROM hometown_welcome ORDER BY batch";
    $dbBatches=dbselectmulti($sql);
    
    $sql="SELECT * FROM batch_geocodes WHERE status=0";
    $dbOpenBatches=dbselectmulti($sql);
    $openbatches='';
    if($dbOpenBatches['numrows']>0)
    {
        foreach($dbOpenBatches['data'] as $ob)
        {
            $openbatches.="<a href='?action=checkgeocode&id=$ob[id]'>Check status of GeoCode batch #$ob[id]</a>,";
        }
    }
    tableStart("<a href='?action=import'>Import HomeTown Welcome Batch</a>,
    <a href='?action=geocode'>Geocode all listings</a>,$openbatches 
    <a href='advertisingHometownWelcomeMapping.php'>View Batch Map</a>,
    ","Batch Name",4);
    if ($dbBatches['numrows']>0)
    {
        foreach($dbBatches['data'] as $type)
        {
            $id=$type['id'];
            $batch=stripslashes($type['batch']);
            print "<tr><td>$batch</td>";
            print "<td><a href='?action=edit&id=$id'>Edit</a></td>\n";
            print "<td><a href='advertisingHometownWelcomeMapping.php?batch=$batch'>View on map</a></td>\n";
            print "<td><a href='?action=delete&id=$id' class='delete'>Delete</a></td>\n";
            print "</tr>\n";
        }
    }
    tableEnd($dbBatches);
        
    
}

function geocodeBusinesses()
{
    error_reporting(E_ERROR);
    //windows bing map api key    AsUEJjHX1oAE16UX-8mQFmQaE8I4CFXizSHRg6s6vJ35MB6Dm-cVuirpT-XUn3-B
    print "Beginning geocode process<br>";
    //now batch geocode them all
    $key = "AsUEJjHX1oAE16UX-8mQFmQaE8I4CFXizSHRg6s6vJ35MB6Dm-cVuirpT-XUn3-B";
    $url = 'http://spatial.virtualearth.net/REST/v1/Dataflows/Geocode?description=MangoAccounts&input=xml&output=json&key=' . $key;
    // STEP 1 - Create a geocode job
    if($_GET['limit'])
    {
        $limit=intval($_GET['limit']);
    } else {
        $limit=50;
    }
    $sql="SELECT id, address, city, state, zip FROM hometown_welcome WHERE lat IS Null AND geocoding=0 LIMIT $limit";
    $dbAddresses=dbselectmulti($sql);
    print "Batch processing $limit accounts - ".$dbAddresses['numrows']." actually selected<br>";
    $data="<GeocodeFeed>\n";
    

    $updateids='';
    foreach($dbAddresses['data'] as $ad)
    {
        $data.='<GeocodeEntity Id="'.$ad['id'].'" xmlns="http://schemas.microsoft.com/search/local/2010/5/geocode">'."\n";
        $data.="<GeocodeRequest Culture='en-US'>\n";
        $data.='<Address AddressLine="'.str_replace("&","&amp;",stripslashes($ad['address'])).'" AdminDistrict="'.stripslashes($ad['state']).'" Locality="'.stripslashes($ad['city']).'" PostalCode="'.stripslashes($ad['zip']).'" />'."\n";
        $data.="</GeocodeRequest>\n";
        $data.="</GeocodeEntity>\n";
        $updateids.="$ad[id],";
        // SAMPLE:  1|en-US||One Microsoft Way|WA||||Redmond|98052
    }
    
    $data.="</GeocodeFeed>\n";
    // Call custom function to generate an HTTP request and get back an HTTP response
    // This function constructs and sends an HTTP request with a provided URL and data, and returns an HTTP response object 
    // This function uses the php_http extension 
   // Call custom function to generate an HTTP request and get back an HTTP response
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $response = curl_exec($ch);
    $response=json_decode($response,true);
    print "<pre>\n";
    print_r($response);
    print "</pre>\n";
    $jobId=$response['resourceSets'][0]['resources'][0]['id'];
    $jobStatus=$response['resourceSets'][0]['resources'][0]['status'];
    $statusDescription=$response['statusDescription'];
     
    echo "Job Created:<br>";
    echo " Request Status: ".$statusDescription."<br>";
    echo " Job ID: ".$jobId."<br>";
    echo " Job Status: ".$jobStatus."<br><br>";

    if($statusDescription=='Bad Request')
    {
        print "<pre>\n";
        print_r($response);
        print $data;
        print "</pre>\n";
            
    } elseif($jobId!='') {
        $updateids=substr($updateids,0,strlen($updateids)-1);
        if($updateids!='')
        {
            $sql="UPDATE hometown_welcome SET geocoding=1 WHERE id IN ($updateids)";
            $dbUpdate=dbexecutequery($sql);
            if($dbUpdate['error']!=''){print $dbUpdate['error'];} 
        }
        
        // STEP 2 - Get the status of geocode job(s)
        $sql="INSERT INTO batch_geocodes (job_id, status) VALUES ('$jobId',0)";
        $dbInsert=dbinsertquery($sql);
        $id=$dbInsert['insertid'];
        print "<br><a href='?action=checkgeocode&id=$id'>Check the status of this batch</a><br>";
        
    } else {
        print "Something unknown went wrong in the batch geocoding process. The status was $statusDescription";
    }
    print "<br><br><a href='?action=list'>Return to batch list</a>";
}

function fetchGeocode()
{
    $key = "AsUEJjHX1oAE16UX-8mQFmQaE8I4CFXizSHRg6s6vJ35MB6Dm-cVuirpT-XUn3-B";
    $id=intval($_GET['id']);
        
    $sql="SELECT * FROM batch_geocodes WHERE id=$id AND status=0";
    $dbJob=dbselectsingle($sql);
    if($dbJob['numrows']>0)
    {
        $jobId=$dbJob['data']['job_id'];
        // Call the API to determine the status of all geocode jobs associated with a Bing Maps key
        echo "Checking status...<br>";
        $checkUrl = "http://spatial.virtualearth.net/REST/v1/Dataflows/Geocode/".$jobId."?output=json&key=".$key;
        
        // Construct the URL to check the job status, including the jobId

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $checkUrl);
        $checkResponse = curl_exec($ch);
        curl_close($ch);
        $checkResponse=json_decode($checkResponse,true);
        /*
        print "<pre>";
            print_r($checkResponse);
        print "</pre>\n";
        */        
        $successful=$checkResponse['resourceSets'][0]['resources'][0]['processedEntityCount'];
        $failures=$checkResponse['resourceSets'][0]['resources'][0]['failedEntityCount'];
        $created=$checkResponse['resourceSets'][0]['resources'][0]['createdDate'];
        $jobStatus=$checkResponse['resourceSets'][0]['resources'][0]['status'];
        $Links = $checkResponse['resourceSets'][0]['resources'][0]['links'];
        foreach ($Links as $Link) {
            if ($Link['name'] == "succeeded") 
            { 
              $successUrl = $Link['url']; 
            }
        }
        echo "created: $created<br>Status:$jobStatus<br>
        <br>Successful Geocodes:$successful<br>
        <br>Failed Geocodes:$failures
        <br>Success url: $successUrl<br><br>";
        
        if($jobStatus=='Completed')
        {  
            
            // STEP 3 - Obtain results from a successfully geocoded set of data
            
            // Access the URL for the successful requests, and convert response to an XML element
            $successUrl .= "?output=xml&key=".$key;
            print "Checking with url: $successUrl<br>";
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_URL, $successUrl);
            $successReponse = curl_exec($ch);
            if(curl_exec($ch) === false)
            {
                echo 'Curl error: ' . curl_error($ch);
            }
            else
            {
                echo 'Operation completed without any errors.<br>';
                $array = json_decode(json_encode((array)simplexml_load_string($successReponse)),1);
                /*
                print "<pre>";
                print_r($array);
                print "</pre>\n";
                */
                $geoCodedLocations=$array['GeocodeEntity'];
                
                
                foreach($geoCodedLocations as $geolocation)
                {
                    $idAttr=0;
                    $locationLat='';
                    $locationLon='';
                    /*
                    print "<pre>";
                    print_r($geolocation);
                    print "</pre>\n";
                    */
                    $idAttr=$geolocation['@attributes']['Id'];
                    $response=$geolocation['GeocodeResponse'];
                    if(isset($response['RooftopLocation']))
                    {
                        $location=$response['RooftopLocation'];
                        $locationLat=$location['@attributes']['Latitude'];
                        $locationLon=$location['@attributes']['Longitude'];
                    } else {
                        $location=$response['InterpolatedLocation'];
                        $locationLat=$location['@attributes']['Latitude'];
                        $locationLon=$location['@attributes']['Longitude'];
                    }
                    if($locationLon!='' && $idAttr!=0)
                    {
                        //print "For $idAttr we got $locationLat, $locationLon<br>";
                        $sql="UPDATE hometown_welcome SET lat='$locationLat', lng='$locationLon', geocoding=0 WHERE id=$idAttr";
                        $dbUpdate=dbexecutequery($sql);
                        if($dbUpdate['error']=='')
                        {
                            $success++;
                        }
                    } 
                }
                $sql="UPDATE batch_geocodes SET status=1 WHERE id=$id";
                $dbUpdate=dbexecutequery($sql); 
            }
            curl_close($ch);
            print "A total of $success updates were successfully made to the database.";
        } else {
            print "<br><a href='?action=checkgeocode&id=$id'>Check the status of this batch again</a>";
       
        }
        
    } else {
        print "That batch job is no longer available<br>";
    }
    print "<br><br><a href='?action=list'>Return to account list</a>";
}


$Page->footer();?>
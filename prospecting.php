<?php
include("includes/boot.php");
global $User;
//set up some arrays
$sql="SELECT * FROM prospect_status ORDER BY status DESC";
$dbStatuses=dbselectmulti($sql);
foreach($dbStatuses['data'] as $s)
{
    $statuses[$s['id']]=$s['status'];
}


$campaignid=intval($_GET['id']);
$prospectid=intval($_GET['prospectid']);


$sql="SELECT * FROM prospect_campaigns WHERE id=$campaignid";
$dbCampaign=dbselectsingle($sql);
$campaign=$dbCampaign['data'];

print "<div class='page-header' style='margin:0;border: none;'>
  <h1 style='margin:0'>Campaign: <small>".stripslashes($campaign['campaign_name'])."</small> <span class='pull-right'><button type='button' onclick='nextProspect();' class='btn btn-primary btn-sm'>Go to next prospect</button></span></h1>
</div>";


print "<div class='row'>";

print "<div class='col-xs-12 col-md-4'>
    <div class='panel panel-primary'>
        <div class='panel-heading'>
            <h3 class='panel-title'>The pitch</h3>
        </div>
        <div class='panel-body'>\n";
          print stripslashes($campaign['notes']);
    print "
        </div>
</div>\n";

print "</div>\n";


//if $prospectid is 0, then pick a random prospect that is assigned to this user, or unassigned

if($prospectid==0)
{
    $sql="SELECT A.*, B.user_id 
    FROM prospects A, prospect_campaign_xref B, prospect_touches C 
    WHERE B.campaign_id=$campaignid 
    AND A.id=B.prospect_id 
    AND (B.user_id=0 OR B.user_id=".$User->id.") 
    AND A.id = C.prospect_id 
    AND C.status=1 
    ORDER BY RAND() LIMIT 1";
} else {
    $sql="SELECT A.*, B.user_id 
    FROM prospects A, prospect_campaign_xref B 
    WHERE A.id=$prospectid 
    AND A.id=B.prospect_id";
    
}
$dbProspect=dbselectsingle($sql);
$prospect=$dbProspect['data'];

//create a new "opening touch" for this prospect, plus if it's not assigned, assign it to this user
$dt=date("Y-m-d H:i:s");

if($prospect['user_id']==0)
{
    $sql="UPDATE prospect_campaign_xref SET user_id=$userid WHERE campaign_id=$campaignid AND prospect_id=$prospect[id]";
    $dbUpdate=dbexecutequery($sql);
    if($dbUpdate['error']!=''){print $dbUpdate['error'];}
    $sql="INSERT INTO prospect_touches (campaign_id, prospect_id, user_id, action_datetime, status, notes) 
VALUES ('$campaignid', '$prospectid', '$userid', '$dt', 9, 'Opened for user action')";
    $dbUpdate=dbinsertquery($sql);
    if($dbUpdate['error']!=''){print $dbUpdate['error'];}
    
}
$sql="INSERT INTO prospect_touches (campaign_id, prospect_id, user_id, action_datetime, status, notes) 
VALUES ('$campaignid', '$prospectid', '$userid', '$dt', 2, 'Assigned to user ".$_SESSION['firstname']."')";
$dbUpdate=dbinsertquery($sql);
if($dbUpdate['error']!=''){print $dbUpdate['error'];}

//grab all touches
$sql="SELECT * FROM prospect_touches WHERE prospect_id=$prospect[id] ORDER BY action_datetime";
$dbTouches=dbselectmulti($sql);
if($dbTouches['error']!=''){print $dbTouches['error'];}


print "<div class='col-xs-12 col-md-8'>
    <div class='panel panel-primary'>
        <div class='panel-heading clearfix'>
            <h3 class='panel-title'>Prospect <span class='pull-right'><button type='button' onclick='addProspectContact();' class='btn btn-dark btn-xs'>Add Contact</button></span></h3>
        </div>
        <div id='actions' class='panel-body'>\n";
        print "<div class='col-xs-5'>\n";
        print "<b>Business:</b> ".stripslashes($prospect['account_name'])."<br>";
        print "<b>Address:</b> ".stripslashes($prospect['address1'])."<br>";
        print "<b>City:</b> ".stripslashes($prospect['city'])."<br>";
        print "<b>Main Phone:</b> ".stripslashes($prospect['phone'])."<br>";
        print "<b>Website:</b> ".($prospect['website']!='' ? "<a href='".(strpos($prospect['website'],"ttp:")>0 ? '' : 'http://').stripslashes($prospect['website'])."' target='_blank'>".stripslashes($prospect['website']) ."</a>" : ' NA')."<br>";
        print "<b>Employees:</b> ".stripslashes($prospect['employees'])."<br>";
        print "<b>Female Owned:</b> ".($prospect['female_owned'] == 1 ? "Yes" : "No");
        print "</div>\n";
        print "<div class='col-xs-7'>\n";
        print "<table class='table table-bordered table-condensed'>\n";
        print "<tr><th>Name</th><th>Phone</th><th>Email</th></tr>\n";
        
        $sql="SELECT * FROM prospect_contacts WHERE prospect_id=$prospect[id]";
        $dbContacts=dbselectmulti($sql);
        if($dbContacts['numrows']>0)
        {
            foreach($dbContacts['data'] as $contact)
            {
                print "<tr><td>".stripslashes($contact['first_name'].' '.$contact['last_name'])."</td>";
                print "<td>".$contact['phone']."</td>";
                print "<td>".$contact['email']."</td>";
                print "</tr>\n";
            }
        }
        print "</table>\n";
        print "</div>\n";
    print "
        </div>
</div>\n";

print "</div>\n";

print "</div><!-- closing top row -->\n";

print "<div class='row'>\n";
    print "<div class='col-xs-12 col-md-4'>
    <div class='panel panel-primary'>
        <div class='panel-heading'>
            <h3 class='panel-title'>Actions</h3>
        </div>
        <div id='actions' class='panel-body' style='padding:10px 8px;'>\n";
        print "<div class='col-xs-6' style='padding:2px;'>\n";
            print "<button class='btn btn-block btn-dark' type='button'>Call Back</button>";
            print "<button class='btn btn-block btn-dark' type='button'>Call Back Later</button>";
            print "<button class='btn btn-block btn-dark' type='button'>Out of Business</button>";
            print "<button class='btn btn-block btn-dark' type='button'>No Answer</button>";
        print "</div>\n";
        
        print "<div class='col-xs-6' style='padding:2px;'>\n";
            print "<button class='btn btn-block btn-dark' type='button'>Do Not Contact</button>";
            print "<button class='btn btn-block btn-dark' type='button'>Schedule Appointment</button>";
            print "<button class='btn btn-block btn-dark' type='button'>Send Information</button>";
            print "<button class='btn btn-block btn-dark' type='button'>Other Action</button>";
        print "</div>\n";
        
    print "
        </div>
        </div>
</div>\n";
    
    print "<div class='col-xs-12 col-md-8'>
    <div class='panel panel-primary'>
        <div class='panel-heading '>
        <h3 class='panel-title clearfix'>Notes<span class='pull-right'><button type='button' onclick='addProspectNote();' class='btn btn-dark btn-xs'>Add Note</button></span></h3>
      </div>
      \n";
        
        print "<div id='notes' class='panel-body' style='height: 600px; overflow-y: scroll'>\n";
        if($dbTouches['numrows']>0)
        {
            foreach($dbTouches['data'] as $touch)
            {
                print "<div style='float:left;width:100px;'>\n";
                print $statuses[$touch['status']];
                print "</div>\n"; 
                print "<div style='float:left;width:100px;'>\n";
                print date("m/d/Y H:i",strtotime($touch['action_datetime']));
                print "</div>\n"; 
                print "<div style='float:left;width:340px;'>\n";
                print stripslashes($touch['notes']);
                print "</div>\n<div class='clear'></div>\n";   
            }
        }
        print "</div>
        </div>\n";

    print "</div>\n";

print "</div><!-- closing main row -->\n";


$Page->footer();
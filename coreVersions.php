<?php
//<!--VERSION: .9 **||**-->
include("includes/boot.php") ;


if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}
switch ($action)
{
    case "Save Version":
    version_save();
    break;
    
    case "edit":
    version_edit();
    break;
    
    case "complete":
    version_complete();
    break;
    
    case "delete":
    version_delete();
    break;
    
    case "up":
    move_up();
    break;
    
    case "down":
    move_down();
    break;
        
    case "betaup":
    move_beta_up();
    break;
    
    case "betadown":
    move_beta_down();
    break;
        
    case "requests":
    view_requests();
    break;
        
    case "Add Request":
    add_request();
    break;
        
    default:
    versions_list();
    break;
}

function view_requests()
{
    global $users;
    
    //get all versions
    $sql="SELECT id, version_alpha, version_beta, version_gamma, version_title FROM core_version WHERE version_status=0 ORDER BY version_alpha ASC, version_beta ASC, version_gamma ASC";
    $dbVersions=dbselectmulti($sql);
    $versions[0]="Select version to add to";
    if($dbVersions['numrows']>0)
    {
        foreach($dbVersions['data'] as $ver)
        {
           $versions[$ver['id']]=$ver['version_alpha'].'.'.$ver['version_beta'].'.'.$ver['version_gamma'].' -- '.stripslashes($ver['version_title']);
        }
    }
    
    
    $sql="SELECT * FROM core_version_request WHERE incorporated_version=0 ORDER BY submitted_datetime ASC";
    $dbRequests=dbselectmulti($sql);
    if($dbRequests['numrows']>0)
    {
        print "<div class='row'><div class='col-xs-12'><a href='?action=list' class='btn btn-primary pull-right'>Return to version list</a></div></div>\n";
        print "<table class='table table-striped table-bordered'>\n";
        print "<tr><th>Requested By</th><th>Time</th><th>Request</th></tr>\n";
        foreach($dbRequests['data'] as $request)
        {
            print "<tr><td>".$users[$request['submitted_by']]."</td><td>".$request['submitted_datetime']."</td>";
            print "<td><form method=post class='form-horizontal'>\n";
            print stripslashes($request['request'])."<hr>";
            print "<input type='hidden' name='request_id' value='$request[id]' />";
            make_select('version_id',$versions[0],$versions,'Add to');
            make_submit('submit','Add Request');
            print "</form>\n";
        }
        print "</table>\n";
    } else {
        print "<div class='alert alert-info' role='alert'>There are currently no pending requests. <a href='?action=list' class='btn btn-primary'>Return to version list</a></div>";
    }
}

function add_request()
{
    $request_id=intval($_POST['request_id']);
    $version_id=intval($_POST['version_id']);
    
    $sql="UPDATE core_version_request SET incorporated_version='$version_id'";
    $dbUpdate=dbexecutequery($sql);
    $sql="SELECT request FROM core_version_request WHERE id=$request_id";
    $dbRequest=dbselectsingle($sql);
    $request="<p>".$dbRequest['data']['request']."</p>";
    
    $sql="UPDATE core_version SET version_details = CONCAT (version_details,'$request') WHERE id=$version_id";
    $dbUpdate=dbexecutequery($sql);
    
    //redirect("?action=list");
}
function move_beta_up()
{
    /* this needs to change the beta on all items, plus fix the version numbers */
    $alpha = addslashes($_GET['alpha']);
    $beta = addslashes($_GET['beta']);
    
    $newBeta = $beta - 1;
    
    //get array of both existing sets, that way we can them without have to worry that we're pulling changed things already
    $sql="SELECT * FROM core_version WHERE version_alpha = $alpha AND version_beta = $beta";
    //print $sql.'<br>';
    $dbSetOne = dbselectmulti($sql);
    
    //get the other set, the one that is being swapped with
    $sql="SELECT * FROM core_version WHERE version_alpha = $alpha AND version_beta = $newBeta";
    //print $sql.'<br>';
    $dbSetTwo = dbselectmulti($sql);
    
    //now loop through setOne, update version_beta and version_number
    if($dbSetOne['numrows']>0)
    {
        foreach($dbSetOne['data'] as $item)
        {
            $versionNumber=$item['version_alpha'].$newBeta.$item['version_gamma'];
            $sql="UPDATE core_version SET version_beta = '$newBeta', version_number='$versionNumber' WHERE id=$item[id]";
   // print 'Updating set one '.$sql.'<br>';
            $dbUpdate=dbexecutequery($sql); 
        }
    }
    
    //now update the other set
    if($dbSetTwo['numrows']>0)
    {
        foreach($dbSetTwo['data'] as $item)
        {
            $versionNumber=$item['version_alpha'].$beta.$item['version_gamma'];
            $sql="UPDATE core_version SET version_beta = '$beta', version_number='$versionNumber' WHERE id=$item[id]";
   // print 'Updating set two '.$sql.'<br>';
            $dbUpdate=dbexecutequery($sql); 
        }
    }
    redirect("?action=list");
}

function move_beta_down()
{
    /* this needs to change the beta on all items, plus fix the version numbers */
    $alpha = addslashes($_GET['alpha']);
    $beta = addslashes($_GET['beta']);
    
    $newBeta = $beta + 1;
    
    //get array of both existing sets, that way we can them without have to worry that we're pulling changed things already
    $sql="SELECT * FROM core_version WHERE version_alpha = $alpha AND version_beta = $beta";
    //print $sql.'<br>';
    $dbSetOne = dbselectmulti($sql);
    
    //get the other set, the one that is being swapped with
    $sql="SELECT * FROM core_version WHERE version_alpha = $alpha AND version_beta = $newBeta";
    //print $sql.'<br>';
    $dbSetTwo = dbselectmulti($sql);
    
    //now loop through setOne, update version_beta and version_number
    if($dbSetOne['numrows']>0)
    {
        foreach($dbSetOne['data'] as $item)
        {
            $versionNumber=$item['version_alpha'].$newBeta.$item['version_gamma'];
            $sql="UPDATE core_version SET version_beta = '$newBeta', version_number='$versionNumber' WHERE id=$item[id]";
   // print 'Updating set one '.$sql.'<br>';
            $dbUpdate=dbexecutequery($sql); 
        }
    }
    
    //now update the other set
    if($dbSetTwo['numrows']>0)
    {
        foreach($dbSetTwo['data'] as $item)
        {
            $versionNumber=$item['version_alpha'].$beta.$item['version_gamma'];
            $sql="UPDATE core_version SET version_beta = '$beta', version_number='$versionNumber' WHERE id=$item[id]";
   // print 'Updating set two '.$sql.'<br>';
            $dbUpdate=dbexecutequery($sql); 
        }
    }
    redirect("?action=list");
}


function move_up()
{
    $currentID = intval($_GET['id']);
    
    $sql="SELECT * FROM core_version WHERE id=$currentID";
    $dbCurrent = dbselectsingle($sql);
    
    $current = $dbCurrent['data'];
    
    $cAlpha = $current['version_alpha'];
    $cBeta = $current['version_beta'];
    $cGamma = $current['version_gamma'];
    $cVersion = $current['version_number'];
    
    $newAlpha = $cAlpha;
    $newBeta = $cBeta;
    $newGamma = $cGamma - 1;
    
    $sql="SELECT * FROM core_version WHERE version_alpha = '$newAlpha' AND version_beta = '$newBeta' AND version_gamma = '$newGamma'";
    $dbNew = dbselectsingle($sql);
    $new = $dbNew['data'];
    $newVersion = $new['version_number'];
    $newID = $new['id'];
    
    //do the swap
    $sql="UPDATE core_version SET version_gamma = '$newGamma', version_number='$newVersion' WHERE id = $currentID";
    $dbUpdate = dbexecutequery($sql);
    
    $sql="UPDATE core_version SET version_gamma = '$cGamma', version_number='$cVersion' WHERE id = $newID";
    $dbUpdate = dbexecutequery($sql);
    redirect("?action=list");
}

function move_down()
{
    $currentID = intval($_GET['id']);
    
    $sql="SELECT * FROM core_version WHERE id=$currentID";
    $dbCurrent = dbselectsingle($sql);
    
    $current = $dbCurrent['data'];
    
    $cAlpha = $current['version_alpha'];
    $cBeta = $current['version_beta'];
    $cGamma = $current['version_gamma'];
    $cVersion = $current['version_number'];
    
    $newAlpha = $cAlpha;
    $newBeta = $cBeta;
    $newGamma = $cGamma + 1;
    
    $sql="SELECT * FROM core_version WHERE version_alpha = '$newAlpha' AND version_beta = '$newBeta' AND version_gamma = '$newGamma'";
    $dbNew = dbselectsingle($sql);
    $new = $dbNew['data'];
    $newVersion = $new['version_number'];
    $newID = $new['id'];
    
    //do the swap
    $sql="UPDATE core_version SET version_gamma = '$newGamma', version_number='$newVersion' WHERE id = $currentID";
    $dbUpdate = dbexecutequery($sql);
    
    $sql="UPDATE core_version SET version_gamma = '$cGamma', version_number='$cVersion' WHERE id = $newID";
    $dbUpdate = dbexecutequery($sql);
    redirect("?action=list");
}

function version_delete()
{
    $id=intval($_GET['id']);
    
    $sql="SELECT * FROM core_version WHERE id=$id";
    $dbVersion=dbselectsingle($sql);
    
    $sql="DELETE FROM core_version WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    
    $alpha = $dbVersion['data']['version_alpha'];
    $beta = $dbVersion['data']['version_beta'];
    $gamma = $dbVersion['data']['version_gamma'];
    
    $sql="SELECT * FROM core_version WHERE version_alpha = '$alpha' AND version_beta='$beta' AND version_gamma>$gamma ORDER BY version_gamma ASC";
    $dbChange=dbselectmulti($sql);
    if($dbChange['numrows']>0)
    {
        foreach($dbChange['data'] as $v)
        {
            $newGamma = $v['version_gamma']-1;
            $newNumber = $alpha.$beta.$newGamma;
            $sql="UPDATE core_version SET version_gamma = '$newGamma', version_number='$newNumber' WHERE id=$v[id]";
            $dbUpdate=dbexecutequery($sql);
        }
    }
    redirect("?action=list");
}

function version_complete()
{
    $id=intval($_GET['id']);
    $date = date("Y-m-d");
    $sql="UPDATE core_version SET complete_date='$date', version_status=1 WHERE id=$id";
    $dbUpdate=dbexecutequery($sql);
    
    redirect("?action=list");
}

function version_edit()
{
    print "<div class='row'>\n";
        print "<div class='col-xs-12 col-sm-8'>\n";
        
        if($_GET['id'])
        {
            $id=intval($_GET['id']);
            $sql="SELECT * FROM core_version WHERE id=$id";
            $dbVersion = dbselectsingle($sql);
            $version = $dbVersion['data'];
            $date = stripslashes($version['version_target']);
            $status = $version['version_status'];
            $title = stripslashes($version['version_title']);
            $details = stripslashes($version['version_details']);
            $alpha = $version['version_alpha'];
            $beta = $version['version_beta'];
            $gamma = $version['version_gamma'];
        } else {
            $date = date("Y-m-d",strtotime("+1 month"));
            $status = 0;
            
            if(isset($_GET['alpha']) && isset($_GET['beta']))
            {
                $alpha = intval($_GET['alpha']);
                $beta = intval($_GET['beta']);
                
                $sql="SELECT version_gamma FROM core_version WHERE version_alpha=$alpha AND version_beta=$beta ORDER BY version_gamma DESC LIMIT 1";
                $dbMaxGamma = dbselectsingle($sql);
                $gamma = $dbMaxGamma['data']['version_gamma']+1;
            } else {
                //find the highest version number and set it one higher
                $sql="SELECT version_alpha, version_beta, version_gamma FROM core_version ORDER BY version_number DESC LIMIT 1";
                
                $dbMax=dbselectsingle($sql);
                $max = $dbMax['data'];
                if($max['version_beta']<100)
                {
                    $alpha = $max['version_alpha'];
                } else {
                    $alpha = $max['version_alpha']+1;
                }
                if($max['version_gamma']<100)
                {
                    $beta = $max['version_beta'];
                } else {
                    $beta = $max['version_beta']+1;
                }
                if($max['version_gamma']==100)
                {
                    $gamma = 1;
                } else {
                    $gamma = $max['version_gamma']+1;
                }
            }
            
        }
        
        print "<form method=POST action=\"$_SERVER[PHP_SELF]\" class='form-horiztonal'>\n";
        make_number('version_alpha',$alpha,'Alpha','Enter the alpha part of the version number (0,1,2)');
        make_number('version_beta',$beta,'Beta','Enter the beta part of the version number (1,2,3, etc)');
        make_number('version_gamma',$gamma,'Gamma','Enter the gamma part of the version number (1,2,3, etc)');
        make_text('version_title',$title,'Title','Short descriptive title for this version');
        make_textarea('version_details',$details,'Details','Full notes of changes for this version');
        make_date('version_target',$date,'Target date','When should this be ready?');
        make_checkbox('version_status',$status,'Completed','Check if completed');
        make_submit('submit','Save Version');
        make_hidden('id',$id);
        print "</form>\n";
        
        print "</div>\n";
        print "<div class='col-xs-12 col-sm-4'>\n";
        print "<div class='well'>\n";
        print "</div>\n";
        print "</div>\n";
    print "</div>\n";  
}

function version_save()
{
    $id=intval($_POST['id']);
    
    $version_alpha=addslashes($_POST['version_alpha']);
    $version_beta=addslashes($_POST['version_beta']);
    $version_gamma=addslashes($_POST['version_gamma']);
    $version_number=$version_alpha.$version_beta.$version_gamma;
    
    $version_title=addslashes($_POST['version_title']);
    $version_details=addslashes($_POST['version_details']);
    $version_target=addslashes($_POST['version_target']);
    if($_POST['version_status']){$version_status=1;}else{$version_status=0;}
    
    if($id==0 || $id=='')
    {
        $sql="INSERT INTO core_version (version_alpha, version_beta, version_gamma, version_title, version_details, version_status, version_number, version_target) VALUES
        ('$version_alpha', '$version_beta', '$version_gamma', '$version_title', '$version_details', '$version_status', '$version_number', '$version_target')";
        $dbInsert = dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $sql="UPDATE core_version SET version_alpha='$version_alpha', version_beta='$version_beta', version_gamma='$version_gamma', 
        version_title='$version_title', version_details='$version_details', version_status='$version_status', version_number='$version_number', version_target='$version_target' WHERE id=$id";
        $dbUpdate = dbexecutequery($sql);
        $error=$dbUpdate['error'];
        
    }
    
    if ($error!='')
    {
        setUserMessage('There was a problem saving the user group.<br>'.$error,'error');
    } else {
        setUserMessage('User group successfully saved','success');
    }
    redirect("?action=list");
}



function versions_list() {
?>
    <div class='row'>
    <div class='col-xs-10'>
    <ul class="ssortable" style='list-style:none;'>
        <?php
            $alphas = array("0"=>"Feature-incomplete Alpha","1"=>"Core Features Complete Beta","2"=>"Non-Production Feature Rollout");
            foreach($alphas as $alpha=>$alphaTitle)
            {
                print "
        <li id='root-$alpha'>
            <div class='col-xs-12 label label-primary' style='width:100%;text-align:left;padding:10px;color:white;'>$alphaTitle</div>
            ";
                
                $sql="SELECT MIN(version_beta) AS beta FROM core_version WHERE version_alpha = $alpha";
                $dbMinBeta = dbselectsingle($sql);
                $minBeta = $dbMinBeta['data']['beta'];
                
                $sql="SELECT MAX(version_beta) AS beta FROM core_version WHERE version_alpha = $alpha";
                $dbMaxBeta = dbselectsingle($sql);
                $maxBeta = $dbMaxBeta['data']['beta'];
                
                //get all versions in sorter
                $sql="SELECT DISTINCT(version_beta) AS beta FROM core_version WHERE version_alpha = $alpha ORDER BY version_beta ASC";
                $dbBetas=dbselectmulti($sql);
                if($dbBetas['numrows']>0)
                {
                   foreach($dbBetas['data'] as $b)
                   {
                       $beta = $b['beta'];
                       print "
            <ul style='list-style:none;'>
                <li id='v".$alpha."-".$beta."'>
                    <div class='col-xs-12' style='background-color:#999;color:white;font-weight:bold;padding:10px;'>Version - $alpha . $beta <span class='pull-right'>";
                    if($beta>$minBeta)
                    {
                        print "<a href='?action=betaup&alpha=$alpha&beta=$beta'><i class='fa fa-arrow-up'></i></a>";
                    }
                    if($beta<$maxBeta)
                    {
                        print "<a href='?action=betadown&alpha=$alpha&beta=$beta'><i class='fa fa-arrow-down'></i></a>";
                    }
                    print "<a href='?action=edit&alpha=$alpha&beta=$beta' class='btn btn-primary' style='margin-left:20px;'>Add new gamma version</a>";    
                    print "</span></div>";
                
                        //get all versions in sorter
                        $sql="SELECT * FROM core_version WHERE version_alpha = $alpha AND version_beta = $beta ORDER BY version_gamma ASC";
                        $dbGammas=dbselectmulti($sql);
                        if($dbGammas['numrows']>0)
                        {
                           $i=1;
                           $sql="SELECT version_gamma FROM core_version WHERE version_alpha = $alpha AND version_beta = $beta AND version_status=0 ORDER BY version_gamma ASC";
                           $dbMin = dbselectsingle($sql);
                           $minG = $dbMin['data']['version_gamma'];
                           foreach($dbGammas['data'] as $g)
                           {
                               $gamma = $g['version_gamma'];
                               if($g['version_status']=='1')
                               {
                                   $status='text-decoration: line-through';
                               } else {
                                   $status='';
                               }
                               print "
                    <ul style='list-style:none;'>
                        <li id='v".$alpha."-".$beta."-".$gamma."' >
                            <div class='col-xs-1' style='padding-top:20px;'>";
                            if($g['version_status']==0)
                            {
                                if($gamma>$minG){
                                    print "<a href='?action=up&id=$g[id]'><i class='fa fa-arrow-up'></i></a><br>";
                                } else {
                                    print "<br>";
                                }
                                if($i<($dbGammas['numrows'])){
                                    print "<a href='?action=down&id=$g[id]'><i class='fa fa-arrow-down'></i></a>";
                                }
                            } else {
                                print "<i class='fa fa-2x fa-check text-success'></i>";
                            }
                            print "</div>";
                            print "<div class='col-xs-11' style='margin: 10px 0;padding:10px 0;border-bottom: thin solid black;'><span style='$status'>Sub-version $alpha . $beta . $gamma | $g[version_title]</span>
                            <br>$g[version_details]";
                            print "<div class='btn-group pull-right' role='group'>\n";
                            if($g['version_status']==0)
                            {
                                print "<a href='?action=complete&id=$g[id]' class='btn btn-success'>Complete</a>";
                            }
                            print "<a href='?action=edit&id=$g[id]' class='btn btn-primary'>Edit</a>";
                            if($g['version_status']==0)
                            {
                                print "<a href='?action=delete&id=$g[id]' class='btn btn-danger delete'>Delete</a>";
                            }
                            
                            print "</div>
                            </div>
                        ";
                               print "
                        </li>
                    </ul>
                    ";
                    $i++;
                           } 
                        }
                        
                       print "
                </li>
            </ul>
            ";
                   } 
                }
                $calpha = $version['version_alpha'];
                $cbeta = $version['version_beta'];
                $cgamma = $version['version_gamma'];
                
                print "
        </li>
        ";
            }
        ?>
        
    </ul>
    </div>
    <div class='col-xs-2'>
    <a href='?action=edit' class='btn btn-2x btn-primary'>Add new alpha version</a><br><br>
    <a href='?action=requests' class='btn btn-2x btn-primary'>Show user requests</a>
    </div>
    </div>
    <?php
    
}

 
 /*
 //var data = \$('ul.sortable').nestedSortable('toArray', {listType: 'ul'});
        //\$('#info').html(data);
        
 */
/*
    $('.expandEditor').attr('title','Click to show/hide item editor');
    $('.disclose').attr('title','Click to show/hide children');
    $('.deleteMenu').attr('title', 'Click to delete item.');

    $('.disclose').on('click', function() {
        $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
        $(this).toggleClass('ui-icon-plusthick').toggleClass('ui-icon-minusthick');
    });
    
    $('.expandEditor, .itemTitle').click(function(){
        var id = $(this).attr('data-id');
        $('#menuEdit'+id).toggle();
        $(this).toggleClass('ui-icon-triangle-1-n').toggleClass('ui-icon-triangle-1-s');
    });
    
    $('.deleteMenu').click(function(){
        var id = $(this).attr('data-id');
        $('#menuItem_'+id).remove();
    });
        
    $('#serialize').click(function(){
        serialized = $('ol.sortable').nestedSortable('serialize');
        $('#serializeOutput').text(serialized+'\n\n');
    })

    $('#toHierarchy').click(function(e){
        hiered = $('ol.sortable').nestedSortable('toHierarchy', {startDepthCount: 0});
        hiered = dump(hiered);
        (typeof($('#toHierarchyOutput')[0].textContent) != 'undefined') ?
        $('#toHierarchyOutput')[0].textContent = hiered : $('#toHierarchyOutput')[0].innerText = hiered;
    })
*/ 
$Page->footer();
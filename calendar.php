<?php
//core calendar file
include("includes/boot.php") ;

?>
<body>
<div id='loading' style='display:none'>loading...</div>
<div id='quickjump' style='z-index:30000;'>
<?php
if($_GET['qdate'])
{
    $qdate=$_GET['qdate'];
} else {
    $qdate=date("Y-m-d",strtotime("+1 week"));
}

//lets make a drop down with a list of the next 52 weeks in it with starting dates
//calculate the date that is "sunday" for the current week
$startdate=date("Y-m-d");
while (date("w",strtotime($startdate))!=0)
{
    $startdate=date("Y-m-d",strtotime($startdate."-1 day"));
}
print "Select a week to jump to: <select id='quickdate' onChange='jumpDate(this.value);'>";
for($i=1;$i<53;$i++)
{
    $parts=explode("-",$startdate);
    $year=$parts[0];
    $month=$parts[1]-1;
    $day=$parts[2];
    print "<option id='$startdate' value='$year-$month-$day'>$startdate</option>\n";
    $startdate=date("Y-m-d",strtotime($startdate."+1 week"));       
}
print "</select>";
?>
</div>
<div id='calendar'></div>
<?php
$Page->footer();
?>


<?php
include("includes/boot.php") ;
if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}
    switch ($action)
    {
        case "batch":
        batch_import();
        break;
        
        case "Import Batch":
        process_batch_import();
        break;
        
        
        default:
        batch_import();
        break;
        
    } 
     
function batch_import()
{
    global $ordersources, $siteID;
    
    //get newsprint vendors
    $sql="SELECT id, account_name FROM accounts WHERE newsprint=1 ORDER BY account_name";
    $dbVendors = dbselectmulti($sql);
    $vendors[0]='Select vendor';
    if($dbVendors['numrows']>0)
    {
        foreach($dbVendors['data'] as $vendor)
        {
            $vendors[$vendor['id']]=stripslashes($vendor['account_name']);
        }
    }
    $date=date("Ymd");
    print "<form method=post enctype='multipart/form-data' class='form-horizontal'>\n";
    make_select('vendor_id',$vendors[0],$vendors,'Select vendor','All orders will be created under this vendor');
    make_select('source',$ordersources[0],$ordersources,'Select source','Order source for rolls');
    make_file('edi','Batch Import File','Please select the CSV file containing the rolls tags and weights<br>Format of file should be in EDIWise format. Orders will be auto-created during the import process.');
    make_submit('submit','Import Batch');
    print "</form>\n"; 
}   

function process_batch_import()
{
   global $siteID;
   require_once 'includes/parsecsv.lib.php';
        
   $vendorID = intval($_POST['vendor_id']);
   $source=addslashes($_POST['source']);
   
   $file=$_FILES['edi']['tmp_name'];
   $contents=file_get_contents($file);
   
   $csv = new parseCSV();
   $csv->heading = true;
   $result = $csv->parse_string($contents);
   
   $orderID=1;
   $orderItemID=1;
   
   //get the paper type info (common name, roll width and brightness for the rolls);
   $orders=array();
   
   if(count($result)>0)
   {
       foreach($result as $line)
       {
           $rollTag = $line['Bar Code'];
           $kind = $line['P.Group-Diameter-Width'];
           $kindParts = explode("-",str_replace(" ","",$kind));
           $paper = $kindParts[0];
           $width = $kindParts[2];
           
           //look up paper roll width (in mm)
           $sql="SELECT id,width FROM paper_sizes WHERE width_mm='$width'";
           $dbSize = dbselectsingle($sql);
           if($dbSize['numrows']>0)
           {
               $sizeID=$dbSize['data']['id'];
               $sizeWidth=$dbSize['data']['width'];
           } else {
               $sizeID = 0; // default since it didn't match
               $sizeWidth = 0;
           }
           
           //look up paper info
           $sql="SELECT id, common_name,paper_brightness, paper_weight FROM paper_types WHERE grade_code LIKE '%$paper%'";
           $dbType = dbselectsingle($sql);
           if($dbType['numrows']>0)
           {
               $typeID=$dbType['data']['id'];
               $typeName=$dbType['data']['common_name'];
               $typeBrightness=$dbType['data']['paper_brightness'];
               $typeWeight=$dbType['data']['paper_weight'];
           } else {
               $typeID = 0; // default since it didn't match
               $typeName = 'unknown';
               $typeBrightness = '0';
               $typeWeight = 'unknown';
           }
           
           $order = explode("/",str_replace(" ","",$line['Order/Lot/Vehicle ID']));
           $order = $order[0];
            
           
           $date = date("Y-m-d",strtotime($line['Date']));
           $manifest = $line['Manifest No.'];
           $weight = $line['Weight'];
           
           //see if the order exists 
           if(in_array($order,$orders))
           {
               $orderID = array_search($order,$orders);
           } else {
               $sql="SELECT id FROM orders WHERE order_code='$order'";
               $dbOrder = dbselectsingle($sql);
               if($dbOrder['numrows']>0)
               {
                   $orderID=$dbOrder['data']['id'];
                   $orders[$orderID]=$order;
                   
               } else {
                   //create new order
                   $orderDate = date("Y-m-d H:i");
                   $orderBy = $_SESSION['userid'];
                   $sql="INSERT INTO orders (vendor_id, order_by, order_datetime, order_code, order_source, imported, validated, status, site_id) VALUES ('$vendorID', '$orderBy', '$orderDate', '$order','$source',1,0,4,'$siteID')";
                   $dbOrder = dbinsertquery($sql);
                   if($dbOrder['error']) die('aborting due to: '.$dbOrder['error']);
                   $orderID = $dbOrder['insertid'];
                   
                   //print "Creating order with $sql<br>";
                   $orderID++;
                   $orders[$orderID]=$order;
                   
                   print "<h3>Created a new order: $orderID for order $order</h3>";
               }
           }
           
           //now the order item
           if(in_array($orderID.'-'.$kind,$orderItems))
           {
               //we've processed this paper size & type already so we have the order item id
               $orderItemID = array_search($orderID.'-'.$kind,$orderItems);
               
           } else {
               //look it up
               $sql="SELECT id FROM order_items WHERE order_id = '$orderID' AND paper_type_id='$typeID' AND size_id='$sizeID'";
               $dbItem = dbselectsingle($sql);
               if($dbItem['numrows']>0)
               {
                   $orderItemID = $dbItem['data']['id'];
                   $orderItems[$orderItemID]=$orderID.'-'.$kind;
               } else {
                   //create a new order item
                   $sql="INSERT INTO order_items (order_id, paper_type_id, size_id, itemdisplay_order,imported,site_id) VALUES ('$orderID', '$typeID', '$sizeID', 1, 1, '$siteID')";
                   $dbOrderItem = dbinsertquery($sql);
                   $orderItemID = $dbOrderItem['insertid'];
                   //$orderItemID++;
                   $orderItems[$orderItemID]=$orderID.'-'.$kind;
                   //print "Creating order item with $sql<br>";
                   print "<h4>Created a new order item: $orderItemID for paper type $typeID / $paper and $sizeID / $width</h4>";
               }
           }
           
                        
           //check first to make sure that rolltag hasn't been already added to the database
           $sql="SELECT * FROM rolls WHERE roll_tag='$rollTag'";
           $dbCheck=dbselectsingle($sql);
           if($dbCheck['numrows']==0)
           {
               //add it to the database
               $sql="INSERT INTO rolls (order_id, order_item_id, common_name, roll_width, paper_brightness, paper_weight, status, receive_datetime, roll_tag, butt_roll, roll_weight, parent_tag, manifest_number, site_id, validated, imported) VALUES ('$orderID', '$orderItemID','$typeName', '$sizeWidth', '$typeBrightness', '$typeWeight', 1, '$date', 
               '$rollTag', 0, '$weight', '', '$manifest', '$siteID', 1, 1)";
               //print "<br>Would be inserting with:<br>$sql<hr>";
               
               $rolls[]=array('orderID'=>$orderID,'orderItemID'=>$orderItemID,'paperType'=>$typeName,'rollWidth'=>$sizeWidth,'rollTag'=>$rollTag);
               $rollCount++;
               $dbInsertRoll=dbinsertquery($sql);
               if($dbInsertRoll['error']!='')
               {
                   print "There was a problem inserting the  roll into the roll table.<br>&nbsp;&nbsp;&nbsp;$sql<br>";
               }
               
           } else {
               print "Rolltag $rollTag was already in the roll database. You may be uploading old records.<br>";
           }
           
       }
       print "<br><a href='?action=batch'>Import another order</a><br>"; 
       print "<table class='table table-bordered table-striped'>\n";
       print "<tr><th>Order ID</th><th>Item ID</th><th>Type</th><th>Size</th><th>RollTag</th></tr>\n";
       foreach($rolls as $roll)
       {
           print "<tr>";
           print "<td>$roll[orderID]</td><td>$roll[orderItemID]</td><td>$roll[paperType]</td><td>$roll[rollWidth]</td><td>$roll[rollTag]</td></tr>\n";
       }
       print "<tr><th colspan=5>Imported $rollCount rolls</th></tr>";
       print "</table>\n";
       
   } else {
       print "Sorry, the file was empty.<br>";
   }
   
} 
$Page->footer();
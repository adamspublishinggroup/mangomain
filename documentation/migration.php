<?php
include 'functions.php';
page_header() ?>
<?php nav(); ?>

    <!-- Main component for a primary marketing message or call to action -->
    <div class="jumbotron">
        <h1>Migration</h1>
        <p>This document supplies all Mango migration information that needs to happen to move from V1 to V2.</p>

    </div>


<div class="well">
    <h3>Database Changes</h3>
    <p>Make sure <em>ALL</em> tables have 'site_id' int(11) unsigned NOT NULL</p>
    <p>Make sure <em>ALL</em> tables have 'created_at' DATETIME NOT NULL</p>
    <p>Make sure <em>ALL</em> tables have 'updated_at' DATETIME NOT NULL</p>
    <p>Convert all tables to InnoDB</p>
    <pre>
        ALTER TABLE `user_sites` ENGINE=InnoDB;
        ALTER TABLE `user_positions` ENGINE=InnoDB;
        ALTER TABLE `user_groups_xref` ENGINE=InnoDB;
        ALTER TABLE `user_groups` ENGINE=InnoDB;
        ALTER TABLE `user_departments` ENGINE=InnoDB;
        ALTER TABLE `users` ENGINE=InnoDB;
        ALTER TABLE `stitchers_hoppers` ENGINE=InnoDB;
        ALTER TABLE `stitchers` ENGINE=InnoDB;
        ALTER TABLE `rolls` ENGINE=InnoDB;
        ALTER TABLE `publications_insertzones` ENGINE=InnoDB;
        ALTER TABLE `publications_inserttrucks` ENGINE=InnoDB;
        ALTER TABLE `publications_insertruns` ENGINE=InnoDB;
        ALTER TABLE `publications_insertroutes` ENGINE=InnoDB;
        ALTER TABLE `paper_types` ENGINE=InnoDB;
        ALTER TABLE `paper_sizes` ENGINE=InnoDB;
        ALTER TABLE `order_items` ENGINE=InnoDB;
        ALTER TABLE `orders` ENGINE=InnoDB;
        ALTER TABLE `monthly_inventory_items` ENGINE=InnoDB;
        ALTER TABLE `monthly_inventory` ENGINE=InnoDB;
        ALTER TABLE `mango_news_viewed` ENGINE=InnoDB;
        ALTER TABLE `maintenance_topics` ENGINE=InnoDB;
        ALTER TABLE `maintenance_solution_topic_xref` ENGINE=InnoDB;
        ALTER TABLE `maintenance_solutions_images` ENGINE=InnoDB;
        ALTER TABLE `maintenance_solutions` ENGINE=InnoDB;
        ALTER TABLE `kill_inserts` ENGINE=InnoDB;
        ALTER TABLE `job_rolls` ENGINE=InnoDB;
        ALTER TABLE `jobs_recurring` ENGINE=InnoDB;
        ALTER TABLE `jobs_inserter_trucks` ENGINE=InnoDB;
        ALTER TABLE `jobs_inserter_rundata` ENGINE=InnoDB;
        ALTER TABLE `jobs_inserter_routes` ENGINE=InnoDB;
        ALTER TABLE `jobs_inserter_plans` ENGINE=InnoDB;
        ALTER TABLE `jobs_inserter_packages_settings` ENGINE=InnoDB;
        ALTER TABLE `jobs_inserter_packages` ENGINE=InnoDB;
        ALTER TABLE `jobs` ENGINE=InnoDB;
        ALTER TABLE `insert_zoning` ENGINE=InnoDB;
        ALTER TABLE `insert_storage_locations` ENGINE=InnoDB;
        ALTER TABLE `inserts_received` ENGINE=InnoDB;
        ALTER TABLE `inserts` ENGINE=InnoDB;
        ALTER TABLE `inserters_hoppers` ENGINE=InnoDB;
        ALTER TABLE `inserters` ENGINE=InnoDB;
        ALTER TABLE `helpdesk_types` ENGINE=InnoDB;
        ALTER TABLE `helpdesk_topics` ENGINE=InnoDB;
        ALTER TABLE `helpdesk_tickets` ENGINE=InnoDB;
        ALTER TABLE `helpdesk_statuses` ENGINE=InnoDB;
        ALTER TABLE `helpdesk_solution_topic_xref` ENGINE=InnoDB;
        ALTER TABLE `helpdesk_solutions_images` ENGINE=InnoDB;
        ALTER TABLE `helpdesk_solutions` ENGINE=InnoDB;
        ALTER TABLE `helpdesk_priorities` ENGINE=InnoDB;
        ALTER TABLE `faq` ENGINE=InnoDB;
        ALTER TABLE `email_groups` ENGINE=InnoDB;
        ALTER TABLE `edi_rolls` ENGINE=InnoDB;
        ALTER TABLE `edi_order_items` ENGINE=InnoDB;
        ALTER TABLE `cron_log` ENGINE=InnoDB;
        ALTER TABLE `core_sites` ENGINE=InnoDB;
        ALTER TABLE `core_preferences` ENGINE=InnoDB;
        ALTER TABLE `core_permission_page` ENGINE=InnoDB;
        ALTER TABLE `core_permission_list` ENGINE=InnoDB;
        ALTER TABLE `core_pages` ENGINE=InnoDB;
        ALTER TABLE `core_menu` ENGINE=InnoDB;
        ALTER TABLE `core_cronexecution` ENGINE=InnoDB;
        ALTER TABLE `core_cron` ENGINE=InnoDB;
        ALTER TABLE `accounts_contacts` ENGINE=InnoDB;
        ALTER TABLE `accounts` ENGINE=InnoDB;
    </pre>

    <h4>Add foreign key constraints to the following tables for handling parent deletions</h4>
    <ul>
        <li>accounts_contacts (accounts_id references accounts.id) </li>
    </ul>
    <hr>
    <ul class="list-unstyled">
        <li>
            <ul class="list-unstyled">
                Users
                <ul>Add Fields
                    <li>Add field: token | varchar(100) | nullable</li>
                    <li>Add field: online | tinyint | 0</li>
                    <li>Add field: last_online | datetime | nullable</li>
                </ul>
                <ul>Change Fields
                    <li>Change field: password | varchar (255) from (60) | nullable</li>
                    <li>Change field: vision_data_sales_id | to sales_id</li>
                    <li>Change field: vision_data_sales_name | to sales_name</li>
                </ul>
                <ul>Remove Fields
                    <li>businesscard_title</li>
                    <li>pims_access</li>
                    <li>intranet_access</li>
                    <li>delete_intranet_news</li>
                    <li>delete_intranet_events</li>
                    <li>delete_intranet_docs</li>
                    <li>edit_intranet_news</li>
                    <li>edit_intranet_events</li>
                    <li>edit_intranet_docs</li>
                    <li>master_key</li>
                    <li>submaster_key</li>
                    <li>mango</li>
                    <li>papaya</li>
                    <li>guava</li>
                    <li>kiwi</li>
                    <li>pineapple</li>
                    <li>phone_punchdown</li>
                    <li>simple_menu</li>
                    <li>extension_id</li>
                    <li>weight</li>
                    <li>simple_tables</li>
                </ul>

            </ul>
            <ul class="list-unstyled">
                Core Permission List
                <ul>Add Fields
                </ul>
                <ul>Change Fields
                </ul>
                <ul>Remove Fields
                    <li>weight</li>
                    <li>name</li>
                    <li>type</li>
                    <li>auto_enable</li>
                </ul>

            </ul>
            <ul class="list-unstyled">
                user_messages
                <ul>Add Fields
                    <li>Add field: id | int | auto-increment | primary</li>
                    <li>Add field: user_id | int | nullable</li>
                    <li>Add field: create_datetime | datetime | nullable</li>
                    <li>Add field: updated_datetime | datetime | nullable</li>
                    <li>Add field: received_datetime | datetime | nullable</li>
                    <li>Add field: read_datetime | datetime | nullable</li>
                    <li>Add field: status | int | default(0)</li>
                    <li>Add field: sender_id | int | default(0)</li>
                    <li>Add field: message_type | varchar(50) | enum vales (message, chat, alert,job_update</li>
                    <li>Add field: message_subject | varchar(255) | </li>
                    <li>Add field: message_body | text | </li>
                    <li>Add field: message_priority | tinyint(1) | default 0</li>

                </ul>
                <ul>Change Fields
                </ul>
                <ul>Remove Fields

                </ul>

            </ul>
            <ul class="list-unstyled">
                publications_insertruns
                <ul>Add Fields
                    <li>Add field: status | int | DEFAULT 1</li>
                </ul>
                <ul>Change Fields
                </ul>
                <ul>Remove Fields
                    run_days
                    monday_count
                    tuesday_count
                    wednesday_count
                    thursday_count
                    friday_count
                    saturday_count
                    sunday_count
                </ul>

            </ul>
        </li>
    </ul>
</div>

<?php page_footer() ?>
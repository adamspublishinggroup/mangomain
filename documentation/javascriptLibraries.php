<?php
include 'functions.php';
page_header() ?>
<?php nav(); ?>

<!-- Main component for a primary marketing message or call to action -->
<div class="jumbotron">
    <h1>Javascript Libraries</h1>
    <p>The following javascript libraries are included in the all.js file</p>

</div>
<div class="alert alert-info" role="alert">Where possible, all Javascript libraries are managed with bower, stored in bower_components root directory and merged into a main all.js file with gulp. All JS and CSS files are referenced in gulpfile.js in the root and are compiled, minified, etc. with Gulp into all.js, all.css, and app.js files.</div>
<ul>
    <li><a href="http://http://jquery.com/">jQuery</a></li>
    <li><a href="http://jqueryui.com/" target="_blank">jquery UI</a></li>
    <li><a href="http://www.getbootstrap.com/" target="_blank">Bootstrap 3</a></li>
    <li><a href="http://www.momentjs.com/" target="_blank">Moment JS</li>
    <li><a href="http://fullcalendar.io/" target="_blank">Full Calendar (arshaw) </a></li>
    <li><a href="http://www.datatables.net/" target="_blank">Datatables (plus bootstrap, buttons, bootstrap buttons, colVis, flash, print, colReorder plugins) </a></li>
    <li><a href="https://github.com/jschr/bootstrap-modal" target="_blank">Bootstrap Modal Manager </a></li>
    <li><a href="http://qtip2.com/" target="_blank">QTip2 tooltips </a></li>
    <li><a href="https://github.com/malsup/form/" target="_blank">Malsup Ajax Form handler </a></li>
    <li><a href="http://eonasdan.github.io/bootstrap-datetimepicker/" target="_blank">Bootstrap Datetimepicker</a></li>
    <li><a href="https://github.com/bootstrap-tagsinput/bootstrap-tagsinput" target="_blank">Bootstrap Tags input</a></li>
    <li><a href="http://summernote.org/" target="_blank">Summernote WYSIWYG text editor </a></li>
    <li><a href="http://digitalbush.com/projects/masked-input-plugin/" target="_blank">Masked Input </a></li>
    <li><a href="https://select2.github.io/" target="_blank">Select2 select box replacement</a></li>
    <li><a href="http://1000hz.github.io/bootstrap-validator/" target="_blank">Bootstrap form validation </a></li>
    <li><a href="https://github.com/Eonasdan/bootstrap-datetimepicker" target="_blank">Date/time picker</a></li>
    <li><a href="https://github.com/mjolnic/bootstrap-colorpicker" target="_blank">Color picker</a></li>
    <li><a href="https://github.com/seiyria/bootstrap-slider" target="_blank">Slider</a></li>
    <li><a href="http://www.dropzonejs.com/" target="_blank">DropzoneJS (file upload handler)</a></li>
    <li><a href="https://twitter.github.io/typeahead.js/" target="_blank">Typeahead (autocompletion)</a></li>
    <li><a href="https://github.com/istvan-ujjmeszaros/bootstrap-duallistbox/" target="_blank">Dual Listbox - </a></li>
    <li><a href="https://github.com/tuupola/jquery_chained/" target="_blank">Chained selects</a></li>
    <li><a href="https://github.com/swisnl/jQuery-contextMenu" target="_blank">jQuery context menu</a></li>
    <li><a href="https://github.com/VinceG/twitter-bootstrap-wizard" target="_blank">Bootstrap Form Wizard</a></li>
    <li>
        <ul>Smart Admin UI template (admin look/feel and some custom widgets)
            <li>SmartNotifications (large & small)</li>
            <li>Jarvis (sortable dashboard widgets)</li>
            <li>SmartChat (chat handler + emoticons)</li>
            <li>"App" - general theme UI functionality & navigation handler</li>
        </ul>
    </li>
</ul>
<?php page_footer() ?>

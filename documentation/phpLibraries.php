<?php
include 'functions.php';
page_header() ?>
<?php nav(); ?>

<!-- Main component for a primary marketing message or call to action -->
<div class="jumbotron">
    <h1>PHP Libraries</h1>
    <p>The following PHP libraries are included in the overall build</p>

</div>
<ul>
    <li>Intervention Image Library - http://image.intervention.io/</li>
    <li>phpMimeMail (v3.0) https://github.com/smxi/php-html-mime-mail</li>
    <li>-------------future?-----------</li>
    <li>Predis - https://github.com/nrk/predis</li>
    <li>PHP Fast Cache - http://www.phpfastcache.com/</li>
    <li>Slim API Framework (for REST services) - http://www.slimframework.com/</li>
    
</ul>
<?php page_footer() ?>

<?php
include("includes/boot.php");

//setup drop-downs for clock
$everyhours=array();
for ($i=0;$i<=24;$i++){
    $everyhours[$i]=$i;
}
$everyminutes=array();
for ($i=0;$i<=55;$i=$i+5){
    if ($i<10){$i="0".$i;}
    $everyminutes[$i]=$i;
}
$dailyhours=$everyhours;
$dailyminutes=$everyminutes;
$weeklyhours=$everyhours;
$weeklyminutes=$everyminutes;
$monthlyhours=$everyhours;
$monthlyminutes=$everyminutes;



if ($_POST['submit']=='Add'){
        save_cron('insert');
    } elseif ($_POST['submit']=='Update'){
       save_cron('update'); 
    } else { 
        show_cron();
    }

function show_cron() {
    $cronid=intval($_GET['cronid']);
    $jobid=intval($_GET['jobid']);
    if ($_GET['action']=='viewlog'){
        $sql="SELECT * from core_cronexecution WHERE id=$jobid";
        $dbresult=dbselectsingle($sql);
        $record=$dbresult['data'];
        print "Job was scheduled for $record[exectime] and actually ran at $record[actualtime]<br />";
        print "The following are the notes from the job<br/><br />";
        $notes=explode(";",$record['jobnotes']);
        foreach($notes as $key=>$note){
            print $note."<br />";
        }
        print "<br /><br />";
        print "<a href='?action=view&cronid=$record[cronid]'>Back to job log</a>"; 
    } elseif ($_GET['action']=='run')
    {
        print "<a href='?action=list' class='submit'>Return to cron job list</a><br>";
        print "<iframe src='cronprocessorSingle.php?cronid=$cronid' frameborder=0 width=650 height=600></iframe>\n";     
      
    } elseif ($_GET['action']=='delete'){
    $sql="DELETE FROM core_cron where id=$cronid";
    $dbDelete=dbexecutequery($sql);
    $sql="DELETE FROM core_cronexecution where cronid=$cronid";
    $dbDelete=dbexecutequery($sql);
    if ($error!='')
    {
        setUserMessage('There was a problem deleting the cron job.<br>'.$error,'error');
    } else {
        setUserMessage('Cron job has been successfully deleted','success');
    }
    redirect("?action=list");
  
  }elseif ($_GET['action']=='clearpending'){
    $sql="DELETE FROM core_cronexecution";
    $dbDelete=dbexecutequery($sql);
    $error=$dbDelete['error'];
    if ($error!='')
    {
        setUserMessage('There was a problem clearing all pending jobs.<br>'.$error,'error');
    } else {
        setUserMessage('All future cron jobs have been successfully purged.','success');
    }
    redirect("?action=list");
  
  }elseif ($_GET['action']=='view'){
    
    $sql="SELECT * FROM core_cronexecution WHERE cronid=$cronid";
    $dbLog=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new cron job</a>,<a href='?action=list'>Return to job list</a>","Scheduled Time,Actual Time",4);
    if ($dbLog['numrows']>0){
        foreach ($dbLog['data'] as $record) {
            $exectime=stripslashes($record['exectime']);
            $actualtime=stripslashes($record['actualtime']);
            $id=$record['id'];
            print "<tr><td>$exectime</td><td>$actualtime</td>";
            print "<td><a href='?action=viewlog&jobid=$id'>View Notes</a></td>";
            print "</tr>\n";
            }
    }
    tableEnd($dbLog);

  }elseif ($_GET['action']=='edit' || $_GET['action']=='add'){
    if ($_GET['action']=='edit'){  
        $sql="SELECT * FROM core_cron WHERE id=$cronid";
        $dbresult=dbselectsingle($sql);
        $rc=$dbresult['numrows'];
        $record=$dbresult['data'];
        $description=stripslashes($record['description']);
        $title=stripslashes($record['title']);
        $everyminutes=stripslashes($record['everyminutes']);
        $daily=stripslashes($record['dailytime']);
        $dailyhour=date("G",strtotime($daily));
        $dailyminute=date("i",strtotime($daily));
        $weekly=stripslashes($record['weeklytime']);
        $weeklyhour=date("G",strtotime($weekly));
        $weeklyminute=date("i",strtotime($weekly));
        $monthly=stripslashes($record['monthlytime']);
        $monthlyhour=date("G",strtotime($monthly));
        $monthlyminute=date("i",strtotime($monthly));
        $frequencytype=stripslashes($record['frequencytype']);
        $script=stripslashes($record['script']);
        $enabled=stripslashes($record['enabled']);
        $dailyday=stripslashes($record['dailyday']);
        $monthlyday=stripslashes($record['monthlyday']);
        $date_offset=stripslashes($record['date_offset']);
        $params=stripslashes($record['params']);
        $function=stripslashes($record['function']);
        $mon=stripslashes($record['mon']);
        $tue=stripslashes($record['tue']);
        $wed=stripslashes($record['wed']);
        $thu=stripslashes($record['thu']);
        $fri=stripslashes($record['fri']);
        $sat=stripslashes($record['sat']);
        $sun=stripslashes($record['sun']);
        $button="Update";
        $temp=stripslashes($record['startdate']);
        $startdate=date("Y-m-d",strtotime($temp));
        
    } else {
        $mon=1;
        $tue=1;
        $wed=1;
        $thu=1;
        $fri=1;
        $sat=1;
        $sun=1;
        $everyhour="0";
        $everyminute="00";
        $dailyhour="0";
        $dailyminute="00";
        $weeklyhour="0";
        $weeklyminute="00";
        $monthlyhour="0";
        $monthlyminute="00";
        $ampm="am";
        $enabled=1;
        $frequency=1;
        $frequencytype=1;
        $button="Add";
        $startdate=date("Y-m-d",strtotime('today'));
        $date_offset=0;
        $everyminutes=15;
        $dailyday=1;
        $monthlyday=1;
    }
    $frequencies[1]='Every X minutes';
    $frequencies[2]='Every X days at specified time';
    $frequencies[3]='Every selected day at specified time';
    $frequencies[4]='Every specified day of the month at specified time';
    $minutetype=0;
    $dailytype=0;
    $weeklytype=0;
    $monthlytype=0;
    if ($frequencytype==1){$minutetype=1;}
    if ($frequencytype==2){$dailytype=1;}
    if ($frequencytype==3){$weeklytype=1;}
    if ($frequencytype==4){$monthlytype=1;}
    print "<form method=post class='form-horizontal'>\n";
    make_checkbox('enabled',$enabled,'Active','Check to enable this cron job');
    make_text('title',$title,'Name','Name of the cron job',50);
    make_textarea('description',$description,'Description','',60,10);
    make_text('script',$script,'Script','What is the name of the script to be included from the cron jobs folder?',50);
    make_text('function',$function,'Function','What is the name of the function to call in the script?',50);
    make_text('params',$params,'Parameters','What parameters should be passed to the function?',50);
    make_number('date_offset',$date_offset,'Date offset','Offset for date if used by function. By default, dates are calculated as \'today\' (Ex. 1 equals tomorrow, 0 equals today, -1 equals yesterday)');
    make_date('startdate',$startdate,'Start date','What date should this job start?');
    
    make_select('frequencytype',$frequencies[$frequnecy],$frequencies,'Run Schedule','What type of schedule to run this job on');
    ?>
    <div class="form-group">
        <label for="date_offset" class="col-sm-2 control-label">Run Schedule</label>
        <div class="col-sm-10">
          <div class='row'>
            <div class='col-sm-2'>
              <div class='checkbox'>
                <label>
                    <input type=radio name='frequencytype' class='form-control' value='minute' <?php echo ($minutetype?'checked':''); ?>> By Minute
                </label>
              </div>
            </div>
            <div class='col-sm-10'>Run every <?php echo make_number('everyminutes',$everyminutes) ?> minutes(s)</div>
          </div>
          
          <div class='row'>
            <div class='col-sm-2'>
                <div class='checkbox'>
                    <label>
                        <input type=radio name='frequencytype' class='form-control' value='daily' <?php echo ($dailytype?'checked':''); ?>> By Day
                    </label>
                </div>
            </div>
            <div class='col-sm-4'>Run every <?php echo make_number('dailyday',$dailyday); ?> day(s) at</div>
            <div class='col-sm-3'>
              <select name='dailyhours' id='dailyhours' class='form-control'  autocomplete='off'>
                <option value='0' <?php if($dailyhour=='0')print "selected" ?>>0</option>
                <option value='1' <?php if($dailyhour=='1')print "selected" ?>>1</option>
                <option value='2' <?php if($dailyhour=='2')print "selected" ?>>2</option>
                <option value='3' <?php if($dailyhour=='3')print "selected" ?>>3</option>
                <option value='4' <?php if($dailyhour=='4')print "selected" ?>>4</option>
                <option value='5' <?php if($dailyhour=='5')print "selected" ?>>5</option>
                <option value='6' <?php if($dailyhour=='6')print "selected" ?>>6</option>
                <option value='7' <?php if($dailyhour=='7')print "selected" ?>>7</option>
                <option value='8' <?php if($dailyhour=='8')print "selected" ?>>8</option>
                <option value='9' <?php if($dailyhour=='9')print "selected" ?>>9</option>
                <option value='10' <?php if($dailyhour=='10')print "selected" ?>>10</option>
                <option value='11' <?php if($dailyhour=='11')print "selected" ?>>11</option>
                <option value='12' <?php if($dailyhour=='12')print "selected" ?>>12</option>
                <option value='13' <?php if($dailyhour=='13')print "selected" ?>>13</option>
                <option value='14' <?php if($dailyhour=='14')print "selected" ?>>14</option>
                <option value='15' <?php if($dailyhour=='15')print "selected" ?>>15</option>
                <option value='16' <?php if($dailyhour=='16')print "selected" ?>>16</option>
                <option value='17' <?php if($dailyhour=='17')print "selected" ?>>17</option>
                <option value='18' <?php if($dailyhour=='18')print "selected" ?>>18</option>
                <option value='19' <?php if($dailyhour=='19')print "selected" ?>>19</option>
                <option value='20' <?php if($dailyhour=='20')print "selected" ?>>20</option>
                <option value='21' <?php if($dailyhour=='21')print "selected" ?>>21</option>
                <option value='22' <?php if($dailyhour=='22')print "selected" ?>>22</option>
                <option value='23' <?php if($dailyhour=='23')print "selected" ?>>23</option>
                <option value='24' <?php if($dailyhour=='24')print "selected" ?>>24</option>
              </select>
            </div>
            <div class='col-sm-3'>
              <select name='dailyminutes' id='dailyminutes' class='form-control'  autocomplete='off' >
                <option value='00' <?php if($dailyminute=='00')print "selected" ?>>00</option>
                <option value='05' <?php if($dailyminute=='05')print "selected" ?>>05</option>
                <option value='10' <?php if($dailyminute=='10')print "selected" ?>>10</option>
                <option value='15' <?php if($dailyminute=='15')print "selected" ?>>15</option>
                <option value='20' <?php if($dailyminute=='20')print "selected" ?>>20</option>
                <option value='25' <?php if($dailyminute=='25')print "selected" ?>>25</option>
                <option value='30' <?php if($dailyminute=='30')print "selected" ?>>30</option>
                <option value='35' <?php if($dailyminute=='35')print "selected" ?>>35</option>
                <option value='40' <?php if($dailyminute=='40')print "selected" ?>>40</option>
                <option value='45' <?php if($dailyminute=='45')print "selected" ?>>45</option>
                <option value='50' <?php if($dailyminute=='50')print "selected" ?>>50</option>
                <option value='55' <?php if($dailyminute=='55')print "selected" ?>>55</option>
              </select>
            </div>
          </div>
          
          <div class='row'>
            <div class='col-sm-3'>
                <div class='checkbox'>
                    <label>
                        <input type=radio name='frequencytype' class='form-control' value='weekly' <?php echo ($weeklytype?'checked':''); ?>>
                        Run weekly<br>(choose days below)
                    </label>
                </div>
            </div>
            <div class='col-sm-3'>
               <div class='checkbox'>
                <label>
                    <input type="checkbox" id="mon" name="mon" <?php echo ($mon?'checked':''); ?>>Monday
                </label>
               </div>
               <div class='checkbox'>
                <label>
                    <input type="checkbox" id="tue" name="tue" <?php echo ($tue?'checked':''); ?>>Tuesday
                </label>
               </div>
               <div class='checkbox'>
                <label>
                    <input type="checkbox" id="wed" name="wed" <?php echo ($wed?'checked':''); ?>>Wednesday
                </label>
               </div>
               <div class='checkbox'>
                <label>
                    <input type="checkbox" id="thu" name="thu" <?php echo ($thu?'checked':''); ?>>Thursday
                </label>
               </div>
               <div class='checkbox'>
                <label>
                    <input type="checkbox" id="fri" name="fri" <?php echo ($fri?'checked':''); ?>>Friday
                </label>
               </div>
               <div class='checkbox'>
                <label>
                    <input type="checkbox" id="sat" name="sat" <?php echo ($sat?'checked':''); ?>>Saturday
                </label>
               </div>
               <div class='checkbox'>
                <label>
                    <input type="checkbox" id="sun" name="sun" <?php echo ($sun?'checked':''); ?>>Sunday
                </label>
               </div>
            </div>
            <div class='col-sm-3'>
                <select name='weeklyhours' id='weeklyhours' class='form-control'  autocomplete='off'>
                    <option value='0' <?php if($weeklyhour=='0')print "selected" ?>>0</option>
                    <option value='1' <?php if($weeklyhour=='1')print "selected" ?>>1</option>
                    <option value='2' <?php if($weeklyhour=='2')print "selected" ?>>2</option>
                    <option value='3' <?php if($weeklyhour=='3')print "selected" ?>>3</option>
                    <option value='4' <?php if($weeklyhour=='4')print "selected" ?>>4</option>
                    <option value='5' <?php if($weeklyhour=='5')print "selected" ?>>5</option>
                    <option value='6' <?php if($weeklyhour=='6')print "selected" ?>>6</option>
                    <option value='7' <?php if($weeklyhour=='7')print "selected" ?>>7</option>
                    <option value='8' <?php if($weeklyhour=='8')print "selected" ?>>8</option>
                    <option value='9' <?php if($weeklyhour=='9')print "selected" ?>>9</option>
                    <option value='10' <?php if($weeklyhour=='10')print "selected" ?>>10</option>
                    <option value='11' <?php if($weeklyhour=='11')print "selected" ?>>11</option>
                    <option value='12' <?php if($weeklyhour=='12')print "selected" ?>>12</option>
                    <option value='13' <?php if($weeklyhour=='13')print "selected" ?>>13</option>
                    <option value='14' <?php if($weeklyhour=='14')print "selected" ?>>14</option>
                    <option value='15' <?php if($weeklyhour=='15')print "selected" ?>>15</option>
                    <option value='16' <?php if($weeklyhour=='16')print "selected" ?>>16</option>
                    <option value='17' <?php if($weeklyhour=='17')print "selected" ?>>17</option>
                    <option value='18' <?php if($weeklyhour=='18')print "selected" ?>>18</option>
                    <option value='19' <?php if($weeklyhour=='19')print "selected" ?>>19</option>
                    <option value='20' <?php if($weeklyhour=='20')print "selected" ?>>20</option>
                    <option value='21' <?php if($weeklyhour=='21')print "selected" ?>>21</option>
                    <option value='22' <?php if($weeklyhour=='22')print "selected" ?>>22</option>
                    <option value='23' <?php if($weeklyhour=='23')print "selected" ?>>23</option>
                    <option value='24' <?php if($weeklyhour=='24')print "selected" ?>>24</option>
                  </select>
            </div>
            <div class='col-sm-3'>
                  <select name='weeklyminutes' id='weeklyminutes' class='form-control'  autocomplete='off' >
                    <option value='00' <?php if($weeklyminute=='00')print "selected" ?>>00</option>
                    <option value='05' <?php if($weeklyminute=='05')print "selected" ?>>05</option>
                    <option value='10' <?php if($weeklyminute=='10')print "selected" ?>>10</option>
                    <option value='15' <?php if($weeklyminute=='15')print "selected" ?>>15</option>
                    <option value='20' <?php if($weeklyminute=='20')print "selected" ?>>20</option>
                    <option value='25' <?php if($weeklyminute=='25')print "selected" ?>>25</option>
                    <option value='30' <?php if($weeklyminute=='30')print "selected" ?>>30</option>
                    <option value='35' <?php if($weeklyminute=='35')print "selected" ?>>35</option>
                    <option value='40' <?php if($weeklyminute=='40')print "selected" ?>>40</option>
                    <option value='45' <?php if($weeklyminute=='45')print "selected" ?>>45</option>
                    <option value='50' <?php if($weeklyminute=='50')print "selected" ?>>50</option>
                    <option value='55' <?php if($weeklyminute=='55')print "selected" ?>>55</option>
                  </select>
            </div>
          
                
        </div>
        <div class='row'>
            <div class='col-sm-3'>
                <div class='checkbox'>
                    <label>
                        <input type=radio name='frequencytype' class='form-control' value='monthly' <?php echo ($monthlytype?'checked':''); ?>>
                        Run Monthly
                    </label>
                </div>
            </div>
            <div class='col-sm-3'>
                Run once each month on day <?php echo make_number('monthlyday',$monthlyday); ?> of the month at 
            </div>
                <div class='col-sm-3'>
                    <select name='monthlyhours' id='monthlyhours' class='form-control'  autocomplete='off'>
                        <option value='0' <?php if($monthlyhour=='0')print "selected" ?>>0</option>
                        <option value='1' <?php if($monthlyhour=='1')print "selected" ?>>1</option>
                        <option value='2' <?php if($monthlyhour=='2')print "selected" ?>>2</option>
                        <option value='3' <?php if($monthlyhour=='3')print "selected" ?>>3</option>
                        <option value='4' <?php if($monthlyhour=='4')print "selected" ?>>4</option>
                        <option value='5' <?php if($monthlyhour=='5')print "selected" ?>>5</option>
                        <option value='6' <?php if($monthlyhour=='6')print "selected" ?>>6</option>
                        <option value='7' <?php if($monthlyhour=='7')print "selected" ?>>7</option>
                        <option value='8' <?php if($monthlyhour=='8')print "selected" ?>>8</option>
                        <option value='9' <?php if($monthlyhour=='9')print "selected" ?>>9</option>
                        <option value='10' <?php if($monthlyhour=='10')print "selected" ?>>10</option>
                        <option value='11' <?php if($monthlyhour=='11')print "selected" ?>>11</option>
                        <option value='12' <?php if($monthlyhour=='12')print "selected" ?>>12</option>
                        <option value='13' <?php if($monthlyhour=='13')print "selected" ?>>13</option>
                        <option value='14' <?php if($monthlyhour=='14')print "selected" ?>>14</option>
                        <option value='15' <?php if($monthlyhour=='15')print "selected" ?>>15</option>
                        <option value='16' <?php if($monthlyhour=='16')print "selected" ?>>16</option>
                        <option value='17' <?php if($monthlyhour=='17')print "selected" ?>>17</option>
                        <option value='18' <?php if($monthlyhour=='18')print "selected" ?>>18</option>
                        <option value='19' <?php if($monthlyhour=='19')print "selected" ?>>19</option>
                        <option value='20' <?php if($monthlyhour=='20')print "selected" ?>>20</option>
                        <option value='21' <?php if($monthlyhour=='21')print "selected" ?>>21</option>
                        <option value='22' <?php if($monthlyhour=='22')print "selected" ?>>22</option>
                        <option value='23' <?php if($monthlyhour=='23')print "selected" ?>>23</option>
                        <option value='24' <?php if($monthlyhour=='24')print "selected" ?>>24</option>
                      </select>
                </div>
                <div class='col-sm-3'>
                   <select name='monthlyminutes' id='monthlyminutes' class='form-control'  autocomplete='off' >
                    <option value='00' <?php if($monthlyminute=='00')print "selected" ?>>00</option>
                    <option value='05' <?php if($monthlyminute=='05')print "selected" ?>>05</option>
                    <option value='10' <?php if($monthlyminute=='10')print "selected" ?>>10</option>
                    <option value='15' <?php if($monthlyminute=='15')print "selected" ?>>15</option>
                    <option value='20' <?php if($monthlyminute=='20')print "selected" ?>>20</option>
                    <option value='25' <?php if($monthlyminute=='25')print "selected" ?>>25</option>
                    <option value='30' <?php if($monthlyminute=='30')print "selected" ?>>30</option>
                    <option value='35' <?php if($monthlyminute=='35')print "selected" ?>>35</option>
                    <option value='40' <?php if($monthlyminute=='40')print "selected" ?>>40</option>
                    <option value='45' <?php if($monthlyminute=='45')print "selected" ?>>45</option>
                    <option value='50' <?php if($monthlyminute=='50')print "selected" ?>>50</option>
                    <option value='55' <?php if($monthlyminute=='55')print "selected" ?>>55</option>
                  </select>
                </div>  
            </div> 
        </div>
    </div>
    <?php
    make_submit('submit',$button,'Save job');
    make_hidden('cronid',$cronid);
    print "</form>\n";
  } else {
    $sql="SELECT cronLastCall, cronSystemEnabled FROM core_preferences";
    $dbLastCall=dbselectsingle($sql);
    $lastCall=$dbLastCall['data']['cronLastCall'];
    $systemEnabled=$dbLastCall['data']['cronSystemEnabled'];
    if($systemEnabled){$system="Cron system is enabled";}else{$system="Cron system has been disabled!";}
    
    
    $sql="SELECT * FROM core_cronexecution WHERE exectime<'$currenttimestamp' AND (status=0 OR status=2)";
    //print $sql."<br>";
    $root=$GLOBALS['systemRootPath'];

    $dbCronJobsCount=dbselectmulti($sql);
    $system.="<br />Need to run a total of $dbCronJobsCount[numrows] jobs";
    
    $sql="SELECT * FROM core_cron";
    $dbCronJobs=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Create new cron job</a>,<a href='?action=clearpending'>Clear all pending jobs</a>,<hr>,<b>$system</b>,System last ran at $lastCall","Cron job name,Last Successful Run,Next Scheduled Run",7);
    if ($dbCronJobs['numrows']>0){
        foreach ($dbCronJobs['data'] as $record) {
            $title=stripslashes($record['title']);
            $id=$record['id'];
            //get the last run and next scheduled run for this job
            $sql="SELECT MIN(exectime) as jtime FROM core_cronexecution WHERE status=0 AND cronid=$id";
            $dbNext=dbselectsingle($sql);
            $nexttime=$dbNext['data']['jtime'];
            $sql="SELECT MAX(exectime) as jtime FROM core_cronexecution WHERE status=1 AND cronid=$id";
            $dbLast=dbselectsingle($sql);
            $lasttime=$dbLast['data']['jtime'];
            
            print "<tr><td>$title</td><td>$lasttime</td><td>$nexttime</td>";
            print "
            <td>
                <div class='btn-group'>
                  <a href='?action=edit&cronid=$id' class='btn btn-dark'>Edit</a>
                  <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    <span class='caret'></span>
                    <span class='sr-only'>Toggle Dropdown</span>
                  </button>
                  <ul class='dropdown-menu'>
                    <li><a href='?action=run&cronid=$id'>Run job</a></li>
                    <li><a href='?action=view&cronid=$id'>View Log</a></li>
                    <li><a href='?action=delete&cronid=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
                  </ul>
                </div>
            </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbCronJobs);
  }
    
    
}

  
function save_cron($action) {
    global $scriptpath;
    $id=$_POST['cronid'];
    $description=addslashes($_POST['description']);
    $title=addslashes($_POST['title']);
    $dailytime=($GLOBALS['dailyhours'][$_POST['dailyhours']].":".$GLOBALS['dailyminutes'][$_POST['dailyminutes']]);
    $weeklytime=($GLOBALS['weeklyhours'][$_POST['weeklyhours']].":".$GLOBALS['weeklyminutes'][$_POST['weeklyminutes']]);
    $monthlytime=($GLOBALS['monthlyhours'][$_POST['monthlyhours']].":".$GLOBALS['monthlyminutes'][$_POST['monthlyminutes']]);
    $dailyday=addslashes($_POST['dailyday']);
    if ($dailyday==''){$dailyday=0;}
    $monthlyday=addslashes($_POST['monthlyday']);
    if ($monthlyday==''){$monthlyday=0;}
    $everyminutes=addslashes($_POST['everyminutes']);
    if ($everyminutes==''){$everyminutes=0;}
    if ($dailytime==':'){$dailytime="0:00";}
    if ($weeklytime==':'){$weeklytime="0:00";}
    if ($monthlytime==':'){$monthlytime="0:00";}
    $frequencytype=addslashes($_POST['frequencytype']);
    if ($frequencytype=='minute'){$frequencytype=1;}
    if ($frequencytype=='daily'){$frequencytype=2;}
    if ($frequencytype=='weekly'){$frequencytype=3;}
    if ($frequencytype=='monthly'){$frequencytype=4;}
    if($_POST['mon']){$mon=1;}else{$mon=0;}
    if($_POST['tue']){$tue=1;}else{$tue=0;}
    if($_POST['wed']){$wed=1;}else{$wed=0;}
    if($_POST['thu']){$thu=1;}else{$thu=0;}
    if($_POST['fri']){$fri=1;}else{$fri=0;}
    if($_POST['sat']){$sat=1;}else{$sat=0;}
    if($_POST['sun']){$sun=1;}else{$sun=0;}
    $startdate=$_POST['startdate'];
    $script=addslashes($_POST['script']);
    $function=addslashes($_POST['function']);
    $params=addslashes($_POST['params']);
    $date_offset=addslashes($_POST['date_offset']);
    if($_POST['enabled']){$enabled=1;}else{$enabled=0;}
    if ($action=='insert'){
       $sql="INSERT INTO core_cron (date_offset, description, title, enabled, frequencytype, mon, tue, wed, thu, fri, sat, 
       sun, script, dailyday, dailytime, weeklytime, monthlyday, monthlytime, everyminutes, startdate, function, params) 
       VALUES('$date_offset', '$description', '$title', $enabled, $frequencytype, $mon, $tue, $wed, $thu, $fri, $sat, $sun, '$script', $dailyday, '$dailytime', 
       '$weeklytime', '$monthlyday', '$monthlytime', $everyminutes, '$startdate', '$function', '$params')";
       $dbresult=dbinsertquery($sql);
    } else {
       $sql="UPDATE core_cron SET date_offset='$date_offset', description='$description', title='$title', enabled=$enabled, params='$params', function='$function',
       script='$script', frequencytype=$frequencytype, mon=$mon, tue=$tue, wed=$wed, thu=$thu,
       fri=$fri, sat=$sat, sun=$sun, dailyday=$dailyday, dailytime='$dailytime', weeklytime='$weeklytime', monthlyday='$monthlyday',
       monthlytime='$monthlytime', everyminutes=$everyminutes, startdate='$startdate' WHERE id=$id";
       $dbresult=dbexecutequery($sql);
   }
   $error=$dbresult['error'];
   if ($error!='')
    {
        setUserMessage('There was a problem saving the cron job.<br>'.$error,'error');
    } else {
        setUserMessage('Cron job has been successfully saved','success');
    }
    redirect("?action=list");
    
}

$Page->footer();   
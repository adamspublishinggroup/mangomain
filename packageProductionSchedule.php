<?php
  include("includes/boot.php");

  global $pubs, $Prefs;
  $packageID = intval($_GET['packageid']);
  
  $sql="SELECT * from jobs_inserter_packages WHERE id=$packageID";
  $dbMainPackage = dbselectsingle($sql);
  $mainPackage = $dbMainPackage['data'];
  
  //get all hoppers/stations for the specified inserter
  $sql="SELECT * FROM inserters_hoppers WHERE inserter_id=$mainPackage[inserter_id] ORDER BY hopper_number ASC";
  $dbStations = dbselectmulti($sql);
  if($dbStations['numrows']==0)
  {
      $Page->throwError('It appears as if the inserter has not been configured as of yet.');
  }
  
  
  //get viable zones for the publication and day of week
  $dow=date("w",strtotime($mainPackage['pub_date']));
  $sql="SELECT * FROM publications_insertzones WHERE pub_id=$mainPackage[pub_id] AND dow_$dow ORDER BY zone_order";
  $dbZones = dbselectmulti($sql);
  
  
  
  print "<div class='page-header'>
      <h1>Production Schedule  <small>for ".$pubs[$mainPackage['pub_id']]." ".date("m/d/Y",strtotime($mainPackage['pub_date']))." schedule run time: ".date("m/d/Y H:i",strtotime($mainPackage['package_startdatetime']))."</small></h1>
    </div>";

  print "<div class='row'>\n";
      print "<div class='col-xs-12 col-md-10'>\n";  
          print "<table class='table table-striped table-bordered'>\n";
          print "<tr><th>Station</th><th>Insert Name</th>";
          if($dbZones['numrows']>0)
          {
            foreach($dbZones['data'] as $zone)
            {
                print "<th>".$zone['zone_label']."</th>";    
            }
              
          }
          print "</tr>\n";
          $pcount = array();
          $stationsUsed = array();
          //now the inserts themselves
          //we're going to iterate over the stations, and find the matching insert
          foreach($dbStations['data'] as $station)
          {
              $found=false;
              print "<tr>";
              print "<td>".$station['hopper_label']."</td>";
              
              $sql="SELECT * FROM jobs_packages_inserts WHERE package_id=$packageID AND hopper_id=$station[id]";
              $dbPI=dbselectsingle($sql);
              if($dbPI['numrows']>0)
              {
                  $found=true;
                  $insertID=$dbPI['data']['insert_id'];
                  $insertType = $dbPI['data']['insert_type'];
                  $insertZones = array();
                  if($insertType=='insert')
                  {
                      //now get the inserts
                      $sql="SELECT A.*, B.account_name 
                      FROM inserts A, accounts B 
                      WHERE A.advertiser_id = B.id AND A.id = $insertID";
                      $dbInsert = dbselectsingle($sql);
                      print $dbInsert['error'];
                      $insert=$dbInsert['data'];
                      
                      print "<td>".$insert['account_name']."<br>".$insert['insert_tagline']."</td>";
                      //now the zoning
                      $sql = "SELECT B.* FROM inserts_schedule A, inserts_zoning B 
                      WHERE A.id=B.schedule_id AND A.insert_id = $insertID
                      AND A.pub_id = $mainPackage[pub_id] AND A.insert_date = '$mainPackage[pub_date]'";
                      $dbInsertZones = dbselectmulti($sql);
                      if($dbInsertZones['numrows']>0)
                      {
                          foreach($dbInsertZones['data'] as $iz)
                          {
                              $insertZones[]=$iz['zone_id'];
                          }
                      }
                      if($dbZones['numrows']>0)
                      {
                          foreach($dbZones['data'] as $zone)
                          {
                              //we need to see if zone id is in $insert['zones']
                              if(in_array($zone['id'],$insertZones))
                              {
                                  print "<td><i class='fa fa-check text-success'></i></td>";
                                  $pcount[$zone['id']]=$pcount[$zone['id']]+$insert['pages'];
                                  $stationsUsed[$zone['id']][]=$station['hopper_label'];
                              } else {
                                  print "<td></td>";
                              }
                          }
                      }
                      
                      
                  } elseif($insertType=='package')
                  {
                      //get any packages inserting back in
                      //now get the inserts
                      $sql="SELECT * FROM jobs_inserter_packages WHERE id=$insertID";
                      $dbPackage = dbselectsingle($sql);
                      $package=$dbPackage['data'];
                      print "<td><b>Package: ".$package['package_name']."</td>";
                      if($dbZones['numrows']>0)
                      {
                          foreach($dbZones['data'] as $zone)
                          {
                              //we need to look at all the inserts that are in this package, we'll need to see if at least one of them is in this zone
                              //if so we'll add the 
                              $sql = "SELECT B.* FROM inserts_schedule A, inserts_zoning B, jobs_packages_inserts C 
                              WHERE C.package_id=$insertID AND A.id=B.schedule_id AND A.insert_id = C.insert_id
                              AND A.pub_id = $mainPackage[pub_id] AND B.zone_id=$zone[id] AND A.insert_date = '$mainPackage[pub_date]'";
                              $dbInsertZones = dbselectsingle($sql);
                              if($dbInsertZones['numrows']>0)
                              {
                                  print "<td><i class='fa fa-check text-success'></i></td>";
                                  $pcount[$zone['id']]=$pcount[$zone['id']]+($package['standard_pages']);
                                  $stationsUsed[$zone['id']][]=$station['hopper_label'];
                              } else {
                                  print "<td></td>";
                              } 
                              
                          }
                      }
                  } elseif($insertType=='sticky')
                  {
                      $specialInstructions.="Sticky note ID $insertID found";
                  }
                  
              } else {
                  $found = false;
              }
              
              //now find the insert that goes with it
              foreach($dbInserts['data'] as $insert)
              {
                  if($insert['hopper_id']==$station['id'])
                  {
                      $found=true;
                      print "<td>".$insert['account_name']."<br>".$insert['insert_tagline']."</td>";
                      //now the zoning
                      if($dbZones['numrows']>0)
                      {
                          foreach($dbZones['data'] as $zone)
                          {
                              //we need to see if zone id is in $insert['zones']
                              if(in_array($zone['id'],$insert['zones']))
                              {
                                  print "<td><i class='fa fa-check text-success'></i></td>";
                              } else {
                                  print "<td></td>";
                              }
                          }
                      }
                      
                  } 
              }
              if(!$found)
              {
                  print "<td></td><td></td>";
                  if($dbZones['numrows']>0)
                  {
                      foreach($dbZones['data'] as $zone)
                      {
                          print "<td></td>";
                      }
                  }
              }
              print "</tr>\n";    
          }
          print "<tr><td></td><td>Page Count</td>";
          foreach($dbZones['data'] as $zone)
          {
              print "<td>".$pcount[$zone['id']]."</td>";
          }
          print "</tr>\n";
          print "</table>\n";  

      print "</div>\n";
      
      print "<div class='col-xs-12 col-md-2'>\n";
          print "<h4>Special Instructions</h4>\n";
          print "<hr>";
          print "<h4>Bundle Size</h4>\n";
          
          //calculate the optimum bundle size by zone. If there is an outlier, we'll handle it separately
          $bundle['Regular']=0;
          foreach($dbZones['data'] as $zone)
          {
              if($mainPackage['bundle_size']!=0)
              {
                $size = round(($mainPackage['bundle_size'] / $pcount[$zone['id']]),0);
              } else {
                $size = round(($Prefs->maxStackerPageCount / $pcount[$zone['id']]),0);
              }
              //look at the first digit
              $firstDigit = substr($size,strlen($size)-1,1);
              $otherDigits = substr($size,0,strlen($size)-1);
              if($firstDigit<5){$firstDigit = 0;}else{$firstDigit=0;$otherDigits+=1;}
              $newSize = $otherDigits.$firstDigit;
              if($newSize>$bundle['Regular'] && $bundle['Regular']==0){$bundle['Regular']=$newSize;}
              if($newSize>$bundle['Regular']*1.2){$bundle[$zone['zone_name']]=$newSize;}
                                                                                             
          }
          foreach($bundle as $type=>$size)
          {
              print "<h4>$type</h4><h2>$size</h2>";
          }
      print "</div>\n";
  
  print "</div>\n";
  
  print "<div class='row'>\n";
  
  //grab publication draw for this day
  $sql="SELECT zone_id, draw FROM publications_draws WHERE pub_id=$mainPackage[pub_id] AND pub_date = '$mainPackage[pub_date]'";
  $dbDraw = dbselectmulti($sql);
  if($dbDraw['numrows']>0)
  {
      foreach($dbDraw['data'] as $d)
      {
          $draws[$d['zone_id']]=$d['draw'];
      }
  } else {
      $draws=array();
  }
      print "<div class='col-xs-12'>\n";  
          print "<table class='table table-striped table-bordered'>\n";
          $i=65;
          print "<tr><th>Order</th><th>Zone</th><th style='width:80%'>Stations Running</th><th>Zone Total</th></tr>\n";
          if($dbZones['numrows']>0)
          {
              foreach($dbZones['data'] as $zone)
              {
                  print "<tr><td>".strtoupper(chr($i))."</td>";
                  print "<td>".$zone['zone_label']."</td>";
                  
                  print "<td>";
                      //now loop through all stations
                  foreach($dbStations['data'] as $station)
                  {
                      print "<div style='float:left;width:30px;text-align:center;'>\n";
                      if(in_array($station['hopper_label'],$stationsUsed[$zone['id']]))
                      {
                          print $station['hopper_label'];
                      } else {
                          print "-";
                      }
                      print "</div>";
                  }
                  print "</td>";
                  //now we need to show the zone total
                  print "<td>";
                  print $draws[$zone['id']];
                  $total +=$draws[$zone['id']];
                  print "</td>";
                  print "</tr>\n";
                  $i++;
              }
          }
          print "<tr><th></th><th></th><th style='text-align:right;'><b>GRAND TOTAL</b></th><th><b>$total</b></th></tr>\n";
          print "</table>\n";  

      print "</div>\n";
  print "</div>\n";
 
 
  
  
  
  $Page->footer();
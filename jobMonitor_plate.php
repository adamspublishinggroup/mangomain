<?php
$pageFluid = true;   
include("includes/boot.php");

$jobData = job_nav();

$layoutid=$jobData['job']['layout_id'];
$pubname=$jobData['pub']['pub_name'];
$pubdate=$jobData['job']['pub_date'];
$pubday=strtolower(date("l",strtotime($jobData['job']['pub_date'])));
$jobname=$jobData['run']['run_name'];
$draw=$jobData['job']['draw'];
$jobid=$jobData['job']['id'];
$runid=$jobData['job']['run_id'];
$platenotes=stripslashes($jobData['job']['plate_notes']);

$sql="SELECT * FROM jobs_sections WHERE job_id=$jobid";
$jobsections=dbselectsingle($sql);
$jsections=$jobsections['data'];

    
print "<div id='plateside' class='col-xs-12 col-sm-9'>";
    plateData($jobid);


    print "<div class='row'>";
        print "<table class='table table-bordered'>\n";
        $colors=array("black","cyan","magenta","yellow");
        for ($i=1;$i<=3;$i++)
        {
            $sectioncode="section".$i."_code";
            $sectionname="section".$i."_name";
            $sectionlow=$jsections["section".$i."_lowpage"];
            $sectionhigh=$jsections["section".$i."_highpage"];
            $sectionused=$jsections["section".$i."_used"];
            if ($sectionused==1)
            {
                //now get the plates
                $sql="SELECT * FROM job_plates WHERE job_id=$jobid AND section_code='$jsections[$sectioncode]' ORDER BY low_page ASC, version DESC";
                $dbPlates=dbselectmulti($sql);
                if ($dbPlates['numrows']>0)
                {
                    print "<tr id='plate$pid'>\n";
                    print "<td><b>Section $jsections[$sectionname]</b></td>
                    <td><b>Plate Times</b></td>
                    </tr>\n";
                    foreach ($dbPlates['data'] as $plate)
                    {
                        $detail="";
                        $releasetime="";
                        $receivek='';
                        $receivec='';
                        $receivem='';
                        $receivey='';
                        $approveall='';
                        $approvek='';
                        $approvec='';
                        $approvem='';
                        $approvey='';
                        $receiveall='';
                        if ($plate['plate_approval']!='')
                        {
                            $releasetime=date("H:i:s",strtotime($plate['plate_approval']));
                            $detail.=$releasetime." - plate approved<br>\n";
                        }
                        foreach($colors as $key=>$color)
                        {
                            $receive=$color."_received";
                            $approval=$color."_approval";
                            $ctp=$color."_ctp";
                            if ($plate[$receive]!='')
                            {
                                if($color=="black"){$receivek=date("H:i:s",strtotime($plate[$receive]));}
                                if($color=="cyan"){$receivec=date("H:i:s",strtotime($plate[$receive]));}
                                if($color=="magenta"){$receivem=date("H:i:s",strtotime($plate[$receive]));}
                                if($color=="yellow"){$receivey=date("H:i:s",strtotime($plate[$receive]));}
                                $detail.=date("D, M jS Y \@ H:i:s",strtotime($plate[$receive]))." - $color plate out of bender<br>\n";
                            }
                            if ($plate[$approval]!='')
                            {
                                if($color=="black"){$approvek=date("H:i:s",strtotime($plate[$approval]));}
                                if($color=="cyan"){$approvec=date("H:i:s",strtotime($plate[$approval]));}
                                if($color=="magenta"){$approvem=date("H:i:s",strtotime($plate[$approval]));}
                                if($color=="yellow"){$approvey=date("H:i:s",strtotime($plate[$approval]));}
                                $detail.=date("D, M jS Y \@ H:i:s",strtotime($plate[$approval]))." - $color plate waiting for approval<br>\n";
                            }
                            if ($plate[$ctp]!='')
                            {
                                $detail.=date("D, M jS Y \@ H:i:s",strtotime($plate[$ctp]))." - $color plate delivered to platesetter<br>\n";;
                            }
                        }
                        if ($receivek!='' && $receivec!='' && $receivem!='' && $receivey!=''){$receiveall=$receivek;}
                        if ($approvek!='' && $approvec!='' && $approvem!='' && $approvey!=''){$approveall=$approvek;}
                            
                        if ($detail==''){$detail="No details at this time.<br>";}else{$detail.="<br>\n";}
                        $displayed=false;
                        $pid=$plate['id'];
                        $plateversion=$plate['version'];
                        $platenumber=$plate['low_page'];
                        
                        //get all the pages that are on this plate
                        $sql="SELECT DISTINCT(page_number), color FROM job_pages WHERE plate_id=$pid AND page_number<>0 ORDER BY page_number ASC";
                        $dbPlatePages=dbselectmulti($sql);
                        $platecolor='Black';
                        if ($dbPlatePages['numrows']>0)
                        {
                            $ppages="";
                            foreach ($dbPlatePages['data'] as $platepage)
                            {
                                $ppages.=" ".$platepage['page_number'];
                                if($platepage['color']){$platecolor='Full Color';}
                            }
                        }   
                        
                        /*
                        $platek="<input type='text' id='plateapprovek_$pid' name='plateapprovek_$pid' value='$approvek' style='width:60px'><script type='text/javascript'>\$('#plateapprovek_$pid').timepicker({onClose: function(dateText, inst){setPaginationTime('$pid','plateapprovek',dateText);}});</script>";
                        $platek.="<img src='artwork/approveCheckK.png' width=20 style='padding-top:2px;' onclick=\"setPaginationTime('$pid','plateapprovek','now');\" alt='Approve Black Plate' title='Click to receive the black for this plate.'>\n";
                        $platec="<input type='text' id='plateapprovec_$pid' name='plateapprovec_$pid' value='$approvec' style='width:60px'><script type='text/javascript'>\$('#plateapprovec_$pid').timepicker({onClose: function(dateText, inst){setPaginationTime('$pid','plateapprovec',dateText);}});</script>";
                        $platec.="<img src='artwork/approveCheckC.png' width=20 style='padding-top:2px;' onclick=\"setPaginationTime('$pid','plateapprovec','now');\" alt='Approve Cyan Plate' title='Click to receive the cyan for this plate.'>\n";
                        $platem="<input type='text' id='plateapprovem_$pid' name='plateapprovem_$pid' value='$approvem' style='width:60px'><script type='text/javascript'>\$('#plateapprovem_$pid').timepicker({onClose: function(dateText, inst){setPaginationTime('$pid','plateapprovem',dateText);}});</script>";
                        $platem.="<img src='artwork/approveCheckM.png' width=20 style='padding-top:2px;' onclick=\"setPaginationTime('$pid','plateapprovem','now');\" alt='Approve Magenta Plate' title='Click to receive the magenta for this plate.'>\n";
                        $platey="<input type='text' id='plateapprovey_$pid' name='plateapprovey_$pid' value='$approvey' style='width:60px'><script type='text/javascript'>\$('#plateapprovey_$pid').timepicker({onClose: function(dateText, inst){setPaginationTime('$pid','plateapprovey',dateText);}});</script>";
                        $platey.="<img src='artwork/approveCheckY.png' width=20 style='padding-top:2px;' onclick=\"setPaginationTime('$pid','plateapprovey','now');\" alt='Approve Yellow Plate' title='Click to receieve the yellow for this plate.'>\n";
                        $plateall="<input type='text' id='plateapproveally_$pid' name='plateapproveall_$pid' value='$approveall' style='width:60px'><script type='text/javascript'>\$('#plateapproveall_$pid').timepicker({onClose: function(dateText, inst){setPaginationTime('$pid','plateapproveall',dateText);}});</script>";
                        */
                        
                        print "
                        <tr id='page$pid'>\n";
                            print "<td>Plate $platenumber - ver. $plateversion $platecolor<br><small>Pages on plate: $ppages</small></td>";
                        print "<td>";    
                        print "<div class='row'>";
                        print "<span class='col-xs-12 col-sm-4'>";
                            addPlateTime($pid,$approvek,$color='K');
                        print "</span>";
                        print "<span class='col-xs-12 col-sm-4'>";
                            addPlateTime($pid,$approvec,$color='C');
                        print "</span>";
                        print "<span class='col-xs-12 col-sm-4'>";
                            addPlateTime($pid,$approvem,$color='M');
                        print "</span>";
                        print "<span class='col-xs-12 col-sm-4'>";
                            addPlateTime($pid,$approvey,$color='Y');
                        print "</span>";
                        print "<span class='col-xs-12 col-sm-4'>";
                            addPlateTime($pid,$approveall,$color='all');
                        print "</span>";
                        print "<span class='col-xs-12 col-sm-4'>";
                        print "<a href='#' onClick=\"viewPlateDetails($pid);\" class='btn btn-default'>View Details</a>
                        </span>";
                        print "</div>";        
                        print "<div id='plateDetails$pid' style='display:none;margin-left:20px;font-size:10px;border:thin solid black;padding:4px;'>$detail</div>\n";
                        print "</td>\n";
                        print "</tr>\n";
                    }
                    
                }

            } 

        }
        print "</table>\n";

    print "</div><!-- close row -->\n";
print "</div><!-- close left column -->\n";

print "<div id='rightrail' class='col-xs-12 col-sm-3'>\n";


 


 print "<div id='deadlines'>\n";
       print "<p style='font-weight:bold;font-size:14px;margin-left:10px;'>Deadlines and page flow:</p>\n";
       print "<div id='deadlinedata'>\n";
           $scheduledstart=$jobData['job']['startdatetime'];
           $colorlead=$jobData['run']['last_colorpage_leadtime'];
           $pagelead=$jobData['run']['last_page_leadtime'];
           $platelead=$jobData['run']['last_plate_leadtime'];
           $colordeadline=date("H:i",strtotime($scheduledstart."-$colorlead minutes"));     
           $pagedeadline=date("H:i",strtotime($scheduledstart."-$pagelead minutes"));     
           $platedeadline=date("H:i",strtotime($scheduledstart."-$platelead minutes"));     
           $fullcolordeadline=strtotime($scheduledstart."-$colorlead minutes");     
           $fullpagedeadline=strtotime($scheduledstart."-$pagelead minutes");     
           $fullplatedeadline=strtotime($scheduledstart."-$platelead minutes");     
           $now=time();
           //get number of plates still out
           $sql="SELECT COUNT(id) as platesout FROM job_plates WHERE job_id=$jobid AND plate_approval IS Null";
           $dbPlatesOut=dbselectsingle($sql);
           $platesout=$dbPlatesOut['data']['platesout'];         
           //get number of pages still out
           $sql="SELECT COUNT(id) as pagesout FROM job_pages WHERE job_id=$jobid AND page_release IS Null AND current=1";
           $dbPagesOut=dbselectsingle($sql);
           $pagesout=$dbPagesOut['data']['pagesout'];         
           //get number of color pages still out
           $sql="SELECT COUNT(id) as colorout FROM job_pages WHERE job_id=$jobid AND color=1 AND color_release IS Null AND current=1";
           $dbColorOut=dbselectsingle($sql);
           $colorout=$dbColorOut['data']['colorout'];         
           print "<li>".date("H:i")." current system time</li>\n";
           print "<li>".date("H:i",strtotime($scheduledstart))." Press start</li>\n";
           print "<li>$platedeadline Last plate release</li>\n";
           if ($now>$fullplatedeadline && $platesout>0)
           {
               print "<li style='font-weight:bold;color:red;'>Past last plate deadline with $platesout plates remaining</li>";
           } else {
               print "<li>$platesout plates remaining</li>";
           }
           print "<li>$pagedeadline Last page release</li>\n";
           if ($now>$fullpagedeadline && $pagesout>0)
           {
               print "<li style='font-weight:bold;color:red;'>Past last page deadline with $pagesout pages remaining</li>";
           } else {
               print "<li>$pagesout pages remaining</li>";
           }
           print "<li>$colordeadline Last color release</li>\n";
           if ($now>$fullcolordeadline && $colorout>0)
           {
               print "<li style='font-weight:bold;color:red;'>Past last color deadline with $colorout color pages remaining</li>";
           } else {
               print "<li>$colorout color pages remaining</li>";
           }
       print "</div>\n";
       
      print "<div style='margin-top:10px;'>\n";
    
        
        
        $sql="SELECT press_id, notes_press, notes_job FROM jobs WHERE id=$jobid";
        $dbJobInfo=dbselectsingle($sql);
        $jobinfo=$dbJobInfo['data'];
        print "<h3>Messages</h3>\n";
        $message=$GLOBALS['pressStartMessage'];
        if($jobData['job']['job_message']!='')
        {
            $message.="<li>".stripslashes($jobData['job']['job_message'])."</li>";
        }
        //look at the pub id and see if there is an associated run message
        $sql="SELECT pub_message FROM publications WHERE id=$pubid";
        $dbPubMessage=dbselectsingle($sql);
        $pubmessage=$dbPubMessage['data']['pub_message'];
        if($pubmessage!=''){$message.="<li>".stripslashes($pubmessage)."</li>";}
        
        //look at the run id and see if there is an associated run message
        $sql="SELECT run_message FROM publications_runs WHERE id=$runid";
        $dbRunMessage=dbselectsingle($sql);
        $runmessage=$dbRunMessage['data']['run_message'];
        if($runmessage!=''){$message.="<li>".stripslashes($runmessage)."</li>";}
        
        $message=str_replace(array("\r", "\n", "\t"), '<br>',$message);
        $message=addslashes($message);
        $message="<div style='font-size:16px;'>$message</div>";
        print "<h3>Notes</h3>\n";
        print stripslashes($jobinfo['notes_job']."\n".$jobinfo['notes_press'])."<br />";
          

      print "<p style='font-weight:bold;font-size:14px;margin-left:10px;'>Plate Notes:</p>\n";
       print "<textarea id='platenotes' rows=10 cols=25 />$platenotes</textarea><br />";
      print "<input type='button' onclick=\"saveMonitorNotes('plate');\" value='Save Notes' />";
      print "</div>\n";
 print "</div>\n";     
 
       print "<div id='pagesout' style='width:265px;'>\n";
          print "<fieldset>\n<legend>Pages Remaining</legend>\n";
          print "<div id='pageslist' style='padding:2px;height:200px;overflow-y:auto;border:1px solid black;background-color:white'>\n";
          pageslist($jobid);
           print "</div>\n";
          print "</fieldset>\n";
      print "</div>\n";
        
      print "<div id='remakes' style='width:265px;'>\n";
        print "<fieldset>\n<legend>Remakes</legend>\n";
        print "<div id='remakeslist' style='padding:2px;height:200px;overflow-y:auto;border:1px solid black;background-color:white'>\n";
        remakeslist($jobid);
        print "</div>\n";
        print "</fieldset>\n";
      print "</div>\n";
        
        
print "</div><!-- closing left column -->\n";


function remakeslist($jobid)
{
    $sql="SELECT DISTINCT(page_number), section_code, version, workflow_receive FROM job_pages WHERE job_id=$jobid AND version>1 ORDER BY page_number ASC, version DESC";
    $dbPages=dbselectmulti($sql);
    if ($dbPages['numrows']>0)
    {
        print "<table>\n";
        print "<tr><th>Section</th><th>Page</th><th>Version</th><th>Receive Time</th></tr>\n";
        foreach($dbPages['data'] as $page)
        {


            print "<tr>\n";
            print "<td>".$page['section_code']."</td>\n";
            print "<td>".$page['page_number']."</td>\n";
            print "<td>".$page['version']."</td>\n";
            print "<td>";
            if ($page['workflow_receive']!='')
            {
                print date("H:i:s",strtotime($page['workflow_receive']));
            } else {
                print "Not received";
            }
            print "</td>";
            print "</tr>\n";
        
        }
        print "</table>\n";
    } else {
        print "No remakes at this time.";
    }
}

function pageslist($jobid)
{
    $sql="SELECT DISTINCT(page_number), section_code, page_number, color FROM job_pages WHERE job_id=$jobid AND page_release is Null ORDER BY section_code ASC, page_number ASC";
    $dbPages=dbselectmulti($sql);
    if ($dbPages['numrows']>0)
    {
        print "<table>\n";
        print "<tr><th>Section</th><th>Page</th><th>Color</th></tr>\n";
        foreach($dbPages['data'] as $page)
        {
            print "<tr>\n";
            print "<td>".$page['section_code']."</td>\n";
            print "<td>".$page['page_number']."</td>\n";
            if ($page['color']){print "<td>Full color</td>";}else{print "<td>Black</td>";}
            print "</tr>\n";
        }
        print "</table>\n";
    } else {
        print "All pages have been received.";
    }
}        
print "</div>\n";
print "</div>\n";


function addPlateTime($pid,$value,$color='K')
{
    switch ($color)
    {
        case "K":
        $img = 'approveCheckK.png';
        break;
        
        case "C":
        $img = 'approveCheckC.png';
        break;
        
        case "M":
        $img = 'approveCheckM.png';
        break;
        
        case "Y":
        $img = 'approveCheckY.png';
        break;
        
        default:
        $img = 'approveColorCheck.png';
        break;
        
    }
    print "
    <div class='input-group date' id='plateapprove".$color."_".$pid."_tp'>
        <input type='text' class='form-control' name='plateapprove".$color."_".$pid."' id='plateapprove".$color."_".$pid."' value='$value'/>
        <span class='input-group-addon'>
            <i class='fa fa-clock-o'></i>
        </span>
        <span class='input-group-btn'>
             <button class='btn btn-default' type='button' onClick=\"setPaginationTime('$pid','plateapprove".strtolower($color)."','now');\"><img src='artwork/$img' width=20 style='padding-top:2px;' /></button>
        </span>
        
    </div>
    <script>
    \$('#plateapprove".$color."_".$pid."_tp').datetimepicker({format: 'HH:mm',
    useCurrent: true,
    showTodayButton: true,
    icons: {
        time: 'fa fa-clock-o',
     }
    });
    \$('#plateapprove".$color."_".$pid."_tp').on('dp.change',function (e) {
       setPaginationTime('$pid','plateapprove".strtolower($color)."',e.date.format('HH:mm'));
    });
    </script>
    ";
    print "\n";
    
}

function plateData($jobid)
{
    
//grab stats if available
$sql="SELECT * FROM job_stats WHERE job_id=$jobid";
$dbStats=dbselectsingle($sql);

if ($dbStats['numrows']>0)
{
    $wasteplates=$dbStats['data']['plates_waste'];
    $remakeplates=$dbStats['data']['plates_remake'];
    $lastpage=$dbStats['data']['plateroom_lastpage'];
    if ($dbStats['data']['plateroom_lastpage_time']!='')
    {
        $lastpagetime=date("H:i",strtotime($dbStats['data']['plateroom_lastpage_time']));   
    } else {
        $lastpagetime='';
    }
    if(trim($remakeplates=='')){$remakeplates=0;}
    if(trim($wasteplates=='')){$wasteplates=0;}
    if(trim($lastpage=='')){$lastpage=0;}
} else {
    $wasteplates=0;
    $remakesplates=0;
    $lastpage='';
}
 
?>
<div class="row">
  <div class="panel panel-primary">
      <div class="panel-heading">Plate Data</div>
      <div class="panel-body">
            <div class="form-inline">
              <div class="form-group">
                <label for="remakeplates">Remake plate count</label>
                 <input id='remakeplates' type='number' size=5 style='width:50px;' onBlur="plateMonitorExtra('remake',<?php echo $jobid ?>,this.value);" value='<?php echo $remakeplates ?>' />
              </div>
              <div class="form-group">
                <label for="remakeplates">Waste plate count</label>
                 <input id='wasteplates' type='number' size=5 style='width:50px;' onBlur="plateMonitorExtra('waste',$jobid,this.value);"  value='<?php echo $wasteplates ?>' />
              </div>
              <div class="form-group">
                <label for="remakeplates">Last Page</label>
                 <input id='lastpage' type='text' size=5 style='width:50px;' onBlur="plateMonitorExtra('lastpage',$jobid,this.value);" value='<?php echo $lastpage ?>' />
            
              </div>
              <div class="form-group">
                <label for="remakeplates">Last Page Time</label>
                 <div class='input-group date' style='width:120px;' id='lastpagetime_tp'>
                    <input type='text' class='form-control' name='lastpagetime' id='lastpagetime' value='<?php echo $lastpagetime ?>'/>
                    <span class='input-group-addon'>
                        <i class='fa fa-clock-o'></i>
                    </span>
                 </div>
                 <script>
                     $('#lastpagetime_tp').datetimepicker({format: 'HH:mm',
                     useCurrent: true,
                     showTodayButton: true,
                     icons: {
                        time: 'fa fa-clock-o',
                     }
                     });
                     $("#lastpagetime_tp").on("dp.change",function (e) {
                       plateMonitorExtra('lastpagetime',<?php echo $jobid ?>,e.date.format("HH:mm"));
                     });
                 </script>
            
              </div>
            
            </div>
      
      </div>
  </div>
</div>

<?php  
}
?>
<script>
setInterval('getDeadlineDetails(<?php echo $jobid; ?>)',60000);
</script>
<?php
$Page->footer();
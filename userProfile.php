<?php
//<!--VERSION: .9 **||**-->

include("includes/boot.php") ;

if ($_POST)
{
    save_profile();
} else {
    show_profile();
}

function show_profile()
{
    global $siteID, $departments, $employeepositions;
    $id=$_SESSION['userid'];
    $sql="SELECT * FROM users WHERE id=$id";
    $dbresult=dbselectsingle($sql);
    $record=$dbresult['data'];
    $firstname=stripslashes($record['firstname']);
    $lastname=stripslashes($record['lastname']);
    $middlename=stripslashes($record['middlename']);
    $home=stripslashes($record['home']);
    $business=stripslashes($record['business']);
    $cell=stripslashes($record['cell']);
    $fax=stripslashes($record['fax']);
    $extension=stripslashes($record['extension']);
    $email=stripslashes($record['email']);
    $carrier=stripslashes($record['carrier']);
    $mugshot=stripslashes($record['mugshot']);
    $department=stripslashes($record['department_id']);
    $position=stripslashes($record['position_id']);
    $button="Save Profile";
    $carriers=array("verizon"=>"Verizon","tmobile"=>"T-Mobile","sprint"=>"Sprint","att"=>"AT&amp;T","virgin"=>"Virgin Mobile","nextel"=>"Nextel","cingular"=>"Cingular",'cricket'=>"Cricket");
    
    

print "<div id='userTabs'>\n";

print "<form method=post class='form-horizontal' enctype='multipart/form-data'>\n";

print "<ul class='nav nav-tabs' role='tablist'>
        <li role='presentation' class='active'><a href='#general' aria-controls='general' role='tab' data-toggle='tab'>Profile Details</a></li>
        <li role='presentation' ><a href='#alerts' aria-controls='alerts' role='tab' data-toggle='tab'>Alerts & Reports</a></li>
        <li role='presentation' ><a href='#dashboard' aria-controls='dashboard' role='tab' data-toggle='tab'>Dashboard</a></li>
        ";
print "</ul>\n";
        

print "<div class='tab-content'>\n";
    print "<div id='general' role='tabpanel' class='tab-pane active'>\n";
            make_select('department',$departments[$department],$departments,'Department');
            make_select('position',$employeepositions[$position],$employeepositions,'Position');
            make_text('firstname',$firstname,'First Name','',50);
            make_text('middlename',$middlename,'Middle Name','',50);
            make_text('lastname',$lastname,'Last Name','',50,false,false,'','','createUsername();');
            make_file('mugshot','Picture','',$mugshot);
            make_email('email',$email,'Email Address','',40);
            make_phone('business',$business,'Office Number');
            make_phone('home',$home,'Home Number');
            make_phone('cell',$cell,'Cell Number');
            make_select('carrier',$carriers[$carrier],$carriers,'Cell Carrier');
    print "</div>";

    print "<div id='alerts' role='tabpanel' class='tab-pane'>\n";



    print "
<div class='form-group'>
    <label class=\"col-sm-2 control-label\">Email Reports</label>
    <div class=\"col-sm-10\"><small>Please select the daily email reports you would like to receive.</small><br />
        <div class='row'>\n";
            $sql="SELECT * FROM user_reports WHERE user_id=$id";
            $dbUserReports=dbselectmulti($sql);
            $userrep=array();
            if ($dbUserReports['numrows']>0)
            {
                foreach ($dbUserReports['data'] as $userreport)
                {
                    $userrep[]=$userreport['report_id'];
                }
            }
            $sql="SELECT * FROM email_reports";
            $dbReports=dbselectmulti($sql);
            if ($dbReports['numrows']>0) {
                foreach ($dbReports['data'] as $report) {
                    if (in_array($report['id'], $userrep)) {
                        $checked = 1;
                    } else {
                        $checked = 0;
                    }
                    print "<div class='col-xs-12 col-md-3'>\n";
                    print make_checkbox('er_' . $report['id'], $checked) . " " . $report['report_name'];
                    print "</div>\n";
                }
            }
    print "
        </div>
    </div>
</div>\n";

    print "
<div class='form-group'>
    <label class=\"col-sm-2 control-label\">Text Alerts</label>
    <div class=\"col-sm-10\"><small>Please choose which publication/run combos you would like text alerts for.</small>
        <div id='textalerts' class='row'>\n";
            $sql="SELECT * FROM user_textalerts WHERE user_id=$id";
            $dbAlerts=dbselectmulti($sql);
            global $pubs;
            $runs=array();
            print "<div class='col-xs-12 col-md-2'>Type ".make_select('alerttype','press',array('press'=>'Press Run','inserter'=>'Inserter Runs'),false,'setAlertType();');
            print "</div>\n";
            print "<div class='col-xs-12 col-md-4'>Publication: ".make_select('pub_id',$pubs[0],$pubs);
            print "</div>\n";
            print "<div class='col-xs-12 col-md-3'>Press Run: ".make_select('pressrun_id',$runs[0],$runs);
            print "</div>\n";
            print "<div class='col-xs-12 col-md-2'>Inserter Run: ".make_select('insertrun_id',$runs[0],$runs);
            print "</div>\n";
        ?>
                <script type="text/javascript">
                function setAlertType()
                {
                    var atype=$('#alerttype').val();
                    if(atype=='press')
                    {
                       $('#pressruns').css({'display':'block'});
                       $('#insertruns').css({'display':'none'});
                    } else {
                       $('#pressruns').css({'display':'none'});
                       $('#insertruns').css({'display':'block'});
                    }
                }
                $("#pub_id").selectChain({
                    target: $("#pressrun_id"),
                    type: "post",
                    url: "includes/ajax_handlers/fetchRuns.php",
                    data: { ajax: true, zero:1 }
                });
                $("#pub_id").selectChain({
                    target: $("#insertrun_id"),
                    type: "post",
                    url: "includes/ajax_handlers/fetchInsertRuns.php",
                    data: { ajax: true, zero:1 }
                });
                </script>
            <?php
            print "<div class='col-xs-12 col-md-1'><input type='button' value='Add Alert' onclick='addAlert();' class='btn btn-default'></div>\n";
        print "</div>
        <div id='alerts' class='row'>";
            if ($dbAlerts['numrows']>0)
            {
                foreach ($dbAlerts['data'] as $alert)
                {
                    print "<div id='alert$alert[id]' class='col-xs-12'>$alert[alert_name] <span class='fa fa-trash' onclick='deleteAlert($alert[id]);'>&nbsp;</span></div>\n";
                }
            }
    print "
         </div>
    </div>
</div>\n";



            make_hidden('userid',$id);
            make_hidden('ref',$_SERVER['HTTP_REFERER']);
    print "</div>\n";

    print "<div id='dashboard' role='tabpanel' class='tab-pane'>\n";
    $sql="SELECT * FROM dashboard_items ORDER BY dashboard_name";
    $dbDItems=dbselectmulti($sql);
    if($dbDItems['numrows']>0)
    {
        foreach($dbDItems['data'] as $item)
        {
            //see if the user has this one selected
            $sql="SELECT * FROM user_dashboard WHERE module_id=$item[id]";
            $dbCheck=dbselectsingle($sql);
            if($dbCheck['numrows']>0)
            {
                $found=1;
            } else {
                $found=0;
            }
            make_checkbox('dashboard_'.$item['id'],$found,stripslashes($item['dashboard_name']),' Check to enable this item');
        }
    } else {
        print "<p>Sorry, no dashboard items have been configured yet.</p>";
    }
    print "</div>\n";
print "</div>";
    make_submit('submit',$button);
    print "</form>\n";
print "</div>\n";
}


function save_profile()
{
    $userid=intval($_POST['userid']);
    $username=trim(addslashes($_POST['username']));
    $firstname=addslashes($_POST['firstname']);
    $lastname=addslashes($_POST['lastname']);
    $middlename=addslashes($_POST['middlename']);
    $home=addslashes($_POST['home']);
    $business=addslashes($_POST['business']);
    $cell=addslashes($_POST['cell']);
    $fax=addslashes($_POST['fax']);
    $extension=addslashes($_POST['extension']);
    $email=addslashes($_POST['email']);
  
    $password=password_hash($_POST['password'],PASSWORD_DEFAULT);
  
    $carrier=$_POST['carrier'];
    $position=$_POST['position'];
    $department=$_POST['department'];
    $cell=str_replace("-","",$cell);
    $cell=str_replace(" ","",$cell);
    $cell=str_replace("(","",$cell);
    $cell=str_replace(")","",$cell);
    $cell=str_replace(".","",$cell);
    if(strlen($cell)<10){$cell=$GLOBALS['newspaperAreaCode'].$cell;}
        
    $sql="UPDATE users SET firstname='$firstname', middlename='$middlename', lastname='$lastname', business='$business', home='$home', cell='$cell', position_id='$position', department_id='$department', fax='$fax', email='$email', extension='$extension', carrier='$carrier' WHERE id=$userid";   
    $dbresult=dbexecutequery($sql);
    $error=$dbresult['error'];
    //now the email reports
    $values='';
    foreach($_POST as $key=>$value)
    {
        if(substr($key,0,3)=='er_')
        {
            $value=str_replace("er_","",$key);
            $values.="($userid,$value),";
        }   
    }
    $values=substr($values,0,strlen($values)-1);
    //get rid of all previous records
    $sql="DELETE FROM user_reports WHERE user_id=$userid";
    $dbDelete=dbexecutequery($sql);
    //add the new ones
    if ($values!='')
    {
        $sql="INSERT INTO user_reports (user_id,report_id) VALUES $values";
        $dbInsert=dbinsertquery($sql);
    }
    
    //now handle all the dashboard item stuff
    $sql="SELECT id FROM dashboard_items";
    $dbDashboard=dbselectmulti($sql);
    if($dbDashboard['numrows']>0)
    {
        $dashboardids="";
        foreach($dbDashboard['data'] as $d)
        {
            $dashboardids=$d['id'].",";
        }
        //now we have all the ids, we need to remove them as we find them so that in the end we will be able to delete those
        //dashboard items for the user that they have unchecked
        foreach($_POST as $key=>$value)
        {
            if(substr($key,0,10)=='dashboard_')
            {
                $key=str_replace("dashboard_","",$key);
                //see if the user already has this one
                $sql="SELECT * FROM user_dashboard WHERE user_id=$userid AND module_id=$key";
                $dbCheck=dbselectsingle($sql);
                if($dbCheck['numrows']==0)
                {
                    //ok, this is a new one, look up the default column and order for it
                    $sql="SELECT * FROM dashboard_items WHERE id=$key";
                    $dbItem=dbselectsingle($sql);
                    $item=$dbItem['data'];
                    $sql="INSERT INTO user_dashboard (user_id, module_id, module_column, module_order, collapsed) VALUES 
                    ('$userid', '$key', '$item[default_column]', '$item[default_order]', 0)";
                    $dbInsert=dbinsertquery($sql);
                    
                    //now remove it from the list of dashboardids
                    $dashboardids=str_replace("$key,","",$dashboardids);
                }
                    
            }
        }
        
        $dashboardids=substr($dashboardids,0,strlen($dashboardids)-1); //get rid of trailing comma now
        //now, get rid of any user dashboard items that are still in the dashboard ids list
        if($dashboardids!='')
        {
            $sql="DELETE FROM user_dashboard WHERE user_id=$userid AND module_id IN ($dashboardids)";
            $dbDelete=dbexecutequery($sql);
        }
    } else {
        //just skipping in case we don't have any items set up yet, which would cause an error
       
    }
    
    setUserMessage('Your profile has been updated successfully','success');
    redirect($_POST['ref']);
}
$Page->footer();
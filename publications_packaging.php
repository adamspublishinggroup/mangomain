<?php
//<!--VERSION: .9 **||**-->

include("includes/boot.php") ;
include("includes/pbsManifestImport.php") ;

if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}

switch ($action)
{
    case "Save Zone":
    save_zone('insert');
    break;
    
    case "Update Zone":
    save_zone('update');
    break;
    
    case "addzone":
    zones('add');
    break;
    
    case "editzone":
    zones('edit');
    break;
    
    case "deletezone":
    zones('delete');
    break;

    case "listzones":
    zones('list');
    break;

    case "Save Truck":
    save_truck('insert');
    break;
    
    case "Update Truck":
    save_truck('update');
    break;
    
    case "importtrucks":
        pbsimport();
    break;
    
    case "Load PBS File":
        process_PBSfile();
    break;
    
    case "Load Trucks":
    load_trucks();
    break;
    
    case "addtruck":
    trucks('add');
    break;
    
    case "edittruck":
    trucks('edit');
    break;
    
    case "deletetruck":
    trucks('delete');
    break;

    case "trucks":
    trucks('list');
    break;
    
    case "Save Route":
    save_route('insert');
    break;
    
    case "Update Route":
    save_route('update');
    break;
    
    case "addroute":
    routes('add');
    break;
    
    case "editroute":
    routes('edit');
    break;
    
    case "deleteroute":
    routes('delete');
    break;

    case "listroutes":
    routes('list');
    break;

    
    default:
    zones('list');
    break;
}


function zones($action)
{
    $pubid=intval($_GET['pubid']);
    if ($action=='add' || $action=='edit')
    {
        if ($action=='add')
        {
            $button="Save Zone";
            $sql="SELECT MAX(zone_order) as mo FROM publications_insertzones WHERE run_id='$runid'";
            $dbMax=dbselectsingle($sql);
            $max=$dbMax['data']['mo'];
            $zoneorder=$max+1;
            $zonecount=0;
        } else {
            $button="Update Zone";
            $zoneid=intval($_GET['zoneid']);
            $sql="SELECT * FROM publications_insertzones WHERE id=$zoneid";
            $dbRun=dbselectsingle($sql);
            $run=$dbRun['data'];
            $zonename=stripslashes($run['zone_name']);
            $zonelabel=stripslashes($run['zone_label']);
            $systemcode=stripslashes($run['system_code']);
            $zoneorder=stripslashes($run['zone_order']);
            $zonezip=stripslashes($run['zone_zip']);
            $dow0=stripslashes($run['dow_0']);
            $dow1=stripslashes($run['dow_1']);
            $dow2=stripslashes($run['dow_2']);
            $dow3=stripslashes($run['dow_3']);
            $dow4=stripslashes($run['dow_4']);
            $dow5=stripslashes($run['dow_5']);
            $dow6=stripslashes($run['dow_6']);
        }
        print "<form method=post class='form-horizontal'>\n";
        make_text('zonename',$zonename,'Zone Name');
        make_text('zonelabel',$zonelabel,'Zone Label','A short code for the zone (Ex. A)');
        make_text('systemcode',$systemcode,'System Code','Code used in the export file from the advertising system');
        make_text('zonezip',$zonezip,'Zone Zip');
        make_number('zoneorder',$zoneorder,'Zone Order');
        make_number('dow_0',$dow0,'Sunday Average Count','Average number of papers in this zone for Sunday');
        make_number('dow_1',$dow1,'Monday Average Count','Average number of papers in this zone for Monday');
        make_number('dow_2',$dow2,'Tuesday Average Count','Average number of papers in this zone for Tuesday');
        make_number('dow_3',$dow3,'Wednesday Average Count','Average number of papers in this zone for Wednesday');
        make_number('dow_4',$dow4,'Thursday Average Count','Average number of papers in this zone for Thursday');
        make_number('dow_5',$dow5,'Friday Average Count','Average number of papers in this zone for Friday');
        make_number('dow_6',$dow6,'Saturday Average Count','Average number of papers in this zone for Saturday');
        
        make_hidden('zoneid',$zoneid);
        make_hidden('pubid',$pubid);
        
        make_submit('submit',$button);
        print "</form>\n";
    }elseif ($action=='delete')
    {
        $zoneid=intval($_GET['zoneid']);
        $sql="DELETE FROM publications_insertzones WHERE id=$zoneid";
        $dbDelete=dbexecutequery($sql);
        $error=$dbDelete['error'];
        if ($error!='')
        {
            setUserMessage('There was a problem deleting the zone.<br />'.$error,'error');
        } else {
            setUserMessage('The zone has been successfully deleted.','success');
        }
        redirect("?action=listzones&pubid=$pubid");
    } else {
       //show all the pubs
       $sql="SELECT * FROM publications_insertzones WHERE pub_id=$pubid ORDER BY zone_order";
       $dbRuns=dbselectmulti($sql);
       tableStart("<a href='?action=addzone&pubid=$pubid'>Add zone</a>,<a href='publications.php?action=list'>Return to publications</a>","Order,Label,Name,System Code",4);
       if ($dbRuns['numrows']>0)
       {
            foreach($dbRuns['data'] as $run)
            {
                $zoneid=$run['id'];
                $zonename=$run['zone_name'];
                $zonelabel=$run['zone_label'];
                $systemcode=$run['system_code'];
                $zoneorder=$run['zone_order'];
                print "<tr><td>$zoneorder</td><td>$zonelabel</td>\n";
                print "<td>$zonename</td>\n";
                print "<td>$systemcode</td>\n";
                print "<td>
            <div class='btn-group'>
              <a href='?action=editzone&pubid=$pubid&zoneid=$zoneid' class='btn btn-dark'>Edit</a>
              <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <span class='caret'></span>
                <span class='sr-only'>Toggle Dropdown</span>
              </button>
              <ul class='dropdown-menu'>
                <li><a href='?action=trucks&pubid=$pubid&zoneid=$zoneid'>Trucks</a></li>
                <li><a href='?action=deletezone&pubid=$pubid&zoneid=$zoneid' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
              </ul>
            </div>
            </td>";
                print "</tr>\n";
            }
       }
       tableEnd($dbRuns);
    }
        
}

function save_zone($action)
{
    $pubid=intval($_POST['pubid']);
    $zoneid=intval($_POST['zoneid']);
    $zonename=addslashes($_POST['zonename']);
    $zonelabel=addslashes($_POST['zonelabel']);
    $systemcode=addslashes($_POST['systemcode']);
    $zoneorder=addslashes($_POST['zoneorder']);
    $zonezip=addslashes($_POST['zonezip']);
    $dow0=intval($_POST['dow_0']);
    $dow1=intval($_POST['dow_1']);
    $dow2=intval($_POST['dow_2']);
    $dow3=intval($_POST['dow_3']);
    $dow4=intval($_POST['dow_4']);
    $dow5=intval($_POST['dow_5']);
    $dow6=intval($_POST['dow_6']);
    if ($zoneorder==''){$zoneorder=99;}
    if ($action=='insert')
    {
        $sql="INSERT INTO publications_insertzones (pub_id, zone_name, system_code, zone_order, zone_zip, dow_0, dow_1, dow_2, dow_3, dow_4, dow_5, dow_6, zone_label)
         VALUES ('$pubid', '$zonename', '$systemcode',  '$zoneorder', '$zonezip', '$dow0', '$dow1', '$dow2', '$dow3', '$dow4', '$dow5', '$dow6', '$zonelabel' )";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $sql="UPDATE publications_insertzones SET zone_zip='$zonezip', zone_name='$zonename', system_code='$systemcode', zone_order='$zoneorder', dow_0='$dow0', dow_1='$dow1', dow_2='$dow2', dow_3='$dow3', dow_4='$dow4', dow_5='$dow5', dow_6='$dow6', zone_label='$zonelabel' WHERE id=$zoneid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the zone.<br />'.$error,'error');
    } else {
        setUserMessage('The zone has been successfully saved.','success');
    }
    redirect("?action=listzones&pubid=$pubid");
    
}


function trucks($action)
{
    $pubid=intval($_GET['pubid']);
    $zoneid=intval($_GET['zoneid']);
    //build a list of zones
    
    if ($action=='add' || $action=='edit')
    {
        if ($action=='add')
        {
            $button="Save Truck";
            $sql="SELECT MAX(truck_order) AS mo FROM publications_inserttrucks WHERE pub_id=$pubid AND zone_id=$zoneid";
            $dbMax=dbselectsingle($sql);
            $truckorder = $dbMax['data']['mo']+1;
        } else {
            $button="Update Truck";
            $truckid=intval($_GET['truckid']);
            $sql="SELECT * FROM publications_inserttrucks WHERE id=$truckid";
            $dbTruck=dbselectsingle($sql);
            $truck=$dbTruck['data'];
            $truckname=stripslashes($truck['truck_name']);
            $truckorder=stripslashes($truck['truck_order']);
            $truckzips=stripslashes($truck['zip_codes']);
            $notes=stripslashes($truck['truck_notes']);
        }
        print "<form method=post class='form-horizontal'>\n";
        make_text('truckname',$truckname,'Truck Name');
        make_text('truckorder',$truckorder,'Truck Order');
        make_text('truckzips',$truckzips,'Truck Zip Codes','Separate zip codes with a pipe "|".');
        make_textarea('trucknotes',$notes,'Notes','',80,10,true);
        make_submit('submit',$button);
        print "<input type='hidden' name='truckid' value='$truckid'>\n";
        print "<input type='hidden' name='zoneid' value='$zoneid'>\n";
        print "<input type='hidden' name='pubid' value='$pubid'>\n";
        print "</form>\n";
    }elseif ($action=='delete')
    {
        $sql="DELETE FROM publications_inserttrucks WHERE id=".intval($_GET['truckid']);
        $dbDelete=dbexecutequery($sql);
        $error=$dbDelete['error'];
        if ($error!='')
        {
            setUserMessage('There was a problem deleting the truck.<br />'.$error,'error');
        } else {
            setUserMessage('The truck has been successfully deleted.','success');
        }
        redirect("?action=trucks&pubid=$pubid&zoneid=");
    } else {
       //show all the pubs
       $sql="SELECT * FROM publications_inserttrucks WHERE pub_id=$pubid AND zone_id=$zoneid ORDER BY truck_order";
       $dbTrucks=dbselectmulti($sql);
       tableStart("<a href='?action=addtruck&pubid=$pubid&zoneid=$zoneid'>Add truck</a>,<a href='?action=importtrucks&pubid=$pubid&zoneid=$zoneid'>Import Trucks from Circulation</a>,<a href='?action=list&pubid=$pubid'>Return to zones</a>,<a href='publications.php?action=list'>Return to publications</a>","Truck Order,Truck Name",3);
       if ($dbTrucks['numrows']>0)
       {
            foreach($dbTrucks['data'] as $truck)
            {
                $truckid=$truck['id'];
                $truckorder=$truck['truck_order'];
                $truckname=$truck['truck_name'];
                print "<tr><td>$truckorder</td>\n";
                print "<td>$truckname</td>\n";
                print "<td>
            <div class='btn-group'>
              <a href='?action=edittruck&pubid=$pubid&truckid=$truckid&zoneid=$zoneid' class='btn btn-dark'>Edit</a>
              <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <span class='caret'></span>
                <span class='sr-only'>Toggle Dropdown</span>
              </button>
              <ul class='dropdown-menu'>
                <li><a href='?action=deletetruck&pubid=$pubid&truckid=$truckid&zoneid=$zoneid' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
              </ul>
            </div>
            </td>";
                
                print "</tr>\n";
            }
       }
       tableEnd($dbTrucks);
    }


}




function routes($action)
{
    $pubid=$_GET['pubid'];
    $truckid=$_GET['truckid'];
    if ($action=='add' || $action=='edit')
    {
        if ($action=='add')
        {
            $button="Save Route";
            $zoneorder=1;
        } else {
            $button="Update Route";
            $routeid=intval($_GET['routeid']);
            $sql="SELECT * FROM publications_insertroutes WHERE id=$routeid";
            $dbRoutes=dbselectsingle($sql);
            $route=$dbRoutes['data'];
            $routenumber=stripslashes($route['route_number']);
            $bulk=stripslashes($route['bulk']);
            $account=stripslashes($route['route_account']);
            $driver=stripslashes($route['route_driver']);
            $phone=stripslashes($route['route_phone']);
            $order=stripslashes($route['route_sequence']);
            $notes=stripslashes($route['route_notes']);
        }
        print "<form method=post class='form-horizontal'>\n";
        make_text('routenumber',$routenumber,'Route #');
        make_checkbox('bulk',$bulk,'Bulk','Check for bulk route.');
        make_text('account',$account,'Account Name','',50);
        make_text('driver',$driver,'Driver Name');
        make_text('phone',$phone,'Driver Phone');
        make_text('order',$order,'Sequence');
        make_textarea('notes',$notes,'Notes','Will appear on bundle top',70,20,true);
        make_submit('submit',$button);
        print "<input type='hidden' name='routeid' value='$routeid'>\n";
        print "<input type='hidden' name='truckid' value='$truckid'>\n";
        print "<input type='hidden' name='pubid' value='$pubid'>\n";
        print "</form>\n";
    }elseif ($action=='delete')
    {
        $sql="DELETE * FROM publications_insertroutes WHERE id=".intval($_GET['routeid']);
        $dbDelete=dbexecutequery($sql);
        $error=$dbDelete['error'];
        if ($error!='')
        {
            setUserMessage('There was a problem deleting the route.<br />'.$error,'error');
        } else {
            setUserMessage('The route has been successfully deleted.','success');
        }
        redirect("?action=listroutes&pubid=$pubid&truckid=$truckid");
    } else {
       //show all the pubs
       $sql="SELECT * FROM publications_insertroutes WHERE truck_id=$truckid ORDER BY route_sequence";
       $dbRoutes=dbselectmulti($sql);
       tableStart("<a href='?action=addroute&pubid=$pubid&runid=$runid&truckid=$truckid'>Add route</a>,<a href='?action=listtrucks&pubid=$pubid&runid=$runid'>Return to trucks</a>,<a href='publications.php?action=list'>Return to publications</a>","Sequence,Route Number,Route Driver",5);
       if ($dbRoutes['numrows']>0)
       {
            foreach($dbRoutes['data'] as $route)
            {
                $routeid=$route['id'];
                $routenumber=$route['route_number'];
                $account=$route['route_account'];
                $order=$route['route_sequence'];
                print "<tr><td>$order</td>\n";
                print "<td>$routenumber</td>\n";
                print "<td>$account</td>\n";
                print "<td>
            <div class='btn-group'>
              <a href='?action=editroute&pubid=$pubid&truckid=$truckid&routeid=$routeid' class='btn btn-dark'>Edit</a>
              <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <span class='caret'></span>
                <span class='sr-only'>Toggle Dropdown</span>
              </button>
              <ul class='dropdown-menu'>
                <li><a href='?action=deleteroute&pubid=$pubid&truckid=$truckid&routeid=$routeid' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
              </ul>
            </div>
            </td>";
                
                print "</tr>\n";
            }
       }
       tableEnd($dbRoutes);
    }


}


function save_route($action)
{
    $pubid=intval($_POST['pubid']);
    $truckid=intval($_POST['truckid']);
    $routeid=intval($_POST['routeid']);
    $routenumber=addslashes($_POST['routenumber']);
    $order=addslashes($_POST['order']);
    $notes=addslashes($_POST['notes']);
    $driver=addslashes($_POST['driver']);
    $account=addslashes($_POST['account']);
    $phone=addslashes($_POST['phone']);
    if ($draw==''){$draw=0;}
    if ($_POST['bulk']){$bulk=1;}else{$bulk=0;}
    if ($action=='insert')
    {
        $sql="INSERT INTO publications_insertroutes (pub_id, truck_id, route_number,
        bulk, route_sequence, route_driver, route_phone, route_notes, route_account)
         VALUES ('$pubid', '$truckid', '$routenumber', '$bulk', '$order', '$driver',
          '$phone', '$notes', '$account')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $sql="UPDATE publications_insertroutes SET route_account='$account', route_number='$routenumber', route_notes='$notes', 
        route_driver='$driver', bulk='$bulk', route_phone='$phone', route_sequence='$order' WHERE id=$routeid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the route.<br />'.$error,'error');
    } else {
        setUserMessage('The route has been successfully added.','success');
    }
    redirect("?action=listroutes&pubid=$pubid&truckid=$truckid");
    
}


function save_truck($action)
{
    $pubid=intval($_POST['pubid']);
    $truckid=intval($_POST['truckid']);
    $zoneid=intval($_POST['zoneid']);
    $name=addslashes($_POST['truckname']);
    $order=addslashes($_POST['truckorder']);
    $notes=addslashes($_POST['trucknotes']);
    $zips=addslashes($_POST['truckzips']);
    if ($order==''){$order=99;}
    if ($action=='insert')
    {
        $sql="INSERT INTO publications_inserttrucks (pub_id, zone_id, truck_name, truck_order, truck_notes, zip_codes)
         VALUES ('$pubid', '$zoneid', '$name', '$order', '$notes', '$zips')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $sql="UPDATE publications_inserttrucks SET truck_notes='$notes', zip_codes='$zips', truck_name='$name', truck_order='$order' WHERE id=$truckid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if ($error!='')
    {
        setUserMessage('There was a problem adding the truck to the route.<br />'.$error,'error');
    } else {
        setUserMessage('Truck has been successfully added to the route.','success');
    }
    redirect("?action=trucks&pubid=$pubid&zoneid=$zoneid");
}

$Page->footer();
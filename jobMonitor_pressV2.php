<?php
$pageFluid = true;
include("includes/boot.php");
include("includes/ajax_handlers/layoutGenerator.php");
global $pressDepartmentID;
//build a list of operators
$sql="SELECT A.* FROM users A, user_positions B WHERE A.position_id=B.id AND B.operator=1 AND A.department_id=$pressDepartmentID AND A.site_id=".SITE_ID." ORDER BY A.lastname";
$dbOperators=dbselectmulti($sql);
$operators=array();
$operators[0]="Set Operator";
if ($dbOperators['numrows']>0)
{
    foreach ($dbOperators['data'] as $operator)
    {
        $operators[$operator['id']]=$operator['firstname'].' '.$operator['lastname'];
    }   
}

$jobData = job_nav();

global $Prefs, $producttypes;
$layoutid=$jobData['job']['layout_id'];
$pubname=$jobData['pub']['pub_name'];
$pubdate=$jobData['job']['pub_date'];
$pubday=strtolower(date("l",strtotime($jobData['job']['pub_date'])));
$jobname=$jobData['run']['run_name'];
$jobid=$jobData['job']['id'];
$runid=$jobData['job']['run_id'];
$pubid=$jobData['job']['pub_id'];
?>
<div class='row'>
    <div class='col-xs-12 col-sm-3'>
    <?php pressTabs($layoutid,$jobid,$operators); ?>
    </div>
    <div class='col-xs-12 col-sm-7'>
    <?php 
    pressMonitorMain($jobData); ?>
    </div>
    <div class='col-xs-12 col-sm-2'>
    <?php pagePanels($jobid); ?>
    </div>
</div>

<?php

function pressMonitorMain($jobData)
{
    global $Prefs, $producttypes;
    $layoutid=$jobData['job']['layout_id'];
    $pubname=$jobData['pub']['pub_name'];
    $pubdate=$jobData['job']['pub_date'];
    $pubday=strtolower(date("l",strtotime($jobData['job']['pub_date'])));
    $jobname=$jobData['run']['run_name'];
    $jobid=$jobData['job']['id'];
    $runid=$jobData['job']['run_id'];
    $pubid=$jobData['job']['pub_id'];
   
    $lap=$jobData['job']['lap'];
    if ($lap!='none')
    {
      $lap="<span style='font-weight:bold;color:red;'>$lap</span>";  
    }
    if($jobData['job']['slitter']){$slitter='Engaged';}else{$slitter='Disengaged';}
    $folderpin=$GLOBALS['folderpins'][$jobData['job']['folder_pin']];
    $overrun=$jobData['job']['overrun']+$jobData['job']['draw_other']+$jobData['job']['draw_customer'];
    $gatefold=$jobData['job']['gatefold'];
    if ($jobData['job']['quarterfold']){$quarter="Quarterfold";}else{$quarter="Half-fold";}
    $jobname=$jobData['run']['run_name'];
    $draw=$jobData['job']['draw'];
    if($jobData['job']['stitch'] || $jobData['job']['trim'])
    {
        $stitchTrim = true;
    }
    $schedstart=date("m/d H:i",strtotime($jobData['job']['startdatetime']));
    $schedstop=date("m/d H:i",strtotime($jobData['job']['enddatetime']));
    $jobid=$jobData['job']['id'];
    $statsid=$jobData['job']['stats_id'];
    $papertypeid=$jobData['job']['papertype'];

    
    /* GET section information to build up the job type box 
    *  @TODO will need to update this when we change out how sections work
    */
    $sql="SELECT * FROM jobs_sections WHERE job_id=$jobid";
    $dbSections = dbselectsingle($sql);
    $sections = $dbSections['data'];
    $jobInfo='';
    for($i==1;$i<=3;$i++)
    {
        if($sections['section'.$i.'_used'])
        {
            $jobInfo.="Section $i: ".($sections['section'.$i.'_highpage']-$sections['section'.$i.'_lowpage']+1)." page ".$producttypes[$sections['section'.$i.'_producttype']]."<br>";
        }
    }
    
    

    if($GLOBALS['stickyNoteLocation']=='press')
    {
        //do something different?
    }
    //look in inserts for this pub date, pub id and sticky_note=1
    $sql="SELECT * FROM inserts A, inserts_schedule B WHERE A.id=B.insert_id B.pub_id='$pubid' AND B.insert_date='$pubdate' AND sticky_note=1";
    $dbStickyNote=dbselectsingle($sql);
    if($dbStickyNote['numrows']>0)
    {
        $sticky="<span style='color:red;font-weight:bold;'>STICKY NOTE!!!</span>";
    }


    //now get the standard name for this paper
    $sql="SELECT * FROM paper_types WHERE id=$papertypeid";
    $dbPaper=dbselectsingle($sql);
    $paper=$dbPaper['data']['common_name'];
    $coverpaper=$jobData['job']['papertype_cover'];
    if($coverpaper!=0)
    {
        //now get the standard name for this paper
        $sql="SELECT * FROM paper_types WHERE id=$coverpaper";
        $dbPaper=dbselectsingle($sql);
        $coverpaper=$dbPaper['data']['common_name'];
    } else {
        $coverpaper='Same paper';
    }
    if($GLOBALS['askForRollSize'])
    {
        $rollSize=$GLOBALS['sizes'][$jobData['job']['rollSize']]."\" roll";
    }

     //lets check and see if we have a job_stat record for this job
    $sql="SELECT * FROM job_stats WHERE id=$statsid";
    $dbJobStats=dbselectsingle($sql);
    if ($dbJobStats['numrows']>0)
    {
        if ($dbJobStats['data']['startdatetime_actual']!='')
        {
            $starttime=date("H:i",strtotime($dbJobStats['data']['startdatetime_actual']));
        } else {
            $starttime='';
        }
        if ($dbJobStats['data']['stopdatetime_actual']!='')
        {
            $stoptime=date("H:i",strtotime($dbJobStats['data']['stopdatetime_actual']));
        } else {
            $stoptime='';
        }
        if ($dbJobStats['data']['goodcopy_actual']!='Null' && $dbJobStats['data']['goodcopy_actual']!='')
        {
            $goodtime=date("H:i",strtotime($dbJobStats['data']['goodcopy_actual']));
        } else {
            $goodtime='';
        }
        $startcounter=$dbJobStats['data']['counter_start'];
        $stopcounter=$dbJobStats['data']['counter_stop'];
        $startspoils=$dbJobStats['data']['spoils_startup'];
        if($dbJobStats['data']['setup_start']!='' && $dbJobStats['data']['setup_start']!='Null')
        {
            $setupstart=date("H:i",strtotime($dbJobStats['data']['setup_start']));
        } else {
            $setupstart='';
        }
        if($dbJobStats['data']['setup_stop']!='' && $dbJobStats['data']['setup_stop']!='Null')
        {
            $setupstop=date("H:i",strtotime($dbJobStats['data']['setup_stop']));
        } else {
            $setupstop='';
        }
        if($setuptime==''){$setuptime=0;}
    }
    
       print "<input type='hidden' id='captureStopNotes' value='".$Prefs->captureStopNotes."' />\n";

    ?>
    <div class='row' id='pressMonitor'>
        <div class='col-xs-12 col-sm-10'>
            <h4>Job Information</h4>
            <table class='table table-striped table-bordered'>
                <tr>
                    <td class='pressInfoLabel'>Publication</td>
                    <td><?php echo $pubname ?></td>
                    
                    <td class='pressInfoLabel'>Job Name</td>
                    <td><?php echo $jobname ?></td>
                </tr>
                
                <tr>
                    <td class='pressInfoLabel'>Pub Date</td>
                    <td><?php echo $pubdate ?></td>
                    
                    <td class='pressInfoLabel'>Sticky Note</td>
                    <td><?php echo ($sticky==0? "No" : "Yes"); ?></td>
                </tr>
                
                <tr>
                    <td class='pressInfoLabel'>Paper</td>
                    <td><?php echo $paper.' '.$rollSize ?></td>
                    
                    <td class='pressInfoLabel'>Page Width</td>
                    <td><?php echo $jobData['job']['pagewidth'] ?>"</td>
                </tr>
                
                <tr>
                    <td class='pressInfoLabel'>Draw</td>
                    <td>
                    <div class="input-group">
                      <input type='number' class='form-control' id='pressdraw' readonly=true value='<?php echo $draw ?>'>
                      <span class="input-group-btn">
                         <button id='updatedraw' class='btn btn-default' onClick='updatePressDraw();'>Edit</button>
                      </span>
                    </div>
                    </td>
                                   
                    <td class='pressInfoLabel'>Overrun</td>
                    <td><?php echo $overrun ?></td>
                </tr>
                
                <tr>
                    <td class='pressInfoLabel'><?php if($Prefs->pressFolderIndicator=='pin'){print "Folder Pin";}else{print "Lap";}?></td>
                    <td><?php if($Prefs->pressFolderIndicator=='pin'){ echo $folderpin;} else {echo $lap;} ?></td>
                    
                    <td class='pressInfoLabel'>Cover paper</td>
                    <td><?php echo $coverpaper ?></td>
                </tr>
                
                <tr>
                    <td class='pressInfoLabel'>Quarterfold</td>
                    <td><?php echo  ($quarter==0? "No" : "Yes"); ?></td>
                    
                    <td class='pressInfoLabel'>Gatefold</td>
                    <td><?php echo  ($gatefold==0? "No" : "Yes"); ?></td>
                </tr>
                
                <tr>
                    <td class='pressInfoLabel'>Slitter</td>
                    <td><?php echo  ($slitter==0? "No" : "Yes"); ?></td>
                    
                    <td class='pressInfoLabel'>Job Type</td>
                    <td><?php echo $jobInfo ?></td>
                </tr>
                
                <tr>
                    <td class='pressInfoLabel'>Stitch & Trim</td>
                    <td><?php echo ($stitchTrim?'Yes it does':'No') ?></td>
                    
                    <td class='pressInfoLabel'>Placeholder</td>
                    <td>----</td>
                </tr>
                
                
                <tr>
                    <td class='pressInfoLabel'>Scheduled Press Start</td>
                    <td><?php echo $schedstart ?></td>
                    
                    <td class='pressInfoLabel'>Scheduled Press Stop</td>
                    <td><?php echo $schedstop ?></td>
                </tr>
                
                
                <tr>
                    <td class='pressInfoLabel'>Setup Start</td>
                    <td>
                        <div class="input-group date" id='benchmark_setupStart_tp'>
                            <input type='text' class='form-control' name='benchmark_setupStart' id='benchmark_setupStart' value='<?php echo $setupstart ?>'/>
                            <span class='input-group-addon'>
                                <i class='fa fa-clock-o'></i>
                            </span>
                        </div>
                    </td>
                    
                    <td class='pressInfoLabel'>Setup Stop</td>
                    <td>
                        <div class="input-group date" id='benchmark_setupStop_tp'>
                            <input type='text' class='form-control' name='benchmark_setupStop' id='benchmark_setupStop' value='<?php echo $setupstop ?>'/>
                            <span class='input-group-addon'>
                                <i class='fa fa-clock-o'></i>
                            </span>
                        </div>
                    </td>
                    
                </tr>
                
                <tr>
                    <td class='pressInfoLabel'>Start Time</td>
                    <td>
                        <div class="input-group date" id='benchmark_starttime_tp'>
                            <input type='text' class='form-control' name='benchmark_starttime' id='benchmark_starttime' value='<?php echo $starttime ?>'/>
                            <span class='input-group-addon'>
                                <i class='fa fa-clock-o'></i>
                            </span>
                        </div>
                    </td>
                    
                    <td class='pressInfoLabel'>Start Counter</td>
                    <td>
                        <div class="input-group">
                            <input type='number' class='form-control number' name='benchmark_startcounter' readonly id='benchmark_startcounter' value='<?php echo $startcounter ?>'/>
                            <span class='input-group-btn'>
                                <button class='btn btn-default' onClick='updatePressCounter("startcounter",<?php echo $jobid ?>, "Enter value for the start counter");'>Edit</button>
                            </span>
                        </div>
                    </td>
                    
                </tr>
                
                <tr>
                    <td class='pressInfoLabel'>Good Copy</td>
                    <td>
                        <div class="input-group date" id='benchmark_goodtime_tp'>
                            <input type='text' class='form-control' name='benchmark_goodtime' id='benchmark_goodtime' value='<?php echo $goodtime ?>'/>
                            <span class='input-group-addon'>
                                <i class='fa fa-clock-o'></i>
                            </span>
                        </div>
                    </td>
                    
                    <td class='pressInfoLabel'>Spoils</td>
                    <td>
                        <div class="input-group">
                            <input type='number' class='form-control number' name='benchmark_spoils' id='benchmark_spoils' readonly value='<?php echo $startspoils ?>'/>
                            <span class='input-group-btn'>
                                <button class='btn btn-default' onClick='updatePressCounter("spoils",<?php echo $jobid ?>, "Enter value for the total spoils");'>Edit</button>
                            </span>
                        </div>
                    </td>
                    
                </tr>
                
                <tr>
                    <td class='pressInfoLabel'>Stop Time</td>
                    <td>
                        <div class="input-group date" id='benchmark_stoptime_tp'>
                            <input type='text' class='form-control' name='benchmark_stoptime' id='benchmark_stoptime' value='<?php echo $stoptime ?>'/>
                            <span class='input-group-addon'>
                                <i class='fa fa-clock-o'></i>
                            </span>
                        </div>
                    </td>
                    
                    <td class='pressInfoLabel'>Stop Counter</td>
                    <td>
                        <div class="input-group">
                            <span class='input-group-btn'>
                                <button class='btn btn-default clipper'>
                                    <i class='fa fa-clipboard'></i>
                                </button>
                            </span>
                            <input type='text' class='form-control number' name='benchmark_stopcounter' id='benchmark_stopcounter' value='<?php echo $stopcounter ?>'/>
                            <span class='input-group-btn'>
                                <button class='btn btn-default' onClick='updatePressCounter("stopcounter",<?php echo $jobid ?>, "Enter value for the stop counter");'>Edit</button>
                            </span>
                        </div>
                    </td>
                    
                </tr>
          
            </table>
            <script>
            // data-clipboard-target='#benchmark_stopcounter'
                new Clipboard('.clipper', {
                    text: function(trigger) {
                        return $('#benchmark_stopcounter').val();
                    }
                });
                
                $('.number').keyboardMouse({type:'numpad',callback:popupKeyboardHandler});
                function popupKeyboardHandler(e)
                {
                    var field = e[0].id;
                    field = field.replace('benchmark_','');
                    value = e.val();
                    jobMonitorPress(field,value,<?php echo $jobid ?>);
                }
                $('#benchmark_setupStart_tp').datetimepicker({format: 'HH:mm',
                    useCurrent: true,
                    showTodayButton: false
                }).on("dp.hide",function (e) {
                    jobMonitorPress('setupstart',$('#benchmark_setupStart').val(),<?php echo $jobid ?>);
                });
                
                $('#benchmark_setupStop_tp').datetimepicker({format: 'HH:mm',
                    useCurrent: true,
                    showTodayButton: false
                }).on("dp.hide",function (e) {
                    jobMonitorPress('setupstop',$('#benchmark_setupStop').val(),<?php echo $jobid ?>);
                });
                
                $('#benchmark_starttime_tp').datetimepicker({format: 'HH:mm',
                    useCurrent: true,
                    showTodayButton: false
                }).on("dp.hide",function (e) {
                    jobMonitorPress('starttime',$('#benchmark_starttime').val(),<?php echo $jobid ?>);
                });
                
                $('#benchmark_stoptime_tp').datetimepicker({format: 'HH:mm',
                    useCurrent: true,
                    showTodayButton: false
                }).on("dp.hide",function (e) {
                    jobMonitorPress('stoptime',$('#benchmark_stoptime').val(),<?php echo $jobid ?>);
                });
                
                $('#benchmark_goodtime_tp').datetimepicker({format: 'HH:mm',
                    useCurrent: true,
                    showTodayButton: false
                }).on("dp.hide",function (e) {
                    jobMonitorPress('goodtime',$('#benchmark_goodtime').val(),<?php echo $jobid ?>);
                });
                
                function updatePressCounter(field,jobID,title)
                {
                    bootbox.prompt({
                        title: title,
                        inputType: 'number',
                        value: $('#benchmark_'+field).val(),
                        callback: function (result) {
                            if(result!='' && result != null)
                            { 
                                jobMonitorPress(field,result,jobID);
                                $('#benchmark_'+field).val(result);
                            }
                        }
                    });
                }
                
                function jobMonitorPress(field,value,jobID)
                {
                    $.ajax({
                      url: "includes/ajax_handlers/jobmonitorPress.php",
                      type: "POST",
                      data: ({type:'stat',jobid:jobID,value:value,source:field}),
                      dataType: "json",
                      success: function(response){
                          //all good
                      }
                    })
                    
                }
            </script>
        </div>
        <div class='col-xs-12 col-sm-2'>
            <h4>Press Stop Codes</h4>
            <?php  stopCodes($jobid); ?>
        </div>
    
    </div>
    <?php       
}


function remakeslist($jobid)
{
    $cache=checkCache('jobBoxes'.$jobid,'remakes');
    if($cache)
    {
        print $cache;
    } else {
        $sql="SELECT DISTINCT(page_number), section_code, version, workflow_receive FROM job_pages WHERE job_id=$jobid AND version>1 ORDER BY page_number ASC, version DESC";
        $dbPages=dbselectmulti($sql);
        $data='';
        if ($dbPages['numrows']>0)
        {
            $data.="<table>\n";
            $data.= "<tr><th>Section</th><th>Page</th><th>Version</th><th>Receive Time</th></tr>\n";
            foreach($dbPages['data'] as $page)
            {
                $data.= "<tr>\n";
                $data.= "<td>".$page['section_code']."</td>\n";
                $data.= "<td>".$page['page_number']."</td>\n";
                $data.= "<td>".$page['version']."</td>\n";
                $data.= "<td>";
                if ($page['workflow_receive']!='')
                {
                    $data.= date("H:i:s",strtotime($page['workflow_receive']));
                } else {
                    $data.= "Not received";
                }
                $data.= "</td>";
                $data.= "</tr>\n";
            
            }
            $data.= "</table>\n";
        } else {
            $data.= "No remakes at this time.";
        }
        
        print $data;
        setCache('jobBoxes'.$jobid,'remakes',$data);
    }
}

function pageslist($jobid)
{
    $cache=checkCache('jobBoxes'.$jobid,'missingpages');
    if($cache)
    {
        print $cache;
    } else {
        $data='';
        $sql="SELECT DISTINCT(page_number), section_code, page_number, color FROM job_pages WHERE job_id=$jobid AND page_release is Null ORDER BY section_code ASC, page_number ASC";
        $dbPages=dbselectmulti($sql);
        if ($dbPages['numrows']>0)
        {
            $data="<table>\n";
            $data.="<tr><th>Section</th><th>Page</th><th>Color</th></tr>\n";
            foreach($dbPages['data'] as $page)
            {
                $data.= "<tr>\n";
                $data.= "<td>".$page['section_code']."</td>\n";
                $data.= "<td>".$page['page_number']."</td>\n";
                if ($page['color']){$data.=  "<td>Full color</td>";}else{$data.=  "<td>Black</td>";}
                $data.= "</tr>\n";
            }
            $data.= "</table>\n";
        } else {
            $data.= "All pages have been received.";
        }
        print $data;
        setCache('jobBoxes'.$jobid,'missingpages',$data);
    }
    
}

function plateslist($jobid)
{
    $cache=checkCache('jobBoxes'.$jobid,'missingplates');
    if($cache)
    {
        print $cache;
    } else {
        $data='';
        $sql="SELECT DISTINCT(low_page), section_code, low_page, color FROM job_plates WHERE job_id=$jobid AND black_receive is Null ORDER BY section_code ASC, low_page ASC";
        $dbPages=dbselectmulti($sql);
        if ($dbPages['numrows']>0)
        {
            $data.="<table>\n";
            $data.="<tr><th>Section</th><th>Plate</th><th>Color</th></tr>\n";
            foreach($dbPages['data'] as $page)
            {
                $data.="<tr>\n";
                $data.= "<td>".$page['section_code']."</td>\n";
                $data.="<td>".$page['low_page']."</td>\n";
                if ($page['color']){$data.=  "<td>Full color</td>";}else{$data.="<td>Black</td>";}
                $data.=  "</tr>\n";
            }
            $data.="</table>\n";
        } else {
            $data.="All plates have been received.";
        }
        print $data;
        setCache('jobBoxes'.$jobid,'missingplates',$data);
    }
}

function stopCodes($jobid)
{
    $sql="SELECT * FROM stop_codes ORDER BY stop_order";
    $dbCodes=dbselectmulti($sql);
    if ($dbCodes['numrows']>0)
    {
        $i=1;
        print "<div class='btn-group-vertical'>\n";
        foreach($dbCodes['data'] as $code)
        {
            print "<a href='includes/ajax_handlers/pressStopCodeDisplay.php?type=stopcode&stopid=$code[id]&jobid=$jobid' rel='$jobid' class='fancybox.iframe pressStopButton btn btn-danger '>$code[stop_name]</a>\n"; 
            if ($i<=5)
            {
                $i++;
            } else {
                $i=1;
            }
        }
        print "</div>\n";
    
    }


}


function benchmarks($jobid,$runid,$operators)
{
    global $pubday;
    //at the top, specify the job operator
    $sql="SELECT job_pressoperator FROM job_stats A, jobs B WHERE A.id=B.stats_id AND B.id=$jobid";
    $dbOp=dbselectsingle($sql);
    if ($dbOp['numrows']>0)
    {
        if ($dbOp['data']['job_pressoperator']!=0){$opid=$dbOp['data']['job_pressoperator'];}else{$opid=0;}
    } else {
        $opid=0;
    }
    
    //get a list of all benchmarks for press that are in "displaylist" 
    $sql="SELECT A.id, A.benchmark_name, A.benchmark_type, B.$pubday FROM benchmarks A, run_benchmarks B  
    WHERE A.benchmark_category='press' AND A.benchmark_displaylist=1 AND A.id=B.benchmark_id AND B.run_id=$runid ORDER BY A.benchmark_order";
    $dbBenchmarks=dbselectmulti($sql);
    if ($dbBenchmarks['numrows']>0)
    {
        print "<table class='table table-bordered table-condensed'>\n";
        print "<tr><th style='width:80px;font-size:12px;'>Benchmark</th><th style='width:80px;font-size:12px;'>Goal</th><th style='width:120px;font-size:12px;'>Actual</th></tr>\n";
        print "<tr><td>Principal Operator</td>";
        //sign off sections
        print "<td colspan=2>\n";
        print make_select('jobOperator',$operators[$opid],$operators,false,"benchmarkChange(this.id,'');");
        print "</td>\n";
        print "</tr>\n";
        foreach($dbBenchmarks['data'] as $benchmark)
        {
            $bid=$benchmark['id'];
            $name=$benchmark['benchmark_name'];
            $type=$benchmark['benchmark_type'];
            $goal=$benchmark[$pubday];
            //see if we have an actual for this benchmark in the job_benchmarks table
            $sql="SELECT * FROM job_benchmarks WHERE job_id=$jobid AND benchmark_id=$bid";
            $dbJobBenchmark=dbselectsingle($sql);
            if ($dbJobBenchmark['numrows']>0)
            {
                if ($type=='time')
                {
                    $actual=$dbJobBenchmark['data']['benchmark_actual_time'];
                    $actual=date("H:i",strtotime($actual));
                } else {
                    $actual=$dbJobBenchmark['data']['benchmark_actual_number'];
                }
            }else {
                if ($type=='time')
                {
                    $actual="00:00";
                } else {
                    $actual="";
                }
            }
            
            
            print "<tr>\n";
            print "<td style='width:80px;font-size:12px;'>$name</td>";
            print "<td style='width:80px;font-size:12px;'>$goal</td>";
            if ($type=='time')
            {
                $clicker="timeInit('benchmark_$bid','benchmarkChange','$bid');";
                $nowbutton="<input type='button' style='margin-left:4px;width:40px;font-size:10px;' onclick=\"timeSet('$bid');\" value='Set'>";   
            } else {
                $clicker="numpad_init('benchmark_$bid','ares');";
                $nowbutton="";
            }
            print "<td><input style='width:50px;' type='text' id='benchmark_$bid' value='$actual' onclick=\"$clicker\">$nowbutton</td>";
            print "</tr>\n";
        }
        print "</table>\n";
    }

}


function pressTabs($layoutid,$jobid,$operators)
{
    print "<div class='btn-group' style='margin-bottom:10px;'>\n";
    print "<a class='btn btn-default' data-id='$jobid' data-toggle='modal' data-target='#pressNotesModal'>Update/View Job Notes</a>\n"; 
    print "<a href='#' class='btn btn-default' onclick=\"window.open('maintenancePress.php','Press Maintenance','width=1000,height=650,toolbar=0,status=0,location=0,scrollbars=1');return false;\">Maintenance</a>\n";
    print "</div>\n";      
    
    
    print "<ul class='skinnytab nav nav-tabs' role='tablist'>
        <li role='presentation' class='active'><a href='#jobnotes' aria-controls='jobnotes' role='tab' data-toggle='tab'>Notes</a></li>
        <li role='presentation'><a href='#layoutTab' aria-controls='layoutTab' role='tab' data-toggle='tab'>Layout</a></li>
        <li role='presentation'><a href='#checklistTab' aria-controls='checklistTab' role='tab' data-toggle='tab'>Checklist</a></li>
        <li role='presentation'><a href='#presscrewTab' aria-controls='presscrewTab' role='tab' data-toggle='tab'>Crew</a></li>
        <li role='presentation'><a href='#paperTab' aria-controls='paperTab' role='paperTab' data-toggle='tab'>Newsprint</a></li>
        <li role='presentation'><a href='#benchmarkTab' aria-controls='benchmarkTab' role='paperTab' data-toggle='tab'>Benchmarks</a></li>
        ";
    print "</ul>\n";
    print "<div class='tab-content'>\n";
        print "<div id='jobnotes' role='tabpanel' class='tab-pane active' >\n";
            jobnotes($jobid);
        print "</div>\n";
        print "<div id='layoutTab' role='tabpanel' class='tab-pane'>\n";
            configureDiagram($layoutid,true,false,true);
        print "</div>\n";
        print "<div id='checklistTab' role='tabpanel' class='tab-pane'>\n";
            checklist($jobid,$operators);
        print "</div>\n";
        print "<div id='presscrewTab' role='tabpanel' class='tab-pane'>\n";
            crew($jobid);
        print "</div>\n";
        print "<div id='paperTab' role='tabpanel' class='tab-pane'>\n";
            jobnewsprint($jobid);
        print "</div>\n";
        print "<div id='benchmarkTab' role='tabpanel' class='tab-pane'>\n";
            benchmarks($jobid,$runid,$operators);
        print "</div>\n";
    print "</div>\n";
}

function jobnotes($jobid)
{
    
    print "<h4>Messages</h4>\n";
    $sql="SELECT A.notes_press, A.notes_job, A.job_message, B.run_message, C.pub_message FROM jobs A, publications_runs B, publications C 
        WHERE A.id=$jobid AND A.run_id=B.id AND A.pub_id=C.id";
    $dbJobInfo=dbselectsingle($sql);
    $jobinfo=$dbJobInfo['data'];

    if($Prefs->pressStartMessage!='')
    {
        print "<div class='alert alert-danger' role='alert'><b>Startup Message:</b> ".$Prefs->pressStartMessage."</div>\n";
    }
    if($jobinfo['pub_message']!='')
    {
        print "<div class='alert alert-info' role='alert'><b>Publication Message:</b> ".stripslashes($jobinfo['pub_message'])."</div>\n";
    }
    if($jobinfo['run_message']!='')
    {
        print "<div class='alert alert-warning' role='alert'><b>Run Message:</b> ".stripslashes($jobinfo['run_message'])."</div>\n";
    }
    if($jobinfo['job_message']!='')
    {
        print "<div class='alert alert-success' role='alert'><b>Job Message:</b> ".stripslashes($jobinfo['job_message'])."</div>\n";
    }
    
    print "<h4>Notes</h4>\n";
    print stripslashes($jobinfo['notes_job'])."\n";
    print "<div id='on_page_press_notes'>".stripslashes($jobinfo['notes_press'])."</div>";
          
}

function crew($jobid)
{
    global $siteID,$pressDepartmentID;
    print "<div style='font-size:12px;'>\n";
    $sql="SELECT * FROM users WHERE department_id=$pressDepartmentID ORDER BY lastname";
    $dbPressman=dbselectmulti($sql);
    $sql="SELECT job_pressman_ids FROM job_stats A, jobs B WHERE A.id=B.stats_id AND B.id=$jobid";
    $dbStats=dbselectsingle($sql);
    $jobpressmanids=array();
    if($dbStats['numrows']>0)
    {
        $jobpressmanids=explode("|",$dbStats['data']['job_pressman_ids']);
    }
    if ($dbPressman['numrows']>0)
    {
        $i=0;
        foreach($dbPressman['data'] as $s)
        {
            if (in_array($s['id'],$jobpressmanids))
            {
                $checked="checklist_checked";
            } else {
                $checked="checklist_unchecked";
            }
            $pressmanid=$s['id'];
            if($i==1)
            {
                $margin='';
            } else {
                $margin='margin-right:10px;';
            }
            
            print "<div id='pressman_$pressmanid' class='$checked' onClick='pressmanChange($jobid,$pressmanid);'>\n";
            print $s['firstname']." ".$s['lastname']."</div>\n";
             
            
        }
    }
    print "</div>\n";  
}

function jobnewsprint($jobid)
{
    global $papertypes, $sizes, $pressid, $pressmen, $siteID, $pressDepartmentID, $jobData, $broadsheetPageHeight;
    //need all press towers
    $sql="SELECT * from press_towers WHERE press_id=$pressid AND tower_type='printing' ORDER BY tower_order";
    $dbTowers=dbselectmulti($sql);
    $towers=$dbTowers['data'];
    $sizes[0]='Roll width';
    $papertypes[0]='Type of paper';
    $sql="SELECT * FROM jobs WHERE id=$jobid";
    $dbJob=dbselectsingle($sql);
    $job=$dbJob['data'];
    $newtowers=array();
    $pubdate = $job['pub_date'];
    $printdate=$job['startdatetime'];
    foreach($towers as $tower)
    {
        //first thing is to see if we have already created a paper record for this job and tower in job_paper
        $sql="SELECT * FROM job_paper WHERE job_id='$jobid' AND tower_id='$tower[id]'";
        $dbExisting=dbselectsingle($sql);
        if($dbExisting['numrows']>0)
        {
            //means that already exists
            $e=$dbExisting['data'];
            $newtowers[$tower['id']]['id']=$tower['id'];
            $newtowers[$tower['id']]['tower_name']=$tower['tower_name'];    
            $newtowers[$tower['id']]['used']=1;    
            $newtowers[$tower['id']]['papertype']=$e['papertype_id'];    
            $newtowers[$tower['id']]['size']=$e['size_id'];
        } else{
            //see if this tower is used
            $towerid=$tower['id'];
            $sql="SELECT * FROM layout_sections WHERE layout_id=$job[layout_id] AND towers LIKE '%$towerid%'";
            $dbUsed=dbselectsingle($sql);
            if ($dbUsed['numrows']>0)
            {
                //lets verify because single digits can be returned by this (example Tower 1 gives 1 and 10);
                $z=$dbUsed['data']['towers'];
                $z=explode("|",$z);
                $found=0;
                foreach($z as $key=>$v)
                {
                    if($towerid==$v)
                    {
                        $found=1;
                    }   
                }
                $newtowers[$tower['id']]['used']=$found;        
            } else {
                $newtowers[$tower['id']]['used']=0;
            }
            
            $newtowers[$tower['id']]['tower_name']=$tower['tower_name'];
            $newtowers[$tower['id']]['id']=$tower['id'];
            $newtowers[$tower['id']]['papertype']=$job['papertype'];
            //now to figure out how many pages on the plate - thus the paper size
            $layoutid=$job['layout_id'];
            $towerid=$tower['id'];
            $sql="SELECT * FROM layout_page_config WHERE layout_id=$layoutid AND tower_id=$towerid AND side='10' AND tower_row=1";
            $dbPages=dbselectmulti($sql);
            $pcount=0;
            if($dbPages['numrows']>0)
            {
                foreach($dbPages['data'] as $pages)
                {
                    if ($pages['page_number']!='0'){$pcount++;}    
                }
                //we're assuming everything is a newspaper lead. for commercial
                //leads, we'll just assume the press crew will set it.
                //On 9/27/12 changed from using GLOBALS[broadsheetPageWidth] to the actual page width defined in the job
                //$rollwidth=$GLOBALS['broadsheetPageWidth']*$pcount;
                //find the index in the sizes array with this roll width
                
                $rollwidth=$job['pagewidth']*$pcount;
                $rollwidthmin=$rollwidth-.1;
                $rollwidthmax=$rollwidth+.1;
                
                //find the index in the sizes array with this roll width
                $sql="SELECT id FROM paper_sizes WHERE width>='$rollwidthmin' AND width<='$rollwidthmax' AND status=1";
                $dbSizeFind=dbselectsingle($sql);
                if($dbSizeFind['numrows']>0)
                {
                    $rollwidthid=$dbSizeFind['data']['id'];
                } else {
                    $rollwidthid='Please choose';
                }
                $newtowers[$tower['id']]['size']=$rollwidthid; 
            } else {
                $newtowers[$tower['id']]['size']='Size';
            }
            
            
            
            //add the job_paper record
            $sql="INSERT INTO job_paper (job_id, pub_id, run_id, tower_id, papertype_id, 
                size_id, pub_date, print_date, roll_width, page_width, page_length, price_per_ton, 
                factor, calculated_tonnage, calculated_cost, site_id) VALUES ('$jobid', '".$jobData['job']['pub_id']."',
                '".$jobData['job']['run_id']."', '$towerid', '".$jobData['job']['papertype']."', '$rollwidthid',
                '$pubdate', '$printdate', '$sizes[$rollwidthid]','".$jobData['job']['pagewidth']."',
                '$broadsheetPageHeight', '0', '0', '0', '0', '$siteID')";
            $dbInsertPaper=dbinsertquery($sql);
        }
    }
    $towers=$newtowers;
    
    print "<form name='jobpaper' id='jobpaper' method=post action='includes/ajax_handlers/jobmonitorPressNewsprint.php'>\n";
    if (count($towers)>0)
    {
        foreach ($towers as $tower)
        {
            if ($tower['used'])
            {
                $ptype=$tower['papertype'];
                $psize=$tower['size'];
                
            } else {
                $ptype=0;
                $psize=0;
            }
            if($tower['tower_name']!='')
            {
                print "<div style='width:100px;float:left;'>$tower[tower_name]: <input type=hidden name='tower_$tower[id]' id='tower_$tower[id]' value='$tower[tower_name]'></div>";
                print "<div style='float:left;'>\n";
                print make_select("t_$tower[id]_papertype",$papertypes[$ptype],$papertypes)."<br />";
                print make_select("t_$tower[id]_size",$sizes[$psize],$sizes);
                print "</div><div class='clear'></div>\n";
            }
        }    
    }
    print "<input type='hidden' value='$jobid' name='pjid' />\n";
    print "<input type='hidden' value='$jobid' name='pubid' />\n";
    print "<input type='hidden' value='$jobid' name='runid' />\n";
    print "<input type='hidden' value='$jobid' name='runid' />\n";
    print "<input type='submit' value='Save Newsprint' style='float:left;'/>\n";
    print "<div id='newsprintSaveMessage' style='float:left;margin-left:10px;font-weight:bold;'></div>\n";
    print "</form>\n";
    print "<div class='clear'></div>\n";
    ?>
    <script>
    $(document).ready(function() { 
    var options = { 
        target:        '#newsprintSaveMessage'   // target element(s) to be updated with server response 
        //beforeSubmit:  function(){alert('submitting');},  // pre-submit callback 
        //success:      function(){$('#newsprintSaveMessage').css('display','block')}                 // post-submit callback 

        // other available options: 
        //url:       url         // override for form's 'action' attribute 
        //type:      type        // 'get' or 'post', override for form's 'method' attribute 
        //dataType:  null        // 'xml', 'script', or 'json' (expected server response type) 
        //clearForm: true        // clear all form fields after successful submit 
        //resetForm: true        // reset the form after successful submit 

        // $.ajax options can be used here too, for example: 
        //timeout:   3000 
    }; 
   
    // bind form using 'ajaxForm' 
    $('#jobpaper').ajaxForm(options);
    });
    
    </script>
    <?php    
}

function checklist($jobid,$operators)
{
    global $siteID;
    //first, get all the checklist items
    print "<div style='font-size:12px;'>\n";
    $sql="SELECT * FROM checklist WHERE site_id=$siteID AND category='press' ORDER BY checklist_order ASC";
    $dbChecklist=dbselectmulti($sql);
    if ($dbChecklist['numrows']>0)
    {
        foreach ($dbChecklist['data'] as $checkitem)
        {
            //lets see if this one has been added and is value=true for this job    
            $checkid=$checkitem['id'];
            $checkname=$checkitem['checklist_item'];
            $sql="SELECT * FROM job_checklist WHERE job_id=$jobid AND checklist_id=$checkid";
            $dbJobCheck=dbselectsingle($sql);
            if ($dbJobCheck['numrows']>0 && $dbJobCheck['data']['checklist_value']==1)
            {
                $checked="checklist_checked";
            } else {
                $checked="checklist_unchecked";
            }
            print "<div id='check_$checkid' class='$checked' onClick='checklistChange(\"$checkid\");'>\n";
            print $checkname;
            print "</div>\n";
        }
        
        //see if a pressman has approved this checklist already
        $sql="SELECT checklist_approved FROM job_stats WHERE job_id=$jobid";
        $dbOp=dbselectsingle($sql);
        if ($dbOp['numrows']>0)
        {
            if ($dbOp['data']['checklist_approved']!=0){$opid=$dbOp['data']['checklist_approved'];}else{$opid=0;}
        } else {
            $opid=0;
        }
        //sign off sections
        print "I hereby confirm that I have personally made sure the above checklist items have been completed.<br>Signed";
        print make_select('checklistOperator',$operators[$opid],$operators,false,"benchmarkChange(this.id,'');");
    }
    print "</div>\n";



}


function pagePanels($jobid)
{
    print "<div class='col-xs-12'>\n";
    print "<h4>Pages Remaining</h4>\n";
    print "<div id='pageslist' style='padding:2px;height:200px;overflow-y:auto;border:1px solid black;background-color:white'>\n";
    pageslist($jobid);
    print "</div>\n";
    print "</div>\n";
    
    print "<div class='col-xs-12'>\n";
    print "<h4>Plates Remaining</h4>\n";
    print "<div id='plateslist' style='padding:2px;height:200px;overflow-y:auto;border:1px solid black;background-color:white'>\n";
    plateslist($jobid);
    print "</div>\n";
    print "</div>\n";
    
    print "<div class='col-xs-12'>\n";
    print "<h4>Remakes</h4>\n";
    print "<div id='remakeslist' style='padding:2px;height:200px;overflow-y:auto;border:1px solid black;background-color:white'>\n";
    remakeslist($jobid);
    print "</div>\n";
    print "</div>\n";
}


?>
<script type="text/javascript">
setInterval("pressBoxes()",60000);    

$(".pressStopButton").fancybox({
        padding        : 5,
        openEffect     : 'elastic',
        closeEffect    : 'elastic',
        overlayColor   : '#000',
        overlayOpacity : 0.6,
        arrows         : false,
        modal          : true,
        scrolling      : 'no'
});
</script>

<!-- Modal -->
<div class="modal fade" id="pressNotesModal" tabindex="-1" role="dialog" aria-labelledby="pressNotesLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="pressNotesLabel">Job Notes</h4>
      </div>
      <div class="modal-body">
        <div id='overall_notes'>
        </div>
        <p><b>Press Notes</b></p>
        <textarea id='modal_job_notes' class='GuiEditor' style='width:100%;height:100px;'></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onClick='saveJobNotes()'>Save changes</button>
      </div>
    </div>
  </div>
</div>

<?php
$script = <<<SCRIPT
    $('#pressNotesModal').on('show.bs.modal', function (event) {
      var button = $(event.relatedTarget);
      var jobID = button.data('id');
      var modal =$(this);
      $.ajax({
          url: 'includes/ajax_handlers/jobmonitorPress.php',
          type: 'POST',
          data: {type: 'getNotes', jobid: jobID},
          dataType: 'json',
          success: function(response){
              //console.log(response);
              $('#overall_notes').html(response.notes);
              $('#modal_job_notes').val(response.press_notes);
              var editor =  $('#modal_job_notes').cleditor()[0];
              editor.updateFrame();
          }
      })
})

function saveJobNotes()
{
     var jobID = {$jobid};
     var notes = $('#modal_job_notes').val();
     $.ajax({
          url: 'includes/ajax_handlers/jobmonitorPress.php',
          type: 'POST',
          data: {type: 'saveNotes', jobid: jobID, notes:notes},
          dataType: 'json',
          success: function(response){
              $('#pressNotesModal').modal('hide');
              $('#on_page_press_notes').html(notes);
          }
      })
}
SCRIPT;
$Page->addScript($script);
 
$Page->footer();
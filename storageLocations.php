<?php
//<!--VERSION: .9 **||**-->
include("includes/boot.php") ;

if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}
    switch ($action)
    {
        case "move":
        changeParent('add');
        break;
        
        case "Move Item":
        save_move();
        break;
        
        case "Save Location":
        save_location('insert');
        break;
        
        case "Update Location":
        save_location('update');
        break;
        
        case "add":
        setup_location('add');
        break;
        
        case "edit":
        setup_location('edit');
        break;
        
        case "delete":
        setup_location('delete');
        break;
        
        case "list":
        setup_location('list');
        break;
        
        default:
        setup_location('list');
        break;
        
    } 
    
    
function setup_location($action)
{
    global $siteID, $locationTypes;
    $locationid=intval($_GET['locationid']);
    $parentid=intval($_GET['parentid']);
    $type=addslashes($_GET['type']);
    if ($action=='add' || $action=='edit')
    {
        if ($action=='add')
        {
            $button="Save Location";
            $type='inserts';
            $order=1;
            $notifications=0;
        } else {
            $button="Update Location";
            $sql="SELECT * FROM storage_locations WHERE id=$locationid";
            $dbLocations=dbselectsingle($sql);
            $location=$dbLocations['data'];
            $name=stripslashes($location['location_name']);
            $type=stripslashes($location['location_type']);
            $order=stripslashes($location['location_order']);
            $notifications=stripslashes($location['notifications']);
            $notification_email=stripslashes($location['notification_email']);
        }
        print "<form method=post class='form-horizontal'>\n";
        make_select('location_type',$locationTypes[$type],$locationTypes,'Storage Type');
        make_text('location_name',$name,'Location Name');
        make_radiocheck('checkbox','notifications',$notifications,'Get notifications','Receive notifications when a transfer is made/requested');
        make_text('notification_email',$notification_email,'Notification Email','Email that will receive notifications');
        make_number('location_order',$order,'Location Order');
        make_hidden('locationid',$locationid);
        make_hidden('parentid',$parentid);
        make_submit('submit',$button);
        print "</form>\n";  
    } elseif($action=='delete') {
        $sql="DELETE FROM storage_locations WHERE id=$locationid";
        $dbUpdate=dbexecutequery($sql);
        if ($error!='')
        {
            setUserMessage('There was a problem deleting the storage location.<br />'.$error,'error');
        } else {
            setUserMessage('The storage location has been successfully deleted.','success');
        }
        redirect("?action=list&parentid=$parentid&type=$type");
    } else {
        if ($_GET['parentid'])
        {
            $parentid=intval($_GET['parentid']);
        } else {
            $parentid=0;
        }
        if ($_GET['type'])
        {
            $type=addslashes($_GET['type']);
        } else {
            $type='inserts';
        }
        print "<form method=GET >";
        make_select('type',$type,$locationTypes,'Location Types');
        print "<input type=submit value='Select type' />";
        print "</form>\n";
        $temp=buildLocations($type);
        $sql="SELECT * FROM storage_locations WHERE location_type='$type' AND parent_id=$parentid ORDER BY location_order";
        $dbLocations=dbselectmulti($sql);
        tableStart("<a href='?action=list'>Return to top level</a>,<a href='?action=add&type=$type&parentid=$parentid'>Add new location</a>","Storage Type,Name",6);
        if ($dbLocations['numrows']>0)
        {
            foreach($dbLocations['data'] as $location)
            {
                $name=$location['location_name'];
                $locationid=$location['id'];
                $type=$locationTypes[$location['location_type']];
                $parentid=$location['parent_id'];
                print "<tr><td>$type</td><td>$name</td>";
                print "<td>
                <div class='btn-group'>
                  <a href='?action=edit&locationid=$locationid&parentid=$parentid' class='btn btn-dark'>Edit</a>
                  <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    <span class='caret'></span>
                    <span class='sr-only'>Toggle Dropdown</span>
                  </button>
                  <ul class='dropdown-menu'>
                    <li><a href='?action=list&parentid=$locationid'>Subs</a></li>
                    <li><a href='?action=move&locationid=$locationid&parentid=$parentid'>Move to new parent</a></li>
                    <li><a href='?action=delete&locationid=$locationid&parentid=$parentid' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
                  </ul>
                </div>
                </td>";
                print "</tr>\n";
            }
        }
        tableEnd($dbLocations);
        
    }
}

function save_location($action)
{
    global $siteID;
    $locationid=$_POST['locationid'];
    $parentid=$_POST['parentid'];
    $name=addslashes($_POST['location_name']);
    $order=addslashes($_POST['location_order']);
    $type=addslashes($_POST['location_type']);
    $notification_email=addslashes($_POST['notification_email']);
    if($_POST['notificaitons']){$notifications=1;}else{$notifications=0;}
    
    if ($action=='insert')
    {
        $sql="INSERT INTO storage_locations (location_type, location_name, location_order, parent_id, site_id,
        notifications, notification_email) VALUES ('$type','$name', '$order', '$parentid', '$siteID', '$notifications', 
        '$notification_email')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $sql="UPDATE storage_locations SET location_type='$type', location_name='$name', location_order='$order', 
        notifications='$notifications', notification_email='$notification_email' WHERE id=$locationid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the storage location.<br />'.$error,'error');
    } else {
        setUserMessage('The storage location has been successfully saved.','success');
    }
    redirect("?action=list&parentid=$parentid&type=$type");
    
}



function changeParent()
{
    global $siteID;
    
    $parentid=$_GET['parentid'];
    $locationid=$_GET['locationid'];
    $sql="SELECT id, location_name FROM storage_locations WHERE parent_id=0 AND site_id=$siteID ORDER BY location_order";
    $dbParents=dbselectmulti($sql);
    $parents[0]="TOP LEVEL";
    if ($dbParents['numrows']>0)
    {
        foreach($dbParents['data'] as $p)
        {
            $parents[$p['id']]=$p['location_name'];    
        }
    }
    print "<form method=post class='form-horizontal'>\n";
    make_select('newparent',$parents[$parentid],$parents,'New parent','Select the new parent for this item');
    make_hidden('parentid',$parentid);
    make_hidden('locationid',$locationid);
    make_submit('submit','Move Item');
    print "</form>\n";    
}

function save_move()
{
    $parentid=$_POST['parentid'];
    $locationid=$_POST['locationid'];
    $newparent=$_POST['newparent'];
    $sql="UPDATE storage_locations SET parent_id=$newparent WHERE id=$locationid";
    $dbUpdate=dbexecutequery($sql);
    if ($error!='')
    {
        setUserMessage('There was a problem updating the location.<br />'.$error,'error');
    } else {
        setUserMessage('The location has been successfully updated.','success');
    }
    redirect("?action=list&parentid=$newparent");
    
}


$Page->footer();
?>

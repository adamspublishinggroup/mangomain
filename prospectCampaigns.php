<?php
include("includes/boot.php");
if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}

switch ($action)
{
    case "Save Campaign":
    save_campaign('insert');
    break;
    
    case "Save Prospects":
    save_prospects();
    break;
    
    case "Update Campaign":
    save_campaign('update');
    break;
    
    case "add":
    setup_campaign('add');
    break;
    
    case "edit":
    setup_campaign('edit');
    break;
    
    case "delete":
    setup_campaign('delete');
    break;
    
    case "list":
    setup_campaign('list');
    break;
    
    case "prospects":
    manage_prospects();
    break;
    
    case "assignments":
    manage_assignments();
    break;
    
    default:
    setup_campaign('list');
    break;
}


function manage_prospects()
{
    ?>
    <style type='text/css'>
        .ui-autocomplete-multiselect.ui-state-default {
            display: block;
            background: #fff;
            border: 1px solid #ccc;
            padding: 3px 3px;
            padding-bottom: 0px;
            overflow: hidden;
            cursor: text;
        }

        .ui-autocomplete-multiselect .ui-autocomplete-multiselect-item .ui-icon {
            float: right;
            cursor: pointer;
        }

        .ui-autocomplete-multiselect .ui-autocomplete-multiselect-item {
            display: block;
            border: 1px solid #ccc;
            border-radius: 4px;
            padding: 1px 3px;
            margin-right: 2px;
            margin-bottom: 3px;
            color: #333;
            background-color: #f6f6f6;
        }

        .ui-autocomplete-multiselect input {
            display: inline-block;
            border: none;
            outline: none;
            height: auto;
            margin: 2px;
            overflow: visible;
            margin-bottom: 5px;
            text-align: left;
        }

        .ui-autocomplete-multiselect.ui-state-active {
            outline: none;
            border: 1px solid #7ea4c7;
            -moz-box-shadow: 0 0 5px rgba(50,150,255,0.5);
            -webkit-box-shadow: 0 0 5px rgba(50,150,255,0.5);
            -khtml-box-shadow: 0 0 5px rgba(50,150,255,0.5);
            box-shadow: 0 0 5px rgba(50,150,255,0.5);
        }

        .ui-autocomplete {
            border-top: 0;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>
    <?php
    
    $campaignid=intval($_GET['id']);
    
    //get cities
    $sql="SELECT DISTINCT(city) FROM prospects ORDER BY city";
    $dbCities=dbselectmulti($sql);
    $cities=array('all'=>'All Cities');
    if($dbCities['numrows']>0)
    {
        foreach($dbCities['data'] as $city)
        {
            if($city['city']!='')
            {
                $cities[$city['city']]=$city['city'];
            }
        }
    }
    
    //cet codes
    $sql="SELECT * FROM naics_codes";
    $dbCodes=dbselectmulti($sql);
    foreach($dbCodes['data'] as $c)
    {
        $codes[$c['naics_code']]=$c['naics_name'];
    }
    //load any existing settigns
    $sql="SELECT * FROM prospect_campaigns WHERE id=$campaignid";
    $dbCampaign=dbselectsingle($sql);
    $campaign=$dbCampaign['data'];
    
    print "<div class='col-sm-12 col-md-8'>";
        print "<form id='pform' method=post class='form-horizontal'>\n";
        print "<div class='form-group'>
    <label for='naics_codes' class='col-sm-2 control-label'>NAICS Categories</label>
    <div class='col-sm-10'>
      <small>Select one or more NAICS codes for prospects</small><br />
        <input type='text' id='naics_codes' class='form-control' name='naics_codes' size=60 />\n";
            print "<div id='codes' class='ui-autocomplete-multiselect'>\n";
            
            if($campaign['cat_list']!='')
            {
                $catlist=explode(",",stripslashes($campaign['cat_list']));
                foreach($catlist as $key=>$id)
                {
                    $naics=$codes[$id];
                    print "<div class='ui-autocomplete-multiselect-item' id='$id'>$naics<span class='ui-icon ui-icon-close' onclick='\$(this).parent().remove();'></span></div>\n";
                }
            }
            print "</div>\n\n";
        print "</div>
        </div>\n";
        
        print "<div class='form-group'>
    <label for='prospects_search' class='col-sm-2 control-label'>Prospects</label>
    <div class='col-sm-10'>
        <small>Find prospects by searching by name</small><br />
        <input type='text' id='prospects_search' name='prospects_search' class='form-control'  size=60 />";
        print "<div id='prospects' class='ui-autocomplete-multiselect'>\n";
            
            if($campaign['prospect_list']!='')
            {
                $sql="SELECT id, account_name, city FROM prospects WHERE id IN ($campaign[prospect_list])";
                $dbPros=dbselectmulti($sql);
                if($dbPros['numrows']>0)
                {
                    foreach($dbPros['data'] as $pro)
                    {
                        print "<div class='ui-autocomplete-multiselect-item' id='$pro[id]'>$pro[account_name] - $pro[city]<span class='ui-icon ui-icon-close' onclick='\$(this).parent().remove();'></span></div>\n";
                    }
                }
                
            }
            print "</div>\n\n";
        print "</div>
        </div>\n";
        
        print "<div class='form-group'>
    <label for='prospects_search' class='col-sm-2 control-label'>Cities</label>
    <div id='cities' class='col-sm-10'>
        <small>Select which cities you want to draw prospects from</small> <br>";
            $wrap=ceil(count($cities)/3);
            $i=1;
            print "<div style='width:130px;float:left;margin-right:10px;'>";
            $camp_cities=explode(",",str_replace("city_","",$campaign['city_list']));
            foreach($cities as $id=>$value)
            {
                
                if(in_array($value,$camp_cities))
                {
                    $checked = 'checked';
                } else {
                    $checked = '';
                }
                print "<label for='city_$id'><input type='checkbox' id='city_$id' name='city_$id' $checked> $value</label><br />";
                if($i>=$wrap)
                {
                    print "</div>";
                    print "<div style='width:130px;float:left;margin-right:10px;'>";
                    $i=1;
                } else {
                    $i++;
                }
            }
            print "</div><div class='clear'></div>\n";
        
        print "</div>
        </div>\n";
        
        make_hidden('campaignid',$campaignid);
        print "
        <div class='form-group'>
    <div class='col-sm-offset-2 col-sm-10'>
      <input type='button' class='btn btn-dark' onclick='getCodes(\"codes\",\"true\");' value='Save Prospects' />
    </div>
  </div>";
        
    print "</form>\n";
    print "</div>
    <div class='col-sm-12 col-md-4'>";
    print "
        <div class='form-group'>
    <div class='col-sm-offset-2 col-sm-10'>
      <input type='button' class='btn btn-dark' onclick='getCodes(\"codes\",\"false\");' value='Sample Prospects' />
    </div>
  </div>";
    print "<span id='response_count'>0</span> selected prospects<br />";
    print "<div id='prospect_list' style='height:600px;overflow-y:scroll;'>
        </div>";
        
    
    print "</div>";
    
    ?>
    <script>
    $(function(){
        var items=[];
        
        $( "#naics_codes" ).autocomplete({
            source: "includes/ajax_handlers/fetchNaicsCodes.php",
            select: function (event, ui)
            {
                var newItem = $("<div></div>")
                    .addClass("ui-autocomplete-multiselect-item")
                    .text(ui.item.label)
                    .append(
                        $("<span></span>")
                            .addClass("ui-icon ui-icon-close")
                            .click(function(){
                                var item = $(this).parent();
                                item.remove();
                            })
                    );
                newItem.prop('id',ui.item.value);
                newItem.data('code',ui.item.value);
                    
                newItem.appendTo($('#codes'));
                
                items[ui.item.label] = ui.item;
                $('#naics_codes').val('');
                return false;
            }
        });
        
        $( "#prospects_search" ).autocomplete({
            source: "includes/ajax_handlers/fetchProspects.php",
            select: function (event, ui)
            {
                var newItem = $("<div></div>")
                    .addClass("ui-autocomplete-multiselect-item")
                    .text(ui.item.label)
                    .append(
                        $("<span></span>")
                            .addClass("ui-icon ui-icon-close")
                            .click(function(){
                                var item = $(this).parent();
                                item.remove();
                            })
                    );
                newItem.prop('id',ui.item.value);
                newItem.data('code',ui.item.value);
                    
                newItem.appendTo($('#prospects'));
                
                items[ui.item.label] = ui.item;
                return false;
            }
        });
        
        
    })
    
    function getCodes(type,save)
    {
        //grab ids of all items from the correct pile
        var nai_ids=[],
            prosp_ids=[];
        var city_list=[];    
        if(type=='codes')
        {
            var container = $('#naics_codes');
        } else {
            var container = $('#prospects_search');
        }
       $("#codes").children("div").each(function(i,item){
            nai_ids[i]=item.id;
        })
       $("#prospects").children("div").each(function(i,item){
            prosp_ids[i]=item.id;
        })
        
       //if you want to do something specific for each check box
        $("#cities :checkbox").each(function () {
              //do it here
              if (this.checked)
                  city_list[city_list.length]=this.id;
        });
 
       $.ajax({
            type: "POST",
            url: "includes/ajax_handlers/fetchCampaignProspects.php",
            data: { campaign_id: $('#campaignid').val(), nai_ids: nai_ids.join(','), prosp_ids: prosp_ids.join(','),cities: city_list.join(','),saving: save },
            dataType : 'json',
            success: function(response){
                if(save=='true')
                {
                    $('#pform').submit();    
                } else {
                    $('#response_count').html(response.count);
                    var prospects=response.names;
                    $.each(prospects,function(id,prospect)
                    {
                        var newName=$("<span id='"+id+"'>"+prospect+"</span><br />");
                        $('#prospect_list').append(newName);
                    }) 
                }  
            }
       });
                      
    }
    </script>
    <?php
}

function save_prospects()
{
    print_r($_POST);
}

function setup_campaign($action)
{
    $id=intval($_GET['id']);
    if($action=='add' || $action=='edit')
    {
        if($action=='add')
        {
           $start=date("Y-m-d");
           $end=date("Y-m-d",strtotime("+1 week")); 
           $button="Save Campaign"; 
        } else {
           $sql="SELECT * FROM prospect_campaigns WHERE id=$id";
           $dbCampaign=dbselectsingle($sql);
           $campaign=$dbCampaign['data'];
           $name=stripslashes($campaign['campaign_name']);
           $start=$campaign['start_date'];
           $end=$campaign['end_date'];
           $notes=stripslashes($campaign['notes']);
           $button="Update Campaign";
        } 
        print "<form method=post class='form-horizontal'>";
        make_text('name',$name,'Name');
        make_date('start',$start,'Start Date');
        make_date('end',$end,'End Date');
        make_textarea('notes',$notes,'Notes about this campaign (how to sell, offers, etc.)');
        make_hidden('id',$id);
        make_submit('submit',$button);
        print "</form>\n"; 
    } elseif($action=='delete')
    {
        $sql="DELETE FROM prospect_campaign_xref WHERE campaign_id=$id";
        $dbDelete=dbexecutequery($sql);
        $sql="DELETE FROM prospect_campaigns WHERE id=$id";
        $dbDelete=dbexecutequery($sql);
        redirect("?action=list");
    } else {
        $sql="SELECT * FROM prospect_campaigns ORDER BY start_date";
        $dbGroups=dbselectmulti($sql);
        tableStart("<a href='?action=add'>Add new campaign</a>","Campaign Name, Start Date, End Date",8);
        if ($dbGroups['numrows']>0)
        {
            foreach($dbGroups['data'] as $group)
            {
                $name=stripslashes($group['campaign_name']);
                $start=date("m/d/Y",strtotime($group['start_date']));
                $end=date("m/d/Y",strtotime($group['end_date']));
                $id=$group['id'];
                print "<tr><td>$name</td><td>$start</td><td>$end</td>";
                print "<td style='width:130px;'>
            <div class='btn-group' >
              <a href='?action=edit&id=$id' class='btn btn-dark'>Edit </a>
              <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <span class='caret'></span>
                <span class='sr-only'>Toggle Dropdown</span>
              </button>
              <ul class='dropdown-menu'>
                <li><a href='?action=prospects&id=$id'> Prospects</a></li>
                <li><a href='?action=assignments&id=$id'> Assignments</a></li>
                <li><a href='prospecting.php?id=$id'> Prospecting</a></li>
                <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
              </ul>
            </div>
            </td>
            ";
                print "</tr>";
            }
        }
        tableEnd($dbGroups);
    } 
}

function save_campaign($action)
{
    $id=$_POST['id'];
    $name=addslashes($_POST['name']);
    $notes=addslashes($_POST['notes']);
    $start=$_POST['start'];
    $end=$_POST['end'];
    if($action=='insert')
    {
        $sql="INSERT INTO prospect_campaigns (campaign_name, notes, start_date, end_date) VALUES ('$name', '$notes', '$start', '$end')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $sql="UPDATE prospect_campaigns SET campaign_name='$name', notes='$notes', start_date='$start', end_date='$end' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    if ($error!='')
    {
        setUserMessage('There was a problem saving the prospect.<br>'.$error,'error');
    } else {
        setUserMessage('The prospect was successfully saved.','success');
    }
    redirect("?action=list");
}

$Page->footer();
<?php
//<!--VERSION: .9 **||**-->

if($_GET['action']=='exportall' || $_GET['action']=='exportemailonly')
{
    include("includes/functions_db.php");
    if($_GET['action']=='exportall'){
        $sql="SELECT A.*, B.department_name, C.position_name FROM users A, user_departments B, user_positions C WHERE A.position_id=C.id AND A.department_id=B.id ORDER BY A.department_id, A.lastname, A.firstname";
    } elseif($_GET['action']=='exportemailonly') {
        $sql="SELECT A.*, B.department_name, C.position_name FROM users A, user_departments B, user_positions C WHERE A.email<>'' AND A.position_id=C.id AND A.department_id=B.id ORDER BY A.department_id, A.lastname, A.firstname";
    }
    $dbStaff=dbselectmulti($sql);
    if($dbStaff['numrows']>0)
    {
        header('Content-Type: text/csv');
        header("Content-Disposition: attachment; filename='employees.csv'");
        print "First Name,Last Name,Department,Title,Phone,Extension,Cell,Email,Master Key,Sub-master Key\n";
        foreach($dbStaff['data'] as $staff)
        {
            print stripslashes($staff['firstname']).",";    
            print stripslashes($staff['lastname']).",";    
            print stripslashes($staff['department_name']).",";    
            print stripslashes($staff['position_name']).",";    
            print stripslashes($staff['business']).",";    
            print stripslashes($staff['extension']).",";    
            print stripslashes($staff['cell']).",";    
            print stripslashes($staff['email']).",";    
            print stripslashes($staff['master_key']).",";    
            print stripslashes($staff['submaster_key'])."\n";    
        }
        
    } else {
        ?>
        <html>
        <body>
        <h2>There was a problem and no staff members where found.</h2>
        <?php 
        echo $dbStaff['error']."<br>";
        ?>
        </body>
        </html>
        <?php
        
    }
    die();
} else {
    include("includes/boot.php");

    if ($_POST['submit']=='Add'){
        save_user('insert');
    } elseif ($_POST['submit']=='Change Password'){
        update_password(); 
    } elseif ($_POST['submit']=='Update'){
       save_user('update'); 
    } elseif ($_POST['submit']=='Set Permissions'){
       save_permissions(); 
    } elseif ($_POST['submit']=='Set Publications'){
       save_publications(); 
    } elseif ($_POST['submit']=='Save Sites'){
       save_sites(); 
    } elseif ($_POST['submit']=='Save Rooms'){
       save_rooms(); 
    } elseif ($_POST['submit']=='Set Groups'){
       save_groups(); 
    } elseif ($_POST['submit']=='Reset Password'){
       save_reset(); 
    } elseif ($_GET['action']=="add" || $_GET['action']=="edit"){
        edit_user();
    } elseif ($_GET['action']=="password"){
        change_password($id);
    } elseif ($_GET['action']=='delete'){
        delete_user();
    } elseif ($_GET['action']=='permissions') {
        permissions();   
    } elseif ($_GET['action']=='pubs') {
        publications();   
    } elseif ($_GET['action']=='rooms') {
        rooms();   
    } elseif ($_GET['action']=='sites') {
        sites();   
    } elseif ($_GET['action']=='groups') {
        groups();   
    } elseif ($_GET['action']=='resetpassword') {
        reset_password(); 
    } else { 
        show_users();
    }
}

function edit_user()
{
    global $departments, $employeepositions, $carriers;
    
    $sql="SELECT * FROM core_permission_groups ORDER BY group_name";
    $dbPgroups=dbselectmulti($sql);
    $pgroups[0]='Please choose';
    if($dbPgroups['numrows']>0)
    {
        foreach($dbPgroups['data'] as $p)
        {
            $pgroups[$p['id']]=$p['group_name'];    
        }
    }
    $id=intval($_GET['userid']);
    if ($_GET['action']=='add') {
        $button="Add";
        $allpubs=0;
        $position=0;
        $department=0;
        $carrier=0;
        $pgroup=0;
        $debug=0;
        $tempemployee=0;
        $simplemenus=0;
      } elseif ($_GET['action']=='edit'){
        $sql="SELECT * FROM users WHERE id=$id";
        $dbresult=dbselectsingle($sql);
        $record=$dbresult['data'];
        $firstname=stripslashes($record['firstname']);
        $lastname=stripslashes($record['lastname']);
        $middlename=stripslashes($record['middlename']);
        $home=stripslashes($record['home']);
        $business=stripslashes($record['business']);
        $cell=stripslashes($record['cell']);
        $carrier=stripslashes($record['carrier']);
        $fax=stripslashes($record['fax']);
        $extension=stripslashes($record['extension']);
        $email=stripslashes($record['email']);
        $emaildomain=stripslashes($record['emaildomain']);
        $username=stripslashes($record['username']);
        $weight=stripslashes($record['weight']);
        $summary=stripslashes($record['summary']);
        $allpubs=stripslashes($record['allpubs']);
        $admin=stripslashes($record['admin']);
        $mugshot=stripslashes($record['mugshot']);
        $emailpassword=stripslashes($record['email_password']);
        $netpassword=stripslashes($record['network_password']);
        $notes=stripslashes($record['notes']);
        $tempemployee=$record['temp_employee'];
        $department=$record['department_id'];
        $position=$record['position_id'];
        $pgroup=$record['permission_group'];
        $vdsalesid=$record['vision_data_sales_id'];
        $vdsalesname=$record['vision_data_sales_name'];
        $debug=$record['debug_user'];
        $simplemenus=$record['simple_menu'];
        $bctitle=stripslashes($record['businesscard_title']);
        $button="Update";
    }
    
    print "<div>\n";
        print "<form method=post class='form-horizontal' enctype='multipart/form-data'>\n";
            
            print "<ul id='userInfo' class=\"nav nav-tabs\" role=\"tablist\">\n";
               print '
               <li role="presentation" class="active">
                <a href="#basics" aria-controls="basics" role="tab" data-toggle="tab">Basics</a>
               </li>
               <li role="presentation">
                <a href="#details" aria-controls="details" role="tab" data-toggle="tab">Details</a>
               </li>
               <li role="presentation">
                <a href="#access" aria-controls="access" role="tab" data-toggle="tab">Access</a>
               </li>
               <li role="presentation">
                <a href="#advanced" aria-controls="advanced" role="tab" data-toggle="tab">Advanced</a>
               </li>
               ';
            print "
            </ul>\n";
         
            print "<div id='tabs' class='tab-content'>\n"; //begins wrapper for tabbed content
                print '
                <div role="tabpanel" class="tab-pane active" id="basics">
                ';
                    make_select('department',$departments[$department],$departments,'Department');
                    make_select('position',$employeepositions[$position],$employeepositions,'Position');
                    make_text('firstname',$firstname,'First Name','',50);
                    make_text('middlename',$middlename,'Middle Name','',50);
                    make_text('lastname',$lastname,'Last Name','',50,false,false,'','','createUsername();');
                    make_email('email',$email,'Email Address','',50);
                    if(checkPermission(1,'function'))
                    {
                        make_select('pgroup',$pgroups[$pgroup],$pgroups,'Permission Group','Base set of permissions for this user');
                    } else {
                        make_hidden('pgroup',$pgroup);
                    }
                print "</div>\n";
                
                print '
                <div role="tabpanel" class="tab-pane" id="details">
                ';
                   make_text('bctitle',$bctitle,'Title for business cards','Enter a title as it would appear on business cards and email signatures',50);
                   make_file('mugshot','Picture','',$mugshot);
                   make_phone('business',$business,'Office Number','This is the full number, not just the extention.','20');
                   make_text('extension',$extension,'Extension Number','This is the internal office extention.','10');
                   make_phone('home',$home,'Home Number');
                   make_select('carrier',$carriers[$carrier],$carriers,'Cell Carrier','Select the cell service for this number');
                   make_phone('cell',$cell,'Cell Number');
                    
                print "</div>\n";
                
                print '
                <div role="tabpanel" class="tab-pane" id="access">
                ';
                
                    make_checkbox('allpubs',$allpubs,'All Pubs',' Auto enable all new publications');
                    if(checkPermission(1,'function'))
                    {
                        make_checkbox('admin',$admin,'Admin',' User is an admin');
                        make_checkbox('debug',$debug,'Debugger',' Check if this user is allowed to see debugging messages.');
                    } else {
                        make_hidden('admin',$admin);
                        make_hidden('debug',$debug);
                    }
                    make_checkbox('simplemenus',$simplemenus,'Simple Menus',' Check this to display the simplified menu system');
                    make_text('username',$username,'Username');
                    
                    if ($_GET['action']=='add')
                    {
                        make_text('password',substr(md5(time()),0,8),'Password');
                        
                    } else {
                        make_text('password','','Password','Enter new password to change, leave blank to keep it the same'); 
                    }
                print "</div>\n";
                
                print '
                <div role="tabpanel" class="tab-pane" id="advanced">
                ';
                    make_text('salesid',$vdsalesid,'Ad System Sales ID','Enter the sales id of this employee if they are a salesperson.');
                    make_text('salesname',$vdsalesname,'Ad System Sales Name','Enter the ad system sales name exactly as it appears.');
                    make_checkbox('summary',$summary,'Summary',' Receive daily production summary');
                    
                    if(checkPermission(1,'function'))
                    {    
                        make_text('emailpassword',$emailpassword,'Email Password','Password for email system');
                        make_text('netpassword',$netpassword,'Network Password','Custom network password that user uses');
                    }
                    make_checkbox('tempemployee',$tempemployee,'Temp Employee',' Check this an employee who is a temp or contract worker.');
                    make_number('hours',$hours,'Weekly Hours');
                    make_textarea('notes',$notes,'Notes','Notes about employee',60,10);
                print "</div>\n";
            
            print "</div>\n";
            make_submit('submit',$button);
            make_hidden('userid',$id);
        print "</form>\n";
    print "</div>\n";
}

function show_users()
{
    global $departments, $employeepositions, $carriers;
    
    $sql="SELECT * FROM core_permission_groups ORDER BY group_name";
    $dbPgroups=dbselectmulti($sql);
    $pgroups[0]='Please choose';
    if($dbPgroups['numrows']>0)
    {
        foreach($dbPgroups['data'] as $p)
        {
            $pgroups[$p['id']]=$p['group_name'];    
        }
    } 
   
    $sql="SELECT * FROM users WHERE site_id=".SITE_ID." ORDER BY lastname";
    $dbUsers=dbselectmulti($sql);
    
    tableStart("<a href='?action=add'>Add new staff member</a>,<a href='?action=exportall'>Export All Employees</a>,<a href='?action=exportemailonly'>Export Employees with email addresses</a>,<a href='?action=exportwiki'>Export to Wiki</a>","Name,Username");
    
    if ($dbUsers['numrows']>0)
    {
        foreach ($dbUsers['data'] as $user) {
            $id=$user['id'];
            $firstname=stripslashes($user['firstname']);
            $lastname=stripslashes($user['lastname']);
            $username=stripslashes($user['username']);
            print "<tr><td><a href='?action=edit&userid=$id'>$lastname, $firstname</a></td>";
            //print "<td><a href='?action=password&userid=$id'>Change password</a></td>";
            print "<td>$username</td>";
             print "<td>
                <div class='btn-group'>
                  <a href='?action=edit&userid=$id' class='btn btn-dark'>Edit</a>
                  <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    <span class='caret'></span>
                    <span class='sr-only'>Toggle Dropdown</span>
                  </button>
                  <ul class='dropdown-menu'>
                    <li><a href='?action=groups&userid=$id'>Groups</a></li>
                    <li><a href='?action=rooms&userid=$id'>Chat Rooms</a></li>
                    <li><a href='?action=pubs&userid=$id'>Publications</a></li>
                    ";
                    if (checkPermission('3','item'))
                    {
                        print "<li><a href='?action=permissions&userid=$id'>Permissions</a></li>";
                    } 
                    if (checkPermission('27','item'))
                    {
                        print "<li><a href='?action=resetpassword&userid=$id'>Reset Password</a></li>";
                    } 
                    if (checkPermission('23','item'))//grant site access
                    {
                        print "<li><a href='?action=sites&userid=$id'>Sites</a></li>";
                    } 
                    print " <li><a href='?action=delete&userid=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
                  </ul>
                </div>
                </td>";
                
            print "</tr>\n";      
        }
    }
    tableEnd($dbUsers);   

}

function permissions()
{
    $userid=$_GET['userid'];
    $sql="SELECT * FROM users WHERE id=$userid";
    $dbUser=dbselectsingle($sql);
    $user=$dbUser['data'];
    $sql="SELECT * FROM core_permission_list WHERE type='cms' ORDER BY displayname";
    $dbPermissions=dbselectmulti($sql);
    $sql="SELECT * FROM user_permissions WHERE user_id=$userid";
    $dbStaff=dbselectmulti($sql);
    $staffpermissions=$dbStaff['data'];
    if ($dbPermissions['numrows']>0)
    {
        print "<h4>Please select permissions for $user[firstname] $user[lastname]:</h4>";
        print "<input type='button' value='Select All' onClick=\"checkAllCheckboxes('permList',true);\">\n";  
        print "<input type='button' value='Deselect All' onClick=\"checkAllCheckboxes('permList',false);\"><br />\n";  
        print "<div id='permList'>\n";
        print "<form method=post class='form-horizontal'>\n";
        $i=1;
        //split 3 columns
        $col=round($dbPermissions['numrows']/3,0);
        print "<div style='float:left;width:250px;margin-right:10px;'>\n";
        foreach($dbPermissions['data'] as $permission)
        {
            $pvalue=0;
            if ($dbStaff['numrows']>0)
            {
                foreach($staffpermissions as $staffpermission)
                {
                    if ($permission['id']==$staffpermission['permissionID'])
                    {
                        if ($staffpermission['value']==1)
                        {
                            $pvalue=1;
                        }        
                    }
                }
            }
            print make_checkbox('permission_'.$permission['id'],$pvalue);
            print "<label for='permission_$permission[id]'>&nbsp;&nbsp;".$permission['displayname']."</label><br>";
            if ($i==$col)
            {
                $i=1;
                print "</div>\n";
                print "<div style='float:left;width:250px;margin-right:10px;'>\n";
            } else {
                $i++;
            }
        }
        print "</div><div class='clear'></div>\n";
        print "<div class='label'></div><div class='input'>\n";
        make_hidden('userid',$userid);
        make_submit('submit','Set Permissions');
        print "</form>\n";
        print "</div><div class='clear'></div>\n";
        print "</div>\n";
        
    } else {
       print "Sorry, no permissions have been defined yet.";
    }
    
}

function save_permissions()
{
    $userid=$_POST['userid'];
    //start by deleting all existing permissions for this user
    $sql="DELETE FROM user_permissions WHERE user_id=$userid";
    $dbDelete=dbexecutequery($sql);
    $sql="SELECT * FROM core_permission_list WHERE type='cms' ORDER BY weight";
    $dbPermissions=dbselectmulti($sql);
    $value="";
    foreach ($dbPermissions['data'] as $permission)
    {
        $pvalue=0;
        if ($_POST["permission_$permission[id]"])
        {
            $pvalue=1;
        } else if($permission['auto_enable']==1)
        {
            $pvalue=1;
        }
        $value.="('$permission[id]','$userid','$pvalue'),";
    }
    $value=substr($value,0,strlen($value)-1);
    if($value!='')
    {
        $sql="INSERT INTO user_permissions (permissionID, user_id, value) VALUES $value";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $error='';
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the user permissions.<br>'.$error,'error');
    } else {
        setUserMessage('User permission successfully saved','success');
    }
    clearCache('menu');
    redirect("?action=list");
    
}


function publications()
{
    $userid=$_GET['userid'];
    $sql="SELECT * FROM users WHERE id=$userid";
    $dbUser=dbselectsingle($sql);
    $user=$dbUser['data'];
    $sql="SELECT * FROM publications ORDER BY pub_name";
    $dbPublications=dbselectmulti($sql);
    $sql="SELECT * FROM user_publications WHERE user_id=$userid";
    $dbUsers=dbselectmulti($sql);
    if ($dbPublications['numrows']>0)
    {
        print "<h4>Please set publications for $user[firstname] $user[lastname]:</h4>";
        print "<input type='button' value='Select All' onClick=\"checkAllCheckboxes('publist',true);\">\n";  
        print "<input type='button' value='Deselect All' onClick=\"checkAllCheckboxes('publist',false);\"><br />\n";  
        print "<form method=post class='form-horizontal'>\n";
        $i=1;
        //split 2 columns
        $col=round($dbPublications['numrows']/3,0);
        print "<div class='publist' style='float:left;width:250px;margin-right:10px;'>\n";
        foreach($dbPublications['data'] as $publication)
        {
            $pvalue=0;
            if ($dbUsers['numrows']>0)
            {
                foreach($dbUsers['data'] as $userpub)
                {
                    if ($publication['id']==$userpub['pub_id'])
                    {
                        if ($userpub['value']==1)
                        {
                            $pvalue=1;
                        }        
                    }
                }
            }
            print make_checkbox('publication_'.$publication['id'],$pvalue);
            print "&nbsp;&nbsp;<label for='publication_$publication[id]'>".$publication['pub_name']."</label><br>";
            if ($i==$col)
            {
                $i=1;
                print "</div>\n";
                print "<div class='publist' style='float:left;width:250px;margin-right:10px;'>\n";
            } else {
                $i++;
            }
        }
        print "</div><div class='clear'></div>\n";
        make_hidden('userid',$userid);
        print "<div class='label'></div><div class='input'>\n";
        make_submit('submit','Set Publications');
        print "</form>\n";
        print "</div>\n";
        print "<div class='clear'></div></div>\n";
    } else {
       print "Sorry, no publications have been defined yet.";
    }
    
}

function save_publications()
{
    $userid=$_POST['userid'];
    //start by deleting all existing permissions for this user
    $sql="DELETE FROM user_publications WHERE user_id=$userid";
    $dbDelete=dbexecutequery($sql);
    $sql="SELECT * FROM publications";
    $dbPublications=dbselectmulti($sql);
    $value="";
    foreach ($dbPublications['data'] as $publication)
    {
        $pvalue=0;
        if ($_POST["publication_$publication[id]"])
        {
            $pvalue=1;
        }
        $value.="('$publication[id]','$userid','$pvalue'),";
    }
    $value=substr($value,0,strlen($value)-1);
    if($value!='')
    {
        $sql="INSERT INTO user_publications (pub_id, user_id, value) VALUES $value";
        $dbinsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $error='';
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the publication for the user.<br>'.$error,'error');
    } else {
        setUserMessage('Publication for the user successfully saved','success');
    }
    redirect("?action=list");//&where=$_POST[where]&order=$_POST[order]");
    
}

function sites()
{
    $userid=$_GET['userid'];
    $sql="SELECT * FROM core_sites ORDER BY site_name";
    $dbSites=dbselectmulti($sql);
    if ($dbSites['numrows']>0)
    {
        print "<h2>Select the sites that this user is a member of.</h2>\n";
        print "<form method=post class='form-horizontal'>\n";
        foreach($dbSites['data'] as $site)
        {
            //see if the employee has this one
            $sql="SELECT * FROM user_sites WHERE site_id=$site[id] AND user_id=$userid";
            $dbExisting=dbselectsingle($sql);
            if ($dbExisting['numrows']>0){$checked=1;}else{$checked=0;}
            print make_checkbox('site_'.$site['id'],$checked)." <label for='site_$site[id]'>".$site['site_name']."</label><br />\n";    
        }
        make_hidden('userid',$userid);
        make_submit('submit','Save Sites');
        print "</form>\n";
    } else {
        print "<a href='?action=list'>Sorry, there are no sites configured yet. Click to return to user list.</a><br />\n";
    } 
    
}

function save_sites()
{
    $userid=$_POST['userid'];
    $values="";
    //clear existing
    $sql="DELETE FROM user_sites WHERE user_id=$userid";
    $dbDelete=dbexecutequery($sql);
    foreach($_POST as $key=>$value)
    {
        if (substr($key,0,5)=='site_')
        {
            $id=str_replace("site_","",$key);
            $values.="($userid,$id), ";    
        }
    }
    $values=substr($values,0,strlen($values)-2);
    if($values!='')
    {
        $sql="INSERT INTO user_sites (user_id, site_id) VALUES $values";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $error='';
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the site for the user.<br>'.$error,'error');
    } else {
        setUserMessage('User site successfully saved','success');
    }
    redirect("?action=list");
}


function rooms()
{
    $userid=$_GET['userid'];
    $sql="SELECT * FROM chat_rooms ORDER BY room_name";
    $dbRooms=dbselectmulti($sql);
    if ($dbRooms['numrows']>0)
    {
        print "<h2>Select the chatrooms that this user should have access to:</h2>\n";
        print "<form method=post class='form-horizontal'>\n";
        foreach($dbRooms['data'] as $room)
        {
            //see if the employee has this one
            $sql="SELECT * FROM user_chatrooms WHERE room_id=$room[id] AND user_id=$userid";
            $dbExisting=dbselectsingle($sql);
            if ($dbExisting['numrows']>0){$checked=1;}else{$checked=0;}
            print make_checkbox('room_'.$room['id'],$checked)." <label for='room_$room[id]'>".$room['room_name']."</label><br />\n";    
        }
        make_hidden('userid',$userid);
        make_submit('submit','Save Rooms');
        print "</form>\n";
    } else {
        print "<a href='?action=list'>Sorry, there are no chat rooms configured yet. Click to return to user list.</a><br />\n";
    } 
    
}
function save_rooms()
{
    $userid=$_POST['userid'];
    $values="";
    //clear existing
    $sql="DELETE FROM user_chatrooms WHERE user_id=$userid";
    $dbDelete=dbexecutequery($sql);
    foreach($_POST as $key=>$value)
    {
        if (substr($key,0,5)=='room_')
        {
            $id=str_replace("room_","",$key);
            $values.="($userid,$id), ";    
        }
    }
    $values=substr($values,0,strlen($values)-2);
    if($values!='')
    {
        $sql="INSERT INTO user_chatrooms (user_id, room_id) VALUES $values";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        
    } else {
        $error='';
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the chat rooms for the user.<br>'.$error,'error');
    } else {
        setUserMessage('User chat rooms successfully saved','success');
    }
    redirect("?action=list");  
}



function delete_user()
{
    $id=$_GET['userid'];
    $sql="DELETE from users WHERE id=$id";   
    $result=dbexecutequery($sql);
    $error=$result['error'];
    $sql="DELETE from user_permissions WHERE user_id=$id";   
    $result=dbexecutequery($sql);
    $error.=$result['error'];
    $sql="DELETE from user_publications WHERE user_id=$id";   
    $result=dbexecutequery($sql);
    $error.=$result['error'];
    $sql="DELETE from user_groups_xref WHERE user_id=$id";   
    $result=dbexecutequery($sql);
    $error.=$result['error'];
    $sql="DELETE from user_sites WHERE user_id=$id";   
    $result=dbexecutequery($sql);
    $error.=$result['error'];
    if ($error!='')
    {
        setUserMessage('There was a problem deleting the user.<br>'.$error,'error');
    } else {
        setUserMessage('User successfully deleted','success');
    }
    redirect("?action=list");
}

function reset_password()
{
    $userid=$_GET['userid'];
    print "<form method='post'>\n";
    make_password('userpassword',$_POST['userpassword'],'New password');
    make_password('confirmpassword','','Re-enter');
    make_hidden('userid',$userid);
    make_submit('submit','Reset Password');
    print "</form>\n";    
}

function save_reset()
{
    $userid=$_POST['userid'];
    $password=md5($_POST['userpassword']);
    $confirm=md5($_POST['confirmpassword']);
    //lets see if the new and confirming one are the same
    if ($password===$confirm)
    {
        $sql="UPDATE users SET password='$password' WHERE id=$userid";
        $dbUser=dbexecutequery($sql);
        if ($error!='')
        {
            setUserMessage('There was a problem saving the new password','error');
        } else {
            setUserMessage('New password successfully updated','success');
        }
        redirect("?action=edit&userid=$userid");
               
    } else {
        print "Your new passwords do not match!<br />\n";
        reset_password($userid);
    }
}


function change_password($userid='')
{
    if ($userid=='')
    {
        $userid=$_GET['userid'];
    }
    print "<form method='post'>\n";
    make_password('original','','Original Password');
    make_password('userpassword',$_POST['userpassword'],'New password');
    make_password('confirmpassword','','Re-enter');
    make_hidden('userid',$userid);
    make_submit('submit','Change Password');
    print "</form>\n";
}

function update_password()
{
    $userid=intval($_POST['userid']);
    $sql="SELECT password FROM users WHERE id=$userid";
    $dbExisting=dbselectsingle($sql);
    if($dbExisting['numrows']>0)
    {
        if( password_verify( $_POST['original'], $dbExisting['data']['password'] ) )
        {        
            //ok, they entered the correct password
            //lets see if the new and confirming one are the same
            if ($_POST['userpassword']===$_POST['confirmpassword'])
            {
                $password=password_hash($_POST['userpassword'],PASSWORD_DEFAULT);
      
                $sql="UPDATE users SET password='$password', password_old=0 WHERE id=$userid";
                $dbUser=dbexecutequery($sql);
                if ($error!='')
                {
                    setUserMessage('There was a problem updating the user password','error');
                } else {
                    setUserMessage('User password successfully updated','success');
                }
                redirect("?action=edit&userid=$userid");       
            } else {
                print "Your new passwords do not match!<br />\n";
                change_password($userid);
            }
        
        } else {
            print "You entered the wrong original password!<br />\n";
            change_password($userid);
        }
  }
} 



function save_user($action)
{
    $userid=intval($_POST['userid']);
    $username=trim(addslashes($_POST['username']));
    $firstname=addslashes($_POST['firstname']);
    $lastname=addslashes($_POST['lastname']);
    $middlename=addslashes($_POST['middlename']);
    //we need the id of this record, so...
    $home=addslashes($_POST['home']);
    $business=addslashes($_POST['business']);
    $cell=addslashes($_POST['cell']);
    $fax=addslashes($_POST['fax']);
    $extension=addslashes($_POST['extension']);
    $email=addslashes($_POST['email']);
    $title=addslashes($_POST['title']);
    $bctitle=addslashes($_POST['bctitle']);
    
    $emailpassword=addslashes($_POST['emailpassword']);
    $netpassword=addslashes($_POST['netpassword']);
    
    $password=password_hash($_POST['password'],PASSWORD_DEFAULT);
  
    $notes=addslashes($_POST['notes']);
    $position=intval($_POST['position']);
    $department=intval($_POST['department']);
    $carrier=addslashes($_POST['carrier']);
    $pgroup=intval($_POST['pgroup']);
    $salesid=addslashes($_POST['salesid']);
    $salesname=addslashes($_POST['salesname']);
    $cell=str_replace("-","",$cell);
    $cell=str_replace(" ","",$cell);
    $cell=str_replace("(","",$cell);
    $cell=str_replace(")","",$cell);
    $cell=str_replace(".","",$cell);
    
    if ($_POST['summary']){$summary=1;}else{$summary=0;}
    if ($_POST['allpubs']){$allpubs=1;}else{$allpubs=0;}
    if ($_POST['admin']){$admin=1;}else{$admin=0;}
    if ($_POST['debug']){$debug=1;}else{$debug=0;}
    if ($_POST['tempemployee']){$tempemployee=1;}else{$tempemployee=0;}
    if ($_POST['superuser']){$superuser=1;}else{$superuser=0;}
    if ($_POST['simplemenus']){$simplemenus=1;}else{$simplemenus=0;}
    
    
    /*
    @TODO need to deprecate all of these data fields
    */
    $extensionid=0;
    $pims=1;
    $mango=1;
   
    if ($action=='insert'){
        $token = generate_random_string(64);
        $sql="INSERT INTO users (allpubs, admin, username, firstname, middlename, lastname, extension, business, home, cell, fax, email, position_id, department_id, summary, carrier, permission_group, site_id, notes, sales_id, debug_user, temp_employee, simple_menu, sales_name, super_user, businesscard_title, token, password_old) 
          VALUES ('$allpubs', '$admin', '$username', '$firstname', '$middlename', '$lastname', '$extension', '$business', '$home', '$cell', '$fax', '$email', '$position', '$department', '$summary', '$carrier', '$pgroup', '".SITE_ID."',  
          '$notes', '$salesid', '$debug', '$tempemployee', '$simplemenus', '$salesname', '$superuser', '$bctitle', '$token', 0)";
        $dbresult=dbinsertquery($sql);
        $userid=$dbresult['insertid'];
        $error=$dbresult['error'];
        
        //set up the default chat room for this user
        $sql="SELECT * FROM chat_rooms WHERE default_room=1";
        $dbDefault=dbselectsingle($sql);
        if($dbDefault['numrows']>0)
        {
            $roomid=$dbDefault['data']['id'];
            $sql="INSERT INTO user_chatrooms (user_id, room_id) VALUES ('$userid', '$roomid')";
            $dbInsertRoom=dbinsertquery($sql);
            $error.=$dbInsertRoom['error'];
        }
        
   } else {
       $sql="UPDATE users SET notes='$notes', carrier='$carrier', username='$username', mango='$mango', businesscard_title='$bctitle', firstname='$firstname', middlename='$middlename', lastname='$lastname', 
       business='$business', home='$home', cell='$cell', position_id='$position', department_id='$department', fax='$fax', email='$email', 
       summary='$summary', allpubs='$allpubs', extension='$extension', admin='$admin', carrier='$carrier', 
       permission_group='$pgroup', sales_id='$vdsalesid', debug_user='$debug', temp_employee='$tempemployee', super_user='$superuser', simple_menu='$simplemenus', sales_name='$vdsalesname' WHERE id=$userid";   
       $dbresult=dbexecutequery($sql);
       $error=$dbresult['error'];
   }
   
   if($_POST['password']!='')
   {
       $sql="UPDATE users SET password='$password', password_old=0 WHERE id=$userid";
       $dbUpdate=dbexecutequery($sql);
   }
   if($_POST['netpassword']!='')
   {
       $sql="UPDATE users SET network_password='$netpassword' WHERE id=$userid";
       $dbUpdate=dbexecutequery($sql);
   }
   if($_POST['emailpassword']!='')
   {
       $sql="UPDATE users SET email_password='$emailpassword' WHERE id=$userid";
       $dbUpdate=dbexecutequery($sql);
   }
   if ($allpubs)
   {
       //need to add a record for each publication
       $sql="DELETE FROM user_publications WHERE user_id=$userid";
       $dbDelete=dbexecutequery($sql);
       $sql="SELECT * FROM publications WHERE site_id=$siteID";
       $dbPublications=dbselectmulti($sql);
       $value="";
       foreach ($dbPublications['data'] as $publication)
       {
           $value.="('$publication[id]','$userid','1'),";
       }
       $value=substr($value,0,strlen($value)-1);
       $sql="INSERT INTO user_publications (pub_id, user_id, value) VALUES $value";
       $dbinsert=dbinsertquery($sql);
   }
   
   if($pgroup!=0)
   {
       $perms=array();
       //we have a default permission group for this user
       //first, lets see if they have any existing permissions
       $sql="SELECT * FROM user_permissions WHERE user_id=$userid AND value=1";
       $dbExisting=dbselectmulti($sql);
       if($dbExisting['numrows']>0)
       {
           foreach($dbExisting['data'] as $existing)
           {
               $perms[]=$existing['permissionID'];
           }
       }
       //now clear all existing permissions for this user
       $sql="DELETE FROM user_permissions WHERE user_id=$userid";
       $dbDelete=dbexecutequery($sql);
       
       //now grab the permissions set up for the selected group
       $sql="SELECT permission_id FROM core_permission_group_xref WHERE group_id=$pgroup AND value=1";
       $dbGperms=dbselectmulti($sql);
       if($dbGperms['numrows']>0)
       {
           foreach($dbGperms['data'] as $gperm)
           {
               //don't add it if we already have it in our list
               if(!in_array($gperm['permission_id'],$perms))
               {
                   $perms[]=$gperm['permission_id'];
               }   
           }
       }
       //finally if the count of permissions is greater than 0, insert those permissions for this user
       if (count($perms)>0)
       {
           $values="";
           foreach($perms as $key=>$permid)
           {
              $values.="('$permid','$userid',1),";    
           }
           $values=substr($values,0,strlen($values)-1);
           if($values!='')
           {
               $sql="INSERT INTO user_permissions (permissionID,user_id,value) VALUES $values";
               $dbInsert=dbinsertquery($sql);
               //print "perm insert sql: $sql<br>".$dbInsert['error'];
           }
       }
      
   }
   if(isset($_FILES))
     { //means we have browsed for a valid file
        foreach($_FILES as $file) {
            switch($file['error']) {
                case 0: // file found
                if($file['name'] != NULL && okFileType($file['type'],'image',$file['name']) != false)  {
                    //get the new name of the file
                    //to do that, we need to push it into the database, and return the last record ID
                   // process the file
                    $path="artwork/userPics/$siteID/";
                    if (!file_exists($path))
                    {
                        mkdir($path);
                    }
                    $newname=$file['name'];
                    $newname=str_replace(" ","",$newname);
                    $newname=str_replace("/","",$newname);
                    $newname=str_replace("\\","",$newname);
                    $newname=str_replace("*","",$newname);
                    $newname=str_replace("?","",$newname);
                    $newname=str_replace("!","",$newname);
                    $newname=str_replace("'","",$newname);
                    $newname=str_replace(";","",$newname);
                    $newname=str_replace(":","",$newname);
                    $newname=str_replace("'","",$newname);
                    $newname=str_replace("%","",$newname);
                    $newname=str_replace("\$","",$newname);
                    $newname="ticket_".$ticketid."_".$newname;
                    if(processFile($file,$path,$newname) == true) {
                        $sql="UPDATE users SET mugshot='$newname' WHERE id=$userid";
                        $result=dbinsertquery($sql);
                        $error.=$result['error'];
                    } else {
                       $error.= 'There was an error inserting the image named '.$file['name'].' into the database. The sql statement was $sql';  
                    }
                }
                break;

                case (1|2):  // upload too large
                $error.= 'file upload is too large for '.$file['name'];
                break;

                case 4:  // no file uploaded
                break;

                case (6|7):  // no temp folder or failed write - server config errors
                $error.= 'internal error - flog the webmaster on '.$file['name'];
                break;
            }
        }
     }
   
    if ($error!='')
    {
        setUserMessage('There was a problem saving the user.<br>'.$error,'error');
        
    } else {
        setUserMessage('User successfully saved','success');
    }
    redirect("?action=list");    
}

function groups()
{
    //get all departments
    global $siteID;
    $userid=$_GET['userid'];
    $sql="SELECT * FROM user_groups WHERE site_id=$siteID ORDER BY group_name";
    $dbGroups=dbselectmulti($sql);
    if ($dbGroups['numrows']>0)
    {
        print "<h2>Select the groups that this employee is a member of.</h2>\n";
        print "<form method=post class='form-horizontal'>\n";
        foreach($dbGroups['data'] as $group)
        {
            //see if the employee has this one
            $sql="SELECT * FROM user_groups_xref WHERE group_id=$group[id] AND user_id=$userid";
            $dbExisting=dbselectsingle($sql);
            if ($dbExisting['numrows']>0){$checked=1;}else{$checked=0;}
            print make_checkbox('group_'.$group['id'],$checked)." ".$group['group_name']."<br />\n";    
        }
        make_hidden('user_id',$userid);
        make_submit('submit','Set Groups');
        print "</form>\n";
    } else {
        print "<a href='?action=list'>Sorry, there are no groups configured yet. Click to return to employee list.</a><br />\n";
    } 
}


function save_groups()
{
    $userid=$_POST['user_id'];
    $values="";
    //clear existing
    $sql="DELETE FROM user_groups_xref WHERE user_id=$userid";
    $dbDelete=dbexecutequery($sql);
    foreach($_POST as $key=>$value)
    {
        if (substr($key,0,6)=='group_')
        {
            $id=str_replace("group_","",$key);
            $values.="($userid,$id), ";    
        }
    }
    $values=substr($values,0,strlen($values)-2);
    if($values!='')
    {
        $sql="INSERT INTO user_groups_xref (user_id, group_id) VALUES $values";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $error='';
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the group for the user.<br>'.$error,'error');
    } else {
        setUserMessage('User group successfully saved','success');
    }
    redirect("?action=list");
}
  

function exportWiki()
{
    global $departments, $employeepositions;
    
    header('Content-Type: text/txt'); // as original file
    header('Content-Disposition: attachment; filename="users-wiki-'.date("Ymd").$sub.'.txt"');
        
    print "======Organizational Contacts======\n";
    
    //first get all super-users
    print "=====IT Liasons=====\n";
    print "^Person^Position^Department^DID Phone^Extension^Cell^Email^Workstation ID^\n";
    $sql="SELECT * FROM users WHERE super_user=1 ORDER BY lastname, firstname";
    $dbSuper=dbselectmulti($sql);
    if($dbSuper['numrows']>0)
    {
        foreach($dbSuper['data'] as $u)
        {
            //look for any assigned equipment
            $sql="SELECT id, device_name, device_type FROM it_devices WHERE assigned_to=$u[id]";
            $dbDevices=dbselectmulti($sql);
            $devices='';
            if($dbDevices['numrows']>0)
            {
                foreach($dbDevices['data'] as $device)
                {
                    $devices.="[[equipment#$device[device_type]|$device[device_name]]]\\\\ ";    
                }
            }
            print "|".stripslashes($u['lastname'].', '.$u['firstname'])." |";   
            print $employeepositions[$u['position_id']]." |";   
            print $departments[$u['department_id']]." |";   
            print $u['business']." |";   
            print $u['extension']." |";   
            print $u['cell']." |";   
            print "[[mailto:".stripslashes($u['email'])."|".stripslashes($u['email'])."]] |"; 
            print $devices." | \n";  
        }    
    }
    
    //now get all department heads
    print "=====Department Heads=====\n";
    print "^Person^Position^Department^DID Phone^Extension^Cell^Email^Workstation ID^\n";
    $sql="SELECT A.* FROM users A, user_positions B WHERE B.director=1 AND A.position_id=B.id ORDER BY A.lastname, A.firstname";
    $dbSuper=dbselectmulti($sql);
    if($dbSuper['numrows']>0)
    {
        foreach($dbSuper['data'] as $u)
        {
            //look for any assigned equipment
            $sql="SELECT id, device_name, device_type FROM it_devices WHERE assigned_to=$u[id]";
            $dbDevices=dbselectmulti($sql);
            $devices='';
            if($dbDevices['numrows']>0)
            {
                foreach($dbDevices['data'] as $device)
                {
                    $devices.="[[equipment#$device[device_type]|$device[device_name]]]\\\\ ";    
                }
            }
            print "|".stripslashes($u['lastname'].', '.$u['firstname'])." |";   
            print $employeepositions[$u['position_id']]." |";   
            print $departments[$u['department_id']]." |";   
            print $u['business']." |";   
            print $u['extension']." |";   
            print $u['cell']." |";   
            print "[[mailto:".stripslashes($u['email'])."|".stripslashes($u['email'])."]] |"; 
            print $devices." | \n";  
        }    
    }
    
    print "===== Employees =====\n";
    print "** :!: Keep sorted by department then alphabetical**\n";

    //now go through each department and show users
    foreach($departments as $deptid=>$deptname)
    {
        print "====$deptname====\n";
        //now get all department heads
        print "^Person^Position^DID Phone^Extension^Cell^Email^Workstation ID^\n";
        $sql="SELECT * FROM users WHERE department_id=$deptid ORDER BY lastname, firstname";
        $dbSuper=dbselectmulti($sql);
        if($dbSuper['numrows']>0)
        {
            foreach($dbSuper['data'] as $u)
            {
                //look for any assigned equipment
                $sql="SELECT id, device_name, device_type FROM it_devices WHERE assigned_to=$u[id]";
                $dbDevices=dbselectmulti($sql);
                $devices='';
                if($dbDevices['numrows']>0)
                {
                    foreach($dbDevices['data'] as $device)
                    {
                        $devices.="[[equipment#$device[device_type]|$device[device_name]]]\\\\ ";    
                    }
                }
                print "|".stripslashes($u['lastname'].', '.$u['firstname'])." |";   
                print $employeepositions[$u['position_id']]." |";   
                print $u['business']." |";   
                print $u['extension']." |";   
                print $u['cell']." |";   
                print "[[mailto:".stripslashes($u['email'])."|".stripslashes($u['email'])."]] |"; 
                print $devices." | \n";  
            }    
        }
    }
}
$Page->footer();
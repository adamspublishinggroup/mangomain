<?php
//<!--VERSION: 1.0 **||**-->

include("includes/boot.php") ;

if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}
    switch ($action)
    {
        case "Save Size":
        save_size('insert');
        break;
        
        case "Update Size":
        save_size('update');
        break;
        
        case "add":
        setup_sizes('add');
        break;
        
        case "edit":
        setup_sizes('edit');
        break;
        
        case "delete":
        setup_sizes('delete');
        break;
        
        case "list":
        setup_sizes('list');
        break;
        
        default:
        setup_sizes('list');
        break;
        
    } 
    
    
function setup_sizes($action)
{
    if ($action=='add' || $action=='edit')
    {
        if ($action=='add')
        {
            $button="Save Size";
            $sizeIN='0.00';
            $sizeMM=0;
        } else {
            $button="Update Size";
            $sizeid=intval($_GET['sizeid']);
            $sql="SELECT * FROM paper_sizes WHERE id=$sizeid";
            $dbSize=dbselectsingle($sql);
            $size=$dbSize['data'];
            $sizeIN=stripslashes($size['width']);
            $sizeMM=stripslashes($size['width_mm']);
            $display=stripslashes($size['display']);
        }
        print "<form method=post class='form-horizontal'>\n";
        make_descriptor('Leaving one or the other blank or set to zero will cause it to be calculated from the one you enter a value for.','Info');
        make_text('size_in',$sizeIN,'Size (in)','Enter roll width in inches');
        make_text('size_mm',$sizeMM,'Size (mm)','Enter roll width in mm');
        
        make_checkbox('display',$display,'Display','Allow this option to be chosen');
        make_hidden('sizeid',$sizeid);
        make_submit('submit',$button);
        print "</form>\n";  
    } elseif($action=='delete') {
        $sizeid=intval($_GET['sizeid']);
        $sql="UPDATE paper_sizes SET status=99 WHERE id=$sizeid";
        $dbUpdate=dbexecutequery($sql);
        redirect("?action=list");
    } else {
        $sql="SELECT * FROM paper_sizes WHERE status=1 ORDER BY width ASC";
        $dbSizes=dbselectmulti($sql);
        tableStart("<a href='?action=add'>Add new paper size</a>","Size (in),Size (mm)",3);
        if ($dbSizes['numrows']>0)
        {
            foreach($dbSizes['data'] as $paper)
            {
                $sizeIN=$paper['width']."\"";
                $sizeMM=$paper['width_mm']."mm";
                $sizeid=$paper['id'];
                print "<tr>";
                print "<td>$sizeIN</td>";
                print "<td>$sizeMM</td>";
              print "<td>
            <div class='btn-group'>
              <a href='?action=edit&sizeid=$sizeid' class='btn btn-dark'>Edit</a>
              <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <span class='caret'></span>
                <span class='sr-only'>Toggle Dropdown</span>
              </button>
              <ul class='dropdown-menu'>
                <li><a href='?action=delete&sizeid=$sizeid' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
              </ul>
            </div>
            </td>";
            print "</tr>";
            }
        }
        tableEnd($dbSizes);
        
    }
}

function save_size($action)
{
    $sizeid=intval($_POST['sizeid']);
    $sizeIN=addslashes($_POST['size_in']);
    $sizeMM=addslashes($_POST['size_mm']);
    if(($sizeIN=='' || $sizeIN=='0.00') && $sizeMM!='')
    {
        //convert mm to inches
        $sizeIN = round($sizeMM/25.4,2);
    } elseif(($sizeMM=='' || $sizeMM=='0.00') && $sizeIN!='')
    {
        //convert inches to mm
        
        $sizeMM = round($sizeIN*25.4,0);
    }
    
    if ($_POST['display']){$display=1;}else{$display=0;}
    if ($action=='insert')
    {
        $sql="INSERT INTO paper_sizes (width, status, display, width_mm, site_id) VALUES ('$sizeIN', 1, '$display','$sizeMM', ".SITE_ID.")";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $sql="UPDATE paper_sizes SET width='$sizeIN', display='$display', width_mm='$sizeMM' WHERE id=$sizeid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the size.'.$error,'error');
    } else {
        setUserMessage('Paper size successfully saved.','success');
    }
    redirect("?action=list");
}

$Page->footer();
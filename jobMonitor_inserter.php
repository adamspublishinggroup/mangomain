<?php
/*
*  inserter package data script
*  this script is meant to be the "quick data"
*/
include("includes/boot.php");

$package = package_nav();

print "<div class='row'>";
inserterData($package);
print "</div>";


function inserterData($package)
{
    $packageid=$package['id'];
    //get plan details
    $sql="SELECT A.*, B.pub_name FROM jobs_inserter_packages A, publications B WHERE A.id=$packageid AND A.pub_id=B.id";
    $dbPackage=dbselectsingle($sql);
    $package=$dbPackage['data'];
    $pubid=$package['pub_id'];
    $pubdate=$package['pub_date'];
    $displaydate=date("D, m/d/Y",strtotime($package['pub_date']));
    $pubname=$package['pub_name'];
    $packagename=$package['package_name'];
    $planid=$package['plan_id'];
    $inserterid=$package['inserter_id'];
   $sql="SELECT A.* FROM inserts A, jobs_packages_inserts B WHERE B.insert_type='insert' AND B.package_id=$packageid AND B.insert_id=A.id";
   $dbPackageInserts=dbselectmulti($sql);
   if($dbPackageInserts['numrows']>0)
   {  
        //collect all the other job data
        //check for sticky note
       foreach($dbPackageInserts['data'] as $insert)
       {
            if ($insert['sticky_note']){
                $sticky=1;
            }    
       }
   }
   
   //get plan
   $sql="SELECT * FROM jobs_inserter_plans WHERE id=$planid";
   $dbPlan=dbselectsingle($sql);
   $plan=$dbPlan['data'];
   //check for addressing
     
   $address=$plan['address'];
   
   $operators=array();
   $operators[0]="Please choose";
   
   global $mailroomDepartmentID;
   
   $sql="SELECT A.* FROM users A, user_positions B WHERE A.department_id=$mailroomDepartmentID AND A.position_id=B.id AND B.operator=1 ORDER BY firstname";
   $dbOperators=dbselectmulti($sql);
   if ($dbOperators['numrows']>0)
   {
       foreach($dbOperators['data'] as $operator)
       {
           $operators[$operator['id']]=$operator['firstname']." ".$operator['lastname'];
       }
   }
   $feeders=array();
   $feeders[0]="Please choose";
   $sql="SELECT * FROM users WHERE department_id=3 ORDER BY firstname";
   $dbFeeders=dbselectmulti($sql);
   if ($dbFeeders['numrows']>0)
   {
       foreach($dbFeeders['data'] as $feeder)
       {
           $feeders[$feeder['id']]=$feeder['firstname']." ".$feeder['lastname'];
       }
   }
   $feeders[999999]='Temp Employee';
   //lets check to see if we have any existing stats that we are editing
   $sql="SELECT * FROM jobs_inserter_rundata WHERE package_id=$packageid";
   $dbData=dbselectsingle($sql);
   if ($dbData['numrows']>0)
   {
       $data=$dbData['data'];
       
       $signoff=$data['signoff'];
       $setupby=explode("|",$data['setupby']);
       
       //actively used data:
       $runStart=$data['package_startdatetime'];
       $runStop=$data['package_stopdatetime'];
       $setupStart=$data['setup_starttime'];
       $setupStop=$data['setup_stoptime'];
       
       $pallets=$data['pallets'];
       $bundleSizeHD=$data['bundle_size_hd'];
       
       $feederCount=$data['feeders'];
       $dockCount=$data['dock_temp'];
       $stackerCount=$data['stacker_temp'];
       $mailCount=$data['maildesk'];
       
       
       $totalBundles=$data['total_bundles'];
       $bundleSizeSC=$data['bundle_size_sc'];
       $runlength=$data['run_length'];
       $feedhours=$data['feed_hours'];
       $presstemp=$data['press_temp'];
       
       $feederpiecesperhour=$data['feeder_pieces_per_hour'];
       $calcfeederpiecesperhour=$data['calc_feeder_pieces_per_hour'];
       $totalpiecesperhour=$data['total_pieces_per_hour'];
       $calctotalpiecesperhour=$data['calc_total_pieces_per_hour'];
       
       
       $leftover=$data['leftover_jackets'];
       $systemTotalPieces=$data['system_total_pieces'];
       $calcTotalPieces=$data['calc_total_pieces'];
       $dataid=$data['id'];
       $request=$package['inserter_request'];
       $address=$data['address'];
       $addresscount=$data['address_count'];
       $sticky=$data['sticky_note'];
       $stickycount=$data['sticky_note_count'];
       $doubleout=$data['doubleout'];
       $packagesbuilt=$data['packages_built']; 
       $pressrequest=$data['press_request'];
       $notes=$data['notes'];
       $hoppersused=$data['hoppers_used'];
       $calcHoppersUsed=$data['calc_hoppers_used'];
       $calcTotalStaff=$data['calc_total_staff'];
       $dockstaff=explode("|",$data['dock_staff']);
       $stackerstaff=explode("|",$data['stacker_staff']);
       $pressstaff=explode("|",$data['press_staff']);
       $ftePre=$data['fte_pre'];
       $ftePost=$data['fte_post'];
        
   } else {
       $setupby=array();                                        
       $signoff=0;
       $pressrequest=$package['inserter_request'];
       
       $setupStart=date("Y-m-d H:i",strtotime($package['package_startdatetime']." - 30 minutes"));
       $setupStop=date("Y-m-d H:i",strtotime($package['package_startdatetime']." - 10 minutes"));
       $runStart=date("Y-m-d H:i",strtotime($package['package_startdatetime']." + 10 minutes"));
       $runStop=date("Y-m-d H:i",strtotime($package['package_stopdatetime']." - 10 minutes"));
       
       $feederCount=0;
       $mailCount=0;
       $dockCount=0;
       $stackerCount=0;
       
       
       $pressrunid=$package['pressrun_id'];
       $dataid=0;
       $notes="";
       $hopperfeeders=array();
       $dockstaff=array();
       $stackerstaff=array();
       $pressstaff=array();
       $packagesbuilt=0;
       $pallets=0;
       $employeecount=0;
       $systemTotalPieces=0;
       $hoppersused=0;
       $leftover=0;
       $stickycount=0;
       $addresscount=0;
       $runlength=0;
       $feedhours=0;
       $feederpiecesperhour=0;
       $calcfeederpiecesperhour=0;
       $totalpiecesperhour=0;
       $calctotalpiecesperhour=0;
       $calcTotalStaff=0;
       
       $ftePre=0;
       $ftePost=0;
       $bundleSizeHD = 0;
       $bundleSizeSC = 0;
       $totalBundles = 0;
   }
   
   if($pressrunid!=0)
   {
       $sql="SELECT run_name FROM publications_runs WHERE id=$pressrunid";
       $dbPressRun = dbselectsingle($sql);
       $pressrun= " - ".stripslashes($dbPressRun['data']['run_name']);
       
   } else {
       $pressrun = "";
   }
   
   ?>
   <div class='col-md-9 col-sm-12'>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Package: <?php echo $pubname.$pressrun ?> - <?php echo $packagename ?> - Publishing <?php echo date("l m/d/Y",strtotime($pubdate)) ?></h3>
            </div>
            <div class="panel-body">
                <table class='table table-bordered'>
                    <tr>
                    <td style='background-color:#ccc;'>Setup Start</td>
                    <td>
                        <div class='input-group date col-xs-6' style='min-width:300px;' id='setup_start_tp'>
                            <input type='text' class='form-control' name='setup_start' id='setup_start' value='<?php echo $setupStart ?>'/>
                            <span class='input-group-addon'>
                                <i class='fa fa-calendar'></i>
                            </span>
                            <span class='input-group-btn'>
                                <button onclick="setCurrentTime('setup_start');" class='btn btn-dark'>Use current</button>
                            </span>
                        </div>
                        <script>
                            $('#setup_start_tp').datetimepicker({format: 'YYYY-MM-DD HH:mm',
                            showTodayButton: true,
                            sideBySide: true,
                            icons: {
                                time: 'fa fa-clock-o',
                             },
                             defaultDate: '<?php echo $setupStart; ?>'
                            });
                        </script>
                    </td>
                    <td style='background-color:#ccc;'>Setup Stop</td>
                    <td>
                        <div class='input-group date col-xs-6' style='min-width:300px;' id='setup_stop_tp'>
                            <input type='text' class='form-control' name='setup_stop' id='setup_stop' value='<?php echo $setupStop ?>'/>
                            <span class='input-group-addon'>
                                <i class='fa fa-calendar'></i>
                            </span>
                            <span class='input-group-btn'>
                                <button onclick="setCurrentTime('setup_stop');" class='btn btn-dark'>Use current</button>
                            </span>
                        </div>
                        <script>
                            $('#setup_stop_tp').datetimepicker({format: 'YYYY-MM-DD HH:mm',
                            showTodayButton: true,
                            sideBySide: true,
                            icons: {
                                time: 'fa fa-clock-o',
                             },
                             defaultDate: '<?php echo $setupStop; ?>'
                            });
                        </script>
                    </td>
                    </tr>
                    <tr>
                        <td style='background-color:#ccc;'>Scheduled Start</td>
                        <td><?php echo date("D, m/d/Y \@ H:i",strtotime($package['package_startdatetime'])); ?></td>
                        <td style='background-color:#ccc;'>Actual Start</td>
                        <td>
                         <div class='input-group date col-xs-6' style='min-width:300px;' id='actual_starttime_tp'>
                                <input type='text' class='form-control' name='actual_starttime' id='actual_starttime' value='<?php echo $runStart ?>'/>
                                <span class='input-group-addon'>
                                    <i class='fa fa-calendar'></i>
                                </span>
                                <span class='input-group-btn'>
                                    <button onclick="setCurrentTime('package_startdatetime');" class='btn btn-dark'>Use current</button>
                                </span>
                         </div>
                            <script>
                                $('#actual_starttime_tp').datetimepicker({format: 'YYYY-MM-DD HH:mm',
                                useCurrent: true,
                                showTodayButton: true,
                                sideBySide: true,
                                icons: {
                                    time: 'fa fa-clock-o',
                                 },
                                 defaultDate: '<?php echo $runStart; ?>'
                                });
                            </script>
                        </td>
                    </tr>
                    <tr>
                        <td style='background-color:#ccc;'>Scheduled Stop</td>
                        <td><?php echo date("D, m/d/Y \@ H:i",strtotime($package['package_stopdatetime'])); ?></td>
                        <td style='background-color:#ccc;'>Actual Stop</td>
                        <td>
                            <div class='input-group date col-xs-6' style='min-width:300px;' id='actual_stoptime_tp'>
                                <input type='text' class='form-control' name='actual_stoptime' id='actual_stoptime' value='<?php echo $runStop ?>'/>
                                <span class='input-group-addon'>
                                    <i class='fa fa-calendar'></i>
                                </span>
                                <span class='input-group-btn'>
                                    <button onclick="setCurrentTime('package_stopdatetime');" class='btn btn-dark'>Use current</button>
                                </span>
                            </div>
                            <script>
                                $('#actual_stoptime_tp').datetimepicker({format: 'YYYY-MM-DD HH:mm',
                                showTodayButton: true,
                                sideBySide: true,
                                icons: {
                                    time: 'fa fa-clock-o',
                                 },
                                 defaultDate: '<?php echo $runStop ?>'
                                });
                            </script>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class='row'>
            <div class='col-xs-12 col-sm-6'>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Run Details</h3>
                    </div>
                    <div class="panel-body">
                        <table class='table table-bordered'>
                        <tr>
                            <td style='background-color:#ccc;'>Request Count</td>
                            <td><?php echo $pressrequest; ?></td>
                        </tr>
                        <tr>
                            <td style='background-color:#ccc;'>Produced Count</td>
                            <td>
                            <div class="input-group form-horizontal">
                                <input type='number' value='<?php echo $packagesbuilt; ?>' class='form-control' id='packages_built' disabled/>
                                <span class="input-group-btn">
                                    <button class="btn btn-dark" type="button" onclick='setPackageCount();' id='packageCountBTN'>Update</button>
                                </span>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td style='background-color:#ccc;'>Bundle Size</td>
                            <td>
                            <div class="input-group form-horizontal">
                                <input type='number' value='<?php echo $bundleSizeHD; ?>' class='form-control' id='bundle_size_hd' disabled/>
                                <span class="input-group-btn">
                                    <button class="btn btn-dark" type="button" onclick='setBundleSize();' id='bundleSizeBTN'>Update</button>
                                </span>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td style='background-color:#ccc;'>Pallet Count</td>
                            <td>
                            <div class="input-group form-horizontal">
                                <input type='number' value='<?php echo $pallets; ?>' class='form-control' id='pallet_count' disabled/>
                                <span class="input-group-btn">
                                    <button class="btn btn-dark" type="button" onclick='setPalletCount();' id='palletCountBTN'>Update</button>
                                </span>
                            </div>
                            </td>
                        </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class='col-xs-12 col-sm-6'>
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Staffing</h3>
                    </div>
                    <div class="panel-body">
                        <table class='table table-bordered'>
                        <tr>
                        <td style='background-color:#ccc;'>Signoff By</td>
                        <td>
                                <?php if(count($operators)>0)
                                print "<select class='form-control' id='signoff' onChange=\"packageMonitorUpdate('signoff',\$(this).val());\">\n";
                                {
                                    foreach($operators as $id=>$operator)
                                    {
                                        print "<option value='$id' ".($id==$signoff? 'selected':'').">$operator</option>\n";  
                                    }   
                                }
                                print "
                            </select>";
                                ?>
                        </td>
                        </tr>
                        <tr>
                            <td style='background-color:#ccc;'>Feeder Count</td>
                            <td>
                            <div class="input-group form-horizontal">
                                <input type='number' value='<?php echo $feederCount; ?>' class='form-control' id='feeder_count' disabled/>
                                <span class="input-group-btn">
                                    <button class="btn btn-dark" type="button" onclick='setFeederCount();' id='feederCountBTN'>Update</button>
                                </span>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td style='background-color:#ccc;'>Stacker Count</td>
                            <td>
                            <div class="input-group form-horizontal">
                                <input type='number' value='<?php echo $stackerCount; ?>' class='form-control' id='stacker_count' disabled/>
                                <span class="input-group-btn">
                                    <button class="btn btn-dark" type="button" onclick='setStackerCount();' id='stackerCountBTN'>Update</button>
                                </span>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td style='background-color:#ccc;'>Dock Count</td>
                            <td>
                            <div class="input-group form-horizontal">
                                <input type='number' value='<?php echo $dockCount; ?>' class='form-control' id='dock_count' disabled/>
                                <span class="input-group-btn">
                                    <button class="btn btn-dark" type="button" onclick='setDockCount();' id='dockCountBTN'>Update</button>
                                </span>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td style='background-color:#ccc;'>Maildesk Count</td>
                            <td>
                            <div class="input-group form-horizontal">
                                <input type='number' value='<?php echo $mailCount; ?>' class='form-control' id='mail_count' disabled/>
                                <span class="input-group-btn">
                                    <button class="btn btn-dark" type="button" onclick='setMailCount();' id='mailCountBTN'>Update</button>
                                </span>
                            </div>
                            </td>
                        </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
   </div>
   
   <div class='col-md-3 col-sm-12'>
       <div class="panel panel-primary">
       <div class="panel-heading">
        <h3 class="panel-title">List of Inserts</h3>
       </div>
       <div class="panel-body">
            <table class='table table-bordered table-striped'>
            <thead>
            <tr>
                <th>Insert Name</th>
                <th>Tab Pages</th>
                <th>Type</th>
            </tr>
            </thead>
            <tbody>
            <?php
               
            //get all the inserts scheduled for this package
            $sql="SELECT B.account_name, D.insert_quantity, C.insert_type, A.stanard_pages FROM inserts A, accounts B, jobs_packages_inserts C, inserts_schedule D
                WHERE A.id=C.insert_id AND C.package_id=$packageid AND A.advertiser_id=B.id AND C.insert_id=D.insert_id ORDER BY B.account_name";
            $dbInserts=dbselectmulti($sql);
            $inserts=array();
            if($dbInserts['numrows']>0)
            {
                $insertCount=0;
                $totalPages=0;
                foreach($dbInserts['data'] as $insert)
                {
                    $insertCount++;
                    $totalPages+=$insert['standard_pages'];
                    print "
            <tr>
                <td>$insert[account_name]</td>
                <td>$insert[standard_pages]</td>
                <td>$insert[insert_type]</td>
            </tr>";
                }
            }    
            ?>
            <tr>
            <td colspan=2 style='background-color:#ccc;'>Total Tab Pages</td><td><?php echo $totalPages ?></td>
            </tr>
            <tr>
            <td colspan=2 style='background-color:#ccc;'>Total Inserts</td><td><?php echo $insertCount ?></td>
            </tr>
            </tbody>
            
            </table>
            <a href='http://10.56.1.10/inserterPlanner.php?action=packages&planid=<?php echo $planid ?>&pubid=<?php echo $pubid ?>' target='_blank' class='btn btn-primary'><i class='fa fa-external-link'></i> Edit Package</a>
       </div>
       </div>
   </div>
   
   
    <script>
       
       $(document).ready(function(){
            loadPackageOfInserts(); 
            $('#actual_starttime_tp').on("dp.change",function(e){
                packageMonitorUpdate('actual_starttime',e.date.format("YYYY-MM-DD HH:mm"));
            });
            $('#actual_stoptime_tp').on("dp.change",function(e){
                packageMonitorUpdate('actual_stoptime',e.date.format("YYYY-MM-DD HH:mm"));
            }); 
            $('#setup_start_tp').on("dp.change",function(e){
                packageMonitorUpdate('setup_start',e.date.format("YYYY-MM-DD HH:mm"));
            }); 
            $('#setup_stop_tp').on("dp.change",function(e){
                packageMonitorUpdate('setup_stop',e.date.format("YYYY-MM-DD HH:mm"));
            });    
       })
       
       function setPackageCount()
       {
           var btnName = $('#packageCountBTN').html();
           if(btnName == 'Update')
           {
               $('#packages_built').prop("disabled",false);
               $('#packageCountBTN').html('Save');
           } else {
               $('#packages_built').prop("disabled",true);
               $('#packageCountBTN').html('Update');
               packageMonitorUpdate('packages_built',$('#packages_built').val());
           }    
       }
       
       
       function setBundleSize()
       {
           var btnName = $('#bundleSizeBTN').html();
           if(btnName == 'Update')
           {
               $('#bundle_size_hd').prop("disabled",false);
               $('#bundleSizeBTN').html('Save');
           } else {
               $('#bundle_size_hd').prop("disabled",true);
               $('#bundleSizeBTN').html('Update');
               packageMonitorUpdate('bundle_size_hd',$('#bundle_size_hd').val());
           }    
       }
       
       
       function setPalletCount()
       {
           var btnName = $('#palletCountBTN').html();
           if(btnName == 'Update')
           {
               $('#pallet_count').prop("disabled",false);
               $('#palletCountBTN').html('Save');
           } else {
               $('#pallet_count').prop("disabled",true);
               $('#palletCountBTN').html('Update');
               packageMonitorUpdate('pallets',$('#pallet_count').val());
           }    
       }
       
       function setStackerCount()
       {
           var btnName = $('#stackerCountBTN').html();
           if(btnName == 'Update')
           {
               $('#stacker_count').prop("disabled",false);
               $('#stackerCountBTN').html('Save');
           } else {
               $('#stacker_count').prop("disabled",true);
               $('#stackerCountBTN').html('Update');
               packageMonitorUpdate('stacker_temp',$('#stacker_count').val());
           }    
       }
       
       function setFeederCount()
       {
           var btnName = $('#feederCountBTN').html();
           if(btnName == 'Update')
           {
               $('#feeder_count').prop("disabled",false);
               $('#feederCountBTN').html('Save');
           } else {
               $('#feeder_count').prop("disabled",true);
               $('#feederCountBTN').html('Update');
               packageMonitorUpdate('feeders',$('#feeder_count').val());
           }    
       }
       
       function setDockCount()
       {
           var btnName = $('#dockCountBTN').html();
           if(btnName == 'Update')
           {
               $('#dock_count').prop("disabled",false);
               $('#dockCountBTN').html('Save');
           } else {
               $('#dock_count').prop("disabled",true);
               $('#dockCountBTN').html('Update');
               packageMonitorUpdate('dock_temp',$('#dock_count').val());
           }    
       }
        
       function setMailCount()
       {
           var btnName = $('#mailCountBTN').html();
           if(btnName == 'Update')
           {
               $('#mail_count').prop("disabled",false);
               $('#mailCountBTN').html('Save');
           } else {
               $('#mail_count').prop("disabled",true);
               $('#mailCountBTN').html('Update');
               packageMonitorUpdate('maildesk',$('#mail_count').val());
           }    
       }
       
       function loadPackageOfInserts()
       {
           $.ajax({
            url: 'includes/ajax_handlers/generateInsertPackageDisplay.php',
            data: {packageid:<?php echo $packageid; ?>,people:1,count:1,editing:true,maxwidth:600},
            dataType: 'html',
            success: function(response){
               $('#packageholder').html(response); 
            }
           })    
          
       }
       
       function setCurrentTime(field)
       {
           var d = moment().format('YYYY-MM-DD HH:mm'); 
           $('#'+field).val(d);
           packageMonitorUpdate('runstart_one',d);
       }
       
       function packageMonitorUpdate(field,value)
       {
           var script = 'packageMonitor.php';
           var dataObj = {
               packageid:<?php echo $packageid; ?>,
               field: field,
               value: value
           }
           var response = genericAjaxHandler(script,dataObj);
          
       }
    </script>
   
   <?php
   
   
   
   /*
       
       make_select('signoff',$operators[$signoff],$operators,'Insert Signoff');
       make_select('operator',$operators[$signoff],$operators,'Lead Operator');
       make_datetime('setupStart',$setupStart,'Setup Start'); 
       make_datetime('setupStop',$setupStop,'Setup Finish'); 
       
       make_number('pressrequest',$pressrequest,'How many papers requested','How many papers were requested?');
       make_number('packagesbuilt',$packagesbuilt,'How many papers produced','How many complete papers produced?');
       make_number('employeecount',$employeecount,'How many employees','How many mailroom folks on shift?');
       make_number('pallets',$pallets,'Shrink wrap','How many pallets were shrink wrapped?');
       make_number('bundle_size_hd',$bundleSizeHD,'Bundle Size HD','How many papers in Home Delivery Bundles?');
       make_number('bundle_size_sc',$bundleSizeSC,'Bundle Size SC','How many papers in Single Copy Bundles?');
       make_number('total_bundles',$totalBundles,'Total Bundles','How many total bundles were produced?');
       make_number('hoppers',$hoppersused,'How many stations used','Number of stations used to feed jacket and inserts?');
       make_number('leftover',$leftover,'Leftover','How many left over jackets are there?');
       make_datetime('runstart1',$runstart1,'Run Start'); 
       make_datetime('runfinish1',$runfinish1,'Run Finish'); 
       make_datetime('firstbundle1',$firstbundle1,'First Bundle'); 
       make_datetime('lastbundle1',$lastbundle1,'Last Bundle'); 
       make_datetime('firsttruck1',$firsttruck1,'First Truck'); 
       make_datetime('lasttruck1',$lasttruck1,'Last Truck'); 
    */   
       print "<a href='inserterPlanner.php?action=packages&planid=$planid&pubid=$pubid' target='_blank' class='button'>Edit package/inserts</a>\n";
       
       
   //make_textarea('notes',$notes,'Notes','Enter any notes about the job');
   //make_select('maildesk',$feeders[$maildesk],$feeders,'Mail Desk');
   //echo "# of stack down workers:<br />".make_text('tempstacker_staff',$stackertemp);
       
}

function save_inserterData()
{
    $pubid=intval($_POST['pubid']);
    $packageid=intval($_POST['packageid']);
    $planid=intval($_POST['planid']);
    $dataid=intval($_POST['dataid']);
    if ($_POST['doubleout']){$doubleout=1;}else{$doubleout=0;}
    if ($_POST['address']){$address=1;}else{$address=0;}
    if ($_POST['sticky']){$sticky=1;}else{$sticky=0;}
    $stickycount=intval($_POST['stickycount']);
    $addresscount=intval($_POST['addresscount']);
    $hoppers=intval($_POST['hoppers']);
    $calcpieces=intval($_POST['calcpieces']);
    $pressrequest=intval($_POST['pressrequest']);
    $packagesbuilt=intval($_POST['packagesbuilt']);
    $employeecount=intval($_POST['employeecount']);
    $ftePre=intval($_POST['ftePre']);
    $ftePost=intval($_POST['ftePost']);
    $systemtotalpieces=intval($_POST['systemtotalpieces']);
    $pallets=intval($_POST['pallets']);
    $leftover=intval($_POST['leftover']);
    $signoff=intval($_POST['signoff']);
    $maildesk=intval($_POST['maildesk']);
    $presstemp=intval($_POST['temppress_staff']);
    $stackertemp=intval($_POST['tempstacker_staff']);
    $docktemp=intval($_POST['tempdock_staff']);
    $notes=addslashes($_POST['notes']);
    $bundleSizeHD = intval($_POST['bundle_size_hd']);
    $bundleSizeSC = intval($_POST['bundle_size_sc']);
    $totalBundles = intval($_POST['total_bundles']);
    $setupStart = $_POST['setupStart'];
    $setupStop = $_POST['setupStop'];
    
    $pressmanids="";
    //find pressman now
    $setupby="";
    $dockstaff="";
    $hopperfeeders="";
    $stackerstaff="";
    $pressstaff="";
    $calcStaff=0;
    $feedcount=0;
    foreach($_POST as $key=>$value)
    {
        if (substr($key,0,9)=="operator_")
        {
            $key=str_replace("operator_","",$key);
            $setupby.="$key|";
            $calcStaff++;    
        }
        if (substr($key,0,5)=="dock_")
        {
            $key=str_replace("dock_","",$key);
            $dockstaff.="$key|";
            $calcStaff++;    
        }
        if (substr($key,0,8)=="stacker_")
        {
            $key=str_replace("stacker_","",$key);
            $stackerstaff.="$key|";
            $calcStaff++;    
        }
        if (substr($key,0,6)=="press_")
        {
            $key=str_replace("press_","",$key);
            $pressstaff.="$key|";
            $calcStaff++;    
        }
        if (substr($key,0,6)=="other_")
        {
            $key=str_replace("other_","",$key);
            $otherequipment.="$key|";
        }
    }
    $notes=addslashes(str_replace("<input type=\"hidden\" /><!--Session data--><input type=\"hidden\" />","",$_POST['notes']));
    $setupby=substr($setupby,0,strlen($setupby)-1);
    $dockstaff=substr($dockstaff,0,strlen($dockstaff)-1);
    $stackerstaff=substr($stackerstaff,0,strlen($stackerstaff)-1);
    $pressstaff=substr($pressstaff,0,strlen($pressstaff)-1);
    
    //add the temp staff to the total count
    $calcStaff=$calcStaff+$presstemp+$stackertemp+$docktemp;
    if($maildesk!=0){$calcStaff++;}
    
    
    $packagestartdatetime=$_POST['package_startdatetime'];
    $packagestopdatetime=$_POST['package_stopdatetime'];
    $runstart1=$_POST['runstart1'];
    $runfinish1=$_POST['runfinish1'];
    $firstbundle1=$_POST['firstbundle1'];
    $lastbundle1=$_POST['lastbundle1'];
    $firsttruck1=$_POST['firsttruck1'];
    $lasttruck1=$_POST['lasttruck1'];
    if($_POST['displaydouble']=='none')
    {
        if($_POST['runstart2']=='')
        {
            $runstart2=$runstart1;
        } else {
            $runstart2=$_POST['runstart2'];
        }
        if($_POST['runfinish2']=='')
        {
            $runfinish2=$runfinish1;    
        } else {
            $runfinish2=$_POST['runfinish2'];
        }
        if($_POST['firstbundle2']=='')
        {
            $firstbundle2=$firstbundle1;    
        } else {
            $firstbundle2=$_POST['firstbundle2'];
        }
        if($_POST['lastbundle2']=='')
        {
            $lastbundle2=$lastbundle1;    
        } else {
            $lastbundle2=$_POST['lastbundle2'];
        }
        if($_POST['firsttruck2']=='')
        {
            $firsttruck2=$firsttruck1;
        } else {
            $firsttruck2=$_POST['firsttruck2'];
        }
        if($_POST['lasttruck2']=='')
        {
            $lasttruck2=$lasttruck1;
        } else {
            $lasttruck2=$_POST['lasttruck2'];
        }
    } else {
        $runstart2=$_POST['runstart1'];
        $runfinish2=$_POST['runfinish1'];
        $firstbundle2=$_POST['firstbundle1'];
        $lastbundle2=$_POST['lastbundle1'];
        $firsttruck2=$_POST['firsttruck1'];
        $lasttruck2=$_POST['lasttruck1'];
    }
    $s1=strtotime($runstart1);
    $f1=strtotime($runfinish1);
    $s2=strtotime($runstart2);
    $f2=strtotime($runfinish2);
    if ($f2>$f1)
    {
        $fin=$f2;
    } else {
        $fin=$f1;
    }
    if ($s2>$s1)
    {
        $start=$s1;
    } else {
        $start=$s2;
    }
    $runlength=round(($fin-$start)/60,2); //run time in minutes
    
    
    //now look for station information
    $stationvalues="";
    $calchoppers=0;
    $feeders=array();
    foreach($_POST as $key=>$value)
    {
        if(substr($key,0,8)=='station_')
        {
            $stationInsertID = 0;
            $stationPackID = 0;
            //this post item holds the station insert id, so it only flags on those stations with an assigned insert
            $keyparts=explode("_",$key);
            $stationid=$keyparts[1];
            $stationInsertID=$value; //"J" is passed for a generic jacket, J-ID is passed for a package
            
            $personid=$_POST['stationd_'.$stationid.'_person'];
            $quantity=$_POST['stationd_'.$stationid.'_count'];
            if($stationInsertID=='' || $stationInsertID=='J'){$iid=0;}
            if(substr($stationInsertID,0,2)=='J-'){$stationPackID=str_replace("J-","",$stationInsertID);$stationInsertID=0;}
            if($personid==''){$personid=0;}
            if($stationid==''){$stationid=0;}
            if($quantity==''){$quantity=0;}
            $runningQuantity+=$quantity;
            if($iid!=0)
            {
                $stations[]=$stationid;
                
            }
            if($quantity>0){
                $calchoppers++;
            }
            if(!in_array($personid,$feeders)){$feeders[]=$personid;$feedcount++;$calcStaff++;}
            $stationvalues.="('$packageid', '$stationid', '$stationInsertID','$stationPackID', '$personid', '$quantity'),";
        }
    }
    //update run data with the runningQuantity for the calc_pieces factor
    $calcpieces=$runningQuantity;
    
     
    $stationvalues=substr($stationvalues,0,strlen($stationvalues)-1);
    //delete any existing
    $sql="DELETE FROM jobs_inserter_rundata_stations WHERE package_id='$packageid'";
    $dbDelete=dbexecutequery($sql);
    $error.=$dbDelete['error'];
    //add the new ones
    if($stationvalues!='')
    {
        $sql="INSERT INTO jobs_inserter_rundata_stations (package_id, station_id, insert_id, prepack_id, user_id, quantity) VALUES $stationvalues";
        $dbInsert=dbinsertquery($sql);
        $error.=$dbInsert['error'];
    }
    
    $feedhours=round($runlength*$feedcount/60,2);
    $totalhours=round($runlength*$calcStaff/60,2);
    if ($feedhours!=0)
    {
        $feederpiecesperhour=round($systemtotalpieces/$feedhours,2);
        $calcfeederpiecesperhour=round($calcpieces/$feedhours,2); 
    } else {
        $piecesperhour=0;
        $calcpiecesperhour=0;
    }
    if ($totalhours!=0)
    {
        $totalpiecesperhour=round($systemtotalpieces/$totalhours,2);
        $calctotalpiecesperhour=round($calcpieces/$totalhours,2);
    } else {
        $totalpiecesperhour=0;
        $calctotalpiecesperhour=0;
    }
    
    if($calctotalpiecesperhour==''){$calctotalpiecesperhour=0;}
    if($leftover==''){$leftover=0;}
    if($hopperfeeders==''){$hopperfeeders=0;}
    if($dockstaff==''){$dockstaff=0;}
    if($stackerstaff==''){$stackerstaff=0;}
    if($feedhours==''){$feedhours=0;}
    if($feedcount==''){$feedcount=0;}
    if($feederpiecesperhour==''){$feederpiecesperhour=0;}
    if($calcfeederpiecesperhour==''){$calcfeederpiecesperhour=0;}
    if($totalpiecesperhour==''){$totalpiecesperhour=0;}
    if($runlength==''){$runlength=0;}
    if($presstemp==''){$presstemp=0;}
    if($stackertemp==''){$stackertemp=0;}
    if($docktemp==''){$docktemp=0;}
    if($maildesk==''){$maildesk=0;}
    if($pallets==''){$pallets=0;}
    if($systemtotalpieces==''){$systemtotalpieces=0;}
    if($calcpieces==''){$calcpieces=0;}
    if($pressrequest==''){$pressrequest=0;}
    if($packagesbuilt==''){$packagesbuilt=0;}
    
    
    
    
    
    
    if ($dataid==0)
    {
        $sql="INSERT INTO jobs_inserter_rundata (pub_id, package_id, plan_id, sticky_note, address, 
        doubleout, hoppers_used, leftover_jackets, employee_count, pallets, system_total_pieces, 
        calc_total_pieces, press_request, packages_built, signoff, setupby, package_startdatetime, 
        package_stopdatetime, runstart_one, runfinish_one, firstbundle_one, lastbundle_one, firsttruck_one, 
        lasttruck_one, runstart_two, runfinish_two, firstbundle_two, lastbundle_two, firsttruck_two, lasttruck_two, 
        notes, dock_staff, stacker_staff, feed_hours, feeders, feeder_pieces_per_hour, 
        calc_feeder_pieces_per_hour,total_pieces_per_hour, calc_total_pieces_per_hour, run_length, press_temp, 
        stacker_temp, dock_temp, maildesk, other_equipment, calc_hoppers_used, calc_total_staff, sticky_note_count, address_count, setup_starttime, setup_stoptime, bundle_size_hd, bundle_size_sc, total_bundles, fte_pre, fte_post, run_notes) VALUES ('$pubid', '$packageid', '$planid', '$sticky', '$address', 
        '$doubleout', '$hoppers', '$leftover', '$employeecount', '$pallets', '$systemtotalpieces', '$calcpieces', 
        '$pressrequest', '$packagesbuilt', '$signoff', '$setupby', '$packagestartdatetime', '$packagestopdatetime', 
        '$runstart1', '$runfinish1', '$firstbundle1', '$lastbundle1', '$firsttruck1', '$lasttruck1', '$runstart2', 
        '$runfinish2', '$firstbundle2','$lastbundle2', '$firsttruck2', '$lasttruck2', '$notes', 
        '$dockstaff', '$stackerstaff', '$feedhours', '$feedcount', '$feederpiecesperhour', 
        '$calcfeederpiecesperhour', '$totalpiecesperhour', '$calctotalpiecesperhour', '$runlength', '$presstemp', 
        '$stackertemp', '$docktemp', '$maildesk', '$otherequipment', '$calchoppers', '$calcStaff', '$stickycount', '$addresscount', '$setupStart', '$setupStop', '$bundleSizeHD', '$bundleSizeSC', '$totalBundles','$ftePre', '$ftePost', '$notes')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error']; 
    } else {
        $sql="UPDATE jobs_inserter_rundata SET pub_id='$pubid', package_id='$packageid', plan_id='$planid', 
        sticky_note='$sticky', dock_staff='$dockstaff', press_staff='$pressstaff', calc_total_staff='$calcStaff', 
        stacker_staff='$stackerstaff', address='$address', doubleout='$doubleout', hoppers_used='$hoppers', 
        leftover_jackets='$leftover', employee_count='$employeecount', pallets='$pallets', 
        system_total_pieces='$systemtotalpieces', calc_total_pieces='$calcpieces', press_request='$pressrequest', 
        packages_built='$packagesbuilt', signoff='$signoff', setupby='$setupby', calc_hoppers_used='$calchoppers',
        package_startdatetime='$packagestartdatetime', package_stopdatetime='$packagestopdatetime', 
        runstart_one='$runstart1', runfinish_one='$runfinish1', firstbundle_one='$firstbundle1', 
        lastbundle_one='$lastbundle1', firsttruck_one='$firsttruck1', lasttruck_one='$lasttruck1', 
        runstart_two='$runstart2', runfinish_two='$runfinish2', firstbundle_two='$firstbundle2', 
        lastbundle_two='$lastbundle2', firsttruck_two='$firsttruck2', lasttruck_two='$lasttruck2', 
        notes='$notes', run_length='$runlength', feed_hours='$feedhours', feeders='$feedcount', 
        feeder_pieces_per_hour='$feederpiecesperhour', calc_feeder_pieces_per_hour='$calcfeederpiecesperhour', 
        total_pieces_per_hour='$totalpiecesperhour', calc_total_pieces_per_hour='$calctotalpiecesperhour', 
        press_temp='$presstemp', stacker_temp='$stackertemp', dock_temp='$docktemp', maildesk='$maildesk', 
        other_equipment='$otherequipment', sticky_note_count='$stickycount', address_count='$addresscount', 
        setup_starttime = '$setupStart', setup_stoptime='$setupStop', bundle_size_hd='$bundleSizeHD', 
        bundle_size_sc='$bundleSizeSC', total_bundles='$totalBundles', fte_pre='$ftePre', fte_post='$ftePost', 
        run_notes='$notes' WHERE id=$dataid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    
    
    
    //now apply wear and tear to this station
    if(count($stations)>0)
    {
        inserterWearTear($packageid);
    }
    
    
    if ($error!='')
    {
        setUserMessage('There was a problem saving the insert run data.<br>'.$error,'error');
    } else {
        setUserMessage('Insert run data has been successfully saved.','success');
    }
    if($_POST['popout']=='true')
    {
        print "<script>window.close();</script>";
    } else {
       redirect("?action=list");
    }
}

function package_nav()
{
    //lets see which publications this user has access to
    global $User;
    $sql="SELECT * FROM user_publications WHERE user_id=".$User->id." AND value=1";
    $dbPubs=dbselectmulti($sql);
    if ($dbPubs['numrows']>0)
    {
        $pubids="";
        foreach($dbPubs['data'] as $pub)
        {
            $pubids.=$pub['pub_id'].",";
        }
        $pubids=substr($pubids,0,strlen($pubids)-1);
        $pubfilter="AND A.pub_id IN ($pubids)";
    } else {
        $pubfilter="";
    }
    global $siteID;
    
    $jobid=intval($_GET['packageid']);
    $lastid=intval($_GET['lastid']);
    $action=addslashes($_GET['action']);
    $now=date("Y-m-d H:i");
    $nowdate=date("Y-m-d");
    if ($jobid!=0)
    {
        $sql="SELECT * FROM jobs_inserter_packages WHERE id=$jobid";
        $dbCurrentJob=dbselectsingle($sql);
        $start=$dbCurrentJob['data']['package_startdatetime'];
        $end=$dbCurrentJob['data']['package_stopdatetime'];
        if ($action=='prev')
        {
            $sql="SELECT * FROM jobs_inserter_packages WHERE package_stopdatetime<='$start' AND A.status=1 $pubfilter ORDER BY package_stopdatetime DESC LIMIT 1";    
        } elseif ($action=='next')
        {
            $sql="SELECT * FROM jobs_inserter_packages WHERE package_startdatetime>='$end' AND A.status=1 $pubfilter ORDER BY A.package_startdatetime ASC LIMIT 1";
        }
    }elseif($lastid!=0)
    {   
        //ok, we have a lastid, so we're looking for a less or greater job id than the last one
        $sql="SELECT * FROM jobs_inserter_packages WHERE id=$lastid";
        $dbLastJob=dbselectsingle($sql);
        $start=$dbLastJob['data']['package_startdatetime'];
        $end=$dbLastJob['data']['package_stopdatetime'];
        if ($action=='prev')
        {
            $sql="SELECT * FROM jobs_inserter_packages A WHERE A.site_id=$siteID AND A.package_stopdatetime<='$start' AND A.status=1 AND $pubfilter ORDER BY A.package_stopdatetime DESC LIMIT 1";    
        } elseif ($action=='next')
        {
            $sql="SELECT * FROM jobs_inserter_packages A WHERE A.site_id=$siteID AND A.package_startdatetime>='$end' AND A.status=1 AND $pubfilter ORDER BY A.package_startdatetime ASC LIMIT 1";
        }
        if ($_GET['bug']){print "<br>Got a last id of $lastid<br>Checking with action of $action and a sql of $sql<br>";}
        $dbCurrentJob=dbselectsingle($sql);
        if ($dbCurrentJob['numrows']==0)
        {
            //no previous or next job, so default to the job closest to now
            $sql="SELECT * FROM jobs_inserter_packages A WHERE A.site_id=$siteID AND A.package_startdatetime>='$now' AND A.status=1 ORDER BY A.package_startdatetime ASC LIMIT 1";
            $dbCurrentJob=dbselectsingle($sql);
            if ($dbCurrentJob['numrows']==0)
            {
                //so no jobs beyond this point, just pick the latest job then
                $sql="SELECT * FROM jobs_inserter_packages A WHERE A.site_id=$siteID AND A.status=1 AND $pubfilter ORDER BY A.package_startdatetime DESC LIMIT 1";
                $dbCurrentJob=dbselectsingle($sql);
            }
        }
    
    } else { 
        //no job id or last id specified, just pick the job closest to now
        $sql="SELECT * FROM jobs_inserter_packages A WHERE A.site_id=$siteID AND A.package_startdatetime>='$now' AND A.status=1 $pubfilter ORDER BY A.package_startdatetime ASC LIMIT 1";
        $dbCurrentJob=dbselectsingle($sql);
        if ($dbCurrentJob['numrows']==0)
        {
            //so no jobs beyond this point, just pick the latest job then
            $sql="SELECT * FROM jobs_inserter_packages A WHERE site_id=$siteID $pubfilter ORDER BY package_startdatetime DESC LIMIT 1";
            $dbCurrentJob=dbselectsingle($sql);
        }
        
    }
    $jobid=$dbCurrentJob['data']['id'];
    //print "Current id is $jobid";
    //print "Using $sql";
    $job=$dbCurrentJob['data'];
    $jobid=$job['id'];
    $pubdate=date("D F j, Y",strtotime($job['pub_date']));

    //gather all the data about this job
    $sql="SELECT * FROM publications WHERE id=$job[pub_id]";
    $dbPub=dbselectsingle($sql);
    $pub=$dbPub['data'];
    $pubname=$pub['pub_name'];

    $sql="SELECT * FROM publications_insertruns WHERE id=$job[run_id]";

    $dbRun=dbselectsingle($sql);
    $run=$dbRun['data'];
    $runname=$run['run_name'];

    $prev="<a href='?action=prev&lastid=$jobid'><i class='fa fa-2x fa-backward'></i></a>\n";
    $next="<a href='?action=next&lastid=$jobid'><i class='fa fa-2x fa-forward'></i></a>\n";
        
   
    
    print "<div id='nav' class='row' style='margin-bottom:20px;'>\n";
        print "<div class='col-xs-1' style='padding:6px;'><span class='pull-right'>$prev</span></div>";
        print "<div id='nav_center' class='col-xs-10'>\n";
            print "<p class='bg-primary text-center' style='font-size:16px;padding:10px;'>Current Job is $pubname - $runname run for $pubdate - #$jobid</p>";//save a hidden text field here with the job id
            print "<input type='hidden' id='jobid' name='jobid' value='$jobid'>\n";
            print "<input type='hidden' id='ares' name='ares' value=''>\n";
            //here we will make a quick jump of jobs 24 hours back and 48 hours ahead
            $start=date("Y-m-d H:i",strtotime("-24 hours"));
            $end=date("Y-m-d H:i",strtotime("+48 hours"));
            
            $sql="SELECT A.*, B.pub_name FROM jobs_inserter_packages A, publications B WHERE A.pub_id=B.id AND A.status<>99 AND A.package_startdatetime>='$start' AND A.package_startdatetime<='$end' $pubfilter ORDER BY A.package_startdatetime ASC";
            //print "$sql<br />";
            
            $dbQuickJobs=dbselectmulti($sql);
            
            if ($dbQuickJobs['numrows']>0)
            {
                print "<select name='quickjob' id='quickjob' onchange='pressQuickJump(this.value);'>\n";
                print "<option id='opt_0' name='opt_0' value='0'>Select quick jump job</option>\n";
                foreach($dbQuickJobs['data'] as $qj)
                {
                    $pname=$qj['pub_name'];
                    $rname=$qj['run_name'];
                    $qjpubdate=date("D F j",strtotime($qj['pub_date']));
                    $starttime=date("m/d H:i",strtotime($qj['package_startdatetime']));
                    print "<option id='opt_$qj[id]' name='opt_$qj[id]' value='$qj[id]'>$pname - $rname for $qjpubdate, Start: $starttime</option>\n";
                }
                print "</select>\n";
            }
        print "</div>\n";
        print "<div class='col-xs-1' style='padding:6px;'><span>$next</span></div>";
        
    print "</div>\n";
   
    return $job;
}

$Page->footer();
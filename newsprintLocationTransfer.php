<?php
include("includes/boot.php") ;

if($_POST)
{
    transferRolls();
} else {
    get_rolltags();
} 
  
  
function get_rolltags()
{
    
    global $defaultNewsprintLocation;
        
    print "<form method=post class='form-horizontal'>\n";
    $slocations = buildLocations('newsprint');
    make_select('storageLocation',$slocations[$defaultNewsprintLocation],$slocations,'Transfer rolls to','Select where you are transferring rolls to');
    make_text('manifest_number','','Manifest Number','Enter a manifest number to transfer a full load of newsprint.');     
    make_textarea('rolltags',$tags,'Tags','',60,30,false);
    make_submit('submit','Transfer Rolls');
    print "</form>\n";
     
}


function transferRolls()
{
    $locationID=intval($_POST['storageLocation']);
    
    $sql="SELECT * FROM storage_locations WHERE id=$locationID";
    $dbLocation = dbselectsingle($sql);
    $location = $dbLocation['data']['location_name'];
    
    $tags=addslashes($_POST['rolltags']);
    if($_POST['manifest_number']!='')
    {
        $manifest = addslashes($_POST['manifest_number']);
        $sql="UPDATE rolls SET storage_location='$locationID' WHERE manifest_number LIKE '%$manifest%'";
        $dbUpdate=dbexecutequery($sql);
        print "<div class='alert alert-success' role='alert'>The requested manifest has been moved to $location</div>";
    } else {
        if($tags!='')
        {
            $tags = explode("\n",$tags);
            $cleantags = array();
            foreach($tags as $tag)
            {
                if(trim($tag)!='')
                {
                    $cleantags[]=$tag;
                }
            }
            $tags=$cleantags;
            $sql="SELECT * FROM accounts WHERE newsprint=1";
            $dbVendors=dbselectmulti($sql);
            if ($dbVendors['numrows']>0)
            {
                $count=0;
                print "Moving ".count($tags)." rolls<br>";
                foreach($tags as $rolltag)
                {
                    $rolltag = trim($rolltag);
                    if (!in_array($rolltag,$processed))
                    {
                        print "Testing $rolltag<br>";
                        foreach($dbVendors['data'] as $vendor)
                        { 
                              $vendorid=$vendor['id'];
                              $rollremoval=$vendor['rolltag_removal'];
                              //ok, what we are going to have to do is check the rolls after massaging the rolltag for each vendor
                              $checktag=substr($rolltag,$rollremoval);//this should do it
                              $sql="SELECT id, status FROM rolls WHERE roll_tag='$checktag'";
                              $dbRoll=dbselectsingle($sql);
                              if ($dbRoll['numrows']>0)
                              {
                                $rollid=$dbRoll['data']['id'];
                                $status=$dbRoll['data']['status'];
                                if ($status=='9')
                                {
                                    if (!in_array($rolltag,$alreadyused))
                                    {
                                        $alreadyused[]=$rolltag;    
                                    }
                                    
                                } else {
                                    //ok, we've found a real roll tag, and it has not been processed to completion by the business office before
                                    //so now we just need to update with a status of 9
                                    $count++;
                                    $processed[]=$rolltag;
                                    $sql="UPDATE rolls SET storage_location = $locationID WHERE id=$rollid";
                                    $dbUpdate=dbexecutequery($sql);
                                    break;
                                }
                              }
                        }
                          if ($rollid==0)
                          {
                              //do one last check without removing anything from the rolltag
                              $sql="SELECT id, status FROM rolls WHERE roll_tag='$rolltag'";
                              $dbRoll=dbselectsingle($sql);
                              if ($dbRoll['numrows']>0)
                              {
                                $rollid=$dbRoll['data']['id'];
                                $status=$dbRoll['data']['status'];
                                if ($status=='9')
                                {
                                    if (!in_array($rolltag,$alreadyused))
                                    {
                                        $alreadyused[]=$rolltag;    
                                    }
                                } else {
                                    //ok, we've found a real roll tag, and it has not been processed to completion by the business office before
                                    //so now we just need to update with a status of 9
                                    $processed[]=$rolltag;
                                    $count++; 
                                    $sql="UPDATE rolls SET storage_location = $locationID WHERE id=$rollid";
                                    $dbUpdate=dbexecutequery($sql);
                                    $error=$dbUpdate['error']; 
                                }
                              } 
                          }
                        if ($rollid==0)
                        {
                            if (!in_array($rolltag,$missing))
                            {
                                $missing[]=$rolltag;
                            }
                                        
                        }
                        $processed[]=$rolltag;
                                    
                    }
                }
            }   
            if(count($missing)>0)
            {
                print "The following rolltags were not found in the system:";
                print "<ul>";
                foreach($missing as $tag)
                {
                    print "<li>$tag</li>";
                }
                print "</ul>";
            } 
             
            if(count($alreadyused)>0)
            {
                print "The following rolltags were shown as having been already removed from inventory:";
                print "<ul>";
                foreach($alreadyused as $tag)
                {
                    print "<li>$tag</li>";
                }
                print "</ul>";
            } 
            if ($error!='')
            {
                print $error;
            } else {
                print "Successfully transferred $count rolls to $location<br><br><a href='?action=trasfer'>Click here to transfer more rolls.</a>";
            }
        }
    }
    
}
$Page->footer(); 
<?php
 
 include ("includes/boot.php"); 

    global $pubids;
    print "<div class='hidden-print'>\n";
        print "<form method=post class='form-horizontal'>\n";
        make_date('start',date("Y-m-d"),'Start Date');
        make_date('stop',date("Y-m-d",strtotime("+1 month")),'Stop Date');
        make_submit('submit','Show Calendar');
        print "</form>\n";
    print "</div>\n";
    if ($_POST['submit'])
    {
        $start=$_POST['start'];
        $stop=$_POST['stop'];
    } else {
        $start=date("Y-m-d");
        $stop=date("Y-m-d",strtotime("+1 month"));
    }
     
    $sql="SELECT A.*, B.pub_name, C.run_name FROM jobs A, publications B, publications_runs C 
    WHERE A.site_id=".SITE_ID." AND A.pub_id=B.id AND A.run_id=C.id AND A.status<>99 AND A.startdatetime>='$start' 
    AND A.startdatetime<='$stop' AND A.pub_id=B.id AND A.pub_id IN ($pubids) ORDER BY A.startdatetime ASC";
    $dbJobs=dbselectmulti($sql);

    if ($dbJobs['numrows']>0)
    {
        print "<table class='table table-bordered table-striped'>\n";
        print "<tr><th><a href='#' onclick='window.print();'><i class='fa fa-print'></i> Print</a></th><th colspan=4><p style='text-align:center;font-size:18px;font-weight:bold;'>Jobs scheduled</p></th><th></th></tr>\n";
        print "<tr><th>Publication</th><th>Run Name</th><th>Pub Date</th><th>Press Time</th><th>Draw</th><th>Paper</th></tr>\n";
        $cdate=$start;
        print "<tr><td colspan=6 style='text-align;center;'><p style='text-align:center;font-weight:bold;'>Jobs printing: ".date("l, F jS Y",strtotime($cdate))."</p></td></tr>\n";
        foreach($dbJobs['data'] as $job)
        {
            if (date("Y-m-d",strtotime($job['startdatetime']))!=$cdate)
            {
                $cdate=date("Y-m-d",strtotime($job['startdatetime']));
                print "<tr><td style='border-top: 2px solid black;height:2px;padding:0;margin:0;' colspan=6>&nbsp;</td></tr>\n";
                print "<tr><td colspan=6 style='text-align;center;'><p style='text-align:center;font-weight:bold;'>Jobs printing: ".date("l, F jS Y",strtotime($cdate))."</p></td></tr>\n";
            
            }
            print "<tr>\n";
            print "<td>$job[pub_name]</td>";        
            print "<td>$job[run_name]<br />".$folders[$job['folder']]."</td>";        
            print "<td>$job[pub_date]</td>";        
            print "<td>".date("D m/d",strtotime($job['startdatetime']))." at ".date("H:i",strtotime($job['startdatetime']))."</td>";        
            print "<td>$job[draw]</td>";        
            $jobid=$job['id'];
            print "<td>".$GLOBALS['papertypes'][$job['papertype']]."</td>";        
            
            
            print "</tr>\n";
            
            
            
        }
        print "</table>\n";
        
        //lets display a box at the bottom listing any unscheduled jobs in the system
        $date=date("Y-m-d");
        $sql="SELECT A.*, B.pub_name, C.run_name FROM jobs A, publications B, publications_runs C WHERE A.site_id=$siteID AND A.pub_date>='$date' AND A.continue_id=0 AND A.startdatetime='' AND A.pub_id=B.id AND A.run_id=C.id ORDER BY A.pub_date";
        $dbUnscheduled=dbselectmulti($sql);
        if ($dbUnscheduled['numrows']>0)
        {
            print "<div style='width:670px;border:2 px solid black;padding:4px;font-family:Tahoma'>\n";
            print "<p style='font-weight:bold;font-size:16px;'>The following jobs are in the system but have NOT been scheduled yet.</p>\n";
            foreach($dbUnscheduled['data'] as $job)
            {
                $jobid=$job['id'];
                $pubdate=date("D, F m Y",strtotime($job['pub_date']));
                print "<p>$job[pub_name] - $job[run_name] publishing on $pubdate<a href='pressJobs.php?action=schedulejob&jobid=$jobid' target='_blank'>Click here to schedule it</a></p>\n";    
            }
            
            print "</div>\n";
        }
    } else {
        print "<h2>You lucked out!<br>There are no scheduled jobs during that time...or something has gone horribly wrong ;)</h2>\n";
    }


function getjobs($date)
{
    global $pubids, $siteID;
    $sql="SELECT A.*, B.pub_name, B.pub_code, C.run_name FROM jobs A, publications B, publications_runs C WHERE A.site_id=$siteID AND A.pub_id=B.id AND A.run_id=C.id AND A.continue_id=0 AND A.status<>99 AND A.startdatetime>='$date 00:00:01' AND A.startdatetime<='$date 23:59:59' AND A.pub_id=B.id AND A.pub_id IN ($pubids) ORDER BY A.startdatetime ASC";
    $dbJobs=dbselectmulti($sql);

    if ($dbJobs['numrows']>0)
    {
        foreach($dbJobs['data'] as $job)
        {
            print $job['pub_code']." - ".$job['run_name']." - ".date("m/d",strtotime($job['pub_date']))."<br />\n";        
            
        }
        
    }
}            

$Page->footer();
<?php
  include("includes/boot.php");
  
  /* this page will simply output a change log of all "completed" version changes */
  
  $tab=$_GET['tab'];
  if($tab=='' || !isset($_GET['tab'])){$tab='changes';}
  
  if($_POST)
  {
      $id=intval($_POST['id']);
      $submitid=intval($_POST['submitter_id']);
      $request=addslashes($_POST['request']);
      
      if($id==0)
      {
          $sql="INSERT INTO core_version_request (request,submitted_by,submitted_datetime,incorporated_version) VALUES ('$request',$submitid,'".date("Y-m-d H:i")."',0)";
          $dbInsert=dbinsertquery($sql);
      } else {
          $sql="UPDATE core_version_request SET request='$request' WHERE id=$id";
          $dbUpdate=dbexecutequery($sql);
      }
      redirect("?tab=request");
  }
  ?>
  <div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="<?php echo ($tab=='changes'?'active':'') ?>"><a href="#changes" aria-controls="changes" role="tab" data-toggle="tab">Change Log</a></li>
    <li role="presentation" class="<?php echo ($tab=='future'?'active':'') ?>"><a href="#future" aria-controls="future" role="tab" data-toggle="tab">Future Version Plans</a></li>
    <li role="presentation" class="<?php echo ($tab=='request'?'active':'') ?>"><a href="#request" aria-controls="request" role="tab" data-toggle="tab">Request a feature/Report a bug</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane <?php echo ($tab=='changes'?'active':'') ?>" id="changes">
    <?php changes(); ?>
    </div>
    <div role="tabpanel" class="tab-pane <?php echo ($tab=='future'?'active':'') ?>"  id="future">
    <?php future(); ?>
    </div>
    <div role="tabpanel" class="tab-pane <?php echo ($tab=='request'?'active':'') ?>" id="request">
    <?php requests(); ?>
    </div>
  </div>

</div>
  <?php
  
  function changes() {
      $sql="SELECT * FROM core_version WHERE version_status=1 ORDER BY version_alpha DESC, version_beta DESC, version_gamma DESC";
      $dbChanges = dbselectmulti($sql);
      
      if($dbChanges['numrows']>0)
      {
          print "
            <div class='container'>
                <div class='row'>";
          print '
                  <div class="page-header">
                    <h1>Change Log</h1>
                  </div>
                  ';
          foreach($dbChanges['data'] as $change)
          {
              $title = stripslashes($change['version_title']);
              $completeDate = date("m/d/Y",strtotime($change['complete_date']));
              $alpha = $change['version_alpha'];
              $beta = $change['version_beta'];
              $gamma = $change['version_gamma'];
              $desc = stripslashes($change['version_details']);
              ?>
              <div class="panel panel-primary">
              <div class="panel-heading"><b><?php echo $alpha.".".$beta.".".$gamma ?></b> - <?php echo $title ?><span class='pull-right'>Completed: <?php echo $completeDate ?></span></div>
              <div class="panel-body">
                <?php echo $desc ?>
              </div></div>
              <?php
          }
          print "</div>\n</div>\n";
      }
  }
  
  function future()
  {
      $sql="SELECT * FROM core_version WHERE version_status=0 ORDER BY version_alpha ASC, version_beta ASC, version_gamma ASC";
      $dbChanges = dbselectmulti($sql);
      
      if($dbChanges['numrows']>0)
      {
          print "
            <div class='container'>
                <div class='row'>";
          print '
                  <div class="page-header">
                    <h1>Future Plans</h1>
                  </div>
                  ';
          foreach($dbChanges['data'] as $change)
          {
              $title = stripslashes($change['version_title']);
              $completeDate = date("m/d/Y",strtotime($change['version_target']));
              $alpha = $change['version_alpha'];
              $beta = $change['version_beta'];
              $gamma = $change['version_gamma'];
              $desc = stripslashes($change['version_details']);
              ?>
              <div class="panel panel-success">
              <div class="panel-heading"><b><?php echo $alpha.".".$beta.".".$gamma ?></b> - <?php echo $title ?><span class='pull-right'>Planned: <?php echo $completeDate ?></span></div>
              <div class="panel-body">
                <?php echo $desc ?>
              </div></div>
              <?php
          }
          print "</div>\n</div>\n";
      }
      
  }
  
  
  function requests()
  {
      global $User;
      print "<form method=post class='form-horizontal'>\n";
      if(isset($_GET['id']))
      {
          $sql="SELECT * FROM core_version_request WHERE id=".intval($_GET['id']);
          $dbRequest=dbselectsingle($sql);
          $submitID = $dbRequest['data']['submited_by'];
      } else {
          $submitID=$User->id;
      }
      make_textarea('request',stripslashes($dbRequest['data']['request']),'Request','Enter a description of the feature or bug you are reporting.');
      make_hidden('id',intval($_GET['id']));
      make_hidden('submitter_id',$submitID);
      make_submit('submit','Save Request');
      print "</form>\n";
      
      //get all versions
      $sql="SELECT id, version_alpha, version_beta, version_gamma, version_title FROM core_version";
      $dbVersions=dbselectmulti($sql);
      if($dbVersions['numrows']>0)
      {
          foreach($dbVersions['data'] as $ver)
          {
             $versions[$ver['id']]=$ver['version_alpha'].'.'.$ver['version_beta'].'.'.$ver['version_gamma'].' -- '.stripslashes($ver['version_title']);
          }
      }
      
      $sql="SELECT * FROM core_version_request WHERE submitted_by='".$User->id."' ORDER BY submitted_datetime";
      $dbRequests=dbselectmulti($sql);
      print "<table class='table table-striped table-bordered'>\n";
      print "<tr><th>Submit Date</th><th>Request</th><th>In version</th></tr>\n";
      if($dbRequests['numrows']>0)
      {
          foreach($dbRequests['data'] as $request)
          {
              print "<tr><td>".date("Y-m-d H:i",strtotime($request['submitted_datetime']))."</td>";
              print "<td>".wordwrap(stripslashes($request['request']),100,true)."</td>";
              if($request['incorporated_version']!=0)
              {
                print "<td>".$versions[$request['incorporated_version']]."</td>";
              } else {
                print "<td><a href='?tab=request&id=$request[id]' class='btn btn-primary'>You can still edit</a></td>";  
              }
              print "</tr>\n";
          }
          
      }
      print "</table>\n";
  }
  $Page->footer();
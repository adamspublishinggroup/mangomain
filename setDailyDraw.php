<?php
//the purpose of this script it to allow a user to set the draw for all runs for a specified publication and publication date  
include("includes/boot.php") ;

if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}

switch($action)
{
    case "Set Draw":
    set_draw();
    break;
    
    case "Save Draw":
    save_draw();
    break;
    
    default:
    get_pub();
}

function get_pub()
{
    global $pubs;
    print "<form id='drawzones' method=post class='form-horizontal'>\n";
    print "<h4>Global draw set</h4><p>This tool will set the draw for all runs (that have the draw link enabled) for the specified date and publication. Zone draws will also be used on the inserter for zoning counts. Please use with caution!</p>\n";
    make_select('pub_id',$pubs[0],$pubs,'Publication','Specify publication to set the draw for');
    make_date('pub_date',date("Y-m-d",strtotime("+1 day")),'Pub Date','Publication date to set the draw for');
    make_submit('submit','Set Draw');
    print "</form>\n";  
    
}

function set_draw()
{
    global $pubs;
    $pubID=intval($_POST['pub_id']);
    $pubDate = addslashes($_POST['pub_date']);
    print "<form id='drawzones' method=post class='form-horizontal'>\n";
    print "<h4>Global draw set</h4><p>This tool will set the draw for all runs (that have the draw link enabled) for the specified date and publication. Zone draws will also be used on the inserter for zoning counts. Please use with caution!</p>\n";
    make_descriptor($pubs[$pubID],'Publication');
    make_descriptor($pubDate,'Publication Date');
    make_hidden('pub_id',$pubID);
    make_hidden('pub_date',$pubDate);
    
    $sql="SELECT * FROM publications_insertzones WHERE pub_id = $pubID ORDER BY zone_order ASC";
    $dbZones=dbselectmulti($sql);
    if($dbZones['numrows']>0)
    {
        $sql="SELECT * FROM publications_draws WHERE pub_id=$pubID AND pub_date='$pubDate'";
        $dbExisting = dbselectmulti($sql);
        
        foreach($dbZones['data'] as $zone)
        {
            $found = false;
            //see if there is an existing one
            if($dbExisting['numrows']>0)
            {
                foreach($dbExisting['data'] as $e)
                {
                    if($e['zone_id']==$zone['id'])
                    {
                        $zoneDraw = $e['draw'];
                        $found = true;
                    }   
                }
                if(!$found)
                {
                    $zoneDraw = 0;
                }
            } else {
                $zoneDraw = 0;
            }
            make_number('zone_'.$zone['id'],$zoneDraw,'Zone '.$zone['zone_label'],'Draw for '.$zone['zone_name'].' zone');
        }
    }
    make_number('draw',0,'Overall draw','Set an overall draw (will be over-ridden by a zone total, use this only if you will not be setting zone draws.');
    make_submit('submit','Save Draw');
    print "</form>\n";
    
}

function save_draw()
{
    global $pubs;
    $date=addslashes($_POST['pub_date']);
    $pub=intval($_POST['pub_id']);
    $draw=intval($_POST['draw']);
    $total=0;
    foreach($_POST as $key=>$value)
    {
        if(substr($key,0,5)=='zone_')
        {
            $total+=$value;
            $zoneid=str_replace("zone_","",$key);
            $inserts[]="($pub,$zoneid,'$date',$value)";
        }
    }
    if($draw=='' || $draw==0){$draw=$total;}
    
    //remove any existing publications_draws
    $sql="DELETE FROM publications_draws WHERE pub_id=$pub AND pub_date='$date'";
    $dbClear = dbexecutequery($sql);
    
    if(count($inserts)>0)
    {
        //now create the new one
        $sql="INSERT INTO publications_draws (pub_id, zone_id, pub_date, draw) VALUES ".implode(",",$inserts);
        $dbAdd = dbinsertquery($sql);
    }
        
    $sql="UPDATE jobs SET draw='$draw' WHERE pub_id='$pub' AND pub_date='$date'
AND run_id IN (SELECT id FROM publications_runs WHERE pub_id='$pub' AND allow_draw_link=1)";
    $dbUpdate=dbexecutequery($sql);
    $error=$dbUpdate['error'];
    if($error=='')
    {
        setUserMessage('All jobs for '.$pubs[$pub].' running on '.date("m/d/Y",strtotime($date)).' have been set to a draw of '.$draw,'success');    
    } else {
        setUserMessage('There was a problem setting the draw amount.<br>'.$error,'error');
    }
    redirect("index.php");
}

$Page->footer();
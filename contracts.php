<?php
include("includes/boot.php") ;
if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}
    switch ($action)
    {
        case "Save Contract":
        save_contract('insert');
        break;
        
        case "Update Contract":
        save_contract('update');
        break;
        
        case "add":
        setup_contracts('add');
        break;
        
        case "edit":
        setup_contracts('edit');
        break;
        
        case "delete":
        setup_contracts('delete');
        break;
        
        case "list":
        setup_contracts('list');
        break;
        
        default:
        setup_contracts('list');
        break;
        
    } 
    
    
function setup_contracts($action)
{
    if ($action=='add' || $action=='edit')
    {
        if ($action=='add')
        {
            $button="Save Contract";
        } else {
            $button="Update Contract";
            $id=intval($_GET['id']);
            $sql="SELECT * FROM contracts WHERE id=$id";
            $dbContract=dbselectsingle($sql);
            $contract=$dbContract['data'];
            $company=stripslashes($contract['contract_name']);
            $station_setup=stripslashes($contract['inserter_station_setup']);
            $per_piece=stripslashes($contract['inserter_per_piece']);
            
        }
        print "<form method=post class='form-horizontal'>\n";
        print "<div id='contractTabs'>\n";
            print "<ul>
                    <li><a href='#general'>General</a></li>
                    <li><a href='#press'>Press</a></li>
                    <li><a href='#inserter'>Inserter</a></li>  
                    ";
            print "</ul>\n";
                    
            print "<div id='general'>\n";
                make_text("contract_name",$name,'Contract Name');
            print "</div>";
            
            print "<div id='press'>\n";
            print "</div>";
            
            print "<div id='inserter'>\n";
                make_number('station_setup',$station_setup,'Station Setup');
                make_number('per_piece',$per_piece,'Per Piece Charge');
            print "</div>";
        
        print "</div>\n";    
        make_hidden('id',$id);
        make_submit('submit',$button);
        print "</form>\n";

        print '
        <script>
            $(function() {
                $( "#contractTabs" ).tabs();
            });
            </script>
        '; 
    } elseif($action=='delete') {
        $id=$_GET['id'];
        $sql="DELETE FROM contracts WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        redirect("?action=list");
    } else {
        $sql="SELECT * FROM contracts ORDER BY contract_name";
        $dbContracts=dbselectmulti($sql);
        tableStart("<a href='?action=add'>Add new contract</a>","Contract Name",3);
        if ($dbContracts['numrows']>0)
        {
            foreach($dbContracts['data'] as $contract)
            {
                $name=$contract['contract_name'];
                $id=$contract['id'];
                print "<tr><td>$name</td>";
                print "<td>
                <div class='btn-group'>
                  <a href='?action=edit&id=$id' class='btn btn-dark'>Edit</a>
                  <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                    <span class='caret'></span>
                    <span class='sr-only'>Toggle Dropdown</span>
                  </button>
                  <ul class='dropdown-menu'>
                    <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
                  </ul>
                </div>
                </td>";
                print "</tr>\n";
            }
        }
        tableEnd($dbContracts);
    }
}

function save_contract($action)
{
    $id=intval($_POST['id']);
    $contract_name=addslashes($_POST['contract_name']);
    $station_setup=addslashes($_POST['station_setup']);
    $per_piece=addslashes($_POST['per_piece']);
    if ($action=='insert')
    {
        $sql="INSERT INTO contracts (contract_name, inserter_station_setup, inserter_per_piece) 
        VALUES ('$contract_name', '$station_setup', '$per_piece')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $sql="UPDATE contracts SET contract_name='$contract_name', inserter_station_setup='$station_setup', 
        inserter_per_piece='$per_piece' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the contract.','error');
    } else {
        setUserMessage('Contract successfully saved','success');
    }
    redirect("?action=list");
    
}
$Page->footer();

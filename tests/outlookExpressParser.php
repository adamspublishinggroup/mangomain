<?php 
$dbx = new DBX ( "test.dbx" ); 

$messages = $dbx -> DBXParse ( ); 
if($_GET['item'])
{
    header ( 'Content-type:message/rfc822' ); 
    header ( 'Content-disposition:attachment;filename=message.eml'); 
    echo $messages [ $_GET [ 'item' ] ]; 
} else {
    $i = 0; 
    $n = 1; 
    foreach ( $messages as $message ) { 
        echo "<a href=\"?item=".($i++)."\">Message ".($n++)."</a><br>"; 
    } 
}

Class DBX { 
    const DBX_MAGIC = 4262637007; 

    var $src; 
    var $cnt; 
    var $out = Array ( ); 
    /* 
     * 
     * DBX ( $dbx ) 
     * 
     */ 
    function DBX ( $dbx ) { 
        $this -> cnt = 0; 

        DBX::_dbx_open ( $dbx ); 
        DBX::_dbx_read ( 0, $root, 512 ); 
        if ( DBX::_dbx_int32 ( $root, 0 ) != self::DBX_MAGIC ) { 
            DBX::_dbx_error ( sprintf ( "<strong>%s</strong> not a DBX format!", $dbx ) ); 
        } 
        if ( ( DBX::_dbx_int32 ( $root, 228 ) == 0 ) != ( DBX::_dbx_int32 ( $root, 196 ) == 0 ) ) { 
            DBX::_dbx_warning ( 
                    sprintf ( 
                        "<strong>%s</strong>: Offset %x && Count %x\n", 
                        $dbx, DBX::_dbx_int32 ( $root, 228 ), DBX::_dbx_int32 ( $root, 196 ) 
                    ) 
            ); 
        } 
        if ( DBX::_dbx_int32 ( $root, 228 ) && DBX::_dbx_int32 ( $root, 196 ) ) { 
            /* 
             * 
             * Process DBX src 
             * 
             */ 
             DBX::_dbx_list_header ( DBX::_dbx_int32 ( $root, 228 ), 0, $dbx ); 
        } 
        DBX::_dbx_close ( ); 
    } 
    /* 
     * 
     * _dbx_list_header ( $offset, $parent, $dbx ) 
     * 
     */ 
    function _dbx_list_header ( $offset, $parent, $dbx ) { 
        do { 
            DBX::_dbx_read ( $offset, $head, 24 ); 
            if ( DBX::_dbx_int32 ( $head, 0 ) != $offset ) { 
                DBX::_dbx_warning ( 
                    sprintf ( 
                        "%s: Self (0x%x) != offset (0x%x)", $dbx, 
                        DBX::_dbx_int32 ( $head, 0 ), $offset 
                    ) 
                ); 
            } 
            if ( DBX::_dbx_int32 ( $head, 12 ) != $parent ) { 
                DBX::_dbx_warning ( 
                    sprintf ( 
                        "%s: Back (0x%x) != parent (0x%x)", $dbx, 
                        DBX::_dbx_int32 ( $head, 12 ), $parent 
                    ) 
                ); 
            } 
            if ( DBX::_dbx_int32 ( $head, 4 ) != 0) { 
                DBX::_dbx_warning ( 
                    sprintf ( 
                        "%s: Zero (0x%x) != 0", $dbx, 
                        DBX::_dbx_int32 ( $head, 4 ) 
                    ) 
                ); 
            } 

            $offset += 24; 
            $n = ( DBX::_dbx_int32 ( $head, 16 ) >> 8 ) & 0xff; 

            for ( $i = 0; $i < $n; $i += 1 ) { 
                DBX::_dbx_read ( $offset, $list, 12 ); 
                if ( DBX::_dbx_int32 ( $list, 0  ) ) { 
                    DBX::_dbx_mail_header ( DBX::_dbx_int32 ( $list, 0 ), $dbx ); 
                } 
                if ( DBX::_dbx_int32 ( $list, 4  ) ) { 
                    DBX::_dbx_list_header ( 
                        DBX::_dbx_int32 ( $list, 4 ), 
                        DBX::_dbx_int32 ( $head, 0 ), $dbx 
                    ); 
                } 
                $offset += 12; 
            } 

             $parent = DBX::_dbx_int32 ( $head, 0 ); 
             $offset = DBX::_dbx_int32 ( $head, 8 ); 
        } while ( $offset != 0 ); 
    } 
    /* 
     * 
     * _dbx_mail_header ( $offset, $dbx ) 
     * 
     */ 
    function _dbx_mail_header ( $offset, $dbx ) { 
        $d_offset = 0; $s_offset = 0; $m_offset = 0; $indirect = false; 

        DBX::_dbx_read ( $offset, $mail, 12 ); 
        if ( DBX::_dbx_int32 ( $mail, 0 ) != $offset ) { 
            DBX::_dbx_warning ( 
                sprintf ( 
                    "%s: Self (0x%x) != Offset (0x%x)\n", 
                    $dbx, DBX::_dbx_int32 ( $mail, 0 ), $offset 
                ) 
            ); 
        } 

        $offset += 12; 
        $n = ( DBX::_dbx_int32 ( $mail, 8 ) >> 16 ) & 0xff; 

        if ( $n ) { 
            for ( $i = 0; $i < $n; $i += 1 ) { 
                DBX::_dbx_read ( $offset, $info, 4 ); 
                switch ( ord ( $info { 0 } ) ) { 
                    case 0x0e: 
                        $s_offset = DBX::_dbx_int24 ( $info, 1 ); 
                        break; 
                    case 0x12: 
                        $d_offset = DBX::_dbx_int24 ( $info, 1 ); 
                        break; 
                    case 0x04: 
                        $indirect = true; 
                    case 0x84: 
                        $m_offset = DBX::_dbx_int24 ( $info, 1 ); 
                        break; 
                } 
                $offset += 4; 
            } 
        } 
        if ( $m_offset ) { 
            if ( $indirect ) { 
                DBX::_dbx_read ( $offset + $m_offset, $m_offset, 4 ); 
            } 
            DBX::_dbx_mail_message ( $m_offset ); 
            $this -> cnt++; 
        } 
    } 
    /* 
     * 
     * _dbx_mail_message ( $offset ) 
     * 
     */ 
    function _dbx_mail_message ( $offset ) { 
        $resid = 0; 

        do { 
            DBX::_dbx_read ( $offset, $text, 16 ); 
            $offset += 16; 
            $resid = DBX::_dbx_int32 ( $text, 8 ); 

            do { 
                $n = min ( $resid, 4096 ); 
                DBX::_dbx_read ( $offset, $buf, $n ); 
                $this -> out [ $this -> cnt ] .= $buf; 
                $offset += $n; 
                $resid  -= $n; 
            } while ( $resid > 0 ); 
            $offset = DBX::_dbx_int32 ( $text, 12 ); 
        } while ( $offset != 0 ); 
    } 
    /* 
     * 
     * _dbx_open ( $dbx ) 
     * 
     */ 
    function _dbx_open ( $dbx ) { 
        if ( !$this -> src = fopen ( $dbx, "rb" ) ) { 
            DBX::_dbx_error ( 
                sprintf ( 
                    "Unable to open src <strong>%s</strong>, script execution has stopped!", $dbx 
                ) 
            ); 
        } 
    } 
    /* 
     * 
     * _dbx_read ( $offset, &$buf, $size ) 
     * 
     */ 
    function _dbx_read ( $offset, &$buf, $size ) { 
        if ( fseek ( $this -> src, $offset, SEEK_SET ) != 0 || ! $buf = fread ( $this -> src, $size ) ) { 
            DBX::_dbx_error ( sprintf ( "Ooops, fseek/fread error at line %d", ftell ( $this -> src ) ) ); 
        } 
    } 
    /* 
     * 
     * _dbx_close ( ) 
     * 
     */ 
    function _dbx_close ( ) { 
        if ( $this -> src ) { 
            fclose ( $this -> src ); 
        } 
    } 
    /* 
     * 
     * _dbx_warning ( $warning ) 
     * 
     */ 
    function _dbx_warning ( $warning ) { 
        print $warning . "<br />"; 
    } 
    /* 
     * 
     * _dbx_error ( $error ) 
     * 
     */ 
    function _dbx_error ( $error ) { 
        print $error . "<br />"; 
        exit ( 0 ); 
    } 
    /* 
     * 
     * _dbx_int24 ( $bytes, $offset ) 
     * 
     */ 
    function _dbx_int24 ( $bytes, $offset ) { 
        return ( 
            ord ( $bytes { $offset + 2 } ) * 65536    + 
            ord ( $bytes { $offset + 1 } ) * 256      + 
            ord ( $bytes { $offset } ) ); 
    } 
    /* 
     * 
     * _dbx_int32 ( $bytes, $offset ) 
     * 
     */ 
    function _dbx_int32 ( $bytes, $offset ) { 
        return ( 
            ord ( $bytes { $offset + 3 } ) * 16777216 + 
            ord ( $bytes { $offset + 2 } ) * 65536    + 
            ord ( $bytes { $offset + 1 } ) * 256      + 
            ord ( $bytes { $offset } ) ); 
    } 
    /* 
     * 
     * DBXParse ( ) 
     * 
     */ 
    function DBXParse ( ) { 
        return $this -> out; 
    } 
} 
?> 
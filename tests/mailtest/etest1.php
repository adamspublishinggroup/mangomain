<?php
error_reporting(E_ERROR);


//EXAMPLE: 
$pop3 = new POP3Reader(); 
$pop3->host='mail.newswest.com';
$pop3->pUSERPASS('mango@idahopress.com','mango'); 
if($pop3->error){echo 'ERROR: '.$pop3->error; exit(0); } 

$messages = $pop3->pLIST();
print_r($messages);
 
foreach($messages as $message)
{
    $file=$pop3->pRETR(1); 
    $Parser = new MimeMailParser();
    $Parser->setText($message);
    $to = $Parser->getHeader('to');
    $from = $Parser->getHeader('from');
    $subject = $Parser->getHeader('subject');
    $text = $Parser->getMessageBody('text');
    $html = $Parser->getMessageBody('html');
    $attachments = $Parser->getAttachments();
    print "Attachement:<br />";
    print_r($attachments);
}


$pop3->pQUIT();


class POP3Reader 
{ 
    public $host = '192.168.1.1'; 
    public $port = 110; 
    public $timeout = 10; 
    public $error = null; 
    private $fd = null; 
     
    public function __construct() 
    { 
        $this->fd = fsockopen($this->host,$this->port,&$errno,&$errstr,$this->timeout); 
        stream_set_blocking($this->fd,true); 
        if( $errno ) 
        { 
            echo 'Error: '.$errno.': '.$errstr; 
            exit(1); 
        } 
        $msg = $this->_read(); 
        if( ! $this->_check($msg) ) 
        { 
            $this->error = $msg; 
            $this->_write('QUIT'); 
        } 
    } 
     
    private function _write($cmd) 
    { 
        fwrite($this->fd,$cmd."\r\n"); 
    } 
     
    private function _read($stream=false) 
    { 
        $retval = null; 
        if( ! $stream ) 
        { 
            $retval = fgets($this->fd,1024); 
            //$retval = preg_replace("/\r?\n/","\r\n",$retval); 
        } else { 
            while( ! feof($this->fd) ) 
            { 
                $tmp = fgets($this->fd,1024); 
                //$tmp = preg_replace("/\r?\n/","\r\n",$tmp); 
                if( chop($tmp) == '.') break; 
                $retval .= $tmp; 
            } 
        } 
        return $retval; 
    } 
     
    private function _check($msg) 
    { 
        $stat = substr($msg,0,strpos($msg,' ')); 
        if($stat == '-ERR') return false; 
        //if($stat == '+OK') return true; 
        return true; 
    } 

    //login to server 
    public function pUSERPASS($user, $password) 
    { 
        $this->_write('USER '.$user); 
        $msg = $this->_read(); 
        if( ! $this->_check($msg) ) 
        { 
            $this->error = $msg; 
            $this->_write('QUIT'); 
            return false; 
        } 
        $this->_write('PASS '.$password); 
        $msg = $this->_read(); 
        if( ! $this->_check($msg) ) 
        { 
            $this->error = $msg; 
            $this->_write('QUIT'); 
            return false; 
        } 
        return true; 
    } 
     
    public function pSTAT() 
    { 
        $retval = null; 
        $this->_write('STAT'); 
        $msg = $this->_read(); 
        if( ! $this->_check($msg) ) 
        { 
            $this->error = $msg; 
            $this->_write('QUIT'); 
            return false; 
        } 
        list(,$retval['number'],$retval['size']) = split(' ',$msg); 
        return $retval; 
    } 
     
    //list messages on server 
    public function pLIST() 
    { 
        $this->_write('LIST'); 
        $msg = $this->_read(); 
        if( ! $this->_check($msg) ) 
        { 
            $this->error = $msg; 
            $this->_write('QUIT'); 
            return false; 
        } 
        $msg = split("\n",$this->_read(true)); 
        for($x = 0; $x < sizeof($msg); $x++ ) 
        { 
            $tmp = split(' ',$msg[$x]); 
            $retval[$tmp[0]] = $tmp[1]; 
        } 
        unset($retval['']); 
        return $retval; 
    } 
     
    //retrive a message from server 
    public function pRETR($num) 
    { 
        $this->_write('RETR '.$num); 
        $msg = $this->_read(); 
        if( ! $this->_check($msg) ) 
        { 
            $this->error = $msg; 
            $this->_write('QUIT'); 
            return false; 
        } 
        $msg = $this->_read(true); 
        return $msg; 
    } 
     
    //marks a message deletion from the server 
    //it is not actually deleted until the QUIT command is issued. 
    //If you lose the connection to the mail server before issuing 
    //the QUIT command, the server should not delete any messages 
    public function pDELE($num) 
    { 
        $this->_write('DELE '.$num); 
        $msg = $this->_read(); 
        if( ! $this->_check($msg) ) 
        { 
            $this->error = $msg; 
            $this->_write('QUIT'); 
            return false; 
        } 
    } 
     
    //This resets (unmarks) any messages previously marked for deletion in this session 
    //so that the QUIT command will not delete them 
    public function pRSET() 
    { 
        $this->_write('RSET'); 
        $msg = $this->_read(); 
        if( ! $this->_check($msg) ) 
        { 
            $this->error = $msg; 
            return false; 
        } 
    } 
     
    //This deletes any messages marked for deletion, and then logs you off of the mail server. 
    //This is the last command to use. This does not disconnect you from the ISP, just the mailbox. 
    public function pQUIT() 
    { 
        $this->_write('QUIT'); 
        $msg = $this->_read(); 
        if( ! $this->_check($msg) ) 
        { 
            $this->error = $msg; 
            return false; 
        }         
    } 
     
    public function __destruct() 
    { 
        fclose($this->fd); 
    } 
} 




/*
$save_to = '/tmp'; 

$mbox = new AReader($host, $user, $password); 
 
if (!$mbox->Connect()) { 
    die('Unable to establish connection with mailbox'); 
} 
 
while ($mbox->FetchMail()) { 
    if ($mbox->HasAttachment()) { 
        while($mbox->FetchAttachment()) { 
            $data = $mbox->SaveAttachment($save_to); 
        } 
    } 
     
    //$mbox->DeleteMail(); 
} 
 
$mbox->Close(); 
*/

class AReader { 
        var $host; 
        var $login; 
        var $password; 
        var $mbox; 
        var $struct; 
        var $emails_quan = 0; // inbox (or any other box) emails quantity 
        var $current_email_index = 0; // current email index 
        var $current_attach_index = 2; // current attachment index 
        var $attachment = ''; 
        var $attachment_filename = ''; 
        var $processed_emails; 
        var $attachment_types = array('ATTACHMENT', 'INLINE');         
         
        function AReader($host, $login, $password) { 
            $this->host = $host; 
            $this->login = $login; 
            $this->password = $password;             
        } 
         
        function Connect() { 
            $this->mbox = imap_open($this->host, $this->login, $this->password); 
             
            if (empty($this->mbox))    { 
                return false; 
            } else { 
                $this->emails_quan = imap_num_msg($this->mbox); 
                return true; 
            } 
        } 
         
        function FetchMail() { 
            if (empty($this->mbox) || $this->emails_quan == $this->current_email_index) { 
                return false; 
            } 
                         
            $this->current_email_index++; 
            $this->current_attach_index = 2; 
             
            $this->struct = imap_fetchstructure($this->mbox, $this->current_email_index, FT_UID); 
             
            return true; 
        } 
         
        function HasAttachment() {             
            return property_exists($this->struct, 'parts'); 
        } 
         
        function FetchAttachment() { 
            $this->attachment = ''; 
                         
            if (empty($this->struct) 
                || !property_exists($this->struct, 'parts') 
                || !array_key_exists($this->current_attach_index - 1, $this->struct->parts) 
            ) { 
                return false; 
            }                         
             
            $parts_count = count($this->struct->parts) + 1; 
                         
            while (true) { 
                if ($this->current_attach_index > $parts_count) { 
                    return false; 
                } 
                 
                $part = $this->struct->parts[$this->current_attach_index - 1]; 
                 
                if (!property_exists($part, 'disposition') || !in_array($part->disposition, $this->attachment_types)) { 
                    $this->current_attach_index++; 
                    continue; 
                } 
                 
                if (!empty($part->parameters)) { 
                    $parameters = $part->parameters; 
                    $fattr = 'NAME'; 
                } else { 
                    $parameters = $part->dparameters; 
                    $fattr = 'FILENAME'; 
                } 
                 
                foreach ($parameters as $parameter) { 
                    if ($parameter->attribute == $fattr) { 
                        $filename = $parameter->value; 
                    } 
                } 
                 
                if (empty($filename)) { 
                    $this->current_attach_index++; 
                    continue; 
                } 
                 
                $decoded = imap_mime_header_decode($filename); 
                $filename = ''; 
                foreach ($decoded as $dec) {                     
                    if (!empty($dec->text)) { 
                        $encoding = $dec->charset; 
                        $fpart = $dec->text; 
                        $filename .= $fpart; 
                    } 
                } 
                 
                $this->attachment_filename = $filename;                 
                                 
                $this->attachment = imap_fetchbody($this->mbox, $this->current_email_index, $this->current_attach_index);                
                $this->attachment = base64_decode($this->attachment); 
                 
                $this->current_attach_index++; 
                 
                if (empty($this->attachment)) {                     
                    return false; 
                } 
                 
                return true; 
            } 
             
        } 
         
        function SaveAttachment($save_path, $new_filename = '') { 
            if (!empty($this->attachment)) { 
                $save_to = $save_path.'/'.(!empty($new_filename) ? $new_filename : $this->attachment_filename); 
                $bytes_quan = file_put_contents($save_to, $this->attachment); 
                if (!empty($bytes_quan)) { 
                    return $save_to; 
                } 
            } 
             
            return ''; 
        } 
         
        function GetAttachmentFilename() { 
            return $this->attachment_filename; 
        } 
         
        function DeleteMail() { 
            if ($this->current_email_index > 0) { 
                imap_delete($this->mbox, $this->current_email_index); 
            } 
        } 
         
        function Close() { 
            if ($this->mbox) { 
                imap_close($this->mbox, CL_EXPUNGE); 
            } 
        }         
    } 
  

?>

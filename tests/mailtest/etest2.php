<?php

    require('mime_parser.php');
    require('rfc822_addresses.php');
    require("pop3.php");

  /* Uncomment when using SASL authentication mechanisms */
    /*
    require("sasl.php");
    */

    stream_wrapper_register('pop3', 'pop3_stream');  /* Register the pop3 stream handler class */

    $pop3=new pop3_class;
    $pop3->hostname="mail.newswest.com";             /* POP 3 server host name                      */
    $pop3->port=110;                         /* POP 3 server host port,
                                                usually 110 but some servers use other ports
                                                Gmail uses 995                              */
    $pop3->tls=0;                            /* Establish secure connections using TLS      */
    $user="mango@idahopress.com";                        /* Authentication user name                    */
    $password="mango";                    /* Authentication password                     */
    $pop3->realm="";                         /* Authentication realm or domain              */
    $pop3->workstation="";                   /* Workstation for NTLM authentication         */
    $apop=0;                                 /* Use APOP authentication                     */
    $pop3->authentication_mechanism="USER";  /* SASL authentication mechanism               */
    $pop3->debug=0;                          /* Output debug information                    */
    $pop3->html_debug=1;                     /* Debug information is in HTML                */
    $pop3->join_continuation_header_lines=1; /* Concatenate headers split in multiple lines */

    if(($error=$pop3->Open())=="")
    {
        echo "<PRE>Connected to the POP3 server &quot;".$pop3->hostname."&quot;.</PRE>\n";
        if(($error=$pop3->Login($user,$password,$apop))=="")
        {
            echo "<PRE>User &quot;$user&quot; logged in.</PRE>\n";
            if(($error=$pop3->Statistics($messages,$size))=="")
            {
                echo "<PRE>There are $messages messages in the mail box with a total of $size bytes.</PRE>\n";
                if($messages>0)
                {
                    $pop3->GetConnectionName($connection_name);
                    for($i=1;$i<=$messages;$i++)
                    {
                        $message_file='pop3://'.$connection_name.'/'.$i;
                        $mime=new mime_parser_class;
                        
                        
                        $saveDirectory='msg/';
                        if(!file_exists($saveDirectory.$i)){
                            if(!mkdir($saveDirectory.$i,0777))
                            {
                                print "Unabled to create new directory.<br />";
                                break;
                            }
                        }
                        $saveDirectory.=$i."/";
                        /*
                        * Set to 0 for not decoding the message bodies
                        */
                        $mime->decode_bodies = 1;
                        $mime->extract_addresses=1;
                        $mime->use_part_file_names=1;
                        $parameters=array(
                            'File'=>$message_file,
                            
                            /* Read a message from a string instead of a file */
                            /* 'Data'=>'My message data string',              */

                            /* Save the message body parts to a directory     */
                            'SaveBody'=>$saveDirectory,                            

                            /* Do not retrieve or save message body parts     */
                            'SkipBody'=>0,
                        );
                        $success=$mime->Decode($parameters, $decoded);


                        if(!$success)
                            echo '<h2>MIME message decoding error: '.HtmlSpecialChars($mime->error)."</h2>\n";
                        else
                        {
                            /*
                            echo '<h2>MIME message decoding successful</h2>'."\n";
                            echo '<h2>Message structure</h2>'."\n";
                            echo '<pre>';
                            var_dump($decoded[0]);
                            echo '</pre>';
                            */
                            if($mime->Analyze($decoded[0], $results))
                            {
                                /*
                                echo '<h2>Message analysis</h2>'."\n";
                                echo '<pre>';
                                var_dump($results);
                                echo '</pre>';
                                */
                                $subject=$results["Subject"];
                                $sent=$results["Date"];
                                $from=$results["From"][0];
                                $fromAddress=$from['address'];
                                $fromName=$from['name'];
                                $messageFile=$results["DataFile"];
                                $messageText=file_get_contents($messageFile);
                                if(isset($results['Attachments']) && count($results['Attachments'])>0)
                                {
                                    $attach="Found these attachments:<br />";
                                    foreach($results['Attachments'] as $at)
                                    {
                                        $attach.="Found ".$at['FileName']."<br />";
                                    }
                                }
                                print "Message sent: $sent<br />";
                                print "Got a subject of: $subject<br />";
                                print "Got a sender of $fromName at $fromAddress<br />";
                                print $attach;
                                print "And the message itself was:<br />$messageText<br /><br /><hr>";
                                
                            }
                            else
                                echo 'MIME message analyse error: '.$mime->error."\n";
                        }
                        
                        
                        if(($error=$pop3->DeleteMessage($i))=="")
                        {
                            echo "<PRE>Marked message $i for deletion.</PRE>\n";
                        }
                        
                    }
                }
                
                //if(($error=$pop3->ResetDeletedMessages())=="")
                //{
                //   echo "<PRE>Resetted the list of messages to be deleted.</PRE>\n";
                //}
                if($error==""
                && ($error=$pop3->Close())=="")
                    echo "<PRE>Disconnected from the POP3 server &quot;".$pop3->hostname."&quot;.</PRE>\n";
            }
        }
    }
    if($error!="")
        echo "<H2>Error: ",HtmlSpecialChars($error),"</H2>";
?>
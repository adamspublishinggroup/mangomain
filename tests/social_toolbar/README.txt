This package is released under the GPL. You may alter and use the code in any way that you wish, but remember to include a mention of the
author if your site or company have a credits section available. jQuery Plugins used for Tooltips and Modal Window capabilities are copyright
of their respective authors and you may need to look at the sites related to them for further copyright information.

Thanks and remember to subscribe to my RSS feed!

Addy Osmani, AddyOsmani.com.
Feb 2010.
<HTML>
<HEAD>
<TITLE>Test for Manuel Lemos's PHP POP3 class</TITLE>
</HEAD>
<BODY>
<?php

    require("../includes/mail/functions_pop3mail.php");

  /* Uncomment when using SASL authentication mechanisms */
    /*
    require("sasl.php");
    */

    $pop3=new pop3_class;
    $pop3->hostname="mail.newswest.com";             /* POP 3 server host name                      */
    $pop3->port=110;                         /* POP 3 server host port,
                                                usually 110 but some servers use other ports
                                                Gmail uses 995                              */
    $pop3->tls=0;                            /* Establish secure connections using TLS      */
    $user="mango@idahopress.com";                        /* Authentication user name                    */
    $password="mango";                    /* Authentication password                     */
    $pop3->realm="";                         /* Authentication realm or domain              */
    $pop3->workstation="";                   /* Workstation for NTLM authentication         */
    $apop=0;                                 /* Use APOP authentication                     */
    $pop3->authentication_mechanism="USER";  /* SASL authentication mechanism               */
    $pop3->debug=0;                          /* Output debug information                    */
    $pop3->html_debug=1;                     /* Debug information is in HTML                */
    $pop3->join_continuation_header_lines=1; /* Concatenate headers split in multiple lines */

    if(($error=$pop3->Open())=="")
    {
        echo "<PRE>Connected to the POP3 server &quot;".$pop3->hostname."&quot;.</PRE>\n";
        if(($error=$pop3->Login($user,$password,$apop))=="")
        {
            echo "<PRE>User &quot;$user&quot; logged in.</PRE>\n";
            if(($error=$pop3->Statistics($messages,$size))=="")
            {
                echo "<PRE>There are $messages messages in the mail box with a total of $size bytes.</PRE>\n";
                $result=$pop3->ListMessages("",0);
                if(GetType($result)=="array")
                {
                    for(Reset($result),$message=0;$message<count($result);Next($result),$message++)
                        echo "<PRE>Message ",Key($result)," - ",$result[Key($result)]," bytes.</PRE>\n";
                    $result=$pop3->ListMessages("",1);
                    if(GetType($result)=="array")
                    {
                        for(Reset($result),$message=0;$message<count($result);Next($result),$message++)
                            echo "<PRE>Message ",Key($result),", Unique ID - \"",$result[Key($result)],"\"</PRE>\n";
                        if($messages>0)
                        {
                            if(($error=$pop3->RetrieveMessage(1,$headers,$body,2))=="")
                            {
                                echo "<PRE>Message 1:\n---Message headers starts below---</PRE>\n";
                                for($line=0;$line<count($headers);$line++)
                                {
                                    if (strpos($headers[$line],"ubject:")>0)
                                    {
                                        $subject=str_replace("Subject: ","",$headers[$line]);
                                        print "Found a subject of $subject <br />";    
                                    }
                                    if (strpos($headers[$line],"eply-To:")>0)
                                    {
                                        $replyTo=str_replace("Reply-to: ","",$headers[$line]);
                                        print "Found a reply to  of $replyTo <br />";    
                                    }
                                }
                                //echo "<PRE>",HtmlSpecialChars($headers[$line]),"</PRE>\n";
                                //echo "<PRE>---Message headers ends above---\n---Message body starts below---</PRE>\n";
                                for($line=0;$line<count($body);$line++)
                                    echo "<PRE>",HtmlSpecialChars($body[$line]),"</PRE>\n";
                                echo "<PRE>---Message body ends above---</PRE>\n";
                                /*
                                if(($error=$pop3->DeleteMessage(1))=="")
                                {
                                    echo "<PRE>Marked message 1 for deletion.</PRE>\n";
                                    if(($error=$pop3->ResetDeletedMessages())=="")
                                    {
                                        echo "<PRE>Resetted the list of messages to be deleted.</PRE>\n";
                                    }
                                    
                                }
                                */
                            }
                        }
                        if($error==""
                        && ($error=$pop3->Close())=="")
                            echo "<PRE>Disconnected from the POP3 server &quot;".$pop3->hostname."&quot;.</PRE>\n";
                        
                    }
                    else
                        $error=$result;
                }
                else
                    $error=$result;
            }
        }
    }
    if($error!="")
        echo "<H2>Error: ",HtmlSpecialChars($error),"</H2>";
        
//mailparse_msg_parse_file ("test.txt");

$save_dir = '/tests/mailtest/';
foreach($attachments as $attachment) {
  // get the attachment name
  $filename = $attachment->filename;
  // write the file to the directory you want to save it in
  if ($fp = fopen($save_dir.$filename, 'w')) {
    while($bytes = $attachment->read()) {
      fwrite($fp, $bytes);
    }
    fclose($fp);
  }
}

/*
$host="{imap.gmail.com:993/imap/ssl}INBOX"; // pop3host 
$login="iptwebtech@gmail.com"; //pop3 login 
$password="iptwebtech"; //pop3 password 
$savedirpath="" ; // attachement will save in same directory where scripts run othrwise give abs path 
$jk=new ReadAttachment(); // Creating instance of class#### 
$jk->getdata($host,$login,$password,$savedirpath); // calling member function 

        # Coded By Jijo Last Update Date [Jan/19/06] (http://www.phpclasses.org/browse/package/2964.html)
# Updated 2008-12-18 by Dustin Davis (http://nerdydork.com)
    # Utilized $savedirpath parameter
    # Added delete_emails parameter
*/    
class ReadAttachment
{
    function getdecodevalue($message,$coding) {
        switch($coding) {
            case 0:
            case 1:
                $message = imap_8bit($message);
                break;
            case 2:
                $message = imap_binary($message);
                break;
            case 3:
            case 5:
                $message=imap_base64($message);
                break;
            case 4:
                $message = imap_qprint($message);
                break;
        }
        return $message;
    }


    function getdata($host,$login,$password,$savedirpath,$delete_emails=false) {
        // make sure save path has trailing slash (/)
        $savedirpath = str_replace('\\', '/', $savedirpath);
        if (substr($savedirpath, strlen($savedirpath) - 1) != '/') {
            $savedirpath .= '/';
        }
        
        $mbox = imap_open ($host, $login, $password) or die("can't connect: " . imap_last_error());
        print "Connected<br />";
        $message = array();
        $message["attachment"]["type"][0] = "text";
        $message["attachment"]["type"][1] = "multipart";
        $message["attachment"]["type"][2] = "message";
        $message["attachment"]["type"][3] = "application";
        $message["attachment"]["type"][4] = "audio";
        $message["attachment"]["type"][5] = "image";
        $message["attachment"]["type"][6] = "video";
        $message["attachment"]["type"][7] = "other";
        
        print "There are ".imap_num_msg($mbox)." messages to process<br />";
        for ($jk = 1; $jk <= imap_num_msg($mbox); $jk++) {
            print "Processing message $jk<br />";
            $structure = imap_fetchstructure($mbox, $jk , FT_UID);    
            $parts = $structure->parts;
            $fpos=2;
                  
            for($i = 1; $i < count($parts); $i++) {
                $message["pid"][$i] = ($i);
                $part = $parts[$i];
                if($part->disposition == "ATTACHMENT") {
                    $message["type"][$i] = $message["attachment"]["type"][$part->type] . "/" . strtolower($part->subtype);
                    $message["subtype"][$i] = strtolower($part->subtype);
                    $ext=$part->subtype;
                    $params = $part->dparameters;
                    $filename=$part->dparameters[0]->value;
                    print "Saving image with $filename<br />";
                    $mege="";
                    $data="";
                    $mege = imap_fetchbody($mbox,$jk,$fpos);  
                    $filename="$filename";
                    $fp=fopen($savedirpath.$filename,w);
                    $data=$this->getdecodevalue($mege,$part->type);    
                    fputs($fp,$data);
                    fclose($fp);
                    $fpos+=1;
                }
            }
            if ($delete_emails) {
                // imap_delete tags a message for deletion
                imap_delete($mbox,$jk);
            }
        }
        // imap_expunge deletes all tagged messages
        if ($delete_emails) {
            imap_expunge($mbox);
        }
        imap_close($mbox);
    }
}        
        
        ?>

</BODY>
</HTML>

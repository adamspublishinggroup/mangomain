<?php
  session_start();
  //1 ship of 5 cell length, 1 - 4 cells, 2 - 3 cells and 1 - 2 cells.
?>
<html>
<head>
<script src="../includes/jscripts/jquery-1.4.3.min.js"></script>
<style type='text/css'>
.red {
    background-color:red;
}
.blue {
    background-color:blue;
}
.ship {
    background-color: #666;
}
.miss {
    background-color: #efefef;
}
</style>
</head>
<body>
<div style='width:500px;height:500px;'>
<?php
for($i=1;$i<=10;$i++)
{
    for($j=1;$j<=10;$j++)
    {
        print "<div id='$i_$j' class='blue' style='float:left;width:48px;height:48px;border:1px solid white;'>&nbsp;</div>\n";
    }
    print "<div style='clear:both;height:0px;'></div>\n";
}

?>
</div>


<?php
  /* this a test of a new control that leverages the eodanas bootstrap datetime picker */
  error_reporting(E_ALL);
  require('../includes/mainmenu.php');
?>
<div class='form-horizontal'>
<div class="form-group has-success">
  <label class="control-label" for="inputSuccess1">Input with success</label>
  <input type="text" class="form-control" id="inputSuccess1" aria-describedby="helpBlock2">
  <span id="helpBlock2" class="help-block">A block of help text that breaks onto a new line and may extend beyond one line.</span>
</div>
</div>


<?php
footer();

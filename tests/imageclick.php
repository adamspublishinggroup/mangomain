<html>
<head>
<style type='text/css'>
.annotation {
    background-image: url(../artwork/icons/blue-pushpin.png);
    width:32px;
    height:32px;
    position:absolute;
    top:0;
    left:0;
    background-repeat: no-repeat;
}
.note {
    background-color:yellow;
    width:200px;
    height:200px;
    padding:4px;
    position:absolute;
    top:0;
    left:0;
    z-index:1000;
}
</style>
<script type='text/javascript' src="../includes/jscripts/jquery-1.6.1.min.js"></script>
<script type='text/javascript'>
var pinx;
var piny;

$(document).ready(function(){
   $("#adholder").click(function(e){
      pinx=e.pageX-64;
      piny=e.pageY-64;
      if( $(e.target).is('.note') )
      {
          
      } else if($(e.target).is('.annotation'))
      {
          var current_id=$(e.target).attr("id");
          var pinid=current_id.split("_");
          noteClick(pinid[1]);
      } else if($(e.target).is('#adholder')) {
          addPin();
      }
      e.stopPropagation();
                
   });
   
})

function noteClick(noteid)
{  
    var adid=$('#adid').val();
    var note='';
    $.ajax({
       type: "POST",
       url: "adnote_handler.php",
       data: "action=load&adid="+adid+"&noteid="+noteid,
       success: function(msg){
         var parts=msg.split("|");
           if(parts[0]=='success')
           {
                note="<div id='note_"+noteid+"' class='note' style='top:"+piny+";left:"+pinx+";'>&nbsp;Note#"+noteid+"<br><textarea id='notetext_"+noteid+"' cols=20 rows=7></textarea><br><input type='button' value='Remove' onclick='removeNote("+noteid+")'><input type='button' value='Save' onclick='saveNote("+noteid+")'></div>";
                $('#adholder').append(note);
                //alert('trying to store '+parts[1]+' in the text field');
                $('#notetext_'+noteid).val(parts[1]);
           } else {
               alert('error!');
           }
       }
     });
    
   
    
}

function removeNote(noteid)
{
    var adid=$('#adid').val();
    //event.stopPropagation();
     $.ajax({
       type: "POST",
       url: "adnote_handler.php",
       data: "action=delete&adid="+adid+"&noteid="+noteid,
       success: function(msg){
         var parts=msg.split("|");
           if(parts[0]=='success')
           {
               $('#note_'+noteid).hide();
               $('#pin_'+noteid).hide();
               $('#note_'+noteid).remove();
               $('#pin_'+noteid).remove();
           } else {
               alert('error!');
           }
       }
     });
}

function saveNote(noteid)
{
    var adid=$('#adid').val();
    var note=$('#notetext_'+noteid).val();
    $.ajax({
       type: "POST",
       url: "adnote_handler.php",
       data: "action=save&adid="+adid+"&noteid="+noteid+"&note="+note,
       success: function(msg){
           var parts=msg.split("|");
           if(parts[0]=='success')
           {
               //alert('trying to remove note with id of '+noteid); 
               $('#note_'+noteid).hide();
               $('#note_'+noteid).remove();
           } else {
               alert('error!');
           }
       }
     });     
}


function addPin()
{
    var newImage='';
    var adid=$('#adid').val();
    $.ajax({
       type: "POST",
       url: "adnote_handler.php",
       data: "action=new&adid="+adid+"&pinx="+pinx+"&piny="+piny,
       success: function(msg){
           var parts=msg.split("|");
           if(parts[0]=='success')
           {
               var newid=parts[1];                                               
               newImage="<div id='pin_"+newid+"' class='annotation' style='top:"+piny+";left:"+pinx+";'>&nbsp;</div>";
               $('#adholder').append(newImage);
               //alert(newImage);
           } else {
               alert('error!');
           }
     
       }
     }); 
    
}

function refreshNotes()
{
    
}
</script>
</head>
<body>
<html>
<body>
<input type='text' id='adid' value='1'><br><br>

<div id='adholder' style='position:absolute;top:50;left:50;width:960px;height:500px;background: url(adtest.jpg) no-repeat center;'>

<?php
//add existing pins
include("../includes/functions_db.php");
$sql="SELECT * FROM ad_notes WHERE ad_id=1";
$dbPins=dbselectmulti($sql);
if($dbPins['numrows']>0)
{
    foreach($dbPins['data'] as $pin)
    {
        print "<div id='pin_$pin[id]' class='annotation' style='top:$pin[piny];left:$pin[pinx];'>&nbsp;</div>";    
    }
}
?>
</div>

<div id='notes'>

</div>
</body>
</html>

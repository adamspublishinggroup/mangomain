<?php
include '../includes/classMailroomLabel.php';

$label=new MailroomLabel(10,10,10,10,300);
$label->debug=true;
$label->addFont("../includes/fonts/Actor-Regular.ttf");
$label->canvasWidth=432;    
$label->addBorder(2);
$label->backgroundColor='ffffff';
$label->addBarcode(123456);
$label->addText('Received By: Joe',14);
$label->addText('Receive Date/Time: 
    Tuesday Oct 15, 2015 at 10:30a.m.',14);
$label->addText('Tracking ID: IPT1354',14);
$label->outputFormat='png';
//$label->saveTo('');
$label->buildImage('testImage');

/*
$handle = printer_open('Brother QL-1060N');
printer_start_doc($handle, "Insert Label");
printer_start_page($handle);
printer_draw_bmp($handle, 'labelImage.bmp', 20, 20);
printer_end_page($handle);
printer_end_doc($handle);
printer_close($handle);
*/  
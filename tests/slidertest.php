<!DOCTTYPE html>
<html>
<head>
<link href="../styles/bootstrap.min.css" rel='stylesheet' type='text/css' />
<link href="../styles/slider.css" rel='stylesheet' type='text/css' />
<script type="text/javascript" src="../includes/jscripts/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="../includes/jscripts/bootstrap.min.js"></script>
<script type="text/javascript" src="../includes/jscripts/bootstrap-slider.js"></script>
<style type="text/css">
body {
    font-family:Trebuchet MS, Arial, sans-serif;
}
.question {
    float:left;
    width: 250px;
    font-weight:bold;
    margin-right:10px;
    padding-right:10px;
    border-right: thin solid #666;
    margin-top: 5px; 
}
.ranking {
    float:left;
    margin-top: 5px;
}
.clear {
    clear:both;
}
</style>
</head>
<body>
<div style='width:500px;padding-left:20px;'>
<?php
    $questions=array(
        array("id"=>"q1","question"=>"I am naturally organized. I create order and structure in my life","color"=>"gold"),
        array("id"=>"q2","question"=>"I need to do fun and exciting things in my life.","color"=>"orange"),
        array("id"=>"q3","question"=>"I value gaining knowledge.  I want to spend my time learning more about what interests me.","color"=>"green"),
        array("id"=>"q4","question"=>"I like it when everyone around me gets along and does not argue or fight because I need and value harmony.","color"=>"blue"),
        array("id"=>"q5","question"=>"I need others to show that they really care about me.","color"=>"blue"),
        array("id"=>"q6","question"=>"I am curious. I like to ask questions and want to know why.","color"=>"green"),
        array("id"=>"q7","question"=>"I need change and excitement in my life, or I become bored.","color"=>"orange"),
        array("id"=>"q8","question"=>"I like to plan ahead. I need to be prepared ahead of time.","color"=>"gold"),
        array("id"=>"q9","question"=>"I like to think deeply about subjects that interest me.","color"=>"green"),
        array("id"=>"q10","question"=>"I am naturally impulsive. I often do things without thinking first.","color"=>"orange"),
        array("id"=>"q11","question"=>"I naturally care about people and need them to care about me.","color"=>"blue"),
        array("id"=>"q12","question"=>"I naturally do what is right and like to follow the rules.","color"=>"gold"),
        array("id"=>"q13","question"=>"I like to take risks and that sometimes gets me in trouble.","color"=>"orange"),
        array("id"=>"q14","question"=>"I need to be treated so I feel special and important to others.","color"=>"blue"),
        array("id"=>"q15","question"=>"I live a cautius life. I need to be safe and avoid danger.","color"=>"gold"),
        array("id"=>"q16","question"=>"I like to tell others what I know and have learned in my life.","color"=>"green"),
        array("id"=>"q17","question"=>"I like to make lists and follow them to accomplish many things.","color"=>"orange"),
        array("id"=>"q18","question"=>"I really want to be friendly and get along with everyone I know.","color"=>"blue"),
        array("id"=>"q19","question"=>"I hate to make mistakes.  I need to have the right answer.","color"=>"green"),
        array("id"=>"q20","question"=>"I go all out to win. I like activities that are physical and competitive.","color"=>"orange"),
        array("id"=>"q21","question"=>"I learn and work best when by myself rather than in a group.","color"=>"green"),
        array("id"=>"q22","question"=>"I need freedom and flexibility.  I hate following other people�s rules.","color"=>"orange"),
        array("id"=>"q23","question"=>"I like to belong to clubs and groups that do good deeds.","color"=>"gold"),
        array("id"=>"q24","question"=>"I am naturally sensitive.  I get my feelings hurt easily.","color"=>"blue"),
        array("id"=>"q25","question"=>"I need to learn and do things by following a schedule and routine.","color"=>"gold"),
        array("id"=>"q26","question"=>"I want to make the world a better place for others because I care.","color"=>"blue"),
        array("id"=>"q27","question"=>"I am naturally playful and have a good sense of humor.","color"=>"orange"),
        array("id"=>"q28","question"=>"I like to spend time alone so I can think and analyze my thoughts.","color"=>"green"),
        array("id"=>"q29","question"=>"I love it when people work together and cooperate.","color"=>"blue"),
        array("id"=>"q30","question"=>"I enjoy entertaining others and making people laugh.","color"=>"orange"),
        array("id"=>"q31","question"=>"I need to be responsible and have others see me as responsible.","color"=>"gold"),
        array("id"=>"q32","question"=>"I enjoy thinking and using my mind to solve problems.","color"=>"green"),
        array("id"=>"q33","question"=>"I like to spend time helping others get along and feel happy.","color"=>"blue"),
        array("id"=>"q34","question"=>"I am a natural problem-solver and enjoy analyzing problems.","color"=>"green"),
        array("id"=>"q35","question"=>"I desire excitement and adventure. I crave feeling a 'rush'.","color"=>"orange"),
        array("id"=>"q36","question"=>"I need to do what is right. I have a strong sense of right and wrong.","color"=>"gold"),
        array("id"=>"q37","question"=>"I have difficulty sitting still and being attentive for long periods of time.","color"=>"orange"),
        array("id"=>"q38","question"=>"I am affectionate and nurturing in my relationships.","color"=>"blue"),
        array("id"=>"q39","question"=>"I really value intelligence.  Others see me as intelligent.","color"=>"green"),
        array("id"=>"q40","question"=>"I complete my work before I relax or play.  Work comes before play.","color"=>"gold")
        );
      foreach($questions as $question)
      {
          ?>
          <div class='question'><?php echo $question['question'] ?></div> <div class='ranking'><input id='<?php echo $question['id'];?>' type="text" class="span2 <?php echo $question['color'];?> slider" value="3" data-slider-min="0" data-slider-max="6" data-slider-step="1" data-slider-value="3" data-slider-orientation="horizontal" data-slider-selection="after" data-slider-tooltip="hide"><br /><span id='<?php echo $question['id'];?>_val'>Neutral</span></div><div class='clear'></div>

          <?php
              
      }  
?>
</div>
<div style='top:40px;right:200px;margin-left:40px;position:fixed;padding:10px;border:1px solid black;background-color:#ccc;'>
Green: <input type='text' value='30' id='green' /> <br />
Gold: <input type='text' value='30' id='gold' /> <br />
Blue: <input type='text' value='30' id='blue' /> <br />
Orange: <input type='text' value='30' id='orange' /> <br />
</div>
<div class='clear'></div>
<script type="text/javascript">
$(document).ready(function(){
    $('.slider').slider().on('slideStop', function(ev){
        var slideVal=$(this).val();
        var message='Neutral';
        if(slideVal==0)
        {
            message='Not me at all!';
        } else if(slideVal==1)
        {
            message='Rarely if ever'; 
        } else if(slideVal==2)
        {
            message='I generally disagree';
        } else if(slideVal==3)
        {
            message='Neutral';
        } else if(slideVal==4)
        {
            message='I would agree with this';
        } else if(slideVal==5)
        {
            message='Pretty much always';
        } else {
            message='Describes me perfectly!';
        }
        $('#'+$(this).attr('id')+"_val").html(message);
        var callingclass=$(this).attr('class')
        var holder='';
        var counter=0;
        if($(this).hasClass('green')){holder='green';}
        if($(this).hasClass('gold')){holder='gold';}
        if($(this).hasClass('orange')){holder='orange';}
        if($(this).hasClass('blue')){holder='blue';}
        $('.slider').each(function(){
            if($(this).attr('class')==callingclass){
                
                counter=counter+parseInt($(this).val());   
            } 
        })
        $('#'+holder).val(counter);
    })
})
</script>

</body>
</html>
<?php
  
?>

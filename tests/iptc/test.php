<?php
  /*
  Test of writing Exit / IPTC data to a jpeg
  Uses library: https://github.com/agutoli/Image_Iptc
  */
  error_reporting(0);
require 'Iptc.php';
$dataFile = "legislators.csv";
$dataContents = file_get_contents($dataFile);
$dataContents = explode("\n",$dataContents);
array_shift($dataContents);

foreach($dataContents as $record)
{
    if(trim($record)!='')
    {
        $record = str_replace('"','',$record);
        $recordElements = explode(",",$record);
        $file = end(explode("/",$recordElements[4]));
        $name = explode(" ",strtolower($recordElements[1]));
        foreach($name as &$namePart)
        {
            $namePart = ucfirst($namePart);
        }
        $name = implode(" ",$name);
        $name = str_replace("(r)","(R)",$name);
        $name = str_replace("(d)","(D)",$name);
        
        
        //rename file
        $newFile = str_replace("(","",$name);
        $newFile = str_replace(")","",$newFile);
        $newFile = str_replace(" ","_",$newFile);
        $newFile = "fixedFiles/".$newFile.".jpg";
        
        print "File: $file  -> Name: $name -> new File: $newFile<br>";
        
        if(copy("sourceFiles/".$file,$newFile ))
        {
            echo "  -- File copied<br>";
        
            //process file
            //attempt to make file readable
            $iptc = new Iptc($newFile);
            $iptc->set(Iptc::HEADLINE, $name);
            $iptc->write();
            $iptc->set(Iptc::CAPTION, $name);
            $iptc->write();
            $iptc->set(Iptc::OBJECT_NAME, $name);
            $iptc->write();
            
        } else {
            print "  -- Unable to copy file - did not write data<br>";
        }
    }
}

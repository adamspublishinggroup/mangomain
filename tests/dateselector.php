<?php

//sunday is 0
echo get_specifiedday(3,'last',9,2009);


function get_specifiedday($day,$pos,$month,$year)
{
    $count=0;
    $maxdays=date("t",mktime(0,0,0,$month,1,$year)); //gives us number of days in given month
    if ($pos=='last')
    {
        for ($i=$maxdays;$i>=1;$i--)
        {
            $num = date("w",mktime(0,0,0,$month,$i,$year));
            if ($num==$day)
            {
                $date=date("Y-m-d H:i:s",mktime(0,0,0,$month,$i,$year));
                break;
            }
        }
    } else {
        for ($i=1;$i<=$maxdays;$i++)
        {
            $num = date("w",mktime(0,0,0,$month,$i,$year));
            if ($num==$day)
            {
                $count++;
                if ($count==$pos)
                {
                    $date=date("Y-m-d H:i:s",mktime(0,0,0,$month,$i,$year));
                }
            }
        
        }
    }
    if (strtotime($date)<time())
    {
        if ($month<12)
        {
            $month++;
        } else {
            $month=1;
            $year++;
        }
        return get_specifiedday($day,$pos,$month,$year);
    } else {
        return $date; 
    }     
}
?>

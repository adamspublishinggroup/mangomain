<html>
<head>
<title>PHPMailer - SMTP (Gmail) basic test</title>
</head>
<body>

<?php

//error_reporting(E_ALL);
error_reporting(E_STRICT);

date_default_timezone_set('America/Boise');

require_once('../class.phpmailer.php');
//include("class.smtp.php"); // optional, gets called from within class.phpmailer.php if not already loaded

$mail             = new PHPMailer();

$body             = 'This is a test bounce through gmail';

/*
$mail->IsSMTP(); // telling the class to use SMTP
$mail->Host       = "mail.idahopress.com"; // SMTP server
$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
                                           // 1 = errors and messages
                                           // 2 = messages only
$mail->SMTPAuth   = true;                  // enable SMTP authentication
$mail->SMTPSecure = "ssl";                 // sets the prefix to the servier
$mail->Host       = "smtp.gmail.com";      // sets GMAIL as the SMTP server
$mail->Port       = 465;                   // set the SMTP port for the GMAIL server
$mail->Username   = "iptwebtech@gmail.com";  // GMAIL username
$mail->Password   = "iptwebtech";            // GMAIL password
*/
$mail->Mailer = 'smtp';
$mail->Port = 465;
$mail->IsSMTP(); // telling the class to use SMTP
$mail->SMTPSecure = 'ssl';
$mail->Host = 'ssl://smtp.gmail.com'; // SMTP server
$mail->SMTPAuth = true; // turn on SMTP authentication
$mail->SMTPKeepAlive = true;
$mail->Username = "iptwebtech@gmail.com"; // SMTP username
$mail->Password = "iptwebtech"; // SMTP password

$mail->SetFrom('jhansen@idahopress.com', 'Joe Hansen');

$mail->AddReplyTo('jhansen@idahopress.com', 'Joe Hansen');

$mail->Subject    = "subject";

//$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

$mail->MsgHTML($body);

$address = "2088807978@vtext.com";
$mail->AddAddress($address, "John Doe");

//$mail->AddAttachment("images/phpmailer.gif");      // attachment
//$mail->AddAttachment("images/phpmailer_mini.gif"); // attachment

if(!$mail->Send()) {
  echo "Mailer Error: " . $mail->ErrorInfo;
} else {
  echo "Message sent!";
}

?>

</body>
</html>

<?php
//core calendar file
$pageFluid=true;
include("includes/boot.php") ;
if($_GET['start'])
{
    $defaultDate = addslashes($_GET['start']);
} else {
    $defaultDate = date("Y-m-d");
}
?>
       
<script>
getUnscheduled(<?= date("Y") ?>,<?= date("n") ?>,<?= date("d")?>);
$(document).ready(function() {
    
    if(calendarSchedulePress)
    {
        var enableDrag=true;
    } else {
        var enableDrag=false;
    }
    var lastClickTime = 0;
    var lastClickID = 0;
    
    var calView = 'agendaWeek';
    
    if($( window ).width()<800)
    {
        calView = 'agendaDay';   
    }
    
    var currentJobID=0;
    $('#calendar').fullCalendar({
        themeSystem: 'bootstrap3',
        defaultDate: $.fullCalendar.moment('<?php echo $defaultDate ?>'),
        defaultView: calView,
        allDaySlot: false,
        firstHour: calendarStartPress,
        slotMinutes: calendarPressSlots,
        header: {
            left: '',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        slotEventOverlap:false,
        overlap: false,
        editable: true,
        height: 700,
        eventStartEditable: enableDrag,
        eventDurationEditable: enableDrag,
        events: [ 
            <?php
            fetchEvents();          
            ?>
        ],
        eventDrop: function(event, delta, revertFunc, jsEvent, ui, view) {
            if(calendarSchedulePress && event.canEdit==1)
            { 
                if(event.eventtype=='maintenance')
                {
                    $.ajax({
                       url: 'includes/ajax_handlers/maintenanceScheduledTicketHandler.php',
                       type: "POST",
                       data: {type:'move',scheduleid:event.id,newStart: event.start.format()},
                       dataType: 'json',
                       error: function()
                       {
                           revertFunc();
                       },
                       success: function(response) {
                           if(response.status=='success')
                           {
                              //don't do anything to annoy the user with a dialog or somethign :) 
                           } else {
                              alertMessage("Update Failed",response.message,'error');
                              revertFunc(); 
                           }
                       }
                    });    
                } else {
                    $.ajax({
                       url: 'includes/ajax_handlers/updateCalendarPress.php',
                       type: "POST",
                       data: {type:'move',jobid:event.id,newStart: event.start.format()},
                       dataType: 'json',
                       error: function()
                       {
                           revertFunc();
                       },
                       success: function(response) {
                           if(response.status=='success')
                           {
                              //don't do anything to annoy the user with a dialog or somethign :) 
                           } else {
                              alertMessage("Update failed",response.message,'error');
                              revertFunc(); 
                           }
                       }
                    });
                }
            } 
        },
        eventResize: function(event, delta, revertFunc, jsEvent, ui, view) {
            if(calendarSchedulePress && event.canEdit==1)
            { 
                if(event.eventtype=='maintenance')
                {
                    $.ajax({
                       url: 'includes/ajax_handlers/maintenanceScheduledTicketHandler.php',
                       type: "POST",
                       data: {type:'resize',scheduleid:event.id,newEnd: event.end.format()},
                       dataType: 'json',
                       error: function()
                       {
                           revertFunc();
                       },
                       success: function(response) {
                           if(response.status=='success')
                           {
                              //don't do anything to annoy the user with a dialog or somethign :) 
                           } else {
                              alertMessage("Resize failed",response.message,'error');
                              revertFunc(); 
                           }
                       }
                    });    
                } else {
                    $.ajax({
                       url: 'includes/ajax_handlers/updateCalendarPress.php',
                       type: "POST",
                       data: {type:'resize',jobid:event.id,newEnd: event.end.format()},
                       dataType: 'json',
                       error: function()
                       {
                           revertFunc();
                       },
                       success: function(response) {
                           if(response.status=='success')
                           {
                              //don't do anything to annoy the user with a dialog or somethign :) 
                           } else {
                              alertMessage("Update failed",response.message, 'error');
                              revertFunc(); 
                           }
                       }
                    });
                }
            }
        },
        
        dblclick: function(event, jsEvent) {
            /*
            alert('You doubleclicked! Event: ' + event.title+' and id='+event.id);
            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            alert('View: ' + view.name);
            */
            
        },
        eventClick: function(event, jsEvent, view) {
            currentJobID=event.id;
            var d = new Date();
                
            if(lastClickID == event.id)
            {
                var clickPause = d.getTime() - lastClickTime;
                if( clickPause < 500)
                {
                    console.log('doubleclicked on '+lastClickID);
                    if(event.eventtype=='maintenance')
                    {
                        window.open('maintenanceSchedulePopup.php?popup=true&ne=true&ticketid='+event.id,'Scheduled Maintenance',"scrollbars=0, resizeable=1, width=740, height=750");
                    } else {
                        if(calendarSchedulePress && event.canEdit==1)
                        {
                            window.open('jobPressPopup.php?id='+event.id,'Press Job Editor',"scrollbars=0, resizeable=1, width=940, height=850");
                        } else {
                            window.open('jobPressPopup.php?id='+event.id+'&ne=true','Press Job Editor',"scrollbars=0, resizeable=1, width=940, height=850");
                        }
                    }
                } else {
                    lastClickTime = d.getTime();
                }
            } else {
                lastClickID = event.id;
                lastClickTime = d.getTime();
                return false;
            }
            //sample of coloring an event after click
            //event.backgroundColor = 'yellow';
            //$('#calendar').fullCalendar('rerenderEvents');
            
        },
        eventMouseover: function( event, jsEvent, view ) { 
            $(this).css('border-color','red');
        },
        eventMouseout: function( event, jsEvent, view ) { 
            $(this).css('border-color',event.borderColor);
            lastClickID = 0;
            lastClickTime = 0;
        },
        
        dayClick: function(date, jsEvent, view) {
            /*           
            alert('Clicked on: ' + date.format());
            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

            alert('Current view: ' + view.name);
            */
            // change the day's background color just for fun
           if(view.name=='month')
           {
               $('#calendar').fullCalendar(
                   'changeView','agendaDay' 
               );
                $('#calendar').fullCalendar(
                   'gotoDate', date 
               );
                
           } else {
               if(calendarSchedulePress)
               {
                   var addMessage='This will create a new press job. Are you sure?';
                   var addTitle = "Are you sure you want to create a new job?";
                   var myButtons = {
                        confirm: {
                            label: ' Create new job',
                            className: 'btn-primary'
                        },
                        cancel: {
                            label: 'Cancel',
                            className: 'btn-default'
                        }
                    }
                   
                   if (minPressCalendarJobAddDate > date && overrideLockouts != true)
                   {
                       //means we are trying to schedule a job earlier than earliest allowed
                       var showDate=String(minPressCalendarJobAddDate).split(" GMT");
                       addTitle='Scheduled date/time not allowed';
                       myButtons = {
                                cancel: {
                                    label: 'Understood',
                                    className: 'btn-default'
                                }
                            }
                       addMessage="You can only schedule jobs after "+showDate[0]+". Please contact the production coordinator to schedule this press run.";
                   }
                    bootbox.confirm({
                        title: addTitle,
                        message: addMessage,
                        buttons: myButtons,
                        callback: function (result) {
                            if(result)
                            {
                                $.ajax({
                                   url: 'includes/ajax_handlers/updateCalendarPress.php',
                                   type: "POST",
                                   data: {type:'add',jobid:0,date:date.format()},
                                   dataType: 'json',
                                   success: function(response) {
                                       if(response.status=='success')
                                       {
                                          window.open('jobPressPopup.php?id='+response.jobid,'Press Job Editor',"scrollbars=1, resizeable=1, width=940, height=900");
                                          
                                       } else {
                                          alertMessage("Job creation failed",response.message,'error');
                                       }
                                   }
                                });
                            }
                            
                        }
                      });
                   
               } else {
                   alert('no click!');
               }
           }
            
            
        },

        eventRender: function(event, element) {
            element.attr('data-event-id', event.id);
            element.attr('data-can-edit', event.canEdit);
            element.addClass('context-menu');
            if(event.eventtype=='maintenance')
            {
                element.find('.fc-content').append("<br/>" + event.description);
            } else {
                element.find('.fc-time').prepend('F-'+event.folder+' '+event.tags+"<span id='details"+event.id+"' class='jobdetails' data-id='"+event.id+"' style='float:right;'></span><br />").append(" | <b>Pub Date: </b>"+event.pubDate);
                element.find('.fc-content').append(event.description);
                element.qtip({
                    content: {
                        text: event.tooltip,
                        title: 'Job Details',
                        button: true
                    }, 
                    position: {
                            my: 'left center',
                            at: 'right center'
                        },
                    style: {
                        widget: true, // Optional shadow...
                        def: false,
                        tip: 'left center' // Tips work nicely with the styles too!
                    },
                    show: {
                        event: 'click',
                        solo: true // Only show one tooltip at a time
                    },
                    hide: {
                        event: 'click mouseleave unfocus'
                    }
                });
                
            } 
        },
        droppable: true,
        drop: function( date, jsEvent, ui, resourceId) {
            //we'll update the record via ajax, then refresh the calendar...
            var jobid=ui.helper[0].id;
            $.ajax({
               url: 'includes/ajax_handlers/updateCalendarPress.php',
               type: "POST",
               data: {type:'drop',jobid:jobid,date:date.format()},
               dataType: 'json',
               success: function(response) {
                   if(response.status=='success')
                   {
                      //$('#calendar').fullCalendar('refetchEvents');
                   } else {
                      alertMessage("Job sheduling failed",response.message,'error');
                   }
               }
            });
            $('#'+jobid).remove();
        }                  
    });

$( window ).resize(function() {
  
    if($( window ).width()<800)
    {
        calView = 'agendaDay';   
    } else {
        calView = 'agendaWeek';
    }
    $('#calendar').fullCalendar('changeView', calView);  
});
  
           
$.contextMenu({
        selector: '.context-menu',
        items: {
            edit: {
                name: "Edit Job",
                callback: function(key, opt){
                    var id = opt.$trigger[0].dataset.eventId;
                    var canEdit = opt.$trigger[0].dataset.canEdit;
                    if(calendarSchedulePress && canEdit==1)
                    {
                        window.open('jobPressPopup.php?id='+id,'Press Job Editor',"scrollbars=0, resizeable=1, width=940, height=850");
                    } else {
                        window.open('jobPressPopup.php?id='+id+'&ne=true','Press Job Editor',"scrollbars=0, resizeable=1, width=940, height=850");
                    }
                }
            },
            print: {
                name: "Print Job Ticket",
                callback: function(key, opt){
                    var id = opt.$trigger[0].dataset.eventId;
                    window.open('jobPressTicket.php?action=print&jobid='+id,'Press Job Ticket',"scrollbars=0, resizeable=1, width=750, height=640");
                }
            },
            printStacker: {
                name: "Print Stacker Ticket",
                callback: function(key, opt){
                    var id = opt.$trigger[0].dataset.eventId;
                    var canEdit = opt.$trigger[0].dataset.canEdit;
                    window.open('printouts/pressStackerTicket.php?jobid='+id,'Press Stacker Ticket',"scrollbars=0, resizeable=1, width=750, height=640");
                }
            },
            editRecurrence: {
                name: "Edit Recurrence",
                callback: function(key, opt){
                    var id = opt.$trigger[0].dataset.eventId;
                    var canEdit = opt.$trigger[0].dataset.canEdit;
                    if(calendarSchedulePress && canEdit==1)
                    {
                       window.open('jobRecurring.php?action=edit&recurringid='+id,'Recurring Job Editor',"scrollbars=0, resizeable=1, width=750, height=640");
                    } else {
                       noPerms('edit the recurrence of');
                    }
                }
            },
            viewPress: {
                name: "View in Press Monitor",
                callback: function(key, opt){
                    var id = opt.$trigger[0].dataset.eventId;
                    var canEdit = opt.$trigger[0].dataset.canEdit;
                    window.location='jobMonitor_press.php?jobid='+id;
                }
            },
            viewPagination: {
                name: "View in Pagination Monitor",
                callback: function(key, opt){
                    var id = opt.$trigger[0].dataset.eventId;
                    var canEdit = opt.$trigger[0].dataset.canEdit;
                    window.location='jobMonitor_pagination.php?jobid='+id;
                }
            },
            viewPlateroom: {
                name: "View in Plateroom Monitor",
                callback: function(key, opt){
                    var id = opt.$trigger[0].dataset.eventId;
                    var canEdit = opt.$trigger[0].dataset.canEdit;
                    window.location='jobMonitor_plate.php?jobid='+id;
                }
            },
            showLayout: {
                name: "Show Layout",
                callback: function(key, opt){
                    var id = opt.$trigger[0].dataset.eventId;
                    var canEdit = opt.$trigger[0].dataset.canEdit;
                    window.open('jobPress.php?action=showlayout&jobid='+id);
                }
            },
            unschedule: {
                name: "Unschedule",
                callback: function(key, opt){
                    var id = opt.$trigger[0].dataset.eventId;
                    var canEdit = opt.$trigger[0].dataset.canEdit;
                    if(calendarSchedulePress && canEdit==1)
                    {   
                        bootbox.prompt({
                            title: 'Unschedule Job',
                            message: "This job will be removed from the schedule if you click ok. Please enter a new requested print date:",
                            inputType: 'date',
                            callback: function (result) {
                                $.ajax({
                                   url: 'includes/ajax_handlers/updateCalendarPress.php',
                                   type: "POST",
                                   data: {type:'unschedule',jobid:id,rdate:result},
                                   dataType: 'json',
                                   success: function(response) {
                                       if(response.status=='success')
                                       {
                                            $('#calendar').fullCalendar( 'removeEvents',id );
                                            var view = $('#calendar').fullCalendar('getView');
                                            var d= new Date();
                                            var curDate = Date.parse(view.start);
                                            d.setTime(curDate);
                                            var curMonth = d.getMonth()+1;
                                            var curDay   = d.getDate();
                                            var curYear  = d.getFullYear();
                                            getUnscheduled(curYear,curMonth,curDay);
                                       } else {
                                          alertMessage("Job unscheduling failed<br />"+response.status+'<br>'+response.message,'error');
                                       }
                                   }
                               });
                            }
                        });
                        
                   } else {
                       noPerms('edit');
                   } 
                }
            },
            delete: {
                name: "Delete Job",
                callback: function(key, opt) {
                    var id = opt.$trigger[0].dataset.eventId;
                    var canEdit = opt.$trigger[0].dataset.canEdit;
                    if(calendarSchedulePress && canEdit==1)
                    {   
                        bootbox.confirm({
                            title: 'Remove Job',
                            message: "This item will be permanently deleted and cannot be recovered. Are you sure?",
                            callback: function (result) {
                               $.ajax({
                                   url: 'includes/ajax_handlers/updateCalendarPress.php',
                                   type: "POST",
                                   data: {type:'delete',jobid:id},
                                   dataType: 'json',
                                   success: function(response) {
                                       if(response.status=='success')
                                       {
                                            $('#calendar').fullCalendar( 'removeEvents',id );
                                       } else {
                                            alertMessage("Job deletion failed",response.status+'<br>'+response.sql,'error');
                                       }
                                   }
                               });
                            }
                        });
                   } else {
                       noPerms('delete');
                   }  
                }
            }
        }
    }); 
    
});
  
function getUnscheduled(year,month,date)
{
    //console.log('getting unscheduled events with date='+month+'/'+date+'/'+year);
    $.ajax({
       url: 'includes/ajax_handlers/updateCalendarPress.php',
       type: "POST",
       data: "type=unscheduled&year="+year+"&month="+month+"&date="+date,
       dataType:'json',
       success: function(response) {
           if(response.status=='success')
           {
               $('.unscheduledHolder').empty();  
               $.each(response.jobs, function (j,job){
                  var newDiv=$('<div id="'+job.id+'" data-duration="01:00"><span id="pop'+job.id+'" style="float:right;"><i class="fa fa-search"></i> </span>'+job.title+'</div>').addClass('ui-widget ui-draggable unscheduledJob');
                  var eventObject = {
                        title: job.title // use the element's text as the event title
                  };
                  // store the Event Object in the DOM element so we can get to it later
                  $(newDiv).data('eventObject', eventObject);
                  var dHolder=$('#'+job.dateholder);
                  
                  $('#'+job.dateholder).append(newDiv);  
                  $('#pop'+job.id).qtip({
                        content: {
                            text: job.tooltip,
                            title: 'Job Details',
                            button: true
                        }, 
                        position: {
                                target: $('#pop'+job.id),
                                my: 'left center',
                                at: 'right center'
                            },
                        style: {
                            widget: true, // Optional shadow...
                            def: false,
                            tip: 'left center' // Tips work nicely with the styles too!
                        },
                        show: {
                            event: 'click',
                            solo: true // Only show one tooltip at a time
                        },
                        hide: {
                            event: 'click mouseleave unfocus'
                        }
                    });
                  
                  // make the event draggable using jQuery UI
                  $(newDiv).draggable({
                      zIndex: 999,
                      revert: true,      // will cause the event to go back to its
                      revertDuration: 0  //  original position after the drag
                  });
                    
              });
              
              
           } else {
              alertMessage("Job sheduling failed",response,'error');
           }
       }
    });
     
}
</script>

<div id='loading' style='display:none'>loading...</div>
<div id='quickjump' style='z-index:30000;'>
<?php
//lets make a drop down with a list of the next 52 weeks in it with starting dates
//calculate the date that is "sunday" for the current week
$startdate=date("Y-m-d");
$baseDay = $startdate;
while (date("w",strtotime($startdate))!=0)
{
    $startdate=date("Y-m-d",strtotime($startdate."-1 day"));
}
$baseDate = $startdate;

if(isset($_GET['start']))
{
    $nextPrev=$_GET['start'];
    $nextDayPrev=$_GET['start'];
} else {
    $nextPrev=$startdate;
    $nextDayPrev=$baseDay;
}
$weekBack=date("Y-m-d",strtotime($nextPrev."-1 week"));
$weekForward=date("Y-m-d",strtotime($nextPrev."+1 week"));
$dayBack=date("Y-m-d",strtotime($nextDayPrev."-1 day"));
$dayForward=date("Y-m-d",strtotime($nextDayPrev."+1 day"));

print "<div class='pull-left' >
<form id='quickJumpForm' name='quickJumpForm' method=get class='form-inline'>\n";
print "<div class='form-group'><label for='start'>Select a week to jump to</label> <select id='start' name='start' class='form-control' onChange='\$(\"#quickJumpForm\").submit();'>";

for($i=8;$i>0;$i--)
{
    $backdate=date("Y-m-d",strtotime($startdate."-$i weeks"));       
    if($_GET['start'])
    {
        if($backdate==$_GET['start']){$selected='selected';} else {$selected='';}
    }
    print "<option id='$backdate' value='$backdate' $selected>$backdate</option>\n";
}
for($i=1;$i<53;$i++)
{
    
    if($_GET['start'])
    {
        if($startdate==$_GET['start']){$selected='selected';} else {$selected='';}
    }
    print "<option id='$startdate' value='$startdate' $selected>$startdate</option>\n";
    $startdate=date("Y-m-d",strtotime($startdate."+1 week"));       
}
print "</select></div>";
print "<div class='hidden-xs btn-group pull-right'><a href='?start=$weekBack' class='btn btn-sm btn-primary'><i class='fa fa-chevron-left'></i></a><a href='?start=$baseDate' class='btn btn-sm btn-primary'>Current Week</a><a href='?start=$weekForward' class='btn btn-sm btn-primary'><i class='fa fa-chevron-right'></i></a></div></div>";

print "<div class='visible-xs btn-group pull-right'><a href='?start=$dayBack' class='btn btn-sm btn-primary'><i class='fa fa-chevron-left'></i></a><a href='?start=$baseDay' class='btn btn-sm btn-primary'>Current Day</a><a href='?start=$dayForward' class='btn btn-sm btn-primary'><i class='fa fa-chevron-right'></i></a></div></div>";
print "</form>\n";
?>

</div>

<div id='unscheduledDrawer' style='position:absolute;left:-500px;'>
</div>
<div id='calendar'></div>

<style type="text/css">
.unscheduledJob {
    cursor:pointer;
    background-color: #ffffcb;
    color: black;
    width:80%;
    margin-left:auto;
    margin-right:auto;
    margin-bottom:4px;
    border: 1px solid black;
}
</style>
<div id='unscheduled' style='margin-left:70px;margin-right:50px;width:95%;'>
    <?php
    if($_GET['start'])
    {
        $start = addslashes($_GET['start']);
    } else {
        $start = $baseDate;
    }
    $end=date("Y-m-d",strtotime($start."+7 days"));

    $sql="SELECT * FROM jobs WHERE startdatetime IS Null AND request_printdate>='$start' AND request_printdate<='$end'";
    $dbJobs=dbselectmulti($sql);
    $json['status']='success';
    $json['sql']=$sql;
    if($dbJobs['numrows']>0)
    {
        $binderyJobs=array();
        foreach($dbJobs['data'] as $job)
        {
          $due=$job['request_printdate'];
          $datedue=date("w",strtotime($due));
          
          //get pub and run and draw
          $sql="SELECT * FROM publications WHERE id=$job[pub_id]";
          $dbPub=dbselectsingle($sql);
          $pub=$dbPub['data'];
          
          $sql="SELECT * FROM publications_runs WHERE id=$job[run_id]";
          $dbRun=dbselectsingle($sql);
          $run=$dbRun['data'];
          
          $draw=$job['draw'];
          $folder=$job['folder'];

          $due=date("m/d/Y",strtotime($due));
          $title=stripslashes($pub['pub_name']).' - '.stripslashes($run['run_name'])."<br>Draw: $draw<br>Request: $due";
          if($job['quarterfold']){$fold="Fold: quarter-fold";}else{$fold="Fold: half-fold";}
          if($job['stitch'] || $job['trim']){$stitch="Stitch &amp; Trim: Yes";}else{$stitch="Stitch &amp; Trim: No";}
          if($job['rollSize']!=0)
          {
            $papersize="Roll Size: ".$sizes[$job['rollSize']];
              
          } else {
            $papersize="Roll Size: not set";
              
          }
          if($job['papertype']!=0)
          {
            $papertype="Paper Type: ".$papertypes[$job['papertype']];
              
          } else {
            $papertype="Paper Type: not set";
              
          }
        
        /*
        $sql="SELECT count(id) as pcount FROM job_pages WHERE version=1 AND job_id=$job[id]";
        $dbPages=dbselectsingle($sql);
        $pagecount=$dbPages['data']['pcount'];
        if($pagecount==0){$tpages="Total Pages: not set";}else{$tpages="Total pages: ".$pagecount;}
            
        //get sections and types
        $sql="SELECT * FROM jobs_sections WHERE job_id=$job[id]";
        $dbSections=dbselectsingle($sql);
        if($dbSections['numrows']>0)
        {
            $ptypes=array();
            $scodes=array();
            $sections=$dbSections['data'];
            for($i=1;$i<=3;$i++)
            {
                $rawpages=0;
                $rawcolorpages=0;
                $rawspotpages=0;
                if($sections['section'.$i.'_used']==1)
                {
                    $sectionformat=$sections['section'.$i.'_producttype'];
                    $sectioncode=$sections['section'.$i.'_code'];
                    $sectioncode=str_replace("0","",$sectioncode);
                    $sectioncode=str_replace(" ","",$sectioncode);
                    switch($sectionformat)
                    {
                        case 0:
                            if(!in_array('Bdsht',$ptypes)){$scodes[]=$sectioncode.'-Bdsht';$ptypes[]='Bdsht';}
                        break;
                        
                        case 1:
                            $broadsheetpages+=$rawpages/2;
                            $broadsheetcolorpages+=$rawcolorpages/2;
                            $broadsheetspotpages+=$rawspotpages/2;
                            if(!in_array('Tab',$ptypes)){$scodes[]=$sectioncode.'-Tab';$ptypes[]='Tab';}
                        break;
                        
                        case 2:
                            $broadsheetpages+=$rawpages/2;
                            $broadsheetcolorpages+=$rawcolorpages/2;
                            $broadsheetspotpages+=$rawspotpages/2;
                            if(!in_array('Tab',$ptypes)){$scodes[]=$sectioncode.'-Tab';$ptypes[]='Tab';}
                        break;
                        
                        case 3:
                            $broadsheetpages+=$rawpages/4;
                            $broadsheetcolorpages+=$rawcolorpages/4;
                            $broadsheetspotpages+=$rawspotpages/4;
                            if(!in_array('Flexi',$ptypes)){$scodes[]=$sectioncode.'-Flexi';$ptypes[]='Flexi';}
                        break;
                    }
                }
                
            }
            if(count($scodes)>0){
                $scodes=trim(implode(",",$scodes),',');
            } else {
                $scodes='None set';
            }
        }
        */
          $tooltip="Folder: $folder<br />".stripslashes($pub['pub_name']).' - '.stripslashes($run['run_name'])."<br>Draw: $draw<br />Pub Date: ".date("m/d/Y",strtotime($job['pub_date']));
          $tooltip.="<br>Request: $due<br />$fold<br />$papertype<br /><br />$papersize<br />$stitch<br />$tpages<br />Sections: $scodes<br />JobID: $job[id]";
            
          $binderyJobs[]=array('id'=>$job['id'],'title'=>$title,'tooltip'=>$tooltip,'dateholder'=>"usDate_".$datedue);    
        }
        $json['jobs']=$binderyJobs;
    } else {
        $json['jobs']=array();
    }
    
    for($i=0;$i<=6;$i++)
    {
        //$date=date("Ymd",strtotime($basedate." +$i days"));
        print "<div style='float:left;width:".floor(100/7)."%;margin-right:2px;text-align:center;background-color:#bbb;'>
        <p class='place'><b>Unscheduled Jobs</b></p>
        <div id='usDate_$i' class='unscheduledHolder'>
        </div>
        </div>\n";
    }
    ?>
    <div class='clear'></div>
</div>

<?php
global $User;

function fetchEvents()
{
    global $pubids, $sizes, $papertypes, $folders, $User;
    if($_GET['start'])
    {
       $startOfWeek=date("Y-m-d",strtotime($_GET['start'])); 
       //$startOfWeek=date("Y-m-d",strtotime($startOfWeek."-1 day")); 
    } else {
       $dow = date("w");
       $startOfWeek = date("Y-m-d",strtotime("-$dow days"));
    }
    if($_GET['end'])
    {
       $endOfWeek=date("Y-m-d",strtotime($_GET['end']));
       //$endOfWeek=date("Y-m-d",strtotime($endOfWeek."+1 day")); 
    } else {
       $endOfWeek = date("Y-m-d",strtotime($startOfWeek."+7 days"));
    }
    if($User->hasPermission(45))
    {
        $sql="SELECT A.*, B.run_name, C.pub_name, C.pub_color, C.reverse_text FROM jobs A, publications_runs B, publications C WHERE A.startdatetime>='$startOfWeek 00:00' AND A.enddatetime<'$endOfWeek 23:59' AND A.run_id=B.id AND A.pub_id=C.id AND A.status<>99";
    } else {
        $sql="SELECT A.*, B.run_name, C.pub_name, C.pub_color, C.reverse_text FROM jobs A, publications_runs B, publications C WHERE A.startdatetime>='$startOfWeek 00:00' AND A.enddatetime<'$endOfWeek 23:59' AND A.pub_id IN ($pubids) AND A.run_id=B.id AND A.pub_id=C.id AND A.status<>99";
    }
    $dbSchedule=dbselectmulti($sql);
    $pubids=explode(",",$pubids);
    if ($dbSchedule['numrows']>0)
    {
        $i=0;
        foreach ($dbSchedule['data'] as $schedule)
        {
            $title=$schedule['pub_name'].' '.$schedule['run_name'];
            if ($schedule['layout_id']!=0)
            {
                $jobticket="<i class='fa fa-print'></i> ";
            } else {
                $jobticket=""; 
            }
            $caution='';
            if ($schedule['recurring_id']!=0)
            {
                $recurring="<i class='fa fa-repeat'></i>";
                $recurringid=$schedule['recurring_id'];    
            } else {
                $recurring='';
                $recurringid=0;
            }
        
            $draw=$schedule['draw'];
            $jobid=$schedule['id'];
            $folder=$folders[$schedule['folder']];
            $fclass='Folder: '.$folder;
            $jid="JOB ID: $schedule[id]";
            $description="Folder: $folder<br />Draw: $draw<br />$jid";
            $fulldescription="Folder: $folder<br />$title<br>Draw: $draw<br />$jid";
            
            if ($schedule['pub_color']!='')
            {
                $backcolor=$schedule['pub_color'];
            } else {
                $backcolor="#EEEEEE";
            }
            if ($schedule['reverse_text'])
            {
                $forecolor='#FFFFFF';
            } else {
                $forecolor="#000000";
            }
            
            if($schedule['quarterfold']){$fold="Fold: quarter-fold";}else{$fold="Fold: half-fold";}
            if($schedule['stitch'] || $schedule['trim']){$stitch="Stitch &amp; Trim: Yes";}else{$stitch="Stitch &amp; Trim: No";}
            
            
            if($schedule['rollSize']!=0)
            {
              $papersize="Roll Size: ".$sizes[$schedule['rollSize']];
            } else {
              $papersize="Roll Size: not set";
            }
            if($schedule['papertype']!=0)
            {
              $papertype="Paper: ".$papertypes[$schedule['papertype']];
            } else {
              $papertype="Paper: not set";
            }
            
            /*
            $sql="SELECT count(id) as pcount FROM job_pages WHERE version=1 AND job_id=$jobid";
            $dbPages=dbselectsingle($sql);
            $pagecount=$dbPages['data']['pcount'];
            if($pagecount==0){$tpages="Total Pages: not set";}else{$tpages="Total pages: ".$pagecount;}
            
            //get sections and types
            $sql="SELECT * FROM jobs_sections WHERE job_id=$schedule[id]";
            $dbSections=dbselectsingle($sql);
            if($dbSections['numrows']>0)
            {
                $ptypes=array();
                $scodes=array();
                $sections=$dbSections['data'];
                for($i=1;$i<=3;$i++)
                {
                    $rawpages=0;
                    $rawcolorpages=0;
                    $rawspotpages=0;
                    if($sections['section'.$i.'_used']==1)
                    {
                        $sectionformat=$sections['section'.$i.'_producttype'];
                        $sectioncode=$sections['section'.$i.'_code'];
                        $sectioncode=str_replace("0","",$sectioncode);
                        $sectioncode=str_replace(" ","",$sectioncode);
                        switch($sectionformat)
                        {
                            case 0:
                                if(!in_array('Bdsht',$ptypes)){$scodes[]=$sectioncode.'-Bdsht';$ptypes[]='Bdsht';}
                            break;
                            
                            case 1:
                                $broadsheetpages+=$rawpages/2;
                                $broadsheetcolorpages+=$rawcolorpages/2;
                                $broadsheetspotpages+=$rawspotpages/2;
                                if(!in_array('Tab',$ptypes)){$scodes[]=$sectioncode.'-Tab';$ptypes[]='Tab';}
                            break;
                            
                            case 2:
                                $broadsheetpages+=$rawpages/2;
                                $broadsheetcolorpages+=$rawcolorpages/2;
                                $broadsheetspotpages+=$rawspotpages/2;
                                if(!in_array('Tab',$ptypes)){$scodes[]=$sectioncode.'-Tab';$ptypes[]='Tab';}
                            break;
                            
                            case 3:
                                $broadsheetpages+=$rawpages/4;
                                $broadsheetcolorpages+=$rawcolorpages/4;
                                $broadsheetspotpages+=$rawspotpages/4;
                                if(!in_array('Flexi',$ptypes)){$scodes[]=$sectioncode.'-Flexi';$ptypes[]='Flexi';}
                            break;
                        }
                    }
                    
                    
                }
                if(count($scodes)>0){
                    $scodes=trim(implode(",",$scodes),',');
                } else {
                    $scodes='None set';
                }
            }
            
            */
            $tooltip="Folder: $folder<br />$title<br>Draw: $draw<br />Pub Date: ".date("m/d/Y",strtotime($schedule['pub_date']));
            $tooltip.="<br />$fold<br />$papertype<br /><br />$papersize<br />$stitch<br />$tpages<br />Start: ".date("m/d/Y H:i",strtotime($schedule['startdatetime']))."<br />End: ".date("m/d/Y H:i",strtotime($schedule['enddatetime']))."<br />Sections: $scodes<br />$jid";
            
            $classname="publications".$schedule['pub_id'];
            $canEdit = $User->canEdit($schedule['pub_id']);
            
            $pubDate = date("Y-m-d",strtotime($schedule['pub_date']));
            //$description = "<b>Pub Date: </b>".$pubDate."<br />".$description;
            $i++;
            //now print it out
            print "
    {
        id : $schedule[id],
        title : \"$title\",
        pubDate : \"$pubDate\",
        start : '".date("Y-m-d H:i",strtotime($schedule['startdatetime']))."',
        end : '".date("Y-m-d H:i",strtotime($schedule['enddatetime']))."',
        allDay : false,
        tags : \"$jobticket.$caution.$recurring\",
        description : \"$description\",
        fulldetails : \"$fulldescription\",
        color : '$backcolor',
        backgroundColor : '$backcolor',
        borderColor : '$backcolor',
        textColor : '$forecolor',
        folder : '$schedule[folder]',
        recurringid : '$recurringid',
        canEdit : $canEdit,
        tooltip : \"$tooltip\",
        editable : '$mypub',
        eventtype : 'job',
        surl : 'jobPressPopup.php?id=$schedule[id]'
    }";
            if($i<$dbSchedule['numrows']){print ",\n";}
            
        }

    }
    $sql="SELECT * FROM maintenance_scheduled WHERE equipment_type='press' AND starttime>='$startOfWeek' AND endtime<'$endOfWeek'";
    $dbSchedule=dbselectmulti($sql);
    if($_GET['mode']=='test'){print $sql;}
    if ($dbSchedule['numrows']>0)
    {
        $i=0;
        foreach ($dbSchedule['data'] as $schedule)
        {
            $jid="Maintenance ID: $schedule[id]";
            $description="Maintenance task.";
            
        print "
    {
        id' => $schedule[id],
        title' => 'Maintenance',
        start : '".date("Y-m-d H:i",strtotime($schedule['starttime']))."',
        end : '".date("Y-m-d H:i",strtotime($schedule['endtime']))."',
        allDay : false,
        tags : '',
        description : '$description',
        fulldetails : '$description',
        color : '#fff',
        tooltip : 'Maintenance task',
        backgroundColor : '#000',
        borderColor : '#000',
        textColor : '#fff',
        eventtype : 'maintenance',
        surl : ''
    }";
            if($i<$dbSchedule['numrows']){print ",\n";}
        }

    }
    $end = time();
    
    
}
$Page->footer();

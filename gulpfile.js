// including plugins
var gulp = require('gulp')
, uglify = require("gulp-uglify")
, sass = require("gulp-sass")
, rename = require("gulp-rename")
, clean = require("gulp-clean")
, concat = require("gulp-concat");
 
// functions
var concat = require('gulp-concat-multi');
 
function scripts() {
  concat({
    'vendor.js': 'js/vendor/**/*.js',
    'app.js': ['js/lib/**/*.js', 'js/app.js']
  })
	.pipe(gulp.dest('dist/js'));
} 
 
// task
gulp.task('bundle-one', function () {
    gulp.src('./CoffeeScript/*.coffee') // path to your files
    .pipe(concat('bundleOne.js')) // concat files
    .pipe(coffee()) // compile coffee
    .pipe(uglify()) // minify files
    .pipe(header(getCopyrightVersion(), {version: getVersion()})) // Add the copyright
    .pipe(gulp.dest('path/to/destination'));
});
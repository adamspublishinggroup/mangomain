<?php
//<!--VERSION: .7 **||**-->
?>
<html>
<head>
<style type='text/css'>
body {
    font-family:Verdana;
    font-size:12px;
}
.fieldlabel
{
    float:left;
    font-weight:bold;
    width:150px;
    height:30px;
    padding-top:14px;
}
.fieldvalue
{
    float:left;
    height:30px;
    width:200px;
    border-bottom:1px solid black;   
}
.noteline
{
    float:left;
    height:30px;
    width:420px;
    border-bottom:1px solid black;   
}
.checkbox {
    height:20px;
    width:20px;
    border: 1px solid black;
    float:left;
    margin-right:3px;
}
.checkitem {
    width:170px;
    font-size:10px;
    float:left;
}

</style>
</head>
<?php
if ($_GET['action']=='print')
{
    include("includes/functions_db.php");
    include("includes/functions_formtools.php");
    include("includes/config.php");
} else {
    include("includes/boot.php") ;
    $scriptpath='http://'.$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'] ;
}

//print "<body onready='window.print();'>\n";
print "<body>\n";
if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}

switch ($action)
{
    case "print":
    inserter_jobTicket('print');
    break;
}
  

function inserter_jobTicket()
{
    $packageid=intval($_GET['packageid']);
    //get plan details
    $sql="SELECT A.*, B.pub_name FROM jobs_inserter_packages A, publications B WHERE A.id=$packageid AND A.pub_id=B.id AND A.continue_id=0";
    $dbPackage=dbselectsingle($sql);
    $package=$dbPackage['data'];
    $pubid=$package['pub_id'];
    $pubdate=$package['pub_date'];
    $planid=$package['plan_id'];
    $displaydate=date("D, m/d/Y",strtotime($package['pub_date']));
    $pubname=$package['pub_name'];
    $packagename=$package['package_name'];
    
    $sql="SELECT * FROM inserts WHERE id=$package[jacket_insert_id]";
    $dbJacket=dbselectsingle($sql);
    //now find the inserts
    $sql="SELECT A.*, B.customer_name FROM inserts A, customers B WHERE A.package_id=$packageid AND A.advertiser_id=B.id ORDER BY B.customer_name";
    $dbInserts=dbselectmulti($sql);
    
    //get a list of hoppers for this inserter
    $sql="SELECT * FROM jobs_inserter_plans WHERE id=$planid";
    $dbPlan=dbselectsingle($sql);
    $plan=$dbPlan['data'];
    $inserterid=$plan['inserter_id'];
    $sql="SELECT * FROM inserters WHERE id=$inserterid";
    $dbInserter=dbselectsingle($sql);
    $inserter=$dbInserter['data'];
    
    $sql="SELECT * FROM inserters_hoppers WHERE inserter_id=$inserterid ORDER BY hopper_number";
    $dbHoppers=dbselectmulti($sql);
    $hoppers=array();
    $hoppersTwo=array();
    $double=0;
    if ($package['double_out'])
    {
        $turn=$inserter['inserter_turn'];
        foreach($dbHoppers['data'] as $hopper)
        {
            if ($hopper['hopper_number']<=$turn)
            {
                $hoppers[$hopper['id']]=$hopper['hopper_number'];
            }   
            if ($hopper['hopper_number']>$turn)
            {
                $hoppersTwo[$hopper['id']]=$hopper['hopper_number'];
            }
        }
        $double=1;
    } else {
        foreach($dbHoppers['data'] as $hopper)
        {
            $hoppers[$hopper['id']]=$hopper['hopper_number'];
        }
    }
    print "<div id='layout' style='width:670px;height:900px;'>\n";
    print "<p style='text-align:center;font-weight:bold;font-size:16px;'>Inserter Job Ticket</p>\n";
    print "<p style='text-align:center;font-weight:bold;font-size:14px;'>for $pubname - $packagename run, Publish date: $displaydate</p>\n";
    //look in inserts for this pub date, pub id and sticky_note=1
    $sql="SELECT A.*, B.customer_name FROM inserts A, customers B WHERE A.pub_id='$pubid' AND A.pub_date='$pubdate' AND A.sticky_note=1 AND A.advertiser_id=B.id";
    $dbStickyNote=dbselectsingle($sql);
    if($dbStickyNote['numrows']>0)
    {
        print "<p style='text-align:center;font-weight:bold;'>There is a sticky note from ".$dbStickyNote['data']['customer_name']." for this publication today!</p>\n";
    }
    
    
    print "<div id='leftside' style='width:360px;float:left;'>\n";
    
    //generate the layout of the inserter
    //if ($double)
    //{
        $hoppercount=0;
        //generate side two first
        //if the inserter is oval, lets reverse the order of hoppers on this side
        if ($inserter['inserter_type']=='oval')
        {
            $hoppers=array_reverse($hoppers,1);
        }
        
        print "<div id='inserterSideTwo' style='width:175;float:left;margin-right:5px;'>\n";
        print "<p style='text-align:center;font-weight:bold;font-size:14px;'>$inserter[side_two_name]</p>\n";
        foreach($hoppersTwo as $hopperid=>$hopperName)
        {
            $detail=build_hoppers($hopperid,$hopperName,$dbInserts['data']);
            if ($detail['used']){$hoppercount++;$insertcount+=$detail['count'];}
        }
        
        print "</div>\n"; 
        print "<div id='inserterSideOne' style='width:175;float:left;margin-right:5px;'>\n";
        print "<p style='text-align:center;font-weight:bold;font-size:14px;'>$inserter[side_one_name]</p>\n";
        foreach($hoppers as $hopperid=>$hopperName)
        {
            $detail=build_hoppers($hopperid,$hopperName,$dbInserts['data']);
            if ($detail['used']){$hoppercount++;$insertcount+=$detail['count'];}
        }
        
        print "</div>\n";    
    /*} else {
        $hoppercount=0;
        print "<p style='text-align:center;font-weight:bold;font-size:14px;'>$inserter[side_one_name]</p>\n";
        foreach($hoppers as $hopperid=>$hopperName)
        {
            $detail=build_hoppers($hopperid,$hopperName,$dbInserts['data']);
            if ($detail['used']){$hoppercount++;$insertcount+=$detail['count'];}
        }
        
    }
    */
    print "Total Hoppers Used: $hoppercount<br />\n";
    print "Total Calculated Pieces: $insertcount<br />\n";
    
     print "<div style='margin-top:10px;background-color:white;padding:4px;'>\n";
        //pull in any checklist items
        $sql="SELECT * FROM checklist WHERE checklist_category='Mailroom' ORDER BY checklist_order ASC";
        $dbCheck=dbselectmulti($sql);
        if ($dbCheck['numrows']>0)
        {
            print "<p style='font-weight:bold;text-size:12px;'>Daily Checklist items</p>\n";
            foreach($dbCheck['data'] as $item)
            {
                print "<div class='checkbox'></div><div class='checkitem'>$item[checklist_item]</div><div style='clear:both;'></div>\n";
                
            }
        }   
     print "</div>\n";  //closes the checklist
   print "</div>\n";  //closes the left side
     
   print "<div id='rightside' style='float:left;width:310px;'>\n"; //open the right side
   
   //check for sticky note
   if ($dbInsert['numrows']>0)
   {
       foreach($dbInserts['data'] as $insert)
       {
            if ($insert['sticky_note']){
                $sticky=true;
                print "<p style='font-weight:bold;font-size:12px;'>This package run has a sticky note for $insert[customer_name]</p>\n";
            }    
       }
   }
   print "<p><b>This run is scheduled to start:</b></p>\n";
   $start=date("D, m/d/Y \@ H:i",strtotime($package['package_startdatetime']));
   print "<p>$start</p>\n";
   print "<p><b>Request to produce:</b>&nbsp;$package[inserter_request]</p>\n";
   
   if ($GLOBALS['insertSignOff']!='')
   {
       print "<p style='font-weight:bold;margin-top:10px;width:320px;border-top:thin solid black;'>INSERT SIGN-OFF POLICY:</p>\n";
       print stripslashes($GLOBALS['insertSignOff']);
   } else {
       print "<p style='font-weight:bold;margin-top:10px;width:320px;border-top:thin solid black;'>Inserted signed off by:</p>\n";
   }
   print "<br />Signed:<p style='width:200px;border-bottom:thin solid black;margin-top:10px;'>&nbsp;</p>\n";
   print "<p style='font-weight:bold;margin-top:10px;width:320px;'>MACHINE SETUP BY:</p>\n";
   print "<p style='width:200px;border-bottom:thin solid black;margin-top:10px;'>&nbsp;</p>\n";
   print "<p style='width:200px;border-bottom:thin solid black;margin-top:10px;'>&nbsp;</p>\n";
   print "<p style='font-weight:bold;margin-top:10px;width:320px;'>&nbsp;</p>\n";
   print "<div style='float:left;font-weight:bold;width:200px;'>How many employees:</div><div style='float:left;width:100px;border-bottom:thin solid black;height:19px;'>&nbsp;</div><div style='clear:both;height:1px;'></div>\n";
   print "<div style='float:left;font-weight:bold;width:200px;'>How many pallets<br />shrink-wrapped:</div><div style='float:left;width:100px;border-bottom:thin solid black;height:19px;'>&nbsp;</div><div style='clear:both;height:1px;'></div>\n";
   print "<div style='float:left;font-weight:bold;width:200px;'>Leftover Jackets:</div><div style='float:left;width:100px;border-bottom:thin solid black;height:19px;'>&nbsp;</div><div style='clear:both;height:1px;'></div>\n";
   print "<div style='float:left;font-weight:bold;width:200px;'>Total pieces:<br /><small>From inserter report</small></div><div style='float:left;width:100px;border-bottom:thin solid black;height:19px;'>&nbsp;</div><div style='clear:both;height:1px;'></div>\n";
   print "<div style='float:left;font-weight:bold;width:200px;'>Did we address it:</div><div style='float:left;width:100px;border-bottom:thin solid black;height:19px;'>&nbsp;</div><div style='clear:both;height:1px;'></div>\n";
   
   //now make spots for times
   //3 floated divs
   print "<div style='float:left;width:100px;font-weight:bold;'>\n";
       print "<p style='height:20px;'><b>Times</b></p>\n";
       print "<p style='height:20px;'>Run Start:</p>\n";
       print "<p style='height:20px;'>Run Finish:</p>\n";
       print "<p style='height:20px;'>First Bundle:</p>\n";
       print "<p style='height:20px;'>Last Bundle:</p>\n";
       print "<p style='height:20px;'>First Truck:</p>\n";
       print "<p style='height:20px;'>Last Truck:</p>\n";
   print "</div>\n";
   print "<div style='float:left;width:90px;margin-left:5px;'>\n";
       print "<p style='height:20px;font-weight:bold;'>$inserter[side_one_name]</p>\n";;
       print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
       print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
       print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
       print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
       print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
       print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
   print "</div>\n";
   if ($double)
   {
       print "<div style='float:left;width:90px;margin-left:5px;'>\n";
           print "<p style='height:20px;font-weight:bold;'>$inserter[side_two_name]</p>\n";;
           print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
           print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
           print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
           print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
           print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
           print "<p style='width:90px;border-bottom:thin solid black;height:19px;background-color:yellow;'>&nbsp;</p>\n";
       print "</div>\n";
   }
   print "<div style='clear:both;height:1px;'></div>\n";    
   
   
   print "</div>\n"; //closes the right side    
   //now some notes Lines:
    print "<p style='font-weight:bold;font-size:14px;'>NOTES:</p>\n";
    print "<div style='width:670px;height:20px;border-bottom:thin solid black;'></div>\n";
    print "<div style='width:670px;height:20px;border-bottom:thin solid black;'></div>\n";
    print "<div style='width:670px;height:20px;border-bottom:thin solid black;'></div>\n";
    print "<div style='width:670px;height:20px;border-bottom:thin solid black;'></div>\n"; 
        
    print "</div>\n"; //close the outer div
    
   
}

function build_hoppers($hopperid,$hoppername,$inserts)
{
    $insertname="";
    $insertcount="";
    $used=false;
    if (count($inserts)>0)
    {
        foreach($inserts as $insert)
        {
            if ($insert['hopper_one_id']==$hopperid || $insert['hopper_two_id']==$hopperid)
            {
                $insertname=$insert['customer_name'];
                if ($insertname=='WE PRINT')
                {
                    $insertname=str_replace("WE PRINT - ","",stripslashes($insert['insert_tagline']));
                } else {
                    $insertname.="<br />".$insert['insert_tagline'];
                }
                $insertcount=$insert['insert_count'];
                $used=true;
            }
            
        }
    }
    print "<div style='margin-bottom:4px;width:20px;height:30px;vertical-align:center;border-right:thin solid black;margin-right:2px;padding:2px;text-align:center;font-weight:bold;color:white;background-color:black;float:left'>$hoppername</div>\n";
    print "<div style='margin-bottom:4px;height:30px;float:left;font-size:10px'><b>$insertname</b><br />Qty: $insertcount</div>\n";
    print "<div style='clear:both;height:1px;'></div>\n";
    return array('used'=>$used,'count'=>$insertcount);     
}
?>

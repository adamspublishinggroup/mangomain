<?php
include("includes/boot.php");

if ($_POST)
{
    send_email();
} else {
    if ($_GET['mode']=='test')
    {
        $startdate="2009-08-04 6:00";
        $enddate="2009-08-05 6:00";
        $rundate="2009-08-05";
    }elseif($_GET['mode']=='manual')
    {
        global $users;
        //ok, start by getting a list of groups that get email summaries
        $sql="SELECT id, group_name FROM email_groups WHERE site_id=".SITE_ID." AND group_active=1";
        $dbGroups=dbselectmulti($sql);
        $groups=array();
        $groups[0]="Please select";
        $groups['all']="Send to all groups";
        if ($dbGroups['numrows']>0)
        {
            foreach($dbGroups['data'] as $g)
            {
                $groups[$g['id']]=stripslashes($g['group_name']);
            }
        }
        //we have people!
        print "<form method=post class='form-horizontal'>\n";
        make_date("run_date",date("Y-m-d"),'What print date?');
        make_select('to',$users[0],$users,'Send to user','Send email to specific user');
        make_select('group',$groups[0],$groups,'Send to group','Send to a defined email group.');
        make_checkbox('noemail',0,'Show on screen','Checked, report will be displayed on screen');
        make_hidden('mode','manual');
        make_submit('submit','Send Email');
        print "</form>\n";
    } else {
        send_email(true);
    }

}

function send_email($auto=false)
{
    if($_POST)
    {
        $rundate=$_POST['run_date'];
        $enddate=$rundate." 6:00";
        $startdate=date("Y-m-d H:i",strtotime($enddate." -1 day"));
    } else {
        $startdate=date("Y-m-d",strtotime("-1 day"))." 6:00";
        $enddate=date("Y-m-d")." 6:00";
        $rundate=date("Y-m-d");
    }
    
    if($_POST['group']!=0 && $_POST['group']!='all' && $_POST) {
    
        $email=build_email_for_group(intval($_POST['group']),$startdate,$enddate);
        //build and send the email
        $emailaddress=stripslashes($group['group_email']);
        $message="<html><head></head><body>";
        if(count($email)>0)
        {
            foreach($email as $part)
            {
                $summary.=$part['summary']."<br>";
                $details.=$part['job']."<br>";
            }
        }
        $message.=$summary.$details."</body></html>\n";
        
        if($_POST['noemail'])
        {
            //dont send this!
            print $message;
            print "Running once with $rundate as date<br>";
            
        } else {
            $mail = new htmlMimeMail();
            $mail->setHtml($message);
            $mail->setFrom($GLOBALS['systemEmailFromAddress']);
            $mail->setSubject("Daily production run summary for $rundate");
            $result = $mail->send($emailaddresses);
        }
        print "<div class='alert alert-success' role='alert'>Email sent successfully. <a href='?action=again' class='btn btn-primary'>Send another</a></div>";
             
    } elseif($_POST['group']=='all' || $auto==true)
    {
        $sql="SELECT * FROM email_groups WHERE site_id=".SITE_ID." AND group_active=1";
        $dbGroups=dbselectmulti($sql);

        if ($dbGroups['numrows']>0)
        {
            //we have people!
            foreach($dbGroups['data'] as $group)
            {
                $email=build_email_for_group($group['id'],$startdate,$enddate);
                //build and send the email
                $emailaddress=stripslashes($group['group_email']);
                
                $message="<html><head></head><body>";
                if(count($email)>0)
                {
                    foreach($email as $part)
                    {
                        $summary.=$part['summary']."<br>";
                        $details.=$part['job']."<br>";
                    }
                }
                $message.=$summary.$details."</body></html>\n";
                
                
                if($_POST['noemail'])
                {
                    //dont send this!
                    print $message;
                    print "Running once with $rundate as date<br>";
                    
                } else {
                    $mail = new htmlMimeMail();
                    $mail->setHtml($message);
                    $mail->setFrom($GLOBALS['systemEmailFromAddress']);
                    $mail->setSubject("Daily production run summary for $rundate");
                    $result = $mail->send($emailaddresses);
                
                }
                print "<div class='alert alert-success' role='alert'>Email sent successfully. <a href='?mode=manual' class='btn btn-primary'>Send another</a></div>";
        
            }
        } 
    } elseif($_POST['to']) {   
        //find out what pubs this user can see
        $email=build_email_for_user($_POST['to'],$startdate,$enddate);
        $sql="SELECT email FROM users WHERE id=".intval($_POST['to']);
        $dbUser=dbselectsingle($sql);
        $emailaddresses[]=$dbUser['data']['email'];
        $summary='';
        $details='';        
        $message="<html><head></head><body>";
        if(count($email)>0)
        {
            foreach($email as $part)
            {
                $summary.=$part['summary']."<br>";
                $details.=$part['job']."<br>";
            }
        }
        $message.=$summary.$details."</body></html>\n";
        
        /**
        * Sends the message.
        */
        
        if($_POST['noemail'])
        {
            //dont send this!
            print $message;
            print "Running once with $rundate as date<br>";
            
        } else {
            $mail = new htmlMimeMail();
            $mail->setHtml($message);
            $mail->setFrom($GLOBALS['systemEmailFromAddress']);
            $mail->setSubject("Daily production run summary for $rundate");
            $result = $mail->send($emailaddresses);
            print "<div class='alert alert-success' role='alert'>Email sent successfully. <a href='?action=again' class='btn btn-primary'>Send another</a></div>";
        }
    } 
}
  
function build_email_for_group($groupid,$startdate,$enddate)
{
    //ok, we need to get the publications that are accessible for this user
    $sql="SELECT * FROM email_groups_publications WHERE group_id=$groupid AND value=1";
    $dbPubs=dbselectmulti($sql);
    $email=array();
    if ($dbPubs['numrows']>0)
    {
        $email="";
        //found pubs for this user
        //now, look through the pubs to find jobs
        foreach($dbPubs['data'] as $pub)
        {
            $pubids[]=$pub['pub_id'];
        }
        
        $pubids=implode(",",$pubids);
        $email = build_emails($pubids,$startdate,$enddate);
    }
    return $email;
}

 
function build_email_for_user($userID,$startdate,$enddate)
{
    $sql="SELECT pub_id FROM user_publications WHERE user_id=$userID AND value=1";
    $dbPubs=dbselectmulti($sql);
    if ($dbPubs['numrows']>0)
    {
        $pubids=array();
        foreach($dbPubs['data'] as $pub)
        {
            $pubids[]=$pub['pub_id'];
        }
        $pubids=implode(",",$pubids);
    }
    $email = build_emails($pubids,$startdate,$enddate);
    return $email;
    
}

function build_emails($pubids,$startdate,$enddate)
{
    $email=array();
    $sql="SELECT * FROM jobs WHERE pub_id IN ($pubids) AND startdatetime>='$startdate' AND enddatetime<='$enddate' 
    AND pub_date<>'' ORDER BY startdatetime DESC";
    $dbJobs=dbselectmulti($sql);
    if ($dbJobs['numrows']>0)
    {
        foreach ($dbJobs['data'] as $job)
        {
            $email[]=getJobDetails($job);    
        }
    }
    
    
    //now the same thing for inserter jobs
    $sql="SELECT * FROM jobs_inserter_plans WHERE pub_id IN ($pubids) AND pub_date='$startdate' ORDER BY startdatetime DESC";
    $dbJobs=dbselectmulti($sql);
    if ($dbJobs['numrows']>0)
    {
        foreach ($dbJobs['data'] as $job)
        {
            $email[]=getInserterJobDetails($job);    
        }
    }
    return $email;
}

function getJobDetails($job)
{
    global $enableJobStops, $enableBenchmarks, $producttypes, $pressmen;
    $jobid=$job['id'];
    //here we display the job data and all it's stats, we'll use a table layout
    //we'll need to get lots of other data pieces at this point
    $sql="SELECT pub_name FROM publications WHERE id=$job[pub_id]";
    $dbPub=dbselectsingle($sql);

    $sql="SELECT * FROM publications_runs WHERE id=$job[run_id]";
    $dbRun=dbselectsingle($sql);
    $run=$dbRun['data'];
    //get total number of plate sets
    $sql="SELECT * FROM job_plates WHERE current=1 AND job_id=$jobid";
    $dbTotalPlates=dbselectmulti($sql);
    $totalplates=$dbTotalPlates['numrows'];
    
    //what day of the week is the pub date... will affect which leadtimes to grab from
    $dow=date("N",strtotime($job['pub_date']));
    $xlastcolor=$run['last_colorpage_leadtime_'.$dow];
    $xlastpage=$run['last_page_leadtime_'.$dow];
    $xlastplate=$run['last_plate_leadtime_'.$dow];
    $xlast2=$run['plates_2_left_leadtime_'.$dow];
    $xlast3=$run['plates_3_left_leadtime_'.$dow];
    $xlast4=$run['plates_4_left_leadtime_'.$dow];
    $xlast5=$run['plates_5_left_leadtime_'.$dow];
    $xlast6=$run['plates_6_left_leadtime_'.$dow];
    $xschedule=$run['schedule_leadtime_'.$dow]; //hours
    $xchaseplate=$run['chase_plate_aftertime_'.$dow];
    $xchasestart=$run['chase_start_aftertime_'.$dow];
    $xrunlength=$run['run_length_'.$dow];
    
    //lets calculate page flow  --get how many at press start time
    $pstart=date("Y-m-d H:i:s",strtotime($job['startdatetime']));
    //how many plates out after this point?
    $sql="SELECT * FROM job_plates WHERE remake=0 AND current=1 AND (plate_approval>='$pstart' OR plate_approval IS NULL) AND job_id=$jobid";
    $dbPlates=dbselectmulti($sql);
    $platesatstart=$dbPlates['numrows'];
    
    $platesatstartTotal=0;
    if($dbPlates['numrows']>0)
    {
        foreach($dbPlates['data'] as $temp)
        {
            if($temp['color'])
            {
                $platesatstartTotal=$platesatstartTotal+4;
            } else {
                $platesatstartTotal++;
            }
        }
    }
        
    //how many pages out after this point?
    $sql="SELECT * FROM job_pages WHERE remake=0 AND (workflow_receive>='$pstart' OR workflow_receive IS NULL) AND job_id=$jobid";
    $dbPages=dbselectmulti($sql);
    $pagesatstart=$dbPages['numrows'];
    
    //lets calculate page flow  --get how many at 5 minutes beforepress start time
    $pstartminus5=date("Y-m-d H:i:s",strtotime($job['startdatetime']." -5 minutes"));
    //how many plates out after this point?
    $sql="SELECT * FROM job_plates WHERE remake=0 AND (plate_approval>='$pstartminus5' OR plate_approval IS NULL) AND job_id=$jobid";
    $dbPlates=dbselectmulti($sql);
    $platesatstartminus5=$dbPlates['numrows'];
    
    $platesatstartTotalMinus5=0;
    if($dbPlates['numrows']>0)
    {
        foreach($dbPlates['data'] as $temp)
        {
            if($temp['color'])
            {
                $platesatstartTotalMinus5=$platesatstartTotalMinus5+4;
            } else {
                $platesatstartTotalMinus5++;
            }
        }
    }    
    
    //how many pages out after this point?
    $sql="SELECT * FROM job_pages WHERE remake=0 AND (page_release>='$pstartminus5' OR page_release IS NULL) AND job_id=$jobid";
    $dbPages=dbselectmulti($sql);
    $pagesatstartminus5=$dbPages['numrows'];
    
    
    if($xlastplate!=0)
    {
        $lastplatetime=date("Y-m-d H:i:s",strtotime($job['startdatetime']."-$xlastplate minutes"));
        //how many plates out after this point?
        $sql="SELECT * FROM job_plates WHERE remake=0 AND (plate_approval>='$lastplatetime' OR plate_approval IS NULL) AND job_id=$jobid";
        //print "starttiem: $job[startdatetime] leadtime:".$dbRun['data']['last_plate_leadtime'].'<br>'.$sql."<br>";
        $dbPlates=dbselectmulti($sql);
        $lastplatecount=$dbPlates['numrows'];
        if($lastplatecount>1)
        {
            $outlast="<span style='color:red;font-weight:bold'>$lastplatecount SETS OUT</span>";
        } else {
            $outlast="<span style='color:green;font-weight:bold'>GOAL MET</span>";
        }
        
        $lastplatecountTotal=0;
        if($dbPlates['numrows']>0)
        {
            foreach($dbPlates['data'] as $temp)
            {
                if($temp['color'])
                {
                    $lastplatecountTotal=$lastplatecountTotal+4;
                } else {
                    $lastplatecountTotal++;
                }
            }
        }
        
        //how many pages out after this point?
        $sql="SELECT * FROM job_pages WHERE remake=0 AND (page_release>='$lastplatetime' OR page_release IS NULL) AND job_id=$jobid";
        //print "starttiem: $job[startdatetime] leadtime:".$dbRun['data']['last_plate_leadtime'].'<br>'.$sql."<br>";
        $dbPages=dbselectmulti($sql);
        $lastpagecount=$dbPages['numrows'];
    }
    if($xlast2!=0)
    {
        $last2platetime=date("Y-m-d H:i:s",strtotime($job['startdatetime']."-$xlast2 minutes"));
        //how many plates out after this point?
        $sql="SELECT * FROM job_plates WHERE remake=0 AND (plate_approval>='$last2platetime' OR plate_approval IS NULL) AND job_id=$jobid";
        $dbPlates=dbselectmulti($sql);
        $last2platecount=$dbPlates['numrows'];
        if($last2platecount>2)
        {
            $outlast2="<span style='color:red;font-weight:bold'>$last2platecount SETS OUT</span>";
        } else {
            $outlast2="<span style='color:green;font-weight:bold'>GOAL MET</span>";
        }
        
        $last2platecountTotal=0;
        if($dbPlates['numrows']>0)
        {
            foreach($dbPlates['data'] as $temp)
            {
                if($temp['color'])
                {
                    $last2platecountTotal=$last2platecountTotal+4;
                } else {
                    $last2platecountTotal++;
                }
            }
        }
        
        
        $sql="SELECT * FROM job_pages WHERE remake=0 AND (page_release>='$last2platetime' OR page_release IS NULL) AND job_id=$jobid";
        $dbPages=dbselectmulti($sql);
        $last2pagecount=$dbPages['numrows'];
    }
    if($xlast3!=0)
    {
        $last3platetime=date("Y-m-d H:i:s",strtotime($job['startdatetime']."-$xlast3 minutes"));
        //how many plates out after this point?
        $sql="SELECT * FROM job_plates WHERE remake=0 AND (plate_approval>='$last3platetime' OR plate_approval IS NULL) AND job_id=$jobid";
        $dbPlates=dbselectmulti($sql);
        $last3platecount=$dbPlates['numrows'];
        if($last3platecount>3)
        {
            $outlast3="<span style='color:red;font-weight:bold'>$last3platecount SETS OUT</span>";
        } else {
            $outlast3="<span style='color:green;font-weight:bold'>GOAL MET</span>";
        }
        
        $last3platecountTotal=0;
        if($dbPlates['numrows']>0)
        {
            foreach($dbPlates['data'] as $temp)
            {
                if($temp['color'])
                {
                    $last3platecountTotal=$last3platecountTotal+4;
                } else {
                    $last3platecountTotal++;
                }
            }
        }
        
        $sql="SELECT * FROM job_pages WHERE remake=0 AND (page_release>='$last3platetime' OR page_release IS NULL) AND job_id=$jobid";
        $dbPages=dbselectmulti($sql);
        $last3pagecount=$dbPages['numrows'];
    }
    if($xlast4!=0)
    {
        $last4platetime=date("Y-m-d H:i:s",strtotime($job['startdatetime']."-$xlast4 minutes"));
        //how many plates out after this point?
        $sql="SELECT * FROM job_plates WHERE remake=0 AND (plate_approval>='$last4platetime' OR plate_approval IS NULL) AND job_id=$jobid";
        $dbPlates=dbselectmulti($sql);
        $last4platecount=$dbPlates['numrows'];
        if($last4platecount>4)
        {
            $outlast4="<span style='color:red;font-weight:bold'>$last4platecount SETS OUT</span>";
        } else {
            $outlast4="<span style='color:green;font-weight:bold'>GOAL MET</span>";
        }
        $last4platecountTotal=0;
        if($dbPlates['numrows']>0)
        {
            foreach($dbPlates['data'] as $temp)
            {
                if($temp['color'])
                {
                    $last4platecountTotal=$last4platecountTotal+4;
                } else {
                    $last4platecountTotal++;
                }
            }
        }
        $sql="SELECT * FROM job_pages WHERE remake=0 AND (page_release>='$last4platetime' OR page_release IS NULL) AND job_id=$jobid";
        $dbPages=dbselectmulti($sql);
        $last4pagecount=$dbPages['numrows'];
    }
    if($xlast5!=0)
    {
        $last5platetime=date("Y-m-d H:i:s",strtotime($job['startdatetime']."-$xlast5 minutes"));
        //how many plates out after this point?
        $sql="SELECT * FROM job_plates WHERE remake=0 AND (plate_approval>='$last5platetime' OR plate_approval IS NULL) AND job_id=$jobid";
        $dbPlates=dbselectmulti($sql);
        $last5platecount=$dbPlates['numrows'];
        if($last5platecount>5)
        {
            $outlast5="<span style='color:red;font-weight:bold'>$last5platecount SETS OUT</span>";
        } else {
            $outlast5="<span style='color:green;font-weight:bold'>GOAL MET</span>";
        }
        
        $last5platecountTotal=0;
        if($dbPlates['numrows']>0)
        {
            foreach($dbPlates['data'] as $temp)
            {
                if($temp['color'])
                {
                    $last5platecountTotal=$last5platecountTotal+4;
                } else {
                    $last5platecountTotal++;
                }
            }
        }
        
        $sql="SELECT * FROM job_pages WHERE remake=0 AND (page_release>='$last5platetime' OR page_release IS NULL) AND job_id=$jobid";
        $dbPages=dbselectmulti($sql);
        $last5pagecount=$dbPages['numrows'];
    }
    if($xlast6!=0)
    {
        $last6platetime=date("Y-m-d H:i:s",strtotime($job['startdatetime']."-$xlast6 minutes"));
        //how many plates out after this point?
        $sql="SELECT * FROM job_plates WHERE remake=0 AND (plate_approval>='$last6platetime' OR plate_approval IS NULL) AND job_id=$jobid";
        $dbPlates=dbselectmulti($sql);
        $last6platecount=$dbPlates['numrows'];
        if($last6platecount>6)
        {
            $outlast6="<span style='color:red;font-weight:bold'>$last6platecount SETS OUT</span>";
        } else {
            $outlast6="<span style='color:green;font-weight:bold'>GOAL MET</span>";
        }
        
        $last6platecountTotal=0;
        if($dbPlates['numrows']>0)
        {
            foreach($dbPlates['data'] as $temp)
            {
                if($temp['color'])
                {
                    $last6platecountTotal=$last6platecountTotal+4;
                } else {
                    $last6platecountTotal++;
                }
            }
        }
        
        $sql="SELECT * FROM job_pages WHERE remake=0 AND (page_release>='$last6platetime' OR page_release IS NULL) AND job_id=$jobid";
        $dbPages=dbselectmulti($sql);
        $last6pagecount=$dbPages['numrows'];
    }
    if($xchaseplate!=0)
    {
        $chaseplatetime=date("Y-m-d H:i:s",strtotime($job['startdatetime']."-$xchaseplate minutes"));
        //how many plates out after this point?
        $sql="SELECT * FROM job_plates WHERE remake=1 AND current=1 AND (black_approval>='$chaseplatetime' OR black_approval IS NULL) AND job_id=$jobid";
        $dbPlates=dbselectmulti($sql);
        $chaseplatecount=$dbPlates['numrows'];
    }

    
    //calculate last remake page information
    $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND remake=1 ORDER BY page_release DESC LIMIT 1";
    $dbMaxPage=dbselectsingle($sql);
    $lastchasepage=$dbMaxPage['data']['page_number'].$dbMaxPage['data']['section_code'];
    $alastchasepagetime=$dbMaxPage['data']['page_release'];

    //calculate last page information
    $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND remake=0 ORDER BY workflow_receive DESC LIMIT 1";
    $dbMaxPage=dbselectsingle($sql);
    $lastpage=$dbMaxPage['data']['page_number'].$dbMaxPage['data']['section_code'];
    $alastpagetime=$dbMaxPage['data']['workflow_receive'];

    //calculate last color page information
    $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND remake=0 AND color=1 ORDER BY workflow_receive DESC LIMIT 1";
    $dbMaxColorPage=dbselectsingle($sql);
    $lastcolorpage=$dbMaxColorPage['data']['page_number'].$dbMaxColorPage['data']['section_code'];
    $alastcolorpagetime=$dbMaxColorPage['data']['workflow_receive'];

    //calculate last plate information
    $sql="SELECT low_page, black_approval, plate_approval, section_code FROM job_plates WHERE job_id=$jobid AND remake=0 ORDER BY plate_approval DESC LIMIT 1";
    $dbMaxPage=dbselectsingle($sql);
    $lastplate=$dbMaxPage['data']['low_page'].$dbMaxPage['data']['section_code'];
    $alastplatetime=$dbMaxPage['data']['black_approval'];

    $sql="SELECT * FROM job_stats WHERE id=$job[stats_id]";
    $dbStats=dbselectsingle($sql);
    $stats=$dbStats['data'];
    
    $counterstart=$stats['counter_start'];
    $counterstop=$stats['counter_stop'];
    $startupspoils=$stats['spoils_startup'];
    $draw=$job['draw'];
    $runningspoils=$stats['spoils_running'];
    $totalspoils=$stats['spoils_total'];
    $operator=$pressmen[$stats['job_pressoperator']];
    $paperDb=dbselectsingle("SELECT * FROM paper_types WHERE id=$job[papertype]");
    $paper=$paperDb['data']['common_name'];

    $targetstop=date("Y-m-d H:i:s",strtotime($stats['startdatetime_goal']."+$xrunlength minutes"));
    $actualstop=$stats['stopdatetime_actual'];
    if(strtotime($actualstop)>strtotime($stats['startdatetime_goal']."+$xrunlength minutes"))
    {
        $stopgoal="<span style='color:red;font-weight:bold;'>LATE: ".getMinVar(strtotime($stats['startdatetime_goal']."+$xrunlength minutes"),strtotime($actualstop),false)."</span>\n";
    } else {
        $stopgoal="<span style='color:green;font-weight:bold;'>ON TIME</span>";
    }
    
    if($stats['start_offset']>0)
    {
        $late="<span style='color:red;font-weight:bold;'>LATE: ".getMinVar(strtotime($stats['startdatetime_goal']),strtotime($stats['startdatetime_actual']),false)."</span>\n";
    } else {
        $late="<span style='color:green;font-weight:bold;'>ON TIME</span>";
    }
    //create the little summary box
    $summary="<span style='font-size:16px;font-weight:bold;'>".$dbPub['data']['pub_name'].' - '.$dbRun['data']['run_name'].' publishing on '.$job['pub_date']."</span><br>\n";
    $summary.="<table style='font-family:Trebuchet MS;font-size:12px;width:400px;border-style:solid;border-color:black;border-width:2px;'>\n";
    $summary.="<tr><th colspan=2 style='background-color:#CCC;color:black;'>Process/Procedure</th>
    <th style='background-color:#CCC;color:black;'>Target</th><th style='background-color:#CCC;color:black;'>Message</th></tr>\n";
    $summary.="<tr><td colspan=2>3 Plate Sets Remaining</td><td>".date("H:i:s",strtotime($last3platetime))."</td><td>$outlast3</td></tr>\n";
    $summary.="<tr><td colspan=2>2 Plate Sets Remaining</td><td>".date("H:i:s",strtotime($last2platetime))."</td><td>$outlast2</td></tr>\n";
    $summary.="<tr><td colspan=2>Last plate set received</td><td>".date("H:i:s",strtotime($lastplatetime))."</td><td>$outlast</td></tr>\n";
    $summary.="<tr><td colspan=2>Press Start</td><td>".date("H:i:s",strtotime($stats['startdatetime_goal']))."</td><td>$late</td></tr>\n";
    if($lastchasepage=='')
    {
        $summary.="<tr><td colspan=2>Last chase page released</td><td>No chase pages</td></tr>\n";
    } else {
        $summary.="<tr><td colspan=2>Last chase page released</td><td>$lastchasepage at ".date("H:i:s",strtotime($alastchasepagetime))."</td></tr>\n";
    }
    
    $summary.="<tr><td colspan=2>Press Stop</td><td>$targetstop</td><td>$stopgoal</td></tr>\n";
    $summary.="<tr><td>Press Notes</td><td colspan=3>".stripslashes($job['notes_press'])."</td></tr>\n";
    $summary.="</table>\n";
    //now the table
    $jobdetails="<small>JOB ID: $job[id]</small><br />\n";
    $jobdetails.="<table border=1 style='font-family:Trebuchet MS;font-size:12px;width:800px;border-style:solid;border-color:black;border-width:2px;'>\n";
    
    $jobdetails.="<tr><th colspan=6 style='font-size:16px;font-weight:bold;background-color:#CCC;color:black;'>".$dbPub['data']['pub_name'].' - '.$dbRun['data']['run_name'].' publishing on '.$job['pub_date']."</th></tr>\n";

    $jobdetails.="<tr><th colspan=3><b>Lead Operator:</b> $operator</th><th colspan=3><b>Base paper:</b> $paper</th></tr>\n";
    $jobdetails.="<tr><td colspan=3><b>Counter Start:</b> $counterstart</td><td colspan=3><b>Counter Stop:</b> $counterstop</td></tr>\n";
    $jobdetails.="<tr><td colspan=2><b>Startup Spoils:</b> $startupspoils</td><td colspan=2><b>Running Spoils:</b> $runningspoils</td><td colspan=2><b>Total Spoils:</b> $totalspoils</td></tr>\n";
    $jobdetails.="<tr><th colspan=3 style='font-weight:bold;background-color:#CCC;color:black;'>Press Times</th><th style='font-weight:bold;background-color:#CCC;color:black;'>Goal</th><th colspan=1 style='font-weight:bold;background-color:#CCC;color:black;'>Actual</th><th colspan=1 style='font-weight:bold;background-color:#CCC;color:black;'>Variance</th></tr>\n";
    //first some info about the job
    $jobdetails.="<tr><td colspan=3><b>Start Time</b></td><td>$stats[startdatetime_goal]</td><td>$stats[startdatetime_actual]</td><td>".getMinVar(strtotime($stats['startdatetime_goal']),strtotime($stats['startdatetime_actual']),true)."</td></tr>\n";
    $jobdetails.="<tr><td colspan=3><b>Good Copy</b></td><td colspan=3>".$stats['goodcopy_actual']."</td></tr>\n";
    $jobdetails.="<tr><td colspan=3><b>Stop Time</b></td><td>$targetstop</td><td>$stats[stopdatetime_actual]</td><td>".getMinVar(strtotime($stats['startdatetime_goal']."+$xrunlength minutes"),strtotime($stats['stopdatetime_actual']),true)."</td></tr>\n";
    $jobdetails.="<tr><td colspan=3><b>Draw Request</b></td><td >$job[draw]</td><td>$stats[gross]</td><td>$stats[spoils_total]</td></tr>\n";
    $jobdetails.="</tr>\n"; 
    
    $jobdetails.="<tr><th colspan=2 style='background-color:#CCC;color:black;'><b>Page & Plate Flow</b></th><th style='background-color:#CCC;color:black;'><b>Value</b></th>";
    $jobdetails.="</tr>\n"; 
    $jobdetails.="<td colspan=2><b>Last Plate</b></td><td>$lastplate at $alastplatetime</td>\n";
    $jobdetails.="</tr>\n";

    $jobdetails.="<tr>";
    $jobdetails.="<td colspan=2><b>Last Page (received at workflow)</b></td><td>$lastpage at $alastpagetime</td>\n";
    $jobdetails.="</tr>\n";

    $jobdetails.="<tr>";
    $jobdetails.="<td colspan=2><b>Last Color Page</b></td><td>$lastcolorpage at $alastcolorpagetime</td>\n";
    $jobdetails.="</tr>\n";
    $jobdetails.="</table>\n";
    
    
    //starting new table here
    $jobdetails.="<table border=1 style='font-family:Trebuchet MS;font-size:12px;width:800px;border-style:solid;border-color:black;border-width:2px;'>\n";
    
    $jobdetails.="<tr><th colspan=2 style='background-color:#CCC;color:black;'><b>Last Page & Plate times</b></th><th style='background-color:#CCC;color:black;'><b>Plate Sets Remaining</b></th>
        <th style='background-color:#CCC;color:black;'><b>Plates Remaining</b></th><th style='background-color:#CCC;color:black;'><b>Pages Remaining</b></th></tr>\n";
    
    $jobdetails.="<tr>";
    $jobdetails.="<td colspan=2><b>At press start time</b></td><td>$platesatstart</td>
    <td>$platesatstartTotal</td><td>$pagesatstart</td>\n";
    $jobdetails.="</tr>\n";
    
    $jobdetails.="<tr>";
    $jobdetails.="<td colspan=2><b>5 minutes before start</b></td><td>$platesatstartminus5</td>
    <td>$platesatstartTotalMinus5</td><td>$pagesatstartminus5</td>\n";
    $jobdetails.="</tr>\n";
    
    $jobdetails.="<tr>";
    $jobdetails.="<td colspan=2><b>$xlast2 minutes before start</b></td><td>$last2platecount</td>
    <td>$last2platecountTotal</td><td>$last2pagecount</td>\n";
    $jobdetails.="</tr>\n";
    
    $jobdetails.="<tr>";
    $jobdetails.="<td colspan=2><b>$xlast3 minutes before start</b></td><td>$last3platecount</td>
    <td>$last3platecountTotal</td><td>$last3pagecount</td>\n";
    $jobdetails.="</tr>\n";

    $jobdetails.="<tr>";
    $jobdetails.="<td colspan=2><b>$xlast4 minutes before start</b></td><td>$last4platecount</td>
    <td>$last4platecountTotal</td><td>$last4pagecount</td>\n";
    $jobdetails.="</tr>\n";

    $jobdetails.="<tr>";
    $jobdetails.="<td colspan=2><b>$xlast5 minutes before start</b></td><td>$last5platecount</td>
    <td>$last5platecountTotal</td><td>$last5pagecount</td>\n";
    $jobdetails.="</tr>\n";

    $jobdetails.="<tr>";
    $jobdetails.="<td colspan=2><b>$xlast6 minutes before start</b></td><td>$last6platecount</td>
    <td>$last6platecountTotal</td><td>$last6pagecount</td>\n";
    $jobdetails.="</tr>\n";
    
    $jobdetails.="<tr>";
    $jobdetails.="<td colspan=2><b>Chase plates $xchaseplate minutes after start</b></td>
    <td>$chaseplatecount</td><td></td><td></td>\n";
    $jobdetails.="</tr>\n";
    $jobdetails.="</table>\n";
    
    
    //starting new table here
    
    $jobdetails.="<table border=1 style='font-family:Trebuchet MS;font-size:12px;width:800px;border-style:solid;border-color:black;border-width:2px;'>\n";
    $jobdetails.="<tr><th style='font-weight:bold;background-color:#CCC;color:black;'>Plate Details</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Pages on plate</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Last page for<br>plate released</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Waiting Approval</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Approved</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Black at CTP</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Wait Time</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Process Time</th>
    </tr>\n";
    $sql="SELECT * FROM job_plates WHERE job_id=$job[id] AND remake=0 ORDER BY section_code ASC, low_page ASC";
    $dbPlates=dbselectmulti($sql);
    if ($dbPlates['numrows']>0)
    {
        foreach($dbPlates['data'] as $plate)
        {
            //get all the pages that are on this plate
            $pid=$plate['id'];
            $sql="SELECT DISTINCT(page_number), color, ftp_receive, page_release FROM job_pages WHERE plate_id=$pid AND page_number<>0 AND remake=0 ORDER BY page_number ASC";
            $dbPlatePages=dbselectmulti($sql);
            $platecolor='B/W';
            $tptime=strtotime("1/1/71");
            $trtime=strtotime("1/1/71");
            $ppages="";
            if ($dbPlatePages['numrows']>0)
            {
                foreach ($dbPlatePages['data'] as $platepage)
                {
                    if(strtotime($platepage['ftp_receive'])>$tptime)
                    {
                        $tptime=strtotime($platepage['ftp_receive']);
                    }
                    if(strtotime($platepage['page_release'])>$trtime)
                    {
                        $trtime=strtotime($platepage['page_release']);
                    }
                    $ppages.=" ".$platepage['page_number'];
                    if($platepage['color']){
                        $platecolor='Color';
                    }
                } 
            } else {
                //have an oddness, lets look for a remake plate
                $backupsql="SELECT * FROM job_plates WHERE job_id=$job[id] AND remake=1 
                AND pub_id='$plate[pub_id]' AND pub_date='$plate[pub_date]' 
                AND section_code='$plate[section_code]' AND low_page='$plate[low_page]'";
                $dbBackup=dbselectsingle($backupsql);
                if($dbBackup['numrows']>0)
                {
                    $pid=$dbBackup['data']['id'];
                
                    $sql="SELECT DISTINCT(page_number), color, ftp_receive, page_release FROM job_pages WHERE plate_id=$pid AND page_number<>0 AND remake=0 ORDER BY page_number ASC";
                    $dbPlatePages=dbselectmulti($sql);
                    $platecolor='Black';
                    if ($dbPlatePages['numrows']>0)
                    {
                        foreach ($dbPlatePages['data'] as $platepage)
                        {
                            if(strtotime($platepage['ftp_receive'])>$tptime)
                            {
                                $tptime=strtotime($platepage['ftp_receive']);
                            }
                            if(strtotime($platepage['page_release'])>$trtime)
                            {
                                $trtime=strtotime($platepage['page_release']);
                            }
                            $ppages.=" ".$platepage['page_number'];
                            if($platepage['color']){
                                $platecolor='Full Color';
                            }
                        }
                    }
                }                       
            }
            if($plate['plate_waiting']!='')
            {
                $pw=date("H:i:s",strtotime($plate['plate_waiting']));
                print "Process time calculated with ".date("H:i:s",$tptime).' and '.$plate['plate_waiting']."<br>";
                $processtime=getMinVar($tptime,strtotime($plate['plate_waiting']),true);
            } else {
                $pw='n/a';
                $processtime='n/a';
            }
            if($plate['plate_approval']!='')
            {
                $pa=date("H:i:s",strtotime($plate['plate_approval']));
                $await=getMinVar(strtotime($plate['plate_waiting']),strtotime($plate['plate_approval']),true);
            } else {
                $pa='n/a';
            }
            if($plate['black_ctp']!='')
            {
                $bc=date("H:i:s",strtotime($plate['black_ctp']));
            } else {
                $bc='n/a';
            }
            if($plate['black_receive']!='')
            {
                $br=date("H:i:s",strtotime($plate['black_receive']));
            } else {
                $br='n/a';
            }
            if($plate['cyan_receive']!='')
            {
                $cr=date("H:i:s",strtotime($plate['cyan_receive']));
            } else {
                $cr='n/a';
            }
            $jobdetails.= "<tr><td><b>$plate[section_code] - $plate[low_page] $platecolor</b></td>
            <td>$ppages</td>
            <td>".date("H:i:s",$trtime)."</td>
            <td>".$pw."</td>
            <td>".$pa."</td>
            <td>".$bc."</td>
            <td>".$await."</td>
            <td>".$processtime."</td>
            </tr>\n";
        }
    } else {
        $jobdetails.="<tr><th colspan=6>No plates defined for this run</th></tr>\n";
    }
    
    
    $jobdetails.="<tr><th style='font-weight:bold;background-color:#CCC;color:black;'>Page Details</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Color</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>FTP Receive</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Workflow Receive</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Page Ripped</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Page Release</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Color Release</th>
    </tr>\n"; 
    $sql="SELECT * FROM job_pages WHERE job_id=$job[id] AND remake=0 ORDER BY section_code ASC, page_number ASC";
    $dbPages=dbselectmulti($sql);
    if ($dbPages['numrows']>0)
    {
         foreach($dbPages['data'] as $page)
         {
             if($page['page_release']!='')
            {
                $pr=date("H:i:s",strtotime($page['page_release']));
            } else {
                $pr='n/a';
            }
            if($page['color_release']!='')
            {
                $cr=date("H:i:s",strtotime($page['color_release']));
            } else {
                $cr='n/a';
            }
            if($page['ftp_receive']!='')
            {
                $fr=date("H:i:s",strtotime($page['ftp_receive']));
            } else {
                $fr='n/a';
            }
            if($page['workflow_receive']!='')
            {
                $wr=date("H:i:s",strtotime($page['workflow_receive']));
            } else {
                $wr='n/a';
            }
            if($page['page_ripped']!='')
            {
                $par=date("H:i:s",strtotime($page['page_ripped']));
            } else {
                $par='n/a';
            }
            if($page['color']){$color='color';}else{$color='b/w';}
            $jobdetails.= "<tr><td><b>$page[section_code] - $page[page_number]</b></td>
            <td>".$color."</td>
            <td>".$fr."</td>
            <td>".$wr."</td>
            <td>".$par."</td>
            <td>".$pr."</td>
            <td>".$cr."</td>
            </tr>\n";
         }
    } else {
        $jobdetails.= "<tr><th colspan=6>No pages defined for this run</th></tr>\n";
    }
    
    $jobdetails.="<tr><th style='font-weight:bold;background-color:#CCC;color:black;'>Page Remakes</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Page Release</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Color Release</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>FTP Receive</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Workflow Receive</th>
    <th style='font-weight:bold;background-color:#CCC;color:black;'>Page Ripped</th>
    </tr>\n";
    $sql="SELECT * FROM job_pages WHERE job_id=$job[id] AND remake=1 ORDER BY section_code ASC, page_number ASC";
    $dbPages=dbselectmulti($sql);
    if ($dbPages['numrows']>0)
    {
         foreach($dbPages['data'] as $page)
         {
             if($page['page_release']!='')
            {
                $pr=date("H:i:s",strtotime($page['page_release']));
            } else {
                $pr='n/a';
            }
            if($page['color_release']!='')
            {
                $cr=date("H:i:s",strtotime($page['color_release']));
            } else {
                $cr='n/a';
            }
            if($page['ftp_receive']!='')
            {
                $fr=date("H:i:s",strtotime($page['ftp_receive']));
            } else {
                $fr='n/a';
            }
            if($page['workflow_receive']!='')
            {
                $wr=date("H:i:s",strtotime($page['workflow_receive']));
            } else {
                $wr='n/a';
            }
            if($page['page_ripped']!='')
            {
                $par=date("H:i:s",strtotime($page['page_ripped']));
            } else {
                $par='n/a';
            }
            $jobdetails.= "<tr><td><b>$page[section_code] - $page[page_number]</b></td>
            <td>".$pr."</td>
            <td>".$cr."</td>
            <td>".$fr."</td>
            <td>".$wr."</td>
            <td>".$par."</td>
            </tr>\n";
         }
    } else {
        $jobdetails.= "<tr><th colspan=6>No remakes for this run</th></tr>\n";
    }
    
    if ($enableJobStops)
    {
        $jobdetails.="<tr><th colspan=6 style='font-size:10pt;font-weight:bold;background-color:#CCC;color:black;'>Job stops</th></tr>\n";
        $sql="SELECT A.*, B.stop_name FROM job_stops A, stop_codes B WHERE A.job_id=$job[id] AND A.stop_code=B.id ORDER BY A.stop_datetime DESC";
        $dbStops=dbselectmulti($sql);
                        if ($dbStops['numrows']>0)
                        {
                        foreach($dbStops['data'] as $stop)
            {
                        $stoptime=date("H:i",strtotime($stop['stop_datetime']));
                        $restarttime=date("H:i",strtotime($stop['stop_restartdatetime']));
                        $downtime=($stop['stop_downtime']/60)." minutes";
                $name=$stop['stop_name'];
                        $jobdetails.= "<tr><td colspan=3><b>$name</b></td>
                <td>$stoptime</td>
                <td>$restarttime</td>
                <td>$downtime</td>
                        </tr>\n";
            }
        } else {
            $jobdetails.= "<tr><th colspan=4>No stops defined for this run</th></tr>\n";
           }
       }
       $jobdetails.= "</table>\n";
       $details['job']=$jobdetails;
       $details['summary']=$summary;
       return $details;
}

function getInserterJobDetails($job)
{
    $jobdetails="<br />\n<br />\nInserter JOB goes here<br />\n";
    return $jobdetails;
}

function getMinVar($startdatetime,$enddatetime,$concise=false)
{
    $timePassed = $enddatetime-$startdatetime; //time passed in seconds
    if($timePassed<0)
    {
        $minus=true;
        $timePassed=-$timePassed;
    }
    // Minute == 60 seconds
    // Hour == 3600 seconds
    // Day == 86400
    // Week == 604800
    $elapsedString = "";
    if($timePassed > 604800)
    {
    $weeks = floor($timePassed / 604800);
    $timePassed -= $weeks * 604800;
    $elapsedString = $weeks." weeks, ";
    }
    if($timePassed > 86400)
    {
    $days = floor($timePassed / 86400);
    $timePassed -= $days * 86400;
    $elapsedString .= $days." days, ";
    }
    if($timePassed > 3600)
    {
        $hours = floor($timePassed / 3600);
        $timePassed -= $hours * 3600;
        if(strlen($hours)==1){$hours="0".$hours;}
        if($concise)
        {
            $elapsedString .= $hours.":";
        } else {
            $elapsedString .= $hours." hours, ";
        }
    }
    if($timePassed > 60)
    {
        $minutes = floor($timePassed / 60);
        $timePassed -= $minutes * 60;
        if(strlen($minutes)==1){$minutes="0".$minutes;}
        if($concise)
        {
            $elapsedString .= $minutes.":"; 
        } else {
            $elapsedString .= $minutes." minutes, "; 
        }
        
    }
    if(strlen($timePassed)==1){$timePassed="0".$timePassed;}
    if($concise)
    {
        $elapsedString .= $timePassed; 
    } else {
        $elapsedString .= $timePassed." seconds";
    }
    if($minus)
    {
        $elapsedString="-".$elapsedString;
    }
    return $elapsedString;
}
 
 
function getJobDetails_older($job)
{
    global $enableJobStops, $enableBenchmarks, $producttypes, $pressmen;
    $jobid=$job['id'];
    //here we display the job data and all it's stats, we'll use a table layout
    //we'll need to get lots of other data pieces at this point
    $sql="SELECT pub_name FROM publications WHERE id=$job[pub_id]";
    $dbPub=dbselectsingle($sql);
    
    $sql="SELECT run_name FROM publications_runs WHERE id=$job[run_id]";
    $dbRun=dbselectsingle($sql);
    
    $sql="SELECT A.benchmark_name, A.benchmark_category, A.benchmark_order, B.* FROM benchmarks A, job_benchmarks B WHERE A.id=B.benchmark_id AND B.job_id=$job[id] ORDER BY A.benchmark_category, A.benchmark_order ASC";
    $dbBenchmarks=dbselectmulti($sql);
    $benchmarks=$dbBenchmarks['data'];
    
    $sql="SELECT * FROM job_stats WHERE job_id=$job[id]";
    $dbStats=dbselectsingle($sql);
    $stats=$dbStats['data'];
    //print_r($stats);
    //first, print out the name of the publication in biggish letters
    
    //now the table
    $jobdetails="<small>JOB ID: $job[id]</small><br />\n";
    $jobdetails.="<table border=1 style='style='font-family:arial;font-size:10pt;background-color:black;width:650px;border-style:solid;border-color:black;border-width:2px;
'>\n";
    $counterstart=$stats['counter_start'];
    $counterstop=$stats['counter_stop'];
    $startupspoils=$stats['spoils_startup'];
    $draw=$job['draw'];
    $runningspoils=$stats['spoils_running'];
    $totalspoils=$stats['spoils_total'];
    $operator=$pressmen[$stats['job_pressoperator']];
    $paperDb=dbselectsingle("SELECT * FROM paper_types WHERE id=$job[papertype]");
    $paper=$paperDb['data']['common_name'];
    $jobdetails.="<tr><th colspan=4 style='font-size:16pt;font-weight:bold;background-color:black;color:white;'>".$dbPub['data']['pub_name']."</th></tr>\n";
    $jobdetails.="<tr><th colspan=2 style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Job Name: ".$dbRun['data']['run_name']."</th>
    <th colspan=2 style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Pub date: $job[pub_date]</th></tr>";
    $jobdetails.="<tr><th colspan=2 style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Lead Operator: $operator</th>
    <th colspan=2 style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Base paper: $paper</th></tr>\n"; 
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Counter Start: $counterstart</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Counter Stop: $counterstop</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Startup Spoils: $startupspoils</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Running Spoils: $runningspoils</td></tr>\n";
    $jobdetails.="<tr><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'></th><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Goal</th><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Actual</th><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Variance</th></tr>\n"; 
    //first some info about the job
    $jobdetails.="<tr><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Start Time</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[startdatetime_goal]</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[startdatetime_actual]</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[start_offset]</td></tr>\n";
    $jobdetails.="<tr><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Good Copy</td><td colspan=3 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>".$stats['goodcopy_actual']."</td></tr>\n";
    $jobdetails.="<tr><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Stop Time</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[stopdatetime_goal]</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[stopdatetime_actual]</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[finish_offset]</td></tr>\n";
    $jobdetails.="<tr><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Draw Request</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$job[draw]</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[gross]</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[spoils_total]</td></tr>\n";
    
    //now just a bunch of stats
    $jobdetails.="<tr><th colspan=2 style='font-size:10pt;font-weight:bold;background-color:black;color:white;'></th><th colspan=2 style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Value</th></tr>\n"; 
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Waste %</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[waste_percent]%</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Downtime</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[total_downtime] minutes</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Running Time</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[run_time] minutes</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Scheduled Running Time</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[sched_runtime] minutes</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Gross average speed</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[run_speed] copies/hr</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Net average speed</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[good_runspeed] copies/hr</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Total Pressman</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[job_pressman_count] pressman</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Pages BW</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[pages_bw] pages (broadsheet eqv.)</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Pages Color</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[pages_color] pages (broadsheet eqv.)</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Plates BW</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[plates_bw] plates</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Plates Color</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[plates_color] plates</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Plates remade</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[plates_remake] plates</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Plates wasted</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[plates_waste] plates</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Last Page</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[last_page]</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Last Color Page</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[last_colorpage]</td></tr>\n";
    $jobdetails.="<tr><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>Last Plate</td><td colspan=2 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stats[last_plate]</td></tr>\n";
    
    $jobdetails.= "<tr><td colspan=4 style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:center;'>Section Information</td></tr>\n";
    //get sectioninfo
    $sql="SELECT * FROM jobs_sections WHERE job_id=$job[id]";
    $dbSections=dbselectsingle($sql);
    if ($dbSections['numrows']>0)
    {
        $jobdetails.= "<tr><td colspan=4>\n";
        $sections=$dbSections['data'];
        $totalpages=0;
        //ok, lets get how many color/bw pages by section
        //section1
        $section1_overrun=$sections['section1_overrun'];
        $section1_name=$sections['section1_name'];
        $section1_code=$sections['section1_code'];
        $section1_totalpages=0;
        $section1_colorpages=0;
        $section1_bwpages=0;
        $section1_format=$producttypes[$sections['section1_producttype']];
        $section1_lead=$leadtypes[$sections['section1_leadtype']];
        if ($sections['section1_gatefold']){$section1_gate='Has gatefold';}else{$section1_gate='';}
        if ($sections['section1_doubletruck']){$section1_double='Has doubletruck';}else{$section1_double='';}
        $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND section_code='$sections[section1_code]' AND version=1 ORDER BY page_number ASC";
        $dbPages=dbselectmulti($sql);
        if ($dbPages['numrows']>0)
        {
            foreach($dbPages['data'] as $page)
            {
                $section1_totalpages++;
                if ($page['color']){$section1_colorpages++;}else{$section1_bwpages++;}
            }   
        }
        $totalpages+=$section1_totalpages;
        //section2
        $section2_overrun=$sections['section2_overrun'];
        $section2_name=$sections['section2_name'];
        $section2_code=$sections['section2_code'];
        $section2_totalpages=0;
        $section2_colorpages=0;
        $section2_bwpages=0;
        $section2_format=$producttypes[$sections['section2_producttype']];
        $section2_lead=$leadtypes[$sections['section2_leadtype']];
        if ($sections['section2_gatefold']){$section2_gate='Has gatefold';}else{$section2_gate='';}
        if ($sections['section2_doubletruck']){$section2_double='Has doubletruck';}else{$section2_double='';}
        $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND section_code='$sections[section2_code]' AND version=1 ORDER BY page_number ASC";
        $dbPages=dbselectmulti($sql);
        if ($dbPages['numrows']>0)
        {
            foreach($dbPages['data'] as $page)
            {
                $section2_totalpages++;
                if ($page['color']){$section2_colorpages++;}else{$section2_bwpages++;}
            }   
        }
        $totalpages+=$section2_totalpages;
        //section3
        $section3_overrun=$sections['section3_overrun'];
        $section3_name=$sections['section3_name'];
        $section3_code=$sections['section3_code'];
        $section3_totalpages=0;
        $section3_colorpages=0;
        $section3_bwpages=0;
        $section3_format=$producttypes[$sections['section3_producttype']];
        $section3_lead=$leadtypes[$sections['section3_leadtype']];
        if ($sections['section3_gatefold']){$section3_gate='Has gatefold';}else{$section3_gate='';}
        if ($sections['section3_doubletruck']){$section3_double='Has doubletruck';}else{$section3_double='';}
        $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND section_code='$sections[section3_code]' AND version=1 ORDER BY page_number ASC";
        $dbPages=dbselectmulti($sql);
        if ($dbPages['numrows']>0)
        {
            foreach($dbPages['data'] as $page)
            {
                $section3_totalpages++;
                if ($page['color']){$section3_colorpages++;}else{$section3_bwpages++;}
            }   
        }
        $totalpages+=$section3_totalpages;
        $jobdetails.=  "<table>\n";
        $jobdetails.=  "<tr><th>Section Name</th><th>Letter</th><th>Format</th><th>Pages</th><th>Color</th><th>BW</th><th>Overrun</th></tr>\n";
        $jobdetails.=  "<tr>";
        $jobdetails.=  "<td>$section1_name</td>";
        $jobdetails.=  "<td>$section1_code</td>";
        $jobdetails.=  "<td>$section1_format</td>";
        $jobdetails.=  "<td>$section1_totalpages</td>";
        $jobdetails.=  "<td>$section1_colorpages</td>";
        $jobdetails.=  "<td>$section1_bwpages</td>";
        $jobdetails.=  "<td>$section1_overrun</td>";
        $jobdetails.=  "</tr>\n";
        
        $jobdetails.=  "<tr>";
        $jobdetails.=  "<td>$section2_name</td>";
        $jobdetails.=  "<td>$section2_code</td>";
        $jobdetails.=  "<td>$section2_format</td>";
        $jobdetails.=  "<td>$section2_totalpages</td>";
        $jobdetails.=  "<td>$section2_colorpages</td>";
        $jobdetails.=  "<td>$section2_bwpages</td>";
        $jobdetails.=  "<td>$section2_overrun</td>";
        $jobdetails.=  "</tr>\n";
        
        $jobdetails.=  "<tr>";
        $jobdetails.=  "<td>$section3_name</td>";
        $jobdetails.=  "<td>$section3_code</td>";
        $jobdetails.=  "<td>$section3_format</td>";
        $jobdetails.=  "<td>$section3_totalpages</td>";
        $jobdetails.=  "<td>$section3_colorpages</td>";
        $jobdetails.=  "<td>$section3_bwpages</td>";
        $jobdetails.=  "<td>$section3_overrun</td>";
        $jobdetails.=  "</tr>\n";
        
        $jobdetails.=  "<tr>";
        $jobdetails.=  "<td>Totals:</td>";
        $jobdetails.=  "<td></td>";
        $jobdetails.=  "<td></td>";
        $jobdetails.=  "<td>$totalpages</td>";
        $jobdetails.=  "<td>".($section1_colorpages+$section2_colorpages+$section3_colorpages)."</td>";
        $jobdetails.=  "<td>".($section1_bwpages+$section2_bwpages+$section3_bwpages)."</td>";
        $jobdetails.=  "<td>----</td>";
        $jobdetails.= "</tr>\n";
        
        $jobdetails.= "</table>\n";
        $jobdetails.= "</td></tr>\n";
    } else {
        $jobdetails.= "<tr><td colspan=4>No sections defined at this time</td></tr>\n";
    }
    
    
    
    
    if ($enableBenchmarks)
    {
        if ($dbBenchmarks['numrows']>0)
        {
            foreach($benchmarks as $benchmark)
            {
                $bname=$benchmark['benchmark_name'];
                if ($benchmark['benchmark_type']=='time')
                {
                    $goal=date("H:i",strtotime($benchmark['benchmark_goal_time']));
                    $actual=date("H:i",strtotime($benchmark['benchmark_actual_time']));
                    $difference=date("H:i",strtotime($benchmark['benchmark_difference']));
                } else {
                    $goal=$benchmark['benchmark_goal_number'];
                    $actual=$benchmark['benchmark_actual_number'];
                    $difference=$benchmark['benchmark_difference'];
                }
                $jobdetails.="<tr><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$bname</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$goal</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$actual</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$difference</td></tr>\n";
            }
        }
    }
    $jobdetails.="<tr><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Plate Details</th><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Approved</th><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Black out of bender</th><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Color out of bender</th></tr>\n";
    $sql="SELECT * FROM job_plates WHERE job_id=$job[id] ORDER BY section_code ASC, low_page ASC";
    $dbPlates=dbselectmulti($sql);
    if ($dbPlates['numrows']>0)
    {
        foreach($dbPlates['data'] as $plate)
        {
            $jobdetails.= "<tr><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$plate[section_code] - $plate[low_page]</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$plate[black_approval]</td>
            <td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$plate[black_receive]</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$plate[cyan_receive]</td></tr>\n";
        }
    } else {
        $jobdetails.="<tr><th colspan=4>No plates defined for this run</th></tr>\n";
    }
    $jobdetails.="<tr><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Page Details</th><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Page Release</th><th style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Color Release</th></tr>\n";
    $sql="SELECT * FROM job_pages WHERE job_id=$job[id] AND version=1 ORDER BY section_code ASC, page_number ASC";
    $dbPages=dbselectmulti($sql);
    if ($dbPages['numrows']>0)
    {
        foreach($dbPages['data'] as $page)
        {
            $jobdetails.= "<tr><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$page[section_code] - $page[page_number]</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$page[page_release]</td><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$page[color_release]</td></tr>\n";
        }
    } else {
        $jobdetails.= "<tr><th colspan=4>No pages defined for this run</th></tr>\n";
    }
    
    if ($enableJobStops)
    {
        $jobdetails.="<tr><th colspan=4 style='font-size:10pt;font-weight:bold;background-color:black;color:white;'>Job stops</th></tr>\n";
        $sql="SELECT A.*, B.stop_name FROM job_stops A, stop_codes B WHERE A.job_id=$job[id] AND A.stop_code=B.id ORDER BY A.stop_datetime DESC";
        $dbStops=dbselectmulti($sql);
        if ($dbStops['numrows']>0)
        {
            foreach($dbStops['data'] as $stop)
            {
                $stoptime=date("H:i",strtotime($stop['stop_datetime']));
                $restarttime=date("H:i",strtotime($stop['stop_restartdatetime']));
                $downtime=($stop['stop_downtime']/60)." minutes";
                $name=$stop['stop_name'];
                $jobdetails.= "<tr><td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$name</td>
                <td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$stoptime</td>
                <td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$restarttime</td>
                <td style='font-size:10pt;background-color:#ccc;color:black;border-style:solid;border-width:1px;text-align:right;'>$downtime</td>
                </tr>\n";
            }
        } else {
            $jobdetails.= "<tr><th colspan=4>No stops defined for this run</th></tr>\n";
        }
    }
    $jobdetails.= "</table>\n";
    
    //now add any notes about the job
    $jobdetails.="<br /><b>Job Notes:</b><br />".$job['notes_job'];
    $jobdetails.="<br /><br /><b>Press Notes:</b><br />".$job['notes_press'];
    $jobdetails.="<br /><br /><hr><br />\n";
    return $jobdetails; 
} 


function getInserterJobDetails_older($job)
{
    $jobdetails="<br />\n<br />\nInserter JOB goes here<br />\n";
    return $jobdetails;
}

function getMinVar_older($startdatetime,$enddatetime)
{
    $timePassed = $enddatetime-$startdatetime; //time passed in seconds
    // Minute == 60 seconds
    // Hour == 3600 seconds
    // Day == 86400
    // Week == 604800
    $elapsedString = "";
    if($timePassed > 604800)
    {
    $weeks = floor($timePassed / 604800);
    $timePassed -= $weeks * 604800;
    $elapsedString = $weeks." weeks, ";
    }
    if($timePassed > 86400)
    {
    $days = floor($timePassed / 86400);
    $timePassed -= $days * 86400;
    $elapsedString .= $days." days, ";
    }
    if($timePassed > 3600)
    {
    $hours = floor($timePassed / 3600);
    $timePassed -= $hours * 3600;
    $elapsedString .= $hours." hours, ";
    }
    if($timePassed > 60)
    {
    $minutes = floor($timePassed / 60);
    $timePassed -= $minutes * 60;
    $elapsedString .= $minutes." minutes, ";
    }
    $elapsedString .= $timePassed." seconds";

    return $elapsedString;
}

$Page->footer();
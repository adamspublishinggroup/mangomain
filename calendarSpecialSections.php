<?php
//core calendar file
$pageFluid=true;
include("includes/boot.php") ;
if($_GET['start'])
{
    $defaultDate = addslashes($_GET['start']);
} else {
    $defaultDate = date("Y-m").'-01';
}
?>
<script>
$(document).ready(function() {
    
    var lastClickTime = 0;
    var lastClickID = 0;
    
    var calView = 'month';
    
    if($( window ).width()<800)
    {
        calView = 'agendaDay';   
    }
    
    var currentJobID=0;
    $('#calendar').fullCalendar({
        themeSystem: 'bootstrap3',
        defaultDate: $.fullCalendar.moment('<?php echo $defaultDate ?>'),
        defaultView: calView,
        header: {
            left: '',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        editable: true,
        height: 700,
        events: <?php
            fetchEvents();          
            ?>,
        dblclick: function(event, jsEvent) {
            /*
            alert('You doubleclicked! Event: ' + event.title+' and id='+event.id);
            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
            alert('View: ' + view.name);
            */
            
        },
        eventClick: function(event, jsEvent, view) {
            currentJobID=event.id;
            var d = new Date();
                
            if(lastClickID == event.id)
            {
                var clickPause = d.getTime() - lastClickTime;
                if( clickPause < 500)
                {
                    window.open('specialSections.php?id='+event.id,'Press Job Editor',"scrollbars=0, resizeable=1, width=940, height=850");
                } else {
                    lastClickTime = d.getTime();
                }
            } else {
                lastClickID = event.id;
                lastClickTime = d.getTime();
                return false;
            }
            
        },
        eventMouseover: function( event, jsEvent, view ) { 
            $(this).css('border-color','red');
        },
        eventMouseout: function( event, jsEvent, view ) { 
            $(this).css('border-color',event.borderColor);
            lastClickID = 0;
            lastClickTime = 0;
        },
        
        dayClick: function(date, jsEvent, view) {
            /*           
            alert('Clicked on: ' + date.format());
            alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

            alert('Current view: ' + view.name);
            */
            // change the day's background color just for fun
           if(view.name=='month')
           {
               $('#calendar').fullCalendar(
                   'changeView','agendaDay' 
               );
                $('#calendar').fullCalendar(
                   'gotoDate', date 
               );
                
           }
        },

        eventRender: function(event, element) {
            element.attr('data-event-id', event.id);
            element.addClass('context-menu');
            element.find('.fc-time').prepend("<span id='details"+event.id+"' class='jobdetails' data-id='"+event.id+"' style='float:right;'></span><br />").append(" | <b>Pub Date: </b>"+event.pubDate);
            element.find('.fc-content').append(event.description);
            element.qtip({
                content: {
                    text: event.tooltip,
                    title: 'Section Details',
                    button: true
                }, 
                position: {
                        my: 'left center',
                        at: 'right center'
                    },
                style: {
                    widget: true, // Optional shadow...
                    def: false,
                    tip: 'left center' // Tips work nicely with the styles too!
                },
                show: {
                    event: 'click',
                    solo: true // Only show one tooltip at a time
                },
                hide: {
                    event: 'click mouseleave unfocus'
                }
            });
             
        }                 
    });

$( window ).resize(function() {
  
    if($( window ).width()<800)
    {
        calView = 'agendaDay';   
    } else {
        calView = 'month';
    }
    $('#calendar').fullCalendar('changeView', calView);  
});
  
           
$.contextMenu({
    selector: '.context-menu',
    items: {
        edit: {
            name: "Edit Special Section",
            callback: function(key, opt){
                var id = opt.$trigger[0].dataset.eventId;
                window.open('specialSections.php?action=edit&id='+id,'Special Section',"scrollbars=0, resizeable=1, width=940, height=850");
            }
        },
        print: {
            name: "Print Ticket",
            callback: function(key, opt){
                var id = opt.$trigger[0].dataset.eventId;
                window.open('specialSections.php?action=view&jobid='+id,'Special Section Ticket',"scrollbars=0, resizeable=1, width=750, height=640");
            }
        }
    }
}); 
    
});
</script>

<div id='loading' style='display:none'>loading...</div>
<div id='quickjump' style='z-index:30000;'>
<?php
$startdate=date("Y-m-d");
$baseDay = $startdate;
while (date("w",strtotime($startdate))!=0)
{
    $startdate=date("Y-m-d",strtotime($startdate."-1 day"));
}
$baseDate = $startdate;

if(isset($_GET['start']))
{
    $nextPrev=$_GET['start'];
    $nextDayPrev=$_GET['start'];
} else {
    $nextPrev=$startdate;
    $nextDayPrev=$baseDay;
}
$weekBack=date("Y-m-d",strtotime($nextPrev."-1 week"));
$weekForward=date("Y-m-d",strtotime($nextPrev."+1 week"));
$dayBack=date("Y-m-d",strtotime($nextDayPrev."-1 day"));
$dayForward=date("Y-m-d",strtotime($nextDayPrev."+1 day"));

print "<div class='pull-left' >
<form id='quickJumpForm' name='quickJumpForm' method=get class='form-inline'>\n";
print "<div class='form-group'><label for='start'>Select a week to jump to</label> <select id='start' name='start' class='form-control' onChange='\$(\"#quickJumpForm\").submit();'>";

for($i=8;$i>0;$i--)
{
    $backdate=date("Y-m-d",strtotime($startdate."-$i months"));       
    if($_GET['start'])
    {
        if($backdate==$_GET['start']){$selected='selected';} else {$selected='';}
    }
    print "<option id='$backdate' value='$backdate' $selected>$backdate</option>\n";
}
for($i=1;$i<12;$i++)
{
    
    if($_GET['start'])
    {
        if($startdate==$_GET['start']){$selected='selected';} else {$selected='';}
    }
    print "<option id='$startdate' value='$startdate' $selected>$startdate</option>\n";
    $startdate=date("Y-m-d",strtotime($startdate."+1 month"));       
}
print "</select></div>";
print "<div class='btn-group pull-right'><a href='?start=$weekBack' class='btn btn-sm btn-primary'><i class='fa fa-chevron-left'></i></a><a href='?start=$baseDate' class='btn btn-sm btn-primary'>Current Month</a><a href='?start=$weekForward' class='btn btn-sm btn-primary'><i class='fa fa-chevron-right'></i></a></div></div>";

print "</form>\n";
?>
</div>
<div id='calendar'></div>

<?php
global $User;

function fetchEvents()
{
    error_reporting(E_ERROR);
    global $pubids, $sizes, $papertypes, $folders, $User;
    if($_GET['start'])
    {
       $startOfMonth=date("Y-m",strtotime($_GET['start'])).'-01'; 
       //$startOfWeek=date("Y-m-d",strtotime($startOfWeek."-1 day")); 
    } else {
       $startOfWeek = date("Y-m").'-01';
    }
    if($_GET['end'])
    {
       $endOfMonth=date("Y-m",strtotime($_GET['end'])).'-'.date("t",strtotime($_GET['end']));
    } else {
       $endOfMonth = date("Y-m").'-'.date("t",strtotime($_GET['end']));
    }
    if($User->hasPermission(45))
    {
        $sql="SELECT * FROM special_sections WHERE site_id=".SITE_ID." AND (
        sales_date>='$startOfMonth' 
        OR page_layout>='$startOfMonth'
        OR dummy_date>='$startOfMonth'
        OR pagination_date>='$startOfMonth'
        OR ads_clear>='$startOfMonth'
        OR editorial_due_date>='$startOfMonth'
        )";
    } else {
        $sql="SELECT * FROM special_sections WHERE site_id=".SITE_ID." AND (
        sales_date>='$startOfMonth' 
        OR page_layout>='$startOfMonth'
        OR dummy_date>='$startOfMonth'
        OR pagination_date>='$startOfMonth'
        OR ads_clear>='$startOfMonth'
        OR editorial_due_date>='$startOfMonth'
        )";
    }
    
    //define various "state" colors
    
    $fields = array('sales_date'=>'#666',
    'page_layout'=>'#777',
    'dummy_date'=>'#888',
    'pagination_date'=>'#999',
    'ads_clear'=>'#000',
    'editorial_due_date'=>'#aaa'
    );
    
    
    $dbSections=dbselectmulti($sql);
    if ($dbSections['numrows']>0)
    {
        $i=0;
        foreach ($dbSections['data'] as $section)
        {
            $title=$section['section_name'];
            $description = htmlentities("Full description");
            //now ad an event for each "type" of due date
            
            foreach($fields as $field=>$color)
            {
                if($section[$field]!='')
                {
                    $events[]=array('id'=> $section['id'],
                        'title'=>"$title - Editorial Content Due",
                        'description'=>$description,
                        'start'=>date("Y-m-d",strtotime($section[$field]))."T00:01",
                        'pubDate'=>date("Y-m-d",strtotime($section[$field])),
                        'allDay'=>true,
                        'color'=>$color,
                        'backgroundColor'=>$color,
                        'borderColor'=>$color,
                        'textColor'=>'#000',
                        'surl'=>'specialSections.php?action=edit&id=$section[id]'
                    );
                    
                }
            }
        }
        print json_encode($events);
    }
    
}

$Page->footer();  
<?php
error_reporting (E_ERROR);
session_start();
if (version_compare(phpversion(), '7.0.0', '<')) {
    require ('includes/functions_db_php5.php');
} else {
    require ('includes/functions_db.php');
}
require ('includes/functions_formtools.php');
require ('includes/functions_graphics.php');
require ('includes/mail/htmlMimeMail.php');
require ('includes/config.php');
require ('includes/functions_common.php');

$appname='MANGO';
$appfield='mango';
$appdesc='Production Management Made Simple';

bootPageHeader();
?>
</head>
<body>
<div class='container'>
<div class='row'>
<div class='col-xs-12 col-sm-6 col-sm-offset-3'>
<div class='hero'>
<h3>Maintenance Mode</h3>
<p>Mango is currently undergoing Maintenance. Please check back later and everything should be just spiffy!</p>
</div>
</div>
<span style='position:absolute;bottom:20px;right:80px;'><a href='corePreferences.php'><i class='fa fa-gears'></i> </a></span>
</div>
<?php
$Page->footer();
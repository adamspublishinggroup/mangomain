<?php
include("includes/boot.php") ;                    
include("includes/layoutGenerator.php");

if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
} 

switch ($action)
{
    case "add";
    recurring('add');
    break;
    
    case "edit";
    recurring('edit');
    break;
    
    case "delete";
    recurring('delete');
    break;
    
    case "listzoning";
    zoning_list();
    break;
    
    case "setzones";
    zoning_config();
    break;
    
    case "Save Zones";
    save_zoning();
    break;
    
    case "Save Recurring Insert";
    save_recurring();
    break;
    
    case "Update Recurring Insert";
    save_recurring();
    break;
    
    case "clear";
    $id=intval($_GET['recurringid']);
    clear_future($id);
    break;
    
    default:
    recurring('list');
    break;
}

function recurring($action)
{
    global $pubs, $recurFrequencies, $advertisers, $Prefs, $Page,$daysofweek;
    $specDates=array("Please Choose",1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31);
    if ($action=='add' || $action=='edit')
    {
        if ($action=='add')
        {
            $button="Save Recurring Insert";
            $days=array();
            $startdate=date("Y-m-d");
            $enddate=date("Y-m-d",strtotime("+1 year"));
            $recurFreq=0;
            $daysout=90;
            $enddatechecked=0;
            $pubID=0;
            $pressrunID=0;
            $accountID=0;
            $slick=0;
            $single=0;
            $sticky=0;
            $product_pages = 0;
            $product_size = 0;
            $standard_pages = 0;
            $quantity = 0;
            $active = 0;
            $pressruns=array();
        } else {
            $button="Update Recurring Insert";
            $recurringID=$_GET['recurringid'];
            $sql="SELECT * FROM inserts_recurring WHERE id=$recurringID";
            $dbInsert=dbselectsingle($sql);
            $insert=$dbInsert['data'];
            $pubID=$insert['pub_id'];
            
            if($pubID!=0)
            {
                 $pressruns[0]="Select a run or leave default";
                 //get pressruns for this pub
                $sql="SELECT id, run_name FROM publications_runs WHERE pub_id=$pubID AND display=1 ORDER BY run_name";
                $dbRuns = dbselectmulti($sql);
                if($dbRuns['numrows']>0)
                {
                    foreach($dbRuns['data'] as $run)
                    {
                        $pressruns[$run['id']] = stripslashes($run['run_name']);
                    }
                }
            } else {
                $pressruns=array();
            }
            
            $pressrunID=$insert['pressrun_id'];
            $accountID = $insert['account_id'];
            $insertTagline=stripslashes($insert['insert_tagline']);
            $active = $insert['active'];
            $days=explode("|",$insert['days_of_week']);
            $daysout=$insert['days_out'];
            $notes=$insert['notes'];
            $daysprev=$job['days_prev'];
            $recurFreq=$insert['recur_frequency'];
            if ($insert['start_date']=='')
            {
                $startdate=date("Y-m-d");    
            } else {
                $startdate=$insert['start_date'];
            }
            if ($insert['end_date']=='')
            {
                 $enddate=date("Y-m-d");    
            } else {
                $enddate=$insert['end_date'];
            }
            $enddatechecked=$insert['end_date_checked'];
            $notes = stripslashes($insert['notes']);
            $slick = $insert['slick_sheet'];
            $sticky = $insert['sticky_note'];
            $single = $insert['single_sheet'];
            $tagline = stripslashes($insert['insert_tagline']);
            $product_pages = stripslashes($insert['product_pages']);
            $standard_pages = stripslashes($insert['standard_pages']);
            $product_size = stripslashes($insert['product_size']);
            $quantity = stripslashes($insert['quantity']);
        }
        
       
        print "<form method=post class='form-horizontal'>\n";
        
            make_checkbox('active',$active,'Activate','Check to make this recurrence active');
            make_select('pub_id',$pubs[$pubID],$pubs,'Insert Publication','','',false,"getPressRuns('pub_id','pressrun_id')");
            make_select('pressrun_id',$pressruns[$pressrunID],$pressruns,'Press Run Name');
            
            make_select('account_id',$advertisers[$accountID],$advertisers,'Accounts');
            make_text('insert_tagline',$insertTagline,'Tagline','Short descriptive tag for insert');
            make_checkbox('slick',$slick,'Slick','Check if insert is a slick sheet');
            make_checkbox('single',$single,'Single','Check if insert is a single sheet');
            make_checkbox('sticky',$sticky,'Sticky','Check if insert is a single sheet');
            make_number('daysout',$daysout,'Days out','How far out to create jobs from current day? This is the continual padding between current day and future. A good value is 90.');
            make_number('quantity',$quantity,'Quantity','Default insert quantity.');
            make_number('productPages',$product_pages,'Product Page Count','','',false,'calcStandardPages();');
            make_select('productSize',$Prefs->insertProducts[$productSize],$Prefs->insertProducts,'Insert Type','Insert format, ex: booklet','',false,'calcStandardPages()');
            make_number('standardPages',$standard_pages,'Standard Page Count','Calculated Standard (broadsheet) pages');
            
            //make sure we capture all the possible recurFreqs which are a list of | delimited values
            //temporary
            if($recurFreq!='')
            {
                $selected = array();
                $temp=explode("|",$recurFreq);
                foreach($temp as $key=>$value)
                {
                    $selected[]=$recurFrequencies[$value];
                }
            } else {
                $selected = $recurFrequencies[0];
            }
            make_select('frequency',$selected,$recurFrequencies,'Recurring Frequency','Specify how regular the recurrence is. (control-click to select multiple)','',false,'',false,true);
            print "<div class='form-group'>\n";
                print "<label for='' class='col-sm-2 control-label'>Publication Days</label>\n";
                print "<div class='col-sm-10'>\n";
                    print "Select one or more days of the week this insert will be for.<br />\n";
                    foreach($daysofweek as $did=>$dname)
                    {
                        if (in_array($did,$days)){$checked="checked";}
                        print "<div class='checkbox'><label><input type='checkbox' name='day_$did' $checked > $dname</label></div>\n";
                        $checked="";        
                    }
                print "</div>\n";
            print "</div>\n";
            
            make_date('startdate',$startdate,'Recurring Starts','Date that the recurrences begin');
            
            print "<div class='form-group'>\n";
                print "<label for='enddatecheck' class='col-sm-2 control-label'>Recurring Ends</label>\n";
                print "<div class='col-sm-10'>\n";
                    print "<small>Date that recurrences end. If unchecked, then continue until disabled.</small><br>";
                    print make_checkbox('enddatecheck',$enddatechecked).' Check if this recurring job ends after a specified date';
                    print make_date('enddate',$enddate);
                print "</div>\n";
            print "</div>\n";
            
            make_textarea('notes',$notes,'Notes','',80,'15');
            make_checkbox('buildnow',0,'Build now','After saving, create future recurring inserts immediately');
         
            make_hidden('recurringid',$recurringID);
            make_submit('submit',$button);
        print "</form>\n";  
        
           
        
    } elseif ($action=='delete')
    {
        $id=intval($_GET['recurringid']);
        $sql="DELETE FROM inserts_recurring WHERE id=$id";
        $dbDelete=dbexecutequery($sql);
        clear_future($id,true);
        $error=$dbDelete['error'];
        if ($error!='')
        {
            setUserMessage('There was a problem deleting the recurrence. '.$error,'error');
        } else {
            setUserMessage('The recurrence has been successfully deleted','success');
        }
        redirect("?action=list");
    } else {
        //get all runs
        $sql="SELECT id, run_name FROM publications_runs";
        $dbRuns=dbselectmulti($sql);
        if($dbRuns['numrows']>0)
        {
            foreach($dbRuns['data'] as $run)
            {
                $runs[$run['id']]=stripslashes($run['run_name']);
            }
        }
        
        $sql="SELECT A.*, B.pub_name, C.account_name FROM inserts_recurring A, publications B, accounts C 
        WHERE A.site_id=".SITE_ID." AND A.pub_id=B.id AND A.account_id=C.id";
        $dbJobs=dbselectmulti($sql);
        tableStart("<a href='?action=add'>Add new recurring insert</a>","Account Name,Publication,Run Name,Recurring Days");
        if ($dbJobs['numrows']>0)
        {
            foreach($dbJobs['data'] as $job)
            {
                $accountname=$job['account_name'];
                $pubname=$job['pub_name'];
                $runname=$runs[$job['pressrun_id']];
                $id=$job['id'];
                $days=str_replace("|",", ",$job['days_of_week']);
                $days=str_replace("0","Sunday",$days);
                $days=str_replace("1","Monday",$days);
                $days=str_replace("2","Tuesday",$days);
                $days=str_replace("3","Wednesday",$days);
                $days=str_replace("4","Thursday",$days);
                $days=str_replace("5","Friday",$days);
                $days=str_replace("6","Saturday",$days);
                print "<tr>";
                print "<td>$accountname</td>\n";
                print "<td>$pubname</td>\n";
                print "<td>$runname</td>\n";
                print "<td>$days</td>\n";
                
                
                 print "<td>
            <div class='btn-group'>
              <a href='?action=edit&recurringid=$id' class='btn btn-dark'>Edit</a>
              <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <span class='caret'></span>
                <span class='sr-only'>Toggle Dropdown</span>
              </button>
              <ul class='dropdown-menu'>
                <li><a href='?action=listzoning&id=$id'>Setup Zoning</a></li>
                <li><a href='cronjobs/recurringInserts.php?mode=manual&specific=$id' target='_blank'>Build Jobs</a></li>
                <li><a href='?action=clear&recurringid=$id' class='delete'>Clear all future inserts</a></li>
                <li><a href='?action=delete&recurringid=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
              </ul>
            </div>
            </td>";
                print "</tr>\n";
            }    
        }
        tableEnd($dbJobs);
        
    }
    
}

function save_recurring($action)
{
    $recurringID=intval($_POST['recurringid']);
    $pubID=intval($_POST['pub_id']);
    $pressrunID=intval($_POST['pressrun_id']);
    $accountID=intval($_POST['account_id']);
    $quantity=intval($_POST['quantity']);
    $productPages=intval($_POST['productPages']);
    $productSize=intval($_POST['productSize']);
    $standardPages=intval($_POST['standardPages']);
    $notes=addslashes($_POST['notes']);
    $insertTagline=addslashes($_POST['insert_tagline']);
    
    if ($_POST['active']){$active=1;}else{$active=0;}
    if ($_POST['slick']){$slick=1;}else{$slick=0;}
    if ($_POST['single']){$single=1;}else{$single=0;}
    if ($_POST['sticky']){$sticky=1;}else{$sticky=0;}
    
    
    $daysout=addslashes($_POST['daysout']);
    $frequency=implode("|",$_POST['frequency']);
    $startdate=addslashes($_POST['startdate']);
    $enddate=addslashes($_POST['enddate']);
    $folderpin=$_POST['folderpin'];
    $jobtype=$_POST['jobtype'];
    if ($_POST['enddatechecked']){$enddatechecked=1;}else{$enddatechecked=0;}
    
    $daysofweek=array();
    foreach($_POST as $key=>$value)
    {
        if (substr($key,0,4)=="day_")
        {
            $daysofweek[]=str_replace("day_","",$key);
        }
    }
    $daysofweek=implode("|",$daysofweek);
    
    if ($recurringID=='0')
    {
        $sql="INSERT INTO inserts_recurring (account_id, pub_id, pressrun_id, quantity, product_size, product_pages, standard_pages, site_id, 
        active, days_out, recur_frequency, start_date, end_date, end_date_checked, notes, days_of_week, slick_sheet, sticky_note, 
        single_sheet, insert_tagline) VALUES ('$accountID', '$pubID', '$pressrunID', '$quantity', '$productSize', '$productPages', '$standardPages', 
        ".SITE_ID.", $active, $daysout, '$frequency', '$startdate', '$enddate', '$enddatechecked', '$notes', '$daysofweek', 
        '$slick', '$sticky', '$single', '$insertTagline')";
        $dbInsert=dbinsertquery($sql); 
        $recurringID=$dbInsert['insertid'];
        $error=$dbInsert['error'];
    } else {
        $sql="UPDATE inserts_recurring SET account_id = '$accountID', pub_id='$pubID', pressrun_id='$pressrunID', quantity='$quantity', 
        product_size='$productSize', product_pages='$productPages', standard_pages='$standardPages', active='$active', days_out='$daysout', 
        recur_frequency='$frequency', start_date='$startdate', end_date='$enddate', end_date_checked='$enddatechecked', 
        notes='$notes', days_of_week='$daysofweek', slick_sheet='$slick', sticky_note='$sticky', single_sheet='$single', insert_tagline='$insertTagline'
         WHERE id=$recurringID";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    if ($error=='')
    {
        
        if ($_POST['buildnow'])
        {
            //clear existing
            
            //build a bunch of jobs now
            include("cronjobs/recurringInserts.php");
            init_recurringInsert($recurringID); 
        }
        if ($error!='')
        {
            setUserMessage('There was a problem creating the recurrence. '.$error,'error');
        } else {
            setUserMessage('The recurrence has been successfully created','success');
        }
    
        redirect("?action=list");
    } else {
        setUserMessage('There was a problem creating the recurrence. '.$error,'error');
        redirect("?action=list");
    }
    
}


/*
* This function will show all the days selected in the recurrence setup, 
* and give you an option to specify the zoning for that day, which is based on the publication zoning
*/
function zoning_list()
{
    global $daysofweek;
    $id=intval($_GET['id']);
    $sql="SELECT * FROM inserts_recurring WHERE id=$id";
    $dbRecurrence = dbselectsingle($sql);
    $recurrence = $dbRecurrence['data'];
    //need to figure out which days of the week we're dealing with  
    $rDays = explode("|",$recurrence['days_of_week']);
    if(count($rDays)>0)
    {
        print "<div class='col-xs-12 col-sm-9'>\n";
        print "<table class='table table-striped table-bordered'>\n";
        print "<tr><th>Day of Week</th><th>Action</th>";
        foreach($rDays as $day)
        {
            print "<tr><td>".$daysofweek[$day]."</td><td><a href='?action=setzones&dow=$day&id=$id' class='btn btn-dark'>Set Zones</a></td></tr>\n";
        }
        print "</table>\n";
        print "</div>\n";
        print "<div class='well col-xs-12 col-sm-3'>\n";
        print "<a href='?action=edit&recurringid=$id'>Edit recurrence</a><br>";
        print "<a href='?action=list'>Return to main</a><br>";
        print "</div>\n";
    } else {
        print "<div class='alert alert-warning' role='alert'>Looks like no days were selected for this insert. <a href='?action=list' class='btn btn-primary'>Return</div>\n";
    }
}

function zoning_config()
{
    global $Page;
    $id=intval($_GET['id']);
    $dow = intval($_GET['dow']);

    $sql="SELECT * FROM inserts_recurring WHERE id=$id";
    $dbRecurrence=dbselectsingle($sql);
    $recurrence=$dbRecurrence['data'];
    
    $sql="SELECT * FROM publications_insertzones WHERE pub_id=$recurrence[pub_id] AND dow_$dow>0 ORDER BY zone_order";
    $dbZones = dbselectmulti($sql);
    
    if($dbZones['numrows']>0)
    {
        print "<form class='form-horizontal' method=post>";
        print "<div class='row'>\n";
        foreach($dbZones['data'] as $zone)
        {
            //see if this one is checked
            $sql="SELECT * FROM inserts_recurring_zoning WHERE recurrence_id=$id AND zone_id=$zone[id]";
            $dbCheck=dbselectsingle($sql);
            if($dbCheck['numrows']>0){$checked='checked';}else{$checked='';}
            
            print "<div class='col-xs-1'>";
            print "<div class='checkbox'>
                <label>
                    <input type=checkbox id='zone_$zone[id]' name='zone_$zone[id]' $checked onclick='toggleTruckChecks($zone[id]);'/> 
                    $zone[zone_name]
                </label>
                </div>";
            //see if there are any defined trucks for this zone
            $sql="SELECT * FROM publications_inserttrucks WHERE pub_id=$recurrence[pub_id] AND zone_id=$zone[id] ORDER BY truck_order";
            $dbTrucks=dbselectmulti($sql);
            if($dbTrucks['numrows']>0)
            {
                print "<hr>";
                foreach($dbTrucks['data'] as $truck)
                {
                    $sql="SELECT * FROM inserts_recurring_zoning_trucks WHERE recurrence_id=$id 
                    AND zone_id=$zone[id] AND truck_id=$truck[id]";
                    $dbCheck=dbselectsingle($sql);
                    if($dbCheck['numrows']>0){$checked='checked';}else{$checked='';}
                    print "<div class='checkbox'><label><input class='zone_$zone[id]' type=checkbox id='truck_$zone[id]_$truck[id]' name='truck_$zone[id]_$truck[id]'  $checked/> $truck[truck_name]</label></div>";
                         
                }
            } 
            print "</div>\n";      
        }
        print "</div>\n";
        make_hidden('id',$id);
        make_hidden('dow',$dow);
        make_submit('submit','Save Zones');
        print "</form>\n";
        $Page->addScript("
      function toggleTruckChecks(id)
      {
          //get current state of this zone
          var zoneCheck = \$('#zone_'+id).prop('checked');
          
          //update all trucks with matching zone class
          \$('.zone_'+id).prop('checked',zoneCheck);   
      } 
        ");
    } else {
        print "<div class='alert alert-warning' role='alert'>There are no defined zones for this publication and date combination. <a href='?action=listzoning&id=$id'>Return to zoning list</a></div>\n";
    }

     
                     
}

function save_zoning()
{
    $id=intval($_POST['id']);
    $dow=intval($_POST['dow']);

    foreach($_POST as $zt=>$value)
    {
      if(substr($zt,0,5)=='zone_')
      {
          $zones[] = str_replace('zone_','',$zt);
      } elseif(substr($zt,0,6)=='truck_')
      {
          $trucks[] = str_replace('truck_','',$zt);
      }
    }

    //clear existing
    $sql="DELETE FROM inserts_recurring_zoning WHERE recurrence_id=$id";
    $dbDelete=dbexecutequery($sql);
    $sql="DELETE FROM inserts_recurring_zoning_trucks WHERE recurrence_id=$id";
    $dbDelete=dbexecutequery($sql);
    
    //get zone counts
    $sql="SELECT id, dow_$dow AS zone_count FROM publications_insertzones";
    $dbCounts = dbselectmulti($sql);
    $counts=array();
    if($dbCounts['numrows']>0)
    {
      foreach($dbCounts['data'] as $count)
      {
          $counts[$count['id']]=$count['zone_count'];
      }
    }

    //now we have an array of IDs
    $inserts=array();
    if(count($zones)>0)
    {
      foreach($zones as $zone)
      {
          $inserts[]="($id,$dow,$zone,".$counts[$zone].")";
      }
    }
    if(count($inserts)>0)
    {
      $sql="INSERT INTO inserts_recurring_zoning (recurrence_id, dow, zone_id, zone_count) VALUES ".implode(",",$inserts);
      $dbInsert=dbinsertquery($sql);
      $error.=$dbInsert['error'];
    }

    //now we have an array of IDs
    $inserts=array();
    if(count($trucks)>0)
    {
      foreach($trucks as $truck)
      {
          $truck=explode("_",$truck);
          //zone id is part 0, truck id is part 1;
          $inserts[]="($id,$truck[0],$truck[1])";
      }
    }
    if(count($inserts)>0)
    {
      $sql="INSERT INTO inserts_recurring_zoning_trucks (recurrence_id, zone_id, truck_id) VALUES ".implode(",",$inserts);
      $dbInsert=dbinsertquery($sql);
      $error.=$dbInsert['error'];
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the zoning. '.$error,'error');
    } else {
        setUserMessage('The zones/trucks have been successfully saved.','success');
    }
    redirect("?action=listzoning&id=$id");
}

function clear_future($id,$every=false)
{
    $date=date("Y-m-d");
    if($every)
    {
        $sql="SELECT A.id FROM inserts A, inserts_schedule B WHERE A.id=B.insert_id AND B.recurrence_id='$id'";
    } else {
        $sql="SELECT A.id FROM inserts A, inserts_schedule B WHERE A.id=B.insert_id AND B.insert_date>='$date' AND B.recurrence_id='$id'";
    }
    $dbJobs=dbselectmulti($sql);
    $ids='';
    if($dbJobs['numrows']>0)
    {
        foreach($dbJobs['data'] as $job)
        {
            $ids[]=$job['id'];
        }
        $ids=implode(",",$ids);
        if($ids!='')
        {
            $sql="DELETE FROM inserts WHERE id IN ($ids)";
            $dbDelete=dbexecutequery($sql);
            $error=$dbDelete['error'];
            $sql="DELETE FROM inserts_schedule WHERE insert_id IN ($ids)";
            $dbDelete=dbexecutequery($sql);
            $error.=$dbDelete['error'];
            $sql="DELETE FROM inserts_zoning WHERE insert_id IN ($ids)";
            $dbDelete=dbexecutequery($sql);
            $error.=$dbDelete['error'];
            $sql="DELETE FROM FROM inserts_zoning_trucks WHERE  insert_id IN ($ids)";
            $error.=$dbDelete['error'];
            
        }
    }
    
    if ($error!='')
    {
        setUserMessage('There was a problem clearing the future recurrences. '.$error,'error');
    } else {
        setUserMessage('The recurrences have been successfully deleted','success');
    }
    redirect("?action=list");
}
$Page->footer(); 
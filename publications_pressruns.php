<?php
//<!--VERSION: .9 **||**-->

include("includes/boot.php");

if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}

switch ($action)
{
    case "Save Run":
    save_run('insert');
    break;
    
    case "Update Run":
    save_run('update');
    break;
    
    case "addrun":
    runs('add');
    break;
    
    case "editrun":
    runs('edit');
    break;
    
    case "deleterun":
    delete_run();
    break;

    case "listruns":
    runs('list');
    break;
    
    
    case "Save Benchmarks":
    save_benchmarks();
    break;

    case "Delete Run":
    move_run();
    break;

    default:
    pubs('list');
    break;
}

function delete_run()
{
    $pubid=intval($_GET['pubid']);
    $runid=intval($_GET['runid']);
    $sql="SELECT * FROM publications_runs WHERE pub_id=$pubid AND id<>$runid AND run_status=1 ORDER BY run_name";
    $dbRuns=dbselectmulti($sql);
    $runs[0]="Dont move, just delete";
    if($dbRuns['numrows']>0)
    {
        foreach($dbRuns['data'] as $run)
        {
            $runs[$run['id']]=stripslashes($run['run_name']);
        }
    }
    //we will present them with a list of other runs to move the jobs that belong to this run to.
    print "<form method=post class='form-horizontal'>\n";
    make_select('newid',$runs[0],$runs,'New run','What run should jobs tied to this run be moved to?');
    make_hidden('pubid',$pubid);
    make_hidden('runid',$runid);
    make_submit('submit',"Delete Run");
    print "</form>\n";
        
    
}

function move_run()
{
    $pubid=$_POST['pubid'];
    $runid=$_POST['runid'];
    $newrunid=$_POST['newid'];
    $sql="UPDATE publications_runs SET run_status=0 WHERE id=$runid";
    $dbDelete=dbexecutequery($sql);
    $error=$dbDelete['error'];
    $sql="UPDATE jobs SET run_id=$newrunid WHERE run_id=$runid";
    $dbMove=dbexecutequery($sql);
    $error.=$dbMove['error'];
    $sql="UPDATE jobs_recurring SET run_id=$newrunid WHERE run_id=$runid";
    $dbMove=dbexecutequery($sql);
    $error.=$dbMove['error'];
    if ($error!='')
    {
        setUserMessage('There was a problem deleting the press run.<br />'.$error,'error');
    } else {
        setUserMessage('The press run has been successfully deleted.','success');
    }
    redirect("?action=listruns&pubid=$pubid"); 
}

function runs($action)
{
    $pubid=$_GET['pubid'];
    if ($action=='add' || $action=='edit')
    {
        if ($action=='add')
        {
            $button="Save Run";
            $runinserts=1;
            $reportable=1;
            $drawlink=0;
            $display=1;
        } else {
            $button="Update Run";
            $runid=$_GET['runid'];
            $sql="SELECT * FROM publications_runs WHERE id=$runid";
            $dbRun=dbselectsingle($sql);
            $run=$dbRun['data'];
            $runname=stripslashes($run['run_name']);
            $runinserts=$run['run_inserts'];
            $productcode=stripslashes($run['run_productcode']);
            $runmessage=stripslashes($run['run_message']);
            $bookingPub=stripslashes($run['booking_pub']);
            $bookingEdition=stripslashes($run['booking_edition']);
            $reportable=$run['reportable'];
            $drawlink=$run['allow_draw_link'];
            $display=$run['display'];
        }
        print "<form method=post class='form-horizontal'>\n";
        if ($runname=='Main')
        {
            print "<div class='label'>Run Name</div><div class='input'>Main</div><input type='hidden' name='runname' value='Main' /><div class='clear'></div>\n";    
        } else {
            make_text('runname',$runname,'Run Name');
        }
        make_text('productcode',$productcode,'Product Code','Enter product code if used.');
        make_checkbox('display',$display,'Display','Allow this run to be selected');
        make_checkbox('draw_link',$drawlink,'Link draws','Allow linking to master publication draw based on pub date');
        make_checkbox('runinserts',$runinserts,'Run inserts','This run inserts back into the main run on the same pub date');
        make_textarea('runmessage',$runmessage,'Run Message','This message displays on the press monitor window when a job with this run comes up',60,3,false);
        make_checkbox('reportable',$reportable,'In Reports','Show this run in reports');
        make_text('booking_pub',$bookingPub,'Insert Pub Code','Pub Code specified in Insert Manifest.');
        make_text('booking_edition',$bookingEdition,'Insert Secondary Code','Secondary Code specified in Insert Manifest.');
        print "<fieldset><legend>Page Flow Targets</legend>\n";
        print "<div class='label'>&nbsp;</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "Schedule leadtime";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "Run Length";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "Last color page";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "Last page";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "Last plate";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "2 Plates Out";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "3 Plates Out";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "4 Plates Out";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "5 Plates Out";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "6 Plates Out";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "Chase Plate";
        print "</div>\n";
        print "<div style='float:left;width:80px;margin-left:10px;font-weight:bold;'>";
        print "Chase Start";
        print "</div>\n";
        print "<div class='clear'></div>\n";
        for($i=1;$i<=7;$i++)
        {
            print "<div class='label'>";
            switch($i)
            {
                case "1":
                print "Monday";
                break;
                
                case "2":
                print "Tuesday";
                break;
                
                case "3":
                print "Wednesday";
                break;
                
                case "4":
                print "Thursday";
                break;
                
                case "5":
                print "Friday";
                break;
                
                case "6":
                print "Saturday";
                break;
                
                case "7":
                print "Sunday";
                break;
                
                
            }
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='schedulelead_$i' name='schedulelead_$i' value='".$run['schedule_leadtime_'.$i]."' onkeypress='return isNumberKey(event);'> hrs.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='runlength_$i' name='runlength_$i' value='".$run['run_length_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='lastcolor_$i' name='lastcolor_$i' value='".$run['last_colorpage_leadtime_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='lastpage_$i' name='lastpage_$i' value='".$run['last_page_leadtime_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='lastplate_$i' name='lastplate_$i' value='".$run['last_plate_leadtime_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='last2plate_$i' name='last2plate_$i' value='".$run['plates_2_left_leadtime_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='last3plate_$i' name='last3plate_$i' value='".$run['plates_3_left_leadtime_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='last4plate_$i' name='last4plate_$i' value='".$run['plates_4_left_leadtime_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='last5plate_$i' name='last5plate_$i' value='".$run['plates_5_left_leadtime_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='last6plate_$i' name='last6plate_$i' value='".$run['plates_6_left_leadtime_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='chaseplate_$i' name='chaseplate_$i' value='".$run['chase_plate_aftertime_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div style='float:left;width:80px;margin-left:10px;'>";
            print "<input type='text' size=4 id='chasestart_$i' name='chasestart_$i' value='".$run['chase_start_aftertime_'.$i]."' onkeypress='return isNumberKey(event);'> min.";
            print "</div>";
            print "<div class='clear'></div>\n";
        }
        print "<div class='label'>Explanation</div><div class='input'>";
        print "<b>Schedule leadtime</b>: How many hours before press start should the job be scheduled (layout selected for recurring jobs)?<br>\n";
        print "<b>Last color page</b>: How many minutes before press start should the last color page be released.<br>\n";
        print "<b>Last page</b>: How many minutes before press start should the last page be released<br>\n"; 
        print "<b>Last plate</b>: How many minutes before press start should the last plate be released?<br>\n"; 
        print "<b>2 Plates Out</b>: How many minutes out should the 2nd to last plate be released?<br>\n"; 
        print "<b>3 Plates Out</b>: How many minutes out should the 3rd to last plate be released?<br>\n"; 
        print "<b>4 Plates Out</b>: How many minutes out should the 4th to last plate be released?<br>\n"; 
        print "<b>5 Plates Out</b>: How many minutes out should the 5th to last plate be released?<br>\n"; 
        print "<b>6 Plates Out</b>: How many minutes out should the 6th to last plate be released?<br>\n"; 
        print "<b>Chase Plate</b>: How many minutes after press start should the last chase plate be released?<br>\n";
        print "<b>Chase Start</b>: How many minutes after first press start should the chase start be?<br>\n";
        
        print "</div><div class='clear'></div>\n";
        print "<input type='button' value='Copy values from Monday to all days' onclick='copyPlateTimeTargets();' />\n";
        print "</fieldset>\n";
        make_submit('submit',$button);
        print "<input type='hidden' name='pubid' value='$pubid'>\n";
        print "<input type='hidden' name='runid' value='$runid'>\n";
        print "</form>\n";
    } else {
       //show all the pubs
       $sql="SELECT * FROM publications_runs WHERE run_status=1 AND pub_id=$pubid ORDER BY run_name";
       $dbRuns=dbselectmulti($sql);
       tableStart("<a href='?action=addrun&pubid=$pubid'>Add run</a>,<a href='publications.php?action=list'>Return to publications</a>",
       "Run Name,Product Code",6);
       if ($dbRuns['numrows']>0)
       {
            foreach($dbRuns['data'] as $run)
            {
                $runid=$run['id'];
                $runname=stripslashes($run['run_name']);
                $productcode=$run['run_productcode'];
                print "<tr><td>$runname</td><td>$productcode</td>\n";
               
                print "<td>
            <div class='btn-group'>
              <a href='?action=editrun&pubid=$pubid&runid=$runid' class='btn btn-dark'>Edit</a>
              <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <span class='caret'></span>
                <span class='sr-only'>Toggle Dropdown</span>
              </button>
              <ul class='dropdown-menu'>
                <li><a href='?action=benchmarks&pubid=$pubid&runid=$runid'>Benchmarks</a></li>";
                if ($runname=='Main')
                {
                    print "<li>Not delete-able</li>\n";    
                } else {
                    print "<li><a href='?action=deleterun&pubid=$pubid&runid=$runid' class='delete'><i class='fa fa-trash'></i> Delete</a></li>\n";
                }
                print "
              </ul>
            </div>
            </td>";
                
                
                print "</tr>\n";
            }
       }
       tableEnd($dbRuns);
    }


}

function save_run($action)
{
    global $siteID;
    $pubid=intval($_POST['pubid']);
    $runid=intval($_POST['runid']);
    if ($_POST['runinserts']){$runinserts=1;}else{$runinserts=0;}
    if ($_POST['reportable']){$reportable=1;}else{$reportable=0;}
    if ($_POST['draw_link']){$drawlink=1;}else{$drawlink=0;}
    if ($_POST['display']){$display=1;}else{$display=0;}
    $runname=($_POST['runname']);
    $runname=str_replace('"','in.',$runname);
    $runname=addslashes($runname);
    $runmessage=addslashes($_POST['runmessage']);
    $productcode=addslashes($_POST['productcode']);
    $bookingPub=addslashes($_POST['booking_pub']);
    $bookinEdition=addslashes($_POST['booking_edition']);
    
    
    $lastpage=addslashes($_POST['lastpage']);
    $lastplate=addslashes($_POST['lastplate']);
    $lastcolor=addslashes($_POST['lastcolor']);
    $schedulelead=addslashes($_POST['schedulelead']);
    $plates2left=addslashes($_POST['last2plate']);
    $plates3left=addslashes($_POST['last3plate']);
    $plates4left=addslashes($_POST['last4plate']);
    $plates5left=addslashes($_POST['last5plate']);
    $plates6left=addslashes($_POST['last6plate']);
    $chaseplate=addslashes($_POST['chaseplate']);
    $chasestart=addslashes($_POST['chasestart']);
    
    if ($action=='insert')
    {
        $sql="INSERT INTO publications_runs (pub_id, display, run_name,run_inserts, run_status, run_message, reportable, 
        run_productcode, allow_draw_link, booking_pub, booking_edition,
        last_page_leadtime_1, last_plate_leadtime_1, last_colorpage_leadtime_1, schedule_leadtime_1, plates_2_left_leadtime_1,
        plates_3_left_leadtime_1,plates_4_left_leadtime_1,plates_5_left_leadtime_1,plates_6_left_leadtime_1,
        chase_plate_aftertime_1, chase_start_aftertime_1, run_length_1,
        last_page_leadtime_2, last_plate_leadtime_2, last_colorpage_leadtime_2, schedule_leadtime_2, plates_2_left_leadtime_2,
        plates_3_left_leadtime_2,plates_4_left_leadtime_2,plates_5_left_leadtime_2,plates_6_left_leadtime_2,
        chase_plate_aftertime_2, chase_start_aftertime_2, run_length_2,
        last_page_leadtime_3, last_plate_leadtime_3, last_colorpage_leadtime_3, schedule_leadtime_3, plates_2_left_leadtime_3,
        plates_3_left_leadtime_3,plates_4_left_leadtime_3,plates_5_left_leadtime_3,plates_6_left_leadtime_3,
        chase_plate_aftertime_3, chase_start_aftertime_3, run_length_3,
        last_page_leadtime_4, last_plate_leadtime_4, last_colorpage_leadtime_4, schedule_leadtime_4, plates_2_left_leadtime_4,
        plates_3_left_leadtime_4,plates_4_left_leadtime_4,plates_5_left_leadtime_4,plates_6_left_leadtime_4,
        chase_plate_aftertime_4, chase_start_aftertime_4, run_length_4,
        last_page_leadtime_5, last_plate_leadtime_5, last_colorpage_leadtime_5, schedule_leadtime_5, plates_2_left_leadtime_5,
        plates_3_left_leadtime_5,plates_4_left_leadtime_5,plates_5_left_leadtime_5,plates_6_left_leadtime_5,
        chase_plate_aftertime_5, chase_start_aftertime_5, run_length_5,
        last_page_leadtime_6, last_plate_leadtime_6, last_colorpage_leadtime_6, schedule_leadtime_6, plates_2_left_leadtime_6,
        plates_3_left_leadtime_6,plates_4_left_leadtime_6,plates_5_left_leadtime_6,plates_6_left_leadtime_6,
        chase_plate_aftertime_6, chase_start_aftertime_6, run_length_6,
        last_page_leadtime_7, last_plate_leadtime_7, last_colorpage_leadtime_7, schedule_leadtime_7, plates_2_left_leadtime_7,
        plates_3_left_leadtime_7,plates_4_left_leadtime_7,plates_5_left_leadtime_7,plates_6_left_leadtime_7,
        chase_plate_aftertime_7, chase_start_aftertime_7, run_length_7
        ) VALUES ('$pubid', '$display', '$runname', '$runinserts', 1, '$runmessage', '$reportable', '$productcode', '$drawlink', 
        '$bookingPub', '$bookingEdition', ";
        for($i=1;$i<=7;$i++)
        {
            if($_POST['lastpage_'.$i]!=''){$sql.="'".addslashes($_POST['lastpage_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['lastplate_'.$i]!=''){$sql.="'".addslashes($_POST['lastplate_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['lastcolor_'.$i]!=''){$sql.="'".addslashes($_POST['lastcolor_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['schedulelead_'.$i]!=''){$sql.="'".addslashes($_POST['schedulelead_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['last2plate_'.$i]!=''){$sql.="'".addslashes($_POST['last2plate_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['last3plate_'.$i]!=''){$sql.="'".addslashes($_POST['last3plate_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['last4plate_'.$i]!=''){$sql.="'".addslashes($_POST['last4plate_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['last5plate_'.$i]!=''){$sql.="'".addslashes($_POST['last5plate_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['last6plate_'.$i]!=''){$sql.="'".addslashes($_POST['last6plate_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['chaseplate_'.$i]!=''){$sql.="'".addslashes($_POST['chaseplate_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['chasestart_'.$i]!=''){$sql.="'".addslashes($_POST['chasestart_'.$i])."', ";} else {$sql.="'0', ";}
            if($_POST['runlength_'.$i]!=''){$sql.="'".addslashes($_POST['runlength_'.$i])."', ";} else {$sql.="'0', ";}
            
        }
        $sql=substr($sql,0,strlen($sql)-2);
        $sql.=")";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
    } else {
        $sql="UPDATE publications_runs SET display='$display', run_inserts='$runinserts', run_name='$runname', pub_id='$pubid', booking_pub='$bookingPub', booking_edition='$bookingEdition', 
        run_message='$runmessage', reportable='$reportable', run_productcode='$productcode', allow_draw_link='$drawlink', ";
        for($i=1;$i<=7;$i++)
        {
            if($_POST['lastpage_'.$i]!=''){$sql.="last_page_leadtime_$i='".addslashes($_POST['lastpage_'.$i])."', ";}else{$sql.="last_page_leadtime_$i='0', ";}
            if($_POST['lastplate_'.$i]!=''){$sql.="last_plate_leadtime_$i='".addslashes($_POST['lastplate_'.$i])."', ";}else{$sql.="last_plate_leadtime_$i='0', ";}
            if($_POST['lastcolor_'.$i]!=''){$sql.="last_colorpage_leadtime_$i='".addslashes($_POST['lastcolor_'.$i])."', ";}else{$sql.="last_colorpage_leadtime_$i='0', ";}
            if($_POST['schedulelead_'.$i]!=''){$sql.="schedule_leadtime_$i='".addslashes($_POST['schedulelead_'.$i])."', ";}else{$sql.="schedule_leadtime_$i='0', ";}
            if($_POST['chaseplate_'.$i]!=''){$sql.="chase_plate_aftertime_$i='".addslashes($_POST['chaseplate_'.$i])."', ";}else{$sql.="last_page_leadtime_$i='0', ";}
            if($_POST['chasestart_'.$i]!=''){$sql.="chase_start_aftertime_$i='".addslashes($_POST['chasestart_'.$i])."', ";}else{$sql.="chase_start_aftertime_$i='0', ";}
            if($_POST['chasestart_'.$i]!=''){$sql.="chase_start_aftertime_$i='".addslashes($_POST['chasestart_'.$i])."', ";}else{$sql.="chase_start_aftertime_$i='0', ";}
            if($_POST['last2plate_'.$i]!=''){$sql.="plates_2_left_leadtime_$i='".addslashes($_POST['last2plate_'.$i])."', ";}else{$sql.="plates_2_left_leadtime_$i='0', ";}
            if($_POST['last3plate_'.$i]!=''){$sql.="plates_3_left_leadtime_$i='".addslashes($_POST['last3plate_'.$i])."', ";}else{$sql.="plates_3_left_leadtime_$i='0', ";}
            if($_POST['last4plate_'.$i]!=''){$sql.="plates_4_left_leadtime_$i='".addslashes($_POST['last4plate_'.$i])."', ";}else{$sql.="plates_4_left_leadtime_$i='0', ";}
            if($_POST['last5plate_'.$i]!=''){$sql.="plates_5_left_leadtime_$i='".addslashes($_POST['last5plate_'.$i])."', ";}else{$sql.="plates_5_left_leadtime_$i='0', ";}
            if($_POST['last6plate_'.$i]!=''){$sql.="plates_6_left_leadtime_$i='".addslashes($_POST['last6plate_'.$i])."', ";}else{$sql.="plates_6_left_leadtime_$i='0', ";}
            if($_POST['runlength_'.$i]!=''){$sql.="run_length_$i='".addslashes($_POST['runlength_'.$i])."', ";}else{$sql.="runlength_='0', ";}
            
        } 
        $sql=substr($sql,0,strlen($sql)-2);
        $sql.=" WHERE id=$runid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the press run.<br />'.$error,'error');
    } else {
        setUserMessage('The press run has been successfully saved.','success');
    }
    redirect("?action=listruns&pubid=$pubid");
    
}



function benchmarks()
{
    global $siteID;
    $runid=intval($_GET['runid']);
    $pubid=intval($_GET['pubid']);
    //get all the benchmarks
    $sql="SELECT * FROM benchmarks WHERE site_id=$siteID ORDER BY benchmark_category, benchmark_name";
    $dbBenchmarks=dbselectmulti($sql);
    if ($dbBenchmarks['numrows']>0)
    {
        //we'll make a table, then loop through each benchmark and look up the values for that one from the run_benchmark table
        print "<form name='benchmarks' method=post>\n";
        print "<table class='grid'>\n";
        print "<tr><th colspan=11>Please note: The date listed below is the Publication Day of the job, so a Sunday pub will have greater times usually</th></tr>\n";
        print "<tr><th>Benchmark</th><th>Category</th><th>Type</th><th>Sunday</th><th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th><th>Saturday</th><th>Action</th></tr>\n";
        foreach($dbBenchmarks['data'] as $benchmark)
        {
            $name=$benchmark['benchmark_name'];
            $type=$benchmark['benchmark_type'];
            $bid=$benchmark['id'];
            $category=$benchmark['benchmark_category'];
            print "<tr><td style='background-color:#999999;font-weight:bold;'>$name</td>";
            print "<td style='background-color:#999999;font-weight:bold;'>$category</td>";
            print "<td style='background-color:#999999;font-weight:bold;'>$type</td>";
            
            //now grab the values for the run_benchmarks for this benchmark id
            $sql="SELECT * FROM run_benchmarks WHERE benchmark_id=$bid AND run_id=$runid ";
            $dbRunInfo=dbselectsingle($sql);
            if ($dbRunInfo['numrows']>0)
            {
                $info=$dbRunInfo['data'];
                $sunday=$info['sunday'];
                $monday=$info['monday'];
                $tuesday=$info['tuesday'];
                $wednesday=$info['wednesday'];
                $thursday=$info['thursday'];
                $friday=$info['friday'];
                $saturday=$info['saturday'];
                $id=$info['id'];
            } else {
                //no record, just use blanks
                $id=0;
                if ($type=='time')
                {
                    $sunday="12:00";
                    $monday="12:00";
                    $tuesday="12:00";
                    $wednesday="12:00";
                    $thursday="12:00";
                    $friday="12:00";
                    $saturday="12:00";
                } else {
                    $sunday=0;
                    $monday=0;
                    $tuesday=0;
                    $wednesday=0;
                    $thursday=0;
                    $friday=0;
                    $saturday=0;
                }
            }
            if ($type=='number'){$numonly="onkeydown='return isNumberKey(event);'";}else{$numonly="";}
            $type.="_";
            $sunday="<input type='text' id='".$type.$bid."_".$id."_sunday' name='".$type.$bid."_".$id."_sunday' value='$sunday' style='width:50px;' $numonly>";
            $monday="<input type='text' id='".$type.$bid."_".$id."_monday' name='".$type.$bid."_".$id."_monday' value='$monday' style='width:50px;' $numonly>";
            $tuesday="<input type='text' id='".$type.$bid."_".$id."_tuesday' name='".$type.$bid."_".$id."_tuesday' value='$tuesday' style='width:50px;' $numonly>";
            $wednesday="<input type='text' id='".$type.$bid."_".$id."_wednesday' name='".$type.$bid."_".$id."_wednesday' value='$wednesday' style='width:50px;' $numonly>";
            $thursday="<input type='text' id='".$type.$bid."_".$id."_thursday' name='".$type.$bid."_".$id."_thursday' value='$thursday' style='width:50px;' $numonly>";
            $friday="<input type='text' id='".$type.$bid."_".$id."_friday' name='".$type.$bid."_".$id."_friday' value='$friday' style='width:50px;' $numonly>";
            $saturday="<input type='text' id='".$type.$bid."_".$id."_saturday' name='".$type.$bid."_".$id."_saturday' value='$saturday' style='width:50px;' $numonly>";
            //now display them
            print "<td>$sunday</td><td>$monday</td><td>$tuesday</td><td>$wednesday</td><td>$thursday</td><td>$friday</td><td>$saturday</td>";
            print "</tr>\n";
        }
        print "<input type='hidden' name='pubid' value='$pubid'>\n";
        print "<input type='hidden' name='runid' value='$runid'>\n";
        print "<tr><th colspan=11><input type='submit' name='submit' value='Save Benchmarks'></th></tr>\n";
        print "</table>\n";
        print "</form>\n";
    } else {
        displayMessage("No benchmarks have been set up yet.");
    }
    
}


function save_benchmarks()
{
    //grab some basics immediately
    global $siteID;
    $runid=intval($_POST['runid']);
    $pubid=intval($_POST['pubid']);
    $values=array();
    foreach($_POST as $key=>$value)
    {
        //look for all the _'s
        if (strpos($key,"_")>0)
        {
            $parts=explode("_",$key);
            $type=$parts[0];
            $benchmark_id=$parts[1];
            $runBenchmark_id=$parts[2];
            $dayfield=$parts[3];
            $temp=array("day"=>"$dayfield='$value'","rbid"=>$runBenchmark_id);
            $values[$benchmark_id][]=$temp;
            
        }
    }
    foreach($values as $key=>$set)
    {
        $days="";
        $parts="";
        $fields="";
        $sqlvalues="";
        foreach($set as $item)
        {
            $runBenchmark_id=$item['rbid'];
            if ($runBenchmark_id==0)
            {
                $parts=explode("=",$item['day']);
                $fields.=$parts[0].",";
                $sqlvalues.=$parts[1].",";
            } else {
                $days.=$item['day'].",";        
            }
        }
        if ($runBenchmark_id==0)
        {
            $fields=substr($fields,0,strlen($fields)-1);
            $sqlvalues=substr($sqlvalues,0,strlen($sqlvalues)-1);
            //inserting a new one
            $sql="INSERT INTO run_benchmarks (site_id, run_id, benchmark_id,$fields) VALUES ($siteID, $runid,$key,$sqlvalues)";
            $dbInsert=dbinsertquery($sql);
            $error=$dbInsert['error'];
        } else {
            $days=substr($days,0,strlen($days)-1);
            //updating one
            $sql="UPDATE run_benchmarks SET $days WHERE id=$runBenchmark_id";
            $dbUpdate=dbexecutequery($sql);
            $error=$dbUpdate['error'];
        }
        
    }
    if($error=='')
    {
        setUserMessage('Benchmarks have been updated','success');
    } else {
        setUserMessage('Error setting benchmarks.'.$error,'error');
    }
    redirect("?action=listruns&pubid=$pubid");
}

$Page->footer();
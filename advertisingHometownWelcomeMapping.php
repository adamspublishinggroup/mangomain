<?php
include("includes/boot.php");
show_map();
function show_map()
{
    global $sales;
    $sql="SELECT * FROM core_preferences";
    $dbPrefs=dbselectsingle($sql);
    $prefs=$dbPrefs['data'];
    
    $sql="SELECT DISTINCT(batch) FROM hometown_welcome ORDER BY batch";
    $dbBatches=dbselectmulti($sql);
    $categories['all']='Show all categories'; 
    if($dbBatches['numrows']>0)
    {
        foreach($dbBatches['data'] as $bat)
        {
            if(trim($bat['batch'])!='')
            {
                $batches[$bat['batch']]=$bat['batch'];
            }
        }    
    }
    
    if($_POST['batch'])
    {
        $batch=$_POST['batch'];
    } elseif($_GET['batch']) {
        $batch=$_GET['batch'];
    } else {
        $batch='all';
    } 
    
    print "<div style='padding:10px;border: 1px solid black;background-coloe:#efefef;margin-bottom:10px;'>\n";
    print "<form method=post class='form-horizontal'>\n";
            make_select('batch',$batches[$batch],$batches,'Batch');
            make_submit('submit','Map this batch');
        print "</form>\n";
    print "</div>\n";
    
    
    if($dbPrefs['data']['officeLat']!='' && $dbPrefs['data']['officeLat']!=0) { 
        $defaultLat=$dbPrefs['data']['officeLat'];
        $defaultLon=$dbPrefs['data']['officeLon'];
    } else {
        $defaultLat=43.57939;
        $defaultLon=-116.55910;
    }
    
    if($_POST)
    {
        if($_POST['batch']!='all')
        {
            $batch=" AND batch='".addslashes($_POST['batch'])."'"; 
        } elseif($_GET['batch']){
            $batch=" AND batch='".$_GET['batch']."'";
        }
        
    } elseif($_GET['batch'])
    {
            $batch=" AND batch='".$_GET['batch']."'";
    }
    $accountsql="SELECT * FROM hometown_welcome WHERE lat<>'' AND lng<>'' $batch";
    ?>

<style type='text/css'>
<?php if ($_GET['mode']=='print' || $_POST['print'])
{
  ?>
  #mapCanvas {
    border: thin solid black;
    width: <?php echo $settings['map_width']; ?>px;
    height: <?php echo $settings['map_height']; ?>px;
}

  <?php  
} else {?>

#mapCanvas {
    border: thin solid black;
    width: 100%;
    height: 600px;
}
<?php } ?>
#legend {
    margin-top:20px;
    padding:10px;
    font-family:Trebuchet MS, Arial, sans-serif;
    font-size:10px;
}
.balloon {
    width:200px;
    
}
</style>

<script type="text/javascript"> 
 
var center;
var map = null;
var currentPopup;
var icon = Array();
icon[0] = 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=0|00CC99|000000';
//need to create an icon for each rep

var bounds = new google.maps.LatLngBounds();
function addMarker(id, lat, lng, info, salesid) {
    var pt = new google.maps.LatLng(lat, lng);
    bounds.extend(pt);
    var marker = new google.maps.Marker({
        id: id,
        position: pt,
        icon: icon[salesid],
        map: map
    });
    var popup = new google.maps.InfoWindow({
        content: info,
        maxWidth: 300
    });
    google.maps.event.addListener(marker, "click", function() {
        if (currentPopup != null) {
            currentPopup.close();
            currentPopup = null;
        }
        popup.open(map, marker);
        currentPopup = popup;
    });
    google.maps.event.addListener(popup, "closeclick", function() {
        currentPopup = null;
    });
};
$(function() {
    var lat=<?php echo $defaultLat ?>;
    var lon=<?php echo $defaultLon ?>;
    center = new google.maps.LatLng(lat,lon);
        
    //Basic
    var MapOptions = {
      zoom: 12,
      center: center,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    // Init the map
    map = new google.maps.Map(document.getElementById("mapCanvas"),MapOptions);
    <?php
    //get the mapping stuff
    
    $dbAccounts=dbselectmulti($accountsql);
    if($dbAccounts['numrows']>0)
    {
        $i=1;
        foreach($dbAccounts['data'] as $item)
        {
            $ballooninfo="<div class=\"balloon\">".$item['first_name'].' '.$item['last_name']."<br>";
            $ballooninfo.=$item['address']."<br>";
            $ballooninfo.=$item['city'].' '.$item['state'].' '.$item['zip']."</div>";
            
            print "addMarker($i,$item[lat],$item[lng],'".addslashes($ballooninfo)."',0);\n";
            $i++;
        }
    }
    ?>
     
  }); 

  
  
</script>

<div id="mapCanvas"><div style='margin-top:50px;text-align:center;margin-left:auto;margin-right:auto;'>Loading...<br><i class="fa fa-spinner fa-spin fa-3x fa-fw"></i></div></div>


<?php
 
}
$Page->footer();

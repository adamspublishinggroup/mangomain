<?php
include("includes/boot.php") ;

if ($_POST)
{
    $action=$_POST['submit'];
    if ($_POST['jssubmitbtn']=='Save Data')
    {
        $action="Save Data";
    }
} else {
    $action=$_GET['action'];
}



switch ($action)
{
     
    case "Save Plan":
    save_plan('insert');
    break;
    
    case "Update Plan":
    save_plan('update');
    break;
    
    case "addplan":
    plans('add');
    break;
    
    case "editplan":
    plans('edit');
    break;
    
    case "deleteplan":
    plans('delete');
    break;
    
    case "print":
    ?>
    <script>
    window.open('printouts/insertplan.php?planid=<?php echo intval($_GET['planid']); ?>','Inserter Plan','width=700,height=800,toolbar=0,status=0,location=0,scrollbars=yes');
    </script>
    <?php
    plans('list');    
    break;
    
    
    default:
    plans('list');
    break;
}

function plans($action)
{
    global $siteID, $pubs;
    
    $planid=intval($_GET['planid']);
    $rules = array();
    $rules[0]="Please select";
    $inserters=array();
    $inserters[0]="Please choose";
    $sql="SELECT * FROM inserters WHERE site_id=$siteID";
    $dbInserters=dbselectmulti($sql);
    if ($dbInserters['numrows']>0)
    {
        foreach($dbInserters['data'] as $inserter)
        {
            $inserters[$inserter['id']]=$inserter['inserter_name'];    
        }
    }
    if ($action=='add' || $action=='edit')
    {
       if ($action=='add')
       {
            $button='Save Plan';
            $pubid=$GLOBALS['defaultInsertPublication'];
            $pressrunid=0;
            $rulesID=0;
            $pubdate=date("Y-m-d",strtotime("+1 day"));
            $inserterid=$GLOBALS['defaultInserter'];
            $address=0;
            $numpackages=1;
       } else {
         $button='Update Plan';  
         $sql="SELECT * FROM jobs_inserter_plans WHERE id=$planid";
         $dbPlan=dbselectsingle($sql);
         $plan=$dbPlan['data'];
         $pubid=$plan['pub_id'];
         $pressrunid=$plan['pressrun_id'];
         $rulesID=$plan['rules_id'];
         $pubdate=$plan['pub_date'];
         $request=$plan['inserter_request']; 
         $inserterid=$plan['inserter_id']; 
         $address=$plan['address']; 
         $numpackages=$plan['num_packages']; 
       }
       
      $runs[0]="Please choose";
      if ($pubid!=0)
      {
          $runs[0]="Please choose or leave empty";
          $pressruns[0]="Please choose or leave empty";
          $runsql="SELECT id, run_name FROM publications_insertruns WHERE pub_id=$pubid";
          $dbRuns=dbselectmulti($runsql);
          if ($dbRuns['numrows']>0)
          {
              foreach ($dbRuns['data'] as $insertrun)
              {
                  $runs[$insertrun['id']]=$insertrun['run_name'];
              }
          }
          
          $pressrunsql="SELECT id, run_name FROM publications_runs WHERE pub_id=$pubid";
          $dbPressRuns=dbselectmulti($pressrunsql);
          if ($dbPressRuns['numrows']>0)
          {
              foreach ($dbPressRuns['data'] as $pressrun)
              {
                  $pressruns[$pressrun['id']]=$pressrun['run_name'];
              }
          }
      }
      print "<form method=post class='form-horizontal'>\n";
      make_select('inserter_id',$inserters[$inserterid],$inserters,'Choose Inserter');
      make_select('pub_id',$pubs[$pubid],$pubs,'Choose publication','','',false,"getPressAndInsertRuns();");
      make_select('pressrun_id',$pressruns[$pressrunid],$pressruns,'Choose Press run','Leave at default unless you are doing a run where it is like a mini-publication (TMC/Select, etc)');
      make_select('rules_id',$rules[$ruleID],$rules,'Autobuild rules','Select a set of auto-build rules for autopackage generation');
      make_date('pubdate',$pubdate,'Publish Date');
      make_slider('num_packages',$numpackages,'# of packages','Total number of packages (includes Main)<br>ATTENTION: Reducing the number of packages once they have been set up will cause all inserts to be resert.',1,10,1);
      make_text('request',$request,'Press Request','Enter the number of papers requested, approximate in final number is not available. This is used to calculate estimated run time.');
      make_checkbox('address',$address,'Address label','Check if we will be printing address labels');
      make_hidden('planid',$planid);
      make_submit('submit',$button);
      print "</form>\n";
   } elseif ($action=='delete')
   {
       $planid=intval($_GET['planid']); 
       $sql="DELETE FROM jobs_inserter_plans WHERE id=$planid";
       $dbDelete=dbexecutequery($sql);
       if ($dbDelete['error']=='')
       {
            //get plan
            $plansql="SELECT * FROM jobs_inserter_packages WHERE plan_id=$planid";
            $dbPackages=dbselectsingle($plansql);
            if($dbPackages['numrows']>0)
            {
                foreach($dbPackages['data'] as $package)
                {
                    $packid=$package['id'];
                    $sql="SELECT * FROM jobs_packages_inserts WHERE plan_id=$planid AND package_id=$packid";
                    $dbInserts=dbselectmulti($sql);
                    
                    if($dbInserts['numrows']>0)
                    {
                        foreach($dbInserts['data'] as $insert)
                        {
                            $temp=removeInsert($insert['plan_id'],$insert['package_id'],$insert['insert_id'],$insert['insert_type'],$insert['hopper_id']);
                            $deleteids=$insert['id'].",";
                        }
                    }
                    $deleteids=substr($deleteids,0,strlen($deleteids)-1);
                    if($deleteids!='')
                    {
                        $sql="DELETE FROM jobs_packages_inserts WHERE id IN ($deleteids)";
                        $dbDelete=dbexecutequery($sql);
                    }
                    //delete the actual package
                    $sql="DELETE FROM jobs_inserter_packages WHERE id=$packid";
                    $dbDelete=dbexecutequery($sql);
                    $error.=$dbDelete['error']; 
                }
            }
            
            
        } else {
            $error=$dbDelete['error'];
        }
        if ($error!='')
        {
            setUserMessage('There was a problem deleting the packaging plan.<br>'.$error.'<br>Plan Sql:'.$plansql,'error');
        } else {
            setUserMessage('The packaging plan has been successfully deleted.','success');
        }
    
        redirect("?action=list");    
   } else {
       global $pubids;
       $currentMonth = date("m");
       $lastDay = date("t");
       $pubstartdate=date("Y")."-$currentMonth-01";
       $pubstopdate=date("Y")."-$currentMonth-$lastDay";
       
       $packstartdate=date("Y-m-d",strtotime("-1 week"));
       $packstopdate=date("Y-m-d",strtotime("+1 week"));
       $pub="pub_id>0";
       if ($_POST['search']=='Search')
       {
            $pubdate="AND pub_date<='".$_POST['pub_stopdate']."' AND pub_date>='".$_POST['pub_startdate']."'";
            $pubid=$_POST['search_pub'];
            if ($pubid!=0)
            {
                $pub="$and pub_id='$pubid'";
            }
            $pubstartdate=$_POST['pub_startdate'];
            $pubstopdate=$_POST['pub_stopdate'];
            
       } else {
           $pubdate="AND pub_date<='$pubstopdate' AND pub_date>='$pubstartdate'";
       }
       $search="<form method=post class='form-horizontal'>\n";
       $search.= "Plans scheduled to publish between<br>";
       $search.=make_date('pub_startdate',$pubstartdate);
       $search.="and<br>";
       $search.=make_date('pub_stopdate',$pubstopdate);
       $search.="<br>Publication<br>";
       $search.=make_select('search_pub',$pubs[$_POST['search_pub']],$pubs);
       $search.="<input type=submit name='search' id='search' value='Search'></input>\n";
       $search.="</form>\n"; 
       
       
       //run sql
       $sql="SELECT * FROM jobs_inserter_plans WHERE $pub $pubdate AND pub_id IN ($pubids) AND site_id=".SITE_ID." ORDER BY pub_date DESC";
       //print $sql;
       $dbPlans=dbselectmulti($sql);
       tableStart("<a href='?action=addplan'>Add Inserter Plan</a>,<a href='inserts.php?action=list'>Show inserts</a>","Publication,Pub Date",3,$search);
       if ($dbPlans['numrows']>0)
       {
           foreach($dbPlans['data'] as $plan)
           {
               print "<tr>\n";
               $planid=$plan['id'];
               $pubid=$plan['pub_id'];
               if($plan['pressrun_id']!=0)
               {
                   $pressrunID = $plan['pressrun_id'];
                   $sql="SELECT run_name FROM publications_runs WHERE id=$pressrunID";
                   $dbRun = dbselectsingle($sql);
                   $runName="- ".$dbRun['data']['run_name'];
                   
               } else {
                   $runName = '';
               }
               $inserterid=$plan['inserter_id'];
               $pub=$pubs[$plan['pub_id']];
               $date=date("m/d/Y",strtotime($plan['pub_date']));
               print "<td>$pub $runName</td>";
               print "<td>$date</td>";
               print "<td>
            <div class='btn-group'>
              <a href='inserterPackageBuilder.php?planid=$planid&pubid=$pubid' class='btn btn-dark'>Build Packages</a>
              <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <span class='caret'></span>
                <span class='sr-only'>Toggle Dropdown</span>
              </button>
              <ul class='dropdown-menu'>
                <li><a href='?action=editplan&planid=$planid'>Edit Plan</a></li>
                <li><a href='inserterPackages.php?planid=$planid&pubid=$pubid'>List Packages</a></li>
                <li><a href='?action=print&planid=$planid'>Print Plan</a></li>
                <li><a href='?action=deleteplan&planid=$planid' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
              </ul>
            </div>
            </td>";
               
               print "</tr>\n";
           }
       }
       tableEnd($dbPlans); 
   }
}
    
function save_plan($action)
{
    $pubid=intval($_POST['pub_id']);
    $rulesID=intval($_POST['rules_id']);
    $pressrunid=intval($_POST['pressrun_id']);
    $inserterid=intval($_POST['inserter_id']);
    $planid=intval($_POST['planid']);
    $pubdate=addslashes($_POST['pubdate']);
    $numpackages=intval($_POST['num_packages']);
    $request=addslashes($_POST['request']);   
    if($_POST['address']){$address=1;}else{$address=0;}
    if($request==''){$request=10000;}
    //figure out which insert run this ties to
    $pubday=date("w",strtotime($pubdate));
        
   
    
    $sql="SELECT * FROM inserters WHERE id=$inserterid";
    
    
    $dbInserter=dbselectsingle($sql);
    $inserter=$dbInserter['data'];
    
    
    $singleoutspeed=$inserter['single_out_speed'];
    
    //we will default to single out to leave a larger window by default
    $speed=$singleoutspeed; 
    
    
    if($speed>0)
    {
        $runminutes=round(($request/$speed),0)+30; //pad by 30 because... :)
    } else {
        $runminutes=120;
    }
    $packdate=date("Y-m-d",strtotime($pubdate."- 1 day"));
    $packdate=$packdate." 19:00";//set default package start to 7pm the night before publication    
    $packstop=date("Y-m-d H:i",strtotime($packdate."+$runminutes minutes"));
       
    
    if ($action=='insert')
    {
        $sql="INSERT INTO jobs_inserter_plans (inserter_id,pub_id, rules_id, pressrun_id, pub_date, inserter_request, address, 
        site_id, num_packages) VALUES ('$inserterid', '$pubid', '$rulesID', '$pressrunid', '$pubdate', '$request', '$address', 
        '".SITE_ID."', '$numpackages')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        if ($error=='')
        {
            //by default on creation of a new plan, we'll auto-create a "MAIN" package with a date 1 day before the pub date
            $planid=$dbInsert['insertid'];
            
            for($i=1;$i<=$numpackages;$i++)
            {
                if($i==1){$pname='Main';}else{$pname='Package #'.$i;} 
                $sql="INSERT INTO jobs_inserter_packages (pub_id, pressrun_id, rules_id, pub_date, package_date, plan_id, inserter_id, 
                package_name, package_startdatetime, package_stopdatetime, inserter_request, site_id) VALUES 
                ('$pubid', '$pressrunid', '$rulesID', '$pubdate', '$packdate', '$planid', '$inserterid', '$pname', '$packdate', 
                '$packstop', '$request', '".SITE_ID."')";
                $dbInsert=dbinsertquery($sql);
                $error.=$dbInsert['error'];
            }
        }    
    } else {
        //lets see if the number of packages changes
        $sql="SELECT * FROM jobs_inserter_plans WHERE id=$planid";
        $dbCheck=dbselectsingle($sql);
        $initial=$dbCheck['data']['num_packages'];
        if($initial!=$numpackages)
        {
            //ok, this means that we need to delete packages, unset insert bindings
            $sql="DELETE FROM jobs_packages_inserts WHERE plan_id='$planid'";
            $dbDeletePackageInserts=dbexecutequery($sql);
            $sql="DELETE FROM jobs_inserter_packages WHERE plan_id='$planid'";
            $dbDeletePackages=dbexecutequery($sql);
            
            //now create new packages
            for($i=1;$i<=$numpackages;$i++)
            {
                if($i==1){$pname='Main';}else{$pname='Package #'.$i;} 
                $sql="INSERT INTO jobs_inserter_packages (pub_id, pressrun_id, rules_id, pub_date, package_date, plan_id, inserter_id, 
                package_name, package_startdatetime, package_stopdatetime, inserter_request, site_id) VALUES 
                ('$pubid', '$pressrunid', '$rulesID','$pubdate','$packdate', '$planid', '$inserterid', '$pname', '$packdate', 
                '$packstop', '$request', '".SITE_ID."')";
                $dbInsert=dbinsertquery($sql);
                
                $error.=$dbInsert['error'];
            }
        }
        $sql="UPDATE jobs_inserter_plans SET address='$address', inserter_id='$inserterid', pub_id='$pubid', 
        pub_date='$pubdate', rules_id='$rulesID', pressrun_id='$pressrunid', inserter_request='$request', num_packages='$numpackages' WHERE id=$planid";
        $dbUpdate=dbexecutequery($sql);
        $error.=$dbUpdate['error'];
    }
    if ($error!='')
    {
        setUserMessage('There was a problem saving the plan.<br>'.$error,'error');
    } else {
        setUserMessage('Insert plan has been successfully saved.','success');
        
    }
    redirect("?action=list");
    
}

$Page->footer();             
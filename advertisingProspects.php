<?php
if($_POST['output']=='csv')
{
    include("includes/functions_db.php");
    include("includes/functions_common.php");
    if($_POST)
    {
        if($_POST['salesid']!=0)
        {
            $salesid=" AND sales_id=".$_POST['salesid']; 
        }
        if($_POST['revenue']!='')
        {
            if($_POST['revenue']=='all')
            {
                $searchrevenue=" AND ytd_revenue>0";
            } elseif($_POST['revenue']=='0')
            {
                $searchrevenue=" AND ytd_revenue=0";
            } else {
                $searchrevenue=" AND ytd_revenue>".$_POST['revenue'];
            } 
        }
        if($_POST['category']!='all')
        {
            if($_POST['category']=='blank')
            {
                $searchcategory=" AND category=''";
            } else {
                $searchcategory=" AND category='".$_POST['category']."'";
            }
        }
        if($_POST['zone']!='all')
        {
            $searchzone=" AND zone_id='".$_POST['zone']."'";
        }
        if($_POST['near'])
        {
            //calculate bounding box around specified address
            //first need to geolocate given address
            if($_POST['address']!='')
            {
                $base_url = "http://maps.google.com/maps/geo";
                $out="&output=csv&sensor=false";
                $address = urlencode($_POST["address"]);
                $url = $base_url . "?q=" .$address.$out;
                //$csv = file_get_contents($url);
                $temp=get_web_page($url);
                $csv=$temp['content'];

                //print "CSV RESULT: <a href='$url' target='_blank'>$url</a> -- $csv<br />\n";
                //print "Attempting to geocode $address with url of $url<br>->Result is ";
                $csvSplit=explode(",",$csv);
                $status=$csvSplit[0];
                $accuracy=$csvSplit[1];
                $lat=$csvSplit[2];
                $lon=$csvSplit[3]; 
            }
            $edison = GeoLocation::fromDegrees($lat, $lon);

            // get bounding coordinates 5 kilometers from location;
            $coordinates = $edison->boundingCoordinates(5,  6371.01);

            $minLat=$coordinates[0]->degLat;
            $minLon=$coordinates[0]->degLon;
            $maxLat=$coordinates[1]->degLat;
            $maxLon=$coordinates[1]->degLon;
        }
        
        
        $accountsql="SELECT * FROM advertising_account_mapping 
        WHERE lat>=$minLat AND lat<=$maxLat AND lon>=$minLon AND lon<=$maxLon $salesid $searchcategory $searchrevenue $searchzone";
    } else {
        $searchcategory=" AND category<>''";
        $searchrevenue=" AND ytd_revenue>0";
        $accountsql="SELECT * FROM advertising_account_mapping 
        WHERE lat<>'' AND lon<>'' $searchcategory $searchrevenue";
    }

    header('Content-Type: text/csv'); // plain text file
    header('Content-Disposition: attachment; filename="prospects_'.date("Y-m-d"));
         
    print "Category,";
    print "Account Name,";
    print "Addresss,";
    print "City,";
    print "Phone,";
    print "Contact,";
    print "Revenue,";
    print "Sales\n";

    $dbAccounts=dbselectmulti($accountsql);
    if($dbAccounts['numrows']>0)
    {
        foreach($dbAccounts['data'] as $item)
        {
            print "$item[category],";
            print "$item[account_name],";
            print "$item[address],";
            print "$item[city],";
            print "$item[phone],";
            print "$item[contact],";
            print "$item[ytd_revenue],";
            print $sales[$item['sales_id']]."\n";
        }
    }
    if($_POST['prospects'])
    {
        if($_POST['code'] && count($_POST['code'])>0)
        {
            foreach($_POST['code'] as $code)
            {
                $naicodes[]=$niccodes[$code];
                //print "// $code<br />\n";
            }
            $naicodes=implode(",",$naicodes);
            if($naicodes!='' && $naicodes!=0)
            {
                $searchcode=" AND naics_code IN($naicodes)";
            }
        } else {
            $searchcode='';
        }
        if($_POST['siccode'] && count($_POST['siccode'])>0)
        {
            $siccode=implode(",",$_POST['siccode']);
            if($siccode!='' && $siccode!=0)
            {
                $searchsiccode=" AND sic_code IN($siccode)";
            }
        } else {
            $searchsiccode='';
        }
        if($_POST['revenue']!='')
        {
            if($_POST['revenue']=='all')
            {
                $searchrevenue="";
            } else {
                $searchrevenue=" AND revenue>=".$_POST['revenue'];
            } 
        }
        if($_POST['zone']!='all')
        {
            $searchzone=" AND territory_id=".$_POST['zone'];
        }
        $sql="SELECT * FROM prospects WHERE territory_tested=1 $searchcode $searchsiccode $searchrevenue $searchzone";
        print "<!-- prospects sql is $sql -->\n";
        $dbProspects=dbselectmulti($sql);
        if($dbProspects['numrows']>0)
        {
            foreach($dbProspects['data'] as $prospect)
            {
                    
                print $niccodenames[$prospect['naics_code']].",";
                print "$prospect[account_name],";
                print "$prospect[address],";
                print "$prospect[city],";
                print "$prospect[phone],";
                print "$prospect[contact_name],";
                print "$prospect[revenue],";
                print "None</td>\n";
            }
        }
    }                 
    
    
    
} else {
    include("includes/boot.php");
       
    error_reporting(E_ERROR);
    global $sales;
    $sql="SELECT * FROM core_preferences";
    $dbPrefs=dbselectsingle($sql);
    $prefs=$dbPrefs['data'];

    $sales[0]='All sales staff';

    $sql="SELECT * FROM advertising_zones ORDER BY name";
    $dbTypes=dbselectmulti($sql);
    if($dbTypes['numrows']>0)
    {
        $minZone=$dbTypes['data'][0]['id'];
        foreach($dbTypes['data'] as $loc)
        {
            $zones[$loc['id']]=stripslashes($loc['name']);
            $legend[]=array("color"=>$loc['color'],"name"=>$loc['name']);      
        }
    } else {
       $minZone=0;
    }
    $zones['all']='All zones';

     
    $sql="SELECT DISTINCT(category) FROM advertising_account_mapping ORDER BY category";
    $dbCats=dbselectmulti($sql);
    $categories['all']='Show all categories'; 
    if($dbCats['numrows']>0)
    {
        foreach($dbCats['data'] as $cat)
        {
            if(trim($cat['category'])!='')
            {
                $categories[$cat['category']]=$cat['category'];
            } else {
                $categories['blank']='No Category';
            }
        }    
    }

    $sql="SELECT * FROM naics_codes ORDER BY naics_name";
    $dbNCodes=dbselectmulti($sql);
    $codes[0]="Select NAICS code";
    if($dbNCodes['numrows']>0)
    {
        foreach($dbNCodes['data'] as $ncode)
        {
            $codes[$ncode['id']]=$ncode['naics_name'];   
            $niccodes[$ncode['id']]=$ncode['naics_code'];   
            $niccodenames[$ncode['naics_code']]=$ncode['naics_name'];   
        }
    }
         
    $sql="SELECT * FROM sic_codes ORDER BY sic_name";
    $dbCodes=dbselectmulti($sql);
    $siccodes[0]="Select SIC code";
    if($dbCodes['numrows']>0)
    {
        foreach($dbCodes['data'] as $code)
        {
            $siccodes[$code['sic_code']]=$code['sic_name'];
        }
    }

    if($_POST['category'])
    {
        $category=$_POST['category'];
    } else {
        $category='all';
    } 
    if($_POST['revenue'])
    {
        $revenue=$_POST['revenue'];
    } else {
        $revenue='all';
    } 
    if($_POST['zone'])
    {
        $zone=$_POST['zone'];
    } else {
        $zone='all';
    }
    if($_POST['code'])
    {
        $code=$_POST['code'];
    } else {
        $code='0';
    }
    if($_POST['siccode'])
    {
        $siccode=$_POST['siccode'];
    } else {
        $siccode='0';
    }
    if($_POST['prospects'])
    {
        $showprospects=1;
    } 
    if($_POST['propects_only'])
    {
        $prospectsonly=1;
    } 

    $revenues=array("all"=>"Any revenue","0"=>"No revenue","1000"=>"Over \$1000","2500"=>"Over \$2500","5000"=>"Over \$5000","10000"=>"Over \$10000");

    print "<div style='padding:10px;border: 1px solid black;background-coloe:#efefef;margin-bottom:10px;'>\n";
    print "<form method=post class='form-horizontal'>\n";
    print "<div style='float:left;width:500px;'>\n";
        make_select('salesid',$sales[$_POST['salesid']],$sales,'Sales Rep');
        make_select('zone',$zones[$zone],$zones,'Territory');
        make_select('category',$categories[$category],$categories,'Category');   
        make_select('output','screen',array('screen'=>"Display on screen",'csv'=>"Output as CSV"),'Category');   
        
    print "</div>\n";
    
    print "<div style='float:left;width:500px;'>\n";
        make_select('revenue',$revenues[$revenue],$revenues,'YTD Revenue');
        make_checkbox('prospects',$showprospects,'Include Prospects','Include prospects from MSG Data');
        print "<div class='label'>NIC Code</div><div class='input'>";
        print "<small>Requires prospects to be checked</small><br />";
        print "<select id='code' name='code' multiple style='width:300px;'>";
        if(count($codes)>0)
        {
            foreach($codes as $cid=>$cname)
            {
                $selected=false;
                if(array_key_exists($cid,$_POST['code']))
                {
                    $selected=true;
                }
                print "<option value='$cid' ".($selected? 'selected':'').">$cname</option>\n";   
            }
        }
        print "</select>\n";
        print "</div><div class='clear'></div>\n";
        
        print "<div class='label'>SIC Code</div><div class='input'>";
        print "<small>Requires prospects to be checked</small><br />";
        print "<select id='siccode' name='siccode' multiple style='width:300px;'>";
        if(count($siccodes)>0)
        {
            foreach($siccodes as $sid=>$sname)
            {
                $selected=false;
                if(array_key_exists($sid,$_POST['siccode']))
                {
                    $selected=true;
                }
                print "<option value='$sid' ".($selected? 'selected':'').">$sname</option>\n";   
            }
        }
        print "</select>\n";
        print "</div><div class='clear'></div>\n";
        make_checkbox('propects_only',$prospectsonly,'Prospects Only','ONLY show prospects');
        make_checkbox('near',$near,'Prospects Near','ONLY show prospects within specified distance of address');
        make_number('distance',$distance,'Distance','Distance to find prospects from target address');
        make_text('address',$address,'Address','Full address with street, city and zip',20);
        print "</div><div class='clear'></div>\n";
        make_submit('submit','Filter Results');
    print "</form>\n";
print "</div>\n";

if($_POST)
{
    if($_POST['salesid']!=0)
    {
        $salesid=" AND sales_id=".$_POST['salesid']; 
    }
    if($_POST['revenue']!='')
    {
        if($_POST['revenue']=='all')
        {
            $searchrevenue=" AND ytd_revenue>0";
        } elseif($_POST['revenue']=='0')
        {
            $searchrevenue=" AND ytd_revenue=0";
        } else {
            $searchrevenue=" AND ytd_revenue>".$_POST['revenue'];
        } 
    }
    if($_POST['category']!='all')
    {
        if($_POST['category']=='blank')
        {
            $searchcategory=" AND category=''";
        } else {
            $searchcategory=" AND category='".$_POST['category']."'";
        }
    }
    if($_POST['zone']!='all')
    {
        $searchzone=" AND zone_id='".$_POST['zone']."'";
    }
    if($_POST['near'])
    {
        //calculate bounding box around specified address
            //first need to geolocate given address
            if($_POST['address']!='')
            {
                $base_url = "http://maps.google.com/maps/geo";
                $out="&output=csv&sensor=false";
                $address = urlencode($_POST["address"]);
                $url = $base_url . "?q=" .$address.$out;
                //$csv = file_get_contents($url);
                $temp=get_web_page($url);
                $csv=$temp['content'];

                //print "CSV RESULT: <a href='$url' target='_blank'>$url</a> -- $csv<br />\n";
                //print "Attempting to geocode $address with url of $url<br>->Result is ";
                $csvSplit=explode(",",$csv);
                $status=$csvSplit[0];
                $accuracy=$csvSplit[1];
                $lat=$csvSplit[2];
                $lon=$csvSplit[3]; 
            }
            $edison = GeoLocation::fromDegrees($lat, $lon);

            // get bounding coordinates 5 kilometers from location;
            $coordinates = $edison->boundingCoordinates(5,  6371.01);

            $minLat=$coordinates[0]->degLat;
            $minLon=$coordinates[0]->degLon;
            $maxLat=$coordinates[1]->degLat;
            $maxLon=$coordinates[1]->degLon;
    }
    
      $accountsql="SELECT * FROM advertising_account_mapping 
        WHERE lat>=$minLat AND lat<=$maxLat AND lon>=$minLon AND lon<=$maxLon $salesid $searchcategory $searchrevenue $searchzone";
} else {
    $searchcategory=" AND category<>''";
    $searchrevenue=" AND ytd_revenue>0";
    $accountsql="SELECT * FROM advertising_account_mapping WHERE lat<>'' AND lon<>'' $searchcategory $searchrevenue";
}

print "<table class='report'>\n";
print "<tr>";
    print "<th>Category</th>";
    print "<th>Account Name</th>";
    print "<th>Addresss</th>";
    print "<th>City</th>";
    print "<th>Phone</th>";
    print "<th>Contact</th>";
    print "<th>Revenue</th>";
    print "<th>Sales</th>";
print "</tr>\n";

$dbAccounts=dbselectmulti($accountsql);
if($dbAccounts['numrows']>0)
{
    foreach($dbAccounts['data'] as $item)
    {
        print "<tr>";
        print "<td>$item[category]</td>";
        print "<td>$item[account_name]</td>";
        print "<td>$item[address]</td>";
        print "<td>$item[city]</td>";
        print "<td>$item[phone]</td>";
        print "<td>$item[contact]</td>";
        print "<td>$item[ytd_revenue]</td>";
        print "<td>".$sales[$item['sales_id']]."</td>";
        print "</tr>\n";
    }
}
if($_POST['prospects'])
{
    if($_POST['code'] && count($_POST['code'])>0)
    {
        foreach($_POST['code'] as $code)
        {
            $naicodes[]=$niccodes[$code];
            //print "// $code<br />\n";
        }
        $naicodes=implode(",",$naicodes);
        if($naicodes!='' && $naicodes!=0)
        {
            $searchcode=" AND naics_code IN($naicodes)";
        }
    } else {
        $searchcode='';
    }
    if($_POST['siccode'] && count($_POST['siccode'])>0)
    {
        $siccode=implode(",",$_POST['siccode']);
        if($siccode!='' && $siccode!=0)
        {
            $searchsiccode=" AND sic_code IN($siccode)";
        }
    } else {
        $searchsiccode='';
    }
    if($_POST['revenue']!='')
    {
        if($_POST['revenue']=='all')
        {
            $searchrevenue="";
        } else {
            $searchrevenue=" AND revenue>=".$_POST['revenue'];
        } 
    }
    if($_POST['zone']!='all')
    {
        $searchzone=" AND territory_id=".$_POST['zone'];
    }
    $sql="SELECT * FROM prospects WHERE territory_tested=1 $searchcode $searchsiccode $searchrevenue $searchzone";
    print "<!-- prospects sql is $sql -->\n";
    $dbProspects=dbselectmulti($sql);
    if($dbProspects['numrows']>0)
    {
        foreach($dbProspects['data'] as $prospect)
        {
            print "<tr>";
                
                print "<td>".$niccodenames[$prospect['naics_code']]."</td>";
                print "<td>$prospect[account_name]</td>";
                print "<td>$prospect[address]</td>";
                print "<td>$prospect[city]</td>";
                print "<td>$prospect[phone]</td>";
                print "<td>$prospect[contact_name]</td>";
                print "<td>$prospect[revenue]</td>";
                print "<td>None</td>";
            print "</tr>\n"; 
        }
    }
}
 print "</table>\n";  
 $Page->footer();  
}
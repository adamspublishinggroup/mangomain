<?php
//<!--VERSION: .9 **||**-->
if ($_POST['output']!='csv')
{
    $render='csv';
    include("../includes/boot.php") ;
} else {
    require ('includes/boot.php');
}
global $newsprintVendors;

//paper types
$sql="SELECT * FROM paper_types WHERE site_id=".SITE_ID." AND status<>99 ORDER BY common_name";
$dbPaper=dbselectmulti($sql);
$papertypes=array();
$papertypes[0]="Type";
if ($dbPaper['numrows']>0)
{
    foreach($dbPaper['data'] as $paper)
    {
        $papertypes[$paper['id']]=$paper['common_name'];
    }
}

//paper sizes
$sql="SELECT * FROM paper_sizes WHERE site_id=".SITE_ID." AND status<>99 ORDER BY width ASC";
$dbSizes=dbselectmulti($sql);
$papersizes=array();
$papersizes[0]="Size";
if ($dbSizes['numrows']>0)
{
    foreach($dbSizes['data'] as $size)
    {
        $papersizes[$size['id']]=$size['width'];
    }
}
$enddate=date("Y-m-d");
$startdate=date("Y")."-1-1";
$vendor="";
$status=""; 
if ($_POST['output']!='csv')
{
    print "<form class='form-horizontal' method=post>\n";
        make_select('vendor',$newsprintVendors[$_POST['vendor']],$newsprintVendors,'Vendor');
        make_select('osource',$ordersources[$_POST['osource']],$ordersources,'Order Source');
        make_select('ptype',$papertypes[$_POST['ptype']],$papertypes, 'Paper type');
        make_select('psize',$papersizes[$_POST['psize']],$papersizes,'Paper size');
        make_date('startdate',$startdate,'Start Date');
        make_date('enddate',$enddate,'End Date');
        make_select('output','Screen',array('screen'=>'Screen','csv'=>'Excel'),'Output');
        make_submit('submit','Generate Report');
        
    print "</form>\n";
}
if ($_POST) {
    $enddate=$_POST['enddate'];
    $startdate=$_POST['startdate'];
    if ($_POST['psize']!=0){
        $psize=$papersizes[$_POST['psize']];
    } else {
        $psize="";
    }
     if ($_POST['ptype']!=0){
        $ptype=$papertypes[$_POST['ptype']];
    } else {
        $ptype="";
    }
    $osource=$_POST['osource'];
    if ($osource=='0'){$osource='';}
    $output=$_POST['output'];
    $totalconsumecount=0;
    $totalconsumeweight=0;
    $report='';
    $exceldata='';
    if ($_POST['vendor']!=0)
    {
        $vid=$_POST['vendor'];
        $vname=$newsprintVendors[$vid];
        $vrolls=vendor_rolls($vid,$vname,$startdate,$enddate,$osource,$psize,$ptype,$output);
        $report.=$vrolls['output'];
        $exceldata=$vrolls['exceldata'];
        $totalconsumecount+=$vrolls['vtccount'];
        $totalconsumeweight+=$vrolls['vtcweight'];    
    } else {
        foreach($newsprintVendors as $vid=>$vname)
        {
            if ($vid!=0)
            {
                $vrolls=vendor_rolls($vid,$vname,$startdate,$enddate,$osource,$psize,$ptype,$output);
                $report.=$vrolls['output'];
                $exceldata.=$vrolls['exceldata'];
                $totalconsumecount+=$vrolls['vtccount'];
                $totalconsumeweight+=$vrolls['vtcweight'];
            }
            
        }
    }
    
    if ($output=='csv')
    {
        $title=str_replace(":","","Newsprint_Consumption - ".date("Y-m-d H:i"));
        $title=str_replace(" ","_",$title);
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=$title.xls;");
        header("Content-Type: application/ms-excel");
        header("Pragma: no-cache");
        header("Expires: 0");
        $tablestart="<?xml version='1.0'?>
        <?mso-application progid='Excel.Sheet'?>
        <Workbook xmlns='urn:schemas-microsoft-com:office:spreadsheet' xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:x='urn:schemas-microsoft-com:office:excel' xmlns:ss='urn:schemas-microsoft-com:office:spreadsheet' xmlns:html='http://www.w3.org/TR/REC-html40'>
        <Worksheet ss:Name='Consumption Report'>
        <Table>";
        $tableend="</Table></Worksheet></Workbook>";
        echo $tablestart.$exceldata.$tableend; 
    } else {
        print "<table class='grid'>\n<tr>\n<th><a href='#' onclick='window.print();'><i class='fa fa-print'></i>Print</a></th><th colspan=3>$reportname</th>\n</tr>\n";
        print $report;
        print "<tr><th colspan=3>GRAND TOTALS</th><th colspan=2>Total Rolls</th></tr>\n";
        print "<tr><th colspan=3>&nbsp;</th><th>Rolls</th><th>Weight</th></tr>\n";
               
        print "<tr></tr>\n";
        print "<tr><td colspan=2>Totals</td><td>$totalconsumecount</td><td>".sprintf("%.3f",$totalconsumeweight/1000).' MT'."</td>
        </tr>\n";
        
        print "</table>\n";
        print "</div>
        </body>
        </html>";
        
    }
}  


function vendor_rolls($vendorid,$vendorname,$startdate,$enddate,$source='',$size='',$type='',$output='screen')
{
    global $siteID;
    if ($output=='csv')
   {
       $tablestart="";
       $tableend="";
       $rowstart="<Row>";
       $rowstart="<Row>";
       $rowend="</Row>";
       $headstart="<Cell>";
       $headend="</Cell>";
       $cellstart5="<Cell></Cell><Cell></Cell><Cell></Cell><Cell></Cell><Cell><Data ss:Type='String'>";
       $cellstart="<Cell><Data ss:Type='String'>";
       $cellend="</Data></Cell>";
       $break="\r\n";
   } else {
       
       $tablestart="<tr><th colspan=4>$vendorname</th>\n</tr>\n";
       $tableend="<tr><th colspan=4></th></tr>\n";
       $rowstart="<tr>";
       $rowend="</tr>\n";
       $cellstart5="<td></td><td></td><td></td><td></td><td>";
       $cellstart="<td>";
       $cellend="</td>";
       $headstart="<th>";
       $headend="</th>";
       $break="<br />\n";
   }
    $vtotalstartcount=0;
    $vtotalstartweight=0;
    $vtotalreceivecount=0;
    $vtotalreceiveweight=0;
    $vtotalconsumecount=0;
    $vtotalconsumeweight=0;
    $vtotalremaincount=0;
    $vtotalremainweight=0;
    $vtotalremainweight=0;
    $startstamp=strtotime($startdate);
    $endstamp=strtotime($enddate);
    if ($source!=''){$source=" AND B.order_source='$source'";}else{$source="";}
    if ($size!=''){$size=" AND A.roll_width='$size'";}else{$size="";}
    if ($type!=''){$type=" AND A.common_name='$type'";}else{$type="";}
    
    
    //lets start by getting all rolls that are marked as unconsumbed
    $sql="SELECT A.common_name, A.roll_weight, A.id, A.roll_tag, A.manifest_number, B.order_source FROM rolls A, orders B WHERE A.site_id=$siteID A.status=1 AND A.order_id=B.id AND B.vendor_id=$vendorid $source $size $type GROUP BY A.common_name";
    //print $sql."<br />";
    $dbRolls=dbselectmulti($sql);
    if ($dbRolls['numrows']>0)
    {
       $output=$tablestart;
       $output.=$rowstart.$headstart.'Source'.$headend;
       $output.=$headstart.'Type'.$headend;
       $output.=$headstart.'Roll Tag'.$headend;
       $output.=$headstart.'Manifest Number'.$headend;
       $output.=$headstart.'Roll Weight'.$headend;
       $output.=$rowend;
       
       $ctype='';
       $cwidth='';
       $consumecount=0;
       $consumeweight=0;
          foreach($dbRolls['data'] as $roll)
          {
              $ctype=$roll['common_name'];
              $osource=$roll['order_source'];
              $rolltag=$roll['roll_tag'];
              $rollweight=$roll['roll_weight'];
              $manifest=$roll['manifest_number'];
              $output.=$rowstart.$cellstart.$osource.$cellend;
              $output.=$cellstart.$ctype.$cellend;
              $output.=$cellstart.$rolltag.$cellend;
              $output.=$cellstart.$manifest.$cellend;
              $output.=$cellstart.sprintf("%.3f",$rollweight/1000).' MT'.$cellend;
              $vtotalconsumecount++;
              $vtotalconsumeweight+=$rollweight;
            

          }
       $output.=$tableend;
    }
    $output.=$rowstart.$cellstart.$cellend.$cellstart.$cellend.$cellstart.$cellend;
    $output.=$cellstart.$vtotalconsumecount.$cellend;
    $output.=$cellstart.sprintf("%.3f",$vtotalconsumeweight/1000).' MT'.$cellend;
    return array("output"=>$output,"vtccount"=>$vtotalconsumecount,"vtcweight"=>$vtotalconsumeweight);
                

}
if($render=='csv')
{
    dbclose();
} else {
    $Page->footer();
}
<?php
if($_POST['output']=='csv')
{
    $render='csv';
    include("../includes/boot.php");
    show_report('csv');
} else {
    include("../includes/boot.php");
    global $pubs;
    $pubs[0]='All publications';
    if($_POST)
    {
        $pub=$_POST['pub'];
        $start=$_POST['start'];
        $end=$_POST['end'];
    } else {
        $pub=0;
        $start=date("Y-m-d");
        $end=date("Y-m-d",strtotime("+1 week"));
    }
    print "<form method=post class='form-horizontal'>\n";
        make_select('pub',$pubs[$pub],$pubs,'Publication');
        make_date('start',$start,'Start Date');
        make_date('end',$end,'End Date');
        make_select('output','screen',array('screen'=>'Screen','csv'=>'Excel'),'Output');
        make_submit('submit','Generate Report');
    print "</form>\n";
    show_report('screen');
} 
  

function show_report($output)
{
    global $pubs;
    $pub=$_POST['pub'];
    $start=$_POST['start'];
    $end=$_POST['end'];
    
    $sql="SELECT * FROM accounts";
    $dbAccounts=dbselectmulti($sql);
    if($dbAccounts['numrows']>0)
    {
        foreach($dbAccounts['data'] as $account)
        {
            $accounts[$account['id']]=stripslashes($account['account_name']);
        }
    }
    
    $sql="SELECT * FROM inserters";
    $dbInserters=dbselectmulti($sql);
    if($dbInserters['numrows']>0)
    {
        foreach($dbInserters['data'] as $inserter)
        {
            $inserters[$inserter['id']]=stripslashes($inserter['inserter_name']);
        }
    }
    switch($output)
    {
        case 'csv':
            $tablestart="Package Date/Time,Pub Date,Package Name,Start Time,End Time,Run Time (hrs),Setup Time (hrs),
            Stations Used,Prepack Count,Produced Count,Insert Count,# Inserts >24pg,Total Pieces,Tab Pages,FTE Pre,FTE Post,Bundles,Pallets,
            Strap Cost,Shrinkwrap Cost,Labor Cost,Total Cost,Contract Speed,Average Speed (papers/hr)\n";
            $tableend='';
            $rowstart='';
            $highlightstart='';
            $rowend="\n";
            $newcell=",";
        break;
        
        case 'screen':
            $tablestart="<table class='report-clean-mango'>
            <tr><th style='width:60px;'>Package Date/Time</th>
            <th style='width:60px;'>Pub Date</th><th style='width:80px;'>Package Name</th><th>Start Time</th><th>End Time</th>
            <th>Run Time (hrs)</th><th>Setup Time (hrs)</th>
            <th>Stations Used</th><th>Prepack Count</th><th>Produced Count</th><th>Insert Count</th><th># Inserts >24pg</th><th>Total Pieces</th>
            <th>Tab Pages</th><th>FTE Pre</th><th>FTE Post</th><th>Bundles</th><th>Pallets</th>
            <th>Strap Cost</th><th>Shrinkwrap Cost</th><th>Labor Cost</th><th>Supply Cost</th>
            <th>Total Cost</th><th>Contract Speed</th><th>Average Speed (papers/hr)</th></tr>\n";
            
            $tableend="</table>\n";
            $highlightstart="<tr style='background-color:#fff000;border-top:2px solid black;margin-top:10px;'><td>";
            $rowstart='<tr><td>';
            $rowend="</td></tr>\n";
            $newcell="</td><td>";
        break;
    }
    if($pub==0)
    {
        $sql="SELECT * FROM jobs_inserter_packages WHERE package_startdatetime>='$start' AND package_stopdatetime<='$end' ORDER BY pub_id, package_date";
    } else {
        $sql="SELECT * FROM jobs_inserter_packages WHERE pub_id=$pub AND package_startdatetime>='$start' AND package_stopdatetime<='$end' ORDER BY pub_id, package_date";
    }
    $report='';
    $dbPackages=dbselectmulti($sql);
    if($dbPackages['numrows']>0)
    {
        $report.=$tablestart;
        foreach($dbPackages['data'] as $package)
        {
            //initialize variables
            $prepackCount = 0;
            $insertCount = 0;
            $pieceCount = 0;
            $stationCount = 0;
            $produceCount = 0; 
            $tabPages = 0; 
            $ftePre = 0; 
            $ftePost = 0; 
            $strapCost = 0; 
            $shrinkCost = 0; 
            $totalFee = 0; 
            $bigInserts = 0; //inserts with 24 or more tab pages
            
            
            /*
            For now, hard code in the statesman factors
            */
            $markup = 0.25;
            $speedNoPrepack = 10000;
            $speedPrepack = 8000;
            $minTimeSetup = 20;
            $headSetup = 3;
            $hoursBeforeBreak = 4;
            
            $insertersPerHead = 0.75; //0.5 after initial period
            $insertersPerPrepack = 2;
            $insertersProduce24page = 1;
            
            //array to hold number of 
            $postInsertingBodies[0] = 2.5;
            $postInsertingBodies[1] = 2.5;
            $postInsertingBodies[2] = 2.5;
            $postInsertingBodies[3] = 2.5;
            $postInsertingBodies[4] = 2.5;
            $postInsertingBodies[5] = 2.5;
            $postInsertingBodies[6] = 3.5;
            $postInsertingBodies[7] = 3.5;
            $postInsertingBodies[8] = 3.5;
            $postInsertingBodies[9] = 3.5;
            $postInsertingBodies[10] = 3.5;
            $postInsertingBodies[11] = 3.5;
            $postInsertingBodies[12] = 3.5;
            $postInsertingBodies[13] = 3.5;
            
            $extraBodyPrepack = 0.5;
            
            $forkliftRate = 10.0;
            $singleOpRate = 23.0;
            $doubleOpRate = 20.0;
            $staffingRate = 10.50;
            $tabPages = 0;
            $strapCostPerBundle = 0.0066;  //.0033 per foot cost * 2'
            $wrapCostPerPallet = 0.30; // .0046 per foot cost * 64'
            
            $annualOverhead = 81252;
            
            //lets get the inserter rundata
            $sql="SELECT * FROM jobs_inserter_rundata WHERE package_id = '$package[id]'";
            $dbRundata = dbselectsingle($sql);
            $runData = $dbRundata['data'];
            
            $produceCount=$runData['packages_built'];
            
            $inserts='';
            $jacket='Not set';
            $day=date("D",strtotime($package['package_date']));
            $date=date("d-M H:i",strtotime($package['package_date']));
            $pubDate=date("d-M",strtotime($package['pub_date']));
            $packageName=$package['package_name'];
            
            $runStart=date("m/d H:i",strtotime($runData['runstart_one']));
            $runStop=date("m/d H:i",strtotime($runData['runfinish_one']));
            
            //time is in seconds, so divide by 3600, round to two decimal places
            $runTime = round((strtotime($runData['runfinish_one']) - strtotime($runData['runstart_one']))/3600,2);
            
            //calculate ACTUAL run speed based on end time - start time
            $avgSpeed = round(($produceCount / $runTime),2);
            
            //setup time
            $setupStart=date("m/d H:i",strtotime($runData['setup_starttime']));
            $setupStop=date("m/d H:i",strtotime($runData['setup_stoptime']));
            //time is in seconds, so divide by 3600, round to two decimal places
            $setupTime = round((strtotime($runData['setup_stoptime']) - strtotime($runData['setup_starttime']))/3600,2);
            
            
            
            $jacketid=$package['jacket_insert_id'];
            $inserterid=$package['inserter_id'];
            $inserter=$inserters[$inserterid];
            $request=$package['inserter_request'];
            $pubid=$package['pub_id'];
            $pub=$pubs[$package['pub_id']];
            
            
            
            //get package station data
            $sql = "SELECT * FROM jobs_inserter_rundata_stations WHERE package_id = $package[id]";
            
            $dbPackageStationData = dbselectmulti($sql);
            //lets do some calculations based on packageStationData
            if($dbPackageStationData['numrows']>0)
            {
                foreach($dbPackageStationData['data'] as $stationData)
                {
                    if($stationData['insert_id']!=0)
                     {
                         $insertCount++; //this variable holds number of inserts
                         $pieceCount+=$stationData['quantity'];
                         $stationCount++; //this variable holds number of stations used
                     } elseif($stationData['prepack_id']!=0)
                     {
                         $prepackCount++; // holds number of prepacks
                         $insertCount++;
                         $pieceCount+=$stationData['quantity'];
                         $stationCount++;
                     }
                }
            }
            
            
            //get settings for this package
            $sql="SELECT * FROM jobs_inserter_packages_settings WHERE package_id=$package[id]";
            $dbSettings=dbselectsingle($sql);
            $settings=$dbSettings['data'];
            $bundleSize=$settings['copies_per_bundle'];
            
            //pull in the list of inserts for this package
            $sql="SELECT A.*, B.hopper_number, C.pages FROM jobs_packages_inserts A, inserters_hoppers B, inserts C 
            WHERE C.id = A.insert_id AND A.package_id=$package[id] AND A.hopper_id=B.id ORDER BY B.hopper_number ASC";
            $dbInserts=dbselectmulti($sql);
            if($dbInserts['numrows']>0)
            {
                foreach($dbInserts['data'] as $insert)
                {
                    if($insert['pages'] > 24)
                    {
                        $bigInserts ++; //this variable holds number of inserts with page count > 24
                        
                    }
                    $tabPages+=$insert['pages'];
                } 
            } else {
                $inserts='None set at this time.';
            }
            
            //set the running speed based on prepack couint
            if($prepackCount<2)
            {
                $runningSpeed = $speedNoPrepack;
            } else {
                $runningSpeed = $speedPrepack;
            }
            
            //calculate payroll
            //operator setup
            if($insertCount>0)
            {
                $opSetup = ($insertCount) * $headSetup; //keiths formula adds one, but we have the jacket in our insert list
                if($opSetup < $minTimeSetup) $opSetup = $minTimeSetup; //if it's less than the minimum, set to minimum
            } else {
                $opSetup = 0; //should not happen since that would not be a run
            }
            //add second operator if has prepacks
            if($prepackCount > 0 )
            {
                $opSetup += $minTimeSetup; // not sure why we are just adding 20 minutes for second operator, but that's what it is
            }
            
            //operator runtime
            $opHours = round(($produceCount/$runningSpeed),2);
            if($prepackCount>0)
            {
                //add extra operator time for having a prepack
                $opHours += round(($produceCount/$runningSpeed),2);
            }
            
            //cost differs if we have 1 or 2 operators
            if($prepackCount>0)
            {
                $opCost = $opHours * $doubleOpRate;  //since we have a prepack, we have two operators
            } else {
                $opCost = $opHours * $singleOpRate;  // no prepack - only one operator
            }
            
            //inserter staffing
            $staffingInserter = 1; // jacket station
            $staffingInserter += $prepackCount * $insertersPerPrepack; //two people for each prepack
            $staffingInserter += $bigInserts; // # of inserts over 24
            $staffingInserter += ceil(($insertCount - $bigInserts - $prepackCount -1) / $insertersPerHead); //(total insert count - # of inserts more than 24 pages - # of prepack - jacket station insert) divided by number of inserters per head
            
            //credit back one hour if we have a second operator (if prepackCount >0)
            if($prepackCount > 0) $staffingInserter --;
            
            //add in stackdown time
            $staffingInserter += $postInsertingBodies[$insertCount]; //from array above
            
            //add in .5 bodies if insert count >=12
            if($insertCount>=12) {$staffingInserter += 0.5;}
            
            //clock in / setup/ clean up time - 20 minutes per person (referenced "minTimeSetup"
            $cleanupTime = $staffingInserter * $minTimeSetup;
            
            $staffingHours = $staffingInserter * round(($produceCount/$runningSpeed),2); //now we multiply number of people times the number of papers produced / set run speed
            
            //add the cleanup time in minutes / 60 
            $staffingHours = $staffingHours + round(($cleanupTime / 60),2);
            
            //multiply staffing hours * staffing rate
            $staffingCost = round($staffingHours * $staffingRate,2);
            
            //add operator cost + staffing cost
            $totalLabor = $opCost + $staffingCost;
            
            $bundles = $runData['total_bundles']; //number of bundles
            $pallets =  $runData['pallets'];  // number of pallets
            $strapCost = round($strapCostPerBundle * $bundles,2); //multiple cost & number
            $wrapCost = round($wrapCostPerPallet * $pallets,2);//multiple cost & number
            $totalSupply = $strapCost + $wrapCost; //add strap and shrink wrap
            
            $totalFee = round(($totalLabor * (1+$markup)) + $totalSupply,2); //total labor * markup + supplies
            
            //$tablestart="Day,Package Date/Time,Pub Date,Package Name\n";
             
            $report.=$rowstart;
            $report.=$date;
            $report.=$newcell.$pubDate;
            $report.=$newcell.$packageName; 
            
            //Start Time, End Time, Run Time (min),Stations Used,Insert Count,Total Pieces,Station Fee,Piece Fee,Total
            
            $report.=$newcell.$runStart; 
            $report.=$newcell.$runStop; 
            $report.=$newcell.$runTime; 
            $report.=$newcell.$setupTime; 
            $report.=$newcell.$stationCount; 
            $report.=$newcell.$prepackCount; 
            
            $report.=$newcell.$produceCount; 
            $report.=$newcell.$insertCount; 
            $report.=$newcell.$bigInserts; 
            $report.=$newcell.$pieceCount; 
            $report.=$newcell.$tabPages; 
            $report.=$newcell.$ftePre; 
            
            $report.=$newcell.$ftePost; 
            $report.=$newcell.$bundles; 
            $report.=$newcell.$pallets; 
            $report.=$newcell.$strapCost; 
            $report.=$newcell.$wrapCost; 
            $report.=$newcell.$totalLabor; 
            $report.=$newcell.$totalSupply; 
            
            $report.=$newcell.$totalFee; 
            $report.=$newcell.$runningSpeed; 
            $report.=$newcell.$avgSpeed; 
            
                      
            $report.=$rowend;
        }
        $report.=$tableend;
        
        if($output=='csv')
        {
            $filename="packageReport-".date("Y-m-d",strtotime($start)).".csv";
            header('Content-Type: text/csv'); // csv text file
            header('Content-Disposition: attachment; filename="'.$filename.'"');
        }
        print $report;
    } else {
        print "Sorry, there are no packages matching those search criteria.";
    }
}
  
if($render=='csv')
{
    dbclose();
} else {
    $Page->footer();
}
<?php

if($_POST['output']=='csv')
{
    $render='csv';
    include("../includes/boot.php");
    show_report('csv');
} else {
    
    include("../includes/boot.php");
    global $pubs;
    if($_POST)
    {
        $pub=$_POST['pub'];
        $start=$_POST['start'];
        $end=$_POST['end'];
    } else {
        $pub=0;
        $start=date("Y-m-d");
        $end=date("Y-m-d",strtotime("+1 week"));
    }
    print "<form method=post class='form-horizontal'>\n";
        make_select('pub',$pubs[$pub],$pubs,'Publication');
        make_date('start',$start,'Start Date');
        make_date('end',$end,'End Date');
        make_select('output','screen',array('screen'=>'Screen','csv'=>'Excel'),'Output');
        make_submit('submit','Generate Report');
    print "</form>\n";
    if($_POST) show_report('screen');
} 
  

function show_report($output)
{
    global $pubs;
    $pub=$_POST['pub'];
    $start=$_POST['start'];
    $end=$_POST['end'];
    $sql="SELECT id, account_name FROM accounts WHERE account_advertiser=1";
    $dbAccounts=dbselectmulti($sql);
    if($dbAccounts['numrows']>0)
    {
        foreach($dbAccounts['data'] as $account)
        {
            $accounts[$account['id']]=stripslashes($account['account_name']);
        }
    }
    switch($output)
    {
        case 'csv':
            $tablestart="Day,Pub Date,Pub,Control #,Account Name,Std Pages,Real Pages,Description,Request,Zoning,Receive Date,Receive Qty,Receive Type/Qty,ID\n";
            $tableend='';
            $rowstart='"';
            $highlightstart='"';
            $rowend="\"\n";
            $newcell="\",\"";
        break;
        
        case 'screen':
            $tablestart="<div class='container'><div class='row'><div class='col-sm-12'><table class='table table-striped table-bordered'>
            <tr><th>Day</th><th style='width:60px;'>Pub Date</th><th style='width:60px;'>Pub</th><th style='width:60px;'>Control #</th><th>Account Name</th><th>Std Pages</th><th>Tab Pages</th><th>Description</th><th>Request</th><th>Zoning</th><th>Receive Date</th><th>Receive Qty</th><th>Receive Type/Qty</th><th>ID</th></tr>\n";
            $tableend="</table></div></div></div>\n";
            $highlightstart="<tr style='background-color:#fff000;border-top:2px solid black;margin-top:10px;'><td>";
            $rowstart='<tr><td>';
            $rowend="</td></tr>\n";
            $newcell="</td><td>";
        break;
    }
    $sql="SELECT A.*, B.pub_id, B.run_id, B.insert_quantity, B.insert_date FROM inserts A, inserts_schedule B 
    WHERE A.id=B.insert_id AND B.pub_id=$pub AND B.insert_date>='$start' AND B.insert_date<='$end' AND B.killed=0 ORDER BY insert_date ASC";
    $dbInserts=dbselectmulti($sql);
    if($dbInserts['numrows']>0)
    {
        $report.=$tablestart;
        $cdate=date("d-M",strtotime($start));
        $first=true;
        foreach($dbInserts['data'] as $insert)
        {
            $day=date("D",strtotime($insert['insert_date']));
            $date=date("d-M",strtotime($insert['insert_date']));
            
            if($insert['weprint_id']!=0)
            {
                $account="INHOUSE - ".$pubs[$insert['pub_id']];
            } else {
                $account=$accounts[$insert['advertiser_id']];
            }
            $pub = $pubs[$insert['pub_id']];
            $controlnumber=$insert['control_number'];
            $productPages=$insert['product_pages'];
            $pages=$insert['standard_pages'];
            $tagline=stripslashes($insert['insert_tagline']);
            $insertRequest=$insert['insert_quantity'];
            
            if($insert['received']!=0)
            {
                $receiveDate=date("m/d/Y",strtotime($insert['receive_date']));
            } else {
                $receiveDate='Not received';
            }
            $receiveQty=$insert['receive_count'];
            $receiveType=$insert['ship_quantity'].' '.$insert['ship_type'];
            if($date!=$cdate || $first)
            {
                $cdate=$date;
                $report.=$highlightstart;
                $first=false; 
            } else {
                $report.=$rowstart;
            }
            
            $zones = "Zones: ";
            //get zones for this insert
            $sql="SELECT B.zone_label FROM inserts_zoning A, publications_insertzones B WHERE A.zone_id=B.id AND A.schedule_id=$insert[schedule_id] AND A.insert_id=$insert[id]";
            $dbZones = dbselectmulti($sql);
            if($dbZones['numrows']>0)
            {
                foreach($dbZones['data'] as $zone)
                {
                    $zones.=" $zone[zone_label] |";
                }
            } 
            
            
            
            $report.=$day;
            $report.=$newcell.$date;
            $report.=$newcell.$pub;
            $report.=$newcell.$controlnumber;
            $report.=$newcell.$account;
            $report.=$newcell.$pages;
            $report.=$newcell.$productPages;
            $report.=$newcell.$tagline;
            $report.=$newcell.$insertRequest;
            $report.=$newcell.$zones;
            $report.=$newcell.$receiveDate;
            $report.=$newcell.$receiveQty;
            $report.=$newcell.$receiveType;
            if($output!='csv')
            {
                $report.=$newcell."<a href='/inserts.php?action=edit&insertid=".$insert['id']."' target='_blank'>".$insert['id']."</a>";
            } else {
                $report.=$newcell.$insert['id'];
            }
            $report.=$rowend;
        }
        $report.=$tableend;
        
        if($output=='csv')
        {
            $filename="insertReport-".$pubs[$pub].'-'.date("Y-m-d",strtotime($start)).".csv";
            header('Content-Type: text/plain'); // plain text file
            header('Content-Disposition: attachment; filename="'.$filename.'"');
        }
        print $report;
    } else {
        print "Sorry, there are no ads for those search criteria.";
    }
}
  
if($render=='csv')
{
    dbclose(); 
} else {
    $Page->footer();
}
<?php

    /*
     This is a report to get average spoils, draw, gross run time, net run time,
     gross speed, net speed on a particular pub/run combo
    */
    include("../includes/boot.php");
    global $pubs;
    if ($_POST)
    {
        $start=$_POST['start_date'];     
        $end=$_POST['end_date'];
        
        //get the config start day of week
        $schedulingStartDayOfWeek = $Prefs->schedulingStartDayOfWeek;
        
        while(date("w",strtotime($start))>$schedulingStartDayOfWeek)
        {
            $start = date("Y-m-d",strtotime($start." -1 day"));
        }
        
        //advance the end to the end of a week (dow = 6)
        while(date("w",strtotime($end))<6)
        {
            $end = date("Y-m-d",strtotime($end." +1 day"));
        }
        
        $report = new Report();

        $report->SetHeaders("Week Starting,Total Runs,Total Draw,Total Pages,Total Run Time (min)");

        $totalJobs = 0;
        $totalDraw = 0;
        $totalPages = 0;
        $totalTime = 0;
        $totalWeeks = 0;
        //now loop through each week and get the start and end date 
        $weekStartDate = $start;
        $weekEndDate = date("Y-m-d",strtotime($weekStartDate."+6 days"));
        while(strtotime($weekEndDate)<=strtotime($end))
        {
           $weekStartDate = date("Y-m-d",strtotime($weekStartDate."+1 week"));
           $weekEndDate = date("Y-m-d",strtotime($weekStartDate."+6 days"));
           
           $sql = "SELECT B.* FROM jobs A, job_stats B WHERE A.startdatetime>='$weekStartDate 00:01' AND A.startdatetime<='$weekEndDate 23:59' AND A.id = B.job_id ORDER BY A.startdatetime";
           $dbJobs = dbselectmulti($sql);
           $jobs = 0;
           $draw = 0;
           $pages = 0;
           $time = 0; 
           
           if($dbJobs['numrows']>0)
           {
               foreach($dbJobs['data'] as $job)
               {
                    $jobs++;
                    $draw+=$job['gross'];
                    $pages+=$job['pages_color']+$job['pages_bw'];
                    $time+=$job['run_time'];   
               }
           }
           $row = array($weekStartDate,$jobs,$draw,$pages,$time);
           $report->addRecord($row);
           
           $totalJobs+= $jobs;
           $totalDraw+= $draw;
           $totalPages+= $pages;
           $totalTime+= $time;
           $totalWeeks++;
        }
        
        if($totalWeeks!=0)
        {
            $avgJobs = round($totalJobs/$totalWeeks,0);
            $avgDraw = round($totalDraw/$totalWeeks,0);
            $avgPages = round($totalPages/$totalWeeks,0);
            $avgTime = $totalTime/$totalWeeks;
            
            $row = array("<b>Total Weeks:</b> $totalWeeks",
            "<b>Average Jobs:</b> ".$avgJobs, 
            "<b>Average Draw:</b> ".$avgDraw, 
            "<b>Average Pages:</b> ".$avgPages, 
            "<b>Average Time:</b> ".$avgTime
            );
            $report->addRecord($row); 
        }
        $report->output('screen');
        
    } else {
        $startDateYear=date("Y",strtotime("-1 month"));
        $startDateMonth=date("m",strtotime("-1 month"));
        $startDate=$startDateYear."-".$startDateMonth."-01";
        $endDate=$startDateYear."-".$startDateMonth.date("t",strtotime($startDate));
        print "<form method=post class='form-horizontal'>\n";
            make_date('start_date',$startDate,'Start print date');
            make_date('end_date',$endDate,'End print date');
            make_submit('submit','Generate Report');
        print "</form>\n";
    }
$Page->footer();
<?php
/* this report is intended to all you to pull a report of maintenance on any piece of equipment */

include("../includes/boot.php") ;
global $pubs, $siteID;
if ($_POST)
{
    generate_report();
} else {
    $equipmentid=$GLOBALS['pressid'];
    $sql="SELECT id, tower_name AS ename FROM press_towers WHERE press_id='$equipmentid' ORDER BY tower_order";
    $components[0]='All components';
   
    if($dbComponents['numrows']>0)
    {
        foreach($dbComponents['data'] as $component)
        {
            $component[$component['id']]=stripslashes($component['tower_name']);
        }
    }
    print "<form method=post class='form-horizontal'>\n";
    
    
    make_select('component_id',$components[0],$components,'Component','Select a tower or leave at all to 
    report on all towers.');
    
    make_date('start',date("Y-m-d",strtotime("-1 week")),'Start Time');
    make_date('end',date("Y-m-d"),'Stop Time');
        
    make_submit('submit','Run Report');
    print "</form>\n"; 
}

function generate_report()
{
    $componentid=$_POST['component_id'];
    $base=explode("_",$base);
    $start=$_POST['start'];
    $stop=$_POST['end'];
    press_report($componentid,$start,$stop);
     
}

function press_report($componentid,$start,$stop)
{
    $equipmentid=$GLOBALS['pressid'];
    
    if($componentid==0)
    {
        //grabbing all towers
        $sql="SELECT * FROM press_towers WHERE press_id=$equipmentid ORDER BY tower_order";
    } else {
        //specific tower selected
        $sql="SELECT * FROM press_towers WHERE press_id=$equipmentid AND id=$componentid";
    }
    if(debug){print "Tower sql is $sql<br>";}
    $dbTowers=dbselectmulti($sql);
    if($dbTowers['numrows']>0)
    {
        print "<table class='report grid'>\n";
        print "<tr>
        <th>Part Name</th>
        <th>Location</th>
        <th>Install Date</th>
        <th>Replacement Date</th>
        <th>Current Impressions</th>
        <th>Current Days</th>
        <th colspan=3>Status</th>
        </tr>";
        foreach($dbTowers['data'] as $tower)
        {
            //get the core component id
            $towertype=$tower['tower_type'];
            $sql="SELECT * FROM equipment_component WHERE equipment_id='$equipmentid' AND component_type='$towertype' AND equipment_type='press'";
            $dbBaseComponent=dbselectsingle($sql);
            $basecomponentid=$dbBaseComponent['data']['id'];
            
            print "<tr><th colspan=8>$tower[tower_name]</th></tr>\n";
            
                $partsql="SELECT A.*, B.component_id FROM equipment_part A, equipment_part_xref B 
                WHERE A.id=B.part_id AND B.equipment_id='$equipmentid' 
                AND B.component_id='$basecomponentid' AND B.equipment_type='press'";
                $dbPressParts=dbselectmulti($partsql);
                if(debug){print "&nbsp;&nbsp;&nbsp;&nbsp;Part sql $partsql<br>";}  
                
                print "<tr><td colspan=9></td></tr>\n";
                print "<tr><td colspan=9>Part Replacements</td></tr>\n";
                
                if ($dbPressParts['numrows']>0)
                {        
                    foreach($dbPressParts['data'] as $presspart)
                    {
                               //lets see if we can find an open instance of this part
                               $sql="SELECT * FROM part_instances WHERE part_id=$presspart[id] AND equipment_id='$equipmentid' AND component_id='$tower[id]' AND ((install_datetime>='$start' AND install_datetime<='$stop') OR (replace_datetime>='$start' AND replace_datetime<='$stop'))";
                               if(debug){print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instance sql $sql<br>";}  
                
                               $dbInstances=dbselectmulti($sql);
                               if ($dbInstances['numrows']>0)
                               {
                                   foreach($dbInstances['data'] as $instance)
                                   {
                                       print "<tr>";
                                       print "<td>$presspart[part_name]</td>";
                                       print "<td>$instance[sub_component_location]</td>";
                                       $installed=date("m/d/Y", strtotime($instance['install_datetime']));
                                       if($instance['replace_datetime']!='' && $instance['replace_datetime']!='Null')
                                       {
                                           $replaced=date("m/d/Y", strtotime($instance['replace_datetime']));
                                       } else {
                                           $replaced='Not yet';
                                       }
                                       $curCount=$instance['cur_count'];
                                       $curTime=round($instance['cur_time']/3600*24,2);
                                       print "<td>$installed</td><td>$replaced</td><td>$curCount</td><td>$curTime days</td>";
                                       print "<td colspan=3>";
                                       if($presspart['part_life_type']=='impressions')
                                       {
                                           $lifeCount=$presspart['part_life_impressions'];
                                           if($lifeCount<$curCount)
                                           {
                                               print "<span style='color:red;'>Part is beyond the recommended life of $lifeCount. Please check and replace soon.</span>";
                                           } else {
                                               print "There are at least ".($lifeCount-$curCount)." cycles remaining before this part reaches its recommended replacement point.";
                                           }
                                       } else {
                                           $lifeCount=$presspart['part_life_days'];
                                           if($lifeCount<$curTime)
                                           {
                                               print "<span style='color:red;'>Part is beyond the recommended life of $lifeCount. Please check and replace soon.</span>";
                                           } else {
                                               print "There are at least ".($lifeCount-$curTime)." days remaining before this part reaches its recommended replacement point.";
                                           }
                                       }
                                       //see if there are any notes
                                       if($instance['replace_notes']!='')
                                       {
                                           print "<br />".stripslashes($instance['replace_notes']);
                                       }
                                       print "</td>\n";
                                       print "</tr>\n";
                                   }
                               } 
                       }
                       
                }
                print "<tr><td colspan=9></td></tr>\n";
                print "<tr><td colspan=9>PM Tasks</td></tr>\n";
                $pmsql="SELECT A.*, B.component_id FROM equipment_pm A, equipment_pm_xref B 
                WHERE A.id=B.pm_id AND B.equipment_id='$equipmentid' 
                AND B.component_id='$basecomponentid' AND B.equipment_type='press'";
                $dbPressPMs=dbselectmulti($partsql);
                if(debug){print "&nbsp;&nbsp;&nbsp;&nbsp;PM sql $pmsql<br>";}  
                if ($dbPressPMs['numrows']>0)
                {        
                    foreach($dbPressPMs['data'] as $pm)
                    {
                               //lets see if we can find an open instance of this part
                               $sql="SELECT * FROM pm_instances WHERE ppm_id=$presspart[id] AND equipment_id='$equipmentid' AND component_id='$tower[id]' AND ((install_datetime>='$start' AND install_datetime<='$stop') OR (replace_datetime>='$start' AND replace_datetime<='$stop'))";
                               if(debug){print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Instance sql $sql<br>";}  
                
                               $dbInstances=dbselectmulti($sql);
                               if ($dbInstances['numrows']>0)
                               {
                                   foreach($dbInstances['data'] as $instance)
                                   {
                                       print "<tr>";
                                       print "<td>$pm[pm_name]</td>";
                                       print "<td>$instance[sub_component_location]</td>";
                                       $installed=date("m/d/Y", strtotime($instance['install_datetime']));
                                       if($instance['replace_datetime']!='' && $instance['replace_datetime']!='Null')
                                       {
                                           $replaced=date("m/d/Y", strtotime($instance['replace_datetime']));
                                       } else {
                                           $replaced='Not yet';
                                       }
                                       $curCount=$instance['cur_count'];
                                       $curTime=round($instance['cur_count']/60,2);
                                       print "<td>$installed</td><td>$replaced</td><td>$curCount</td><td>$curTime days</td>";
                                       print "<td colspan=3>";
                                       if($pm['pm_life_type']=='impressions')
                                       {
                                           $lifeCount=$pm['pm_life_impressions'];
                                           if($lifeCount<$curCount)
                                           {
                                               print "<span style='color:red;'>Part is beyond the recommended life of $lifeCount. Please check and replace soon.</span>";
                                           } else {
                                               print "There are at least ".($lifeCount-$curCount)." cycles remaining before this part reaches its recommended replacement point.";
                                           }
                                       } else {
                                           $lifeCount=$pm['pm_life_days'];
                                           if($lifeCount<$curTime)
                                           {
                                               print "<span style='color:red;'>Part is beyond the recommended life of $lifeCount. Please check and replace soon.</span>";
                                           } else {
                                               print "There are at least ".($lifeCount-$curTime)." days remaining before this part reaches its recommended replacement point.";
                                           }
                                       }
                                       //see if there are any notes
                                       if($instance['replace_notes']!='')
                                       {
                                           print "<br />".stripslashes($instance['replace_notes']);
                                       }
                                       print "</td>\n";
                                       print "</tr>\n";
                                   }
                               } 
                       }
                       
                }
                
        /*    
        } else {
                print "<tr><th colspan=8>No components for for $tower[tower_name]</th></tr>\n";
            } 
            */     
        }
        
        // now grab all the scheduled maintenance tasks for this time period
        print "<tr><td colspan=9></td></tr>\n";
        print "<tr><td colspan=9>Scheduled Maintenance Tasks</td></tr>\n";
        print "<tr><td>Start</td><td>Stop</td><td colspan=2>Goals</td><td colspan=2>Notes</td><td colspan=3>Associated Tickets</td></tr>\n";
        $sql="SELECT * FROM maintenance_scheduled WHERE equipment_type='press' AND equipment_id='$equipmentid' AND starttime>='$start' AND endtime<='$stop'";
        $dbScheduled=dbselectmulti($sql);
        if($dbScheduled['numrows']>0)
        {
            foreach($dbScheduled['data'] as $sched)
            {
                print "<tr>";
                print "<td >";
                print date("m/d/Y H:i",strtotime($sched['starttime']));
                print "</td>";
                print "<td >";
                print date("m/d/Y H:i",strtotime($sched['endtime']));
                print "</td>";
                print "<td colspan=2>";
                print stripslashes($sched['goals']);
                print "</td>";
                print "<td colspan=2>";
                print stripslashes($sched['notes']);
                print "</td>";
                print "<td colspan=3>";
                //lets see if there are any associated tickets
                $sql="SELECT A.* FROM maintenance_tickets A, maintenance_scheduled_tickets B WHERE B.schedule_id=$sched[id] AND A.id=B.ticket_id";
                $dbTickets=dbselectmulti($sql);
                if($dbTickets['numrows']>0)
                {
                    foreach($dbTickets['data'] as $ticket)
                    {
                        print "Problem:<br />".stripslashes($ticket['problem']);    
                        print "Solution:<br />".stripslashes($ticket['solution']);    
                    }
                }
                print "</td>";
                print "</tr>";
            }
        }
        print "</table>\n";  
    } else {
        print "No towers found.";
    }
    
}

$Page->footer();
<?php
//<!--VERSION: .9 **||**-->

global $ordersources, $siteID;
if ($_POST['submit'])
{
    include("../includes/boot.php"); 
    if ($_POST['vendor']!=0){
        $vendor="AND id=".intval($_POST['vendor']);
    } else {
        $vendor="";
    }
    if ($_POST['storageLocation']!=0){
        $location="AND A.storage_location=".intval($_POST['storageLocation']);
    } else {
        $location="";
    }
    if ($_POST['ordersource']!='0'){
        unset($ordersources);
        $ordersources=array($_POST['ordersource']=>$_POST['ordersource']);
    } 
    if ($_POST['ptype']!=0){
        $ptype="AND id=".intval($_POST['ptype']);
    } else {
        $ptype="";
    }
    if ($_POST['psize']!=0){
        $psize="AND id=".intval($_POST['psize']);
    }
    if ($_POST['status']==0)
    { 
        $status="";
    } else {
        $status="AND A.status=$_POST[status] ";
    }
    if ($_POST['ignoresource'])
    {
        $ignore=true;
    }
    
    //now build sql to get rolls
    print "<table class='table table-striped table-bordered'>\n";
    print "<tr><th><a href='#' onclick='window.print();'><i class='fa fa-print'></i>Print</a></th><th colspan=4><p style='text-align:center;font-size:18px;font-weight:bold;'>Inventory Report</p></th></tr>\n";
    print "<tr><th>Vendor</th>";
    if (!$ignore)
    {
        print "<th>Order Source</th>";    
    }
    print "<th>Common Name</th><th>Size</th><th>Rolls in Inventory</th><th>Actual</th></tr>\n";
    
    //we'll do a series of loops, from vendor down to rolls
    $sql="SELECT * FROM accounts WHERE site_id=".SITE_ID." AND newsprint=1 $vendor ORDER BY account_name";
    $dbVendors=dbselectmulti($sql);
    if ($dbVendors['numrows']>0)
    {
        foreach($dbVendors['data'] as $vendor)
        {
            $vendorid=$vendor['id'];
            $vendorname=$vendor['account_name'];
                //now loop through paper types
                    $sql="SELECT * FROM paper_types WHERE status<>99 $ptype ORDER BY common_name";
                    $dbPaperTypes=dbselectmulti($sql);
                    if ($dbPaperTypes['numrows']>0)
                    {
                        foreach($dbPaperTypes['data'] as $papertype)
                        {
                            $commonname=$papertype['common_name'];
                            //now we need sizes
                            $sql="SELECT * FROM paper_sizes WHERE status<>99 $psize ORDER BY width";
                            $dbSizes=dbselectmulti($sql);
                            if ($dbSizes['numrows']>0)
                            {
                                foreach ($dbSizes['data'] as $rollsize)
                                {
                                    $rollwidth=$rollsize['width'];
                                    //now we are finally at the roll level
                                    //now we're going to loop based on order source
                                    if (!$ignore)
                                    {
                                        foreach($ordersources as $source=>$ordersource)
                                        {
                                            if ($ordersource!='Please choose')
                                            {
                                                $sql="SELECT count(A.id) as rollcount FROM rolls A, orders B 
                                                WHERE A.site_id=$siteID AND A.order_id=B.id AND A.roll_width='$rollwidth' 
                                                AND A.common_name='$commonname' AND B.order_source='$ordersource' 
                                                AND B.vendor_id=$vendorid AND A.status=1 $location";
                                                $dbRollCount=dbselectsingle($sql);
                                                $rollcount=$dbRollCount['data']['rollcount'];
                                                if ($rollcount>0)
                                                {
                                                    print "<tr><td>$vendorname</td><td>$ordersource</td><td>$commonname</td><td>$rollwidth</td><td style='text-align:right;'>$rollcount</td><td>__________</td></tr>\n";    
                                                    $totalrolls+=$rollcount;
                                                }
                                            }
                                        }
                                    } else {
                                        $sql="SELECT count(A.id) as rollcount FROM rolls A, orders B 
                                        WHERE A.site_id=$siteID AND A.order_id=B.id AND A.roll_width='$rollwidth' 
                                        AND A.common_name='$commonname' AND B.vendor_id=$vendorid AND A.status=1 $location";
                                        $dbRollCount=dbselectsingle($sql);
                                        $rollcount=$dbRollCount['data']['rollcount'];
                                        //$ordersource=$dbRollCount['data']['ordersource'];
                                        if ($rollcount>0)
                                        {
                                            print "<tr><td>$vendorname</td><td>$commonname</td><td>$rollwidth</td><td style='text-align:right;'>$rollcount</td><td>__________</td></tr>\n";    
                                            $totalrolls+=$rollcount;
                                        }
                                    }        
                                    
                                }
                            } else {
                                print "Sorry, there are no roll sizes configured!<br />\n";
                            }
                            
                            
                        }
                    } else {
                        print "Oops, there are no paper types set up.<br />";
                    }
           
            
        }
        print "<tr><td colspan=6>Total rolls showing in inventory $totalrolls</td></tr>\n";
    } else {
        print "Stopping here because there are no vendors set up.<br />";
    }
    print "</table>\n";
    
} else {
    include("../includes/boot.php") ;
    global $siteID, $newsprintVendors, $defaultNewsprintLocation, $papertypes, $sizes;
    
    $slocations = buildLocations('newsprint');
    
    print "<form method=post class='form-horizontal'>\n";
            make_select('vendor',$newsprintVendors[0],$newsprintVendors,'Vendor')."<br>\n";
            make_select('storageLocation',$slocations[$defaultNewsprintLocation],$slocations,'Storage Location');
            make_select('ordersource',$ordersources[0],$ordersources,'Order Source');
            make_select('ptype',$papertypes[0],$papertypes,'Paper type');
            make_select('psize',$sizes[0],$sizes,'Paper size');
            make_checkbox('ignoresource',1,'Disregard order source');
            make_submit('submit','Generate Report');
    print "</form>\n";
    
}

if($render=='csv')
{ 
    dbclose();
} else {
    $Page->footer();
} 
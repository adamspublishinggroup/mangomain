<?php
$systemMode = true;
include("includes/boot.php");
error_reporting(E_ERROR);
$cronid=intval($_GET['cronid']);
$oldtime=date("Y-m-d",strtotime("-8 days"));
$currenttime=date("H:i:s");
$currentdate=date("Y-m-d",time());       
$currenttimestamp=date("Y-m-d H:i:s",time());
$sysRoot=trim(getcwd());
$phpself=$_SERVER['PHP_SELF'];
$sysRoot=str_replace("cronprocessorSingle.php","",$phpself);
if($sysRoot=='/'){print "fixing...";$sysRoot='';}else{print "Wtf? **$sysRoot** -- <br />";}
$notes='';
$futuredate=date("Y-m-d",strtotime("+48 hours"));
$root=$GLOBALS['systemRootPath'];
$sql="SELECT * FROM core_cron WHERE id=$cronid";
$dbCron=dbselectsingle($sql);
$cronjob=$dbCron['data'];
$script=stripslashes($cronjob['script']);
$scriptFilename=end(explode("/",stripslashes($cronjob['script'])));
$function=stripslashes($cronjob['function']);
$params=stripslashes($cronjob['params']);
$rundate=$currentdate." ".$exectime;

if (strpos($script,"ttp://")>0 || substr($script,0,3)=="../" || substr($script,0,1)=="/")
{
    //lets leave these alone as they should be either calls to an http url, or a relative url from the root
    if(substr($script,0,1)=="/")
    {
          $script= $_SERVER['DOCUMENT_ROOT'].$script;
          $script=str_replace("//","/",$script);
    }
} else {
    $script=$sysRoot.'cronjobs/'.$script;
}
$script=str_replace("ttp://","ttp:**",$script);
$script=str_replace("//","/",$script);
$script=str_replace("ttp:**","ttp://",$script);
print "<br />Now executing $script...<br />";

if (is_file($script))
{
    print "Processing...<br>";
    require_once($script);     
    $notes.= "Called in $script, and executed $function with parameters of $params<br />\n";
    if ($function!='')
    {
        $function($params);                  
    }
    //now update the cronexecution table
    if ($notes==''){
        print "No notes for this job<br>";
    }
    $status=1;
} else {
    $notes.="This job did not run because the script file - $script was not found;";
    $status=2;
}
print "<h4>$job[title] executing $script</h4>";
print $notes; 

dbclose(); 
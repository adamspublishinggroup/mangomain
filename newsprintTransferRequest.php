<?php
include("includes/boot.php") ;
if($GLOBALS['debug']){print "refer is ".$_SERVER['HTTP_REFERER']."<br>";}


/*
Statuses for requests
 1:    "Open";
 2:    "Incomplete";
 3:    "Tranferring";
 9:    "Complete"; 

*/

if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}
    
switch ($action)
{
    case "add":
    request();
    break;
    
    case "edit":
    request('edit');
    break;
    
    case "Add to Request":
    add_request();
    break;
    
    case "Save Request":
    save_request();
    break;
    
    case "Save Load":
    save_rolltags();
    break;
          
    case "newload":
    create_load();
    break;
    
    case "delete":
    delete_request();
    break;
    
    case "list":
    list_requests();
    break;
    
    case "show":
    show_request();
    break;
    
    case "handle":
    handle_request();
    break;
    
    case "editload":
    handle_request();
    break;
    
    case "remove":
    remove_request_item();
    break;
    
    case "removeload":
    remove_load();
    break;
    
    case "receive":
    receive_rolls();
    break;
    
    case "receiveload":
    receive_load();
    break;
    
    case "printmanifest":
    print_loadmanifest();
    break;
    
    case "Receive Load":
    process_receive_rolls();
    break;
    
    default:
    list_requests();
    break;
}

function receive_rolls()
{
   $requestID=intval($_GET['request_id']);
   
   $sql="SELECT * FROM transfer_request_loads WHERE request_id=$requestID";
   $dbLoads = dbselectmulti($sql);
   
   if($dbLoads['numrows']>0)
   {
        print "<table id='stable' class='ui-widget'>";
        print "<thead><tr>";
        print "<th>Prep Date/Time</th><th>Roll Count</th><th>Load Weight</th><th>Action</th>";
        print "</tr></thead>";
        foreach($dbLoads['data'] as $load)
        {
            if(trim($load['roll_tags'])!='')
            {
                $count=count(explode("\n",trim($load['roll_tags'])));
            } else {
                $count=0;
            }
            print "<tr>";
            print "<td>".date("D m/d H:i",strtotime($load['shipment_datetime']))."</td>";    
            print "<td>".$count."</td>";    
            $tags=stripslashes($load['roll_tags']);
            $tagList = implode("','",$tags);
            $sql="SELECT SUM(roll_weight) as rweight FROM rolls WHERE roll_tag IN ('$tagList')";
            $dbWeight=dbselectsingle($sql);
            $weight=round($dbWeight['data']['rweight'] / 0.45359237,2);
            print "<td>$weight lbs</td>";
            print "<td><a href='?action=receiveload&load_id=$load[id]&request_id=$requestID'>Receive Load</a></td>";
            print "</tr>\n";    
        }
        print "</table>";
   } else {
        print "No rolls";
   }
}

function receive_load()
{
    global $defaultNewsprintLocation;
    $requestID=intval($_GET['request_id']);
    $loadID=intval($_GET['load_id']);
    
    $sql="SELECT * FROM transfer_request_loads WHERE id=$loadID";
    $dbLoad = dbselectsingle($sql);
    
    $load = $dbLoad['data'];
    
    $rolltags = explode("\n",stripslashes(trim($load['roll_tags'])));
    $tagList = implode("','",$rolltags);
    $sql="SELECT * FROM rolls WHERE roll_tag IN ('$tagList') ORDER BY common_name, roll_size";
    $dbRolls = dbselectmulti($sql);
    if($dbRolls['numrows']>0)
    {
        print "<form method=post class='form-horizontal'>";
        foreach($dbRolls['data'] as $roll)
        {
            /*
            @TODO
            need to come up with a better way of knowing where paper really is.
            */
            if($roll['location_id']==$defaultNewsprintLocation)
            {
                $loc = 'Transferred';
            } else {
                $loc = 'At remote site';
            }
            make_checkbox('roll_'.$roll['id'],stripslashes($roll['common_name'].' Width: '.$roll['roll_width'].' | Roll Tag: '.$roll['roll_tag'].' Roll is '.$loc)); 
        }
        print "<input type='submit' class='button' value='Receive Load' />";
        make_hidden('request_id',$requestID);
        make_hidden('load_id',$loadID);
        print "</form>\n";
    }  else {
        print "<h3>No rolls in this load!</h3><a href='?action=receive&request_id=$requestID'>Return to load list</a>";
    }      
}

function process_received_rolls()
{
    global $defaultNewsprintLocation;
    
    /*
    @TODO
    need to come up with a better way of deciding where to transfer rolls
    */
    $loadID=intval($_POST['load_id']);
    $requestID=intval($_POST['request_id']);
    
    foreach($_POST as $key)
    {
        if(substr($key,0,5)=='roll_')
        {
            $rollIDs[]=str_replace("roll_","",$key);
        } 
    }
    if(count($rollIDs)>0)
    {
        $rollTags=implode("','",$rollIDs);
        $sql="UPDATE rolls SET location_id=$defaultNewsprintLocation WHERE id IN('$rollTags')";
        $dbUpdate=dbexecutequery($sql);
        print "<h4>Updated the selected rolls to be transferred to this facility.</h4><a href='?action=receive&request_id=$requestID'>Return to load list</a>";
        
    } else {
        print "<h4>No rolls were marked as being received.</h4><a href='?action=receive&request_id=$requestID'>Return to load list</a>";
    }
    
}


function create_load()
{
    $requestID = intval($_GET['request_id']);
    $sql="INSERT INTO transfer_request_loads (request_id, shipment_datetime, roll_tags, load_weight) VALUES ('$requestID', '".date("Y-m-d H:i")."', '', 0)";
    $dbInsert=dbinsertquery($sql);
    
    $loadID=$dbInsert['insertid'];
    
    redirect("?action=handle&request_id=$requestID&load_id=$loadID");
}

function confirm_transfer()
{
    global $papertypes, $sizes;
    $requestID=intval($_GET['request_id']);
        
    $sql="SELECT * FROM transfer_request WHERE id=$requestID";
    $dbRequest=dbselectsingle($sql);
    $request=$dbRequest['data'];
    $deliveryDate = $request['delivery_date'];
    $locationID=$request['location_id'];
    $tags=$request['roll_tags'];
   
    if($_POST)
    {
        $receivedTags=$_POST['received_tags'];
        $rTags=explode("\n",$receivedTags);
        
    }
    print "<h3>The following rolls have been transferred</h3>";
    //show any existing rolls
    $tags=explode("\n",stripslashes($tags));
    $tagList = implode("','",$tags);
    $sql="SELECT * FROM rolls WHERE roll_tag IN ('$tagList') ORDER BY common_name,roll_width,roll_tag";
    $dbRolls=dbselectmulti($sql);
    if($dbRolls['numrows']>0)
    {
        print "<table id='stable' class='ui-widget'>";
        print "<thead><tr>";
        print "<th>Roll Type</th><th>Roll Width</th><th>Roll Tag</th><th>Received</th>";
        print "</tr></thead>";
        foreach($dbRolls['data'] as $roll)
        {
            print "<tr>";
            print "<td>".stripslashes($roll['common_name'])."</td>";    
            print "<td>".stripslashes($roll['roll_width'])."</td>";    
            print "<td>".stripslashes($roll['roll_tag'])."</td>";
            if(in_array(stripslashes($roll['roll_tag']),$rTags)){
                print "<td>Received</td>";
            } else {
                print "<td>Not Received</td>";
            }
            print "</tr>\n";    
        }
        print "</table>";
    }
    print "<form method=post class='form-horizontal'>\n";
    make_textarea('received_tags','','Received Tags','Scan all received tags into this field.',50,30,false);
    make_submit('submit','Receive Rolls');
    print "</form>\n";
    print "<br><a href='?action=list' class='button'>Cancel and returnt to list</a>";
}

function show_request()
{
    global $papertypes, $sizes;
    $requestID=intval($_GET['request_id']);
        
    $sql="SELECT * FROM transfer_request WHERE id=$requestID";
    $dbRequest=dbselectsingle($sql);
    $request=$dbRequest['data'];
    $deliveryDate = $request['delivery_date'];
    $locationID=$request['location_id'];
    $weight=$request['estimated_weight'];
            
    $sql="SELECT * FROM storage_locations WHERE id=$locationID";
    $dbLocation=dbselectsingle($sql);
    $location=$dbLocation['data']['location_name'];
    
    print "<h3>Tranfer Request</h3>";
    print "<p>Request to transfer the following rolls by ".date("D m/d/Y",strtotime($deliveryDate))." to $location.</p>";
    print "<p>Estimated total weight is: ".$weight."</p>\n";
    //show existing items
    $sql="SELECT * FROM transfer_request_items WHERE request_id=$requestID";
    $dbItems=dbselectmulti($sql);
    if($dbItems['numrows']>0)
    {
        print "<table id='stable' class='ui-widget'>";
        print "<thead><tr>";
        print "<th>Roll Type</th><th>Roll Width</th><th>Roll Count</th><th>Est. Weight</th>";
        print "</tr></thead>";
        foreach($dbItems['data'] as $item)
        {
           print "<tr>";
           print "<td>".$papertypes[$item['paper_type_id']]."</td>\n"; 
           print "<td>".$sizes[$item['size_id']]."</td>\n"; 
           print "<td>".$item['rollcount']."</td>\n";
           $sql="SELECT AVG(roll_weight) as rweight FROM rolls 
           WHERE common_name = '".$papertypes[$item['paper_type_id']]."' AND roll_width='".$sizes[$item['size_id']]."'";
           $dbWeight=dbselectsingle($sql);
           $weight=round($dbWeight['data']['rweight'] * $item['rollcount'] / 0.45359237,2);
            
           print "<td>$weight lbs</td>";
           print "</tr>\n";
        }
        print "</table>\n";
    }
    print "<a href='?action=list' class='button'>Return to list</a>";
}

function remove_load()
{
    $requestID=intval($_GET['request_id']);
    $loadID=intval($_GET['load_id']);
    
    $sql="DELETE FROM transfer_request_loads WHERE id=$loadID";
    $dbDelete=dbexecutequery($sql);
    
    redirect("?action=handle&request_id=$requestID");
}

function remove_request_item()
{
    global $papertypes, $sizes;
    $requestID=intval($_GET['request_id']);
    $itemID=intval($_GET['item_id']);
    
    $sql="SELECT * transfer_request_items WHERE id=$itemID";
    $dbItem=dbselectsingle($sql);
    $item=$dbItem['data'];
    
    $sql="SELECT AVG(roll_weight) as rweight FROM rolls WHERE common_name='".$papertypes[$item['paper_type_id']]."' AND roll_width='".$sizes[$item['size_id']]."'"; 
    $dbWeight=dbselectsingle($sql);
    $weight=round($dbWeight['data']['rweight'] * $item['rollcount'] / 0.45359237,2);
            
    $sql="DELETE FROM transfer_request_items WHERE id=$itemID";
    $dbDelete=dbexecutequery($sql);
    
    $sql="DELETE FROM transfer_request_loads WHERE request_id=$requestID";
    $dbDelete=dbexecutequery($sql);
    
    $sql="UPDATE transfer_request SET estimated_weight=estimated_weight-$weight";
    $dbReduce=dbexecutequery($sql);
    
    redirect("?action=edit&request_id=$requestID");
}

function handle_request()
{
    global $papertypes, $sizes;
    $requestID=intval($_GET['request_id']);
        
    $sql="SELECT * FROM transfer_request WHERE id=$requestID";
    $dbRequest=dbselectsingle($sql);
    $request=$dbRequest['data'];
    $deliveryDate = $request['delivery_date'];
    $locationID=$request['location_id'];
    $status=$request['status'];
    if($status==2)print "<h3 style='color:red'>This request in incomplete</h3>";
    $sql="SELECT * FROM storage_locations WHERE id=$locationID";
    $dbLocation=dbselectsingle($sql);
    $location=$dbLocation['data']['location_name'];
    print "<h3>Transfer Request</h3>";
    print "<p>Request to transfer the following rolls by ".date("D m/d/Y",strtotime($deliveryDate))." to $location.</p>";
    //show existing items
    $sql="SELECT * FROM transfer_request_items WHERE request_id=$requestID";
    $dbItems=dbselectmulti($sql);
    if($dbItems['numrows']>0)
    {
        print "<table id='stable' class='ui-widget'>";
        print "<thead><tr>";
        print "<th>Roll Type</th><th>Roll Width</th><th>Roll Count</th><th>Est. Weight</th>";
        print "</tr></thead>";
        foreach($dbItems['data'] as $item)
        {
           print "<tr>";
           print "<td>".$papertypes[$item['paper_type_id']]."</td>\n"; 
           print "<td>".$sizes[$item['size_id']]."</td>\n"; 
           print "<td>".$item['rollcount']."</td>\n";
           $sql="SELECT AVG(roll_weight) as rweight FROM rolls 
           WHERE common_name = '".$papertypes[$item['paper_type_id']]."' AND roll_width='".str_replace(".00","",$sizes[$item['size_id']])."'";
           $dbWeight=dbselectsingle($sql);
           $w = round($dbWeight['data']['rweight'] * $item['rollcount']/0.45359237,2);
           print "<td>$w lb</td>";
           
           print "</tr>\n";
        }
        print "</table>\n";
    }    
    
    //show any existing rolls
    $sql="SELECT * FROM transfer_request_loads WHERE request_id=$requestID";
    $dbLoads = dbselectmulti($sql);
    if($dbLoads['numrows']>0)
    {
        foreach($dbLoads['data'] as $load)
        {
            $tags=stripslashes($load['roll_tags']);
            $tags=explode("\n",trim($tags));
            $tagList = implode("','",$tags);
            $sql="SELECT * FROM rolls WHERE roll_tag IN ('$tagList') ORDER BY common_name, roll_size";
            $dbRolls=dbselectmulti($sql);
            if($dbRolls['numrows']>0)
            {
                print "<hr>Existing rolls in order<br>";
                print "<table id='stable' class='ui-widget'>";
                print "<thead><tr>";
                print "<th>Paper Type</th><th>Roll Size</th><th>Roll Weight</th>";
                print "</tr></thead>";
                foreach($dbRolls['data'] as $roll)
                {
                    print "<tr>";
                    print "<td>".$roll['common_name']."</td>";
                    print "<td>".$roll['roll_width']."</td>";
                    print "<td>".$roll['roll_weight']."</td>";
                    print "</tr>"; 
                }
                print "</table>";
            }
            
        }
        
    }
    print "<hr>";
    print "<br><a href='?action=newload&request_id=$requestID' class='button'>Add new load</a><br>";
    if($dbLoads['numrows']>0)
    {
        print "<table id='stable' class='ui-widget'>";
        print "<thead><tr>";
        print "<th>Prep Date/Time</th><th>Roll Count</th><th>Est. Load Weight</th><th>Actions</th>";
        print "</tr></thead>";
        foreach($dbLoads['data'] as $load)
        {
            if(trim($load['roll_tags'])!='')
            {
                $count=count(explode("\n",trim($load['roll_tags'])));
            } else {
                $count=0;
            }
            print "<tr>";
            print "<td>".date("D m/d H:i",strtotime($load['shipment_datetime']))."</td>";    
            print "<td>".$count."</td>";    
            $tags=stripslashes($load['roll_tags']);
            $tagList = implode("','",$tags);
            $sql="SELECT SUM(roll_weight) as rweight FROM rolls WHERE roll_tag IN ('$tagList')";
            $dbWeight=dbselectsingle($sql);
            $weight=round($dbWeight['data']['rweight'] / 0.45359237,2);
            print "<td>$weight lbs</td>";
            print "<td><a href='?action=editload&load_id=$load[id]&request_id=$requestID'>Edit Load</a>
            <a href='?action=printmanifest&load_id=$load[id]&request_id=$requestID' target='_blank'>Print Manifest</a>
            <a href='?action=removeload&load_id=$load[id]&request_id=$requestID' class='delete'>Remove Load</a></td>";
            print "</tr>\n";    
        }
        print "</table>";
    }
    if($_GET['load_id'] && $_GET['action']=='editload')
    {
        $loadID=intval($_GET['load_id']);
        $sql="SELECT * FROM transfer_request_loads WHERE id=$loadID";
        $dbLoad=dbselectsingle($sql);
        $load=$dbLoad['data'];
        print "<h4>Please enter the rolltags for this load</h4>
        <p>They will be verified against the request.</p>\n";
        print "\n<form method=post class='form-horizontal'>\n";
        make_textarea('rolltags',stripslashes($load['roll_tags']),'Tags','',60,30,false);
        make_hidden('load_id',$loadID);
        make_hidden('request_id',$requestID);
        make_submit('submit','Save Load');
        print "</form>\n";
    }
    print "<br><a href='?action=list' class='button'>Cancel and return to list</a>"; 
}

function save_rolltags()
{
    $requestID=intval($_POST['request_id']);
    $loadID=intval($_POST['load_id']);
    $tags=addslashes($_POST['rolltags']);
    
    if($tags!='')
    {
        global $papertypes, $sizes;
        //get the request items
        
        $sql="SELECT * FROM transfer_request_items WHERE request_id=$requestID";
        $dbItems = dbselectmulti($sql);
        if($dbItems['numrows']>0)
        {
            foreach($dbItems['data'] as $item)
            {
                $type=$papertypes[$item['paper_type_id']];
                $size=$sizes[$item['size_id']];
                $requested[$type][$size]['request']+=$item['rollcount'];
            }
        }
        
        //save them
        $sql="UPDATE transfer_request_loads SET roll_tags ='".addslashes($tags)."' WHERE id=$loadID";
        $dbUpdate=dbexecutequery($sql);
        //now lets loop through all the loads to see if we now have everything accounted for
        $sql="SELECT * FROM transfer_request_loads WHERE request_id=$requestID";
        $dbLoads=dbselectmulti($sql);
        if($dbLoads['numrows']>0)
        {
            foreach($dbLoads['data'] as $load)
            {
                $tags = stripslashes($load['roll_tags']);
                $tags = explode("\n",$tags);
        
                $tagList = implode("','",$tags);
                $missing=array();
                
                $sql="SELECT * FROM rolls WHERE roll_tag IN ('$tagList') ORDER BY common_name, roll_width, roll_tag";
                $dbRolls=dbselectmulti($sql);
                //loop through rolls, we are going to look up type and size and increment the counter
                if($dbRolls['numrows']>0)
                {
                    foreach($dbRolls['data'] as $roll)
                    {
                         $found=false;
                         foreach($requested as $key=>$sizeKey)
                         {
                             if($roll['common_name']==$key)
                             {
                                 foreach($sizeKey as $size=>$count)
                                 {
                                     if($roll['roll_width']==$size)
                                     {
                                         $requested[$key][$size]['count']++;
                                         $found=true;
                                     }
                                 }
                             }
                         }
                         if(!$found)
                         {
                            $missing=true; 
                         }
                    }
                } else {
                      $missing=true; 
                }
            }
        }
        
        
    } else {
        $missing=true;
    }
    
    
    if($missing)
    {
        $sql="UPDATE transfer_request SET status=2 WHERE id=$requestID";
        $dbUpdate=dbexecutequery($sql);
        redirect("?action=handle&request_id=$requestID"); 
    } else {
        //else it's being transferred, set status to 3
        $sql="UPDATE transfer_request SET status=3 WHERE id=$requestID";
        $dbUpdate=dbexecutequery($sql);
        redirect("?action=list");
    }
    
}

function transferRolls()
{
    $locationID=intval($_POST['storageLocation']);
    
    $sql="SELECT * FROM storage_locations WHERE id=$locationID";
    $dbLocation = dbselectsingle($sql);
    $location = $dbLocation['data']['location_name'];
    
    $tags=addslashes($_POST['rolltags']);
    
    if($tags!='')
    {
        $tags = explode("\n",$tags);
        foreach($tags as $tag)
        {
            $updateTags[]="'".trim($tag)."'";
            $count++;
        }
        $updateTags = implode(',',$updateTags);
        
        $sql="UPDATE rolls SET storage_location = $locationID WHERE roll_tag IN($updateTags)";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
        
        if ($error!='')
        {
            print $error;
        } else {
            print "Successfully transferred $count rolls to $location<br><br><a href='?action=trasfer'>Click here to transfer more rolls.</a>";
        }
    }
    
}

function request($action='new')
{
    global $papertypes, $sizes, $defaultNewsprintLocation;
    $slocations = buildLocations('newsprint');
    $requestID=intval($_GET['request_id']);
        
    if($action=='edit')
    {
        $sql="SELECT * FROM transfer_request WHERE id=$requestID";
        $dbRequest=dbselectsingle($sql);
        $request=$dbRequest['data'];
        $deliveryDate = $request['delivery_date'];
        $locationID=$request['location_id'];
        $sourceID=$request['source_id']; 
    } else {
        $deliveryDate = date("Y-m-d",strtotime("+1 day"));
        $locationID = $defaultNewsprintLocation;
        $sourceID=0;
    }
    print "<h4>Transfer Details</h4>"; 
    print "<form method=post class='form-horizontal'>\n";
        make_select('source_id',$slocations[$sourceID],$slocations,'Transfer FROM','Which location will be SENDING the rolls');
        make_select('location_id',$slocations[$locationID],$slocations,'Transfer TO','Which location will be RECEIVING the rolls');
        make_date('delivery_date',$deliveryDate,'Deliver By','When should the order be delivered');
        make_hidden('request_id',$requestID);
        make_submit('submit','Save Request');
    print "</form>\n";
    if($action=='edit')
    {
        print "<hr><form method=post class='form-horizontal'>\n";
        make_select('paper_type',$papertypes[0],$papertypes,'Paper Types','Which type of paper?');
        make_select('paper_size',$sizes[0],$sizes,'Roll Width','Width of roll');
        make_number('rollcount',0,'Number of rolls','How many rolls of this type and size of paper?');
        make_hidden('request_id',$requestID);
        make_submit('submit','Add to Request');
        print "</form>\n";
        
        //show existing items
        print "<h4>Transfer Items</h4>";
        $sql="SELECT * FROM transfer_request_items WHERE request_id=$requestID";
        $dbItems=dbselectmulti($sql);
        if($dbItems['numrows']>0)
        {
            print "<table id='stable' class='ui-widget'>";
            print "<thead><tr>";
            print "<th>Roll Type</th><th>Roll Width</th><th>Roll Count</th><th>Action</th>";
            print "</tr></thead>";
            foreach($dbItems['data'] as $item)
            {
               print "<tr>";
               print "<td>".$papertypes[$item['paper_type_id']]."</td>\n"; 
               print "<td>".$sizes[$item['size_id']]."</td>\n"; 
               print "<td>".$item['rollcount']."</td>\n";
               print "<td><a href='?action=remove&item_id=$item[id]&request_id=$requestID'>Remove</a></td>\n";
               print "</tr>\n";
            }
            print "</table>";
        }
    
    }
    print "<br><a href='?action=list' class='button'>Return to list</a>";  
}

function add_request()
{
    global $papertypes, $sizes;
    
    $requestID = intval($_POST['request_id']);
    
    //look up the request
    $sql="SELECT * FROM transfer_request WHERE id=$requestID";
    $dbRequest=dbselectsingle($sql);
    $request = $dbRequest['data'];
    
    $paperTypeID =  intval($_POST['paper_type']);
    $sizeID =  intval($_POST['paper_size']);
    $rollcount =  intval($_POST['rollcount']);
    if($paperTypeID!=0 && $sizeID!=0 && $rollcount!=0)
    {
        //ok, before we proceed, lets see if we actually have the requested paper at the source
        $sql="SELECT COUNT(id) as rollcount FROM rolls WHERE status=1 AND storage_location=".$request['source_id']." AND
        common_name = '".stripslashes($papertypes[$paperTypeID])."' AND roll_width='".str_replace(".00","",$sizes[$sizeID])."'";
        $dbSourceCount=dbselectsingle($sql);
        $available=$dbSourceCount['data']['rollcount'];
        if($available>=$rollcount)
        {
            $sql="INSERT INTO transfer_request_items (request_id, paper_type_id, size_id, rollcount) 
            VALUES ('$requestID', '$paperTypeID', '$sizeID', '$rollcount')";
            $dbInsert=dbinsertquery($sql);
            
            $sql="SELECT AVG(roll_weight) as rweight FROM rolls WHERE common_name='".$papertypes[$paperTypeID]."' AND roll_width='".$sizes[$sizeID]."'"; 
            $dbWeight=dbselectsingle($sql);
            $weight=round($dbWeight['data']['rweight'] * $rollcount / 0.45359237,2);
            
            $sql="DELETE FROM transfer_request_items WHERE id=$itemID";
            $dbDelete=dbexecutequery($sql);
            
            $sql="UPDATE transfer_request SET estimated_weight=estimated_weight+$weight";
            $dbReduce=dbexecutequery($sql);
        } else {
            //not enough rolls available
            setUserMessage('There are not enough of those rolls available at the source.','error');
        }
    } 
    redirect("?action=edit&request_id=$requestID");
}

function save_request()
{
    global $siteID;
    $requestID = intval($_POST['request_id']);
    $requestDate = date("Y-m-d H:i:s");
    $deliveryDate = addslashes($_POST['delivery_date']);
    $locationID = intval($_POST['location_id']);
    $sourceID = intval($_POST['source_id']);
    $status = 1;
    
    if($requestID==0)
    {
        $sql="INSERT INTO transfer_request (location_id, source_id, request_datetime, delivery_date, status, site_id) 
        VALUES ('$locationID', '$sourceID', '$requestDate', '$deliveryDate', '$status', '$siteID')";
        $dbInsert=dbinsertquery($sql);
        $requestID = $dbInsert['insertid'];
        
        //send notification if needed
        $sql="SELECT * FROM storage_locations WHERE id=$locationID";
        $dbLocation=dbselectsingle($sql);
        $location=$dbLocation['data'];
        if($location['notifications'])
        {
            if($location['notification_email']!='')
            {
                $subject = "New newsprint transfer request to your location";
                $message = "A new newsprint request has been entered. The requested delivery date is $deliveryDate. You'll have to log into Mango to see the rolls being requested.";
                $mail = new htmlMimeMail();
                $mail->setHtml($message);
                $mail->setFrom($GLOBALS['systemEmailFromAddress']);
                $mail->setSubject($subject);
                $mail->setHeader('Sender','Mango');
                if ($sendTo!='')
                {
                    $result = $mail->send(array($sendTo),'smtp');
                } 
                
                $sql="UPDATE transfer_request SET notification_sent = 1 WHERE id=$requestID";
                $dbUpdate=dbexecutequery($sql); 
            } 
        }
        
    } else {
        $sql="UPDATE transfer_request SET location_id='$locationID', source_id='$sourceID', delivery_date='$deliveryDate' WHERE id=$requestID";
        $dbUpdate=dbexecutequery($sql);
    }
    redirect("?action=edit&request_id=$requestID");
}

function delete_request()
{
    $requestID = intval($_GET['request_id']);
    
    $sql="DELETE FROM transfer_request_items WHERE request_id=$requestID";
    $dbDelete=dbexecutequery($sql);
    $sql="DELETE FROM transfer_request WHERE id=$requestID";
    $dbDelete=dbexecutequery($sql);
    $error=$dbDelete['error'];
    if ($error!='')
    {
        setUserMessage('There was a problem deleting the requset.<br>'.$error,'error');
    } else {
        setUserMessage('The request has been removed.','success');
    }
    redirect("?action=list");
}

function list_requests()
{
    $sql="SELECT * FROM transfer_request ORDER BY delivery_date";
    $dbRequests=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Create request</a>","Request Date,Delivery Date,Total Rolls,Status",9);
    if ($dbRequests['numrows']>0)
    {
        foreach($dbRequests['data'] as $request)
        {
            $requestID=$request['id'];
            $sitename=$request['site_name'];
            $siteurl=$request['site_url'];
            $sql="SELECT SUM(rollcount) as totalrolls FROM transfer_request_items WHERE request_id=$requestID";
            $dbRolls = dbselectsingle($sql);
            $rolls = $dbRolls['data']['totalrolls'];
             
            print "<tr>";
            print "<td>".date("m/d/Y H:i",strtotime($request['request_datetime']))."</td>\n";
            print "<td>".date("D m/d/Y",strtotime($request['delivery_date']))."</td>\n";
            print "<td>$rolls</td>\n";
            print "<td>";
            switch($request['status'])
            {
                case 1:
                print "Open";
                break;
                
                case 2:
                print "Incomplete";
                break;
                     
                case 3:
                print "Transferring";
                break;
                
                case 9:
                print "Complete";
                break;
            }
            print "</td>";
            
            print "<td>
            <div class='btn-group'>
              <a href='?action=edit&request_id=$requestID' class='btn btn-dark'>Edit</a>
              <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <span class='caret'></span>
                <span class='sr-only'>Toggle Dropdown</span>
              </button>
              <ul class='dropdown-menu'>
                <li><a href='?action=show&request_id=$requestID'>Show Request</a></li>
                <li><a href='?action=handle&request_id=$requestID'>Fulfill Request</a></li>
                <li><a href='?action=receive&request_id=$requestID'>Receive Transfer</a></li>
                <li><a href='?action=delete&request_id=$requestID' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
              </ul>
            </div>
            </td>";
            
            
            print "</tr>\n";
        
        }
    }
    tableEnd($dbRequests);
}

function print_loadmanifest()
{
    $loadid=intval($_GET['load_id']);
    $requestid=intval($_GET['request_id']);
    
    $sql="SELECT * FROM transfer_request_loads WHERE id=$loadID";
    $dbLoad = dbselectsingle($sql);
    
    $load = $dbLoad['data'];
    
    $rolltags = explode("\n",stripslashes(trim($load['roll_tags'])));
    $tagList = implode("','",$rolltags);
    $sql="SELECT * FROM rolls WHERE roll_tag IN ('$tagList') ORDER BY common_name, roll_size";
    $dbRolls = dbselectmulti($sql);
    if($dbRolls['numrows']>0)
    {
        print "<table class='report'>";
        print "<tr><th>Paper Type</th><th>Roll Width</th><th>Roll Tag</th><th>Weight</th></tr>\n";
        foreach($dbRolls['data'] as $roll)
        {
            /*
            @TODO
            need to come up with a better way of knowing where paper really is.
            */
            if($roll['location_id']==$defaultNewsprintLocation)
            {
                $loc = 'Transferred';
            } else {
                $loc = 'At remote site';
            }
            print "<tr>".stripslashes($roll['common_name'])."</td>";
            print "<td>".$roll['roll_width'].'</td>';
            print '<td>'.$roll['roll_tag'].'</td>';
            $weight = round($roll['roll_weight']*2.2,2);
            $totalWeigth +=$weight;
            print "<td>".$weight." lbs.</td>";
            print "</tr>\n"; 
        }
        print "<tr><td colspan=4>Total load weight: $totalWeight lbs</td></tr>"; 
        print "</table>";
    }  else {
        print "<h3>No rolls in this load!</h3>";
    }      
}
$Page->footer();
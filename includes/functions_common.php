<?php
//@todo this is a check

function redirect($url) {
   if (!headers_sent())
       header('Location: '.$url);
   else {
       echo '<script type="text/javascript">';
       echo 'document.location.href="'.$url.'";';
       echo '</script>';
       echo '<noscript>';
       echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
       echo '</noscript>';
   }
}

function checkPermission($pageID='',$type='page')
{
    global $User;
    $valid=false;
    $pageID=str_replace("/","",$pageID);
    if($pageID=='login.php')
    {
        return true;
    }
    if($pageID=='')
    {
        $pageID=$_SERVER['SCRIPT_NAME'];
    }
    if (!is_numeric($pageID))
    {
        $sql="SELECT kiosk FROM core_pages WHERE filename='$pageID'";
        $dbPagePermissions=dbselectsingle($sql);
        if($dbPagePermissions['data']['kiosk']==1)
        {
            //kiosk mode page
            $_SESSION['kiosk']=true;
            return true;
        } else {
            $_SESSION['kiosk']=false;
        }
    }
    if (isset($GLOBALS['standalone']) && $GLOBALS['standalone']==true){$valid=true;}
    if (!isset($_SESSION['loggedin'])){redirect('/login.php?r='.$_SERVER['PHP_SELF']);}
    
    //if the user has admin privilege, just return true
    if ($User->isAdmin())
    {
        return true;
    }
    //otherwise, check for existence of specific permission
    if($type=='page')
    {
        if (!is_numeric($pageID))
        {
            //looking up by script name rather than id
            //get the permissions for the page that have a value of 1 -- those are required
            $sql="SELECT A.permissionID, C.displayname FROM core_permission_page A, core_pages B, core_permission_list C WHERE A.value=1 AND A.pageID=B.id AND B.filename='$pageID' AND A.permissionID=C.id";
            $dbPagePermissions=dbselectmulti($sql);
        } else {
            //get the permissions for the page that have a value of 1 -- those are required
            $sql="SELECT permissionID FROM core_permission_page WHERE value=1 AND pageID=$pageID";
            $dbPagePermissions=dbselectmulti($sql);
        }
        if ($dbPagePermissions['numrows']>0)
        {
            $valid=false;
            foreach($dbPagePermissions['data'] as $permission)
            {
                $comparisonPermission=$permission['permissionID'];
                if ($User->hasPermission($comparisonPermission)){$valid=true;}
            }
        } else {
            $valid=true;
        }
    } else {
        //just looking for the existence of a particular permission 
        if ($User->hasPermission($pageID)){$valid=true;}
    }
    //return true;
    return $valid;
}

function generate_random_string($length,$numeric=false)
{
    if($numeric)
    {
        $randstr='';
        while(strlen($randstr)<$length)
        {
            $randstr.=mt_rand(0,9);
        }
    } else {
        $randstr = "";
        for($i=0; $i<$length; $i++){
             $randnum = mt_rand(0,61);
             if($randnum < 10){
                $randstr .= chr($randnum+48);
             }else if($randnum < 36){
                $randstr .= chr($randnum+55);
             }else{
                $randstr .= chr($randnum+61);
             }
         } 
    }
    
  return $randstr;
}

function commaReturnTextBlockToArray($sets,$keys)
{
    $sets=explode("\n",$sets);
    $newarray=array();
    $keys=explode(",",$keys);
    if(count($sets)>0)
    {
        $i=0;
        foreach($sets as $set)
        {
            $temp=explode("|",$set);
            $j=0;
            foreach($keys as $id=>$key)
            {
                $newarray[$i][$key]=$temp[$j];
                $j++;
            }
            $i++;       
        }
    }
    return $newarray;    
}


function convertCSVtoTSV($line)
 {
     // Declare the new_line variable to make sure it starts empty
     $new_line = '';
     
     // replacement array for field data preparation
     $replacement_reg = array(
     '/[\n|\r|\t|\v|\f]/',            // remove all invisible return, newline, tabs, etc. from each individual field
     '/^(\s*|"*)(.*?)(\s*|"*)$/m',    // remove all white space from beginning and end of field
     '/^("*)(.*?)("*)$/m',            // remove all " from beginning and end of field
     '/""/',                            // replace all "" with " rfc4180-2.7
     );
     $replacement = array(
     "\\2",                            // replace all special characters
     "\\2",                            // replace all spaces at beginning and end of fields
     "\\2",                            // replace all " at beginning and end of fields
     '"',                            // reduce "" to " (if they exist)
     );
     
     // split the fields out into an array by the delimiter specified for CSV RFC
     $fields = preg_split('/,(?!(?:[^",]|[^"],[^"])+")/', $line);
     
     // process each fields cleansing superfluous chracters (spaces, control characters, and delimiting quotes)
     foreach($fields as $result){
     $result = preg_replace($replacement_reg, $replacement, $result);
     // add a tab to each new line
     $new_line .= $result."\t";
     }
     // replace the last tab with a new line feed
     $new_line = preg_replace('/\t$/', "\n", $new_line);
     return $new_line;
 }

function setUserMessage($message,$type='success',$location='right-bottom')
{
    if($type=='warning' || $type=='error')
    {
        $sticky='1';
        $expire='120';
    }elseif($type=='news'){
        $expire='360';
        $sticky='1';
    }else{
        $expire='30';
        $sticky='0';
    }
    $message = addslashes($message);        
    //types can be message, error and warning
    if($type=='success'){$type='message';}
    $messagearray=array('text'=>$message,'type'=>$type,'location'=>$location);
    global $userid;
    $sendTime = date("Y-m-d H:i");
    $expireTime = date("Y-m-d H:i",strtotime($sendTime." + $expire seconds")); 
    $sql = "INSERT INTO user_messages (user_id, origination_id, sent_time, expire_time, message_level, title, message, sticky) VALUES
    ('$userid', 0, '$sendTime', '$expireTime', 1, 'System Message', '$message', $sticky)";
    $dbInsert=dbinsertquery($sql);  
    $_SESSION['messages'][]=$messagearray;
}

function showUserMessages()
{
    //lets add in any mango news that is marked as "popup"
    //get the userid as well, because we won't show them any messages that are in the mango_news_viewed table
    
    
    if (count($_SESSION['messages'])>0)
    {
        
        print "<script>\n";
        foreach($_SESSION['messages'] as $m)
        {
            $type=$m['type'];
            $location=$m['location'];
            $message=str_replace(array("\r", "\n", "\t"), '',$m['text']);
            $message=htmlentities($message);
            if($type=='warning' || $type=='error')
            {
                $sticky='true';
                $time='5000';
            }elseif($type=='news'){
                $type='success';
                $time='3000';
                $sticky='true';
            }else{
                $time='3000';
                $sticky='false';
            }
            ?>
            $.ctNotify('<?php echo $message; ?>',{type: '<?php echo $type; ?>', isSticky: <?php echo $sticky; ?>, delay: <?php echo $time; ?>},'<?php echo $location; ?>');
            <?php
                
            
        } 
        print "</script>\n";
        unset($_SESSION['messages']);   
    }
}


function displayMessage($message,$type='error',$modal='true',$okbutton="Ok",$width=500)
{
    switch($type)
    {
        case "error":
        print "<div class='ui-widget' style='width:$width px;margin-top:20px;margin-left:auto;margin-right:auto;'>
            <div class='ui-state-error ui-corner-all' style='padding: 0pt 0.7em;'>";
                print "<p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: 0.3em;'></span> 
                <strong>Alert:</strong> $message</p>";
        print "     </div>\n</div>\n";
        break;
        
        case "success":
        print "<div class='ui-widget' style='width:$width px;margin-top:20px;margin-left:auto;margin-right:auto;'>
            <div class='ui-state-highlight ui-corner-all' style='padding: 0pt 0.7em;'>";
                print "<p><span class='ui-icon ui-icon-info' style='float: left; margin-right: 0.3em;'></span> 
                <strong>Success:</strong> $message</p>";
        print "     </div>\n</div>\n";
        break;
        
        case "dialog":
        ?>
        <div id="dialog-message" title="System Message">
              <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
              <?php echo $message ?>
        </div>
        <script>
            $(function() {
                $( "#dialog-message" ).dialog({
                    height: 200,
                    modal: <?php echo $modal ?>
                });
            });
        </script>
        <?php
        break;
        
        case "message":
        ?>
            <div id="dialog-message" title="System Message">
                <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
                <?php echo $message ?>
            </div>
            <script>
            $(function() {
                // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
                $( "#dialog-message" ).dialog({
                    modal: <?php echo $modal ?>,
                    buttons: {
                        Ok: function() {
                            $( this ).dialog( "close" );
                        }
                    }
                });
            });
            </script>
        <?php
        break;
        
        case "confirm":
        ?>
        <div id="dialog-confirm" title="Empty the recycle bin?">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>These items will be permanently deleted and cannot be recovered. Are you sure?</p>
        </div>

        <script>
            $(function() {
                $( "#dialog-confirm" ).dialog({
                    resizable: false,
                    height:200,
                    modal: <?php echo $modal ?>,
                    buttons: {
                        "<?php echo $okbutton ?>": function() {
                            $( this ).dialog( "close" );
                            return true;
                        },
                        Cancel: function() {
                            $( this ).dialog( "close" );
                            return false;
                        }
                    }
                });
            });
            </script>

        <?php
        break;
        
    }

}



function buildLocations($type='inserts')
{
    $insertLocations=array();
    $insertLocations[0]='Please choose';
    $insertLocations=locationNesting(0,$insertLocations,'',$type);
    return $insertLocations;    
}

function locationNesting($pid,$larray,$lead,$type='inserts')
{
    global $siteID;
    $sql="SELECT * FROM storage_locations WHERE location_type='$type' AND parent_id=$pid AND site_id=$siteID ORDER BY location_order";
    $dbLocations=dbselectmulti($sql);
    if ($dbLocations['numrows']>0)
    {
        foreach($dbLocations['data'] as $location)
        {
            $locationid=$location['id'];
            $larray[$locationid]=$lead.$location['location_name'];       
            $sql="SELECT * FROM storage_locations WHERE location_type='$type' AND parent_id=$locationid AND site_id=$siteID ORDER BY location_order";
            $dbLocations=dbselectmulti($sql);
            if ($dbLocations['numrows']>0)
            {
                $lead.="--";
                $larray=locationNesting($locationid,$larray,$lead,$type);
                $lead=substr($lead,0,strlen($lead)-2);
            } else {
                $larray[$locationid]=$lead.$location['location_name'];       
            }
        }
    }
    return $larray;
}

function crc32_file($fileName)
{
    $crc = hash_file("crc32b",$fileName);
    $crc = sprintf("%08x", 0x100000000 + hexdec($crc));
    return substr($crc, 6, 2) . substr($crc, 4, 2) . substr($crc, 2, 2) . substr($crc, 0, 2);
}


function dayDiff( $a, $b )
{
    // First we need to break these dates into their constituent parts:
    $gd_a = getdate( $a );
    $gd_b = getdate( $b );

    // Now recreate these timestamps, based upon noon on each day
    // The specific time doesn't matter but it must be the same each day
    $a_new = mktime( 12, 0, 0, $gd_a['mon'], $gd_a['mday'], $gd_a['year'] );

    $b_new = mktime( 12, 0, 0, $gd_b['mon'], $gd_b['mday'], $gd_b['year'] );

    // Subtract these two numbers and divide by the number of seconds in a
    // day. Round the result since crossing over a daylight savings time
    // barrier will cause this time to be off by an hour or two.
    return round( abs( $a_new - $b_new ) / 86400 );
}



function int2Time($sec)
{
    if($sec==0)
    {
        return "00:00";
    }
    //start with the number of seconds since midnight
    //ex: 2:30  == 150 minutes * 60 seconds =  9000 seconds
    $minutes=$sec/60; //now we have minutes
    $minutes=round($minutes,2);
    if($minutes>1)
    {
        $hours=round($minutes/60,2); //now we have decimal hours
        $parts=explode(".",$hours);
        $hours=$parts[0];
        if (strlen($hours)==1){$hours="0".$hours;}
        if (count($parts)>1)
        {
            $fracMin=".$minutes";
            $minutes=$fracMin*60;
            $minutes=intval($minutes); //drop any fractional seconds
            //pad minutes with a 0 if necessary
            if (strlen($minutes)==1){$minutes="0".$minutes;}
        } else {
            $minutes="00";
        }
        return "$hours:$minutes";
    } else {
        return "00:01";
    }
}

function int2TimeDecimal($sec)
{
    if($sec==0)
    {
        return "0.00";
    }
    //start with the number of seconds since midnight
    //ex: 2:30  == 150 minutes * 60 seconds =  9000 seconds
    $minutes=$sec/60; //now we have minutes
    $minutes=round($minutes,2);
    if($minutes>1)
    {
        $hours=round($minutes/60,2); //now we have decimal hours
        $parts=explode(".",$hours);
        $hours=$parts[0];
        //if (strlen($hours)==1){$hours="0".$hours;}
        //if(substr($hours,0,1)=='0' && strlen($hours)>1){$hours=substr($hours,1,1);}
        if (count($parts)>1)
        {
            $fracMin=$minutes;
            $minutes=$fracMin/60;
            $minutes=floatval($minutes); //drop any fractional seconds
            //pad minutes with a 0 if necessary
            if (strlen($minutes)==1){$minutes="0".$minutes;}
        } elseif(count($parts)==1) {
            $minutes=$hours.".00"; 
        } else {
            $minutes="0.00";
        }
        return $minutes;
    } else {
        return "0.00";
    }
}

function time2Int($time)
{
    $parts=explode(":",$time);
    $hours=intval($parts[0]);
    $minutes=intval($parts[1]);
    $hours=$hours*3600; //convert hours to seconds
    $minutes=$minutes*60; //convert minutes to seconds
    return ($hours+$minutes);

}

function secs2hms($timePassed)
{
    if ($timePassed<0){$timePassed=-$timePassed;$neg=true;}
    // Minute == 60 seconds
    // Hour == 3600 seconds
    // Day == 86400
    // Week == 604800
    $elapsedString = "";
    
    if($timePassed > 3600)
    {
        $hours = floor($timePassed / 3600);
        if ($hours==0){$hours="00";}
        $timePassed -= $hours * 3600;
        $elapsedString .= $hours.":";
    } else {
        $hours="00";
    }
    if($timePassed > 60)
    {
        $minutes = floor($timePassed / 60);
        if ($minutes==0){$minutes="00";}
        $timePassed -= $minutes * 60;
        $elapsedString .= $minutes.":";
    } else {
        $minutes="00";
    }
    if ($timePassed<10){$timePassed="0".$timePassed;}
    $elapsedString=$hours.":".$minutes;
    if ($neg){$elapsedString="-".$elapsedString;}
    return $elapsedString;
}

/*******************************************
* These are some formulas for calculating newsprint usage
* 
*/
function newsprintPagesPerPound($basisweight,$pagewidth=0,$pagelength=0,$paperdataid=0)
{
    //this function calculates the number of pages (broadsheet)/pound
    //for a given ream area, page size and basis weight
    global $paperdata;
    if($pagewidth==0){$pagewidth=$GLOBALS['broadsheetPageHeight'];}
    if($pagelength==0){$pagelength=$GLOBALS['broadsheetPageHeight'];}
    $reamarea=$paperdata[$paperdataid]['reamarea'];
    $ppp=($reamarea/($basisweight*2))/($pagewidth*$pagelength/144);
    return round($ppp,1);
}

function newsprintLinearFeet($rollweight,$basisweight,$rollwidth,$paperdataid=0)
{
    global $paperdata;
    //this function calculates the estimated linear feet on a roll
    $basissize=$paperdata[$paperdataid]['basissize'];
    $linearfeet=($basissize*$rollweight*500)/($basisweight*$rollwidth*12);
    return $linearfeet;
}

function newsprintLinearCopies($rollweight,$basisweight,$rollwidth,$paperdataid)
{
    global $paperdata;
    //this function calculates the estimated broadsheet page copies on a given roll
    $basissize=$paperdata[$paperdataid]['basissize'];
    $linearfeet=($basissize*$rollweight*500)/($basisweight*$rollwidth*12);
    $copies=$linearfeed/$GLOBALS['broadsheetPageHeight'];
    return $copies;
}

function newsprintTonnageEstimator($draw,$pages,$basisweight,$pagewidth=0,$pagelength=0,$paperdataid=0,$waste=0)
{
    global $paperdata;
    if($pagewidth==0){$pagewidth=$GLOBALS['broadsheetPageHeight'];}
    if($pagelength==0){$pagelength=$GLOBALS['broadsheetPageHeight'];}
    //this function estimates the amout of newsprint required for a specific job
    $reamarea==$paperdata[$paperdataid]['reamarea'];
    $pounds=($copies*$pages/2*$pagewidth/12*$pageheight/12*$basisweight*(1+$waste))/$reamarea;
    return $pounds;
}



function basisweightToGsm($basisweight,$paperdataid=0)
{
     //this function converts english weights to gsm weights 
    global $paperdata;
    $basissize=$paperdata[$paperdataid]['basissize'];
    $gsm=($basisweight*1406.5)/$basissize;
    return round($gsm,1);
}

function gsmToBasisweight($gsm,$paperdataid=0)
{
    //this function converts gsm weights to english weights
    global $paperdata;
    $basissize=$paperdata[$paperdataid]['basissize'];
    $basisweight=($gsm*$basissize)/1406.5;
    return round($basisweight,1); 
}

function weightRemainingOnRoll($rollremaining,$rollwidth,$paperdataid)
{
    //this function should be able to calculated the remaining weight on a roll
    global $coreDiameter,$paperdata;
    $factor=$paperdata[$paperdataid]['factor'];
    $remainingweight=(($rollremaining*$rollremaining)-($coreDiameter*$coreDiameter))*$rollwidth*$factor;
    return $remainingweight;
    
    //[(Roll Diameter²) - (Core Diameter²)] x Roll Width x Factor = Approximate Roll Weight 
}

function poundsToKilograms($pounds)
{
    return ($pounds/2.2046);
}

function kilogramsToPounds($kilograms)
{
    return ($kilograms*2.2046);
}

function feetToMeters($feet)
{
    return ($feet/3.28);
}

function metersToFeet($meters)
{
    return ($meters*3.28);
}

function inches2mm($inches)
{
    return round($inches*25.4,0);
}

function mm2inches($mm)
{
    return round($mm/25.4,2);
}

function inches2cm($inches)
{
    return round($inches*2.54,0);
}

function cm2inches($cm)
{
    return round($cm/2.54,2);
}


/*********************
* END OF NEWSPRINT FUNCTIONS
*/

function quicksort($seq) {
    if(!count($seq)) return $seq;
 
    $k = $seq[0];
    $x = $y = array();

        $length = count($seq);
        
    for($i=1; $i < $length; $i++) {    
         if($seq[$i] <= $k) {
             $x[] = $seq[$i];
         } else {
             $y[] = $seq[$i];
         }
     }
 
    return array_merge(quicksort($x), array($k), quicksort($y));
}

function displayAlert($alert)
{
    print "<script type='text/javascript'>
    alert($alert);
    </script>\n";
    
}

function checkTextAlerts($jobid,$action)
{
    $sql="SELECT A.stats_id,A.pub_id,A.run_id,B.pub_name,C.run_name FROM jobs A, publications B, publications_runs C WHERE A.id=$jobid AND A.pub_id=B.id AND A.run_id=C.id";
    $dbJobinfo=dbselectsingle($sql);
    $pubid=$dbJobinfo['data']['pub_id'];    
    $runid=$dbJobinfo['data']['run_id'];
    $pubname=$dbJobinfo['data']['pub_name'];
    $runname=$dbJobinfo['data']['run_name'];
    $statsid=$dbJobinfo['data']['stats_id'];
    $sql="SELECT * FROM user_textalerts WHERE run_id=$runid AND pub_id=$pubid";
    $dbUsers=dbselectmulti($sql);
    if ($dbUsers['numrows']>0)
    {
        //get start stop times
        $sql="SELECT * FROM job_stats WHERE id=$statsid";
        $dbStats=dbselectsingle($sql);
        $stats=$dbStats['data'];
        $starttime=date("H:i",strtotime($stats['startdatetime_actual']));
        $stoptime=date("H:i",strtotime($stats['stopdatetime_actual']));
        if ($action=='start')
        {
            $message="$pubname - $runname started at $starttime"; 
        } else {
            $message="$pubname - $runname finished at $stoptime";
        } 
        foreach($dbUsers['data'] as $user)
        {
            sendTextAlert($user['user_id'],$message);    
        }
    }    
 }  



function sendTextAlert($userid,$message)
{
    $sql="SELECT cell,carrier FROM users WHERE id=$userid";
    $dbCarrier=dbselectsingle($sql);
    $carrier=$dbCarrier['data']['carrier'];
    if ($carrier=='')
    {
        //do nothing
    } else {
        $cell=$dbCarrier['data']['cell'];
        switch ($carrier)
        {
            case "nextel":
            $formatted_number = $cell."@messaging.nextel.com";
            break;
            
            case "virgin":
            $formatted_number = $cell."@vmobl.com";
            break;
            
            case "cingular":
            $formatted_number = $cell."@cingularme.com";
            break;
            
            case "att":
            $formatted_number = $cell."@txt.att.net";
            break;
            
            case "sprint":
            $formatted_number = $cell."@messaging.sprintpcs.com";
            break;
            
            case "tmobile":
            $formatted_number = $cell."@tmomail.net";
            break;
            
            case "verizon":
            $formatted_number = $cell."@vtext.com";
            break;
            
            case "cricket":
            $formatted_number = $cell."@sms.mycricket.com";
            break;
            
        }
        mail("$formatted_number", "Production Update", "$message","From: ".$GLOBALS['systemEmailFromAddress']);
    }
}


function batch_geocode($addresses,$defaultLat=0,$defaultLon=0,$display=false,$updatefield='')
{
    $badads=array();
    $delay = 0;
    $base_url = "http://maps.google.com/maps/geo";
    $out="&output=csv&key=$key&sensor=false";
    //lets test a hardcoded address:
    if($display && $updatefield==''){print "Batch geocoder was handed ".count($addresses)." addresses to check...<br>";}
    // Iterate through the rows, geocoding each address
    $i=0;
    $l=1; 
    foreach ($addresses as $adid=>$caddress)
    {
      $geocode_pending = true;
      while ($geocode_pending) {
        $address = urlencode($caddress["street"].' '.$caddress['city'].' '.$caddress['state'].' '.$caddress['zip']);
        $url = $base_url . "?q=" .$address.$out;
        //$csv = file_get_contents($url);
        $temp=get_web_page($url);
        $csv=$temp['content'];

        //print "CSV RESULT: <a href='$url' target='_blank'>$url</a> -- $csv<br />\n";
        //print "Attempting to geocode $address with url of $url<br>->Result is ";
        $csvSplit=explode(",",$csv);
        $status=$csvSplit[0];
        $accuracy=$csvSplit[1];
        $lat=$csvSplit[2];
        $lon=$csvSplit[3];
        if($display)
        {
            if($updatefield!='' && $l==25)
            {
                print "<script>\$('#$updatefield').html('Checking #$i $caddress[street]<br>Result from web call was $csv with a status of $status')</script>";
                for($k = 0; $k < 320000; $k++)echo ' ';
                $l=1;
            } else if($updatefield==''){
                print "Checking $caddress[street] with url of $url<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Result from web call was $csv with a status of $status<br>";
            }
            $l++;
        }
        if ($status=="200") {
          // successful geocode
          $geocode_pending = false;
          $lat = $csvSplit[2];
          $lng = $csvSplit[3];
          if ($lat=='' || $lat==0){$lat=$defaultLat;}
          if ($lon=='' || $lon==0){$lon=$defaultLon;}
          
          if($display && $updatefield==''){print "Geocoded $adid successfully<br />";}
          $addresses[$adid]['status']='success';
          $addresses[$adid]['lat']=$lat;
          $addresses[$adid]['lon']=$lon;
          $delay=0;
        } else if ($status=="620") {
          // sent geocodes too fast
          $delay = 100000;
        } else {
          // failure to geocode
          $geocode_pending = false;
          $addresses[$adid]['status']='fail';
          $addresses[$adid]['lat']=0;
          $addresses[$adid]['lon']=0;
        }
        usleep($delay);
        //print "$status with lat=$lat and lon=$lng<br>";
      }
      $i++;
    }
   
   return $addresses;
}

function get_web_page( $url )
{
    
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
    );

    $ch      = curl_init( $url );
    
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //     // return web page
    //    CURLOPT_HEADER         => false,    // don't return headers
    //    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    //    CURLOPT_ENCODING       => "",       // handle all encodings
    //    CURLOPT_USERAGENT      => "spider", // who am i
    //    CURLOPT_AUTOREFERER    => true,     // set referer on redirect
    //    CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
    //    CURLOPT_TIMEOUT        => 120,      // timeout on response
    //    CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects)
   
   //curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return $header;
}



/**************************************************************************************
* THIS SECTION IS FOR INSERT RELATED FUNCTIONS LIKE ADDING AN INSERT, REMOVING AN INSERT
* GENERATING THE STATION MARKUP FOR A PACKAGE
* AND GETTING INSERT QTIP DATA FOR PACKAGING AND THE INSERT CALENDAR
*/



function array_unshift_assoc(&$arr, $key, $val) 
{ 
    $arr = array_reverse($arr, true); 
    $arr[$key] = $val; 
    return array_reverse($arr, true); 
}


function send_ticket_message($owner,$ticket,$priorityname,$type,$maxed)
{
    $sql="SELECT * FROM users WHERE id=$ticket[submitted_by]";
    $dbSubmitted=dbselectsingle($sql);
    $submittedby=$dbSubmitted['data']['firstname'].' '.$dbSubmitted['data']['lastname'];
    
    if($_GET['mode']=='test')
    {
        print "For $type ticket#$ticket[id]: sending an email message to $owner that was submitted by $submittedby<br>";
    } else {
        $info.="For $type ticket#$ticket[id]: sending an email message to $owner that was submitted by $submittedby<br>";
    }
    
    $from=$GLOBALS['systemEmailFromAddress'];
    //$to="$fullname <$email>";
    $to=$owner;
    if($maxed)
    {
        $subject='Trouble ticket#'.$ticket['id'].' is at the most critical priority. Please address immediately.';
    } else {
        $subject='Trouble ticket#'.$ticket['id'].' has been escalated. Priority is now '.$priorityname.'.';
    }
    $message="<html><head></head><body>\n";
    $message.= "Hi, we just wanted to let you know that this ticket is still open and needs to be resolved as soon as possible.\n";
    
    if($type=='helpdesk')
    {
        $message.= "<a href='".$GLOBALS['serverIPaddress'].$GLOBALS['systemRootPath']."helpdeskTickets.php?action=edit&id=$ticket[id]'>Click here to view the ticket in the system</a>, or read it below.<br />\n\n";
        $message.="<p>Ticket submitted by $submittedby on ".date("m/d/Y",strtotime($ticket['submitted_datetime'])).' at '.date("h:i A",strtotime($ticket['submitted_datetime'])).$ticket['problem']."</p>\n";
        $message.="<p style='font-size:14px;font-weight:bold;'>Brief:</p>\n";
        $message.="<p>".$ticket['help_brief']."</p>\n";
        $message.="<p style='font-size:14px;font-weight:bold;'>Full Request:</p>\n";
        $message.="<p>".$ticket['help_request']."</p>\n";
        $message.="<br>";
    } else {
        //maintenance ticket
        $message.= "<a href='".$GLOBALS['serverIPaddress'].$GLOBALS['systemRootPath']."maintenanceTickets.php?action=edit&id=$ticket[id]'>Click here to view the ticket in the system</a>, or read it below.<br />\n\n";
        $message.="<p>Ticket submitted by $submittedby on ".date("m/d/Y",strtotime($ticket['submitted_datetime'])).' at '.date("h:i A",strtotime($ticket['submitted_datetime'])).$ticket['problem']."</p>\n";
        $message.="<p style='font-size:14px;font-weight:bold;'>Problem:</p>\n";
        $message.="<p>".$ticket['problem']."</p>\n";
        $message.="<p style='font-size:14px;font-weight:bold;'>Attempted Fix:</p>\n";
        $message.="<p>".$ticket['attempt']."</p>\n";
        $message.="<br>";
    }
    $message.="Mango Help Desk";
    $message.="</body></html>\n";
    $message = wordwrap($message, 70);

    $mail = new htmlMimeMail();
    
    $mail->setHtml($message);
    $mail->setFrom($from);
    $mail->setSubject($subject);
    $mail->send(array($to));
}

function convertCSVtoArray($fileContent,$escape = '\\', $enclosure = '"', $delimiter = ',')
{
    $lines = array();
    $fields = array();

    if($escape == $enclosure)
    {
        $escape = '\\';
        $fileContent = str_replace(array('\\',$enclosure.$enclosure,"\r\n","\r"),
                    array('\\\\',$escape.$enclosure,"\\n","\\n"),$fileContent);
    }
    else
        $fileContent = str_replace(array("\r\n","\r"),array("\\n","\\n"),$fileContent);

    $nb = strlen($fileContent);
    $field = '';
    $inEnclosure = false;
    $previous = '';

    for($i = 0;$i<$nb; $i++)
    {
        $c = $fileContent[$i];
        if($c === $enclosure)
        {
            if($previous !== $escape)
                $inEnclosure ^= true;
            else
                $field .= $enclosure;
        }
        else if($c === $escape)
        {
            $next = $fileContent[$i+1];
            if($next != $enclosure && $next != $escape)
                $field .= $escape;
        }
        else if($c === $delimiter)
        {
            if($inEnclosure)
                $field .= $delimiter;
            else
            {
                //end of the field
                $fields[] = $field;
                $field = '';
            }
        }
        else if($c === "\n")
        {
            $fields[] = $field;
            $field = '';
            $lines[] = $fields;
            $fields = array();
        }
        else
            $field .= $c;
        $previous = $c;
    }
    //we add the last element
    if(true || $field !== '')
    {
        $fields[] = $field;
        $lines[] = $fields;
    }
    return $fields;
}

  
class pointLocation {
    var $pointOnVertex = true; // Check if the point sits exactly on one of the vertices
    function pointLocation() {
    }
    
    
    function pointInPolygon($point, $vertices, $pointOnVertex = true) {
        //$debug=true;
        $this->pointOnVertex = $pointOnVertex;
        // Check if the point sits exactly on a vertex
        if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
            return true;
        }
        if($GLOBALS['debug'] || $debug)
        {
            print "Lat is $point[y] and Lon is $point[x] for the test point<br>";
        }
        // Check if the point is inside the polygon or on the boundary
        $intersections = 0; 
        $vertices_count = count($vertices);
        if($GLOBALS['debug'] || $debug)
        {
            print "There are $vertices_count vertices in the polygon<br>";
        }
        // Check if the point is inside the polygon or on the boundary
        $intersections = 0; 
        $vertices_count = count($vertices);
        if($point['y']<0){$point['y']+=90;}
        if($point['x']<0){$point['x']+=180;}
        
        for ($i=1; $i < $vertices_count; $i++) {
            $vertex1 = $vertices[$i-1]; 
            $vertex2 = $vertices[$i];
            $vertex1['x']+=180;
            $vertex2['x']+=180;
            if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) { // Check if point is on an horizontal polygon boundary
                return true;
            }
            if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) { 
                $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x']; 
                if ($xinters == $point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                    return true;
                }
                if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                    $intersections++; 
                }
            } 
        } 
        if($GLOBALS['debug'] || $debug)
        {
            print "Found a total of $intersections intersections<br>";
        }
        // If the number of edges we passed through is even, then it's in the polygon. 
        if ($intersections % 2 != 0) {
            //print "inside";
            return true;
        } else {
            //print "outside";
            return false;
        }
    }
    
    function pointOnVertex($point, $vertices) {
        foreach($vertices as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }
    
    }
    function pointStringToCoordinates($pointString) {
        $coordinates = explode(" ", $pointString);
        return array("x" => $coordinates[0], "y" => $coordinates[1]);
    } 
} 

 class GeoLocation {

     
     
    /* 
         *
         * Example Usage 
     *
    */

    /*
    *
        $edison = GeoLocation::fromDegrees(40.5187154, -74.4120953);
        $brooklyn = GeoLocation::fromDegrees(40.65, -73.95);

        echo "distance from edison to brooklyn: " . $edison->distanceTo($brooklyn, 3958.762079) . " miles \n";
        echo "distance from edison to brooklyn: " . $edison->distanceTo($brooklyn, 6371.01) . " kilometers \n";
    *
    */

    /*
    *
        $edison = GeoLocation::fromDegrees(40.5187154, -74.4120953);

            // get bounding coordinates 40 kilometers from location;
        $coordinates = $edison->boundingCoordinates(40,  6371.01);

        print_r($coordinates);
        echo "min latitude: " . $coordinates[0]->degLat . " \n";
        echo "min longitude: " . $coordinates[0]->degLon . " \n";

        echo "max latitude: " . $coordinates[1]->degLat . " \n";
        echo "max longitude: " . $coordinates[1]->degLon . " \n";
    *
    */
     
     
    public $radLat;  // latitude in radians
    public $radLon;  // longitude in radians

    public $degLat;     // latitude in degrees
    public $degLon;  // longitude in degrees

    public function __construct() {
        define('MIN_LAT', deg2rad(-90));  // -PI/2
        define('MAX_LAT', deg2rad(90));   //  PI/2
        define('MIN_LON', deg2rad(-180)); // -PI
        define('MAX_LON', deg2rad(180));  //  PI
    }

    /**
     * @param latitude the latitude, in degrees.
     * @param longitude the longitude, in degrees.
     * @return GeoLocation
     */
    public static function fromDegrees($latitude, $longitude) {
        $location = new GeoLocation();
        $location->radLat = deg2rad($latitude);
        $location->radLon = deg2rad($longitude);
        $location->degLat = $latitude;
        $location->degLon = $longitude;
        $location->checkBounds();
        return $location;
    }

    /**
     * @param latitude the latitude, in radians.
     * @param longitude the longitude, in radians.
     * @return GeoLocation
     */
    public static function fromRadians($latitude, $longitude) {
        $location = new GeoLocation();
        $location->radLat = $latitude;
        $location->radLon = $longitude;
        $location->degLat = rad2deg($latitude);
        $location->degLon = rad2deg($longitude);
        $location->checkBounds();
        return $location;
    }

    protected function checkBounds() {
        if ($this->radLat < MIN_LAT || $this->radLat > MAX_LAT ||
                $this->radLon < MIN_LON || $this->radLon > MAX_LON)
            throw new Exception("Invalid Argument");
    }

    /**
     * Computes the great circle distance between this GeoLocation instance
     * and the location argument.
     * @param radius the radius of the sphere, e.g. the average radius for a
     * spherical approximation of the figure of the Earth is approximately
     * 6371.01 kilometers.
     * @return the distance, measured in the same unit as the radius
     * argument.
     */
    public function distanceTo(GeoLocation $location, $radius) {
        return acos(sin($this->radLat) * sin($location->radLat) +
                    cos($this->radLat) * cos($location->radLat) *
                    cos($this->radLon - $location->radLon)) * $radius;
    }

    /**
     * @return the latitude, in degrees.
     */
    public function getLatitudeInDegrees() {
        return $this->degLat;
    }

    /**
     * @return the longitude, in degrees.
     */
    public function getLongitudeInDegrees() {
        return $this->degLon;
    }

    /**
     * @return the latitude, in radians.
     */
    public function getLatitudeInRadians() {
        return $this->radLat;
    }

    /**
     * @return the longitude, in radians.
     */
    public function getLongitudeInRadians() {
        return $this->radLon;
    }

    public function __toString() {
        return "(" . $this->degLat . ", " . $this->degLong . ") = (" .
                $this->radLat . " rad, " . $this->radLon . " rad";
    }


    /**
     * <p>Computes the bounding coordinates of all points on the surface
     * of a sphere that have a great circle distance to the point represented
     * by this GeoLocation instance that is less or equal to the distance
     * argument.</p>
     * <p>For more information about the formulae used in this method visit
     * <a href="http://JanMatuschek.de/LatitudeLongitudeBoundingCoordinates">
     * http://JanMatuschek.de/LatitudeLongitudeBoundingCoordinates</a>.</p>
     * @param distance the distance from the point represented by this
     * GeoLocation instance. Must me measured in the same unit as the radius
     * argument.
     * @param radius the radius of the sphere, e.g. the average radius for a
     * spherical approximation of the figure of the Earth is approximately
     * 6371.01 kilometers.
     * @return an array of two GeoLocation objects such that:<ul>
     * <li>The latitude of any point within the specified distance is greater
     * or equal to the latitude of the first array element and smaller or
     * equal to the latitude of the second array element.</li>
     * <li>If the longitude of the first array element is smaller or equal to
     * the longitude of the second element, then
     * the longitude of any point within the specified distance is greater
     * or equal to the longitude of the first array element and smaller or
     * equal to the longitude of the second array element.</li>
     * <li>If the longitude of the first array element is greater than the
     * longitude of the second element (this is the case if the 180th
     * meridian is within the distance), then
     * the longitude of any point within the specified distance is greater
     * or equal to the longitude of the first array element
     * <strong>or</strong> smaller or equal to the longitude of the second
     * array element.</li>
     * </ul>
     */
    public function boundingCoordinates($distance, $radius) {
        if ($radius < 0 || $distance < 0) throw new Exception('Arguments must be greater than 0.');

        // angular distance in radians on a great circle
        $radDist = $distance / $radius;

        $minLat = $this->radLat - $radDist;
        $maxLat = $this->radLat + $radDist;

        $minLon = 0;
        $maxLon = 0;
        if ($minLat > MIN_LAT && $maxLat < MAX_LAT) {
            $deltaLon = asin(sin($radDist) /
                cos($this->radLat));
            $minLon = $this->radLon - $deltaLon;
            if ($minLon < MIN_LON) $minLon += 2 * pi();
            $maxLon = $this->radLon + $deltaLon;
            if ($maxLon > MAX_LON) $maxLon -= 2 * pi();
        } else {
            // a pole is within the distance
            $minLat = max($minLat, MIN_LAT);
            $maxLat = min($maxLat, MAX_LAT);
            $minLon = MIN_LON;
            $maxLon = MAX_LON;
        }

        return array(
            GeoLocation::fromRadians($minLat, $minLon), 
            GeoLocation::fromRadians($maxLat, $maxLon)
        );
    }
}

function var_log(&$varInput, $var_name='', $reference='', $method = '=', $sub = false) {

    static $output ;
    static $depth ;

    if ( $sub == false ) {
        $output = '' ;
        $depth = 0 ;
        $reference = $var_name ;
        $var = serialize( $varInput ) ;
        $var = unserialize( $var ) ;
    } else {
        ++$depth ;
        $var =& $varInput ;
        
    }
        
    // constants
    $nl = "\n" ;
    $block = 'a_big_recursion_protection_block';
    
    $c = $depth ;
    $indent = '' ;
    while( $c -- > 0 ) {
        $indent .= '|  ' ;
    }

    // if this has been parsed before
    if ( is_array($var) && isset($var[$block])) {
    
        $real =& $var[ $block ] ;
        $name =& $var[ 'name' ] ;
        $type = gettype( $real ) ;
        $output .= $indent.$var_name.' '.$method.'& '.($type=='array'?'Array':get_class($real)).' '.$name.$nl;
    
    // havent parsed this before
    } else {

        // insert recursion blocker
        $var = Array( $block => $var, 'name' => $reference );
        $theVar =& $var[ $block ] ;

        // print it out
        $type = gettype( $theVar ) ;
        switch( $type ) {
        
            case 'array' :
                $output .= $indent . $var_name . ' '.$method.' Array ('.$nl;
                $keys=array_keys($theVar);
                foreach($keys as $name) {
                    $value=&$theVar[$name];
                    var_log($value, $name, $reference.'["'.$name.'"]', '=', true);
                }
                $output .= $indent.')'.$nl;
                break ;
            
            case 'object' :
                $output .= $indent.$var_name.' = '.get_class($theVar).' {'.$nl;
                foreach($theVar as $name=>$value) {
                    var_log($value, $name, $reference.'->'.$name, '->', true);
                }
                $output .= $indent.'}'.$nl;
                break ;
            
            case 'string' :
                $output .= $indent . $var_name . ' '.$method.' "'.$theVar.'"'.$nl;
                break ;
                
            default :
                $output .= $indent . $var_name . ' '.$method.' ('.$type.') '.$theVar.$nl;
                break ;
                
        }
        
        // $var=$var[$block];
        
    }
    
    -- $depth ;
    
    if( $sub == false )
        return $output ;
        
}

  

function checkCache($type,$parameters)
{
    $path=getcwd();
    if(strpos($path,"includes")>0)
    {
        if(strpos($path,"ajax_handlers")>0)
        {
            $path="../../cache/"; 
        } else {
            $path="../cache/";
        }  
    } else {
        $path="cache/";
    }
    if(substr($type,0,8)=='jobBoxes')
    {
        $jobid=str_replace('jobBoxes','',$type);
        $type='jobBoxes';
    }
    switch ($type)
    {
        case 'presscalendar':
            $filename=$type.'-'.$parameters.".txt";
        break;
        case 'jobBoxes':
            $filename=$type.'-'.$parameters.".txt";
        break;
        case 'menu':
            $filename=$type.'-'.$parameters.".txt";
        break;
        default:
        $filename='cache.txt';
        break;
    }
    $filename=$path.$filename;
    
    if(file_exists($filename))
    {
        return file_get_contents($filename);
    } else {
        return false;
    }    
}

function setCache($type,$parameters,$values)
{
    $path=getcwd();
    if(strpos($path,"includes")>0)
    {
        if(strpos($path,"ajax_handlers")>0)
        {
            $path="../../cache/"; 
        } else {
            $path="../cache/";
        }  
    } else {
        $path="cache/";
    }
    if(!file_exists($path))
    {
        mkdir($path);
    }
    if(substr($type,0,8)=='jobBoxes')
    {
        $jobid=str_replace('jobBoxes','',$type);
        $type='jobBoxes';
    }
    switch ($type)
    {
        case 'presscalendar':
            $filename=$type.'-'.$parameters.".txt";
        break;
        case 'jobBoxes':
            $filename=$type.'-'.$parameters.".txt";
        break;
        case 'menu':
            $filename=$type.'-'.$parameters.".txt";
        break;
        default:
            $filename='cache.txt';
        break;
    }
    //if cache directory does not exist, create it
    if(!file_exists($path))
    {
        mkdir($path,0777);
    }
    
    $filename=$path.$filename;
    $handle = fopen($filename, "w+");
    fwrite($handle, $values);
    fclose($handle);
    
    return $filename;
    
}

function clearCache($type)
{   
    //we'll just delete all cached files of that "type"
    $path=getcwd();
    if(strpos($path,"includes")>0)
    {
        if(strpos($path,"ajax_handlers")>0)
        {
            $path="../../cache/"; 
        } else {
            $path="../cache/";
        }  
    } else {
        $path="cache/";
    }
    $files = scandir($path);
    if(count($files)>0)
    {
        foreach($files as $id=>$file)
        {
            $t=explode("-",$file);
            $t=reset($t);
            if($t==$type)
            {
                unlink($path.$file);
            }    
        }
    }
}

function multiKeyExists(array $arr, $key) {

    // is in base array?
    if (array_key_exists($key, $arr)) {
        return true;
    }

    // check arrays contained in this array
    foreach ($arr as $element) {
        if (is_array($element)) {
            if (multiKeyExists($element, $key)) {
                return true;
            }
        }

    }

    return false;
}
   
function generateLabel($fields)
{
    $url=$GLOBALS['labellerURL'];
    $fields_string='';
    //$fields_string=http_build_query($fields);
    
    foreach($fields as $key=>$value)
    {
        $fields_string.="$key: '$value',";
    } 
    rtrim($fields_string,",");
    
    /*
    $script="
    \$(document).ready(function(){
        \$.ajax({
            url: '$url',
            data: ({ $fields_string }),
            type: 'POST',
            dataType: 'jsonp',
            onSuccess: function(response) {
                
            } 
        })
        
    })
    
    
    ";
    $GLOBALS['scripts']['ajaxCall']=$script;
    */
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => true,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
        CURLOPT_URL            => $url,
        CURLOPT_POST           => count($fields),
        CURLOPT_POSTFIELDS     => $fields_string
    );

    $ch      = curl_init( $url );
    curl_setopt_array( $ch, $options );
    
    
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    
    if($content=='success')
    {
        return true;
    } else {
        return false;
    }
    
} 



/*
*   Send email function
*    $params
            string $to -- email address of recipient
            string $subject --- email subject
            string $message -- rich html markup or plain text for email message
            int $adId -- id of ad (maybe used for misc)
            array $attachments -- array of arrays in the format ("filename"=>"full path to file, with DOMAIN!", "name"=>"Your invoice")
     returns a status code
        1 = 'success'
        2 = no email given
        3 = problem sending email
        4 = user was blacklisted
*/
function send_email( $to, $subject, $messageContent, $attachments = array() )
{
    $return[1] = array('status'=>1,'message'=>'Success');
    $return[2] = array('status'=>2,'message'=>'No email address');
    $return[3] = array('status'=>3,'message'=>'Problem sending email');
    $return[4] = array('status'=>4,'message'=>'Recipient was blacklisted');
    $return[5] = array('status'=>5,'message'=>'Too many email flags');

    

    // generate message start
    $messageStart = '';
    $messageStart .= '<html>';
    $messageStart .= '<head>';
    $messageStart .= '<title>' . SITE_URL . '</title>';
    $messageStart .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
    $messageStart .= '<style type="text/css">';
    $messageStart .= '#outlook a { padding: 0; } ';
    $messageStart .= 'body { width: 100% !important; -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; } ';
    $messageStart .= '.ReadMsgBody { width: 100%; } ';
    $messageStart .= '.ExternalClass { width: 100%; } ';
    $messageStart .= '.backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; } ';
    $messageStart .= 'table td { border-collapse: collapse; } ';
    $messageStart .= '.ExternalClass * { line-height: 115%; } ';
    $messageStart .= '</style>';
    $messageStart .= '</head>';
    $messageStart .= '<body bgcolor="#FFFFFF" marginwidth="0" marginheight="0" style="zoom: 100%;">';
    $messageStart .= '<table><tbody>';
    // generate message end
    $messageEnd = '';
    $messageEnd .= '</tbody>';
    $messageEnd .= '</table>';
    $messageEnd .= '</body>';
    $messageEnd .= '</html>';


    // compose full message
    $message = $messageStart . $messageContent . $messageEnd;

    $mail = new htmlMimeMail();
    $mail->setHtml($message);
    $mail->setFrom($GLOBALS['systemEmailFromAddress']);
    $mail->setSubject($subject);
    $mail->setHeader('Sender','Mango System Central');
    
    if(!empty($attachments))
    {
       foreach($attachments as $attachID=>$attachment)
        {
            $attachment = array_merge(array('type'=>'application/octet-stream'),$attachment);
            $file = file_get_contents($attachment['filename']);
            $mail->addAttachment($file,$attachment['name'], $attachment['type']);
        }
    }
    
    if( $mail->send( array($to), 'mail' ) ) {
        return $return[1];
    } else {
        return $return[3];
    }
}

/**
* Return the total number of weeks of a given month.
* @param int $year
* @param int $month
* @param int $start_day_of_week (0=Sunday ... 6=Saturday)
* @return int
*/
function weeks_in_month($year, $month, $start_day_of_week)
{
    // Total number of days in the given month.
    $num_of_days = date("t", mktime(0,0,0,$month,1,$year));

    // Count the number of times it hits $start_day_of_week.
    $num_of_weeks = 0;
    for($i=1; $i<=$num_of_days; $i++)
    {
      $day_of_week = date('w', mktime(0,0,0,$month,$i,$year));
      if($day_of_week==$start_day_of_week)
        $num_of_weeks++;
    }
    return $num_of_weeks;
}


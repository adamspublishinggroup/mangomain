<?php
  /*
  *  Boot handler
  *  this script takes care of all normal page boot initializations
  *  all non-ajax scripts need to include boot.php in the first line
  */
$ts = gmdate("D, d M Y H:i:s") . " GMT";
header("Expires: $ts");
header("Last-Modified: $ts");
header("Pragma: no-cache");
header("Cache-Control: no-cache, must-revalidate");
if($_GET['popup']){$popup=true;}
if(!isset($render)) $render = 'html';
if(!isset($systemMode)) $systemMode = false;
if(!isset($dbClass)) $dbClass = false;
if(!isset($pageFluid)) $pageFluid = false;
$debugger = array();
session_start();
/* load the site config.json file */
$docRoot = $_SERVER['DOCUMENT_ROOT'];
if($docRoot == '') $docRoot = '/var/www/';
define("ABS_PATH", $docRoot);
$urlRoot = str_replace("/var/www","",ABS_PATH)."/";
if(strpos(strtolower($urlRoot),".com")>0)
{
    $urlRoot=explode(".com",$urlRoot);
    $urlRoot=end($urlRoot);
}
$urlRoot = str_replace("/html","",$urlRoot);
define("URL_ROOT", $urlRoot);
define("INCLUDE_DIR",ABS_PATH."/includes/");

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'].'/';
    return $protocol.$domainName;
}
define( 'SITE_URL', siteURL() );
if($systemMode)
{
    //echo "ABS path is ".ABS_PATH;
}
if(!file_exists(ABS_PATH.'/includes/config.json'))
{
    die("Config file not present. Aborting bootstrap");
} else {
    $baseConfig = json_decode(file_get_contents(ABS_PATH.'/includes/config.json'),true);
    $baseConfig = $baseConfig['config'];
    $debugger[]=$baseConfig;
    define( 'DB_HOST', $baseConfig['database']['host'] ); // set database host
    define( 'DB_USER', $baseConfig['database']['username'] ); // set database user
    define( 'DB_PASS', $baseConfig['database']['password'] ); // set database password
    define( 'DB_NAME', $baseConfig['database']['database'] ); // set database name
    define( 'SEND_ERRORS_TO', $baseConfig['send_errors_to'] ); // set database name
    define( 'MODE', $baseConfig['mode'] ); // set database name
    define( 'SITE_ID', $baseConfig['site_id'] ); // set database name 
}
$databaseCalls = array();
if($dbClass)
{
    require (INCLUDE_DIR.'classes/Database.php');
} else {
    if (version_compare(phpversion(), '7.0.0', '<')) {
        require (INCLUDE_DIR.'functions_db_php5.php');
    } else {
        require (INCLUDE_DIR.'functions_db.php');
    }
}
require (INCLUDE_DIR.'config.php');

require (INCLUDE_DIR.'functions_formtools.php');
require (INCLUDE_DIR.'functions_graphics.php');
require (INCLUDE_DIR.'mail/htmlMimeMail.php');

require (INCLUDE_DIR.'functions_common.php');
require (INCLUDE_DIR.'functions_user.php');
require (INCLUDE_DIR.'functions_maintenance.php');
require (INCLUDE_DIR.'functions_packaging.php');
require (INCLUDE_DIR.'functions_press.php');
require (INCLUDE_DIR.'functions_recurring.php');
require (INCLUDE_DIR.'functions_compat.php');

/* load classes */
if($GLOBALS['debug']){error_reporting (E_ALL ^ E_NOTICE);}else{$GLOBALS['debug']=0;error_reporting(E_ERROR);}

if($systemMode==true)
{
    /* this is a mode where the system is executing solo, without a logged in user (like from a cron job) 
    *  No Page is needed in this case
    */
    /* load classes */
    require (INCLUDE_DIR.'classes/Prefs.php');
    require (INCLUDE_DIR.'classes/Report.php');
    $Prefs = Prefs::Instance(SITE_ID);

} else {
    /* load classes */
    require (INCLUDE_DIR.'classes/bootstrapModal.php');
    //require (ABS_PATH.'/includes/classes/DebugLogger.php');
    require (INCLUDE_DIR.'classes/Report.php');
    require (INCLUDE_DIR.'classes/Page.php');
    require (INCLUDE_DIR.'classes/Prefs.php');
    require (INCLUDE_DIR.'classes/User.php');
    $User = User::Instance($_SESSION['userid']);
    $Prefs = Prefs::Instance(SITE_ID);
    if($Prefs->maintenanceMode==1 && $_SERVER['SCRIPT_NAME']!='/corePreferences.php'){redirect("maintenanceMode.php");}
    if($render=='html')
    {
        $Page = Page::Instance($popup);  
    }                            
}
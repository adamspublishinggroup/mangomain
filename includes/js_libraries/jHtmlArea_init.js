$(function(){
    $(".GuiEditor").htmlarea({
        toolbar: [
                ["html"], ["bold", "italic", "underline", "strikethrough", "|", "subscript", "superscript"],
                ["increasefontsize", "decreasefontsize"],
                ["orderedlist", "unorderedlist"],
                ["indent", "outdent"],
                ["justifyleft", "justifycenter", "justifyright"],
                ["link", "unlink", "image", "horizontalrule"],
                ["p", "h1", "h2", "h3", "h4", "h5", "h6"],
                ["cut", "copy", "paste"],
                // custom spellcheck button
                [{
                        css: "check-spelling-button",
                        text: "Check spelling",
                        action: function(btn) {
                                // initiate the spellchecker
                                $(this.editor.body)
                                .spellchecker({
                                        url: "/includes/ajax_handlers/checkspelling.php",
                                        lang: "en", 
                                        engine: "google",
                                        suggestBoxPosition: "below",
                                        innerDocument: false,
                                        wordlist: {
                                                action: "after",
                                                element: $(".jHtmlArea")
                                        },  
                                })
                                .spellchecker("check", function(result){
                                        (result) && alert('There are no incorrectly spelled words.');
                                });
                        }
                }]
        ]
});
});
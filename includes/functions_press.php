<?php
  /*
  *   Press - related functions  
  */
  
  
function press_stats($statsid,$jobid)
 {
    global $siteID, $pressid,$sizes;
    $sql="SELECT * FROM job_stats WHERE id=$statsid";
    $dbStats=dbselectsingle($sql);
    $stats=$dbStats['data'];
    
    $sql="SELECT * FROM jobs WHERE id=$jobid";
    $dbJob=dbselectsingle($sql);
    $jobinfo=$dbJob['data'];
    $schedstart=$jobinfo['startdatetime'];
    $schedstop=$jobinfo['enddatetime'];
    $draw=$jobinfo['draw'];     
    $pubid=$jobinfo['pub_id'];
    $runid=$jobinfo['run_id'];
    $pubdate=$jobinfo['pub_date'];
    $folder=$jobinfo['folder'];
    $goodtime=$stats['goodcopy_actual'];
    $actualstart=$stats['startdatetime_actual'];
    $actualstop=$stats['stopdatetime_actual'];
    if($goodtime==''){
      $goodtime=$actualstart;  
    }
    
    //math for run length calculation
    //get minutes between actual stop and start
    $start=strtotime($actualstart);
    $stop=strtotime($actualstop);
    
    $startoffset=round(($start-strtotime($schedstart))/60,2);
    $finishoffset=round(($stop-strtotime($schedstop))/60,2);
    $schedruntime=round((strtotime($schedstop)-strtotime($schedstart))/60,2);
    $printdate=date("Y-m-d",strtotime($startD));
    $downtime=$stats['total_downtime'];
    $runningtime=round(($stop-$start)/60,2); //should give us running time in decimal minutes
    $goodrunningtime=$runningtime-$downtime;
    
    $counterstart=$stats['counter_start'];
    $counterstop=$stats['counter_stop'];
    $spoilsstartup=$stats['spoils_startup'];
    
    //calculations for waste, speeds, spoils
    $gross=$counterstop-$counterstart;
    $spoilstotal=$gross-$draw;
    $spoilsrunning=$counterstop-$counterstart-$spoilsstartup-$draw;
    $wastepercent=round($spoilstotal/$draw*100,2);
    
    $runspeed=round($gross/($runningtime/60),0);
    $goodrunspeed=round($gross/($goodrunningtime/60),0);
    
    $layoutid=$jobinfo['layout_id'];
    $pagewidth=$jobinfo['pagewidth'];
    $totaltons=0;
    
    if ($jobinfo['data_collected']==0)
    {
        pressWearTear($jobid);       
    }
            
    
   
   
    
    $pubdate=$jobinfo['pub_date'];
    $pubid=$jobinfo['pub_id'];
    $insertpubid=$jobinfo['insert_pub_id'];
    $runid=$jobinfo['run_id'];
    if($jobinfo['redo_job_id']!=0)
    {
        //means we are in a redo, in which case we actually need the original
        //so, we need to requery with jobid = the redo job id
        $reprintid=$jobid;
        $jobid=$jobinfo['redo_job_id'];
        $sql="SELECT * FROM jobs WHERE id=$jobid";
        $dbJobInfo=dbselectsingle($sql);
        $jobinfo=$dbJobInfo['data'];
        $pubdate=$jobinfo['pub_date'];
        $pubid=$jobinfo['pub_id'];
        $runid=$jobinfo['run_id'];
        $insertpubid=$jobinfo['insert_pub_id'];
        
    } else {
        $reprintid=0;
    }
    
    
    $layoutid=$jobinfo['layout_id'];
    if($layoutid!=0)
    {
        $sql="SELECT * FROM layout_sections WHERE layout_id=$layoutid ORDER BY section_number";
        $dbLayout=dbselectmulti($sql);
        
        //get job section information
        $sql="SELECT * FROM jobs_sections WHERE job_id=$jobid";
        $dbJobSections=dbselectsingle($sql);
        $jobSections=$dbJobSections['data'];
        $overrun=0;
        $sinfo=array();
        foreach($dbLayout['data'] as $lay)
        {
            $sinfo[$lay['section_number']]['towers']=explode("|",$lay['towers']);
            $sinfo[$lay['section_number']]['overrun']=$jobSections['data']['section'.$lay['section_number'].'_overrun'];
            if ($jobSections['data']['section'.$lay['section_number'].'_overrun']>0)
            {
                $overrun=$jobSections['data']['section'.$lay['section_number'].'_overrun'];
            }
        }
        
        //calculate plates
        $sql="SELECT * FROM job_plates WHERE job_id=$jobid AND color=0 AND version=1";
        $dbBWplates=dbselectmulti($sql);
        $plates_bw=$dbBWplates['numrows'];
        $sql="SELECT * FROM job_plates WHERE job_id=$jobid AND color=1 AND version=1";
        $dbColorplates=dbselectmulti($sql);
        if ($dbColorplates['numrows']>0)
        {
            foreach($dbColorplates['data'] as $plate)
            {
                $plateid=$plate['id'];
                $sql="SELECT id FROM job_pages WHERE color=1 AND plate_id=$plateid";
                $dbCheckColor=dbselectmulti($sql);
                if($dbCheckColor['numrows']>0)
                {
                    $plates_color+=3;
                }
                $plates_bw+=1;
            }
        }
        
        //calculate pages
        //need to look up section format to convert pages to standard pages
        
        $pages_bw=0;
        $pages_color=0;
        for($i=1;$i<=3;$i++)
        {
            $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND section_code='".$jobSections['section'.$i.'_code']."' AND color=0 AND version=1";
            $dbBWpages=dbselectmulti($sql);
            $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND section_code='".$jobSections['section'.$i.'_code']."' AND color=1 AND version=1";
            $dbColorpages=dbselectmulti($sql);
            $secname="section".$i."_producttype";
            $b=$dbBWpages['numrows'];
            $c=$dbColorpages['numrows'];
            if ($jobSections[$secname]==1 || $jobSections[$secname]==2)
            {
                //tab 
                if (($b2/2)<1)
                {
                    $pages_bw+=0;   
                } else {
                    $pages_bw+=ceil($b/2);
                
                }
                if (($c2/2)<1)
                {
                    $pages_color+=0;   
                } else {
                    $pages_color+=ceil($c/2);
                }
                
            }elseif($jobSections['section'.$i.'_producttype']==3)
            {
               //flexi
               if (($b/4)<0)
               {
                   $pages_bw+=0;
               } else {
                   $pages_bw+=ceil($b/4);
               }
                if (($c/4)<0)
               {
                   $pages_color+=0;
               } else {
                   $pages_color+=ceil($c/4);
               }
                
            }else{
                //broadsheet
                $pages_bw+=$b;
                $pages_color+=$c;
            }
            if (!$GLOBALS['treatGateFoldasFull'])
            {
                if ($jobSections['section'.$i.'_gatefold']){$pages_color--;}   
            }
            
        }
        $totalpages=$pages_bw+$pages_color;
        $totalimpressions=$totalpages*$gross;
        $manhours=($pressmancount*$runningtime)/60;
        if ($totaltons!=0)
        {
            $hoursperton=round($manhours/$totaltons,2);
        } else {
            $hoursperton=0;
        }
        if ($manhours!=0)
        {
            $impressionsperhour=round($impressionsperhour/$manhours);
        } else {
            $impressionsperhour=0;
        }
        
        //now get plate and page times
        $sql="SELECT * FROM job_plates WHERE job_id=$jobid ORDER BY black_receive DESC LIMIT 1";
        $dbLastPlate=dbselectsingle($sql);
        $lastPlate=$dbLastPlate['data']['black_receive'];
        if ($lastPlate!=''){$lastPlate=", last_plate='$lastPlate'";}else{$lastPlate='';}
        
        $sql="SELECT * FROM job_pages WHERE job_id=$jobid ORDER BY page_release DESC LIMIT 1";
        $dbLastPage=dbselectsingle($sql);
        $lastPage=$dbLastPage['data']['page_release'];
        if ($lastPage!=''){$lastPage=", last_page='$lastPage'";}else{$lastPage='';}
        
        $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND color=1 ORDER BY color_release DESC LIMIT 1";
        $dbLastColor=dbselectsingle($sql);
        $lastColor=$dbLastColor['data']['color_release'];
        if ($lastColor!=''){$lastColor=", last_color='$lastColor'";}else{$lastColor='';}
        
        
        //last thing before saving
        //if this is a reprint job we want to make sure to reset the job id to
        //the reprint id, otherwise we're going to mess with the original data
        //so....
        if ($reprintid!=0)
        {
            $jobid=$reprintid;
        }
        
        //updating an existing stat file
            $sql="UPDATE job_stats SET folder='$folder', startdatetime_goal='$schedstart',
             startdatetime_actual='$actualstart', stopdatetime_goal='$schedstop', 
             stopdatetime_actual='$actualstop', run_time='$runningtime', run_speed='$runspeed', 
             good_runspeed='$goodrunspeed', counter_start='$counterstart',  
             counter_stop='$counterstop', spoils_startup='$spoilsstartup', gross='$gross', 
             spoils_running='$spoilsrunning', spoils_total='$spoilstotal', draw='$draw', 
             goodcopy_actual='$goodtime', total_downtime='$downtime', waste_percent='$wastepercent',  
           plates_bw='$plates_bw', plates_color='$plates_color', start_offset='$startoffset', 
            finish_offset='$finishoffset', sched_runtime='$schedruntime', pages_color='$pages_color',
            pages_bw='$pages_bw', $lastPlateUpdate $lastPageUpdate $lastColorUpdate man_hours='$manhours', total_tons='$totaltons', hours_per_ton='$hoursperton', impressions_per_hour='$impressionsperhour',  tower_info='$towerinfo'
             WHERE id=$statsid";
            $dbUpdate=dbexecutequery($sql);
            $error.=$dbUpdate['error'];
            
        $datatime=date("Y-m-d H:i:s");
        $databy=$_SESSION['userid'];
       
        $jobsql="UPDATE jobs SET data_collected=1, dataset_time='$datatime', dataset_by='$databy', notes_press='$notes', stats_id='$statsid' WHERE id=$jobid";
        $dbJobUpdate=dbexecutequery($jobsql);
        $error.=$dbJobUpdate['error'];
        
        
        //lets see if this was a job that should have converted to a insert, but wasn't due to the vagaries of 
        //the recurring job system
        //look up the run to find out if it is a "run inserts" job
        $sql="SELECT * FROM publications_runs WHERE id=$runid";
        $dbRun=dbselectsingle($sql);
        $run=$dbRun['data'];
        if($run['run_inserts'])
        {
            //yes, it's supposed to be an insert as well, lets look for an insert with this job id as weprint_id
            $sql="SELECT * FROM inserts WHERE weprint_id=$jobid";
            $dbCheck=dbselectsingle($sql);
            if($dbCheck['numrows']==0)
            {
                //oops, it hasn't been created, we need to do that now
                printJob2Inserter($jobid);
            }
        }
        printJob2Bindery($jobid);
        printJob2Delivery($jobid);
        
        // update any existing insert tied to this job with a receive=1 and a receive_count= total produced
        $sql="UPDATE inserts SET received=1, receive_count='$draw', receive_by='0', receive_date='".date("Y-m-d")."', receive_datetime='".date("Y-m-d H:i")."' WHERE weprint_id='$jobid";
        $dbInsertUpdate=dbexecutequery($sql);
        $error.=$dbInsertUpdate['error'];
        
        if($error!='')
        {
            print $error;
        }
        
    }
 }    

 
function printJob2Inserter($jobid,$createinsert=0,$debug=false)
{
    $sql="SELECT * FROM jobs WHERE id=$jobid";
    $dbJob=dbselectsingle($sql);
    $job=$dbJob['data'];
    $pubid=$job['pub_id'];
    $runid=$job['run_id'];
    $pubdate=$job['pub_date'];
    $drawTotal=$job['draw'];
    $insertpubid=$job['insertpub_id'];
    $requiresInserting=$job['requires_inserting'];
    $notesInserting=$job['notes_inserting'];
    $GLOBALS['notes'].="SQL: $sql<br>";
    $GLOBALS['notes'].="Run id = $runid, requires inserting = $requiresInserting<br>";
    if($insertpubid=='' || $insertpubid==0){$insertpubid=$pubid;}
    //first, see if there is an existing insert with this job id, if so, then we'll just update draw, date and insert pub as needed
    $standardPages=0;
    $productPages=0;
    
    $sql="SELECT * FROM job_sections WHERE job_id=$jobid";
    $dbSections=dbselectsingle($sql);
    if($dbSections['numrows']>0)
    {
        $sections=$dbSections['data'];
        for($i=1;$i<=3;$i++)
        {
            $sql="SELECT count(id) as pagecount FROM job_pages WHERE job_id=$jobid AND version=1 AND section_code='".$sections['section'.$i.'_code']."'";
            $dbPageCount=dbselectsingle($sql);
            if($dbPageCount['numrows']>0)
            {
               $pages=$dbPageCount['data']['pagecount'];
               if($sections['section'.$i.'_producttype']==0)
               {
                   $standardPages+=$pages;
                   $productPages+=$pages;
               }
               if($sections['section'.$i.'_producttype']==1 || $sections['section'.$i.'_producttype']==2)
               {
                   $standardPages+=($pages/2);
                   $productPages+=$pages;
               }
               if($sections['section'.$i.'_producttype']==3)
               {
                   $standardPages+=($pages/4);
                   $productPages+=($pages/2);
               }
            }
        }    
    }
    
    
    
    $sql="SELECT * FROM inserts WHERE weprint_id=$jobid AND clone_id=0";
    $GLOBALS['notes'].= "Checking for existing insert against $jobid job.<br />";
    $dbExisting=dbselectsingle($sql);
    if($dbExisting['numrows']>0)
    {
        $GLOBALS['notes'].="Found an existing insert.<br />";
        $existingid=$dbExisting['data']['id'];
        
        
        $sql="UPDATE inserts SET insert_count='$drawTotal', receive_count='$drawTotal', standard_pages='$standardPages', product_pages='$productPages', 
        insert_pub_id='$insertpubid', pub_id='$pubid' WHERE id=$existingid";
        $dbUpdate=dbexecutequery($sql);
        $GLOBALS['notes'].="Updated existing insert with $sql<br />";
        
        /*
        make sure there is a schedule, if not, create one
        */
        $sql="SELECT id FROM inserts_schedule WHERE insert_id = $existingid";
        $dbSched = dbselectsingle($sql);
        $scheduleID = $dbSched['data']['id'];
        if($dbSched['numrows']>0)
        {
            $sql="UPDATE inserts_schedule SET pub_id=$pubid, pressrun_id='$runid', insert_quantity='$drawTotal', insert_date='$pubdate' WHERE insert_id=$existingid";
            $dbUpdate=dbexecutequery($sql);
            $GLOBALS['notes'].="Updated existing insert schedule with $sql<br />";
        } else {
            $sql="INSERT INTO inserts_schedule (insert_id, pub_id, pressrun_id, run_id, insert_quantity, insert_date, zoning, killed) VALUES 
            ('$existingid', '$pubid', '$runid', '0', '$drawTotal', '$pubdate', '', 0)";
            $dbInsert=dbinsertquery($sql);
            $scheduleID = $dbInsert['insertid'];
            
            
            $GLOBALS['notes'].="Inserted a new insert schedule with $sql<br />";
        }
        //need to auto-add all zones for this pub to this insert
        $insertDoW=date("w",strtotime($pubdate));
        $sql = "SELECT id, dow_$insertDoW FROM publications_insertzones WHERE dow_$insertDoW>0 AND pub_id = $pubid";
        $dbZones = dbselectmulti($sql);
        if($dbZones['numrows']>0)
        {
            foreach($dbZones['data'] as $zone)
            {
                $zones[]="($scheduleID,$existingid,$zone[id],".$zone['dow_'.$insertDoW].")";  
            }
            $zones = implode(",",$zones);
            $sql="DELETE FROM inserts_zoning WHERE schedule_id=$scheduleID and insert_id=$existingid";
            $dbClear = dbexecutequery($sql);
            
            //add
            $sql="INSERT INTO inserts_zoning (schedule_id, insert_id, zone_id, zone_count) VALUES $zones";
            $dbInsert = dbinsertquery($sql);
        }
         
    } else {
        
        $sql="SELECT * FROM publications WHERE id=$pubid";
        $dbPub=dbselectsingle($sql);
        $pubname=$dbPub['data']['pub_name'];
        
        $sql="SELECT * FROM publications WHERE id=$insertpubid";
        $dbInsertPub=dbselectsingle($sql);
        $buildinsertplan=$dbInsertPub['data']['insert_run']; //this checks for auto insert run creation
        
        $sql="SELECT * FROM publications_runs WHERE id=$runid";
        $dbRun=dbselectsingle($sql);
        $runinserts=$dbRun['data']['run_inserts'];
        $runname=$dbRun['data']['run_name'];
        $mess= "build insertplan was $buildinsertplan and runname was $runname and runinsert as $runinserts<br />\n";    
        $GLOBALS['notes'].=$mess;
        if($requiresInserting || $createinsert){$runinserts=true;}
        $GLOBALS['notes'].="No existing found. have runinsert set to $runinserts.<br />";
        
        //for now we are disabling building insert plans
        $buildinsertplan=false;
        
        
        /*
        if ($buildinsertplan)
        {
            //need to get the default RUN for this pub
            $pubday=date("w",strtotime($pubdate));
            $sql="SELECT * FROM publications_insertruns WHERE pub_id=$insertpubid AND run_days like '%$pubday%' AND main_run=1";
            $GLOBALS['notes'].= 'run select->'.$sql;
            $dbDRun=dbselectsingle($sql);
            if ($dbDRun['numrows']>0)
            {
                $inserterrunid=$dbDRun['data']['id'];
                $daysprev=$dbDRun['data']['days_prev'];
                $starttime=$dbDRun['data']['run_time'];
            } else {
                $inserterrunid=0;
                $starttime='22:00';
                $daysprev=1;
            }
            $inserterid=$GLOBALS['defaultInserter'];
            $sql="SELECT * FROM inserters WHERE id=$inserterid";
            $dbInserter=dbselectsingle($sql);
            $inserter=$dbInserter['data'];
            $speed=$inserter['single_out_speed'];
            if ($speed=='' || $speed==0){$speed=12000;}
            $runtime=$drawTotal/($speed/60); 
            $runtime=round($runtime,0);
            if ($runtime<30){$runtime=30;}
            
            $startdatetime=date("Y-m-d",strtotime($pubdate."-$daysprev days"))." ".$starttime;
            $stopdatetime=date("Y-m-d H:i",strtotime($startdatetime." +$runtime minutes"));
            $continue=false;
            $packagedate=date("Y-m-d",strtotime($startdatetime));
            $GLOBALS['notes'].= "<br />Runtime = $runtime <br />start $startdatetime<br />stop $stopdatetime<br />\n";
            $inserterid=$GLOBALS['defaultInserter'];
            $packdate=date("Y-m-d",strtotime($pubdate." - 1 day"));
            
            //lets check to see if we already have one first
            $sql="SELECT * FROM jobs_inserter_plans WHERE inserter_id=$inserterid AND pub_id=$insertpubid AND run_id=$insertrunid AND pub_date='$pubdate'";
            $dbCheck=dbselectmulti($sql);
            if($dbCheck['numrows']==0)
            {
                //save the plan first
                $sql="INSERT INTO jobs_inserter_plans (inserter_id,pub_id, run_id, pub_date, inserter_request, address, 
                site_id, num_packages) VALUES ('$inserterid', '$insertpubid', '$insertrunid', '$pubdate', '$drawTotal', 
                '0', '$siteID', '1')";
                $dbPlanInsert=dbinsertquery($sql);
                $planid=$dbPlanInsert['insertid'];
                $GLOBALS['notes'].= "<br />Adding a plan with $sql<br>Error (if any) is $dbPlanInsert[error]<br />\n";
                $sql="INSERT INTO jobs_inserter_packages (inserter_id, pub_date, package_date, package_startdatetime, package_stopdatetime, 
                package_name, package_runlength, double_out, pub_id,  jacket_insert_id, inserter_request) VALUES 
                ('$inserterid','$pubdate', '$packagedate', '$startdatetime', '$stopdatetime', 'Main', '$runtime', '0', 
                '$insertpubid', '0', '$drawTotal')";
                 $dbInsert=dbinsertquery($sql);
                 $error=$dbInsert['error'];
                 $packageid=$dbInsert['numrows'];    
                 $GLOBALS['notes'].= "<br />inserting package with $sql<br>Error (if any) $error\n";
                //set up the package settings
                 $sql="SELECT * FROM inserters WHERE id=$inserterid";
                 $dbInserter=dbselectsingle($sql);
                 $inserter=$dbInserter['data'];
                 $sql="INSERT INTO jobs_inserter_packages_settings (package_id, reject_misses, reject_doubles, miss_fault, double_fault, attempt_repair, gap, delivery, copies_per_bundle, turns) VALUES ('$packageid', '$inserter[reject_misses]','$inserter[reject_doubles]','$inserter[miss_fault]','$inserter[double_fault]','$inserter[attempt_repair]','$inserter[gap]','$inserter[delivery]','$inserter[copies_per_bundle]','$inserter[turns]')";
                 $dbInsert=dbinsertquery($sql);
            } else {
                $GLOBALS['notes'].= "<br />Plan and package already exist<br />\n";
            }
        }
        */
        if ($runinserts)
        {
            
            global $wePrintAdvertiserID;
            if($wePrintAdvertiserID==0){$wePrintAdvertiserID=1;}
            $rdate=date("Y-m-d");
            
            //this means that we need to book this run as an insert for this pub date and pub_id
            $sql="INSERT INTO inserts (insert_tagline, advertiser_id, insert_count, shipper, printer, ship_type, ship_quantity, receive_count, product_size, product_pages,
            standard_pages, receive_by, receive_date, weprint_id, site_id, spawned, created_datetime, created_by, received, scheduled, 
            confirmed) VALUES ('WE PRINT - $runname','$wePrintAdvertiserID',  '$drawTotal', 'WE PRINTED', 'WE PRINTED', 'pallet',  '1',
            '$drawTotal', 0, '$productPages', '$standardPages', 'WE PRINTED', '$rdate','$jobid', '$siteID', 1, '".date("Y-m-d H:i:s")."',0,1,1,1)";
            if($debug){print "Inserted insert with $sql<br />";}
            $dbInsert=dbinsertquery($sql);
            $GLOBALS['notes'].="<br />inserting an insert with $sql<br />\n";
            if ($dbInsert['error']!=''){$GLOBALS['notes'].= $dbInsert['error'];}
            $insertid=$dbInsert['insertid'];
            
            $insertday=date("w",strtotime($pubdate));
            $insertdate=date("Y-m-d",strtotime($pubdate));
                                    
            //figure out if there is an insert run for this pub date
            $runsql="SELECT * FROM publications_insertruns WHERE pub_id='$pubid' AND run_days LIKE '%$insertday%'";
            $dbRun=dbselectsingle($runsql);
            if($dbRun['numrows']>0)
            {
                $insertrunid=$dbRun['data']['id'];
            } else {
                $insertrunid=0;
            }
            
            $schedsql="INSERT INTO inserts_schedule (insert_id, pub_id, run_id, pressrun_id, insert_quantity, insert_date, zoning, killed) 
            VALUES ('$insertid', '$insertpubid', '$insertrunid', '$runid', '$drawTotal', '$insertdate', '', 0)";
            $dbInsertSchedule=dbinsertquery($schedsql);
            $scheduleID = $dbInsertSchedule['insertid'];
            
            //need to auto-add all zones for this pub to this insert
            $insertDoW=date("w",strtotime($insertdate));
            $sql = "SELECT id, dow_$insertDoW FROM publications_insertzones WHERE dow_$insertDoW>0 AND pub_id = $pubid";
            $dbZones = dbselectmulti($sql);
            if($dbZones['numrows']>0)
            {
                foreach($dbZones['data'] as $zone)
                {
                    $zones[]="($scheduleID,$existingid,$zone[id],".$zone['dow_'.$insertDoW].")";  
                }
                $zones = implode(",",$zones);
                $sql="DELETE FROM inserts_zoning WHERE schedule_id=$scheduleID and insert_id=$existingid";
                $dbClear = dbexecutequery($sql);
                
                //add
                $sql="INSERT INTO inserts_zoning (schedule_id, insert_id, zone_id, zone_count) VALUES $zones";
                $dbInsert = dbinsertquery($sql);
            }
              
            
            $GLOBALS['notes'].="<br />adding schedule for this insert with $schedsql<br />\n";
            if ($dbInsertSchedule['error']!=''){$GLOBALS['notes'].= $dbInsertSchedule['error'];}
                
        }
    }
    
}


function printJob2Bindery($jobid)
{
    //ok, we need to look and see if there is a need for a binder job first
    $sql="SELECT * FROM jobs WHERE id=$jobid";
    $dbJob=dbselectsingle($sql);
    $job=$dbJob['data'];
    if($job['stitch']==1 || $job['trim']==1)
    {
        //yep, it's a candidate
        //lets see if it's already been created...
        $sql="SELECT * FROM bindery_jobs WHERE job_id=$jobid";
        $dbBin=dbselectsingle($sql);
        if($dbBin['numrows']>0)
        {
            //ok, we have an existing job... we'll just move on
        } else {
            $by=$_SESSION['userid'];
            $dt=date("Y-m-d H:i");
            if($job['bindery_startdate']!=$job['pub_date'])
            {
                $requestStart=date("Y-m-d",strtotime($job['bindery_startdate']));
            } else {
                $requestStart=date("Y-m-d",strtotime($job['pub_date']."-5 days"));
            }
            //need to create a record for it!
            $sql="INSERT INTO bindery_jobs (pub_id, run_id, job_id, pub_date, draw, stitch, trim, quarterfold, glossy_cover,glossy_insides, 
            glossy_cover_draw, glossy_insides_count, bindery_duedate, site_id, created_by, created_datetime,  request_startdate, notes) 
            VALUES ('$job[pub_id]', '$job[run_id]', '$job[id]', '$job[pub_date]', '$job[draw]', '$job[stitch]', '$job[trim]', 
            '$job[quarterfold]', '$job[glossy_cover]', '$job[glossy_insides]', '$job[glossy_cover_draw]','$job[glossy_insides_count]', 
            '$job[bindery_duedate]', ".SITE_ID.", $by, '$dt', '$requestStart', '".addslashes($job['notes_bindery'])."')";
            $dbInsert=dbinsertquery($sql);
            if($dbInsert['error']!='')
            {
                setUserMessage('There was a problem creating the bindery portion of the job.<br>'.$dbInsert['error'],'error');
            } else {
                $GLOBALS['notes'].="<br />Successfully added a bindery job record<br />\n";
            }
        }
    }    
}

function printJob2Delivery($jobid)
{
     global $siteID;
    $sql="SELECT * FROM jobs WHERE id=$jobid";
    $dbJob=dbselectsingle($sql);
    $job=$dbJob['data'];
    if($job['request_delivery']==1)
    {
        //see if the delivery record exists
        $sql="SELECT * FROM delivery_jobs WHERE job_id=$jobid";
        $dbCheck=dbselectsingle($sql);
        if($dbCheck['numrows']>0)
        {
            //exists, nothing we need to do about it.
        } else {
            //create a new delivery record
            //pull customer id from pub
            $sql="SELECT customer_id FROM publications WHERE id=$job[pub_id]";
            $dbCustomer=dbselectsingle($sql);
            $customerid=$dbCustomer['data']['customer_id'];
            
            $due=date("Y-m-d",strtotime($job['pub_date']."-2 days"));
            $sql="INSERT INTO delivery_jobs (job_id, pub_date, delivery_due, customer_id, notes) VALUES 
            ('$jobid', '$job[pub_date]', '$due', '$customerid]', '$job[notes_delivery]')";
            $dbInsert=dbinsertquery($sql);
            if($dbInsert['error']!='')
            {
                setUserMessage('There was a problem creating the delivery portion of the job.<br>'.$dbInsert['error'],'error');
            }else {
                $GLOBALS['notes'].="<br />Successfully added a delivery job record<br />\n";
            }
        }   
    }
}
function printJob2Addressing($jobid)
{
    $sql="SELECT * FROM jobs WHERE id=$jobid";
    $dbJob=dbselectsingle($sql);
    $job=$dbJob['data'];
    if($job['request_addressing']==1)
    {
        //see if the address record exists
        $sql="SELECT * FROM addressing_jobs WHERE job_id=$jobid";
        $dbCheck=dbselectsingle($sql);
        if($dbCheck['numrows']>0)
        {
            //exists, nothing we need to do about it.
        } else {
            //create a new delivery record
            //pull customer id from pub
            
            $due=date("Y-m-d",strtotime($job['pub_date']."-3 days"));
            $draw=$job['draw'];
            $sql="INSERT INTO addressing_jobs (job_id,due_date,draw) VALUES 
            ('$jobid', '$due', '$draw')";
            $dbInsert=dbinsertquery($sql);
            if($dbInsert['error']!='')
            {
                setUserMessage('There was a problem creating the addressing portion of the job.<br>'.$dbInsert['error'],'error');
            }else {
                $GLOBALS['notes'].="<br />Successfully added an addressing job record<br />\n";
            }
        }   
    }
}


function job_nav()
{
    //lets see which publications this user has access to
    global $User, $Page, $pubs;
    $sql="SELECT * FROM user_publications WHERE user_id=".$User->id." AND value=1";
    $dbPubs=dbselectmulti($sql);
    if ($dbPubs['numrows']>0)
    {
        $pubids="";
        foreach($dbPubs['data'] as $pub)
        {
            $pubids.=$pub['pub_id'].",";
        }
        $pubids=substr($pubids,0,strlen($pubids)-1);
        $pubfilter="AND A.pub_id IN ($pubids)";
    } else {
        $pubfilter="";
    }
    
    $jobid=$_GET['jobid'];
    $lastid=$_GET['lastid'];
    $action=$_GET['action'];
    $now=date("Y-m-d H:i");
    $nowdate=date("Y-m-d");
    if ($jobid!=0)
    {
        $sql="SELECT * FROM jobs WHERE id=$jobid";
        $dbCurrentJob=dbselectsingle($sql);
        $start=$dbCurrentJob['data']['startdatetime'];
        $end=$dbCurrentJob['data']['enddatetime'];
        if ($action=='prev')
        {
            $sql="SELECT * FROM jobs A WHERE A.enddatetime<='$start' AND A.status=1 $pubfilter AND site_id=".SITE_ID." ORDER BY A.enddatetime DESC LIMIT 1";    
        } elseif ($action=='next')
        {
            $sql="SELECT * FROM jobs A WHERE A.startdatetime>='$end' AND A.status=1 $pubfilter AND site_id=".SITE_ID." ORDER BY A.startdatetime ASC LIMIT 1";
        }
    }elseif($lastid!=0)
    {   
        //ok, we have a lastid, so we're looking for a less or greater job id than the last one
        $sql="SELECT * FROM jobs WHERE id=$lastid";
        $dbLastJob=dbselectsingle($sql);
        $start=$dbLastJob['data']['startdatetime'];
        $end=$dbLastJob['data']['enddatetime'];
        if ($action=='prev')
        {
            $sql="SELECT * FROM jobs A WHERE A.site_id=".SITE_ID."  AND A.enddatetime<='$start' AND A.status=1 $pubfilter ORDER BY A.enddatetime DESC LIMIT 1";    
        } elseif ($action=='next')
        {
            $sql="SELECT * FROM jobs A WHERE A.site_id=".SITE_ID."  AND A.startdatetime>='$end' AND A.status=1 $pubfilter ORDER BY A.startdatetime ASC LIMIT 1";
        }
        if ($_GET['bug']){print "<br>Got a last id of $lastid<br>Checking with action of $action and a sql of $sql<br>";}
        $dbCurrentJob=dbselectsingle($sql);
        if ($dbCurrentJob['numrows']==0)
        {
            //no previous or next job, so default to the job closest to now
            $sql="SELECT * FROM jobs A WHERE A.site_id=$siteID AND A.startdatetime>='$now' AND A.status=1 $pubfilter ORDER BY startdatetime ASC LIMIT 1";
            $dbCurrentJob=dbselectsingle($sql);
            if ($dbCurrentJob['numrows']==0)
            {
                //so no jobs beyond this point, just pick the latest job then
                $sql="SELECT * FROM jobs A WHERE A.site_id=".SITE_ID."  AND A.status=1 AND $pubfilter ORDER BY A.startdatetime DESC LIMIT 1";
                $dbCurrentJob=dbselectsingle($sql);
            }
        }
    
    } else { 
        //no job id or last id specified, just pick the job closest to now
        $sql="SELECT * FROM jobs A WHERE A.site_id=".SITE_ID."  AND A.startdatetime>='$now' AND A.status=1 $pubfilter ORDER BY A.startdatetime ASC LIMIT 1";
        $dbCurrentJob=dbselectsingle($sql);
        if ($dbCurrentJob['numrows']==0)
        {
            //so no jobs beyond this point, just pick the latest job then
            $sql="SELECT * FROM jobs A WHERE site_id=".SITE_ID."  $pubfilter ORDER BY startdatetime DESC LIMIT 1";
            $dbCurrentJob=dbselectsingle($sql);
        }
        
    }
    $jobid=$dbCurrentJob['data']['id'];
    //print "Current id is $jobid";
    //print "Using $sql";
    $job=$dbCurrentJob['data'];
    $jobid=$job['id'];
    $pubdate=date("D F j, Y",strtotime($job['pub_date']));

    //gather all the data about this job
    $sql="SELECT * FROM publications WHERE id=$job[pub_id]";
    $dbPub=dbselectsingle($sql);
    $pub=$dbPub['data'];
    $pubname=$pub['pub_name'];

    $sql="SELECT * FROM publications_runs WHERE id=$job[run_id]";

    $dbRun=dbselectsingle($sql);
    $run=$dbRun['data'];
    $runname=$run['run_name'];

    $prev="<a href='?action=prev&lastid=$jobid'><i class='fa fa-2x fa-backward'></i></a>\n";
    $next="<a href='?action=next&lastid=$jobid'><i class='fa fa-2x fa-forward'></i></a>\n";
        
    $current['job']=$job;
    $current['pub']=$pub;
    $current['run']=$run;       
    
    print "<div id='nav' class='row' style='margin-bottom:20px;'>\n";
        print "<div class='col-xs-1' style='padding:6px;'><span class='pull-right'>$prev</span></div>";
        print "<div id='nav_center' class='col-xs-10 bg-primary text-center'>\n";
            print "<div class='row'>\n";
            print "<input type='hidden' id='jobid' name='jobid' value='$jobid'>\n";
            print "<input type='hidden' id='ares' name='ares' value=''>\n";
            
            if($_POST)
            {
                $start=addslashes($_POST['date_start'])." 00:01";
                $end=addslashes($_POST['date_end'])." 23:59";
                $sql="SELECT A.*, B.pub_name, C.run_name FROM jobs A, publications B, publications_runs C 
                WHERE A.pub_id=B.id AND A.run_id=C.id AND A.site_id=".SITE_ID." AND A.status<>99 AND A.startdatetime>='$start' 
                AND A.startdatetime<='$end' ".($_POST['pub_id']==0 ? "$pubfilter" : "AND A.pub_id=".intval($_POST['pub_id']))." ORDER BY A.startdatetime ASC";
                //print "$sql<br />";
            } else {
                //here we will make a quick jump of jobs 24 hours back and 48 hours ahead
                $start=date("Y-m-d H:i",strtotime("-24 hours"));
                $end=date("Y-m-d H:i",strtotime("+48 hours"));
                $sql="SELECT A.*, B.pub_name, C.run_name FROM jobs A, publications B, publications_runs C 
                WHERE A.pub_id=B.id AND A.run_id=C.id AND A.site_id=".SITE_ID." AND A.status<>99 AND A.startdatetime>='$start' 
                AND A.startdatetime<='$end' $pubfilter ORDER BY A.startdatetime ASC";
                //print "$sql<br />";
            }
            $dbQuickJobs=dbselectmulti($sql);
            
            if ($dbQuickJobs['numrows']>0)
            {
                print "<div class='col-xs-12 col-sm-5'><select class='form-control' style='margin: 10px auto;width:100%' name='quickjob' id='quickjob' onchange='pressQuickJump(this.value);'>\n";
                foreach($dbQuickJobs['data'] as $qj)
                {
                    $pname=$qj['pub_name'];
                    $rname=$qj['run_name'];
                    $qjpubdate=date("D F j",strtotime($qj['pub_date']));
                    $starttime=date("m/d H:i",strtotime($qj['startdatetime']));
                    print "<option id='opt_$qj[id]' name='opt_$qj[id]' value='$qj[id]' ".($qj['id']==$jobid ? 'selected' : '').">$pname - $rname for $qjpubdate, Start: $starttime</option>\n";
                }
                print "</select></div>\n";
            }
            /*
            *  Now a form to allow searching
            */
            print "<div class='col-xs-12 col-sm-7' style='color:black;padding-top:10px;'>\n";
            print "<form class='form-inline' method=post>\n";
            ?>
            <div class="form-group">
             <label for="pub_id" style='color:white;'>Pub</label>
             <select class="form-control" name="pub_id" id="pub_id" style='width: 200px;'>
             <?php
                 if(count($pubs)>0)
                 {
                     $pubs[0]="Any publication";
                     foreach($pubs as $id=>$value)
                     {
                         print "<option value='$id' ".($id==$_POST['pub_id'] ? "selected" : "").">".$value."</option>\n";
                     }
                 }
             ?>
             </select>
            </div>
            <div class="form-group" >
             <label for="date_start" style='color:white;'>Date</label>
             <div class='input-group date' id='date_start' style='width: 120px;' >
                <input type='text' class="form-control" name='date_start' style='font-size:80%' value="<?php echo $_POST['date_start'] ?>"/>
                <span class="input-group-addon">
                    <span class="fa fa-calendar-o"></span>
                </span>
             </div>
             <div class='input-group date' id='date_end' style='width: 120px;' >
                <input type='text' class="form-control" name='date_end'  style='font-size:80%' value="<?php echo $_POST['date_end'] ?>"/>
                <span class="input-group-addon">
                    <span class="fa fa-calendar-o"></span>
                </span>
             </div> 
            </div>
            <div class="form-group btn-group">
             <input type="submit" class="form-control btn btn-success" name="submit" value='Search'>
             <a href='".$_SERVER['PHP_SELF']."' class='btn btn-primary'>Reset</a>
            </div>
            <?php
$script = <<<SCRIPT
    $(function () {
        $('#date_start').datetimepicker({format: 'YYYY-MM-DD'});
        $('#date_end').datetimepicker({
            format: 'YYYY-MM-DD',
            useCurrent: false //Important! See issue #1075
        });
        $("#date_start").on("dp.change", function (e) {
            $('#date_end').data("DateTimePicker").minDate(e.date);
        });
        $("#date_end").on("dp.change", function (e) {
            $('#date_start').data("DateTimePicker").maxDate(e.date);
        });
    });
SCRIPT;
            $Page->addScript($script);
            print "</form>\n";
            print "</div>\n";
            print "</div>\n";
        print "</div>\n";
        print "<div class='col-xs-1' style='padding:6px;'><span>$next</span></div>";
        
    print "</div>\n";
   
    return $current;
}


function saveLayout($layoutid,$jobid)
{
    global $siteID;
    //now, lets create the plates for this job
    //we'll need to get the pub code for the publication
    //also, pub date and section codes for all sections
    $jobsql="SELECT A.*, B.pub_code FROM jobs A, publications B WHERE A.id=$jobid AND A.pub_id=B.id";
    //print "Job select sql:<br>$jobsql<br>";
    $dbJob=dbselectsingle($jobsql);
    $job=$dbJob['data'];
    print $jobsql;
    //get some info about the run selected
    $sql="SELECT * FROM publications_runs WHERE id=$job[run_id]";
    $dbRun=dbselectsingle($sql);
    $runInfo=$dbRun['data'];
    $productcode=$runInfo['run_productcode'];
    
    $pubcode=$job['pub_code'];
    $pubdate=date("Y-md",strtotime($job['pub_date']));
    $pubid=$job['pub_id'];
    
    //we're actually build pages and plates at this time
    //existing pages/plates will be located first, in case it's just a color update
    $layouttime=date("Y-m-d H:i");
    $layoutby=$_SESSION['userid'];
    $sql="UPDATE jobs SET layout_id=$layoutid, layoutset_time='$layouttime', layoutset_by='$layoutby' WHERE id=$jobid";
    $dbUpdate=dbexecutequery($sql);
    $error.=$dbUpdate['error'];
    

    $jobsection="SELECT * FROM jobs_sections WHERE job_id=$jobid";
    //print "Job section sql:<br>$jobsection<br>";
    $dbJSection=dbselectsingle($jobsection);
    $jsection=$dbJSection['data'];
    $scode[1]=$jsection['section1_code'];
    $scode[2]=$jsection['section2_code'];
    $scode[3]=$jsection['section3_code'];
    
    //now get layout sections
    $lsql="SELECT * FROM layout_sections WHERE layout_id=$layoutid";
    //print "Job layout sections sql:<br>$lsql<br>";
    $dbLSections=dbselectmulti($lsql);


    //first, delete any potential existing job plates and pages
    $sql="DELETE FROM job_pages WHERE job_id=$jobid";
    $dbDelete=dbexecutequery($sql);
    $sql="DELETE FROM job_plates WHERE job_id=$jobid";
    $dbDelete=dbexecutequery($sql);
    $colorconfigs=$GLOBALS['colorconfigs'];
    if ($dbLSections['numrows']>0)
    {
        foreach ($dbLSections['data'] as $lsection)
        {
            
            $section_number=$lsection['section_number'];
            $towers=$lsection['towers'];
            $towers=explode("|",$towers);
            foreach ($towers as $tower)
            {
                $created=date("Y-m-d H:i:s");
                //lets look up the color for a tower
                $sql="SELECT color_config FROM press_towers WHERE id=$tower";
                $dbColor=dbselectsingle($sql);
                if ($dbColor['numrows']>0)
                {
                    if ($dbColor['data']['color_config']=='K')
                    {
                        $color=0;
                        $spot=0;
                        $possiblecolor=0;
                    }else if ($dbColor['data']['color_config']=='K/S'){
                        $color=0;
                        $spot=1;
                        $possiblecolor=1;
                    }else{
                        $color=1;
                        $spot=0;
                        $possiblecolor=1;
                    }
                    $tcolor=array_search($dbColor['data']['color_config'],$colorconfigs,true);
                } else {
                    $color=0;
                    $possiblecolor=0;
                    $tcolor=0;
                }
                
                $plate1="";
                $plate2="";
                $pages1=array();
                $pages2=array();
                $lowpage1=9999; //set arbitrarily high so it gets set immediately to the new page
                $lowpage2=9999; //set arbitrarily high so it gets set immediately to the new page
                //now we need the pages for this layout & tower -- 10 side, then 13 side
                $psql="SELECT * FROM layout_page_config WHERE layout_id=$layoutid AND tower_id=$tower";
                $dbPages=dbselectmulti($psql);
                if ($dbPages['numrows']>0)
                {
                    foreach ($dbPages['data'] as $page)
                    {
                        $side=$page['side'];
                        $page_num=$page['page_number'];
                        if ($page_num!=0)
                        {
                            if ($side==10)
                            {
                                if ($page_num<$lowpage1 && $page_num!=0){$lowpage1=$page_num;}
                                $pages1[]="$pubid, $jobid,'$scode[$section_number]','$productcode', '$pubcode','$pubdate',$color,$spot,$possiblecolor,$tower, $tcolor,$page_num, 1,'$created', 1, '$siteID'),";
                            } else {
                                if ($page_num<$lowpage2 && $page_num!=0){$lowpage2=$page_num;}
                                $pages2[]="$pubid, $jobid,'$scode[$section_number]','$productcode', '$pubcode','$pubdate',$color,$spot,$possiblecolor,$tower, $tcolor, $page_num, 1,'$created', 1, '$siteID'),";
                            }
                        }            
                    
                    }
                    //now we should have 2 items, 2 arrays with pages and a low page number for each plate
                    $plate1="INSERT INTO job_plates (pub_id, job_id, section_code, run_productcode, pub_code, pub_date, low_page, color, spot, version, created, current, site_id) VALUES
                    ($pubid,$jobid, '$scode[$section_number]','$productcode', '$pubcode', '$pubdate','$lowpage1',$color,$spot, 1,'$created', 1, '$siteID')";
                    $dbPlate1=dbinsertquery($plate1);                                             
                    $error.=$dbPlate1['error'];
                    print "Plate save 1 sql:<br>$plate1<br>";

                    $plate1ID=$dbPlate1['insertid'];
                    $plate2="INSERT INTO job_plates (pub_id, job_id, section_code, run_productcode, pub_code, pub_date, low_page, color, spot, version, created, current, site_id) VALUES
                    ($pubid,$jobid, '$scode[$section_number]','$productcode', '$pubcode', '$pubdate','$lowpage2',$color,$spot, 1,'$created', 1, '$siteID')";
                    $dbPlate2=dbinsertquery($plate2);
                    $plate2ID=$dbPlate2['insertid'];
                    $error.=$dbPlate2['error'];
                    //print "Plate save 2 sql:<br>$plate2<br>";

                    //now insert the pages
                    $values1="";
                    foreach($pages1 as $page)
                    {
                        $values1.="($plate1ID,$page";    
                    }
                    $values1=substr($values1,0,strlen($values1)-1);
                    $page1="INSERT INTO job_pages (plate_id, pub_id, job_id, section_code, run_productcode, pub_code, pub_date, color, spot, 
                    possiblecolor, tower_id, tower_color, page_number, version, created, current, site_id) VALUES $values1";
                    $dbPage1=dbinsertquery($page1);
                    $error.=$dbPage1['error'];
                    //print "Page save 1 sql:<br>$page1<br>";

                    //now insert the pages
                    $values2="";
                    foreach($pages2 as $page)
                    {
                        $values2.="($plate2ID,$page";    
                    }
                    $values2=substr($values2,0,strlen($values2)-1);
                    $page2="INSERT INTO job_pages (plate_id, pub_id, job_id, section_code, run_productcode, pub_code, pub_date, color, spot, 
                    possiblecolor, tower_id, tower_color, page_number, version, created, current, site_id) VALUES $values2";
                    $dbPage2=dbinsertquery($page2);
                    $error.=$dbPage2['error'];
                    //print "Page save 2 sql:<br>$page2<br>";

                }
            }
         }
    }
    if($error!='')
   {
       print $error;
   }
}
<?php
include("bootstrapAjax.php");
 
  $campaign_id=intval($_POST['campaign_id']);
  
  $nai_ids=$_POST['nai_ids'];
  $p_ids=$_POST['prosp_ids'];
  $cities=str_replace("city_","",$_POST['cities']);
  $cities=str_replace(",","','",$cities);
  if($nai_ids!='' && $p_ids!='')
  {
      if($cities=='all')
      {
          $sql="SELECT A.id, A.account_name, city FROM prospects A, naics_codes B WHERE (A.naics_code=B.id AND B.naics_code IN ($nai_ids)) OR (A.id IN ($p_ids) AND A.naics_code=B.id)";
      } else {
          $sql="SELECT A.id, A.account_name, city FROM prospects A, naics_codes B WHERE (A.naics_code=B.id AND B.naics_code IN ($nai_ids) AND city IN('$cities')) OR (A.id IN ($p_ids) AND A.naics_code=B.id)";
      }
  } elseif($nai_ids!='')
  {
    if($cities=='all')
      {
        $sql="SELECT A.id, A.account_name, city FROM prospects A, naics_codes B WHERE A.naics_code=B.id AND B.naics_code IN ($nai_ids)";
      } else {
         $sql="SELECT A.id, A.account_name, city FROM prospects A, naics_codes B WHERE A.naics_code=B.id AND B.naics_code IN ($nai_ids) AND city IN('$cities')"; 
      }
  } elseif($p_ids!='')
  {
    $sql="SELECT id, account_name, city FROM prospects WHERE id IN ($p_ids)";
  } elseif($cities!='')
  {
    $sql="SELECT id, account_name, city FROM prospects WHERE city IN ('$cities')";
  } else {
    $sql="SELECT * FROM prospects WHERE id=0";
  }
  $data=array();
  
  
  
  $dbProspects=dbselectmulti($sql);
  
  $count=$dbProspects['numrows'];
  $save=array();
  $touches=array();
  $dt=date("Y-m-d H:i:s");
  if($count>0)
  {
      foreach($dbProspects['data'] as $p)
      {
          $data[]=$p['account_name']." - ".$p['city'];
          $saves[]="($campaign_id,$p[id],0)";
          $touches[]="($campaign_id,$p[id],0,'$dt','$dt',1,'Newly generated from campaign settings')";
      }
  } else {
      $data[]='No matching prospects';
  }
  
  
  if($_POST['saving']=='true')
  {
      //save the settings
      $nai_ids=addslashes($nai_ids);
      $cities=addslashes($cities);
      $p_ids=addslashes($p_ids);
      
      $savesql="UPDATE prospect_campaigns SET cat_list='$nai_ids', prospect_list='$p_ids', city_list='$_POST[cities]' WHERE id=$campaign_id";
      
      if(count($saves)>0)
      {
        //clear existing
        $clear="DELETE FROM prospect_campaign_xref WHERE campaign_id=$campaignid";
        $dbClear=dbexecutequery($sql);
        
        //add new prospects to this campaign  
        $insertsql="INSERT INTO prospect_campaign_xref (campaign_id, prospect_id, user_id) VALUES ".implode(",",$saves);
        $dbInsert=dbinsertquery($insertsql);
        
        //set initial touch
        $touchsql="INSERT INTO prospect_touches (campaign_id, prospect_id, user_id, action_datetime, status, notes) VALUES ".implode(",",$touches);
        $dbInsert=dbinsertquery($touchsql);
           
      }
      
      $dbUpdate=dbexecutequery($savesql);
      $saved=true;
  } 
  
  $return = array('count'=>$count,'names'=>$data, 'sql'=>$sql, 'saved'=>$saved,'save_sql'=>$savesql,'touch_sql'=>$touchsql,'insert_sql'=>$insertsql);
  
  echo json_encode($return);
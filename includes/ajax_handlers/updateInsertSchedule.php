<?php
  include("bootstrapAjax.php");
  
  global $pubs;
  $insertID=intval($_POST['insert_id']);
  $scheduleID=intval($_POST['schedule_id']);
  $pub_id=intval($_POST['pub_id']);
  $pressrun_id=intval($_POST['pressrun_id']);
  $insert_date=addslashes($_POST['insert_date']);
  $insert_quantity=intval($_POST['insert_count']);
  $zonesNTrucks = $_POST['zones'];
  
  $json = array();
  
  //create / update the insert schedule
  if($scheduleID == 0)
  {
      //new one
      $sql = "INSERT INTO inserts_schedule (insert_id, pub_id, pressrun_id, run_id, insert_quantity, insert_date, zoning, killed) VALUES
      ('$insertID', '$pub_id', '$pressrun_id', '0', '$insert_quantity', '$insert_date', '', 0)";
      $dbInsert=dbinsertquery($sql);
      if($dbInsert['error']=='')
      {
           $scheduleID = $dbInsert['insertid'];
           //lets build the table row that will be added
            if($pressrun_id!=0)
            {
                $sql = "SELECT run_name FROM publications_runs WHERE id=$pressrun_id";
                $dbPressRun = dbselectsingle($sql);
                $pressrun=stripslashes($dbPressRun['data']['run_name']);
            } else {
                $pressrun = '';
            }
            
            $sql="SELECT SUM(zone_count) as needed FROM insert_zoning WHERE insert_id=$insertID";
            $dbCount=dbselectsingle($sql);
            if($dbCount['error']==''){
                $insertNeeded=$dbCount['data']['needed'];
                if($insertNeeded==''){$insertNeeded=0;}
            }else{
                $insertNeeded=0;
            }
     
            $newRow="<tr id='sched-$scheduleID'><td>".$pubs[$pub_id]."</td><td>$pressrun</td><td>".date("m/d/Y",strtotime($insert_date))."</td><td>$insert_quantity</td><td>".$insertNeeded."</td><td><a href='?action=edit&subaction=edit&tab=schedules&insertid=$insertID&schedid=$scheduleID'><i class='fa fa-pencil'></i></a>&nbsp;&nbsp;&nbsp;<a href='?action=edit&subaction=delete&tab=schedules&insertid=$insertID&schedid=$scheduleID' class='delete'><i class='fa fa-trash'></i></a></td></tr>";
            $json['newRow']=$newRow;
      } else {
          $scheduleID = 0;
          $json['status']='error';
          $json['message']=$dbInsert['error'];
          $json['newRow']='';
      }
  } else {
      //updating an existing schedule
      $sql="UPDATE inserts_schedule SET pub_id='$pub_id', pressrun_id='$pressrun_id', run_id='0', insert_quantity='$insert_quantity', insert_date = '$insert_date', killed=0 WHERE id=$scheduleID";
      $dbUpdate = dbexecutequery($sql);
       //lets build the table row that will be added
        if($pressrun_id!=0)
        {
            $sql = "SELECT run_name FROM publications_runs WHERE id=$pressrun_id";
            $dbPressRun = dbselectsingle($sql);
            $pressrun=stripslashes($dbPressRun['data']['run_name']);
        } else {
            $pressrun = '';
        }
        
        $sql="SELECT SUM(zone_count) as needed FROM insert_zoning WHERE insert_id=$insertID";
        $dbCount=dbselectsingle($sql);
        if($dbCount['error']==''){
            $insertNeeded=$dbCount['data']['needed'];
            if($insertNeeded==''){$insertNeeded=0;}
        }else{
            $insertNeeded=0;
        }

        $newRow="<tr id='sched-$scheduleID'><td>".$pubs[$pub_id]."</td><td>$pressrun</td><td>".date("m/d/Y",strtotime($insert_date))."</td><td>$insert_quantity</td><td>".$insertNeeded."</td><td><a href='?action=edit&subaction=edit&tab=schedules&insertid=$insertID&schedid=$scheduleID'><i class='fa fa-pencil'></i></a>&nbsp;&nbsp;&nbsp;<a href='?action=edit&subaction=delete&tab=schedules&insertid=$insertID&schedid=$scheduleID' class='delete'><i class='fa fa-trash'></i></a></td></tr>";
        $json['newRow']=$newRow;
        $json['schedule_id']=$scheduleID;
  }
  
  $json['schedule_id']=$scheduleID;
  
  if($scheduleID != 0) {
      
      $error='';
      //clear all current zones
      $sql="DELETE FROM inserts_zoning WHERE schedule_id=$scheduleID AND insert_id=$insertID";
      $dbDelete=dbexecutequery($sql);
      
      //clear all current trucks
      $sql="DELETE FROM inserts_zoning_trucks WHERE schedule_id=$scheduleID AND insert_id=$insertID";
      $dbDelete=dbexecutequery($sql);
      
      //create new zoning records
      //zones are being passed in like zone_122=on&check_124=on&truck_122_1=on
      $zonesNTrucks = str_replace("=on","",$zonesNTrucks); //eliminate all the "=on" parts
      $json['zonesNTrucks']=$zonesNTrucks;
      $zonesNTrucks = explode("&",$zonesNTrucks);
      if(count($zonesNTrucks)>0)
      {
          foreach($zonesNTrucks as $zt)
          {
              if(substr($zt,0,5)=='zone_')
              {
                  $zones[] = str_replace('zone_','',$zt);
              } elseif(substr($zt,0,6)=='truck_')
              {
                  $trucks[] = str_replace('truck_','',$zt);
              }
          }
          
          //now we have an array of IDs
          $inserts=array();
          if(count($zones)>0)
          {
              foreach($zones as $zone)
              {
                  $inserts[]="($scheduleID,$insertID,$zone)";
              }
          }
          if(count($inserts)>0)
          {
              $sql="INSERT INTO inserts_zoning (schedule_id, insert_id, zone_id) VALUES ".implode(",",$inserts);
              $dbInsert=dbinsertquery($sql);
              $error.=$dbInsert['error'];
          }
          
          //now we have an array of IDs
          $inserts=array();
          if(count($trucks)>0)
          {
              foreach($trucks as $truck)
              {
                  $truck=explode("_",$truck);
                  //zone id is part 0, truck id is part 1;
                  $inserts[]="($scheduleID,$insertID,$truck[0],$truck[1])";
              }
          }
          if(count($inserts)>0)
          {
              $sql="INSERT INTO inserts_zoning_trucks (schedule_id, insert_id, zone_id, truck_id) VALUES ".implode(",",$inserts);
              $dbInsert=dbinsertquery($sql);
              $error.=$dbInsert['error'];
          }
      }
      $json['zones']=$zones;
      $json['trucks']=$trucks;
      
      $json['message']=$error;
      
  }
 
  if($error=='') {
    $json['status']='success';
  } else {
    $json['status']='error';
  }
  print json_encode($json);  
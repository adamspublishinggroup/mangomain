<?php
  //generates a block of checkboxes for zonings
include("bootstrapAjax.php");

  $insertid=intval($_POST['insertid']);
  $schedid=intval($_POST['schedid']);
  
  $sql="SELECT A.* FROM publications_insertzones A, inserts_schedule B WHERE A.pub_id=B.pub_id AND B.id=$schedid";
  $dbZones=dbselectmulti($sql);
  if($dbZones['numrows']>0)
  {
      //we are going to format the zones in two columns to conserve space
      foreach($dbZones['data'] as $zone)
      {
          //see if this one is checked
          if($insertid!='')
          {
              $sql="SELECT * FROM insert_zoning WHERE sched_id=$schedid AND zone_id=$zone[id]";
              $dbCheck=dbselectsingle($sql);
              if($dbCheck['numrows']>0){$checked='checked';$total+=$zone['zone_count'];}else{$checked='';}
          }
          $zones[]="<input rel='$zone[zone_count]' type=checkbox id='check_$zone[id]' name='check_$zone[id]' $checked/> $zone[zone_name]<br />";
          
      }
  }
  
  $json['zones']=$zones;
  $json['status']='success';
  
  echo json_encode($json);

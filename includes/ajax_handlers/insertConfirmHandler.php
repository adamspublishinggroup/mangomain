<?php
//this script is designed to handle ajax calls for the inserts.php script
include("bootstrapAjax.php");

  if($_GET)
  {
      confirm();
  } else {
      confirm_insert();
  }
  
  function confirm()
  {
        $insertid=intval($_GET['insertid']);
        $sql="SELECT A.*, B.account_name FROM inserts A, accounts B WHERE A.id=$insertid AND A.advertiser_id=B.id";
        $dbInsert=dbselectsingle($sql);
        $insert=$dbInsert['data'];
        $pubid=$insert['pub_id'];
        $advertisername=stripslashes($insert['account_name']);
        $inserttagline=stripslashes($insert['insert_tagline']);
        $receivedate=stripslashes($insert['receive_date']);
        $notes=stripslashes($insert['insert_notes']);
        if ($insert['confirm_by']!='' &&$insert['confirm_by']!=0)
        {
            $confirmby=$insert['confirm_by'];
        } else {
            $confirmby=$_SESSION['userid'];
        }
        $users=array();
        $users[0]="Please select";
        $sql="SELECT * FROM users ORDER BY firstname, lastname";
        $dbUser=dbselectmulti($sql);
        if($dbUser['numrows']>0)
        {
            foreach($dbUser['data'] as $user)
            {
                $users[$user['id']]=stripslashes($user['firstname']).' '.stripslashes($user['lastname']);
            }
        }
        print "<form id='ajaxInsertForm' class='form-horizontal' name='ajaxInsertForm' action='includes/ajax_handlers/insertActions.php' method=post>\n";
        print "<div class='label'>&nbsp;</div><div class='input'>\n";
        if ($_GET['error'])
        {
            print "<span style='color:red;font-weight:bold;'>Please check the box and fill out all information</span><br />\n";    
        }
        
        print "You are confirming an insert with the following details:<br />\n";
        print "Advertiser: $advertisername<br />\n";
        print "Tagline: $inserttagline<br />\n";
        //lets see if it has been scheduled
        $sql="SELECT A.pub_name, B.insert_date FROM publications A, inserts_schedule B WHERE B.insert_id=$insertid AND B.pub_id=A.id";
        $dbSchedule=dbselectmulti($sql);
        if($dbSchedule['numrows']>0)
        {
            print "<div style='float:left;'><b>Scheduled in:</b></div><div style='float:left;margin-left:4px;'>";
            foreach($dbSchedule['data'] as $schedule)
            {
                print stripslashes($schedule['pub_name']).' on '.date("m/d/Y",strtotime($schedule['insert_date']))."<br>";
            }
            print "</div><div style='clear:both;'></div>\n";
        }
        print "<br />";
        print "</div><div class='clear'></div>\n";
        make_checkbox('confirm',1,'Confirm','Confirm insert booking');
        make_select('confirm_by',$users[$confirmby],$users,'Confirmed by');
        make_textarea('notes','','Notes','',40,10,false);
        print "<input type='hidden' name='insertid' value='$insertid'>\n";
        print "<input type='hidden' name='action' value='saveconfirm'>\n";
        print "</form>\n";
  }
  
  
  
function confirm_insert()
{
    $insertid=$_POST['insertid'];
    if ($_POST['confirm'])
    {
        $by=addslashes($_POST['confirm_by']);
        $notes=addslashes($_POST['notes']);
        $confirmdate=addslashes(date("Y-m-d H:i:s"));
        $sql="UPDATE inserts SET confirmed=1, confirm_by='$by', 
        confirm_datetime='$confirmdate', insert_notes='$notes' WHERE id=$insertid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
        if ($error!='')
        {
            print $error;
        } else {
            //redirect("?action=list");
        }
    }
    print_r($_POST);
}

dbclose();
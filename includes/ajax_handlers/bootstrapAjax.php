<?php
/* this file is a bootstrapper for all ajax calls */

session_start();

function siteURL()
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $domainName = $_SERVER['HTTP_HOST'].'/';
    return $protocol.$domainName;
}
define( 'SITE_URL', siteURL() );

define("ABS_PATH", $_SERVER['DOCUMENT_ROOT']);
/* load the site config.json file */
if(!file_exists(ABS_PATH.'/includes/config.json'))
{
    die("Config file not present. Aborting bootstrap");
} else {
    $baseConfig = json_decode(file_get_contents(ABS_PATH.'/includes/config.json'),true);
    $baseConfig = $baseConfig['config'];
    define( 'DB_HOST', $baseConfig['database']['host'] ); // set database host
    define( 'DB_USER', $baseConfig['database']['username'] ); // set database user
    define( 'DB_PASS', $baseConfig['database']['password'] ); // set database password
    define( 'DB_NAME', $baseConfig['database']['database'] ); // set database name
    define( 'SEND_ERRORS_TO', $baseConfig['send_errors_to'] ); // set database name
    define( 'MODE', $baseConfig['mode'] ); // set database name
    define( 'SITE_ID', $baseConfig['site_id'] ); // set database name  
}                          
if (version_compare(phpversion(), '7.0.0', '<')) {
    require (ABS_PATH.'/includes/functions_db_php5.php');
} else {
    require (ABS_PATH.'/includes/functions_db.php');
}
require (ABS_PATH.'/includes/config.php');

require (ABS_PATH.'/includes/functions_formtools.php');
require (ABS_PATH.'/includes/functions_graphics.php');
require (ABS_PATH.'/includes/mail/htmlMimeMail.php');

require (ABS_PATH.'/includes/functions_common.php');
require (ABS_PATH.'/includes/functions_user.php');
require (ABS_PATH.'/includes/functions_maintenance.php');
require (ABS_PATH.'/includes/functions_packaging.php');
require (ABS_PATH.'/includes/functions_press.php');

/* load classes */
require (ABS_PATH.'/includes/classes/Prefs.php');
require (ABS_PATH.'/includes/classes/User.php');

if($GLOBALS['maintenanceMode']==1 && $_SERVER['SCRIPT_NAME']!='/corePreferences.php'){redirect("maintenanceMode.php");}
if($GLOBALS['debug']){error_reporting (E_ALL ^ E_NOTICE);}else{$GLOBALS['debug']=0;error_reporting(0);}


$Prefs = Prefs::Instance(SITE_ID);
$User = User::Instance($_SESSION['userid']);  
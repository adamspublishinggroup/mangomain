<?php
include("bootstrapAjax.php");

$table=$_POST['table'];
$json = array();
$dbFields=dbgetfields($table);
if($dbFields['numrows']>0)
{
  foreach($dbFields['fields'] as $field)
  {
      $json[] = array('id'=> $field['Field'],"label" =>$field['Field']);
  } 
} else {
    $json=array('id'=>0,'label'=>'No fields found for '.$table);
} 
echo json_encode($json);
<?php
  include("bootstrapAjax.php");
  
  
  $insertID=intval($_POST['insert_id']);
  $scheduleID=intval($_POST['schedule_id']);
  $zonesNTrucks = $_POST['zones'];
  
  //clear all current zones
  $sql="DELETE FROM inserts_zoning WHERE schedule_id=$scheduleID AND insert_id=$insertID";
  $dbDelete=dbexecutequery($sql);
  
  //clear all current trucks
  $sql="DELETE FROM inserts_zoning_trucks WHERE schedule_id=$scheduleID AND insert_id=$insertID";
  $dbDelete=dbexecutequery($sql);
  
  
  //create new zoning records
  //zones are being passed in like zone_122=on&check_124=on&truck_122_1=on
  $zonesNTrucks = str_replace("=on","",$zonesNTrucks); //eliminate all the "=on" parts
  $zonesNTrucks = explode("&",$zonesNTrucks);
  
  foreach($zonesNTrucks as $zt)
  {
      if(substr($zt,0,5)=='zone_')
      {
          $zones[] = str_replace('zone_','',$zt);
      } elseif(substr($zt,0,6)=='truck_')
      {
          $trucks[] = str_replace('truck_','',$zt);
      }
  }
  //now we have an array of IDs
  $inserts=array();
  if(count($zones)>0)
  {
      foreach($zones as $zone)
      {
          $inserts[]="($scheduleID,$insertID,$zone)";
      }
  }
  if(count($inserts)>0)
  {
      $sql="INSERT INTO inserts_zoning (schedule_id, insert_id, zone_id) VALUES ".implode(",",$inserts);
      $dbInsert=dbinsertquery($sql);
  }
  
  //now we have an array of IDs
  $inserts=array();
  if(count($trucks)>0)
  {
      foreach($trucks as $truck)
      {
          $truck=explode("_",$truck);
          //zone id is part 0, truck id is part 1;
          $inserts[]="($scheduleID,$insertID,$truck[0],$truck[1])";
      }
  }
  if(count($inserts)>0)
  {
      $sql="INSERT INTO inserts_zoning_trucks (schedule_id, insert_id, zone_id, truck_id) VALUES ".implode(",",$inserts);
      $dbInsert=dbinsertquery($sql);
  }
  
  
  $data['zones']=$zones;
  $data['trucks']=$trucks;
  $data['error']=$dbUpdate['error'];
  $data['sql']=$sql;
  if($dbUpdate['error']=='')
    $data['status']='success';
  else 
    $data['status']='error';
  
  
  print json_encode($data);

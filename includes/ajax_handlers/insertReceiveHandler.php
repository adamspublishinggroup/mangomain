<?php
//this script is designed to handle ajax calls for the inserts.php script
include("bootstrapAjax.php");

  if($_POST)
  {
      receive();
  } else {
      confirm_insert();
  }
  
  function receive()
  {
        $shiptypes=array("pallet"=>"Pallet","boxes"=>"Boxes");
        $insertid=intval($_GET['insertid']);
        $sql="SELECT A.*, B.account_name FROM inserts A, accounts B WHERE A.id=$insertid AND A.advertiser_id=B.id";
        $dbInsert=dbselectsingle($sql);
        $insert=$dbInsert['data'];
        $advertisername=stripslashes($insert['account_name']);
        $inserttagline=stripslashes($insert['insert_tagline']);
        $receiveDate=stripslashes($insert['receive_date']);
        $shipper=stripslashes($insert['shipper']);
        $printer=stripslashes($insert['printer']);
        $control=stripslashes($insert['control_number']);
        if ($insert['receive_by']!='')
        {
            $receiveby=$insert['receive_by'];
        } else {
            $receiveby=$_SESSION['userid'];
        }
        $receiveCount=stripslashes($insert['receive_count']);
        $receiveWeight=stripslashes($insert['receive_weight']);
        $damage=stripslashes($insert['damage']);
        $insertDamage=stripslashes($insert['insert_damage']);
        $shipType=$insert['ship_type'];
        $pubid=$insert['pub_id'];
        $shipQuantity=$insert['ship_quantity'];
        $tagColor=$insert['tag_color'];
        $storageLocation=$insert['storage_location'];
        $users=array();
        $users[0]="Please select";
        $sql="SELECT * FROM users ORDER BY firstname, lastname";
        $dbUser=dbselectmulti($sql);
        if($dbUser['numrows']>0)
        {
            foreach($dbUser['data'] as $user)
            {
                $users[$user['id']]=stripslashes($user['firstname']).' '.stripslashes($user['lastname']);
            }
        }
       
        print "<form id='ajaxInsertForm' name='ajaxInsertForm' action='includes/ajax_handlers/insertActions.php' method=post class='form-horizontal'>\n";
        print "<div class='label'>&nbsp;</div><div class='input'>\n";
        if ($_GET['error'])
        {
            print "<span style='color:red;font-weight:bold;'>Please check the box and fill out all information</span><br />\n";    
        }
        
        print "You are confirming receipt of an insert with the following details:<br />\n";
        print "<b>Advertiser:</b> $advertisername<br />\n";
        print "<b>Tagline:</b> $inserttagline<br />\n";
        
        //lets see if it has been scheduled
        $sql="SELECT A.pub_name, B.insert_date FROM publications A, inserts_schedule B WHERE B.insert_id=$insertid AND B.pub_id=A.id";
        $dbSchedule=dbselectmulti($sql);
        if($dbSchedule['numrows']>0)
        {
            print "<div style='float:left;'><b>Scheduled in:</b></div><div style='float:left;margin-left:4px;'>";
            foreach($dbSchedule['data'] as $schedule)
            {
                print stripslashes($schedule['pub_name']).' on '.date("m/d/Y",strtotime($schedule['insert_date']))."<br>";
            }
            print "</div><div style='clear:both;'></div>\n";
        }
        print "<br />";
        print "</div><div class='clear'></div>\n";
        make_text('control',$control,'Control #','Control number of received insert');
        make_checkbox('receive',1,'Receive', ' check to set to received');
        make_select('receivedby',$users[$receiveby],$users,'Received By');
        make_date('receiveDate',$receiveDate,'Date received');
        make_number('receiveCount',$receiveCount,'Receive Count','How many did we get?');
        make_select('shipType',$shiptypes[$shipType],$shiptypes,'Ship type','How did they arrive?');
        make_number('shipQuantity',$shipQuantity,'Ship quantity','How many pallets/boxes?');
        make_number('receiveWeight',$receiveWeight,'Receive Weight','Total weight?');
        make_text('tagColor',$tagColor,'Tag Color','Color of pallet tag');
        if ($GLOBALS['insertUseLocation'])
        {
            $slocations=buildLocations('insert');
            make_select('storageLocation',$slocations[$storageLocation],$slocations,'Storage Location','Storage location code');
        }
        make_text('shipper',$shipper,'Shipper','Who delivered it?');
        make_text('printer',$printer,'Printer','Who printed it?');
        print "<div class='label'>Damage</div><div class='input'>";
        print make_checkbox('damaged',$damage,'toggleInsertDamage();')."Check if the inserts arrived damaged";
        if ($damage){$ddisplay='block';}else{$ddisplay='none;';}
        print "<textarea id='insertDamage' name='insertDamage' cols='40' rows='8' style='display:$ddisplay;'>$insertDamage</textarea>\n";
        print "</div><div class='clear'></div>\n";
        print "<input type='hidden' name='insertid' value='$insertid'>\n";
        print "<input type='hidden' name='action' value='savereceive'>\n";
        print "</form>\n";
        
  }
  
function receive_insert()
{
    $insertid=$_POST['insertid'];
    if ($_POST['receive'])
    {
        
        $shipper=addslashes($_POST['shipper']);
        $printer=addslashes($_POST['printer']);
        $receiveCount=addslashes($_POST['receiveCount']);
        $receiveWeight=addslashes($_POST['receiveWeight']);
        if($receiveWeight==''){$receiveWeight='0.00';}
        $pieceWeight=addslashes($_POST['pieceWeight']);
        if($pieceWeight==''){$pieceWeight='0.00';}
        if($_POST['damaged']){$damaged=1;}else{$damaged=0;}
        $insertDamage=addslashes($_POST['insertDamage']);
        $shipType=addslashes($_POST['shipType']);
        $shipQuantity=addslashes($_POST['shipQuantity']);
        $tagColor=addslashes($_POST['tagColor']);
        $storageLocation=addslashes($_POST['storageLocation']);
        $control=addslashes($_POST['control']);
   
        
        
        $by=$_POST['receivedby'];
        $dt=date("Y-m-d H:i:s");
        $date=date("Y-m-d");
        
        if($control!='')
        {
            $sql="UPDATE inserts_received SET matched=1 WHERE control_number='$control'";
            $dbUpdate=dbexecutequery($sql);
        }
        $sql="UPDATE inserts SET control_number='$control', received=1, receive_by='$by', receive_datetime='$dt', receive_date='$date', shipper='$shipper', printer='$printer', receive_count='$receiveCount', receive_weight='$receiveWeight', piece_weight='$pieceWeight', damage='$damaged', insert_damage='$insertDamage', ship_type='$shipType', ship_quantity='$shipQuantity', tag_color='$tagColor', storage_location='$storageLocation' WHERE id=$insertid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
        if ($error!='')
        {
            print $error;
        } else {
            //redirect("?action=list");
        }
    }
}

dbclose();
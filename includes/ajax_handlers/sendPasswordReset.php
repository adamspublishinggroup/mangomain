<?php
  include("bootstrapAjax.php");
  
  /*
  This feature sends an email to the specified email address with a token to reset a password
  */
  
  $email=addslashes($_POST['email']);
  
  $sql="SELECT * FROM users WHERE email='$email'";
  $dbUser = dbselectsingle($sql);
  
  if($dbUser['numrows']>0)
  {
      $user = $dbUser['data'];
      
      $token = $user['token'];
      
      $message = "You submitted a request to reset your password. If you did not request this reset, please let the system administrators know, and you can then ignore this email. Otherwise, click the link below to reset your password.<br>
      <br>
      <a href='".SITE_URL."login.php?a=reset&t=$token'>Click link to recover.</a><br>
      <br>
      If the link doesn't work, please copy and paste the following into your address bar:<br>
      <br>
      ".SITE_URL."login.php?a=reset&t=$token<br>
      <br>
      Thank you,<br>
      <br>
      The Mango Team
      ";  
      
      $response = send_email( $email, "You requested to reset your Mango password.", $message );

     if( $response['status']==1)
      {
          $json['message']="You should receive an email shortly with the recovery information. Don't forget to check your clutter or spam folders if you don't see it in a few minutes.";
      } else {
          $json['message']='There was a problem sending the recovery email.';
      }
  } else {
      $json['message']='Unable to find that email';
  }
  
  echo json_encode($json);
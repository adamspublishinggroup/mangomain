<?php
include("bootstrapAjax.php");
global $pubs;
/*
*   ALL FUNCTIONS REFERENCES IN THE SWITCH LOOP ARE IN THE functions_common.php FILE
*/
$response=array();
$response['process_attachments']=0;
switch($_POST['action'])
{
    case 'saveSettings':
        $planid=$_POST['planid'];
        $packid=$_POST['packid'];
        $packdate=$_POST['packdate'];
        $packname=$_POST['packname'];
        $request=$_POST['request'];
        $orequest=$_POST['orequest'];
        $stickyid=$_POST['stickyid'];
        if($stickyid==''){$stickyid=0;}
        if($request==''){$request=0;}
        $pdate=date("Y-m-d",strtotime($packdate));
       
        $sql="SELECT * FROM inserters WHERE id=$inserterid";
        $dbInserter=dbselectsingle($sql);
        $inserter=$dbInserter['data'];
        $candoubleout=$inserter['can_double_out'];
        $inserterturn=$inserter['inserter_turn'];
        $singleoutspeed=$inserter['single_out_speed'];
        $doubleoutspeed=$inserter['double_out_speed'];
        $singleout=false;

        if($doubleout)
        {
           $speed=$doubleoutspeed; 
        } else {
           $speed=$singleoutspeed; 
        }
        if($speed>0)
        {
            $runminutes=round(($request/$speed),0)+30; //pad by 30 because... :)
        } else {
            $runminutes=120;
        }
        
        $packstop=date("Y-m-d H:i",strtotime($packdate."+$runminutes minutes"));
       if($inserterid!=$oinserterid){$doubleout=0;}
       //first, update the record
       $sql="UPDATE jobs_inserter_packages SET sticky_note_id='$stickyid',inserter_request='$request', package_name='$packname', 
       package_date='$pdate', package_startdatetime='$packdate', package_stopdatetime='$packstop' WHERE id='$packid'";
       $dbUpdate=dbexecutequery($sql);
       $error=$dbUpdate['error'];
       if($error!='')
       {
           $response['status']='error';
           $response['error_message']=$error;
       } else {
           //now if the inserter id has changed, that means we need to generate a new inserter set
           $response['status']='success';
            
        }
        
        //now, lets see if this package is saved in another package. If so we will need to find out the package id and station id
        //we need this so we can update the name and info for that display
        $sql="SELECT * FROM jobs_packages_inserts WHERE plan_id=$planid AND insert_id='$packid' AND insert_type='package'";
        $dbCheck=dbselectsingle($sql);
        if($dbCheck['numrows']>0)
        {
            $response['update_package_id']=$dbCheck['data']['package_id'];
            $response['update_station_id']=$dbCheck['data']['hopper_id'];
        }
         
    break;
     
    
    case 'saveSticky':
        $planid=$_POST['planid']; 
        $packid=$_POST['packid'];
        $insertid=$_POST['insertid']; 
        $sql="UPDATE jobs_inserter_packages SET sticky_note_id=$insertid WHERE id=$packid";
        $dbUpdate=dbexecutequery($sql);
        if($dbUpdate['error']==00)
        {
            $response['status']='success';
        } else {
            $response['status']='error';
        }
    break;
    
    
    case 'deletePackage':
        //just need to unassign any existing packages
        //now we need to remove all inserts that were on this package, and get updated package totals
        
        //please note that this code is also used in inserterPackages.php for the delete package function
        //any updates need to be mirrored between the two
        
        $planid=$_POST['planid'];
        $packid=$_POST['packid'];
        $sql="SELECT * FROM jobs_packages_inserts WHERE plan_id=$planid AND package_id=$packid";
        $dbInserts=dbselectmulti($sql);
        $response['status']='success';
        $response['inserts_removed']=0;
        if($dbInserts['numrows']>0)
        {
            foreach($dbInserts['data'] as $insert)
            {
                $temp=removeInsert($insert['plan_id'],$insert['package_id'],$insert['insert_id'],$insert['insert_type'],$insert['hopper_id']);
                $response['secondary_id']=$temp['secondary_id'];
                $response['secondary_weight']=$temp['secondary_weight'];
                $response['secondary_count']=$temp['secondary_count'];
                $response['secondary_pages']=$temp['secondary_pages'];
                
                
                
                $sql="SELECT A.*, B.account_name FROM inserts A, account B WHERE A.id=$insert[insert_id] AND A.advertiser_id=B.id";
                $dbInsertInfo=dbselectsingle($sql);
                $insertInfo=$dbInsertInfo['data'];
                $insertname=stripslashes($insertInfo['account_name'])." ".stripslashes($insertInfo['insert_tagline']);
                $insertname=str_replace("'","",$insertname);
                $insertname="<b>".$insertname."</b>";
                $insertpages=$insertInfo['standard_pages'];
                $request=$insertInfo['insert_quantity'];
                $insertinfo=$insertname."<br><b>Pages: </b>".$insertpages." <b>Request: </b>$request";
                $response['inserts_removed']=$response['inserts_removed']+1;
                $response['inserts'][]=array('insert_id'=>$insert['insert_id'],'insert_name'=>$insertname,'insert_type'=>$insert['insert_type'],'insert_text'=>$insertinfo);
                $deleteids=$insert['id'].",";
            }
        }
        $deleteids=substr($deleteids,0,strlen($deleteids)-1);
        if($deleteids!='')
        {
            $sql="DELETE FROM jobs_packages_inserts WHERE id IN ($deleteids)";
            $dbDelete=dbexecutequery($sql);
        }
        //subtract one from the number of package in the jobs_inserter_plans
        $sql="UPDATE jobs_inserter_plans SET num_packages=num_packages-1 WHERE id=$planid";
        $dbUpdate=dbexecutequery($sql);
        if($dbUpdate['error']!='')
        {
            $response['status']='error';
            $response['error_message'].=$dbUpdate['error'];
        }
        //delete the actual package
        $sql="DELETE FROM jobs_inserter_packages WHERE id=$packid";
        $dbDelete=dbexecutequery($sql);
        if($dbDelete['error']!='')
        {
            $response['status']='error';
            $response['error_message'].=$dbDelete['error'];
        }
        
    break;
    
    case 'insertDetails':
        $objectID=$_POST['id'];
        $planid=$_POST['planid'];
        $sql="SELECT pub_date FROM jobs_inserter_plans WHERE id=$planid";
        $dbPubDate = dbselectsingle($sql);
        $pubdate = $dbPubDate['data']['pub_date'];
        //see if it's a station. a station will contain a '-' in the middle
        $noinsert=false;
        if(strpos($objectID,'-'))
        {
            //station
            $objectID=explode('-',$objectID);
            $package=explode("_",$objectID[0]);
            $packageid=$package[1];
            $station=explode("_",$objectID[1]);
            $stationid=$station[1];
            $type='Station';
            $id='Package: '.$packageid.' station: '.$stationid;
            
            //going to have to look up the insert in this station
            $sql="SELECT * FROM jobs_packages_inserts WHERE package_id='$packageid' AND hopper_id='$stationid'";
            $dbCheck=dbselectsingle($sql);
            if($dbCheck['numrows']>0)
            {
                $insertid=$dbCheck['data']['insert_id'];
                $type=$dbCheck['data']['insert_type'];
            } else {
                $noinsert=true;
            }
            
        } else {
            //package or insert
            $objectID=explode("_",$objectID);
            $insertid=$objectID[1];
            if($objectID['0']=='package')
            {
                //package
                $type='package';
            } else {
                //insert
                $type='insert';
            }
        }
        
        if($noinsert)
        {
            $qtip='No insert.';
        } else {
            $qtip="<div style='width:250px;font-size:12px;'>\n";
            if($type=='package')
            {
                //generate a list of all inserts in the package
                $sql="SELECT package_name FROM jobs_inserter_packages WHERE id=$insertid";
                $dbPackage=dbselectsingle($sql);
                $sql="SELECT * FROM jobs_packages_inserts WHERE package_id=$insertid ORDER BY insert_type";
                $dbCheck=dbselectmulti($sql);
                if($dbCheck['numrows']>0)
                {
                   $qtip.=getPackageContents($qtip,$insertid);  
                } else {
                   $qtip.="<br><b>Package: </b>".$dbPackage['data']['package_name'];
                   $qtip.=" contains no inserts<br>"; 
                }
            } elseif($type=='insert')
            {
                $qtip.=getInsertToolTip($insertid,$pubdate,$planid);
            }
        
        
            $qtip.="</div>\n";
        }
        
        $response['status']='success';
        $response['qtip']=$qtip;
    
    break;
    
}
echo json_encode($response);   
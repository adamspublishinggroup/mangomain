<?php
  //generates a block of checkboxes for zonings
include("bootstrapAjax.php");

  
  if($_POST['type']=='new'){
    $pubID = intval($_POST['pub_id']);
    $sql="SELECT * FROM publications_insertzones WHERE pub_id = $pubID ORDER BY zone_order ASC";
  } else {
    $insertid=intval($_POST['insert_id']);
    $schedid=intval($_POST['sched_id']);
    
    //look up the schedule to find the day of week
    $sql="SELECT * FROM inserts_schedule WHERE id=$schedid";
    $dbSchedule=dbselectsingle($sql);
    $dow = date('w',strtotime($dbSchedule['data']['insert_date']));
    
    $sql="SELECT A.* FROM publications_insertzones A, inserts_schedule B WHERE A.pub_id=B.pub_id AND B.id=$schedid AND B.insert_id=$insertid AND dow_$dow>0 ORDER BY zone_order ASC";
  }
  
  $zones=array();
  $trucks=array();
  $dbZones=dbselectmulti($sql);
  if($dbZones['numrows']>0)
  {
      //we are going to format the zones in two columns to conserve space
      foreach($dbZones['data'] as $zone)
      {
          //see if this one is checked
          if($insertid!='')
          {
              $sql="SELECT * FROM inserts_zoning WHERE schedule_id=$schedid AND insert_id=$insertid AND zone_id=$zone[id]";
              $dbCheck=dbselectsingle($sql);
              if($dbCheck['numrows']>0){$checked='checked';}else{$checked='';}
          }
          
          $zones[]="<div class='checkbox'><label><input type=checkbox id='zone_$zone[id]' name='zone_$zone[id]' $checked onclick='toggleTruckChecks($zone[id]);'/> $zone[zone_name]</label></div>";
          $zoneBlocks[]=$zone['id'];
          //find any trucks
          $sql="SELECT * FROM publications_inserttrucks WHERE zone_id=$zone[id] ORDER BY truck_order ASC";
          $dbTrucks=dbselectmulti($sql);
          if($dbTrucks['numrows']>0)
          {
              foreach($dbTrucks['data'] as $truck)
              {
                  if($insertid!='')
                  {
                      $sql="SELECT * FROM inserts_zoning_trucks WHERE schedule_id=$schedid AND insert_id=$insertid AND zone_id=$zone[id] AND truck_id=$truck[id]";
                      $dbCheck=dbselectsingle($sql);
                      if($dbCheck['numrows']>0){$checked='checked';}else{$checked='';}
                  }
                  $trucks[$zone['id']][]="<div class='checkbox'><label><input class='zone_$zone[id]' type=checkbox id='truck_$zone[id]_$truck[id]' name='truck_$zone[id]_$truck[id]'  $checked/> $zone[zone_name] | $truck[truck_name]</label></div>";
                   
              }
          }
      }
      
  } else {
      $zones[]="<h4>No zones have been defined for this publication</h4>";
  }
  
  $json['zones']=$zones;
  $json['zone_blocks']=$zoneBlocks;
  $json['trucks']=$trucks;
  $json['status']='success';
  $json['insert_id']=$insertid;
  $json['sched_id']=$schedid;
  $json['sql']=$sql;
  echo json_encode($json);

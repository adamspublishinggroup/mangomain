<?php
  /*
  * THIS script is designed to handle generation of the page color setter for a specified layout
  */
  
  include("bootstrapAjax.php");
  
  //ok, for color we're going to provide a spread of color pages 
    //based on the pages & layout requested earlier
    //get the layout id
    $jobid=intval($_POST['jobid']);
    $sql="SELECT * FROM jobs WHERE id=$jobid";
    $dbJob=dbselectsingle($sql);
    $layoutid=$dbJob['data']['layout_id'];
    
    $fullcolors=array('k'=>"K",'c'=>"Full Color",'s'=>"K/Spot");
    $spotcolors=array('k'=>"K",'s'=>"K/Spot");
    
    $sql="SELECT * FROM jobs_sections WHERE job_id=$jobid";
    $dbJSections=dbselectsingle($sql);
    if ($dbJSections['numrows']>0)
    {
        $sections=$dbJSections['data'];
        $section1_name=$sections['section1_name'];
        $section1_code=$sections['section1_code'];
        $section1_lowpage=$sections['section1_lowpage'];
        $section1_highpage=$sections['section1_highpage'];
        $section1_doubletruck=$sections['section1_doubletruck'];
        $section1_producttype=$sections['section1_producttype'];
        $section1_leadtype=$sections['section1_leadtype'];
        $need_1=$sections['section1_used'];
        
        $section2_name=$sections['section2_name'];
        $section2_code=$sections['section2_code'];
        $section2_lowpage=$sections['section2_lowpage'];
        $section2_highpage=$sections['section2_highpage'];
        $section2_doubletruck=$sections['section2_doubletruck'];
        $section2_producttype=$sections['section2_producttype'];
        $section2_leadtype=$sections['section2_leadtype'];
        $need_2=$sections['section2_used'];
        
        $section3_name=$sections['section3_name'];
        $section3_code=$sections['section3_code'];
        $section3_lowpage=$sections['section3_lowpage'];
        $section3_highpage=$sections['section3_highpage'];
        $section3_doubletruck=$sections['section3_doubletruck'];
        $section3_producttype=$sections['section3_producttype'];
        $section3_leadtype=$sections['section3_leadtype'];
        $need_3=$sections['section3_used'];
        
        
        
        //ok, now lets find press layout that may match
        //this may take awhile if the number of press layouts grows large
        $layoutsql="SELECT * FROM layout WHERE id=$layoutid";
        $dbLayouts=dbselectsingle($layoutsql);
        if ($dbLayouts['numrows']>0)
        {
            //$colors=$GLOBALS['colorconfigs'];
            //if ($need_1){print "Need section 1<br>\n";}
            //if ($need_2){print "Need section 2<br>\n";}
            //if ($need_3){print "Need section 3<br>\n";}
            $name=$dbLayouts['data']['layout_name'];
            $notes=$dbLayouts['data']['layout_notes'];
            //ok grab sections corresponding to this layout
            print "<div style='border: 1px solid black;padding:2px;margin-bottom:10px;'>\n";
            print "<div style='margin-bottom:6px;padding:2px;background-color:black;color:white;font-weight:bold;font-size:14px;'>\n";
            print "Chosen layout: $name - $notes<br>";
            print "</div>\n";
            print "<button onclick='setAll(\"black\");'>Set all to B/W</button> ";
            print "<button onclick='setAll(\"color\");'>Set all to Color</button><br />";
            print "<form id='settingColor' method=post>\n";
            make_hidden('layout_id',$layoutid);
            make_hidden('job_id',$jobid);
            //lets build a list of pages for this layout and indicate color/bw
            //based on press configuration
            //do it 1 section at a time
            if ($need_1)
            {
                print "<div id='section1_$lid'>\n";
                print "<b>Section 1: $section1_name - $section1_code</b><br>\n";
                $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND section_code='$section1_code' ORDER BY page_number ASC";
                $dbPages=dbselectmulti($sql);
                if ($dbPages['numrows']>0)
                {
                    foreach($dbPages['data'] as $page)
                    {
                        print "<div style='width:80px;height:60px;float:left;border:thin solid black;padding:3px;'>\n";
                        print "<p style='margin:0;text-align:center;font-weight:bold;font-size:14px'>$page[page_number]<br>\n";
                        if ($page['possiblecolor']==1)
                        {
                            if($page['color']==1)
                            {
                                $pcolor='c';
                            }elseif($page['color']==0 && $page['spot']==1)
                            {
                                $pcolor='s';
                            } else {
                                $pcolor='k';
                            }
                            print make_select('pageid_'.$page['id'],$fullcolors[$pcolor],$fullcolors);
                        } elseif($page['possiblespot']==1) {
                            switch($page['spot'])
                            {
                                case 0:
                                $pcolor='k';
                                break;
                                case 1:
                                $pcolor='s';
                                break;
                            }
                            print make_select('pageid_'.$page['id'],$spotcolors[$pcolor],$spotcolors);
                        } else {
                            make_hidden('pageid_'.$page['id'],'0');
                            print 'k';
                        }
                        print "</p>\n";
                        print "</div>\n";
                    }
                    print "<div class='clear'></div>\n";
                } else {
                    print "No pages defined for this section.";
                }
                print "</div>\n";
            }
           if ($need_2)
            {
                print "<div id='section2_$lid' >\n";
                print "<b>Section 2: $section2_name - $section2_code</b><br>\n";
                $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND section_code='$section2_code' ORDER BY page_number ASC";
                //print "Layout select sql is $sql<br>";
                $dbPages=dbselectmulti($sql);
                if ($dbPages['numrows']>0)
                {
                    foreach($dbPages['data'] as $page)
                    {
                        print "<div style='width:80px;height:60px;float:left;border:thin solid black;padding:3px;'>\n";
                        print "<p style='margin:0;text-align:center;font-weight:bold;font-size:14px'>$page[page_number]<br>\n";
                        if ($page['possiblecolor']==1)
                        {
                            if($page['color']==1)
                            {
                                $pcolor='c';
                            }elseif($page['color']==0 && $page['spot']==1)
                            {
                                $pcolor='s';
                            } else {
                                $pcolor='k';
                            }
                            print make_select('pageid_'.$page['id'],$fullcolors[$pcolor],$fullcolors);
                        } elseif($page['possiblespot']==1) {
                            switch($page['spot'])
                            {
                                case 0:
                                $pcolor='k';
                                break;
                                case 1:
                                $pcolor='s';
                                break;
                            }
                            print make_select('pageid_'.$page['id'],$spotcolors[$pcolor],$spotcolors);
                        } else {
                            make_hidden('pageid_'.$page['id'],'0');
                            print 'k';
                        }
                        print "</p>\n";
                        print "</div>\n";
                    }
                    print "<div class='clear'></div>\n";
                } else {
                    print "No pages defined for this section.";
                }
                print "</div>\n";
            }
           if ($need_3)
            {
                print "<div id='section3_$lid'>\n";
                print "<b>Section 3: $section3_name - $section3_code</b><br>\n";
                $sql="SELECT * FROM job_pages WHERE job_id=$jobid AND section_code='$section3_code' ORDER BY page_number ASC";
                //print "Layout select sql is $sql<br>";
           
                $dbPages=dbselectmulti($sql);
                if ($dbPages['numrows']>0)
                {
                    foreach($dbPages['data'] as $page)
                    {
                        print "<div style='width:80px;height:60px;float:left;border:thin solid black;padding:3px;'>\n";
                        print "<p style='margin:0;text-align:center;font-weight:bold;font-size:14px'>$page[page_number]<br>\n";
                        if ($page['possiblecolor']==1)
                        {
                            if($page['color']==1)
                            {
                                $pcolor='c';
                            }elseif($page['color']==0 && $page['spot']==1)
                            {
                                $pcolor='s';
                            } else {
                                $pcolor='k';
                            }
                            print make_select('pageid_'.$page['id'],$fullcolors[$pcolor],$fullcolors);
                        } elseif($page['possiblespot']==1) {
                            switch($page['spot'])
                            {
                                case 0:
                                $pcolor='k';
                                break;
                                case 1:
                                $pcolor='s';
                                break;
                            }
                            print make_select('pageid_'.$page['id'],$spotcolors[$pcolor],$spotcolors);
                        } else {
                            make_hidden('pageid_'.$page['id'],'0');
                            print 'k';
                        }
                        print "</p>\n";
                        print "</div>\n";
                    }
                    print "<div class='clear'></div>\n";
                } else {
                    print "No pages defined for this section.";
                }
                print "</div>\n";
            }
            make_hidden('jobid',$jobid);
            make_submit('submit','Set Color','Select');
            print "</form>\n";
            
            print "</div>\n";
            ?>
            <script>
            function setAll(stat)
            {
                if(stat=='black')
                {
                    $('#settingColor select').val('k');
                }
                if(stat=='color')
                {
                    $('#settingColor select').val('c');
                }
            }
            </script>
            <?php
        } else {
            print "Sorry, you need to specify a press layout first.";
        }    
    } else {
        print "No sections have been set up for this job yet. <a href='?action=editjob&jobid=$_GET[jobid]'>Click this link to set up the sections and pages</a>\n";
    }
       

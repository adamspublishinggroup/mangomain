<?php
  include("bootstrapAjax.php");
  
  $json['response']='Your site access has been renewed';
  
  //check for messages
  $messages = array();
  $sql="SELECT * FROM user_messages WHERE user_id=".$User->id." AND id>".intval($_POST['current_id'])." ORDER BY sent_time ASC";
  $dbMessages = dbselectmulti($sql);
  if($dbMessages['numrows']>0)
  {
      foreach($dbMessages['data'] as $message)
      {
          $dt = date("m/d H:i",strtotime($message['sent_time']));
          
          if($message['origination_id']=='0')
          {
              $user="System";
          } else {
              $user=$GLOBALS['users'][$message['origination_id']];
          }
          $titleText = stripslashes($message['title']);
          $messageText = stripslashes($message['message']);
          $messages[]=array('title'=>$titleText, 'sender'=>$user, 'sent_time'=>$dt, 'message'=>$messageText, 'id'=>$message['id']);
      }
  }
  $json['messages']=$messages;
  $json['user_id']=$User->id; 
  
  echo json_encode($json);
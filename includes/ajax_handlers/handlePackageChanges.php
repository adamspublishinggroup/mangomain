<?php
  include("bootstrapAjax.php");
  if($_GET['mode']=='test')
  {
      error_reporting(E_ALL);
      print "Firing manually with:<pre>";
      print_r($_GET);
      print "</pre>\n";
      $action = $_GET['action'];
      $params = $_GET;
      die();
  } else {
      $action = addslashes($_POST['action']);
      $params = $_POST;
  }
  
  $json=array();
  
  switch ($action)
  {
      case "addInsertToStation":
        addInsertToStation($params);
      break;
      
      case "addPackageToStation":
        addPackageToStation($params);
      break;
      
      case "addJacketToStation":
        addJacketToStation($params);
      break;
      
      case "addStickyToPackage":
        addStickyToPackage($params);
      break;
      
      case "removeInsert":
        removeInsertFromPackage($params);
      break; 
      
      case "calcPackage":
        calcPackageTotals($params);
      break;  
      
      case 'saveSettings':
            $planid=intval($_POST['planid']);
            $packid=intval($_POST['packid']);
            $packdate=date("Y-m-d H:i",strtotime($_POST['packdate']));
            $packname=addslashes($_POST['packname']);
            $request=intval($_POST['request']);
            $orequest=intval($_POST['orequest']);
            $stickyid=intval($_POST['stickyid']);
            if($stickyid==''){$stickyid=0;}
            if($request==''){$request=0;}
            $pdate=date("Y-m-d",strtotime($packdate));

            $sql="SELECT * FROM inserters WHERE id=$inserterid";
            $dbInserter=dbselectsingle($sql);
            $inserter=$dbInserter['data'];
            $candoubleout=$inserter['can_double_out'];
            $inserterturn=$inserter['inserter_turn'];
            $singleoutspeed=$inserter['single_out_speed'];
            $doubleoutspeed=$inserter['double_out_speed'];
            $singleout=false;

            if($doubleout)
            {
               $speed=$doubleoutspeed; 
            } else {
               $speed=$singleoutspeed; 
            }
            if($speed>0)
            {
                $runminutes=round(($request/$speed),0)+30; //pad by 30 because... :)
            } else {
                $runminutes=120;
            }

            $packstop=date("Y-m-d H:i",strtotime($packdate."+$runminutes minutes"));
            if($inserterid!=$oinserterid){$doubleout=0;}
            //first, update the record
            $sql="UPDATE jobs_inserter_packages SET sticky_note_id='$stickyid',inserter_request='$request', package_name='$packname', package_date='$pdate', package_startdatetime='$packdate', package_stopdatetime='$packstop' WHERE id='$packid'";
            $dbUpdate=dbexecutequery($sql);
            $error=$dbUpdate['error'];
            if($error!='')
            {
               $json['status']='error';
               $json['error_message']=$error;
            } else {
               //now if the inserter id has changed, that means we need to generate a new inserter set
               $json['status']='success';
            }

            //now, lets see if this package is saved in another package. If so we will need to find out the package id and station id
            //we need this so we can update the name and info for that display
            $sql="SELECT * FROM jobs_packages_inserts WHERE plan_id=$planid AND insert_id='$packid' AND insert_type='package'";
            $dbCheck=dbselectsingle($sql);
            if($dbCheck['numrows']>0)
            {
                $json['update_package_id']=$dbCheck['data']['package_id'];
                $json['update_station_id']=$dbCheck['data']['hopper_id'];
            }
            
        break;
         
        case 'insertDetails':
            $objectID=intval($_POST['id']);
            $planid=intval($_POST['planid']);
            $sql="SELECT pub_date FROM jobs_inserter_plans WHERE id=$planid";
            $dbPubDate = dbselectsingle($sql);
            $pubdate = $dbPubDate['data']['pub_date'];
            //see if it's a station. a station will contain a '-' in the middle
            $noinsert=false;
            if(strpos($objectID,'-'))
            {
                //station
                $objectID=explode('-',$objectID);
                $package=explode("_",$objectID[0]);
                $packageid=$package[1];
                $station=explode("_",$objectID[1]);
                $stationid=$station[1];
                $type='Station';
                $id='Package: '.$packageid.' station: '.$stationid;
                
                //going to have to look up the insert in this station
                $sql="SELECT * FROM jobs_packages_inserts WHERE package_id='$packageid' AND hopper_id='$stationid'";
                $dbCheck=dbselectsingle($sql);
                if($dbCheck['numrows']>0)
                {
                    $insertid=$dbCheck['data']['insert_id'];
                    $type=$dbCheck['data']['insert_type'];
                } else {
                    $noinsert=true;
                }
                
            } else {
                //package or insert
                $objectID=explode("_",$objectID);
                $insertid=$objectID[1];
                if($objectID['0']=='package')
                {
                    //package
                    $type='package';
                } else {
                    //insert
                    $type='insert';
                }
            }
            
            if($noinsert)
            {
                $qtip='No insert.';
            } else {
                $qtip="<div style='width:250px;font-size:12px;'>\n";
                if($type=='package')
                {
                    //generate a list of all inserts in the package
                    $sql="SELECT package_name FROM jobs_inserter_packages WHERE id=$insertid";
                    $dbPackage=dbselectsingle($sql);
                    $sql="SELECT * FROM jobs_packages_inserts WHERE package_id=$insertid ORDER BY insert_type";
                    $dbCheck=dbselectmulti($sql);
                    if($dbCheck['numrows']>0)
                    {
                       $qtip.=getPackageContents($qtip,$insertid);  
                    } else {
                       $qtip.="<br><b>Package: </b>".$dbPackage['data']['package_name'];
                       $qtip.=" contains no inserts<br>"; 
                    }
                } elseif($type=='insert')
                {
                    $qtip.=getInsertToolTip($insertid,$pubdate,$planid);
                }
            
            
                $qtip.="</div>\n";
            }
            
            $json['status']='success';
            $json['qtip']=$qtip;   
        break;
        
        case 'saveSticky':
            $planid=$_POST['planid']; 
            $packid=$_POST['packid'];
            $insertid=$_POST['insertid']; 
            $sql="UPDATE jobs_inserter_packages SET sticky_note_id=$insertid WHERE id=$packid";
            $dbUpdate=dbexecutequery($sql);
            if($dbUpdate['error']==00)
            {
                $json['status']='success';
            } else {
                $json['status']='error';
            }
        break;
      
      default:
        print "Guess we got here?";
      break;
  }
  
  // output results as json
  echo json_encode($json);
  
  
  function addStickyToPackage($params)
  {
      global $json;
      
      $insertID = intval($params['insert_id']);
      $packageID = intval($params['pack_id']);
      $stationID = intval($params['station_id']);
      $planID = intval($params['plan_id']);
      
      $json['params']=$params;
      
      
      $sql="UPDATE jobs_inserter_packages SET sticky_note_id = $insertID WHERE id=$packageID";
      $dbUpdate=dbexecutequery($sql);
      if($dbUpdate['error']=='')
      {
          $json['status']='success';
      } else {
          $json['status']='error';
      }
  
      $sql="SELECT * FROM inserts WHERE id = $insertID";
      $json['insert_details_sql']=$sql;
      $dbInsertDetails = dbselectsingle($sql);
      $insert = $dbInsertDetails['data'];
      
      //look up the insert itself
      if($insert['weprint_id']>0)
      {
          $sql="SELECT A.pub_id, B.run_name FROM jobs A, publications_runs B WHERE A.id=$insert[weprint_id] AND A.run_id=B.id";
          $json['weprint_sql']=$sql;
          $dbJobs=dbselectsingle($sql);
          $accountname=stripslashes($dbJobs['data']['run_name']);
      } else {
          $sql="SELECT * FROM accounts WHERE id=$insert[advertiser_id]";
          $json['account_sql']=$sql;
          $dbAccount=dbselectsingle($sql);
          $accountname=stripslashes($dbAccount['data']['account_name']);
      }
      $insertname=htmlentities($accountname." ".stripslashes($insert['insert_tagline']));
      $insertname=str_replace("'","",$insertname);
      
      //need to find the schedule id for the insert, we have pubid and date
      
      
      $zones = "<span style='color: black;font-weight:bold;'>Zones: ";
      //get zones for this insert
      $sql="SELECT A.zone_label FROM publications_insertzones A, inserts_zoning B, inserts_schedule C, jobs_inserter_packages D 
WHERE A.id=B.zone_id AND B.schedule_id=C.id AND C.pub_id=D.pub_id AND C.insert_date=D.pub_date AND B.insert_id=$insert[id] AND D.id=$packageID";
      $dbZones = dbselectmulti($sql);
      if($dbZones['numrows']>0)
      {
          foreach($dbZones['data'] as $zone)
          {
              $zones.=" $zone[zone_label] |";
          }
      } 
      $zones.="</span>";
      
      
      
      $json['account_name']=$insertname.$zones;
      
  }
  
  /*
  *  Adds an insert to a station in the designated package/plan
  *  Receives the following $_POST variabes
  *  action: 'addInsertToStation',
     plan_id: planID,
     pack_id: packageID,
     insert_id: insertID,
     insert_type: 'insert',
     station_id: stationID
  */
  function addInsertToStation($params)
  {
      global $json;
      
      $insertID = intval($params['insert_id']);
      $packageID = intval($params['pack_id']);
      $stationID = intval($params['station_id']);
      $planID = intval($params['plan_id']);
      
      $json['params']=$params;
      //get package details
      $sql="SELECT * FROM jobs_inserter_packages WHERE id=$packageID";
      $dbPackage = dbselectsingle($sql);
      $package = $dbPackage['data'];
      $inserterID = $package['inserter_id'];
      
      $sql="INSERT INTO jobs_packages_inserts (plan_id, package_id, insert_id, inserter_id, hopper_id, insert_type) 
      VALUES ('$planID', '$packageID', '$insertID', '$inserterID', '$stationID', 'insert')";
      $json['package_insert_sql']=$sql;
      $dbInsert=dbinsertquery($sql);
      if($dbInsert['error']=='')
      {
          $json['status']='success';
      } else {
          $json['status']='error';
          $json['message']=$dbInsert['error'];
      }
      $sql="SELECT * FROM inserts WHERE id = $insertID";
      $json['insert_details_sql']=$sql;
      $dbInsertDetails = dbselectsingle($sql);
      $insert = $dbInsertDetails['data'];
      
      //look up the insert itself
      if($insert['weprint_id']>0)
      {
          $sql="SELECT A.pub_id, B.run_name FROM jobs A, publications_runs B WHERE A.id=$insert[weprint_id] AND A.run_id=B.id";
          $json['weprint_sql']=$sql;
          $dbJobs=dbselectsingle($sql);
          $accountname=stripslashes($dbJobs['data']['run_name']);
      } else {
          $sql="SELECT * FROM accounts WHERE id=$insert[advertiser_id]";
          $json['account_sql']=$sql;
          $dbAccount=dbselectsingle($sql);
          $accountname=stripslashes($dbAccount['data']['account_name']);
      }
      $insertname=htmlentities($accountname." ".stripslashes($insert['insert_tagline']));
      $insertname=str_replace("'","",$insertname);
      
      //need to find the schedule id for the insert, we have pubid and date
      
      
      $zones = "<span style='color: black;font-weight:bold;'>Zones: ";
      //get zones for this insert
      $sql="SELECT A.zone_label FROM publications_insertzones A, inserts_zoning B, inserts_schedule C, jobs_inserter_packages D 
WHERE A.id=B.zone_id AND B.schedule_id=C.id AND C.pub_id=D.pub_id AND C.insert_date=D.pub_date AND B.insert_id=$insert[id] AND D.id=$packageID";
      $dbZones = dbselectmulti($sql);
      if($dbZones['numrows']>0)
      {
          foreach($dbZones['data'] as $zone)
          {
              $zones.=" $zone[zone_label] |";
          }
      } 
      $zones.="</span>";
      
      
      
      $json['account_name']=$insertname.$zones;
      
           
  }
  
  function addJacketToStation($params)
  {
      global $json;
      
      $packageID = intval($params['pack_id']);
      $stationID = intval($params['station_id']);
      $planID = intval($params['plan_id']);
      
      $json['params']=$params;
      //get package details
      $sql="SELECT * FROM jobs_inserter_packages WHERE id=$packageID";
      $dbPackage = dbselectsingle($sql);
      $package = $dbPackage['data'];
      $inserterID = $package['inserter_id'];
      
      $sql="INSERT INTO jobs_packages_inserts (plan_id, package_id, insert_id, inserter_id, hopper_id, insert_type) 
      VALUES ('$planID', '$packageID', '0', '$inserterID', '$stationID', 'jacket')";
      $json['package_insert_sql']=$sql;
      $dbInsert=dbinsertquery($sql);
      if($dbInsert['error']=='')
      {
          $json['status']='success';
      } else {
          $json['status']='error';
          $json['message']=$dbInsert['error'];
      }  
  }
  
  function removeInsertFromPackage($params)
  {
      global $json;
      $insertID = intval($params['insert_id']);
      $packageID = intval($params['pack_id']);
      $stationID = intval($params['station_id']);
      $planID = intval($params['plan_id']);
      $insertType = addslashes($params['insert_type']);
      
      $json['params']=$params;
      
      if($insertType=='sticky')
      {
          $sql="UPDATE jobs_inserter_packages SET sticky_note_id=0 WHERE id=$packageID";
          $dbUpdate=dbexecutequery($sql);
          if($dbUpdate['error']=='')
          {
              $json['status']='success';
          } else {
              $json['status']='error';
              $json['message']=$dbUpdate['error'];
          }
      } else {
          $sql="DELETE FROM jobs_packages_inserts WHERE plan_id=$planID AND package_id=$packageID AND insert_id=$insertID AND hopper_id=$stationID AND insert_type='$insertType'";
          $json['removal_sql']=$sql;
          $dbRemove=dbexecutequery($sql);
          if($dbRemove['error']=='')
          {
              $json['status']='success';
          } else {
              $json['status']='error';
              $json['message']=$dbRemove['error'];
          }
      }
      if($insertType=='insert' || $insertType=='sticky')
      {
          createInsert($insertID);
      }
  }
  
  function createInsert($insertID)
  {
        global $json;
        
        $sql="SELECT * FROM inserts WHERE id=$insertID";
        $dbInsert=dbselectsingle($sql);
        $insert=$dbInsert['data'];
        
        if($insert['weprint_id']>0)
        {
            $sql="SELECT A.pub_id, B.run_name FROM jobs A, publications_runs B WHERE A.id=$insert[weprint_id] AND A.run_id=B.id";
            $dbJobs=dbselectsingle($sql);
            $accountname=stripslashes($dbJobs['data']['run_name']);
        } else {
            $sql="SELECT * FROM accounts WHERE id=$insert[advertiser_id]";
            $dbAccount=dbselectsingle($sql);
            $accountname=stripslashes($dbAccount['data']['account_name']);
        }
        
        
        if($insert['clone_id']!=0){$accountname.=" CLONED";}
        //each insert holder will be 120px wide
            
        $request=$insert['insert_quantity'];
        $insertname=htmlentities($accountname." ".stripslashes($insert['insert_tagline']));
        $insertname=str_replace("'","",$insertname);
        
        if((!$insert['confirmed'] && $GLOBALS['allowScheduleUnconfirmedInserts']) || $insert['confirmed'])
        {
            $notconfirmed='';
        } else {
            $notconfirmed='notconfirmed';
        }
        if($insert['received'])
        {
            $notreceived='';
        } else {
            $notreceived='notreceived';
        }
        if($insert['killed'])
        {
            $killed='killed';
        }  else {
            $killed='';
        }
        if($insert['sticky_note'])
        {
            $sticky='sticky';
        }  else {
            $sticky='';
        }
        $insertClasses = "insert $notconfirmed $notreceived $sticky $cloned $killed col-xs-6 col-sm-1";
        $json['insert_classes']= $insertClasses;
  }
  
  
   /*
  *  Adds an insert to a station in the designated package/plan
  *  Receives the following $_POST variabes
  *  action: 'addInsertToStation',
     plan_id: planID,
     pack_id: packageID,
     insert_id: insertID,
     insert_type: 'insert',
     station_id: stationID
  */
  function addPackageToStation($params)
  {
      global $json;
      
      $insertID = intval($params['insert_id']);
      $packageID = intval($params['pack_id']);
      $stationID = intval($params['station_id']);
      $planID = intval($params['plan_id']);
      
      $json['params']=$params;
      //get package details
      $sql="SELECT * FROM jobs_inserter_packages WHERE id=$packageID";
      $dbPackage = dbselectsingle($sql);
      $package = $dbPackage['data'];
      $inserterID = $package['inserter_id'];
      
      $sql="INSERT INTO jobs_packages_inserts (plan_id, package_id, insert_id, inserter_id, hopper_id, insert_type) 
      VALUES ('$planID', '$packageID', '$insertID', '$inserterID', '$stationID', 'package')";
      $json['package_insert_sql']=$sql;
      $dbInsert=dbinsertquery($sql);
      if($dbInsert['error']=='')
      {
          $json['status']='success';
      } else {
          $json['status']='error';
          $json['message']=$dbInsert['error'];
      }
      $sql="SELECT * FROM jobs_inserter_packages WHERE id = $insertID";
      $json['package_details_sql']=$sql;
      $dbPackageDetails = dbselectsingle($sql);
      $package = $dbPackageDetails['data'];
      
      
      $insertname=htmlentities($package['package_name']);
      $insertname=str_replace("'","",$insertname);
      
      $json['account_name']=$insertname;
      
           
  }
  
  function clones() {
    //case "cloneinsert":
        $pubid=intval($_POST['pubid']);
        $planid=intval($_POST['planid']);
        $insertid=intval($_POST['insertid']);
        $pubdate=$_POST['pubdate'];
        if($insertid!=0)
        {
            //first step, we clone the insert record
            $sql="SELECT * FROM inserts WHERE id=$insertid";
            $dbInsert=dbselectsingle($sql);
            $insert=$dbInsert['data'];
            $fields='';
            $values='';
            foreach($dbInsert['data'] as $field=>$value)
            {
                if($field!='id')
                {
                    $fields.=$field.",";
                    if($field=='clone_id'){$value=$insertid;}
                    $values.="'".$value."',";
                }
            }
            if($fields!='')
            {
                $fields=rtrim($fields,',');
                $values=rtrim($values,',');
                $sql="INSERT INTO inserts ($fields) VALUES ($values)";
                $response['insert_sql']=$sql;
                $dbInsert=dbinsertquery($sql);
                $newinsertid=$dbInsert['insertid'];
                
                //now clone the insert schedule that would have been for this pub/date and insert
                $sql="SELECT * FROM inserts_schedule WHERE insert_id=$insertid AND pub_id=$pubid AND insert_date='$pubdate'";
                $dbSchedule=dbselectsingle($sql);
                if($dbSchedule['numrows']>0)
                {
                    $fields='';
                    $values='';
                    //clone it just the same
                    foreach($dbSchedule['data'] as $field=>$value)
                    {
                        if($field!='id')
                        {
                            $fields.=$field.",";
                            if($field=='insert_id'){$value=$newinsertid;}
                            $values.="'".$value."',";
                        }
                    }
                    if($fields!='')
                    {
                        $fields=rtrim($fields,',');
                        $values=rtrim($values,',');
                        $sql="INSERT INTO inserts_schedule ($fields) VALUES ($values)";
                        $response['schedule_sql']=$sql;
                        $dbInsert=dbinsertquery($sql);
                        $newscheduleid=$dbInsert['insertid'];
                    }
                }
            
                
                if($insert['weprint_id']>0)
                {
                    $sql="SELECT A.pub_id, B.run_name FROM jobs A, publications_runs B WHERE A.id=$insert[weprint_id] AND A.run_id=B.id";
                    $dbJob=dbselectsingle($sql);
                    $jpubid=$dbJobs['data']['pub_id'];
                    $accountname=stripslashes($dbJobs['data']['run_name']);
                } else {
                    //get account name
                    $sql="SELECT account_name FROM accounts WHERE id=$insert[advertiser_id]";
                    $dbAccount=dbselectsingle($sql);
                    $accountname=stripslashes($insert['account_name']);
                }
                $accountname.=" CLONED";
                $insertname=$accountname." ".stripslashes($insert['insert_tagline']);
                $insertname=str_replace("'","",$insertname);
                $response['insertname']=$insertname;
                $request=$insert['insert_quantity'];
                $insertname="<b>$insertname</b>";
                if(!$insert['received']){$insertname="<span style='color:red;'>$insertname</span>";}
                $insertpages=$insert['standard_pages'];
                $insertinfo=$insertname."<br><b>Pages:</b> $insertpages <b>Request: </b>$request";
                $response['insertinfo']=$insertinfo;
                $response['status']='success';
                $response['original_id']=$insertid;
                $response['new_id']=$newinsertid;
                $response['new_schedule_id']=$newscheduleid;
            }else {
                $response['status']='error';
            }
            
        } else {
            $response['status']='error';
            
        }    
    
    // "removeclone":
        //this will delete an insert, set the original back to no clone and remove the associated schedule
        $insertid=intval($_POST['insertid']);
        $sql="SELECT clone_id FROM inserts WHERE id=$insertid";
        $dbOriginal=dbselectsingle($sql);
        $original=$dbOriginal['data']['clone_id'];
        if($original!=0)
        {
            $sql="UPDATE inserts SET clone_id=0 WHERE id=$original";
            $dbUpdate=dbexecutequery($sql);
            if($dbUpdate['error']=='')
            {
               $sql="DELETE FROM inserts WHERE id=$insertid";
               $dbDelete=dbexecutequery($sql);
               $sql="DELETE FROM inserts_schedule WHERE insert_id=$insertid";
               $dbDelete=dbexecutequery($sql);
               if($dbDelete['error']=='')
               {
                   $response['status']='success';
                   $response['message']='Cloned insert has been removed'; 
                   
               } else {
                   $response['status']='error';
                   $response['message']='Problem updating the original record'; 
                } 
            } else {
               $response['status']='error';
               $response['message']='Problem updating the original record'; 
            }
        } else {
            $response['status']='error';
            $response['message']='The clone id was 0';
        }
  }
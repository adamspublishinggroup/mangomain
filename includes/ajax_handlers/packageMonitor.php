<?php
  /*
  packageMonitor.php
  
  This script handles all the data updates from jobMonitor_inserter.php
  */
  include('bootstrapAjax.php');
  
  $packageID = intval($_POST['packageid']);
  $field = addslashes($_POST['field']);
  $value = addslashes($_POST['value']);
  
  $json['status']=200;
  
  //make sure the run data record exists, otherwise create it first
  $sql="SELECT * FROM jobs_inserter_rundata WHERE package_id=$packageID";
  $dbCheck=dbselectsingle($sql);
  if($dbCheck['numrows']==0)
  {
      $sql="SELECT * FROM jobs_inserter_packages WHERE id=$packageID";
      $dbPackage=dbselectsingle($sql);
      $package=$dbPackage['data'];
      $sql="INSERT INTO jobs_inserter_rundata (pub_id,pub_dateplan_id, package_id, package_startdatetime, package_stopdatetime, press_request, pallets, bundle_size_hd, bundle_size_sc, total_bundles) VALUES ('$package[pub_id]','$package[pub_date]','$package[plan_id]','$package[id]','$package[package_startdatetime]','$package[package_stopdatetime]', '$package[inserter_request]',0,0,0,0)";
      $dbInsert=dbinsertquery($sql);
      if($dbInsert['error']!='')
      {
          $json['status']='error';
          $json['message']=$dbInsert['error'];
      }
  }
  
  $sql="UPDATE jobs_inserter_rundata SET $field = '$value' WHERE package_id=$packageID";
  $dbUpdate = dbexecutequery($sql);
  if($dbUpdate['error']!='')
  {
      $json['status']='error';
      $json['message'].=$dbUpdate['error'];
  }
  $json['sql']=$sql;
  
  
  echo json_encode($json);
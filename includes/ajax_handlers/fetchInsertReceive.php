<?php
include("bootstrapAjax.php");

$insertID=intval($_REQUEST['record_id']);

$sql="SELECT A.*, B.account_name FROM inserts A, accounts B WHERE A.id=$insertID AND A.advertiser_id=B.id";
$dbInsert=dbselectsingle($sql);
$insert=$dbInsert['data'];
$pubid=$insert['pub_id'];
foreach($insert as $key=>$value)
{
    $cleanInsert[$key]=stripslashes($value);
}
if ($insert['receive_by']=='' || $insert['receive_by']==0)
{
   $cleanInsert['receive_by']=$_SESSION['userid'];
}

$advertisername=stripslashes($insert['account_name']);
$inserttagline=stripslashes($insert['insert_tagline']);

$details="You are receiving an insert with the following details:<br />\n";
$details.="Advertiser: $advertisername<br />\n";
$details.="Tagline: $inserttagline<br />\n";
//lets see if it has been scheduled
$sql="SELECT A.pub_name, B.insert_date FROM publications A, inserts_schedule B WHERE B.insert_id=$insertid AND B.pub_id=A.id";
$dbSchedule=dbselectmulti($sql);
if($dbSchedule['numrows']>0)
{
    $details.="<table class='table table-bordered'><tr><th>Publication:</th><th>Date</th></tr>";
    foreach($dbSchedule['data'] as $schedule)
    {
        $details.="<tr><td>".stripslashes($schedule['pub_name']).'</td><td>'.date("m/d/Y",strtotime($schedule['insert_date']))."</td></tr>\n";
    }
    $details.= "</table>\n";
}
$cleanInsert['insert_details']=$details;

echo json_encode($cleanInsert);

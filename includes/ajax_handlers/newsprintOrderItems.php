<?php
  //this script handles addition and deletion of items in a newsprint order
include("bootstrapAjax.php");
  
  if($_POST)
  {
      $action=$_POST['action'];
  } else {
      $action=$_GET['action'];
  }
  
  
  switch($action)
  {
      case "deleteorderitem":
      deleteorderitem();
      break;
      
      case "saveorderitem":
      saveorderitem();
      break;
      
      case "editorderitem":
      editorderitem();
      break;
      
      case "addorderitem":
      addorderitem();
      break;
  }
  
function addorderitem()
{
    global $json;
    //paper types
    $sql="SELECT * FROM paper_types ORDER BY common_name";
    $dbPaper=dbselectmulti($sql);
    $papertypes=array();
    $papertypes[0]="Type";
    if ($dbPaper['numrows']>0)
    {
        foreach($dbPaper['data'] as $paper)
        {
            $papertypes[$paper['id']]=$paper['common_name'];
        }
    }

    //paper sizes
    $sql="SELECT * FROM paper_sizes ORDER BY width ASC";
    $dbSizes=dbselectmulti($sql);
    $sizes=array();
    $sizes[0]="Size";
    if ($dbSizes['numrows']>0)
    {
        foreach($dbSizes['data'] as $size)
        {
            $sizes[$size['id']]=$size['width'];
        }
    }  


    $orderid=$_POST['orderid'];
    $i='new';
    $json['status']='success';
    
    
    $json['newItem']="<div id='item_$i' class='row'><div class='col-xs-1'><span id='success_$i' style='display:none;'><i class='fa fa-check'></i></span></div>\n<div class='col-xs-3'>".make_select('paper_'.$i,$papertypes[$item['paper_type_id']],$papertypes)."</div><div class='col-xs-3'>".make_select('size_'.$i,$sizes[$item['size_id']],$sizes)."</div><div class='col-xs-3'> Tons: <input type='text' name='tonnage_$i' id='tonnage_$i' value='$item[tonnage_request]' class='ton' size=5 onChange='calcTonnage();' onBlur='newsprintOrderItemSave(\"$i\",\"$orderid\");' />MT</div>\n<div class='col-xs-1'> <a href='#' onclick='newsprintOrderItemSave(\"$i\",\"$orderid\");' style='text-decoration:none;'><i class='fa fa-floppy-o'></i> Save</a></div>\n<div class='col-xs-1'><a href='#' onclick='newsprintOrderItemDelete(\"$i\",\"$orderid\");' style='text-decoration:none;'><i class='fa fa-trash'></i> Delete</a></div>\n</div>\n";
    
    
}
  
  function saveorderitem()
  {
      global $siteID, $json;
      $orderid=intval($_POST['orderid']);
      $paper=intval($_POST['paper']);
      $size=addslashes($_POST['size']);
      $tonnage=addslashes($_POST['tonnage']);
      $itemid=intval($_POST['itemid']);
      
      if($itemid=='new' || $itemid==0)
      {
          //adding new item, we'll need to be sure to return the insert id
          //get the max display order and add 1 to it
          $sql="SELECT MAX(itemdisplay_order) as mo FROM order_items WHERE order_id='$orderid'";
          $dbMax=dbselectsingle($sql);
          $max=$dbMax['data']['mo'];
          $max++;
          $sql="INSERT INTO order_items (order_id, paper_type_id, size_id,tonnage_request, itemdisplay_order, site_id) VALUES 
          ('$orderid','$paper','$size','$tonnage','$max','$siteID')";
          $dbInsert=dbinsertquery($sql);
          $itemid=$dbInsert['insertid'];
          if($dbInsert['error']!='')
          {
              $json['status']='error';
              $json['message']=$dbInsert['error'];
          } else {
              $json['status']='success';
              $json['id']=$itemid;
          }
      } else {
          //updating an existing one
          $sql="UPDATE order_items SET paper_type_id='$paper', size_id='$size', tonnage_request='$tonnage' WHERE id=$itemid";
          $dbUpdate=dbexecutequery($sql);
          if($dbUpdate['error']!='')
          {
              $json['status']='error';
              $json['message']=$dbUpdate['error'];
          } else {
              //lets look up the total weight for the order
              
              $json['status']='success';
          }
      }
      $sql="SELECT SUM(tonnage_request) as total FROM order_items WHERE order_id=$orderid";
      $dbTotal=dbselectsingle($sql);
      $total=$dbTotal['data']['total'];
      $json['paper']=$GLOBALS['papertypes'][$paper];
      $json['size']=$GLOBALS['sizes'][$size];
      $json['tonnage']=$tonnage;
      $json['total_tons']=$total;
      $json['id']=$itemid;
  }
  
  function editorderitem()
  {
      global $siteID, $json;
      $orderid=intval($_POST['orderid']);
      $itemid=intval($_POST['orderitemid']);
      
      $sql="SELECT * FROM order_items WHERE id=$itemid";
      $dbItem = dbselectsingle($sql);
      $item=$dbItem['data'];
      $paper=$item['paper_type_id'];
      $size=$item['size_id'];
      $tonnage=$item['tonnage_request'];
      
      $sql="SELECT SUM(tonnage_request) as total FROM order_items WHERE order_id=$orderid";
      $dbTotal=dbselectsingle($sql);
      $total=$dbTotal['data']['total'];
      $json['paper']=$paper;
      $json['size']=$size;
      $json['tonnage']=$tonnage;
      $json['total_tons']=$total;
      $json['id']=$itemid;
      $json['status']='success';
  }
  
  function deleteorderitem()
  {
      global $json;
      $orderid=$_POST['orderid'];
      $itemid=str_replace("del_","",$_POST['itemid']);
      $sql="DELETE FROM order_items WHERE id=$itemid";
      $dbDelete=dbexecutequery($sql);
      if($dbDelete['error']!='')
      {
          $json['status']='error';
          $json['message']=$dbDelete['error'];
      } else {
          $sql="SELECT SUM(tonnage_request) as total FROM order_items WHERE order_id=$orderid";
          $dbTotal=dbselectsingle($sql);
          $total=$dbTotal['data']['total'];
          $json['total']=$total;
          $json['status']='success';
      }
  }
  
 
 print json_encode($json);
 
 dbclose();

function manualDraw()
{
    //function used if someone manually keys in a total draw
    $('#drawHD').val(0);
    $('#drawSC').val(0);
    $('#drawMail').val(0);
    $('#drawOffice').val(0);
    $('#drawCustomer').val(0);
    $('#drawOther').val(0);
}

function calcDraw()
{
    var drawHD=$('drawHD').val();
    var drawSC=$('drawSC').val();
    var drawMail=$('drawMail').val();
    var drawOffice=$('drawOffice').val();
    var drawCustomer=$('drawCustomer').val();
    var drawOther=$('drawOther').val();
    var dtotal=0;
    if (drawHD!=''){dtotal+=parseFloat(drawHD);}
    if (drawSC!=''){dtotal+=parseFloat(drawSC);}
    if (drawMail!=''){dtotal+=parseFloat(drawMail);}
    if (drawOffice!=''){dtotal+=parseFloat(drawOffice);}
    if (drawCustomer!=''){dtotal+=parseFloat(drawCustomer);}
    if (drawOther!=''){dtotal+=parseFloat(drawOther);}
    $('drawTotal').val(dtotal);
}

function toggleSection(sid)
{
    if($('#section'+sid+'_name').val()!='') {
        $('#section'+sid+'_enable').prop('checked',true);
    } else {
        $('#section'+sid+'_enable').prop('checked',false);
    }
}


function pressQuickJump(jobid)
{
    document.location.href="?jobid="+jobid;    
}

               

function checklistChange(objid)
{
    var tblock=$('#check_'+objid);
    var tclass=tblock.className;
    var jobid=$('#jobid').val();
    if (tclass=='checklist_checked')
    {
        //means we're unchecking a checked item
        $.ajax({
          url: "includes/ajax_handlers/jobmonitorPress.php",
          type: "POST",
          data: ({type:'checklist',jobid:jobid,value:'0',source:objid}),
          dataType: "html",
          success: function(response){
              response=response.split("|");
              if($.trim(response[0])=='success')
              {
                //all good!
                tblock.className='checklist_unchecked';
              } else {
                  //error
                  var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
             }
          },
           error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
        })
        
    } else {
        //means we're checking an unchecked item
        $.ajax({
          url: "includes/ajax_handlers/jobmonitorPress.php",
          type: "POST",
          data: ({type:'checklist',jobid:jobid,value:'1',source:objid}),
          dataType: "html",
          success: function(response){
              response=response.split("|");
              if($.trim(response[0])=='success')
              {
                //all good!
                tblock.className='checklist_checked';
              } else {
                  //error
                  var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
             }
          },
           error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
        })
        
    }
}

function benchmarkChange(objid,ares)
{
    var t=objid.split('_');
    if (t[0]=='pagesendtime')
    {
        var ob=document.getElementById(objid);
        objid=t[1];
        var type='page';
    } else if (t[0]=='plateapproval')
    {
        var ob=document.getElementById(objid);
        objid=t[1];
        var type='plateapprove';
    } else if (t[0]=='checklistOperator')
    {
        var ob=document.getElementById(objid);
        objid=t[1];
        var type='checklistOperator';
    } else if (t[0]=='jobOperator')
    {
        var ob=document.getElementById(objid);
        objid=t[1];
        var type='jobOperator';
    } else if (t[0]=='platereceivek')
    {
        var ob=document.getElementById(objid);
        objid=t[1];
        var type='platereceivek';
    } else if (t[0]=='platereceivec')
    {
        var ob=document.getElementById(objid);
        objid=t[1];
        var type='platereceivec';
    } else if (t[0]=='platereceivem')
    {
        var ob=document.getElementById(objid);
        objid=t[1];
        var type='platereceivem';
    } else if (t[0]=='platereceivey')
    {
        var ob=document.getElementById(objid);
        objid=t[1];
        var type='platereceivey';
    } else if (t[0]=='platereceiveall')
    {
        var ob=document.getElementById(objid);
        objid=t[1];
        var type='platereceiveall';
    } else if (t[0]=='colortime')
    {
        var ob=document.getElementById('colortime_'+t[1]);
        objid=t[1];
        var type='colorrelease';
    } else if (t[0]=='setupstart')
    {
        var ob=document.getElementById('benchmark_'+objid);
        objid=t[0];
        var type='stat';
    } else if (t[0]=='setupstop')
    {
        var ob=document.getElementById('benchmark_'+objid);
        objid=t[0];       
        var type='stat';  
    } else {
        var ob=document.getElementById('benchmark_'+objid);
        var type='benchmark';
    }
    var bvalue=ob.value;
    var jobid=document.getElementById('jobid').value;
    $.ajax({
      url: "includes/ajax_handlers/jobmonitorPress.php",
      type: "POST",
      data: ({type:type,jobid:jobid,value:bvalue,source:objid}),
      dataType: "html",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
            //all good!
          } else {
              //error
              var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
      },
       error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
    })
}

function jobPressStopNotes(jobid)
{
    if ($('#captureStopNotes').val()=='1')
    {
        var $dialog = $('<div id="jNotes"></div>').dialog({
          title: 'Job completion notes',        
          autoOpen: false, 
          height: 400, 
          width: 600,
          modal:true,
          buttons: [
              {
                text: 'Cancel',
                click: function() { 
                    $(this).dialog('destroy');
                }
              },
              {
                text: 'Save Notes',
                click: function() { 
                    $('#jobnotesForm').submit();
                    $(this).dialog('destroy');  
                }
              }
          ]
        })
        $dialog.load('includes/ajax_handlers/pressStopJobNotes.php?jobid='+jobid).dialog('open');
    } 
}

function jobMonitorPressTimeSet(type)
{
    var jobid=document.getElementById('jobid').value;
    $.ajax({
      url: "includes/ajax_handlers/jobmonitorPress.php",
      type: "POST",
      data: ({type:'stat',jobid:jobid,source:type}),
      dataType: "html",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
              $('#benchmark_'+type).val(response[1]);
              if(type=='stoptime')
              {
                 jobPressStopNotes(jobid); 
              }
          }
      }
    })
}


/*new as of 11/27/10 */
function getLayouts()
{
    //need to build a url based on the selection information provided
    var tosend='';
    var section1=document.getElementById('section1_enable');
    var section2=document.getElementById('section2_enable');
    var section3=document.getElementById('section3_enable');
    if (section1.checked)
    {
        var vs1need=1;
    } else {
        var vs1need=0;
    }
    if (section2.checked)
    {
        var vs2need=1;
    } else {
        var vs2need=0;
    }
    if (section3.checked)
    {
        var vs3need=1;
    } else {
        var vs3need=0;
    }
    var vs1low=document.getElementById('section1_low').value;
    var vs1high=document.getElementById('section1_high').value;
    var vs1format=document.getElementById('section1_format').value;
    var vs1lead=document.getElementById('section1_lead').value;
    var vs1double=document.getElementById('section1_doubletruck').checked?1:0;
    var vs2low=document.getElementById('section2_low').value;
    var vs2high=document.getElementById('section2_high').value;
    var vs2format=document.getElementById('section2_format').value;
    var vs2lead=document.getElementById('section2_lead').value;
    var vs2double=document.getElementById('section2_doubletruck').checked?1:0;
    var vs3low=document.getElementById('section3_low').value;
    var vs3high=document.getElementById('section3_high').value;
    var vs3format=document.getElementById('section3_format').value;
    var vs3lead=document.getElementById('section3_lead').value;
    var vs3double=document.getElementById('section3_doubletruck').checked?1:0;
    
    $.ajax({
      url: 'includes/ajax_handlers/fetchMatchingLayouts.php',
      type: 'get',
      dataType: 'json',
      data: ({s1need:vs1need,s1low:vs1low,s1high:vs1high,s1format:vs1format,s1lead:vs1lead,s1double:vs1double,s2need:vs2need,s2low:vs2low,s2high:vs2high,s2format:vs2format,s2lead:vs2lead,s2double:vs2double,s3need:vs3need,s3low:vs3low,s3high:vs3high,s3format:vs3format,s3lead:vs3lead,s3double:vs3double}),
      success: function(j)
      {
          var options = '';
          for (var i = 0; i < j.length; i++) {
              options += '<option value="' + j[i].id + '">' + j[i].label + '</option>';
          }
          $("#layouts").html(options);  
      },
      error:function (xhr, ajaxOptions, thrownError){
        alert(xhr.status);
        alert(thrownError);
      }
    });
}

/*new as of 11/27/10 */
function getPressDiagram()
{
    if($('#layouts').val()!=0)
    {
        $.ajax({
          url: "includes/ajax_handlers/layoutGenerator.php",
          type: "GET",
          data: ({layoutid:$('#layouts').val(),mode:'inc',display:true,save:false}),
          dataType: "html",
          success: function(response){
             $('#layout_id').val($('#layouts').val());
             $('#layout_preview').html(response);
             $('#setlayoutBtn').show();
          },
          error: function (xhr, desc, er) {
              console.log('error with ajax call: '+desc+" "+er);
           }
       });
   }
}


function pressmanChange(jobid,pressmanid)
{
    var pblock=document.getElementById('pressman_'+pressmanid);
    var pclass=pblock.className;
    if (pclass=='checklist_checked')
    {
        //means we're unchecking a checked item
        $.ajax({
          url: "includes/ajax_handlers/jobmonitorPress.php",
          type: "POST",
          data: ({type:'crew',jobid:jobid,value:'0',source:pressmanid}),
          dataType: "html",
          success: function(response){
              response=response.split("|");
              if($.trim(response[0])=='success')
              {
                //all good!
                pblock.className='checklist_unchecked';
              } else {
                  //error
                  var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
              }
          },
           error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
        })

    } else {
        //means we're checking an unchecked item
        $.ajax({
          url: "includes/ajax_handlers/jobmonitorPress.php",
          type: "POST",
          data: ({type:'crew',jobid:jobid,value:'1',source:pressmanid}),
          dataType: "html",
          success: function(response){
              response=response.split("|");
              if($.trim(response[0])=='success')
              {
                //all good!
                pblock.className='checklist_checked';
              } else {
                  //error
                  var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
              }
          },
           error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
        })
        
    }
    
}


function pressStopInfo(obj,type,towerid,subpart)
{
    var cid="c"+obj.id;
    var cin=document.getElementById(cid);
    var infobox=document.getElementById('stopinfo');
    var cinfo=infobox.value;
    var newinfo=type+"_"+towerid+"_"+subpart+"|";
    if (cin.value=='0')
    {
        cin.value='1';
        obj.className='imgSelected';
        cinfo=cinfo+newinfo;
        infobox.value=cinfo;
    } else if (cin.value=='1')
    {
        cin.value='0';
        obj.className='imgUnselected';
        cinfo=cinfo.replace(newinfo,'');
        infobox.value=cinfo;
    }
}


function pressBoxes()
{
    var tjobid=$('#jobid').val(); 
    $.ajax({
      url: "includes/ajax_handlers/jobmonitorPressBoxes.php",
      type: "POST",
      data: ({jobid:tjobid,type:'missingpages'}),
      dataType: "html",
      success: function(response){
          $('#pageslist').html(response);
          
      }
     });
     $.ajax({
      url: "includes/ajax_handlers/jobmonitorPressBoxes.php",
      type: "POST",
      data: ({jobid:tjobid,type:'missingplates'}),
      dataType: "html",
      success: function(response){
          $('#plateslist').html(response);
      }
     });
     $.ajax({
      url: "includes/ajax_handlers/jobmonitorPressBoxes.php",
      type: "POST",
      data: ({jobid:tjobid,type:'remakes'}),
      dataType: "html",
      success: function(response){
             $('#remakeslist').html(response);
          
      }
     });
}

function removeStopNote(stopid)
{
    ajaxpage('includes/generalAjaxHandler.php?action=&type=deletestopnote&id='+stopid+'&secondid=0');
}

function removeJobStop(jobid,stopid)
{
    var answer=confirm('Are you sure you want to delete this stop?');
    if (answer)
    {
        ajaxpage('includes/generalAjaxHandler.php?action=&type=killstop&id='+jobid+'&secondid='+stopid);
    }
}

function getPressRuns(source_id,target_id)
{
    $.ajax({
      url: "includes/ajax_handlers/fetchRuns.php",
      type: "POST",
      data: ({pub_id:$('#'+source_id).val(),zero:1}),
      dataType: "json",
      success: function(response){
          $.each(response, function(key, run) {   
             $('#'+target_id)
                  .append($('<option>', { "value" : run.id })
                  .text(run.label)); 
          }); 
      }
    })
     
}

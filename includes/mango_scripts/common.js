//global variable initialization
             



function toggleBlock(blockID)
{
    var htmlBlock=document.getElementById(blockID);
    var mode=htmlBlock.style.display;
    mode=(mode=='block')?'none':'block';
    htmlBlock.style.display=mode;       
    
}


function parseFileMonitor()
{
    var delimiter=$('#delimiter').val();
    var sample=$('#sample').val();
    var i=0;
    var sampledisplay='';
    var spiece='';
    if (sample!='')
    {
        if (delimiter!='')
        {
            var spieces=sample.split(delimiter)
            for (i in spieces)
            {
                spiece=spieces[i].split(".");
                sampledisplay+='Position '+i+' is '+spiece[0]+"<br>";    
            }
            $('#sample_display').html(sampledisplay);
        } else {
            //harder :*( need to just worry about it in the piece display area
        }
    }
}

function showFileMonitorPiece(id)
{
    var delimiter=$('#delimiter').val();
    var sample=$('#sample').val();
    var i=0;
    var idval=$('#'+id).val();
    var spiece='';
    var partlength=0;
    if (sample!='')
    {
        if (delimiter!='')
        {
            var spieces=sample.split(delimiter)
            for (i in spieces)
            {
                spieces[i]=spieces[i].split(".");
            }
            if(idval!='')
            {
                spiece=spieces[idval];
            }
        } else {
            //harder :*(
            sample=sample.split(".");
            sample=sample[0];
            var parts=idval.split('-');
            partlength=parseInt(parts[1])-parseInt(parts[0])+1;
            //console.log('length is '+partlength+' part 1 is '+parts[1]+' part 0 is '+parts[0]);
            spiece=sample.substr(parts[0],partlength);
        }
    }
    if(idval!='')
    {
        if (id=='pub_pos')
        {
           $('#pub_sample').html('Sample: '+spiece) 
        } else if (id=='section_pos')
        {
           $('#section_sample').html('Sample: '+spiece) 
        } else if (id=='productcode_pos')
        {
           $('#productcode_sample').html('Sample: '+spiece) 
        } else if (id=='date_pos')
        {
           $('#date_sample').html('Sample: '+spiece) 
        } else if (id=='page_pos')
        {
           $('#page_sample').html('Sample: '+spiece) 
        } else if (id=='color_pos')
        {
           $('#color_sample').html('Sample: '+spiece) 
        }
    }    
}

function getMaintenanceHelpTopics(dept)
{
    if($('#keywords').val()!='')
    {
        $.ajax({
          url: "includes/ajax_handlers/findTroubleSolutions.php",
          type: "GET",
          data: ({keywords:$('#keywords').val(),dept:dept}),
          dataType: "html",
          success: function(response){
             $('#search_results').html(response);
          },
          error: function (xhr, desc, er) {
            
           }
       });
   }
}

/*new as of 11/27/10 */
function getPressAndInsertRuns()
{
    //alert('function called got pubid of '+$("#pub_id").val());
    var pubid=$("#pub_id").val();
    $.ajax({
    url: "includes/ajax_handlers/fetchInsertRuns.php",
    data: {pub_id:pubid},
    type:  'post',
    dataType: 'json',
    success: function (j) {
        var options = [], i = 0, o = null;
        for (i = 0; i < j.length; i++) {
            // required to get around IE bug (http://support.microsoft.com/?scid=kb%3Ben-us%3B276228)
            var options = '';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].id + '">' + j[i].label + '</option>';
            }
            $("#run_id").html(options);
        }

        // hand control back to browser for a moment
        setTimeout(function () {
        $("#run_id")
            .find('option:first')
            .attr('selected', 'selected')
            .parent('select')
            .trigger('change');
        }, 0);
        },
        error: function (xhr, desc, er) {}
           
    });
    
    $.ajax({
    url: "includes/ajax_handlers/fetchRuns.php",
    data: {pub_id:pubid, zero: 1},
    type:  'post',
    dataType: 'json',
    success: function (j) {
        var options = [], i = 0, o = null;
        for (i = 0; i < j.length; i++) {
            // required to get around IE bug (http://support.microsoft.com/?scid=kb%3Ben-us%3B276228)
            var options = '';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].id + '">' + j[i].label + '</option>';
            }
            $("#pressrun_id").html(options);
        }

        // hand control back to browser for a moment
        setTimeout(function () {
        $("#pressrun_id")
            .find('option:first')
            .attr('selected', 'selected')
            .parent('select')
            .trigger('change');
        }, 0);
        },
        error: function (xhr, desc, er) {}
            
        
    });
}

/*new as of 11/27/10 */
function toggleCheckBoxes(status,className) 
{
    if(className==''){className='checkbox'}
    $("."+className).each( function() {
        $(this).attr("checked",status);
    }
    )
    calcInsertZoneTotal(className);
}


function alertMessage(message,type)
{
    if (type=='error')
    {
        var autoclose=false;
        var time=5000;
        var useescape=true;
        var location=top;
        var msgclass='fail';
    } else {
        var autoclose=true;
        var time=5000;
        var useescape=true;
        var location=top;
        var msgclass='success';
    }
    
    jQuery('body').showMessage({
    thisMessage:      message,
    className:        msgclass,
    position:        location,
    opacity:        90,
    useEsc:            useescape,
    displayNavigation: true,
    autoClose:         autoclose,
    delayTime:         time,
    closeText:         'close',
    escText:      'Esc Key or'
    });

}





/********************************************************************************************
*
*  THESE SCRIPTS ARE FOR THE PAGINATION JOB MONITOR
*
********************************************************************************************/
function viewPageSubs(id)
{
    $('#subpages'+id).slideToggle('fast');
    //get details for that page with an ajax call
    $.ajax({
      url: "includes/ajax_handlers/jobmonitorPagination.php",
      type: "POST",
      data: ({action:'getpageversions',id:id}),
      dataType: "json",
      success: function(response){
          if(response.status=='success')
          {
              $('#subpages'+id).html(response.message);
          }
      },
      error:function (xhr, ajaxOptions, thrownError){
          var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
           }
    })
    return false;
    
}

function viewPageDetails(id)
{
    $('#pageDetails'+id).slideToggle('fast');
    $.ajax({
      url: "includes/ajax_handlers/jobmonitorPagination.php",
      type: "POST",
      data: ({action:'getpagedetails',id:id}),
      dataType: "json",
      success: function(response){
          if(response.status=='success')
          {
              $('#pageDetails'+id).html(response.message);
          }
      },
      error:function (xhr, ajaxOptions, thrownError){
          var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
       }
    })
    return false;
}


function viewPlateDetails(id,type)
{
   $('#plateDetails'+id).slideToggle('fast');
   $.ajax({
      url: "includes/ajax_handlers/jobmonitorPagination.php",
      type: "POST",
      data: ({action:'getplatedetails',id:id}),
      dataType: "json",
      success: function(response){
          if(response.status=='success')
          {
              $('#plateDetails'+id).html(response.message);
          }
      },
      error:function (xhr, ajaxOptions, thrownError){
          var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                }) 
       }
    })
    return false;
}

function viewPlateSubs(id,type)
{
    $.ajax({
      url: "includes/ajax_handlers/jobmonitorPagination.php",
      type: "POST",
      data: ({action:'getplateversions',id:id}),
      dataType: "json",
      success: function(response){
          if(response.status=='success')
          {
              $('#subplates'+id).html(response.message)
              $('#toggleplatesub'+id).click(function() {
              $('#subplates'+id).slideToggle('fast', function() {
                // Animation complete.
              });
            });
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
       }
    })
    return false;

}

function setPaginationTime(id, type, value)
{
    $.ajax({
      url: "includes/ajax_handlers/jobmonitorPagination.php",
      type: "POST",
      data: ({action:'settime',id:id,type:type,value:value}),
      dataType: "json",
      success: function(response){
          
          if(response.status=='success')
          {
              if(type=='pageapprove')
              {
                 $('#pagesendtime_'+id).val(response.time);  
              } else if (type=='pagecolor')
              {
                 $('#colortime_'+id).val(response.time); 
              } else if (type=='plateapprove')
              {
                 $('#platesendtime_'+id).val(response.time); 
              } else if (type=='platecolor')
              {
                 $('#platecolortime_'+id).val(response.time); 
              } else if (type=='plateapproveall')
              {
                 $('#plateapproveall_'+id).val(response.time); 
                 $('#plateapproveK_'+id).val(response.time); 
                 $('#plateapproveC_'+id).val(response.time); 
                 $('#plateapproveM_'+id).val(response.time); 
                 $('#plateapproveY_'+id).val(response.time); 
              } else if (type=='plateapprovek')
              {
                 $('#plateapproveK_'+id).val(response.time); 
              } else if (type=='plateapprovec')
              {
                 $('#plateapproveC_'+id).val(response.time); 
              } else if (type=='plateapprovem')
              {
                 $('#plateapproveM_'+id).val(response.time); 
              } else if (type=='plateapprovey')
              {
                 $('#plateapproveY_'+id).val(response.time); 
              }
          } else {
              //error
              var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response.message+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
       }
    })
    return false;

}

function plateMonitorExtra(type,jobid,value)
{
    $.ajax({
      url: "includes/ajax_handlers/jobmonitorPagination.php",
      type: "POST",
      data: ({action:'plateextra',type:type,value:value,jobid:jobid}),
      dataType: "json",
      success: function(response){
          if(response.status=='success')
          {
              
          } else {
             var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response.message+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
       }
    })
    return false;

}

function remakePage(pid)
{
    // @TODO this script needs to be re-written 
    $.ajax({
      url: "includes/ajax_handlers/jobmonitorPagination.php",
      type: "POST",
      data: ({action:'remakepage',id:pid}),
      dataType: "json",
      success: function(response){
          if(response.status=='success')
          {
              //alert("response="+response[0]+"\n"+"newpageid="+response[1]+"\n"+"oldplateid="+response[2]+"\n"+"newplateid="+response[3]+"\n"+"platehtml="+response[4]+"\n"+"pagehtml="+response[5]+"\n");
              //ok, we need to change the id of the existing div and stick in the new contents
              $('#page'+pid).html(response[5]);
              $('#page'+pid).attr("id", 'page'+response[1]);
              //update the plate with new plate id and clear times
              $('#plate'+response[2]).html(response[4]);
              $('#plate'+response[2]).attr("id", 'plate'+response[3]);
              //alert('new page id is '+response[1]);
          } else {
              //error
              var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
       }
    })
    return false; 
}

function resetRemakePage(pageid,originalpageid,plateid,originalplateid)
{
   //@TODO this function needs to be re-written
    $.ajax({
      url: "includes/ajax_handlers/jobmonitorPagination.php",
      type: "POST",
      data: ({action:'undoremake',id:pageid,value:pageid+"|"+originalpageid+"|"+plateid+"|"+originalplateid}),
      dataType: "json",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
              //alert("response="+response[0]+"\n"+"response1="+response[1]+"\n"+"response2="+response[2]);
              //ok, we need to change the id of the existing div and stick in the new contents
              $('#page'+pageid).html(response[1]);
              $('#page'+pageid).attr("id", 'page'+originalpageid);
              //update the plate with new plate id and clear times
              $('#plate'+plateid).html(response[2]);
              $('#plate'+plateid).attr("id", 'plate'+originalplateid);
          } else {
              //error
              var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
       }
    })
    return false; 
}

function getDeadlineDetails(jobid) {
     $.ajax({
      url: "includes/ajax_handlers/jobmonitorPagination.php",
      type: "POST",
      data: ({id:jobid,action:'deadlines'}),
      dataType: "json",
      success: function(response){
          if(response.status=='success')
          {
             $('#deadlinedata').html(response.html);
          } else {
             var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response.html+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('close');
                        }
                    }]
                })
          }
      }
     });
}

function pressDataCheckZero(id)
{
    if (document.getElementById(id).value=='')
    {
        document.getElementById(id).value=0;
    }
}

function pressCounterCheck()
{
    var counterstart=$('#counterstart').val();
    var counterstop=$('#counterstop').val();
    var draw=$('#draw').val();
    var difference=counterstop-counterstart-draw;
    if (difference>5000)
    {
        $('#counterAlertDiv').show();
        $('#counteralert').html("<span style='color:red;font-weight:bold'>Large spoilage, please check numbers</span>");
    } else {
        $('#counteralertdiv').hide();
        $('#counteralert').html();
    }
}

function checkPressData()
{
    //this script will check start/stop counters and times as well as a few other factors
    var override=$('#override');
    var saveform=true;
    var alertmessage="FOUND THE FOLLOWING ERROR(S):\n";
    var lead=$('#pressoperator').val();
    var counterstart=$('#counterstart').val();
    var difference=counterstop-counterstart;
    var startdate=$('#starttime').val();
    var gooddate=$('#goodtime').val();
    var stopdate=$('#stoptime').val();
    var startparts=startdate.split(" ");
    var start=startparts[0].split("-");
    var stime=startparts[1].split(":");
    var starthour=stime[0];
    var startminute=stime[1];
    var datestart=new Date();    
    datestart.setFullYear(start[0],start[1]-1,start[2]);
    datestart.setHours(starthour);
    datestart.setMinutes(startminute);
    
    var goodparts=startdate.split(" ");
    var good=goodparts[0].split("-");
    var gtime=goodparts[1].split(":");
    var goodhour=gtime[0];
    var goodminute=gtime[1];
    var dategood=new Date();    
    dategood.setFullYear(good[0],good[1]-1,good[2]);
    dategood.setHours(goodhour);
    dategood.setMinutes(goodminute);
    
    var stopparts=startdate.split(" ");
    var stop=stopparts[0].split("-");
    var stoptime=stopparts[1].split(":");
    var stophour=stoptime[0];
    var stopminute=stoptime[1];
    var datestop=new Date();    
    datestop.setFullYear(stop[0],stop[1]-1,stop[2]);
    datestop.setHours(stophour);
    datestop.setMinutes(stopminute);
    
    var dt=$('#dataset').val();
    var dtdate=dt.split(' ');
    var dttime=dtdate[1].split(':');
    dtdate=dtdate[0];
    dtdate=dtdate.split('-');
    var dataset=new Date(dtdate[0],dtdate[1],dtdate[2],dttime[0],dttime[1]);
    //ok we have dates, lets start comparing
    if (datestart>dataset)
    {
        saveform=false;
        alertmessage+='You are trying to set a start time later than right now!\n';
    }
    if (datestop>dataset)
    {
        saveform=false;
        alertmessage+='You are trying to set a stop time later than right now!\n';
    }
    if (dategood>dataset)
    {
        saveform=false;
        alertmessage+='You are trying to set a good time later than right now!\n';
    }
    if (datestart>datestop)
    {
        saveform=false;
        alertmessage+='Your start date/time is later than the stop!\n';
    }
    
    if (dategood>datestop)
    {
        saveform=false;
        alertmessage+="Your good date/time is later than the stop!\n";
    }
    if (dategood<datestart)
    {
        saveform=false;
        alertmessage+='Your good date/time is set earlier than the start!\n';
    }
    //now check the minutes alert on anything over 6 hours in time
    var runtime=((((datestop.getTime()-datestart.getTime())/1000)/60)/60)
    if (runtime>pressRunTimeThreshold)
    {
        saveform=false;
        alertmessage+='Looks like a run time over 6 hours, most likely a problem!\n';
    }
    
    if (counterstart==0)
    {
        saveform=false;
        alertmessage+='You missed the counter start number!\n';
    } else if(counterstop==0)
    {
        saveform=false;
        alertmessage+='You missed the counter stop number!\n';
    } else if(difference<0)
    {
        saveform=false;
        alertmessage+='Check your counter numbers, looks like they may be flipped.\n';    
    } else if(difference>pressCounterThreshhold)
    {
        saveform=false;
        alertmessage+='The difference in stop and start is over the threshhold. Please check your number. If they are right, you can change the threshhold in system preferences.\n';    
    }
    if (lead==0)
    {
        saveform=false;
        alertmessage+='Please set lead operator before saving.\n';
    }
    
    
    //if override is checked, alert to that fact, submit the form
    if (override.checked)
    {
        saveform=true;
        alert('You checked override, so we are saving even if there are errors!');
    }
    if (saveform==false)
    {
        alert(alertmessage);   
    } else {
        $("#pressdata").submit(); 
    }
    
    
}

function checkMailroomData()
{
    var saveform=true;
    var alertmessage='';
    alertmessage='Testing mode';
    if (saveform==false)
    {
        alert(alertmessage);   
    } else {
        document.getElementById("maildata").submit(); 
    }
    
}

function limitText(limitField, limitNum) {
    if (limitField.value.length > limitNum) {
        limitField.value = limitField.value.substring(0, limitNum);
    }
}

function toggleInsertDamage()
{
    var dblock=document.getElementById('insertDamage');
    var dcheck=document.getElementById('damaged');
    if (dcheck.checked)
    {
        dblock.style.display='block';
    } else {
        dblock.style.display='none';
    }
    
}

function checkAllCheckboxes(containDiv,status)
{
    $("#"+containDiv+" input").each( function() {
        $(this).attr("checked",status);
    })
    $("."+containDiv+" input").each( function() {
        $(this).attr("checked",status);
    })
}

function uncheckAllCheckboxes()
{
    var fields=document.forms[0]
    for (i = 0; i < fields.length; i++)
    fields[i].checked = false ;
    
}

var selectDest='';



/*functions for purchase order system*/
function addNewPartFromPO()
{
    var pnameInput=document.getElementById('newpartname');
    var pnumberInput=document.getElementById('newpartnumber');
    var pcostInput=document.getElementById('newpartcost');
    var ptaxableInput=document.getElementById('newparttaxable');
    if (ptaxableInput.checked)
    {
        var taxable=1;
    } else {
        var taxable=0;
    }
    if (pnameInput.value!='')
    {
         $.ajax({
          url: "includes/ajax_handlers/poPartLookup.php",
          type: "POST",
          data: ({action:'addPOPart',partname:pnameInput.value,partnumber:pnumberInput.value,partcost:pcostInput.value,taxable:taxable}),
          dataType: "html",
          success: function(response){
              response=response.split("|");
              if($.trim(response[0])=='success')
              {
                pnameInput.value='';
                pnumberInput.value='';
                pcostInput.value='0.00';
                ptaxableInput.checked=false;
                 var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'Success:',
                        buttons:[
                        {
                            text: 'Close',
                            click: function() { 
                                $(this).dialog('close');
                            }
                        }]
                    })
              } else {
                 var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'An error occurred:',
                        buttons:[
                        {
                            text: 'Close',
                            click: function() { 
                                $(this).dialog('close');
                            }
                        }]
                    })
              }
          },
           error:function (xhr, ajaxOptions, thrownError){
               var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'An error occurred:',
                        buttons:[
                        {
                            text: 'Close',
                            click: function() { 
                                $(this).dialog('close');
                            }
                        }]
                    })
           }
         });
        
        
    } else {
        var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>You must enter at least a part name.</p>')
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'An error occurred:',
                        buttons:[
                        {
                            text: 'Close',
                            click: function() { 
                                $(this).dialog('close');
                            }
                        }]
                    })
    }
}

function receiveInventoryItem(pid,poid)
{
    var received=document.getElementById('received_'+pid).value
    var ordered=document.getElementById('qty_'+pid).value
    
    $.ajax({
          url: "includes/ajax_handlers/poPartLookup.php",
          type: "POST",
          
          data: ({action:'receivepoitem',partid:pid,poid:poid,received:received,ordered:ordered}),
          dataType: "html",
          success: function(response){
              response=response.split("|");
              if($.trim(response[0])=='success')
              {
                 document.getElementById('ok_'+pid).style.display="block";
              } else {
                 document.getElementById('error_'+pid).style.display="block";
                 var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'An error occurred:',
                        buttons:[
                        {
                            text: 'Close',
                            click: function() { 
                                $(this).dialog('close');
                            }
                        }]
                    })
              }
          },
           error:function (xhr, ajaxOptions, thrownError){
               var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'An error occurred:',
                        buttons:[
                        {
                            text: 'Close',
                            click: function() { 
                                $(this).dialog('close');
                            }
                        }]
                    })
           }
    });

}

function addInventoryItem(type)
{
    if (type=='name')
    {
        var pid=document.getElementById('spartname_ID').value;
        document.getElementById('spartname').value='';
        document.getElementById('spartname_ID').value='';
    }else if(type=='number')
    {
        var pid=document.getElementById('spartnumber_ID').value;
        document.getElementById('spartnumber').value='';
        document.getElementById('spartnumber_ID').value='';
    } else if(type='service')
    {
        var pid=document.getElementById('sservicename_ID').value;
        document.getElementById('sservicename').value='';
        document.getElementById('sservicename_ID').value='';
    }
    if (pid!='' && pid!='0')
    {
        $.ajax({
          url: "includes/ajax_handlers/poPartLookup.php",
          type: "POST",
          data: ({action:'addpart',type:type,partid:pid}),
          dataType: "html",
          success: function(response){
              response=response.split("|");
              if($.trim(response[0])=='success')
              {
                    $('#poitems').append(response[1]);
                    calculatePOLine(pid)
              } else {
                 var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'An error occurred:',
                        buttons:[
                        {
                            text: 'Close',
                            click: function() { 
                                $(this).dialog('close');
                            }
                        }]
                    })
              }
          },
           error:function (xhr, ajaxOptions, thrownError){
               var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'An error occurred:',
                        buttons:[
                        {
                            text: 'Close',
                            click: function() { 
                                $(this).dialog('close');
                            }
                        }]
                    })
           }
         });
        
        
        
    }
    
}


function calculatePOLine(pid)
{
    var qtyitem='qty_'+pid;
    var costitem='unit_'+pid;
    var lineitem='linetotal_'+pid;
    var qty=Number(document.getElementById(qtyitem).value);
    var costHolder=document.getElementById(costitem);
    var cost=Number(costHolder.value);
    var linecost=Number(cost*qty);
    linecost=linecost.toFixed(2);
    document.getElementById(lineitem).value=linecost;
    cost=cost.toFixed(2);
    costHolder.value=cost;
    
    calculatePO();
}

function deleteInventoryItem(itemID)
{
    var divHolder=document.getElementById("poitems");
    var poitem=document.getElementById('lineitem_'+itemID);   
    divHolder.removeChild(poitem);
    calculatePO();
}

function calculatePO()
{
    var pids=document.getElementById('pids');
    var pidtext='';
    var lines=getElementsByClass('polinetotal');
    var line=new Array();
    var subtotalHolder=document.getElementById('subtotal');
    var taxHolder=document.getElementById('tax');
    var totalHolder=document.getElementById('total');
    var shippingHolder=document.getElementById('shipping');
    var subtotal=0;
    var total=0;
    var tax=0;
    var currentitem=0;
    var curid=0;
    if (shippingHolder.value!='')
    {
        var shipping=Number(shippingHolder.value);
    } else {
        var shipping=0;
    }
    for(i=0;i<lines.length;i++) {
        line=lines[i];
        pidtext=line['id'].split("_");
        curid=pidtext[1];
        currentitem=Number(document.getElementById('linetotal_'+curid).value);
        if (document.getElementById('taxable_'+curid)=='1')
        {
            tax=tax+currentitem*taxRate;
            tax=Number(tax);
            tax=tax.toFixed(2);
        }
        subtotal=subtotal+currentitem;
    }
    total=subtotal+tax+shipping;
    total=Number(total);
    total=total.toFixed(2);
    subtotal=Number(subtotal);
    subtotal=subtotal.toFixed(2);
    subtotalHolder.value=subtotal;
    taxHolder.value=tax;
    totalHolder.value=total;
}

function getElementsByClass(searchClass,node,tag) {
    var classElements = new Array();
    if ( node == null )
        node = document;
    if ( tag == null )
        tag = '*';
    var els = node.getElementsByTagName(tag);
    var elsLen = els.length;
    var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
    for (i = 0, j = 0; i < elsLen; i++) {
        if ( pattern.test(els[i].className) ) {
            classElements[j] = els[i];
            j++;
        }
    }
    return classElements;
}

function updatePressDraw()
{
    var dbtn=document.getElementById('updatedraw');
    var drawfield=document.getElementById('pressdraw');
    var jobid=document.getElementById('jobid').value;
    if (dbtn.value=='Edit')
    {
        dbtn.value='Save';
        drawfield.readOnly=false;
    } else {
         $.ajax({
          url: "includes/ajax_handlers/jobmonitorPress.php",
          type: "POST",
          data: ({type:'updatedraw',draw:drawfield.value,jobid:jobid}),
          dataType: "html",
          success: function(response){
              response=response.split("|");
              if($.trim(response[0])=='success')
              {
                 drawfield.readOnly=true;
                 dbtn.value='Edit';
              } else {
                 var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'An error occurred:',
                        buttons:[
                        {
                            text: 'Close',
                            click: function() { 
                                $(this).dialog('close');
                            }
                        }]
                    })
              }
          },
           error:function (xhr, ajaxOptions, thrownError){
               var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                    .dialog({
                        autoOpen: true,
                        modal: true,
                        title: 'An error occurred:',
                        buttons:[
                        {
                            text: 'Close',
                            click: function() { 
                                $(this).dialog('close');
                            }
                        }]
                    })
           }
         });
    }
}

function pressMaintenanceGeneric()
{
    var equipmentid=$("#equipmentid").val();
    var componentid=$("#componentid").val();
    window.location="?type=generic&equipmentid="+equipmentid+"&componentid="+componentid;
}

function checkForSystemAlerts()
{
    $.ajax({
      url: "includes/ajax_handlers/checkForSystemAlerts.php?cb_="+Math.random(),
      type: "GET",
      data: ({action:'get'}),
      dataType: "html",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
              $.ctNotify($.trim(response[1]),{type: 'warning', isSticky: true, delay: 5000},'left-bottom')
              /*
              $('body').showMessage({
                thisMessage:      $.trim(response[1]),
                className:        'fail',
                position:        'top',
                opacity:        90,
                useEsc:            true,
                displayNavigation: true,
                autoClose:         false,
                delayTime:         0,
                closeText:         'close',
                escText:      'Esc Key or'
                });
                */
          }
      }
      })
              
}

function clearSystemAlerts(id)
{
     $.ajax({
      url: "includes/ajax_handlers/checkForSystemAlerts.php",
      type: "GET",
      data: ({action:'clear',id:id}),
      dataType: "html",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
              $('#systemalert_'+id).hide();
          }
      }
})
} 

function checkForFile(checkfield,msgfield,path)
{
   var file=$('#'+checkfield).val();
   file=file.split("?");
   file=file[0];
   if (path=='core')
   {
        path='/';   
   } else if(path=='script') {
       path='/includes/js_libraries/';
   } else if(path=='style') {
       path='/styles/';
   } else if(path=='include') {
       path='/includes/';
   } else if(path=='handlers') {
       path='/includes/ajax_handlers';
   }
   if(file!='')
   {
        $.ajax({
          url: "includes/ajax_handlers/generalAjax.php",
          type: "POST",
          data: ({action:'checkforfile',filename:file,path:path}),
          dataType: "html",
          success: function(response){
            if($.trim(response)=='true')
            {
                $('#'+msgfield).css({'color':'green'});
                $('#'+msgfield).html('File exists');
            } else {
                $('#'+msgfield).css({'color':'red'});
                $('#'+msgfield).html('File was not found');
            }
          }
              
        });
   }
}

function changePartInventory(type,partid)
{
    $.ajax({
      url: "includes/ajax_handlers/changePartInventory.php",
      type: "POST",
      data: ({type:type,partid:partid}),
      dataType: "json",
      success: function(response){
          if(response['status']=='success')
          {
             $('#invcount_'+partid).html(response['count']);
          } else {
             showMessage('error',response['message']);
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
           showMessage('error',xhr.status+'<br />'+thrownError);
       }
     });
}

function showMessage(type,message)
{
     var $dialog = $('<div id="jConfirm"></div>')
        .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+message+'</p>')
        .dialog({
            autoOpen: true,
            modal: true,
            title: 'An error occurred:',
            buttons:[
            {
                text: 'Close',
                click: function() { 
                    $(this).dialog('close');
                }
            }]
        })
}

function createUsername()
{
    var firstname=document.getElementById('firstname');
    var lastname=document.getElementById('lastname');
    var username=document.getElementById('username');
    if (username.value=='')
    {
        var b='';
        var finitial=firstname.value.substr(0,1);
        var lname=lastname.value;
        b=finitial+lname;
        b=b.toLowerCase();
        username.value=b;    
    }   
}

function addAlert()
{
    var pubid=$('#pub_id').val();
    var userid=$('#userid').val();
    var type=$('#alerttype').val();
    if(type=='press')
    {
        var runid=$('#pressrun_id').val();
    } else {
        var runid=$('#insertrun_id').val();
    }
    
    $.ajax({
      url: "includes/ajax_handlers/userAlerts.php",
      type: "POST",
      data: ({action:'add',pubid:pubid,runid:runid,userid:userid,type:type}),
      dataType: "html",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
             $('#alerts').append(response[1]);
             $('#alerttype').val('press');
             $('#pub_id').val('0');
             $('#pressrun_id').val('0');
             $('#insertrun_id').val('0');
          } else {
             showMessage('error',response[1]);
             
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
          showMessage('error',xhr.status+'<br />'+thrownError);
       }
     });
    
}
function deleteAlert(alertid)
{
    $.ajax({
      url: "includes/ajax_handlers/userAlerts.php",
      type: "POST",
      data: ({action:'delete',alertid:alertid}),
      dataType: "html",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
             $('#alert'+alertid).remove();
          } else {
             showMessage('error',response[1]);
             
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
           showMessage('error',xhr.status+'<br />'+thrownError);
       }
     });
}


function addPartVendor()
{
    var partid=$('#partid').val();
    var vendorid=$('#partvendor').val();
    var number=$('#partnumber').val();
    var cost=$('#partcost').val();
    if(vendorid==0)
    {
       showMessage('error','You need to select at least a vendor before saving.');
       
    } else {
    $.ajax({
      url: "includes/ajax_handlers/partVendors.php",
      type: "POST",
      data: ({action:'add',partid:partid,vendorid:vendorid,number:number,cost:cost}),
      dataType: "html",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
             $('#vendors').append(response[1]);
             $('#partvendor').val('0');
             $('#partnumber').val('');
             $('#partcost').val('');
          } else {
             showMessage('error',response[1]);
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
          showMessage('error',xhr.status+'<br />'+thrownError);
       }
     });
    }
}

function updatePartVendor(partvendorid)
{
    var number=$('#part_number_'+partvendorid).val();
    var cost=$('#part_cost_'+partvendorid).val();
    
    $.ajax({
      url: "includes/ajax_handlers/partVendors.php",
      type: "POST",
      data: ({action:'edit',partvendorid:partvendorid,number:number,cost:cost}),
      dataType: "html",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
              var options={};
              $('#update_'+partvendorid).css({'display':'block'});
              $('#update_'+partvendorid).effect( 'pulsate', options, 200, function(){$('#update_'+partvendorid).css({'display':'none'})});
          } else {
            showMessage('error',response[1]);
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
           showMessage('error',xhr.status+'<br />'+thrownError);
       }
     });
    
}
function deletePartVendor(vendorid)
{
    $.ajax({
      url: "includes/ajax_handlers/partVendors.php",
      type: "POST",
      data: ({action:'delete',vendorid:vendorid}),
      dataType: "html",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
             $('#vendor_'+vendorid).remove();
          } else {
             showMessage('error',response[1]);
          }
      },
       error:function (xhr, ajaxOptions, thrownError){
          showMessage('error',xhr.status+'<br />'+thrownError);
       }
     });
}

//this function copies data in the "monday" publication run page flow target boxes to the other days of the week
function copyPlateTimeTargets()
{
    var i=1;
    for(i=2;i<=7;i++)
    {
        $('#schedulelead_'+i).val($('#schedulelead_1').val());
        $('#lastcolor_'+i).val($('#lastcolor_1').val());
        $('#lastpage_'+i).val($('#lastpage_1').val());
        $('#lastplate_'+i).val($('#lastplate_1').val());
        $('#last2plate_'+i).val($('#last2plate_1').val());
        $('#last3plate_'+i).val($('#last3plate_1').val());
        $('#last4plate_'+i).val($('#last4plate_1').val());
        $('#last5plate_'+i).val($('#last5plate_1').val());
        $('#last6plate_'+i).val($('#last6plate_1').val());
        $('#chaseplate_'+i).val($('#chaseplate_1').val());
        $('#chasestart_'+i).val($('#chasestart_1').val());
        $('#runlength_'+i).val($('#runlength_1').val());
    }
}
function addslashes(str) {
    if(str==''){return str;}
    str=str.replace(/\\/g,'\\\\');
    str=str.replace(/\'/g,'\\\'');
    str=str.replace(/\"/g,'\\"');
    str=str.replace(/\0/g,'\\0');
    return str;
}
function stripslashes(str) {
    if(str==''){return str;}
    str=str.replace(/\\'/g,'\'');
    str=str.replace(/\\"/g,'"');
    str=str.replace(/\\0/g,'\0');
    str=str.replace(/\\\\/g,'\\');
    return str;
}

function noPerms(actionType)
{
   var message = "We apologize, but you do not have permission to "+actionType+" a job on the calendar.";
   var title = "Permission Denied";
   alertMessage(title,message,'error')  
}

function clearAlert(name)
{
    $.ajax({
      url: "includes/ajax_handlers/alertHandler.php",
      type: "POST",
      data: ({name:name}),
      dataType: "html",
      success: function(response){
        $('#'+name).remove();    
      }
    })
}

function saveMonitorNotes(source)
{
    $.ajax({
      url: "includes/ajax_handlers/monitorNotes.php?cb_="+Math.random(),
      type: "POST",
      data: ({action:'notes',notes:$('#'+source+"notes").val(),jobid:$('#jobid').val(),mode:source}),
      dataType: "json",
      success: function(response){
          if(response.status=='error')
          {
              $.ctNotify($.trim(response.message),{type: 'warning', isSticky: true, delay: 5000},'left-bottom')
          }
      }
    })        
}


function removeLayout(type)
{
    if(type=='recurring')
    {
        var jobid=$('#recurringid').val();
    } else {
        var jobid=$('#job_id').val();
    }
    
    var $dialog = $('<div id="jConfirm"></div>')
    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>If you remove the layout, all associated pages and plates will also be removed. Are you sure you want to proceed?</p>')
    .dialog({
        autoOpen: true,
        title: 'Remove Layout?',
        modal: true,
        buttons: {
            Cancel: function() {
                $( this ).dialog( "close" );
                return false;
            },
            'Delete': function() {
                $( this ).dialog( "close" );
                $.ajax({
                  url: 'includes/ajax_handlers/fetchMatchingLayouts.php',
                  type: 'post',
                  dataType: 'json',
                  data: ({jobid:jobid,type:type}),
                  success: function(response)
                  {
                      if(response.status=='success')
                      {
                          $('#layouts').val(0);
                          $('#layout_id').val(0);
                          $('#layout_preview').html('');
                          $('#laymessage').html('Layout, plates and pages have been succesfully removed.');
                      } else {
                          $('#laymessage').css('color','red');
                          $('#laymessage').html('There was a problem removing the layout.<br />'+response.message);
                      }     
                  },
                  error:function (xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                    alert(thrownError);
                  }
                });
            }
            
        },
        open: function() {
            $('.ui-dialog-buttonpane > button:last').focus();
        }
   
    });
    return false;

    
}

function genericAjaxHandler(script,dataObj,callback)
{
   if(script!='')
   {
       $.ajax({
          url: "includes/ajax_handlers/"+script,
          type: "POST",
          data: dataObj,
          dataType: 'json',
          success: function(response){
              if(typeof callback !== 'undefined') callback(response);
          },
          error: function (xhr, desc, er) {
              bootbox.alert({
                  title: "An error occurred",
                  message: xhr.status+'<br />'+desc 
              });
              var badresponse = {status: 404};
              return false
          }
       });
   
   }
}

function saveSelectOption(element_name,handler)
{
    var dataObj = {newValue: $('#'+element_name+"_custom").val(), id: element_name};
    genericAjaxHandler(handler,dataObj,updateSelectOptions);
}

function updateSelectOptions(data)
{
    console.log(data);
    var selectID = data.id;
    var optionID = data.option_id;
    var optionVal = data.option_value;
    $('#'+selectID+"_custom").val('');
    $('#'+selectID+"_selectCustomOptionModal").modal('hide');
    $('#'+selectID).prepend($('<option>', {value:optionID, text:optionVal}));
    $('#'+selectID).val( optionID );//To select the new option
}

function heartbeat()
{
    $.ajax({
      url: "/includes/ajax_handlers/heartbeat.php",
      type: "POST",
      data: {current_id: $('#userMessages').data('latest')},
      dataType: 'json',
      success: function(response){
          if(response.messages.length > 0)
          {
              var latestID = 0;
              jQuery.each(response.messages, function (j,message){
              
                  var newMess = "<li><div style='width:100%;padding:4px;margin: 10px 4px;background-color: #efefef;border-radius:5px;border: 1px solid black;'>";
                  
                  if(message.title!='')
                   {
                       newMess+="<h4>"+message.title+"</h4>";
                   }
                   
                   newMess+="<p><small>From "+message.sender+" at "+message.sent_time+"</small></p>";
                   newMess+= "<p>"+message.message+"</p>";
                   newMess+= "</div>\n</li>";
                   $('#userMessages').append(newMess);
                   
                   if(parseInt(message.id)>latestID){latestID = parseInt(message.id)}
              })
              $('#userMessages').data('latest',latestID);
          }
          
          var cTime = moment().format("D/M H:mm");
          
          addAjaxDebugMessage(cTime, 'heartbeat',  $('#userMessages').data('latest'), response);
      }
    });
    console.log('Fired heartbeat...');
}       
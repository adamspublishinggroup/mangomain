$(document).ready(function() {
    var currentJobID=0;
        $('#calendar').fullCalendar({
        
            defaultView: 'agendaWeek',
            allDaySlot: false,
            firstHour: calendarStartPress,
            slotMinutes: calendarPressSlots,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            theme: true,
            editable: true,
            height: 700,
            
            events: "includes/calendarEventsFetch.php",
            
            eventDrop: function(event,dayDelta,minuteDelta,revertFunc) {
                $.ajax({
                       url: 'includes/calendarEventsUpdate.php',
                       type: "POST",
                       data: "type=move&jobid="+event.id+"&dayDelta="+dayDelta+"&minuteDelta="+minuteDelta,
                       error: function()
                       {
                           revertFunc();
                       },
                       success: function(res) {
                           if(res=='')
                           {
                              //don't do anything to annoy the user with a dialog or somethign :)
                           } else {
                              alert("Update failed\n"+res);
                              revertFunc(); 
                           }
                       }
                    });
            },
            eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
                $.ajax({
                   url: 'includes/calendarEventsUpdate.php',
                   type: "POST",
                   data: "type=resize&jobid="+event.id+"&dayDelta="+dayDelta+"&minuteDelta="+minuteDelta,
                   error: function()
                   {
                       revertFunc();
                   },
                   success: function(res) {
                       if(res=='')
                       {
                          //don't do anything to annoy the user with a dialog or somethign :) 
                       } else {
                          alert("Update failed\n"+res);
                          revertFunc(); 
                       }
                   }
                });
                

            },
            
            loading: function(bool) {
                if (bool) $('#loading').show();
                else $('#loading').hide();
            },
            dblclick: function(event, jsEvent, view) {
                /*
                alert('You doubleclicked! Event: ' + event.title+' and id='+event.id);
                alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                alert('View: ' + view.name);
                */
                window.open('jobPressPopup.php?id='+event.id,'Press Job Editor',"scrollbars=0, resizeable=1, width=940, height=850");
            },
            eventClick: function(event, jsEvent, view) {
                currentJobID=event.id;
                return false;
            },
            
            dayClick: function(date, allDay, jsEvent, view) {

                /*
                if (allDay) {
                    alert('Clicked on the entire day: ' + date);
                }else{
                    alert('Clicked on the slot: ' + date);
                }

                alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

                alert('Current view: ' + view.name);
                */
                // change the day's background color just for fun
               if(view.name=='month')
               {
                   $('#calendar').fullCalendar(
                       'changeView','agendaDay' 
                   );
                    $('#calendar').fullCalendar(
                       'gotoDate', date 
                   );
                    
               } else {
                   var a = this; 
                   var $dialog = $('<div id="jConfirm"></div>')
                    .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This will create a new press job. Are you sure?</p>')
                    .dialog({
                        autoOpen: false,
                        title: 'Are you sure you want to create a new job??',
                        modal: true,
                        buttons: {
                            Cancel: function() {
                                $( this ).dialog( "close" );
                                return false;
                            },
                            'Create Job': function() {
                                $( this ).dialog( "close" );
                                $.ajax({
                           url: 'includes/calendarEventsUpdate.php',
                           type: "POST",
                           data: "type=add&jobid=0&dayDelta=0&minuteDelta=0&date="+date,
                           success: function(res) {
                               res=res.split("|");
                               if(res[0]=='success')
                               {
                                  window.open('jobPressPopup.php?id='+res[1],'Press Job Editor',"scrollbars=0, resizeable=1, width=940, height=850");
                                  
                               } else {
                                  alertMessage("Job creation failed<br />"+res[1],'error');
                               }
                           }
                        });
                            }
                        },
                        open: function() {
                            $('.ui-dialog-buttonpane > button:last').focus();
                        }
                   
                    });
                    $dialog.dialog("open");
               }
                /*
                
                
                if (confirm("Are you sure you want to create a new job?"))
                {
                    $.ajax({
                       url: 'includes/calendarEventsUpdate.php',
                       type: "POST",
                       data: "type=add&jobid=0&dayDelta=0&minuteDelta=0&date="+date,
                       success: function(res) {
                           res=res.split("|");
                           if(res[0]=='success')
                           {
                              window.open('jobPressPopup.php?id='+res[1],'Press Job Editor',"scrollbars=0, resizeable=1, width=900, height=850");
                              
                           } else {
                              alert("Job creation failed\n"+res[1]);
                           }
                       }
                    });
                }
                */
            },

            eventRender: function(event, element) {
                element.find('.fc-event-time').prepend('F-'+event.folder+' '+event.tags+"<span id='details"+event.id+"' style='float:right;'><img src='/artwork/icons/magnifying-glass.png' border=0 height=20 /></span><br />"),
                element.find('.fc-event-title').append("<br/>" + event.description),
                //element.find('.fc-event-bg').append("<br/>" + event.description),
                
                $('#details'+event.id).qtip({
                    content: {
                        text: event.fulldetails,
                        title: {
                          text: 'Job Details',
                          button: '<span onclick="return false;">Close</span>'
                        }
                    }, 
                    position: {
                            target: $('#details'+event.id),
                            my: 'left center',
                            at: 'right center'
                        },
                    style: {
                        classes: 'ui-tooltip-shadow', // Optional shadow...
                        tip: 'left center', // Tips work nicely with the styles too!
                        widget: true
                    },
                    show: {
                        event: 'click',
                        solo: true // Only show one tooltip at a time
                    },
                    hide: 'unfocus'
                }),
                
                element.contextMenu('jobCmenu',{
                    'Print Job Ticket': {
                        click: function(element){ // element is the jquery obj clicked on when context menu launched
                            window.open('jobPressTicket.php?action=print&jobid='+event.id,'Press Job Ticket',"scrollbars=0, resizeable=1, width=750, height=640");
                        }
                    },
                    'Edit Job': {
                        click: function(element){ // element is the jquery obj clicked on when context menu launched
                            window.open('jobPressPopup.php?id='+event.id,'Press Job Editor',"scrollbars=0, resizeable=1, width=900, height=850");
                        }
                    },
                    'Edit Recurrence': {
                        click: function(element){ // element is the jquery obj clicked on when context menu launched
                            window.open('jobRecurring.php?id='+event.id,'Recurring Job Editor',"scrollbars=0, resizeable=1, width=750, height=640");
                        }
                    },
                    'View in Press Monitor': {
                        click: function(element){ // element is the jquery obj clicked on when context menu launched
                            window.location='jobMonitor_press.php?jobid='+event.id;
                        }
                    },
                    'View in Pagination Monitor': {
                        click: function(element){ // element is the jquery obj clicked on when context menu launched
                            window.location='jobMonitor_pagination.php?jobid='+event.id;
                        }
                    },
                    'View in Plateroom Monitor': {
                        click: function(element){ // element is the jquery obj clicked on when context menu launched
                            window.location='jobMonitor_plateroom.php?jobid='+event.id;
                        }
                    },
                    'Delete Job': {
                       click: function(element){ // element is the jquery obj clicked on when context menu launched
                            var a = this; 
                               var $dialog = $('<div id="jConfirm"></div>')
                                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>This item will be permanently deleted and cannot be recovered. Are you sure?</p>')
                                .dialog({
                                    autoOpen: true,
                                    title: 'Are you sure you want to Delete?',
                                    modal: true,
                                    buttons: {
                                        Cancel: function() {
                                            $( this ).dialog( "close" );
                                            return false;
                                        },
                                        'Delete': function() {
                                            $( this ).dialog( "close" );
                                            $.ajax({
                                           url: 'includes/calendarEventsUpdate.php',
                                           type: "POST",
                                           data: "type=delete&jobid="+event.id+"&dayDelta=0&minuteDelta=0",
                                           success: function(res) {
                                               if(res=='')
                                               {
                                                  $('#calendar').fullCalendar( 'removeEvents',event.id );
                                               } else {
                                                  alertMessage("Job deletion failed<br />"+res,'error');
                                               }
                                           }
                                           });
                                        }
                                    },
                                    open: function() {
                                        $('.ui-dialog-buttonpane > button:last').focus();
                                    }
                               
                                });
                                a.dialog("open");
                               
                        }
                    }
                });
                
            }
                        
        });
        
    });

function refreshCalendar()
{
   $('#calendar').fullCalendar('refetchEvents');
}
function jumpDate(cdate)
{
    var temp=cdate.split("-");
    var year=temp[0];
    var month=temp[1];
    var date=temp[2];
    $('#calendar').fullCalendar('gotoDate', year, month, date);
    
}

function isNumberKey(evt)
{
    var charCode = (evt.which) ? evt.which : event.keyCode
    //allow decimal and dollar sign
    //alert (charCode);
    if (charCode==46 || charCode==36){return true;}
    if (charCode>=96 && charCode<=105){return true;}
    if (charCode > 31 && (charCode < 48 || charCode > 57))
    return false;

    return true;
}
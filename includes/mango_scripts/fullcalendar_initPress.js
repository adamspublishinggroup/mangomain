$(document).ready(function() {
    if(calendarSchedulePress)
    {
        var disableDrag=false;
    } else {
        var disableDrag=true;
    }
    var currentJobID=0;
        $('#calendar').fullCalendar({
        
            defaultView: 'agendaWeek',
            allDaySlot: false,
            firstHour: calendarStartPress,
            slotMinutes: calendarPressSlots,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            overlap: false,
            theme: true,
            editable: true,
            height: 700,
            disableDragging: disableDrag,
            dataType: 'json', 
            events: "includes/ajax_handlers/fetchCalendarPress.php",
            eventDrop: function(event,dayDelta,minuteDelta,revertFunc) {
                if(calendarSchedulePress && event.mypub)
                { 
                    if(event.eventtype=='maintenance')
                    {
                        $.ajax({
                           url: 'includes/ajax_handlers/maintenanceScheduledTicketHandler.php',
                           type: "POST",
                           data: {type:'move',scheduleid:event.id,dayDelta:dayDelta,minuteDelta:minuteDelta},
                           dataType: 'json',
                           error: function()
                           {
                               revertFunc();
                           },
                           success: function(response) {
                               if(response.status=='success')
                               {
                                  //don't do anything to annoy the user with a dialog or somethign :) 
                               } else {
                                  alert("Update failed\n"+response.message);
                                  revertFunc(); 
                               }
                           }
                        });    
                    } else {
                        $.ajax({
                           url: 'includes/ajax_handlers/updateCalendarPress.php',
                           type: "POST",
                           data: {type:'move',jobid:event.id,dayDelta:dayDelta,minuteDelta:minuteDelta},
                           dataType: 'json',
                           error: function()
                           {
                               revertFunc();
                           },
                           success: function(response) {
                               if(response.status=='success')
                               {
                                  //don't do anything to annoy the user with a dialog or somethign :) 
                               } else {
                                  alert("Update failed\n"+response.message);
                                  revertFunc(); 
                               }
                           }
                        });
                    }
                }
            },
            eventResize: function(event,dayDelta,minuteDelta,revertFunc) {
                if(calendarSchedulePress && event.mypub)
                { 
                    if(event.eventtype=='maintenance')
                    {
                        $.ajax({
                           url: 'includes/ajax_handlers/maintenanceScheduledTicketHandler.php',
                           type: "POST",
                           data: {type:'resize',scheduleid:event.id,dayDelta:dayDelta,minuteDelta:minuteDelta},
                           dataType: 'json',
                           error: function()
                           {
                               revertFunc();
                           },
                           success: function(response) {
                               if(response.status=='success')
                               {
                                  //don't do anything to annoy the user with a dialog or somethign :) 
                               } else {
                                  alert("Resize failed\n"+response.message);
                                  revertFunc(); 
                               }
                           }
                        });    
                    } else {
                        $.ajax({
                           url: 'includes/ajax_handlers/updateCalendarPress.php',
                           type: "POST",
                           data: {type:'resize',jobid:event.id,dayDelta:dayDelta,minuteDelta:minuteDelta},
                           dataType: 'json',
                           error: function()
                           {
                               revertFunc();
                           },
                           success: function(response) {
                               if(response.status=='success')
                               {
                                  //don't do anything to annoy the user with a dialog or somethign :) 
                               } else {
                                  alert("Update failed\n"+response.message);
                                  revertFunc(); 
                               }
                           }
                        });
                    }
                }
            },
            
            loading: function(bool) {
                if (bool) $('#loading').show();
                else $('#loading').hide();
            },
            dblclick: function(event, jsEvent) {
                /*
                alert('You doubleclicked! Event: ' + event.title+' and id='+event.id);
                alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                alert('View: ' + view.name);
                */
                if(calendarSchedulePress && event.mypub) {
                    if(event.eventtype=='maintenance')
                    {
                        window.open('maintenanceSchedulePopup.php?popup=true&ticketid='+event.id,'Scheduled Maintenance',"scrollbars=0, resizeable=1, width=740, height=750");
                    } else {
                        window.open('jobPressPopup.php?id='+event.id,'Press Job Editor',"scrollbars=1, resizeable=1, width=940, height=850");
                    }
                } else {
                    if(event.eventtype=='maintenance')
                    {
                        window.open('maintenanceSchedulePopup.php?popup=true&ne=true&ticketid='+event.id,'Scheduled Maintenance',"scrollbars=0, resizeable=1, width=740, height=750");
                    } else {
                        window.open('jobPressPopup.php?id='+event.id+'&ne=true','Press Job Editor',"scrollbars=1, resizeable=1, width=940, height=850");
                    }
                }
            },
            eventClick: function(event, jsEvent, view) {
                currentJobID=event.id;
                return false;
                
                //sample of coloring an event after click
                //event.backgroundColor = 'yellow';
                //$('#calendar').fullCalendar('rerenderEvents');
                
            },
            eventMouseover: function( event, jsEvent, view ) { 
                $(this).css('border-color','red');
                
            },
            eventMouseout: function( event, jsEvent, view ) { 
                $(this).css('border-color',event.borderColor);
            },
            
            dayClick: function(date, allDay, jsEvent, view) {

               if(view.name=='month')
               {
                   $('#calendar').fullCalendar(
                       'changeView','agendaDay' 
                   );
                    $('#calendar').fullCalendar(
                       'gotoDate', date 
                   );
                    
               } else {
                   if(calendarSchedulePress)
                   {
                       var jobTitle = "Are you sure you want to create a new job?";
                       var jobMessage = "This will create a new press job. Are you sure?";
                       var jobButtons = {
                                confirm: {
                                    label: 'Yes',
                                    className: 'btn-primary'
                                },
                                cancel: {
                                    label: 'No',
                                    className: 'btn-default'
                                }
                            };
                       
                       if (minPressCalendarJobAddDate > date && overrideLockouts != true)
                       {
                           //means we are trying to schedule a job earlier than earliest allowed
                           var showDate=String(minPressCalendarJobAddDate).split(" GMT");
                           jobTitle='Scheduled date/time not allowed';
                           jobButtons = {
                                Cancel: function() {
                                    $( this ).dialog( "close" );
                                    return false;
                                }
                           }
                           jobMessage="You can only schedule jobs after "+showDate[0]+". Please contact the production coordinator to schedule this press run.";
                       }
                       
                       bootbox.confirm({
                           title: jobTitle,
                           message: jobMessage,
                            buttons: jobButtons,
                            callback: function (result) {
                                if(result)
                                {
                                    $.ajax({
                                       url: 'includes/ajax_handlers/updateCalendarPress.php',
                                       type: "POST",
                                       data: {type:'add',jobid:0,dayDelta:0,minuteDelta:0,date:date},
                                       dataType: 'json',
                                       success: function(response) {
                                           if(response.status=='success')
                                           {
                                              window.open('jobPressPopup.php?id='+response.jobid,'Press Job Editor',"scrollbars=1, resizeable=1, width=940, height=900");
                                              
                                           } else {
                                              alertMessage("Job creation failed<br />"+response.message,'error');
                                           }
                                       }
                                    });
                                }
                            }
                      });
                       
                   } else {
                       noPerms('add');
                   }
               } 
                
            },

            eventRender: function(event, element) {
                if(event.eventtype=='maintenance')
                {
                    element.find('.fc-event-title').append("<br/>" + event.description);
                } else {
                    element.find('.fc-event-time').prepend('F-'+event.folder+' '+event.tags+"<span id='details"+event.id+"' style='float:right;'><i class='fa fa-search'></i></span><br />"),
                    element.find('.fc-event-title').append("<br/>" + event.description),
                    
                    
                    
                    element.contextMenu('jobCmenu_'+event.id,{
                        'Print Job Ticket': {
                            click: function(element){ // element is the jquery obj clicked on when context menu launched
                                window.open('jobPressTicket.php?action=print&jobid='+event.id,'Press Job Ticket',"scrollbars=0, resizeable=1, width=750, height=640");
                                         
                            }
                        },
                        'Edit Job': {
                            click: function(element){ // element is the jquery obj clicked on when context menu launched
                                if(calendarSchedulePress && event.mypub)
                                {
                                    window.open('jobPressPopup.php?id='+event.id,'Press Job Editor',"scrollbars=0, resizeable=1, width=900, height=850");
                                } else {
                                    window.open('jobPressPopup.php?id='+event.id+'&ne=true','Press Job Editor',"scrollbars=0, resizeable=1, width=900, height=850");
                                }
                            }
                        },
                        'Print Stacker Ticket': {
                            click: function(element){ // element is the jquery obj clicked on when context menu launched
                                window.open('printouts/pressStackerTicket.php?jobid='+event.id,'Press Stacker Ticket',"scrollbars=0, resizeable=1, width=750, height=640");
                                         
                            }
                        },
                        'Edit Recurrence': {
                            click: function(element){ // element is the jquery obj clicked on when context menu launched
                                if(calendarSchedulePress && event.mypub)
                                {
                                   window.open('jobRecurring.php?action=edit&recurringid='+event.recurringid,'Recurring Job Editor',"scrollbars=0, resizeable=1, width=750, height=640");
                                } else {
                                   noPerms('edit the recurrence of');
                                }
                            }
                        },
                        'View in Press Monitor': {
                            click: function(element){ // element is the jquery obj clicked on when context menu launched
                                window.location='jobMonitor_press.php?jobid='+event.id;
                            }
                        },
                        'View in Press Monitor Beta': {
                            click: function(element){ // element is the jquery obj clicked on when context menu launched
                                window.location='jobMonitor_pressV2.php?jobid='+event.id;
                            }
                        },
                        'View in Pagination Monitor': {
                            click: function(element){ // element is the jquery obj clicked on when context menu launched
                                window.location='jobMonitor_pagination.php?jobid='+event.id;
                            }
                        },
                        'View in Plateroom Monitor': {
                            click: function(element){ // element is the jquery obj clicked on when context menu launched
                                window.location='jobMonitor_plate.php?jobid='+event.id;
                            }
                        },
                        'Show Layout': {
                            click: function(element){ // element is the jquery obj clicked on when context menu launched
                                window.open('jobPress.php?action=showlayout&jobid='+event.id);
                            }
                        },
                        'Un-schedule Job': {
                            click: function(element){ // element is the jquery obj clicked on when context menu launched
                                if(calendarSchedulePress && event.mypub)
                                {   
                                   var jobTitle = "Please enter a new requested print date (Format: mm/dd/YYYY?";
                                   var jobButtons = {
                                            confirm: {
                                                label: 'Reschedule',
                                                className: 'btn-primary'
                                            },
                                            cancel: {
                                                label: 'No',
                                                className: 'btn-default'
                                            }
                                        };
                                   bootbox.prompt({
                                       title: jobTitle,
                                       inputType: 'date',
                                       buttons: jobButtons,
                                       callback: function (result) {
                                            if(result)
                                            {
                                                $.ajax({
                                                   url: 'includes/ajax_handlers/updateCalendarPress.php',
                                                   type: "POST",
                                                   data: {type:'unschedule',jobid:event.id,rdate:result},
                                                   dataType: 'json',
                                                   success: function(response) {
                                                       if(response.status=='success')
                                                       {
                                                            $('#calendar').fullCalendar( 'removeEvents',event.id );
                                                            var view = $('#calendar').fullCalendar('getView');
                                                            var d= new Date();
                                                            var curDate = Date.parse(view.start);
                                                            d.setTime(curDate);
                                                            var curMonth = d.getMonth()+1;
                                                            var curDay   = d.getDate();
                                                            var curYear  = d.getFullYear();
                                                            getUnscheduled(curYear,curMonth,curDay);
                                                       } else {
                                                          alertMessage("Job unscheduling failed<br />"+response.status+'<br>'+response.message,'error');
                                                       }
                                                   }
                                               });
                                            }
                                       }
                                  });
                                  
                               } else {
                                   noPerms('edit');
                               }  
                            }
                        },
                        'Delete Job': {
                           click: function(element){ // element is the jquery obj clicked on when context menu launched
                                if(calendarSchedulePress && event.mypub)
                                {   
                                       var jobTitle = "Delete Job";
                                       var jobMessage = "This will permanently delete this job, are you sure you want to proceed?";
                                       var jobButtons = {
                                                confirm: {
                                                    label: 'Yes',
                                                    className: 'btn-darnger'
                                                },
                                                cancel: {
                                                    label: 'No',
                                                    className: 'btn-default'
                                                }
                                            };
                                       
                                      
                                       bootbox.confirm({
                                           title: jobTitle,
                                           message: jobMessage,
                                           buttons: jobButtons,
                                           callback: function (result) {
                                               if(result)
                                               {
                                                   $.ajax({
                                                      url: 'includes/ajax_handlers/updateCalendarPress.php',
                                                      type: "POST",
                                                      data: {type:'delete',jobid:event.id,dayDelta:0,minuteDelta:0},
                                                      dataType: 'json',
                                                      success: function(response) {
                                                          if(response.status=='success')
                                                          {
                                                               $('#calendar').fullCalendar( 'removeEvents',event.id );
                                                               
                                                          } else {
                                                               alertMessage("Job deletion failed<br />"+response.status+'<br>'+response.ql,'error');
                                                          }                                                       }
                                                   });
                                               }
                                           }
                                       });
                               } else {
                                   noPerms('delete');
                                }    
                            }
                        }
                    });
                } 
            },
            droppable: true,
            drop: function(date, allDay) {
                //we'll update the record via ajax, then refresh the calendar...
                var jobid=$(this).attr('id');
                $.ajax({
                   url: 'includes/ajax_handlers/updateCalendarPress.php',
                   type: "POST",
                   data: {type:'drop',jobid:jobid,date:date,},
                   dataType: 'json',
                   success: function(response) {
                       if(response.status=='success')
                       {
                          $('#calendar').fullCalendar('refetchEvents');
                       } else {
                          alertMessage("Job sheduling failed<br />"+response.message,'error');
                       }
                   }
                });
                $(this).remove();
            },
            viewDisplay: function(view) {
                var d= new Date();
                var curDate = Date.parse(view.start);
                d.setTime(curDate);
                var curMonth = d.getMonth()+1;
                var curDay   = d.getDate();
                var curYear  = d.getFullYear();
                getUnscheduled(curYear,curMonth,curDay)
                //console.log('The new title of the view is ' + view.title+' and the start date is '+curMonth+'/'+curDay+'/'+curYear);
            }
                        
        });
        
    });

    
function refreshCalendar()
{
   $('#calendar').fullCalendar('refetchEvents');
}
function jumpDate(cdate)
{
    var temp=cdate.split("-");
    var year=temp[0];
    var month=temp[1];
    var date=temp[2];
    month=parseInt(month)-1;
    $('#calendar').fullCalendar('gotoDate', year, month, date);
    //getUnscheduled(year,month,date);  // this is now integrated into the viewDisplay event in full calendar   
}


function getUnscheduled(year,month,date)
{
    //console.log('getting unscheduled events with date='+month+'/'+date+'/'+year);
    $.ajax({
       url: 'includes/ajax_handlers/updateCalendarPress.php',
       type: "POST",
       data: "type=unscheduled&year="+year+"&month="+month+"&date="+date,
       dataType:'json',
       success: function(response) {
           if(response.status=='success')
           {
               $('.unscheduledHolder').empty();  
               $.each(response.jobs, function (j,job){
                  var newDiv=$('<div id="'+job.id+'"><span id="pop'+job.id+'" style="float:right;"><img src="/artwork/icons/magnifying-glass.png" border=0 height=20 /></span>'+job.title+'</div>').addClass('ui-widget ui-draggable unscheduledJob');
                  var eventObject = {
                        title: job.title // use the element's text as the event title
                  };
                  // store the Event Object in the DOM element so we can get to it later
                  $(newDiv).data('eventObject', eventObject);
                  var dHolder=$('#'+job.dateholder);
                  
                  $('#'+job.dateholder).append(newDiv);  
                  $('#pop'+job.id).qtip({
                        content: {
                            text: job.tooltip,
                            title: 'Job Details',
                            button: true
                        }, 
                        position: {
                                target: $('#pop'+job.id),
                                my: 'left center',
                                at: 'right center'
                            },
                        style: {
                            widget: true, // Optional shadow...
                            def: false,
                            tip: 'left center' // Tips work nicely with the styles too!
                        },
                        show: {
                            event: 'click',
                            solo: true // Only show one tooltip at a time
                        },
                        hide: {
                            event: 'click mouseleave unfocus'
                        }
                    });
                  
                  // make the event draggable using jQuery UI
                  $(newDiv).draggable({
                      zIndex: 999,
                      revert: true,      // will cause the event to go back to its
                      revertDuration: 0  //  original position after the drag
                  });
                    
              });
              
              
           } else {
              alertMessage("Job sheduling failed<br />"+res,'error');
           }
       }
    });
    
}
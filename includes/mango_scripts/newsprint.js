
/***************************************************************
*
* THESE ARE FUNCTIONS FOR NEWSPRINT ORDER ITEMS
*
***************************************************************/
function newsprintOrderItemAdd(orderid)
{
    $.ajax({
      url: "includes/ajax_handlers/newsprintOrderItems.php",
      type: "POST",
      data: ({action:'addorderitem',orderid:orderid}),
      dataType: "json",
      success: function(response){
          if(response.status=='success')
          {
             $('#orderitems').append(response.newItem);   
          }
      }
    })
    return false;
    
}

function newsprintOrderItemDelete(orderitemid,orderid)
{
    bootbox.confirm({
          message: "This item will be permanently deleted and cannot be recovered. Are you sure?",
            buttons: {
                confirm: {
                    label: '<i class="fa fa-trash"></i> Yes',
                    className: 'btn-danger'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-default'
                }
            },
            callback: function (result) {
                if(orderitemid!='new')
                {
                   $.ajax({
                      url: "includes/ajax_handlers/newsprintOrderItems.php",
                      type: "POST",
                      data: ({action:'deleteorderitem',itemid:orderitemid,orderid:orderid}),
                      dataType: "json",
                      success: function(response){
                          if(response.status=='success')
                          {
                             $('#item_'+orderitemid).remove();
                          }
                      },
                       
                   });
                } else {
                    //never saved, just remove the row and close the dialog
                    $('#item_'+orderitemid).remove();
                }  
            }
      });
    
    
    return false;
}


function newsprintOrderItemSave(orderitemid,orderid)
{
    //console.log('orderitemid is '+orderitemid);
    $.ajax({
      url: "includes/ajax_handlers/newsprintOrderItems.php",
      type: "POST",
      data: ({action:'saveorderitem',itemid:orderitemid,orderid:orderid,paper:$('#paper_'+orderitemid).val(),size:$('#size_'+orderitemid).val(),tonnage:$('#tonnage_'+orderitemid).val()}),
      dataType: "json",
      success: function(response){
          if(response.status=='success')
          {
             $('#success_'+orderitemid).css("display","block"); 
             //change the id of the existing block if it's new
             if(orderitemid=='new')
             {
                 //the new order id is returned as item 2 in the array
                 $('#paper_new').attr("id",'#paper_'+response.id);
                 $('#size_new').attr("id",'#paper_'+response.id);
                 $('#tonnage_new').attr("id",'#paper_'+response.id);
                 $('#item_new').attr("id",'#paper_'+response.id);
                 $('#item_new').attr("id",'#paper_'+response.id);
                 $('#save_new').attr("id",'#paper_'+response.id);
                 $('#del_new').attr("id",'#paper_'+response.id);
             }
                
          }
      }
    })
    return false;
}


/*****************************************************************************
*
* THESE SCRIPTS ARE FOR NEWSPRINT BATCH PROCESSING
*
*
******************************************************************************/


function addRoll()
{
    //this function adds a roll and roll holder to the newsprint receive script
    var divHolder=document.getElementById("rolls");
    var lastIDe=document.getElementById('lastroll');
    var lastID=lastIDe.value;
    var newRoll="Roll tag: <input type='text' name='newroll_"+lastID+"' id='newroll_"+lastID+"' value='' size=20 onKeyPress='newsprintKeyCapture(\"newroll_"+lastID+"\",event,false,\"newweight_"+lastID+"\");return false;'/>";
    newRoll=newRoll+" Weight (kg): <input type='text' name='newweight_"+lastID+"' id='newweight_"+lastID+"' value='' size=5 onKeyPress='newsprintKeyCapture(\"newweight_"+lastID+"\",event,true,\"addbusroll\");return false;'/>";
    newRoll=newRoll+" <input type=checkbox name='delete_"+lastID+"' id='delete_"+lastID+"' /> Check to delete";
    newRoll=newRoll+"<br>\n";
    var roll=document.createElement('div');
    roll.id='rolldiv_'+lastID;
    roll.innerHTML=newRoll;
    divHolder.appendChild(roll);
    lastIDe.value=parseInt(lastID)+1;
    var newtext=document.getElementById('newroll_'+lastID);
    newtext.focus();       
}

function addBusinessRoll()
{
    var lastID=$('#lastroll').val();
    var newRoll="Roll tag: <input type='text' name='newroll_"+lastID+"' id='newroll_"+lastID+"' value='' onkeypress='newsprintKeyCapture(\"newroll_"+lastID+"\",event,false,\"addrecroll\");return false;' size=20 /><br>\n";
    var roll=$("<div id='rolldiv_"+lastID+"' style='margin-bottom:4px;'>"+newRoll+"</div>");
    $("#process_rolls").append(roll);
    var newtext=$('#newroll_'+lastID);
    newtext.focus();
    $('#lastroll').val(parseInt(lastID)+1);
}

function addJobRoll()
{
    var divHolder=document.getElementById("rolls");
    var lastIDe=document.getElementById('lastroll');
    var lastID=lastIDe.value;
    var newRoll="Roll tag: <input type='text' name='newroll_"+lastID+"' id='newroll_"+lastID+"' value='' size=20 onBlur='checkRollTag("+lastID+");'/>";
    newRoll=newRoll+" Reel: <input type='text' name='newreel_"+lastID+"' id='newreel_"+lastID+"' value='' size=5 />";
    newRoll=newRoll+" <input type=hidden name='rollid_"+lastID+"' id='rollid_"+lastID+"' value=''/>";
    newRoll=newRoll+" <input type=checkbox name='delete_"+lastID+"' id='delete_"+lastID+"' /> Check to delete";
    newRoll=newRoll+" <input type=checkbox name='butt_"+lastID+"' id='butt_"+lastID+"' /> Check to convert to butt roll";
    newRoll=newRoll+" <span id='msg_"+lastID+"'></span>";
    newRoll=newRoll+"<br>\n";
    var roll=document.createElement('div');
    roll.id='rolldiv_'+lastID;
    roll.innerHTML=newRoll;
    divHolder.appendChild(roll);
    lastIDe.value=parseInt(lastID)+1;   
}

function newsprintKeyCapture(objid,e,checknumber,targetid)
{
    //the purpose of this function is to allow the enter key to 
    //point to the correct button to click.
    var key;
    var badkey=false;
    var obj=document.getElementById(objid);
     if(window.event)
          key = window.event.keyCode;     //IE
     else
          key = e.which;     //firefox
    
    if (checknumber) //we are checking to see if the key is a number
    {
        if (key > 31 && (key < 48 || key > 57))
        {
            badkey=true;
        } else {
            badkey=false;
        }
    }
    if (key == 13)
    {
        if (targetid=='addrecroll')
        {
            addBusinessRoll();
        } else if (targetid=='addbusroll')
        {
            addRoll();
        } else {
            var targetobj=document.getElementById(targetid);
            targetobj.focus();
        }
        //need to att buttonName to the input //Get the button the user wants to have clicked
        //var btn = document.getElementById(buttonName);
        //if (btn != null)
        //{ //If we find the button click it

        //    btn.click();
         //   event.keyCode = 0
        //}
     
    } else if (key==8) 
    {
        //delete pressed
        var objval=obj.value;
        objval = objval.substring(0, objval.length - 1);
        obj.value=objval;
    } else if (key==0)
    {
        //tab pressed
        if (targetid=='addrecroll')
        {
            addBusinessRoll();
        } else if (targetid=='addbusroll')
        {
            addRoll();
        } else {
            obj=document.getElementById(targetid);
            obj.focus();
        }
        
    } else {
        var objval=obj.value;
        if (badkey)
        {
            //dont add the key to the field, since we have a non-number and we checked for it.
        } else {
            obj.value=obj.value+String.fromCharCode(key);
        }
    }
}

function calcTonnage()
{
    //ok, we'll need to figure out which elements we want to count
    var divHolder=$(".ton");
    var tonnage=0;
    var current=0;
    
    for (var i=0; i<divHolder.length; i++){
        var subNode=divHolder[i];
        if (subNode.value!=''){current=parseFloat(subNode.value);}else{current=0;}
        tonnage=parseFloat(tonnage)+current;
    }
    
    var tonHolder=document.getElementById('total_weight');
    tonHolder.value=tonnage;
}



function changeRollManifest(type,id,value)
{
    if(value=='field')
    {
        value=$('#'+type+"_"+id).val();
    }
    $.ajax({
      url: "includes/ajax_handlers/newsprintManifestUpdates.php",
      type: "POST",
      data: ({type:type,id:id,value:value}),
      dataType: "html",
      success: function(response){
          response=response.split("|");
          if($.trim(response[0])=='success')
          {
              $('#'+type+"_"+id).css('backgroundColor','#00FF4D'); 
              
          } else if ($.trim(response[0])=='updated') {
              $("#rollstatus_"+id).html(response[1]);  
              $("#rollbatchdate_"+id).html('Batch date updated');  
          } else {
              //error
              var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+response[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           } 
      },
       error:function (xhr, ajaxOptions, thrownError){
           var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
       }
    })
}



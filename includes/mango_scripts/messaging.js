function alertMessage(title,message,type,cbFunction)
{
    var alertTitle = '<span class="text-success">'+title+'</span>';
    
    if(type=='error')
    {
        alertTitle = '<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> '+title+'</span>';
    }  
    bootbox.alert({
        title: alertTitle,
        message: message,
        callback: function () {
            if(jQuery.isFunction( cbFunction ))
            {
               cbFunction(); 
            }
        }
      });
}

function addAjaxDebugMessage(time, source, post, response)
{
    var messageHolder = $('#debugBarAjaxMessages');
    
    var newRow = "<tr><td>"+time+"</td><td>"+source+"</td><td>"+post+"</td><td>"+response+"</td></tr>";
    
    messageHolder.append(newRow);
}
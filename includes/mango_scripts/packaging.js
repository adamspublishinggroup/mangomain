//this script contains all the necessary functions for the insert packaging system
  
// pre-submit callback 
function showRequest(formData, jqForm, options) { 
    // formData is an array; here we use $.param to convert it to a string to display it 
    // but the form plugin does this for you automatically when it submits the data 
    var queryString = $.param(formData); 
 
    // jqForm is a jQuery object encapsulating the form element.  To access the 
    // DOM element for the form do this: 
    // var formElement = jqForm[0]; 
 
    alert('About to submit: \n\n' + queryString); 
 
    // here we could return false to prevent the form from being submitted; 
    // returning anything other than false will allow the form submit to continue 
    return true; 
} 
 
// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  { 
    // for normal html responses, the first argument to the success callback 
    // is the XMLHttpRequest object's responseText property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'xml' then the first argument to the success callback 
    // is the XMLHttpRequest object's responseXML property 
 
    // if the ajaxForm method was passed an Options Object with the dataType 
    // property set to 'json' then the first argument to the success callback 
    // is the json data object returned by the server 
 
    alert('status: ' + statusText + '\n\nresponseText: \n' + responseText + 
        '\n\nThe output div should have already been updated with the responseText.'); 
} 


/*new as of 11/27/10 */
function getInsertRuns()
{
    //alert('function called got pubid of '+$("#pub_id").val());
    var pubid=$("#pub_id").val();
    $.ajax({
    url: "includes/ajax_handlers/fetchInsertRuns.php",
    data: {pub_id:pubid},
    type:  'post',
    dataType: 'json',
    success: function (j) {
        var options = [], i = 0, o = null;
        for (i = 0; i < j.length; i++) {
            // required to get around IE bug (http://support.microsoft.com/?scid=kb%3Ben-us%3B276228)
            var options = '';
            for (var i = 0; i < j.length; i++) {
                options += '<option value="' + j[i].id + '">' + j[i].label + '</option>';
            }
            $("#run_id").html(options);
        }

        // hand control back to browser for a moment
        setTimeout(function () {
        $("#run_id")
                    .find('option:first')
                    .attr('selected', 'selected')
                    .parent('select')
                    .trigger('change');
        }, 0);
        },
        error: function (xhr, desc, er) {
            var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred retrieving insert runs:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           } 
        
    });
}

//inserter planning functions
/*
function getInserterInfo(id)
{
    var inserterfield=$('#inserter_'+id);
    if(inserterfield.val()!=0)
    {
        var inserterid=inserterfield.val();
        $.ajax({
          url: "includes/ajax_handlers/insertPackage.php",
          type: "POST",
          data: ({action:'inserterinfo',inserterid:inserterid,id:id}),
          dataType: "html",
          success: function(response){
              var pieces=response.split("|");
              //alert('getting id of '+id+' and type of '+type+' resp0:'+pieces[0]);
              var checker=$('#doublecheck_'+id);
              if(pieces[0]=='choice')
              {
                $('#hoppers_'+id).val(pieces[1]);
                checker.attr('rel',pieces[1]+'_'+pieces[2]);
                checker.removeAttr('disabled');
              } else {
                $('#hoppers_'+id).val(pieces[1]);
                checker.attr('rel',pieces[1]+'_0');
                checker.attr('disabled','disabled');
              }
          },
           error: function (xhr, desc, er) {
            var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred retrieving inserter info:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
       });
   } else {
       $('#zone_holder').html('');
   }
}

function setMaxInserts(id)
{
    //get the rel value of the input checkbox
    var relvalue=$('#doublecheck_'+id).attr('rel');
    var checkbox=$('#doublecheck_'+id+':checked').length;
    //now split it
    var pieces=relvalue.split("_");
    //which piece we want depends on whether or not the checkbox is checked.
    //alert('for '+checkbox.attr('id')+' value is '+checkbox.val());
    if (checkbox>0)
    {
       $('#hoppers_'+id).val(pieces[1]); 
    } else {
       $('#hoppers_'+id).val(pieces[0]); 
    }
}

function showNextPackage(id)
{
    $('#div_'+id).css({'display':'block'});
    $('#addnext'+id).css({'display':'none'});
    $('#package_'+id).sortable("disable");
    if(id<10)
    {
        id++;
        $('#addnext'+id).css({'display':'block'});
    }
}

function showPackageSave(id)
{
    //the idea here is to display a pulsate effect on the 'saving' span element
    var options={};
    $('#saving_'+id).css({'display':'block'});
    $('#saving_'+id).effect( 'pulsate', options, 200, function(){$('#saving_'+id).css({'display':'none'})});
}

function addInsertToPackage(id,insertid,tabpages,pweight)
{
    var planid=$('#plan_id').val();
    var packageid=$('#packageid_'+id).val();
    $.ajax({
          url: "includes/ajax_handlers/insertPackage.php",
          type: "POST",
          data: ({action:'saveinsert',insertid:insertid,planid:planid,packageid:packageid,tabpages:tabpages,pieceweight:pweight}),
          dataType: "html",
          success: function(response){
              var pieces=response.split("|");
              if(pieces[0]=='success')
              {
                //now run out little saving effect even though we are done so the user sees an update 
                showPackageSave(id);
                var pack=$('#p-'+packageid);
                var packrel=pack.attr('rel');
                var packpieces=packrel.split('_');
                
                if(insertid.substr(0,2)=='p-')
                {
                    tabpages=tabpages.split('_');
                    var packcount=parseInt(packpieces[0])+parseInt(tabpages[0]);
                    var packpages=parseInt(packpieces[1])+parseInt(tabpages[1]);
                    var packweight=parseFloat(packpieces[2])+parseFloat(tabpages[2]);
                } else {
                    var packcount=parseInt(packpieces[0])+1;
                    var packpages=parseInt(packpieces[1])+parseInt(tabpages);
                    var packweight=parseFloat(packpieces[2])+parseFloat(pweight);
                }
                
                var packhtml=pack.html();
                packhtml=packhtml.split(':');
                pack.html(packhtml[0]+': <small>'+packpages+'pgs</small>');
                pack.attr({'rel':packcount+'_'+packpages+'_'+packweight}); 
              } else {
                var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+pieces[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'There was a problem saving the insert:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
              }
          },
           error: function (xhr, desc, er) {
            var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred saving the insert:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
                
           }
       });
  
}

function removeInsertFromPackage(id,insertid,tabpages,pweight)
{
    var packageid=$('#packageid_'+id).val();
    $.ajax({
          url: "includes/ajax_handlers/insertPackage.php",
          type: "POST",
          data: ({action:'removeinsert',insertid:insertid,packageid:packageid,tabpages:tabpages,pieceweight:pweight}),
          dataType: "html",
          success: function(response){
              var pieces=response.split("|");
              if(pieces[0]=='success')
              {
                //now run out little saving effect even though we are done so the user sees an update 
                showPackageSave(id);
                
                var pack=$('#p-'+packageid);
                var packrel=pack.attr('rel');
                var packpieces=packrel.split('_');
                if(insertid.substr(0,2)=='p-')
                {
                    tabpages=tabpages.split('_');
                    var packcount=parseInt(packpieces[0])-parseInt(tabpages[0]);
                    var packpages=parseInt(packpieces[1])-parseInt(tabpages[1]);
                    var packweight=parseFloat(packpieces[2])-parseFloat(tabpages[2]);
                } else {
                    var packcount=parseInt(packpieces[0])-1;
                    var packpages=parseInt(packpieces[1])-parseInt(tabpages);
                    var packweight=parseFloat(packpieces[2])-parseFloat(pweight);
                }
                var packhtml=pack.html();
                packhtml=packhtml.split(':');
                pack.html(packhtml[0]+': <small>'+packpages+'pgs</small>');
                pack.attr({'rel':packcount+'_'+packpages+'_'+packweight});
              } else {
                var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+pieces[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'There was a problem removing the insert:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
              }
          },
           error: function (xhr, desc, er) {
            var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred removing the insert:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                });
           }
       }); 
}

function addJacketToPackage(id,insertid)
{
    var planid=$('#plan_id').val();
    var packageid=$('#packageid_'+id).val();
    $.ajax({
          url: "includes/ajax_handlers/insertPackage.php",
          type: "POST",
          data: ({action:'savejacket',insertid:insertid,planid:planid,packageid:packageid}),
          dataType: "html",
          success: function(response){
              var pieces=response.split("|");
              if(pieces[0]=='success')
              {
                //now run out little saving effect even though we are done so the user sees an update 
                showPackageSave(id);               
              } else {
                var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+pieces[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'There was a problem saving the jacket:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
              }
          },
           error: function (xhr, desc, er) {
            var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred saving the jacket:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
       });     
}

function removeJacketFromPackage(id,insertid)
{
    var packageid=$('#packageid_'+id).val();
    $.ajax({
          url: "includes/ajax_handlers/insertPackage.php",
          type: "POST",
          data: ({action:'removejacket',insertid:insertid,packageid:packageid}),
          dataType: "html",
          success: function(response){
              var pieces=response.split("|");
              if(pieces[0]=='success')
              {
                //now run out little saving effect even though we are done so the user sees an update 
                showPackageSave(id);               
              } else {
                var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+pieces[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'There was a problem removing the jacket:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
              }
          },
           error: function (xhr, desc, er) {
            var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred removing the jacket:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
       }); 
}

function saveInsertPackage(id)
{
   var action='update';
   var packageid=$('#packageid_'+id).val();
   if(packageid==0){action='insert';}
   var runname=$('#name_'+id).val();
   var runquantity=$('#quantity_'+id).val();
   var rundatetime=$('#date_'+id).val();
   var runinserter=$('#inserter_'+id).val();
   var rundouble=$('#doublecheck_'+id+':checked').length;
   var runhoppers=$('#hoppers_'+id).val();
   var runjacket=$('#jacketid_'+id).val();
   var pubid=$('#pub_id').val();
   var pubdate=$('#pub_date').val();
   var planid=$('#plan_id').val();
   var planrequest=$('#plan_request').val();
   if(runname=='' || rundatetime=='')
   {
      var $dialog = $('<div id="jConfirm"></div>')
        .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>You must enter at least a package name and time before saving.</p>')
        .dialog({
            autoOpen: true,
            modal: true,
            title: 'There was a problem saving the package:',
            buttons:[
            {
                text: 'Close',
                click: function() { 
                    $(this).dialog('destroy');
                }
            }]
        }) 
   } else {
   $.ajax({
          url: "includes/ajax_handlers/insertPackage.php",
          type: "POST",
          data: ({action:action,packageid:packageid,id:id,name:runname,datetime:rundatetime,inserterid:runinserter,doubleout:rundouble,hoppers:runhoppers,jacketid:runjacket,pubid:pubid,planid:planid,pubdate:pubdate,planrequest:planrequest,runquantity:runquantity}),
          dataType: "html",
          success: function(response){
              var pieces=response.split("|");
              if(pieces[0]=='success')
              {
                showPackageSave(id);
                if(packageid=='0')
                {
                    var newid=pieces[1];
                    $('#packageid_'+id).val(newid);
                    //now enable the package ul
                    $('#package_'+id).sortable("enable");
                    $('#jacketholder_'+id).css({'display':'block'});
                    //add a new package to the list in the other packages area
                    var newli="<li id='p-"+newid+"' rel='0_0' class='inserts'>"+runname+": <small>0pgs</small></li>\n";
                    $('#packagelist').append(newli);
                } 
                //now run out little saving effect even though we are done so the user sees an update 
                               
              } else {
                var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+pieces[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'There was a problem saving the package:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
              }
          },
           error: function (xhr, desc, er) {
            var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred retrieving inserter info:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
       }); 
   }  
}

function deleteInsertPackage(id)
{
    //get number of elements in the package
    var icount=$('#package_'+id).children('li').length;
    var packageid=$('#packageid_'+id).val();
    if(icount==0)
    {
        $.ajax({
          url: "includes/ajax_handlers/insertPackage.php",
          type: "POST",
          data: ({action:'delete',packageid:packageid}),
          dataType: "html",
          success: function(response){
              var pieces=response.split("|");
              if(pieces[0]=='success')
              {
                //now disable the package ul
                $('#package_'+id).sortable("disable");
                $('#packageid_'+id).val(0);
                $('#name_'+id).val('');
                $('#quantity_'+id).val('');
                $('#date_'+id).val('');
                $('#inserter_'+id).val(0);
                $('#inserts_'+id).val(0);
                $('#tabpages_'+id).val(0);
                $('#weight_'+id).val(0);
                $('#packagedisplay_'+id).html('0');
                $('#packagetabpages_'+id).html('0');
                $('#packageweight_'+id).html('0');
                $('#doublecheck_'+id+':checked').length;
                $('#hoppers_'+id).val(0);
                $('#jacketid_'+id).val(0);
                $('#p_'+packageid).remove();
                $('#jacketholder_'+id).css({'display':'none'});
                if(id>0)
                {
                    $('#div_'+id).css({'display':'none'});
                    $('#addnext'+id).css({'display':'none'});
                    var plusone=id+1;
                    $('#addnext'+plusone).css({'display':'block'});
                }
                
              } else {
                var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+pieces[1]+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'There was a problem deleting the package:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
              }
          },
           error: function (xhr, desc, er) {
            var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>'+xhr.status+'<br />'+thrownError+'</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'An error occurred retrieving inserter info:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                })
           }
       });
    } else {
        var $dialog = $('<div id="jConfirm"></div>')
                .html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Please return all inserts to the insert list before attempting to delete the package.</p>')
                .dialog({
                    autoOpen: true,
                    modal: true,
                    title: 'Unable to delete package:',
                    buttons:[
                    {
                        text: 'Close',
                        click: function() { 
                            $(this).dialog('destroy');
                        }
                    }]
                }) 
    }
}
*/
/*new as of 11/27/10 */
function getInsertRunZones()
{
    var insertid=$('#insertid').val();
    var schedid=$('#schedid').val();
    $.ajax({
      url: "includes/ajax_handlers/fetchInsertZones.php",
      type: "POST",
      data: ({insert_id:insertid,sched_id:schedid}),
      dataType: "json",
      success: function(response){
         $.each(response.zones, function(index,zone){
              $('#zoningForm').append(zone);
         })
      },
      error: function (xhr, desc, er) {
        bootbox.alert({
          title: 'An error occurred',
          message: xhr.status+'<br />'+desc 
       });
       var badresponse = {status: 404};
       return false;
      }
   });
}
   

/*new as of 11/27/10 */
function calcInsertZoneTotal(className) 
{
    var ztotal=0;
    if(className==''){className='checkbox'}
    $("."+className).each( function() {
       if($(this).is(':checked')){ztotal=ztotal+parseInt($(this).attr("rel"));}
    })
    $('#zonetotal').html(ztotal);
    $('#zoned_total').val(ztotal);
}

function calcStandardPages()
{
    var productSize = $('#productSize').val();
    var productPages = $('#productPages').val();
    var factor =  insertProductFactors[productSize];
    var standardPages = parseInt(productPages / factor);
    //console.log('size: '+productSize+' pages:' + productPages + ' factor: '+factor+' standard: '+standardPages);
    $('#standardPages').val(standardPages);
}
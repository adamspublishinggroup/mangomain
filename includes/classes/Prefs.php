<?php

class Prefs
{
    private static $_instance = null;
    private $preferences = array();
    private $jsPreferences = array();
    
    public  $siteID = 0;
    
    private function __construct($siteID=0)
    {
        //pull in the preferences
        $sql="SELECT * FROM core_preferences WHERE site_id=$siteID";
        $dbPrefs=dbselectsingle($sql);
        $preferences=$dbPrefs['data'];
        
        if($dbPrefs['numrows']>0)
        {
            foreach($preferences as $prefName=>$prefValue)
            {
                //all keys shall be stored as camelCase. need to convert database field names that could be in "a_b_c" format to aBxCx";
                if(strpos($prefName,"_")>0)
                {
                    $prefNameParts = explode("_",$prefName);
                    $tempName = array_shift($prefNameParts);
                    if(count($prefNameParts)>0)
                    {
                        foreach($prefNameParts as $pNP)
                        {
                            $tempName.=ucfirst($pNP);
                        }
                    }
                    $prefName = $tempName;
                }
                if(strpos($prefValue,"|")>0)
                {
                    $temp=explode("|",$prefValue);
                    if(count($temp)>0)
                    {
                        $prefValue=array();
                        $prefValueOptions=array();
                        foreach($temp as $tv)
                        {
                            if(strpos($tv,"-")>0)
                            {
                                $subtemp=explode("-",$tv);
                                $prefValue[]=trim($subtemp[0]);
                                $prefValueOptions[]=trim($subtemp[1]);
                                  
                            } else {
                                $prefValue[]=trim($tv);
                            }
                        }
                    }
                }
                $this->preferences[$prefName]=$prefValue;
                  
            }
        }
        
        $this->addStaticPreferences();
    }
    
    /*
    *   This function is designed to add in some "static" or hard-coded preferences, technically these would be configs
    */
    private function addStaticPreferences()
    {
        $this->preferences['insertProducts']=array("Broadsheet","Tab","Singlesheet","Booklet","Magazine","Sticky Note");
        $this->preferences['insertProductsOptions']=array("1","2","1","2","2","1");
        $this->jsPreferences['insertProductFactors']=$this->preferences['insertProductsOptions'];
    }
    
    public function showPreferences()
    {
        print "<pre>";
        print_r($this->preferences);
        print "</pre>\n";
    }
    
    public static function Instance($siteID=0)
    {
        if (self::$_instance == null) {
            self::$_instance = new Prefs($siteID);
        }
        return self::$_instance;
    }
    
    
    public function __set($name, $value)
    {
        $this->preferences[$name] = $value;
    }

    public function __get($name)
    {
        if (array_key_exists($name, $this->preferences)) {
            return $this->preferences[$name];
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }

    public function __isset($name)
    {
        return isset($this->preferences[$name]);
    }

    public function __unset($name)
    {
        unset($this->preferences[$name]);
    }
    
    /*
    *   This will outpout a series of javascript lines with a variable name and a value
    */
    public function jsPreferences()
    {
        if(count($this->jsPreferences)>0)
        {
            foreach($this->jsPreferences as $prefName => $prefValue)
            {
                print "     var $prefName = ".(is_array($prefValue) ? json_encode($prefValue) : "'$prefValue'").";\n";
            }
        } 
    }
}
<?php
use Spipu\Html2Pdf\Html2Pdf;

class Report
{
    private $headers = array();
    private $records = array();
    private $filename = '';
    private $errorMessage = '';
    private $summaries = array();
    private $tableClass = '';
    
    public function __construct()
    {
        $this->tableClass = "table table-striped tabled-bordered";
        $this->filename = "report-".date("Y-m-h-H-i"); 
    }
    
    public function setHeaders($data)
    {
        if(!is_array($data))
        {
            $data = $this->convertCSVtoArray($data);
        }
        $this->headers = $data;
    }
    
    
    /*
    *  This expects an array to be passed, if it's not an array, it will convert it to one (expecting it to be a csv file
    *  It will also then test to see if the column count matches the
    */
    public function addRecord($data,$show=false)
    {
        //we need to make sure the data count matches the header count
        if(!is_array($data))
        {
            $data = $this->convertCSVtoArray($data);
        }
        
        //did we get passed a multi line or multi element set of data?
        if(strpos($data,"\n")>0) $this->addRecords($data);
        
        //test for multi-dim data
        if(isset($data[1]) && is_array($data[1]))$this->addRecords($data);
        
        if(count($data)!=count($this->headers))
        {
            $this->errorMessage.="Column count of data does not match column count of the headers.<br>";
            return false; 
        }
        
        //ok, got here, we have valid good data
        if($show)
        {
            print_r($data);
        }
        $this->records[]=$data; 
    }
    
    /*
    * This function is meant to handle multi-line text files or arrays with multiple records, iteratively calling the addRecord function to add individual records
    */
    public function addRecords($data)
    {
        if(is_array($data))
        {
            foreach ($data as $record)
            {
                $this->addRecord($record);
            }
        } else {
            $data = str_replace("\r\n","\n",$data);
            $data = explode("\n",$data);
            if(count($data)>0)
            {
                foreach ($data as $record)
                {
                    $this->addRecord($record);
                }
            }
        }
    }
    
    /*
    *  This function adds a line at the bottom of the table or csv that has two elements, a label and a value
    *  So we'll need to do a col-span or a bunch of blank ','s to fill out the label side
    *    
    *   $data = array('label'=>'data')
    */
    public function addSummary($data)
    {
        foreach($data as $key=>$value)
        {
            $temp = array();
            /*
            for($i=0;$i<count($this->headers)-1;$i++)
            {
                $temp[]="";
            } 
            */    
            if(is_array($value))
            {
                $key = key($value);
                $value = $value[$key];
            }
            $this->summaries[$key]=$value;
            //$this->addRecord($temp,true);
        }
    }
    
    public function setFilename($filename)
    {
        $filename=str_replace(array(" ","?","*","'","(",")","{","}",",","&","#","@","!","^","=","\\","/","~","|","\""),"",$filename);
        $filename = explode(".",$filename); //we don't want any file extensions
        $this->filename = $filename[0];
    }
    
    public function output($type)
    {
        $message = true;
        switch ($type)
        {
            case 'csv':
                if($this->generateCSV())
                {
                    $message = "<div class='alert alert-success' role='alert'>Your report is ready for download. <a href='/uploads/reports/".$this->filename.".csv'>Right click and select save-link-as to download.</a></div>";
                } else {
                    $message = "<div class='alert alert-warning' role='alert'>There was a problem generating your report. Please let the developers know.</div>";
                }
                
            break;
            
            case 'pdf':
                $message = $this->generatePDF();
            break;
            
            case 'email':
                $message = $this->generateEmail();
            break;
            
            case 'screen':
                $message = $this->generateTable();
            break;
            
        }
        return $message;
    }
    
    /*
    *  Send an email with the report to the signed in user
    */
    function generateEmail()
    {
        $report = $this->generateTable(false);
        
        //who gets it?
        $to = $_SESSION['admin']['email'];
        
        $message = "<p>Below you will find the report that you requested.</p><br /><br />\r\n";
        $message.="<div style='width:800px;'>".$report."</div>";
        $subject = "Here is the report that you requested from ".SITE_NAME;
        $mail = new htmlMimeMail();
        $mail->setHtml($message);
        $mail->setFrom(MAIL_FROM_ADDRESS);
        $mail->setSubject($subject);
        $mail->setHeader('Sender',MAIL_FROM_NAME);
        if( $mail->send( array($to), 'mail' ) ) {
            return "<div class='alert alert-success' role='alert'>Your report has been sent to $to. If you don't see it in a few minutes, please check your junk/spam folders.</div>\n";
        } else {
            return "<div class='alert alert-warning' role='alert'>There was a problem sending your report to $to. Please let the developers know this was the error.".$mail->errors."</div>\n";
        }
    }
    
    /*
    *   Generate a PDF file of the report
    */
    function generatePDF()
    {
        $report = $this->generateTable(false); //get the report table as html
        $html2pdf = new Html2Pdf();
        $html2pdf->writeHTML($report);
        $html2pdf->output(ABS_PATH.'/uploads/reports/'.$this->filename.".pdf", 'F');
        // @TODO now we need to generate the PDF and save it in /uploads/reports and provide a download link
        
        return "<div class='alert alert-success' role='alert'>Your report has been generated. <a href='/uploads/reports/".$this->filename.".pdf' class='btn btn-primary'>Click here to download the report</a></div>\n";
        
    }
    
    /*
    *  Output a formatted table with the report information
    */
    public function generateTable($display=true)
    {
        if(!empty($this->headers))
        {
            $headers = implode("</th><th>",$this->headers);
            $html="
<table class='$this->tableClass'>
    <thead>
        <tr>
          <th>$headers</th>  
        </tr>
    </thead>
    <tbody>
";
            if(!empty($this->records))
            {
                foreach($this->records as $record)
                {
                    $data = implode("</td><td>",$record);
                    $html.= "      <tr>
        <td>$data</td>
      </tr>
      ";                          
            
                }
            }
            
            if(!empty($this->summaries))
            {
                $html.="    <tfoot>

";
                foreach($this->summaries as $label=>$value)
                {
                    $html.="      <tr>\n        <td colspan='".(count($this->headers)-1)."' style='text-align:right; font-weight:bold;'>$label</td><td style='font-weight:bold;'>$value</td>\n      </tr>\n";
                }
                $html.="
    </tfoot>
";
            }
            $html.= "    </tbody>
</table>
";    
            if($display){
                print $html;
            } else {
                return $html;
            }
        } else {
            return false;
        }
    }
    
    /*
    *  Output a csv file with the report information
    */
    public function generateCSV()
    {
        if(!empty($this->headers))
        {
            $fileContents = "";
            $fileContents = implode(",",$this->headers)."\r\n";
            if(!empty($this->records))
            {
                foreach($this->records as $record)
                {
                    $data = implode("\",\"",$record);
                    $fileContents.="\"$data\"\r\n";                          
                }
            }
            if(!empty($this->summaries))
            {
                $temp="";
                for($i=0;$i<count($this->headers)-1;$i++)
                {
                    $temp.=",";
                }
                foreach($this->summaries as $key=>$value)
                {
                    print $temp.$key.",".$value."\r\n"; 
                } 
            }
            file_put_contents("../uploads/reports/".$this->filename.".csv",$fileContents);
            return true;
        } else {
            return false;
        }
    }
    
    
    public function errorMessage()
    {
        if($this->errorMessage!='')
        {
            return $this->errorMessage;
        }
        else
        {
            return "";
        }
    }
    
    private function convertCSVtoArray($fileContent,$escape = '\\', $enclosure = '"', $delimiter = ',')
    {
        $lines = array();
        $fields = array();

        if($escape == $enclosure)
        {
            $escape = '\\';
            $fileContent = str_replace(array('\\',$enclosure.$enclosure,"\r\n","\r"),
                        array('\\\\',$escape.$enclosure,"\\n","\\n"),$fileContent);
        }
        else
            $fileContent = str_replace(array("\r\n","\r"),array("\\n","\\n"),$fileContent);

        $nb = strlen($fileContent);
        $field = '';
        $inEnclosure = false;
        $previous = '';

        for($i = 0;$i<$nb; $i++)
        {
            $c = $fileContent[$i];
            if($c === $enclosure)
            {
                if($previous !== $escape)
                    $inEnclosure ^= true;
                else
                    $field .= $enclosure;
            }
            else if($c === $escape)
            {
                $next = $fileContent[$i+1];
                if($next != $enclosure && $next != $escape)
                    $field .= $escape;
            }
            else if($c === $delimiter)
            {
                if($inEnclosure)
                    $field .= $delimiter;
                else
                {
                    //end of the field
                    $fields[] = $field;
                    $field = '';
                }
            }
            else if($c === "\n")
            {
                $fields[] = $field;
                $field = '';
                $lines[] = $fields;
                $fields = array();
            }
            else
                $field .= $c;
            $previous = $c;
        }
        //we add the last element
        if(true || $field !== '')
        {
            $fields[] = $field;
            $lines[] = $fields;
        }
        return $fields;
    }

}
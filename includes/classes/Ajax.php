<?php
  
  
class Ajax
{
    private $startTime;
    private $endTime;
    
    private $data;
    
    private function __construct()
    {
         $this->startTime = microtime(true);
    }
    
    public function addData($key,$value)
    {
        $this->$data[$key]=$value;
    }
    
    public function output()
    {
        $this->endTime = microtime(true);
        $this->data['ajax_debug']['execution_time']=$this->endTime-$this->startTime;
        $this->data['ajax_debug']['handling_script']=$_SERVER['SCRIPT_NAME'];
        
        //return the json-encoded array
        print json_encode($this->data);
    }
    
}   
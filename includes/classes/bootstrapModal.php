<?php
/* bootstrap dialog class 
*   This class is designed to generate all the necessary markup to show a modal with a form, and generate the necessary ajax code to submit it
*/

class bootstrapDialog
{
    private $modalID;
    private $processFile;
    private $sourceFile;
    private $dataAttr;
    private $formFields = array();
    private $callback = ''; //function to be called on successful save of form
    private $modalHeader;
    private $buttonLabel = 'Submit';
    private $caller = ''; //this will be set as the same as the JS variable referencing the caller
    
    public function __construct($divID)
    {
        //construct with passed in modal div ID
        $this->modalID = $divID;
        $this->caller = $divID."_caller";
    }
    
    //set the submit button label
    public function setButton($button)
    {
        $this->buttonLabel = $button;
    }
    
    //set the text in the modal header
    public function setHeader($header)
    {
        $this->modalHeader = $header;
    }
    
    //set the text in the save action callback
    public function setCallback($callback)
    {
        $this->callback = $callback;
    }
    
    // this is the element on the click element to pull the record id from (ex. data-id, we would pass 'id' to this function
    public function setData($attr)
    {
        $this->dataAttr = $attr;
    }
    
    //this is the source that will get ajax queried to get the data to pre-fill the form.
    public function setSource($source)
    {
        $this->sourceFile = $source;
    }
    
    //this is the end point for the form submission
    public function setProcessor($file)
    {
        $this->processFile = $file;
    }
    
    /*
    Return the js variable that references the calling dom element. This can then be referenced in a callback script to modify the caller (ie, update text, remove click functionality, etc)
    */
    public function getCaller()
    {
        return $this->caller;
    }
    
    //return a button to launch the modal
    public function getButton($label,$recordID,$class='btn btn-primary', $icon='')
    {
        /*$button = "
        <button type='button' class='".($class!='' ? $class : '')."' data-toggle='modal' data-src='".$this->sourceFile."' data-target='#".$this->modalID."' data-modalID='".$this->modalID."' data-id='$recordID' data-function='".$this->modalID."_loader' role='button'>".($icon!='' ? '<i class='.$icon.'></i> ' : '').$label."</button>
        ";
        */
        $button = "
        <button type='button' class='".($class!='' ? $class : '')."' data-toggle='modal' data-src='".$this->sourceFile."' data-target='#".$this->modalID."' data-modalID='".$this->modalID."' data-id='$recordID' data-loader='".$this->modalID."_loader' role='button' onClick='".$this->modalID."_loader(this)'>".($icon!='' ? '<i class='.$icon.'></i> ' : '').$label."</button>
        ";
        return $button;
    }
    
    //return a link to launch the modal
    public function getLink($label,$recordID,$class='btn btn-primary', $icon='')
    {
        $link = "
        <a href='#' class='".($class!='' ? $class : '')."' data-toggle='modal' data-src='".$this->sourceFile."' data-target='#".$this->modalID."' data-modalID='".$this->modalID."' data-id='$recordID' data-loader='".$this->modalID."_loader' >".($icon!='' ? '<i class='.$icon.'></i> ' : '')."$label</a>
        ";
        return $link;
    }
    
    //going to need to know what form elements to include
    //all the standard *make* form elements will be available.
    
    //need to generate the dialog markup
    
    // need to handle the "on show" event and load in data
    
    // need to handle a submit action and serialze form data to send to a processing script
    
    /*
    This element is meant to be an empty div that will get filled with some textual data from the ajax load.
    */
    public function addTextLabel($data)
    {
        $this->formFields[]=array('type'=>'label','data'=>$data);
    }
    
    /*
    * this function handles setting up the form text field
    * pass in an array of attributes like:
     $data['db_field']
     $data['value']
     $data['label']
     $data['explain']
    */
    public function addText($data)
    {
        $this->formFields[]=array('type'=>'text','data'=>$data);
    }
    
    /*
    * this function handles setting up the form text field
    * pass in an array of attributes like:
     $data['db_field']
     $data['value']
     $data['label']
     $data['explain']
    */
    public function addTextarea($data)
    {
        $this->formFields[]=array('type'=>'textarea','data'=>$data);
    }
    
    /*
    * this function handles setting up the form text field
    * pass in an array of attributes like:
     $data['db_field']
     $data['value']
     $data['label']
     $data['explain']
    */
    public function addCheckbox($data)
    {
        $this->formFields[]=array('type'=>'checkbox','data'=>$data);
    }
    
    /*
    * this function handles setting up the form text field
    * pass in an array of attributes like:
     $data['db_field']
     $data['value']
     $data['label']
     $data['explain']
     $data['options']
    */
    public function addSelect($data)
    {
        $this->formFields[]=array('type'=>'select','data'=>$data);
    }
    
    /*
    * this function handles setting up the form text field
    * pass in an array of attributes like:
     $data['db_field']
     $data['value']
     $data['label']
     $data['explain']
    */
    public function addDate($data)
    {
        $this->formFields[]=array('type'=>'date','data'=>$data);
    }
    
    /*
    * this function handles setting up the form text field
    * pass in an array of attributes like:
     $data['db_field']
     $data['value']
     $data['label']
     $data['explain']
    */
    public function addDateTime($data)
    {
        $this->formFields[]=array('type'=>'datetime','data'=>$data);
    }
    
    /*
    * this function handles setting up the form text field
    * pass in an array of attributes like:
     $data['db_field']
     $data['value']
     $data['label']
     $data['explain']
    */
    public function addTime($data)
    {
        $this->formFields[]=array('type'=>'time','data'=>$data);
    }
    
    //generate the html for a text field
    private function buildText($data)
    {
        make_text($data['db_field'],$data['value'],$data['label'],$data['explain_text']);
    }
    
    //this function will cause all the generation to happen.
    public function generate()
    {
       print "<!-- Generating modal for $this->modalID -->\n";
        // generate the form markup
       $this->modalStart();
       $this->modalComplete(); 
       $this->modalJSInit();
       $this->saveForm();
       print "<!-- modal generation complete -->\n";
    }
    
    private function modalStart ()
    {
        ?>
<div class="modal fade bootstrapDialog" id="<?php echo $this->modalID ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $this->modalID ?>_label">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="<?php echo $this->modalID ?>_label"><?php echo $this->modalHeader ?></h4>
      </div>
      <div class="modal-body">
        <form id='<?php echo $this->modalID ?>_form' name='<?php echo $this->modalID ?>_form' class='form-horizontal'>
        <?php
        if($this->formDescription !='')
        {
            print "<p>".$this->formDescription."</p>";
        }
        if(count($this->formFields)>0)
        {
            //display all the form elements
            foreach($this->formFields as $field)
            {
                $data = $field['data'];
                $data['db_field']=$this->modalID."_".$data['db_field'];
                switch ($field['type'])
                {
                    case "label":
                         print "
          <div class='form-group'>
            <label for='$data[db_field]' class='col-sm-2 control-label'>$data[label]</label>
            <div id='$data[db_field]' class='col-sm-10'>
              $data[value]
            </div>
          </div>
          ";            
                    break;
                    
                    case "text":
                        make_text($data['db_field'],$data['value'],$data['label'],$data['explain']);
                    break;
                    
                    case "textarea":
                        make_textarea($data['db_field'],$data['value'],$data['label'],$data['explain'],50,5,false);
                    break;
                    
                    case "checkbox":
                        make_checkbox($data['db_field'],$data['value'],$data['label'],$data['explain']);
                    break;
                    
                    case "date":
                        make_date($data['db_field'],$data['value'],$data['label'],$data['explain'],1,'','',true);
                    break;
                    
                    case "datetime":
                        make_datetime($data['db_field'],$data['value'],$data['label'],$data['explain']);
                    break;
                    
                    case "time":
                        make_time($data['db_field'],$data['value'],$data['label'],$data['explain']);
                    break;
                    
                    case "select":
                        make_select($data['db_field'],$data['value'],$data['options'],$data['label'],$data['explain']);
                    break;
                    
                    case "number":
                        make_number($data['db_field'],$data['value'],$data['label'],$data['explain']);
                    break;
                }
            }
        }
        //NOTE: Form closed in modalComplete() function
    }
    
    private function modalComplete() 
    {
       ?>
        <input type='hidden' id='<?php echo $this->modalID; ?>_mrecordID' name='<?php echo $this->modalID; ?>_mrecordID' value='0' />
        </form>
       <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick='<?php echo $this->modalID; ?>_save()'><?php echo $this->buttonLabel ?></button>
      </div>
    </div>
  </div>
</div>
</div>
       <?php 
        
    }
    
    private function modalJSInit()
    {
        $varScript = "
        var ".$this->caller.";
        var modal".$this->modalID.";\n";
        $this->addScript($varScript);
        
        if(!isset($GLOBALS['bootstrapDialogJSInit']) || $GLOBALS['bootstrapDialogJSInit']==false)
        {
        
          if($this->sourceFile!='')
          {
             
      $script="\n
           \$('.bootstrapDialog').off().on('show.bs.modal', function (event) {
           console.log('booting Modal Ajax load function '+this.id);
           var tempModal = \$(this);
           var rTarget = \$(event.relatedTarget);
           // now we need to do an ajax call to load this record from the source
           \$.ajax({
                  url: 'includes/ajax_handlers/'+\$(rTarget).data('src'),
                  type: 'GET',
                  data: {record_id: \$(rTarget).data('id'),modal_id: \$(rTarget).data('modalID')},
                  dataType: 'json',
                  success: function(response){
                      window[\$(rTarget).data('loader')](response,rTarget,tempModal);";
              $script.="        
                  },
                  error: function (xhr, desc, er) {
                      bootbox.alert({
                          title: 'An error occurred',
                          message: xhr.status+'<br />'+desc 
                      });
                  }
            });
      
      })
        ";
           $this->addScript($script);
           $GLOBALS['bootstrapDialogJSInit']=true;   
           }
        }
       $loadscript = "
       function ".$this->modalID."_loader(response,rTarget,tempModal){
          if(tempModal != null)
          {
          ".$this->caller." = \$(rTarget); // Button that triggered the modal
          modal".$this->modalID." = tempModal;
";
          //ok, at this point we are inside the success function of the ajax load
          //we need to loop through all the form fields and load the data appropriately
          if(count($this->formFields)>0)
          {
              foreach($this->formFields as $field)
              {
                    $data = $field['data'];
                    //$loadscript.="console.log('setting $data[db_field] to '+response.$data[db_field]);\n";
                    switch ($field['type'])
                    {
                        case "label":
                             $loadscript.= "          \$('#".$this->modalID."_$data[db_field]').html(response.$data[db_field]);\n";     
                        break;
                        
                        default:
                            $loadscript.= "          \$('#".$this->modalID."_$data[db_field]').val(response.$data[db_field]);\n"; 
                        break;
                        
                    } 
              }
          }
          $loadscript.="          \$('#".$this->modalID."_mrecordID').val(\$(rTarget).data('id'));
          tempModal.show();
         } 
         }
       ";
       $this->addScript($loadscript);
      
    }
    
    private function saveForm()
    {
        $script = "
        
        function ".$this->modalID."_save()
        {
            
            var formData = \$('#".$this->modalID."_form :not([type=\"checkbox\"])').serialize();
            console.log(formData);
            var formDataCheckBoxes=\$('#".$this->modalID."_form input[type=\"checkbox\"]').map(function(){
            return this.name+'='+this.checked;}).get().join('&');
            console.log(formDataCheckBoxes);
            
            if(formDataCheckBoxes!='' && formData!='') formData+='&'+formDataCheckBoxes;
            else formData+=formDataCheckBoxes; 
            
            formData+='&modal_id=".$this->modalID."';
            console.log('saving form ');
            
            console.log(formData);
            //call the generic Ajax Function
            genericAjaxHandler('".$this->processFile."',formData, ".($this->callback !='' ? $this->callback : 'callback_'.$this->modalID).");
            \$('#".$this->modalID."').modal('hide');
        }
        
        function callback_".$this->modalID."(response)
        {
           /* IF EVERYTHING GOES WELL */
           console.log('default call back');   
        }
        ";
        
        $this->addScript($script);
    }
    private function addScript($script)
    {
        $GLOBALS['scripts'][]=$script;
    }
    
}

<?php

class PressJob
{
    private $id = 0;
    
    private $fields = array('pub_id', 'insert_pub_id', 'run_id', 'pub_date', 
    'startdatetime', 'enddatetime', 'draw', 'draw_hd', 'draw_sc', 'draw_office', 'draw_customer', 'draw_other', 
    'folder', 'lap', 'rollSize', 'papertype', 'papertype_ocver', 'layout_id', 'overrun', 'quarterfold', 'gatefold', 
    'notes_job', 'notes_press', 'recurring_id', 'stitch', 'trim', 'pagewidth', 'glossy_cover', 'glossy_cover_draw', 
    'glossy_insides', 'glossy_insides_count', 'cover_date_due', 'cover_date_print', 'cover_date_output', 'page_release', 
    'page_rip', 'notes_delivery', 'notes_bindery', 'bindery_startdate', 'bindery_duedate', 'scheduled_time', 'scheduled_by', 
    'colorset_time', 'colorset_by', 'drawset_time', 'drawset_by', 'updated_time', 'updated_by', 'dataset_time', 'dataset_by', 
    'layoutset_time', 'layoutset_by', 'data_collected', 'redo_job_id', 'insert_source', 'press_id', 'reprint_original_id', 
    'job_message', 'deleted_by', 'slitter', 'request_printdate', 'requires_addressing', 'requires_delivery', 'requires_inserting', 
    'notes_inserting', 'job_type', 'page_notes', 'plate_notes');
    
    function __construct($id=0)
    {
        if($id==0)
        {
            //create new press job
            $this->createJob();
        } else {
            $this->id = $id;
        }
    }
    
    /*
    *  return an insert record ID
    */
    private function createJob()
    {
        global $db, $User;
        $sql="INSERT INTO jobs (status, created_time, created_by, site_id) VALUES (0,'".date("Y-m-d H:i:s")."',".$User->id.",".SITE_ID.")";
        $dbInsert = dbinsertquery($sql);  
        
        $this->id = $dbInsert['insertid']; 
    }
    
    
    /*
    *   handle all deletion of press job data
    */
    public function delete()
    {
        
    }
    
    
    /*
    *  Update job record with a matched set of data
    *  We'll filter against the "approved" fields to make sure we don't have any bad data coming in
    *  expects data in the form of an array with field name => value (values should be passed in escaped already)
    *  if data is not already escaped, second passed value should be set to true
    */
    public function update($data,$escape=false)
    {
        $updates = array();
        if(isset($data) && count($data)>0)
        {
            foreach($data as $key=>$value)
            {
                if(in_array($key,$this->fields))
                {
                    if($escape){$value=addslashes($value);}
                    $updates[]="$key = '$value'";
                }
            }
            if(count($updates)>0)
            {
                $sql="UPDATE jobs SET ".implode(",",$updates)." WHERE id=".$this->id;
                $dbUpdate=dbexecutequery($sql);
            }
        }         
    }
    
    /*
    *  Builds a press job based on data from a special section record
    */
    
    public function fromSpecialSection($id)
    {
        $id=intval($id); //just to be safe
        $sql="SELECT * FROM special_sections WHERE id=$id";
        $dbSection = dbselectsingle($sql);
            
    }
    
    /*
    *  Returns as array of formatted field names and values
    */
    public function stats()
    {
        
    }
    
    
    /*
    * Returns the sections for the job
    * This will make it easier when we transition to sections as their own records
    */
    public function sections()
    {
        
    }
}   
<?php
/*
*/

class Page
{
    private static $_instance = null;
    
    private $scriptFiles = array(); 
    private $cssFiles = array(); 
    private $scripts = array(); 
    private $styles = array(); 
    
    private $appname='MANGO';
    private $appdesc='Newspaper Management Made Simple';
    
    private $render = 'html';    
    
    private $startExecution = 0;
    private $endExecution = 0;
    
    private function __construct($render='html')
    {
        $this->startExecution = microtime(true);
        if (!checkPermission($_SERVER['SCRIPT_NAME']))
        {
            $_SESSION['accessdenied']=true;
            redirect('/login.php');
        }

        if($_GET['popup'] || $_SESSION['kiosk']==true || $render=='modal')
        {
            $this->render = 'modal';
            //do not load the menu for a popup window mode of a page
        }
        if($GLOBAL['system'])
        {
            
        } else {
            $this->pageHeader();
        }
    }
    
    public static function Instance($popup=false)
    {
        if($popup){$render='modal';}else{$render='html';}
        if (self::$_instance == null) {
            self::$_instance = new Page($render);
        }
        return self::$_instance;
    }
    
    function pageHeader()
    {
        //lets see if an app cookie is set, if not, set it to either a posted app, or the default of mango
        ?><!DOCTYPE html> 
    <html lang="en">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $this->appname.' - '.$this->appdesc; ?></title>
    <META name="author" content="Joe Hansen <jhansen@pioneernewsgroup.com>">
    <META name="copyright" content="<?php echo date("Y"); ?> Pioneer News Group Inc - Joe Hansen">
    <META name="robots" content="none">
    <META http-equiv="cache-control" content="no-cache">
    <META http-equiv="pragma" content="no-cache">
    <META http-equiv="content-type" content="text/html; charset=UTF-8">
    <META http-equiv="expires" content="<?php echo date("r") ?>">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
    <?php
        $this->jsVariables();
    ?>
    <!-- loading core libraries -->
    <!-- jquery -->
    <script src="/includes/core_libraries/jquery-3.2.0.min.js" type="text/javascript"></script>
    <script src="/includes/core_libraries/jquery-ui-1.12.1.custom/jquery-ui.min.js" type="text/javascript"></script>
    <script>
        // Change JQueryUI plugin names to fix name collision with Bootstrap.
        $.widget.bridge('jquitooltip', $.ui.tooltip);
        $.widget.bridge('jquibutton', $.ui.button);
    </script>
    <!-- load a CDN version of jquery-migrate -->
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js" type="text/javascript"></script> 
    
    <!-- Bootstrap JavaScript -->
    <script src="/includes/core_libraries/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    
    <!-- font awesome -->
    <link rel='stylesheet' type='text/css'  href="/includes/core_libraries/font-awesome-4.7.0/css/font-awesome.min.css" />
    <!-- jquery ui core css -->
    <link rel='stylesheet' type='text/css'  href="/includes/core_libraries/jquery-ui-1.12.1.custom/jquery-ui.css" />

    <!-- bootstrap CSS -->
    <link rel="stylesheet" href="/includes/core_libraries/bootstrap-3.3.7-dist/css/bootstrap.min.css" />

    <!-- Optional Bootstrap theme -->
    <link rel="stylesheet" href="/includes/core_libraries/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />
    
    <!-- cleditor WYSIWYG editor -->
    <link rel="stylesheet" href="/includes/core_libraries/cleditor/jquery.cleditor.css" />
    <script src="/includes/core_libraries/cleditor/jquery.cleditor.min.js"></script>
    <script src="/includes/core_libraries/cleditor/jquery.cleditor.table.min.js"></script>
    <script src="/includes/core_libraries/cleditor/jquery.cleditor.icon.min.js"></script>
    <script src="/includes/core_libraries/cleditor/jquery.cleditor_init.js"></script>
    
    <?php
    $scriptname=end(explode("/",$_SERVER['SCRIPT_NAME']));
    $this->loadHeadFiles('all');
    $GLOBALS['scripts']=array();   
    $GLOBALS['modals']=array();
    ?>
    <!--[if lte IE 7]>
    <style type="text/css">
    html .jqueryslidemenu{height: 1%;} /*Holly Hack for IE7 and below*/
    </style>
    <![endif]-->
    <?php
        if($this->render == 'html'){
    ?>
    <style>
    .nav-tabs {
        margin-bottom:10px;
    }
    body { padding: 70px 0; }
    #pageMonitor h4 {
        padding-bottom: 4px;
        margin-bottom: 4px;
        width: 100%;
        border-bottom: thin solid black;
    }
    @media print {
        html, body {
           margin:0;
           padding:0;
        }
        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
        {
            padding: 2px;
            font-size: 12px;
        }
        table, th, td {
           border: 1px solid black;
        }
        .page-header h1 {font-size: 14px;}
        .page-header small {font-size:12px;}
    }
    
    </style>
    <?php } ?>
    </head>
    <body>
    <div class='<?php echo ($GLOBALS['pageFluid'] ? 'container-fluid' : 'container');?>'>
        <?php  
        if($this->render == 'html'){
            $this->nav();
            $refer = str_replace('http://'.$_SERVER['SERVER_NAME'].'/','',$_SERVER['HTTP_REFERER']);
            if($refer=='warehouseMenu.php' && str_replace("/","",$_SERVER['SCRIPT_NAME'])!='warehouseMenu.php')
            {
                print "<a href='warehouseMenu.php' class='btn btn-block btn-primary'>Return to Warehouse Menu</a><br><br>\n";
            }
        }
        
    }
    
    /*
    *  SET any javascript variables used on the page in a general sense
    */
    function jsVariables()
    {
        global $User, $Prefs;
        ?>
    <script type='text/javascript'>
        var debug=<?php echo $GLOBALS['debug'];?>;
            <?php
            //look for specific permissions for some global javascript variables
            //we go through all permissions that have include_js, then see if the user has any of them
            $sql="SELECT * FROM core_permission_list WHERE include_js=1 AND js_varname<>''";
            $dbJSPerms=dbselectmulti($sql);
            if($dbJSPerms['numrows']>0)
            {
                foreach($dbJSPerms['data'] as $perm)
                {
                    $permid=$perm['id'];
                    if($User->hasPermission($permid))
                    {
                        $valid='true';
                    } else {
                        $valid='false';
                    }
                    print "        var ".stripslashes($perm['js_varname'])." = $valid;\n";
                }
            } ?>
        var calendarStartAddressing='<?php echo ($Prefs->calendarStartAddressing=='' ? '6' : ($Prefs->calendarStartAddressing == 'current' ? date("H") : $Prefs->calendarStartAddressing) )?>';
        var calendarStartPress='<?php echo ($Prefs->calendarStartPress=='' ? '6' : ($Prefs->calendarStartPress == 'current' ? date("H") : $Prefs->calendarStartPress))?>';
        var calendarStartPackaging='<?php echo ($Prefs->calendarStartPackaging=='' ? '6' : ($Prefs->calendarStartPackaging == 'current' ? date("H") : $Prefs->calendarStartPackaging ))?>';
        var calendarStartBindery='<?php echo ($Prefs->calendarStartBindery=='' ? '6' : ($Prefs->calendarStartBindery == 'current' ? date("H") : $Prefs->calendarStartBindery ))?>';
        var calendarPackagingSlots=<?php echo ($Prefs->calendarPackagingSlots=='' ? '60' : $Prefs->calendarPackagingSlots )?>;
        var calendarBinderySlots=<?php echo ($Prefs->calendarBinderySlots=='' ? '60' : $Prefs->calendarBinderySlots )?>;
        var calendarPressSlots=<?php echo ($Prefs->calendarPressSlots=='' ? '15' : $Prefs->calendarPressSlots )?>;
        var calendarAddressingSlots=<?php echo ($Prefs->calendarAddressingSlots=='' ? '60' : $Prefs->calendarAddressingSlots )?>;
        var pressRunTimeThreshold=<?= $Prefs->pressRunTimeThreshold !='' ? $Prefs->pressRunTimeThreshold: 3600 ?>;  //sets how long to allow a run to be before flagging as a problem
        var pressCounterThreshhold=<?= $Prefs->pressCounterThreshhold !='' ? $Prefs->pressCounterThreshhold: 1000000 ?>;  //sets how long to allow a run to be before flagging as a problem
        var taxRate=<?php echo ($Prefs->taxRate!='' ? $Prefs->taxRate : '6.0' )?>;
            <?php
            if(in_array(39,$_SESSION['permissions']) || $_SESSION['admin'])
            {
                $minDate=time();
            } else {
                $minDate=strtotime("+".$GLOBALS['lockPressPrint']." hours");
            }
            $minYear=date("Y",$minDate);
            $minMonth=date("m",$minDate)-1;
            $minDay=date("d",$minDate);
            $minHour=date("H",$minDate);
            $minMinute=date("i",$minDate);
            ?>
        var minPressCalendarJobAddDate = new Date(<?php echo $minYear ?>,<?php echo $minMonth ?>,<?php echo $minDay ?>,<?php echo $minHour ?>,<?php echo $minMinute ?>);
            <?php
$Prefs->jsPreferences();
            ?>           
        </script>
        <?php
    }
    function nav()
    {
        global $User;
        $nav = checkCache('menu',$User->id);
        if(trim($nav) !='') 
        {
            print $nav;
            return;
        }
        $nav = "
        <div class='row'>
        <div class='navbar navbar-inverse navbar-fixed-top' role='navigation'>
            <div class='container-fluid'>
                <div class='navbar-header'>
                  <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#mangoMainNav' aria-expanded='false'>
                    <span class='sr-only'>Toggle navigation</span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                    <span class='icon-bar'></span>
                  </button>
                </div>
                
                <div id='mangoMainNav' class='collapse navbar-collapse'>
                    <ul class='nav navbar-nav'>
        ";
        //get the id of the primary site
        $sql="SELECT id, ip_address FROM core_sites WHERE primary_site=1";
        $dbPrimary=dbselectsingle($sql);
        $primaryid=$dbPrimary['data']['id'];
        $primaryaddress=$dbPrimary['data']['ip_address'];
        if($primaryaddress!=$_SERVER['SERVER_ADDR'] && !$User->isAdmin())
        {
            $ptest="AND primary_site_only=0";
        } else {
            $ptest='';
        }
        $sql="SELECT * FROM core_pages WHERE parent_id=0 AND display=1 $ptest ORDER BY weight";
        $dbTop=dbselectmulti($sql);
        $sub=array();
        $i=0;
        if ($dbTop['numrows']>0)
        {
            $nav.= "  <li class='menu-item' ><a href='#' onclick=\"javascript:window.open('chatV2.php?popup=true','Production Chat System','width=520,height=680,toolbar=0,status=0,location=0');return false;\"><i class='fa fa-comments'></i></a></li>\n";
            foreach($dbTop['data'] as $top)
            {
                $nav.= "  <li class='menu-item dropdown'>";
                $file=$top['filename'];
                $name=$top['name'];
                if(substr($file,0,5)=='http:' || substr($file,0,6)=='https:')
                {
                    //external web link
                    $target="target='_blank'";
                } else {
                    $target='';
                    $testfile=explode("?",$file);
                    $testfile=ABS_PATH."/".$testfile[0];
                    if (!file_exists($testfile) && $file!='')
                    {
                        
                        $name="$name - no file!";
                        $file="#";
                    }
                }
                $file=URL_ROOT.$file;
                if ($file==''){$file='#';}
                if ($top['popup'])
                {
                    $width=$top['popup_width'];
                    $height=$top['popup_height'];
                    $nav.= "<a href='#' onclick=\"window.open('$file?popup=true','$name','width=$width,height=$height,toolbar=no,status=no,location=no,scrollbars=no');return false;\">$name</a>";
                } else 
                {
                    $nav.= "<a href='$file' class='dropdown-toggle' data-toggle='dropdown' $target>$name <b class='caret'></b></a>";
                }
                $sql="SELECT * FROM core_pages WHERE parent_id=$top[id] AND display=1 $ptest ORDER BY weight";
                $dbSub=dbselectmulti($sql);
                if ($dbSub['numrows']>0)
                {
                    $nav.= "<ul class='dropdown-menu'>\n";
                    $nav.=$this->submenu($dbSub['data'],$ptest);
                    $nav.= "</ul>\n";
                }
                $nav.= "  </li>\n";
                $i++; 
            }
            
        } else {
            $nav.= "<li>No top menu items have been found</li>";
        }
        $nav.="
         </ul>
                    <ul class='nav navbar-nav navbar-right'>
                    <li class='dropdown'>
                      <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>Welcome ".$User->name()."<span class='caret'></span></a>
                      <ul class='dropdown-menu'>
                        <li><a href='/userProfile.php'>Manage your profile</a></li>
                        <li><a href='/logout.php'>Log out</a></li>
                        <li><a href='#' data-toggle='modal' data-target='#passwordModal'>Change password</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
                
            </div>
        </div>
        </div><!--closes menu row -->
        ";
        setCache('menu',$User->id,$nav);
        print $nav;
    }
    
    
    private function submenu($menus,$ptest)
    {
        foreach($menus as $top)
        {
            $sql="SELECT * FROM core_pages WHERE parent_id=$top[id] AND display=1 $ptest ORDER BY weight";
            $dbSub=dbselectmulti($sql);
            
            if ($dbSub['numrows']>0)
            {
                $nav.= "      <li class='menu-item dropdown dropdown-submenu'>";
            } else {
                $nav.= "      <li class='menu-item'>";
            }
            $file=$top['filename'];
            $name=$top['name'];
            if(substr($file,0,5)=='http:' || substr($file,0,6)=='https:')
            {
                //external web link
                $target="target='_blank'";
            } else {
                $target="";
                $testfile=explode("?",$file);
                $testfile=ABS_PATH."/".$testfile[0];
                if (checkPermission($file))
                {
                    $lock='';
                    $locked=false;    
                } else {
                    $locked=true;
                    $lock="<i class='fa fa-lock'></i> "; 
                }
                    
                if (!file_exists($testfile) && $file!='')
                {
                  $name=$lock." $name <i class='fa fa-exclamation-triangle'></i>";
                  $file="#";
                } else {
                  $name=$lock.$name;
                }
            }
            if ($file==''){$file='#';}
            $file=URL_ROOT.$file;
            if ($top['popup'])
            {
                $width=$top['popup_width'];
                $height=$top['popup_height'];
                if($locked)
                {
                    $nav.= "<a href='#'>$name</a>\n";
                } else {
                    $nav.= "<a href='#' onclick=\"window.open('$file?popup=true','$name','width=$width,height=$height,toolbar=no,status=no,location=no,scrollbars=no');return false;\">$name</a>\n";
                }
            } else 
            {
                if($locked)
                {
                    $nav.= "<a href='#'>$name</a>\n";
                } else {
                    if ($dbSub['numrows']>0)
                    {
                        $nav.= "<a href='$file' class='dropdown-toggle' data-toggle='dropdown' $target>$name</a>\n";  
                    } else {
                        $nav.= "<a href='$file' $target>$name</a>\n";  
                    }
                      
                }
            }
            if ($dbSub['numrows']>0)
            {
                $nav.= "    <ul class='dropdown-menu'>\n";
                $nav.=$this->submenu($dbSub['data'],$ptest);
                $nav.= "    </ul>\n";
            }
            $nav.= "      </li>\n";
        }   
        return $nav;     
    }
    
    function simplemenu()
    {
        if($primaryaddress!=$_SERVER['SERVER_ADDR'] && $_SESSION['admin']==0)
        {
            $ptest="AND primary_site_only=0";
        } else {
            $ptest='';
        }
        
        
        $nav="<div id='mainmenu' class='jqueryslidemenu ui-widget'>\n";
        $nav.= "<ul>\n";
        $nav.= "  <li class='ui-state-default'><a href='#' onclick=\"javascript:window.open('chatV2.php','Production Chat System','width=520,height=680,toolbar=0,status=0,location=0');return false;\">Chat V2</a></li>\n";
        $nav.= "  <li><a href='index.php'>DASHBOARD</a></li>\n";
        $sql="SELECT * FROM simple_menu ORDER BY sort_order ASC";
        $dbSimple=dbselectmulti($sql);
        if($dbSimple['numrows']>0)
        {
            
            foreach($dbSimple['data'] as $simple)
            {
                $nav.= "  <li><a href='#'>".stripslashes($simple['menu_title'])."</a>\n";
                //lets see if there are any sub items
                $sql="SELECT A.* FROM core_pages A, simple_menu_pages B WHERE A.id=B.page_id AND B.simple_menu_id=$simple[id] ORDER BY A.name";
                $dbSub=dbselectmulti($sql);
                if($dbSub['numrows']>0)
                {
                    $nav.= "      <ul>\n";
                    foreach($dbSub['data'] as $sub)
                    {
                        $nav.= "<li>\n";
                        if (checkPermission($sub['id']))
                        {
                            $lock='';
                            $locked=false;    
                        } else {
                            $locked=true;
                            $lock="<span style='display:inline;margin-right:5px;'><i class='fa fa-lock'></i></span>"; 
                        }
                        $file=$sub['filename'];
                        $name=$sub['name'];
                        $testfile=explode("?",$file);
                        $testfile=$testfile[0];
                        if (!file_exists($testfile) && $file!='')
                        {
                          $name=$lock."$name - no file!";
                          $file="#";
                        } else {
                          $name=$lock.$name;
                        }
                        if ($file==''){$file='#';}
                        $file=URL_ROOT.$file;
                
                        if ($sub['popup'])
                        {
                            $width=$sub['popup_width'];
                            $height=$sub['popup_height'];
                            if($locked)
                            {
                                $nav.= "<a href='#'>$name</a>\n";
                            } else {
                                $nav.= "<a href='#' onclick=\"window.open('$file?popup=true','$name','width=$width,height=$height,toolbar=no,status=no,location=no,scrollbars=no');return false;\">$name</a>\n";
                            }
                        } else 
                        {
                            if($locked)
                            {
                                $nav.= "<a href='#'>$name</a>\n";
                            } else {
                                $nav.= "<a href='$file'>$name</a>\n";    
                            }
                        }
                        $nav.= "</li>\n";
                        
                    }
                    $nav.= "      </ul>\n";
                    
                    
                }
                $nav.= "   </li>\n";
            }
        }    
        $nav.= "</ul>";
        $nav.= "<br style='clear: left' />\n";
        $nav.= "</div>\n";
        print $nav;
    }
    
    
    function loadHeadFiles($type='all')
    {
        $fileRoot = realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR);
        $testroot = end(explode(".com",$fileroot));
        $testRoot = str_replace("/var/www/html","",$testRoot);
        $testRoot = str_replace("/var/www","",$testRoot);
        
        $scriptname=end(explode("/",$_SERVER['SCRIPT_NAME']));
        print "<!-- loading dynamic head files at ROOT=".ROOT." -->\n";
        if($type=='all' || $type='styles')
        {
            print "<!-- loading dynamic style files -->\n";
        //lets load the style sheets
            $sql="SELECT * FROM core_system_files WHERE file_type='style' AND head_load=1 AND active=1 ORDER BY load_order ASC";
            $dbStyles=dbselectmulti($sql);
            if($dbStyles['numrows']>0)
            {
                foreach($dbStyles['data'] as $style)
                {
                   $loadfor=explode(",",$style['specific_page']);
                   if($style['specific_page']=='' || in_array($scriptname,$loadfor))
                   {
                      $this->addFile('style',$style['file_name']);      
                   }       
                }
            }
        }
        if($type=='all' || $type=='scripts')
        {
            print "<!-- loading dynamic script files -->\n";
        
            //lets load the javascript files
            $sql="SELECT * FROM core_system_files WHERE file_type='script' AND head_load=1 AND active=1 ORDER BY load_order ASC";
            $dbScripts=dbselectmulti($sql);
            if($dbScripts['numrows']>0)
            {
                foreach($dbScripts['data'] as $script)
                {
                    $loadfor=explode(",",$script['specific_page']);
                    if($script['specific_page']=='' || in_array($scriptname,$loadfor))
                    {
                        $this->addFile('script',$script['file_name']);  
                    } else {
                        print "<!-- when loading $script[file_name] looked for $script[specific_page] compared to $scriptname -->\n";
                    }       
                }
            }
        }
        
        print "<!-- loading mango javascript libraries -->\n";
        $this->addCoreMangoScripts();
        
        if($type=='all' || $type=='fixed')
        {
            print "<!-- loading fixed head files -->\n";
            $sql="SELECT google_map_key FROM core_preferences";
            $dbPrefs=dbselectsingle($sql);
            $key=stripslashes($dbPrefs['data']['google_map_key']);
            print "<script type='text/javascript' src='//maps.google.com/maps/api/js?key=$key&libraries=drawing'></script>\n
            
            ";
            
        }
    }

    
    /* addScript
    *   Expects a string
    *   adds to global scripts array that is added in footer
    */ 
    function addScript($script)
    {
        $GLOBALS['scripts'][]=$script;
    }
    
    /* addCSS
    *    Expects a string
    *    Appends to a style declaration in the footer
    */
    function addCSS($style)
    {
        $GLOBALS['styles'][]=$style;
    }
    
    // nav
    
    // footer nav
    
    // addProperty
    
    /* addFile (type, link)
    *   Used to a links for scripts and style sheets. 
    *   CSS added to head, scripts added to page bottom
    */
    function addFile($type, $link)
    {
        $cb="_v=".MANGO_VERSION; 
        if($type=='script')
        {
            print "<script type='text/javascript' src='/includes/js_libraries/$link?$cb'></script>\n";    
        } else if($type=='core')
        {
            print "<script type='text/javascript' src='/includes/mango_scripts/$link?$cb'></script>\n";    
        } else {
            print "<link rel='stylesheet' type='text/css' href='/styles/$link?$cb' />\n";
        }
        
    }
    
    function addCoreMangoScripts()
    {
        //core dir is
        $dir=INCLUDE_DIR."mango_scripts";
        $files = scandir($dir);
        foreach($files as $file)
        {
            if($file!='.' && $file!='..' && substr($file,0,13)!="fullcalendar_")
            {
               $this->addFile('core',$file); 
            }
        } 
    }
    
    public function throwError($message)
    {
        ?>
        <div class='row'>
            <div class='container'>
                <div class="jumbotron">
                  <h1>Oops, it looks like we have encountered a super big problem!</h1>
                  <p><?php echo $message ?></p>
                  <p><a class="btn btn-primary btn-lg" href="index.php" role="button">Return to dashboard</a></p>
                </div>
            </div>
        </div>
        
        <?php
        $this->footer();
        die();
    }
    
    function footer()
    {
        global $popup, $User;
        if(!$popup){
        ?>
        <style>
        .adminBar {
            width: 100%;
            height: 350px;
            float:left;
            position:fixed !important;
            left:0;
            right:0;
            bottom:50px;
        }
        </style>
        
         
        <nav id='bottomNav' class="navbar navbar-inverse navbar-fixed-bottom" role='navigation'>
          <div class="container-fluid">
            <div class='navbar-header'>
              <button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#mangoBottomNav' aria-expanded='false'>
                <span class='sr-only'>Toggle navigation</span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
                <span class='icon-bar'></span>
              </button>
            </div>
            
            <div id='mangoBottomNav' class='collapse navbar-collapse'>
             <ul class="nav navbar-nav position-none">
              <a class="navbar-brand" href="changeLog.php">MANGO <small>(v<?php echo MANGO_VERSION ?>)</small></a>
              <li class="active"><a href="index.php">Dashboard</a></li>
              <li class=""><a href="warehouseMenu.php">Warehouse</a></li>
              <?php
                  if($User->isAdmin() || $_GET['debug']=='true')
                  {
                      ?>
              <li id='adminMessages' class="dropdown">
                  <a id='adminToggle' href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" >Admin Panel<b class="caret"></b></a>
              </li>
                    <?php
                  }
              ?>
              <!--
              <li><a href="#">Link</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li>
              -->
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <p class="navbar-text">Active Site: <?php echo $GLOBALS['newspaperName'] ?></p>
              <!--<li><a href="#">Link</a></li>-->
              <?php $User->messages(); ?>
              
            </ul>
            
            </div>
          </div>
        </nav>
    </div> <!--close the wrapper -->
        <?php 
        } 
        
        if(count($GLOBALS['modals']>0))
        {
            print "<!-- generating all necessary modals -->\n";
            foreach($GLOBALS['modals'] as $modal)
            {
                print $modal;
            }
        }  
            ?>
                                                            
        <!-- standard framework modals -->
        <!-- Modal -->
        <div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="changePasswordLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="changePasswordLabel">Change your password</h4>
              </div>
              <div class="modal-body form-horizontal">
                   <div class="form-group">
                    <label for="passwordCurrent" class="col-sm-2 control-label">Current Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="passwordCurrent" placeholder="Password">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="passwordNew" class="col-sm-2 control-label">New Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="passwordNew" placeholder="Password">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="passwordNewVerify" class="col-sm-2 control-label">Password Verify</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="passwordNewVerify" placeholder="Password">
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onClick="updatePassword(<?php echo $User->id ?>)">Update Password</button>
              </div>
            </div>
          </div>
        </div>    
        <script type='text/javascript'>
        /* including page level scripts */
        <?php 
        if(count($GLOBALS['scripts']>0))
        {
            foreach($GLOBALS['scripts'] as $script)
            {
                print $script;
            }
        }          
        ?>
        
    </script>
        <?php 
        if(count($GLOBALS['styles']>0))
        {
        ?>
    <style>
        <?php
        foreach($GLOBALS['styles'] as $style)
        {
            print $style;
        }
        ?>
    </style>
        <?php
        }
        
        $this->endExecution = microtime(true);
        if($User->isAdmin() || $_GET['debug']==true)
        {
            $this->adminBar();
        }
        ?>
        
        <!-- finally initialize the session heartbeat to keep session alive and check for message -->
        <script>
        $(document).ready(function(){
            window.setInterval(heartbeat,120000);
        })
        </script>
    </body>
</html>
        <?php
        dbclose();
    }
    
    private function adminBar()
    {
        ?>
        <style>
        .adminBar {
            width:100%;
            height: 400px;
            left:0;
            bottom:0;
            position:fixed;
            background-color: #dedede;
            border-top: 4px solid black;
            display: none;
            overflow-y: scroll;
            margin-bottom: 50px;
            z-index: 99999999;
        }
        </style>
        <script>
            $( "#adminToggle" ).click(function() {
              $( ".adminBar" ).slideToggle( "slow" );
            });
        </script>
        <div class='adminBar'>
            <div>
              <!-- Nav tabs -->
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#performanceBar" aria-controls="performanceBar" role="tab" data-toggle="tab">Performance</a></li>
                <li role="presentation"><a href="#databaseBar" aria-controls="databaseBar" role="tab" data-toggle="tab">Database</a></li>
                <li role="presentation"><a href="#messagesBar" aria-controls="messagesBar" role="tab" data-toggle="tab">Messages</a></li>
                <li role="presentation"><a href="#sessionBar" aria-controls="sessionBar" role="tab" data-toggle="tab">Session</a></li>
                <li role="presentation"><a href="#userBar" aria-controls="userBar" role="tab" data-toggle="tab">Current User</a></li>
                <li role="presentation"><a href="#preferenceBar" aria-controls="preferenceBar" role="tab" data-toggle="tab">Preferences</a></li>
                <li role="presentation"><a href="#debuggerBar" aria-controls="debuggerBar" role="tab" data-toggle="tab">Debugger</a></li>
                <li role="presentation"><a href="#ajaxBar" aria-controls="ajaxBar" role="tab" data-toggle="tab">Ajax Calls</a></li>
              </ul>

              <!-- Tab panes -->
              <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="performanceBar"><?php $this->adminBarPerformance(); ?></div>
                <div role="tabpanel" class="tab-pane" id="databaseBar"><?php $this->adminBarDatabase(); ?></div>
                <div role="tabpanel" class="tab-pane" id="messagesBar"><?php $this->adminBarMessages(); ?></div>
                <div role="tabpanel" class="tab-pane" id="sessionBar"><?php $this->adminBarSession(); ?></div>
                <div role="tabpanel" class="tab-pane" id="userBar"><?php $this->adminBarUser(); ?></div>
                <div role="tabpanel" class="tab-pane" id="preferenceBar"><?php $this->adminBarPreferences(); ?></div>
                <div role="tabpanel" class="tab-pane" id="debuggerBar"><?php $this->adminBarDebugger(); ?></div>
                <div role="tabpanel" class="tab-pane" id="ajaxBar"><?php $this->adminBarAjax(); ?></div>
              </div>

            </div>
        </div>
        <?php
    }
    
    private function adminBarPerformance()
    {
       print "<table class='table table-bordered table-striped table-condensed'>";
       print "<tr><th>Metric</th><th>Value</th></tr>\n";
       print "<tr><td>Load Time</td><td>".($this->endExecution - $this->startExecution)." seconds</td></tr>";
       print "<tr><td>Memory Usage</td><td>".(memory_get_peak_usage(true))." bytes / ".(memory_get_peak_usage(true)/1024/1024)." mb</td></tr>";
       print "</table>"; 
    }
    
    /*
    * prepares an output of all collected database calls
    */
    private function adminBarDatabase()
    {
       global $databaseCalls;
       print "<table class='table table-bordered table-striped table-condensed'>";
       print "<tr><th>Seq. out (".count($databaseCalls).")</th><th>Results</th><th>Status</th><th>Query</th></tr>\n";
       if(count($databaseCalls)>0)
       {
           $i=1;
           foreach($databaseCalls as $call)
           {
               print "<tr><td>$i</td><td>$call[numrows]</td><td>".($call['error']!='' ? 'error':'success')."</td><td>$call[query]</td></tr>";
           }
       }
       print "</table>";   
    }
    
    /*
    *   Displays any debug messages generated during output;
    */
    private function adminBarMessages()
    {
       print "<pre>";
       print_r(debug_backtrace());
       print "</pre>";
    }
    
    /*
    *  Dumps the $_SESSION super-variable
    */
    private function adminBarSession()
    {
       print "<pre>";
       print_r($_SESSION);
       print "</pre>";  
    }
    
   /*
    *  Dumps the $_SESSION super-variable
    */
    private function adminBarUser()
    {
       global $User;
       print "<pre>";
       print_r($User->getUser());
       print "</pre>";  
    }
    
   /*
    *  Dumps the $_SESSION super-variable
    */
    private function adminBarDebugger()
    {
       global $debugger;
        print "<pre>";
       print_r($debugger);
       print "</pre>";  
    }
    
    /*
    *  Dumps the $_SESSION super-variable
    */
    private function adminBarAjax()
    {
       global $ajaxMessages;
       print "<table id='debugBarAjaxMessages' class='table table-striped tabled-bordered'>\n";
       print "<thead><tr><th>Time</th><th>Source</th><th>Post</th><th>Response</th></tr></thead><tbody>\n";
       if(!empty($ajaxMessages))
       {
           foreach($ajaxMessages as $m)
           {
               print "<tr><td>$m[time]</td><td>$m[source]</td><td>$m[post]</td><td>$m[response]</td></tr>\n";
           }
       }
       print "</tbody></table>\n";
    }
    
   /*
    *  Dumps the $_SESSION super-variable
    */
    private function adminBarPreferences()
    {
       global $Prefs;
       $preferences = $Prefs->showPreferences();
       if(count($preferences)>0)
       {
           print "<table class='table table-condensed table-striped table-bordered'>\n";
           print "<tr><th>Preferenace Name</th><th>Preference Value</th></tr>\n";
           foreach($preferences as $name=>$value)
           {
               print "<tr><td>$name</td><td>$value</td></tr>\n";
           }
           print "</table>\n";
       } 
    }
    
   
}

<?php

/*  Layout Class
*   
*/

class Layout {
    
    private $id = 0; // the layout ID
    private $jobID = 0;
    private $sectionCount = 0;
    private $pages = array(); // array of arrays (section, pages)
    
    
    public function __construct()
    {
        
    }
    
    /*
    *  convert page string to page array of numbers
    *  expects something along the order of:
    *   1-8
    *  1, 4-5, 8
    */
    
    private function pages($pageString)
    {
        $pages = array();
        
        // explode string by comma, then check for hyphens
        $tPageParts = explode(",",$pageString);
        
        
        return $pages;    
    }
    
    
    /*
    *   Generate the press layout
    */
    public function generate()
    {
        
    }
}

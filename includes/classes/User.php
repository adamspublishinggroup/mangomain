<?php

class User {
    
    private static $_instance = null;
    
    public   $firstName = '';
    public   $lastName = '';
    private  $email = '';
    public   $id = 0;
    private  $permissions = array();
    private  $publications = array();
    private  $info = array();
    
    private function __construct($id=0)
    {
        $sql="SELECT * FROM users WHERE id=$id";
        $dbUser=dbselectsingle($sql);
        $this->id = $id;
        $this->info = $dbUser['data'];
        
        $this->setPermissions();
        $this->setPublications();
    }
    
    public static function Instance($id=0)
    {
        if (self::$_instance == null) {
            self::$_instance = new User($id);
        }
        return self::$_instance;
    }
    
    private function setPublications()
    {
        $sql="SELECT pub_id FROM user_publications WHERE value=1 AND user_id=$this->id";
        $dbPublications = dbselectmulti($sql);
        if($dbPublications['numrows']>0)
        {
            foreach($dbPublications['data'] as $pub)
            {
                $this->publications[]=$pub['pub_id'];
            }
        }
    }
    public function getPublications()
    {
        return $this->publications;    
    }
    
    private function setPermissions()
    {
        if($this->isAdmin())
        {
            $sql="SELECT id AS permissionID FROM core_permission_list ORDER BY id";
        } else {
            $sql="SELECT permissionID FROM user_permissions WHERE value=1 AND user_id=$this->id";
        }
        $dbPermissions = dbselectmulti($sql);
        if($dbPermissions['numrows']>0)
        {
            foreach($dbPermissions['data'] as $perm)
            {
                $this->permissions[]=$perm['permissionID'];
            }
        }
    }
    
    public function getPermissions()
    {
        return $this->permissions;    
    }
    
    public function hasPermission($permissionID)
    {
        $GLOBALS['debugger'][]=array('action'=>'Checking in User->hasPermission for a permission id','permissionID'=>$permissionID);
        return in_array($permissionID,$this->permissions);
    }
    
    public function canAccess($page)
    {
        if ($this->isAdmin())
        {
            return true;
        }
        //looking up by script name rather than id
        //get the permissions for the page that have a value of 1 -- those are required
        $sql="SELECT A.permissionID, C.displayname FROM core_permission_page A, core_pages B, core_permission_list C 
        WHERE A.value=1 AND A.pageID=B.id AND B.filename='$pageID' AND A.permissionID=C.id";
        $dbPagePermissions=dbselectmulti($sql);
        if ($dbPagePermissions['numrows']>0)
        {
            $valid=false;
            foreach($dbPagePermissions['data'] as $permission)
            {
                $comparisonPermission=$permission['permissionID'];
                if ($this->hasPermission($comparisonPermission)){$valid=true;}
            }
        } else {
            $valid=true;
        }
        return $valid; 
    }
    
    public function canEdit($pubID)
    {
        if ($this->isAdmin())
        {
            return true;
        }
        if(in_array($pubID, $this->publications))
        {
           $access = true; 
        } else {
           $access = false; 
        }
        
        return $access; 
    }
    
    public function isAdmin()
    {
        return $this->info['admin'];
    }
    
    public function name()
    {
        return $this->info['firstname'].' '.$this->info['lastname'];
    }
    
    public function getUser()
    {
        $data['user']=$this->info;
        $data['permissions']=$this->permissions;
        return $data;
    }
    
    public function messages()
    {
        $sql="SELECT * FROM user_messages WHERE user_id='$this->id' ORDER BY sent_time ASC";
        $dbMessages = dbselectmulti($sql);
        
        $messageCount = $dbMessages['numrows'];
        
        if($messageCount>0)
        {
            $maxID = 0;
            foreach($dbMessages['data'] as $mess)
            {
                if($mess['id']>$maxID){$maxID=$mess['id'];}
            }
        }
        
        $sql="SELECT * FROM mango_news WHERE site_id=".SITE_ID." AND id NOT IN (SELECT news_id FROM mango_news_viewed WHERE user_id=$this->id)";
        $dbNews=dbselectmulti($sql);
        $messageCount+=$dbNews['numrows'];   
        
        
         ?>
        <li id='userMessagesHolder' class="dropdown <?php echo ($messageCount>0 ? 'open':''); ?>">
              
        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><span id='message_label'>Messages <span class='badge'><?php echo $messageCount; ?> </span></span><b class="caret"></b></a>
        <ul id='userMessages' data-latest="<?= $maxID ?>" class="dropdown-menu" style="padding: 15px;right:0!important;width: 300px !important;max-height:350px;overflow-y:scroll;">
            <!--
            <li>
               <div class="row">
                  <div class="col-md-12">
                     <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                        <div class="form-group">
                           <label class="sr-only" for="exampleInputEmail2">Email address</label>
                           <input type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" required>
                        </div>
                        <p>Could do a reply style thing in the item as well...</p>
                     </form>
                  </div>
               </div>
            </li>
            <li class="divider"></li>
            -->
            <?php
            if($dbNews['numrows']>0)
            {
                foreach($dbNews['data'] as $newsitem)
                {
                    //who wrote it?
                    $sql="SELECT firstname,lastname FROM users WHERE id=$newsitem[post_by]";
                    $dbAuthor=dbselectsingle($sql);
                    print "<li>";
                        print "<div style='width:100%;padding:4px;margin: 10px 4px;background-color: #efefef;border-radius:5px;border: 1px solid black;'>";
                        print "<h4>".stripslashes($newsitem['headline'])."</h4>\n";
                        
                        print "<p><small>By ".$dbAuthor['data']['firstname'].' '.$dbAuthor['data']['lastname']." at ".date("m/d H:i",strtotime($newsitem['post_datetime']))."</small></p>";
                        print "<p>".stripslashes($newsitem['message'])."</p>";
                       
                        print "</div>\n";
                    print "</li>";
                    
                    //now add it to the mango_news_viewed so the user doesn't have to see it again
                    $sql="INSERT INTO mango_news_viewed (news_id, user_id) VALUES ('$newsitem[id]', $this->id)";
                    $dbInsert=dbinsertquery($sql);
                }
            }
           if($dbMessages['numrows']>0)
           {
               foreach($dbMessages['data'] as $message)
               {
                   print "<li><div style='width:100%;padding:4px;margin: 10px 4px;background-color: #efefef;border-radius:5px;border: 1px solid black;'>";
                   if($message['title']!='')
                   {
                       print "<h4>".stripslashes($message['title'])."</h4>\n";
                   }
                   if($message['origination_id']=='0')
                   {
                       $user="System";
                   } else {
                       $user=$GLOBALS['users'][$message['origination_id']];
                   }
                   print "<p><small>From ".$user." at ".date("m/d H:i",strtotime($message['sent_time']))."</small></p>";
                   print "<p>".stripslashes($message['message'])."</p>";
                   
                   print "</div>\n</li>\n";
               }
           }
           ?>
        </ul>
        </li>
        <?php
        if($messageCount>0) { 
            ?>
        <script>
        var handleHover = function(evObj){
            var $this = $(this);

            if(!$this.hasClass('open')){
                return true;
            }

            if(evObj.type === 'mouseleave'){
                //    on mouseleave...
                handleHover.timer = setTimeout(function(){
                    $this.removeClass('open');
                },4000);

            } else if(evObj.type === 'mouseenter'){
                //    on mouseenter...
                if(handleHover.timer){    //    find timer and clear it
                    clearTimeout(handleHover.timer);
                    delete handleHover.timer;
                }
            }
        };
        setTimeout(function(){
            $('#userMessages').removeClass('open');
        },4000);
        $(window).on('load',function() {
          // Run code
          // do fading 3 times
          for(i=0;i<3;i++) {
            $("#message_label").fadeTo('slow', 0.2).fadeTo('slow', 1.0);
          }
        });
        
        $('#userMessages').hover(handleHover);
        </script>
        <?php
        }
        //purge old messages for user
        $purgeTime = date("Y-m-d H:i:s");
        $purgeStickyTime = date("Y-m-d H:i:s",strtotime("-10 minutes")); // hang on to stickys no more than 10 minutes for now
        //keep anything marked as Sticky for now
        // @TODO need an ajax-y method for user to manually clear all "sticky" messages
        $sql="DELETE FROM user_messages WHERE (expire_time<='$purgeTime' AND sticky=0) OR (expire_time<='$purgeStickyTime' AND sticky=1)";
        $dbRemove=dbexecutequery($sql);
    }
}
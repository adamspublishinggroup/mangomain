<?php
/*
*    create some form variables that are global in score

*/
$tableSearch = '';
$tableOptions = '';

//print a text box
function labelStart($label, $id, $type='normal',$explain='')
{
    if($type=='normal')
    {
    ?>
    <div class="form-group">
        <label for="<?php echo $id ?>" class="col-sm-2 control-label"><?php echo $label ?></label>
        <div class="col-sm-10">
    <?php
    if($explain!='')print "<small>$explain</small><br>";
    } elseif($type=='checkbox')
    {
        ?>
    <div class="form-group">
        <label for="<?php echo $id ?>" class="col-sm-2 control-label"><?php echo $label ?></label>
        <div class="col-sm-10">
            <div class="checkbox">
        <label>
      <?php
    }
}

function labelEnd($inputHTML='',$type='normal')
{
   if($type=='normal')
   {
    print $inputHTML;
    print "</div>
  </div>
  ";
   } else {
       print $inputHTML;
       print "</label>
      </div>
    </div>
  </div>
   ";
   }
}

function tableStart($formoptions,$headers,$cols=0,$searchblock='',$sortcol='false',$hasAction=true)
{
    global $tableSearch, $tableOptions;
    $tableOptions = $formoptions;
    $tableSearch = $searchblock;
    ?>
    <div class='row'>



    <div class='col xs-12 col-sm-9'> <!-- opens up the div for the table! -->
    <div class='table-responsive' style='min-height:75vh !important;'>
    <?php
    $headers=explode(",",$headers);
    $h=0;
    print "<table id='stable' class='table table-striped table-bordered table-hover' cellspacing='0' width='100%'>\n<thead>\n<tr>\n";
    foreach($headers as $key=>$header)
    {
        print "<th>$header</th>";
        $h++;
    }
	if( $hasAction ) {
		print "<th style='min-width:130px;'>Actions</th>";
	}
    print "</tr>\n</thead>
    <tbody>
    ";
}

function tableEnd($set=array('numrows'=>'0'),$extrascript='',$sortcol=0,$sortdir='asc',$stateSave='false')
{
    global $tableSearch, $tableOptions;
    if($_POST['stateSave']){$stateSave='true';}
    if($set['numrows']>10){$limit=10;}else{$limit=$set['numrows'];}
    print '</tbody>
    </table>';
    //wrap up, we're not using anything fancy other than the delete box
        ?>
    <script>
    $('.delete').on('click', function (e) {
      var obj = this;
          e.preventDefault();
          bootbox.confirm({
              message: "This item will be permanently deleted and cannot be recovered. Are you sure?",
                buttons: {
                    confirm: {
                        label: '<i class="fa fa-trash"></i> Yes',
                        className: 'btn-danger'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-default'
                    }
                },
                callback: function (result) {
                    if(result) window.location = obj.href;
                }
          });
    });

    $('#stable').dataTable( {
        paging: true,
        pageLength: 25,
        pagingType: "full_numbers",
        lengthChange: true,
        search: {
            smart: true
          },
        stateSave: <?php echo $stateSave ?>,
        order: [[ <?php echo $sortcol ?>, <?php echo '"'.$sortdir.'"'; ?> ]]
    });
    <?php echo $extrascript; ?>

   </script>
   </div>
   </div>
   <div class='col xs-12 col-sm-3'>
        <div id='tableoptions' class='well'>
           <p><strong>Actions:</strong></p>
            <?php print implode("<br>",explode(",",$tableOptions)); ?>
        <?php if ($tableSearch!=''){?>
           <p><strong>Search:</strong></p>
            <?php print $tableSearch; ?>
        <?php } ?>
        </div>
    </div>
   </div> <!-- close row -->
   <?php

}


/*
function input_text($element_name, $value, $size='', $disabled=false, $onchange='',$onfocus='',$onblur='',$onkeypress='',$onkeydown='',$onkeyup='',$validation='',$onclick='',$label='',$explain_text='')
{
    if ($label!='')
    {
        print "<div class='label'>$label</div><div class='input'>";
    }
    $temp.='<input type="text" id="' . $element_name .'" name="' . $element_name .'"';
    if ($disabled) {$temp.= " readonly";}
    $temp.= " value=\"$value\"";
    if ($size!='') {
        $temp.= " size=$size";
    }
    if ($onchange!=''){
        $temp.=" onchange=\"$onchange\"";
    }
    if ($onfocus!=''){
        $temp.=" onfocus=\"$onfocus\"";
    }
    if ($onblur!=''){
        $temp.=" onblur=\"$onblur\"";
    }
    if ($onkeypress!=''){
        $temp.=" onkeypress=\"$onkeypress\"";
    }
    if ($onkeydown!=''){
        $temp.=" onkeydown=\"$onkeydown\"";
    }
    if ($onkeyup!=''){
        $temp.=" onkeyup=\"$onkeyup\"";
    }
    if ($onclick!=''){
        $temp.=" onclick=\"$onclick\"";
    }
    if ($validation!=''){
        $temp.=" $validation";
    }
    $temp.="></input>";
    if ($label!="")
    {
        if ($explain_text!=''){print "<small>$explain_text</small><br>\n";}
        print $temp;
        print "</div>\n";
        print "<div class='clear'></div>\n";
    } else {
       return $temp;
    }
}

//print a submit button
function input_submit($element_name, $button_name, $disabled=false)
{
    print '<input type="submit" id="' . $element_name .'" name="' . $element_name .'" value="';
    print htmlentities($button_name) .'"';
    if ($disabled) {print " disabled";}
    print '></input>';
}

//print a textarea
function input_textarea($element_name, $value, $cols='40',$rows='5',$mce=true,$disabled=false,$onchange='',$onfocus='',$onblur='',$onkeypress='',$onkeydown='',$onkeyup='',$validation='',$label='',$explain_text='')
{
    if ($label!='')
    {
        print "<div class='label'>$label</div><div class='input'>";
    }

    if ($mce){$meditor=" class=\"GuiEditor\"";}else{$meditor=" class=\"noGuiEditor\"";}
    $temp.= "<textarea id=\"$element_name\" name=\"$element_name\" cols=\"$cols\" rows=\"$rows\"$meditor";
    if ($disabled) {print " readonly";}
    if ($onchange!=''){
        $temp.=" onchange=\"$onchange\"";
    }
    if ($onfocus!=''){
        $temp.=" onfocus=\"$onfocus\"";
    }
    if ($onblur!=''){
        $temp.=" onblur=\"$onblur\"";
    }
    if ($onkeypress!=''){
        $temp.=" onkeypress=\"$onkeypress\"";
    }
    if ($onkeydown!=''){
        $temp.=" onkeydown=\"$onkeydown\"";
    }
    if ($onkeyup!=''){
        $temp.=" onkeyup=\"$onkeyup\"";
    }
    if ($validation!=''){
        $temp.=" $validation";
    }

    $temp.= '>';
    $temp.=htmlentities($value) ;

    $temp.= '</textarea>';
    if ($label!='')
    {
        print "<small>$explain_text</small><br>\n";
        print $temp;
        print "</div>\n<div class='clear'></div>\n";

    } else {
        return $temp;
    }
}

//print a radio button or checkbox
function input_radiocheck($type, $element_name, $element_value=0, $onclick='', $validation='',$label='',$explain_text='')
{
    if ($label!='')
    {
        print "<div class='label'>$label</div><div class='input'>";
    }
    $temp="<input type='$type' id='$element_name' name='$element_name'";
    if ($element_value) {
        $temp.=" checked='checked'";
    }
    if ($validation!=''){
        $temp.=" $validation";
    }
    if (!$onclick=='') {
        $temp.= "onclick='$onclick'></input>\n";
    } else {
        $temp.= "></input>\n";
    }
    if ($label!='')
    {
        print $temp."<label for='$element_name'>$explain_text</label>\n";
        print "</div>\n<div class='clear'></div>\n";
    } else {
        return $temp;
    }
}

function input_checkbox($element_name, $element_value=0, $onclick='', $validation='',$label='',$explain_text='', $groupclass='')
{
    if ($label!='')
    {
        print "<div class='label'>$label</div><div class='input'>";
    }
    $temp="<input type='checkbox' id='$element_name' name='$element_name'";
    if ($element_value) {
        $temp.=" checked='checked'";
    }
    if ($validation!=''){
        $temp.=" $validation";
    }
    if ($groupclass!=''){
        $temp.=" class='$groupclass'";
    }
    if (!$onclick=='') {
        $temp.= "onclick='$onclick'></input>\n";
    } else {
        $temp.= "></input>\n";
    }
    if ($label!='')
    {
        print $temp."<label for='$element_name'>$explain_text</label>\n";
        print "</div>\n<div class='clear'></div>\n";
    } else if ($explain_text!='') {
        $temp.="<label for='$element_name'>$explain_text</label>\n";
        return $temp;
    } else {
        return $temp;
    }
}

//print a password text field
function input_password($element_name, $values, $validation='')
{
    print "<input type=\"password\" id=\"$element_name\" name=\"$element_name\" value=\"";
    print htmlentities($values[$element_name]) ."\" $validation></input>\n";
}

//print a <select> menu
function input_select($element_name, $selected, $options, $multiple = false, $action='', $disabled=false, $validation='',$label='',$explain_text='',$stylewidth=0)
{
    if ($label!='')
    {
        print "<div class='label'>$label</div><div class='input'>";
    }

    // print out the <select> tag
    $temp="<select ";
    // if multiple choices are permitted, add the multiple attribute
    // and add a [] to the end of the tag name
    if ($multiple) {
        $temp.= "name='".$element_name."[]' id='".$element_name."[]'  multiple='multiple'";
    } else {
        $temp.= "name='$element_name' id='$element_name'";
    }
    if (!$action=='') {$temp.=' onChange="'.$action.'"';}
    if ($validation!=''){$temp.= " $validation";}
    if($stylewidth>0)
    {
        $temp.=" style='width:".$stylewidth."px'";
    }
    if ($disabled) {
        $temp.= " disabled>\n";
    } else {
        $temp.= " >\n";
    }

    // set up the list of things to be selected
    $selected_options = array();
    if ($multiple) {
        foreach ($selected[$element_name] as $val) {
            $selected_options[$val] = true;
        }
    } else {
        $selected_options[ $selected[$element_name] ] = true;
    }

    // print out the <option> tags
    foreach ($options as $option => $option_label) {
        $temp.="<option value='". ($option) . "'";
        if ($selected==($option_label)) {
            $temp.=" selected='selected'";
        }
        $temp.= ">" . htmlentities($option_label) . "</option>\n";
    }
    $temp.="</select>\n";
    if ($label!='')
    {
        print $temp."<small>$explain_text</small>\n";
        print "</div>\n<div class='clear'></div>\n";
    } else {
        return $temp."\n";
    }
}

function input_date($element_name,$date,$label='',$explain_text='')
{
    if ($label!='')
    {
        print "<div class='label'>$label</div><div class='input'>";
    }
    //$temp="<div><script>DateInput('$element_name', true, 'YYYY-MM-DD','$date')</script></div>\n";
    $temp="<input type='text' name='$element_name' id='$element_name' value='$date'/><script type='text/javascript'>$('#$element_name').datepicker({ dateFormat: 'yy-mm-dd' });</script>\n";

    if ($label!='')
    {
        print $temp;
        print "</div>\n<div class='clear'></div>\n";
    } else {
        return $temp;
    }
}

function input_time($element_name,$hour,$minute,$label='',$explain_text='')
{
    $minutes=array();
    for ($i=0;$i<=60;$i++){
        if ($i<10){
            $minutes["0$i"]="0$i";
        }else{
            $minutes[$i]=$i;
        }
    }
    $hours=array("00"=>"Midnight",
                    "01"=>"1 am",
                    "02"=>"2 am",
                    "03"=>"3 am",
                    "04"=>"4 am",
                    "05"=>"5 am",
                    "06"=>"6 am",
                    "07"=>"7 am",
                    "08"=>"8 am",
                    "09"=>"9 am",
                    "10"=>"10 am",
                    "11"=>"11 am",
                    "12"=>"Noon",
                    "13"=>"1 pm",
                    "14"=>"2 pm",
                    "15"=>"3 pm",
                    "16"=>"4 pm",
                    "17"=>"5 pm",
                    "18"=>"6 pm",
                    "19"=>"7 pm",
                    "20"=>"8 pm",
                    "21"=>"9 pm",
                    "22"=>"10 pm",
                    "23"=>"11 pm"
                );

    $temp=input_select($element_name."_hour",$hours[$hour],$hours).":";
    $temp.=input_select($element_name."_minute",$minutes[$minute],$minutes);
    if ($label!='')
    {
        print "<div class='label'>$label</div><div class='input'>";
        if ($explain_text!=''){print "<small>$explain_text</small><br>\n";}
        print $temp;
        print "</div>\n<div class='clear'></div>\n";
    } else {
        return $temp;
    }

}

function input_datetime($element_name,$datetime,$label='',$explain_text='')
{

    $temp="<input type='text' name='$element_name' id='$element_name' value='$date'/>
    <script type='text/javascript'>$('#$element_name').datetimepicker();</script>\n";
    if ($label!='')
    {
        print "<div class='label'>$label</div><div class='input'>";
        if ($explain_text!=''){print "<small>$explain_text</small><br>\n";}
        print $temp;
        print "</div>\n<div class='clear'></div>\n";
    } else {
        return $temp;
    }
}

function input_color($element_name,$color,$label='',$explain_text='')
{
    if ($label!='')
    {
        print "<div class='label'>$label</div><div class='input'>";
    }
    print input_text($element_name,$color,'10',false,'','','','','','','','',$label,$explain_text);
    ?>
    <script type='text/javascript'>
    $('#<?php echo $element_name; ?>').ColorPicker({
    onSubmit: function(hsb, hex, rgb, el) {
        $(el).val(hex);
        $(el).ColorPickerHide();
    },
    onBeforeShow: function () {
        $(this).ColorPickerSetColor(this.value);
    },
    onChange: function (hsb, hex, rgb) {
        $(this).css('backgroundColor', '#' + hex);
    }
})
.bind('keyup', function(){
    $(this).ColorPickerSetColor(this.value);
});

</script>
    <?php

    if ($label!='')
    {
        print "</div>\n<div class='clear'></div>\n";
    }
}

function input_address($element_name,$location_name='',$location_street='',$location_city='',$location_state='ID',$location_zip='',$label='',$explain_text='')
{
    $temp="Location Name: ";
    $temp.=input_text($element_name."_name",$location_name,50);
    $temp.="\n<br>Street Address: ";
    $temp.=input_text($element_name."_street",$location_street,50);
    $temp.="\n<br><div style='float:left;margin-right:15px;'>City: ";
    $temp.=input_text($element_name."_city",$location_city,30);
    $temp.="</div>\n<div style='float:left;'>State: ";
    $temp.=input_select($element_name."_state",$GLOBALS['states'][$location_state],$GLOBALS['states']);
    $temp.="</div>\n<div style='float:left;margin-left:15px;'>Zip: \n";
    $temp.=input_text($element_name."_zip",$location_zip,10);
    $temp.="</div>\n<div class='clear'></div>\n";
    if ($label!='')
    {
        print "<div class='label'>$label</div><div class='input'>";
        if ($explain_text!=''){print "<small>$explain_text</small><br>\n";}
        print $temp;
        print "</div>\n<div class='clear'></div>\n";
    } else {
        return $temp;
    }
}

*/



//below here are the new versions of these functions, with more logical placement of the elements
function make_text($element_name, $value, $label='',$explain_text='', $size=50, $validation='',$disabled=false, $onchange='',$onfocus='',$onblur='',$onkeypress='',$onkeydown='',$onkeyup='',$onclick='',$maxlength=255,$placeholder='')
{
    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);
    }
    $temp.='<input class="form-control" type="text" id="' . $element_name .'" name="' . $element_name .'"';
    if ($disabled) {$temp.= " readonly";}
    $temp.= " value=\"$value\"";
    if ($size!='') {
        $temp.= " size=$size";
    }
    if ($placeholder!='') {
        $temp.= " placeholder='$placeholder'";
    } else {
        $temp.=" placeholder='$label'";
    }
    if ($onchange!=''){
        $temp.=" onchange=\"$onchange\"";
    }
    if ($onfocus!=''){
        $temp.=" onfocus=\"$onfocus\"";
    }
    if ($onblur!=''){
        $temp.=" onblur=\"$onblur\"";
    }
    if ($onkeypress!=''){
        $temp.=" onkeypress=\"$onkeypress\"";
    }
    if ($onkeydown!=''){
        $temp.=" onkeydown=\"$onkeydown\"";
    }
    if ($onkeyup!=''){
        $temp.=" onkeyup=\"$onkeyup\"";
    }
    if ($onclick!=''){
        $temp.=" onclick=\"$onclick\"";
    }
    if ($validation!=''){
        $temp.=" $validation";
    }
    $temp.=" maxlength=$maxlength";
    $temp.="/>";
    if ($label!="")
    {
        labelEnd($temp);
    } else {
       return $temp;
    }
}


function make_email($element_name, $value, $label='',$explain_text='',$confirm=false)
{
    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);
    }
    $temp="<input class='form-control' type='email' id='$element_name' name='$element_name' value='$value' size=50 maxlength=255 />";
    if ($confirm)
    {
        $temp.="<br /><input class='form-control' type='email' size=20 id='".$element_name."_confirm' name='".$element_name."_confirm'
        value='$value' maxlength=255 onblur='if(\$(this).value!=\$(\"$element_name\").val()){\$('#".$element_name."_error').show()}else{\$('#".$element_name."_error').hide()}'/>";
        
        $GLOBALS['scripts'][]="
        var myForm = \$('#$element_name').closest('form');

        $( myForm ).validate({
          rules: {
            $element_name: {
              email: true
            },
            ".$element_name."_confirm: {
                equalTo : '#$element_name'
            }
          }
        });
        ";
    }
    if ($label!="")
    {
        labelEnd($temp);
    } else {
       return $temp;
    }
}

function make_number($element_name, $value, $label='',$explain_text='', $validation='',$disabled=false, $onchange='',$onfocus='',$onblur='',$onkeydown='',$onkeyup='',$onclick='')
{
    if ($label!='')
    {
       labelStart($label,$element_name,'normal',$explain_text);
    }
    $temp.='<input type="number" id="' . $element_name .'" name="' . $element_name .'"';
    if ($disabled) {$temp.= " readonly";}
    $temp.= " value=\"$value\"";
    $temp.= " size=10";

    if ($validation!=''){
        $temp.=" $validation";
    }
    if ($onchange!=''){
        $temp.=" onChange=\"$onchange\"";
    }
    if ($onchange!=''){
        $temp.=" onChange=\"$onchange\"";
    }
    if ($onfocus!=''){
        $temp.=" onFocus=\"$onfocus\"";
    }
    if ($onblur!=''){
        $temp.=" onBlur=\"$onblur\"";
    }
    if ($onkeydown!=''){
        $temp.=" onKeydown=\"$onkeydown\"";
    }
    if ($onkeyup!=''){
        $temp.=" onKeyup=\"$onkeyup\"";
    }
    $temp.=" step='0.01'></input>";
    if ($label!="")
    {
        labelEnd($temp);
    } else {
       return $temp;
    }
}

function make_number_range($low_element,$high_element, $low_value, $high_value, $label='',$explain_text='', $step=1,$disabled=false, $onchange='',$onfocus='',$onblur='',$onkeydown='',$onkeyup='',$onclick='')
{
    if ($label!='')
    {
       labelStart($label,$element_name,'normal',$explain_text);
    }
    $temp.='<span><b>Low:</b> <input type="number" id="' . $low_element .'" name="' . $low_element .'"';
    if ($disabled) {$temp.= " readonly";}
    $temp.= " value=\"$low_value\"";

    if ($onchange!=''){
        $temp.=" onChange=\"$onchange\"";
    }
    if ($onfocus!=''){
        $temp.=" onFocus=\"$onfocus\"";
    }
    if ($onblur!=''){
        $temp.=" onBlur=\"$onblur\"";
    }
    if ($onkeydown!=''){
        $temp.=" onKeydown=\"$onkeydown\"";
    }
    if ($onkeyup!=''){
        $temp.=" onKeyup=\"$onkeyup\"";
    }
    $temp.=" step='$step'></input></span>";
    $temp.='<span style="margin-left:20px;"><b>High:</b> <input type="number" id="'. $high_element .'" name="'. $high_element .'"';
    if ($disabled) {$temp.= " readonly";}
    $temp.= " value=\"$high_value\"";

    if ($onchange!=''){
        $temp.=" onChange=\"$onchange\"";
    }
    if ($onfocus!=''){
        $temp.=" onFocus=\"$onfocus\"";
    }
    if ($onblur!=''){
        $temp.=" onBlur=\"$onblur\"";
    }
    if ($onkeydown!=''){
        $temp.=" onKeydown=\"$onkeydown\"";
    }
    if ($onkeyup!=''){
        $temp.=" onKeyup=\"$onkeyup\"";
    }
    $temp.=" step='$step'></input></span>";
    if ($label!="")
    {
        labelEnd($temp);
    } else {
       return $temp;
    }
}

//print a submit button
function make_submit($element_name, $button_name, $label='',$disabled=false,$onclick='',$buttonclass='btn-dark')
{
    ?>
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <input type="submit" id='submit' name='submit' class="btn <?php echo $buttonclass ?>" <?php if ($disabled) {print " disabled";}
    if ($onclick!='') {print " onclick=\"$onclick\"";} ?> value="<?php echo $button_name ?>" />
    </div>
  </div>
    <?php
}

//print a regular button
function make_button($element_name, $button_name, $label='',$disabled=false,$onclick='',$buttonclass='btn-dark')
{
    ?>
    <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="button" class="btn <?php echo $buttonclass ?>" <?php if ($disabled) {print " disabled";}
    if ($onclick!='') {print " onclick=\"$onclick\"";} ?>><?php echo $button_name ?></button>
    </div>
  </div>
    <?php
}

//print a textarea
function make_textarea($element_name, $value, $label='',$explain_text='',$cols='80',$rows='15',$mce=true,$validation='',$disabled=false,$onchange='',$onfocus='',$onblur='',$onkeypress='',$onkeydown='',$onkeyup='')
{
    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);
    }

    if ($mce){$meditor=" class=\"GuiEditor\"";}else{$meditor=" class=\"noGuiEditor\"";}
    $temp.= "<textarea id=\"$element_name\" name=\"$element_name\" cols=\"$cols\" rows=\"$rows\"$meditor";
    if ($disabled) {print " readonly";}
    if ($onchange!=''){
        $temp.=" onchange=\"$onchange\"";
    }
    if ($onfocus!=''){
        $temp.=" onfocus=\"$onfocus\"";
    }
    if ($onblur!=''){
        $temp.=" onblur=\"$onblur\"";
    }
    if ($onkeypress!=''){
        $temp.=" onkeypress=\"$onkeypress\"";
    }
    if ($onkeydown!=''){
        $temp.=" onkeydown=\"$onkeydown\"";
    }
    if ($onkeyup!=''){
        $temp.=" onkeyup=\"$onkeyup\"";
    }
    if ($validation!=''){
        $temp.=" $validation";
    }

    $temp.= ">$value</textarea>\n";
    if ($label!='')
    {
        labelEnd($temp);
    } else {
        return $temp;
    }

}

//print a radio button or checkbox
function make_radiocheck($type, $element_name, $element_value=0, $label='', $explain_text='',$validation='', $onclick='')
{
    if ($label!='')
    {
        labelStart($label,$element_name,'checkbox',$explain_text);
    }
    $temp="<input type='$type' id='$element_name' name='$element_name'";
    if ($element_value) {
        $temp.=" checked='checked'";
    }
    if ($validation!=''){
        $temp.=" $validation";
    }
    if (!$onclick=='') {
        $temp.= "onclick='$onclick' />\n";
    } else {
        $temp.= "/>\n";
    }
    if ($label!='')
    {
       labelEnd($temp,'checkbox');
    } else {
        return $temp;
    }
}

//print a checkbox
function make_checkbox($element_name, $element_value=0, $label='', $explain_text='',$validation='', $onclick='', $groupclass='')
{
    if ($label!='')
    {
        labelStart($label,$element_name,'checkbox','');
    }
    $temp="<input type='checkbox' id='$element_name' name='$element_name'";
    if ($element_value) {
        $temp.=" checked='checked'";
    }
    if ($validation!=''){
        $temp.=" $validation";
    }
    if ($groupclass!=''){
        $temp.=" class='$groupclass'";
    }
    if (!$onclick=='') {
        $temp.= "onclick='$onclick' />\n";
    } else {
        $temp.= "/>\n";
    }
    $temp.=" $explain_text";
    if ($label!='')
    {
        labelEnd($temp,'checkbox');
    } else {
        return $temp;
    }
}

//print a password text field
function make_password($element_name, $value, $label='', $explain_text='',$confirm=false)
{
    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);
    }
    $temp="<input class='input' type='password' id='$element_name' name='$element_name' value='$value' ></input>\n";
    if ($confirm)
    {
        $temp.="<br /><input class='input' type='password' id='".$element_name."_confirm' name='".$element_name."_confirm'
         value='$value' onblur='if(this.value!=document.getElementById(\"$element_name\").value){alert(\"Passwords do not match!\");}' />\n";

    }
    if ($label!='')
    {
        labelEnd($temp);
    } else {
        return $temp;
    }
}

//print a hidden text field
function make_hidden($element_name, $value)
{
    print "<input type='hidden' id='$element_name' name='$element_name' value='$value' />\n";

}

function make_file($element_name, $label='', $explain_text='',$filetoshow='', $buttonclass='submit')
{
    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);

    }
    $temp="<input class='input' type=\"file\" id=\"$element_name\" name=\"$element_name\" class=\"$buttonclass\"/ onblur=\"document.getElementById('".$element_name."_hidden').value=this.value;\">\n";
    $temp.="<input type='hidden' id='".$element_name."_hidden' name='".$element_name."_hidden' value=''/>\n";
    if ($label!='')
    {
        print $temp;
        $fileparts=explode("/",$filetoshow);

        if ($filetoshow!='' && file_exists($filetoshow) && end($fileparts)!='')
        {
            print "<br /><img src='$filetoshow' height=100 border=0>\n";
        }
        labelEnd('');
    } else {
        return $temp;
    }
}

function make_multifile($element_name,$path,$label,$explain_text)
{
    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);
    }
    $temp="<div id='$element_name'></div>\n";
    $temp.="
    <script type='text/javascript'>
      \$(document).ready(function () {
        var currentDir='$path';
        \$('#$element_name').fineUploader({
          request: {
            endpoint: 'includes/ajax_handlers/fineUploaderHandler.php'
          },
          debug: true,
          validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
            sizeLimit: 5000 * 1024 //kb size
          },
          failedUploadTextDisplay: {
            mode: 'custom',
            maxChars: 40,
            responseProperty: 'error',
            enableTooltip: true
          },
          debug: true
        })
        .on('submit', function(event, id, filename) {
             \$(this).fineUploader('setParams', {'dir': '$path'});
          })
      });
    </script>
    ";
    if ($label!='')
    {
        labelEnd($temp);
    } else {
        return $temp;
    }
}

//print a <select> menu
function make_select($element_name, $selected, $options, $label='',$explain_text='',$validation='',$editable = false, $action='', $disabled=false, $multiple=false, $handler='')
{
    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);
    }
    $temp='';
    if($editable)
    {
        $temp.="<div class='input-group'>\n";
    }
    // print out the <select> tag
    $temp.="  <select ";
    $temp.= "name='$element_name".(($multiple)?'[]':'')."' id='$element_name' class='form-control'  autocomplete='off' ";
    if (!$action=='') {$temp.=' onChange="'.$action.'"';}
    if ($validation!=''){$temp.= " $validation";}
    if ($multiple){$temp.= " multiple";}
    if(!$editable)
    {
        $temp.=" style='width:80%;'";
    }
    if ($disabled) {
        $temp.= " disabled>\n";
    } else {
        $temp.= ">\n";
    }

    // print out the <option> tags
    foreach ($options as $option => $option_label) {
        $temp.="    <option value='". ($option) . "'";
        if(is_array($selected))
        {
            if(in_array($option_label,$selected))
            {
                $temp.=" selected";
            }
        } else {
            if ($selected==$option_label) {
                $temp.=" selected";
            }
        }
        $temp.= ">" . ($option_label) . "</option>\n";
    }
    $temp.="  </select>\n";
    if($editable && $handler!='')
    {
        $temp.="<span class='input-group-addon' id='".$element_name."_mod' data-toggle='modal' data-target='#".$element_name."_selectCustomOptionModal'><i class='fa fa-plus'></i></span></div>\n";

        //going to add a script to the global script array
        $GLOBALS['modals'][]="
        <!-- modal for select box $element_name custom option -->
        <div id='".$element_name."_selectCustomOptionModal' class='modal fade' tabindex='-1' role='dialog' aria-labelledby='selectCustomOptionModalLabel$element_name'>
          <div class='modal-dialog' role='document'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
                    <h4 class='modal-title' id='selectCustomOptionModalLabel$element_name'>Add custom option</h4>
                  </div>
                <div class='modal-body form-horizontal'>
                  <input id='".$element_name."_custom' name='".$element_name."_custom' type='text' class='form-control' />
                </div>
                <div class='modal-footer'>
                    <button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
                    <button type='button' class='btn btn-primary' onclick='saveSelectOption(\"$element_name\",\"$handler\");'>Save changes</button>
                </div>
            </div>
          </div>
        </div>
        ";
    }
    if ($label!='')
    {
        labelEnd($temp);
    } else {
        return $temp."\n";
    }

}


function make_state($element_name, $selected, $label='',$explain_text='')
{
    $states=array("AL"=>"ALABAMA",
    "AK"=>"ALASKA",
    "AZ"=>"ARIZONA",
    "AR"=>"ARKANSAS",
    "CA"=>"CALIFORNIA",
    "CO"=>"COLORADO",
    "CT"=>"CONNECTICUT",
    "DE"=>"DELAWARE",
    "DC"=>"DISTRICT OF COLUMBIA",
    "FL"=>"FLORIDA",
    "GA"=>"GEORGIA",
    "GU"=>"GUAM",
    "HI"=>"HAWAII",
    "ID"=>"IDAHO",
    "IL"=>"ILLINOIS",
    "IN"=>"INDIANA",
    "IA"=>"IOWA",
    "KS"=>"KANSAS",
    "KY"=>"KENTUCKY",
    "LA"=>"LOUISIANA",
    "ME"=>"MAINE",
    "MD"=>"MARYLAND",
    "MA"=>"MASSACHUSETTS",
    "MI"=>"MICHIGAN",
    "MN"=>"MINNESOTA",
    "MS"=>"MISSISSIPPI",
    "MO"=>"MISSOURI",
    "MT"=>"MONTANA",
    "NE"=>"NEBRASKA",
    "NV"=>"NEVADA",
    "NH"=>"NEW HAMPSHIRE",
    "NJ"=>"NEW JERSEY",
    "NM"=>"NEW MEXICO",
    "NY"=>"NEW YORK",
    "NC"=>"NORTH CAROLINA",
    "ND"=>"NORTH DAKOTA",
    "OH"=>"OHIO",
    "OK"=>"OKLAHOMA",
    "OR"=>"OREGON",
    "PW"=>"PALAU",
    "PA"=>"PENNSYLVANIA",
    "PR"=>"PUERTO RICO",
    "RI"=>"RHODE ISLAND",
    "SC"=>"SOUTH CAROLINA",
    "SD"=>"SOUTH DAKOTA",
    "TN"=>"TENNESSEE",
    "TX"=>"TEXAS",
    "UT"=>"UTAH",
    "VT"=>"VERMONT",
    "VA"=>"VIRGINIA",
    "VI"=>"VIRGIN ISLANDS",
    "WA"=>"WASHINGTON",
    "WV"=>"WEST VIRGINIA",
    "WI"=>"WISCONSIN",
    "WY"=>"WYOMING"
    );

    labelStart($label,$element_name,'normal',$explain_text);

    // print out the <select> tag
    $temp="<select class='input' ";
    // if multiple choices are permitted, add the multiple attribute
    // and add a [] to the end of the tag name
    $temp.= "name='$element_name' id='$element_name'";
    $temp.= ">\n";


    // print out the <option> tags
    foreach ($states as $option => $option_label) {
        $temp.="<option value='". ($option) . "'";
        if ($selected==$option_label || $selected==$option) {
            $temp.=" selected='selected'";
        }
        $temp.= ">" . ($option_label) . "</option>\n";
    }
    $temp.="</select>\n";
    if ($label!='')
    {
        labelEnd($temp);
    } else {
        return $temp."\n";
    }
}
/*

function make_date($element_name,$date,$label='',$explain_text='')
{
    //just in case date is empty, set to today
    if ($date=='')
    {
        $date=date("Y-m-d");
    }
    //just in case, we're breaking date to two items and using only the first
    $date=explode(" ",$date);
    $date=$date[0];

    if ($label!='')
    {
       labelStart($label,$element_name,'normal',$explain_text);
    }
    //$temp="<div><script>DateInput('$element_name', true, 'YYYY-MM-DD','$date')</script></div>\n";
    $temp="<input type='text' name='$element_name' id='$element_name' value='$date'/><script type='text/javascript'>\$('#$element_name').datepicker({ dateFormat: 'yy-mm-dd' });</script>\n";

    if ($label!='')
    {
        labelEnd($temp);
    } else {
        return $temp;
    }
}

function make_time($element_name,$time,$label='',$explain_text='')
{
    if($label!=''){labelStart($label,$element_name,'normal',$explain_text);};

    $temp="<input type='text' name='$element_name' id='$element_name' value='$time'/>
    <script type='text/javascript'>$('#$element_name').timepicker();</script>\n";
    if ($label!='')
    {
        labelEnd($temp);
    } else {
        return $temp;
    }

}

function make_datetime($element_name,$datetime,$label='',$explain_text='',$stepMinute=1,$minDate='',$maxDate='')
{
    if($label!=''){labelStart($label,$element_name,'normal',$explain_text);}
    $datetime=date("Y-m-d H:i",strtotime($datetime));
    $temp="<input type='text' name='$element_name' id='$element_name' value='$datetime'/>
    <script type='text/javascript'>
    \$('#$element_name').datetimepicker({
        dateFormat: 'yy-mm-dd',
        stepMinute: $stepMinute";
    if($minDate!='')
    {
        $tempDate=explode(" ",$minDate);
        $tempTime=$tempDate[1];
        $tempTime=explode(":",$tempTime);
        $tempDate=explode("-",$tempDate[0]);
        $tempMonth=$tempDate[1]-1;
        $temp.=",\nminDateTime: new Date($tempDate[0], $tempMonth, $tempDate[2], $tempTime[0], $tempTime[1])";
    }
    if($maxDate!='')
    {
        $tempDate=explode(" ",$maxDate);
        $tempTime=$tempDate[1];
        $tempTime=explode(":",$tempTime);
        $tempDate=explode("-",$tempDate[0]);
        $tempMonth=$tempDate[1]-1;
        $temp.=",\nmaxDateTime: new Date($tempDate[0], $tempMonth, $tempDate[2], $tempTime[0], $tempTime[1])";
    }
    $temp.="\n});
    </script>\n";
    if ($label!='')
    {
        labelEnd($temp);
    } else {
        return $temp;
    }
}*/

function make_date($element_name,$date,$label='',$explain_text='',$step=1,$minDate='',$maxDate='',$empty=false)
{
    //just in case date is empty, set to today
    if ($date=='' && !$empty)
    {
        $date=date("Y-m-d");
    }
    //just in case, we're breaking date to two items and using only the first
    $date=explode(" ",$date);
    $date=$date[0];
    if(!$empty) $options=",
    defaultDate: '$date'";
    if($minDate!='')
    {
        $options.=",
        minDate: '".date("Y-m-d",strtotime($minDate))."'";
    }
    if($maxDate!='')
    {
        $options.=",
        maxDate: '".date("Y-m-d",strtotime($maxDate))."'";
    }
    $temp='';
    if ($label!='')
    {
       $temp="<div class='form-group'>
        <label for='$id' class='col-sm-2 control-label'>$label</label>
        <div class='col-sm-10'>
        ";
    }
    if($explain_text!=''){$temp.="<small>$explain_text</small><br>";}
    $temp.="
    <div class='input-group date col-xs-3' style='min-width:200px;' id='".$element_name."_tp'>
        <input type='text' class='form-control' name='$element_name' id='$element_name' value='$date'/>
        <span class='input-group-addon'>
            <i class='fa fa-calendar'></i>
        </span>
    </div>
    <script>
    \$('#".$element_name."_tp').datetimepicker({format: 'YYYY-MM-DD',
    useCurrent: true,
    showTodayButton: true,
    icons: {
        time: 'fa fa-clock-o',
     }
    $options
    })
    </script>
    ";
    if ($label!='')
    {
        $temp.="        </div>
        </div>
        ";
        print $temp;
    } else {
        return $temp;
    }
}

function make_time($element_name,$time,$label='',$explain_text='')
{
    //just in case date is empty, set to today
    if ($time=='')
    {
        $time=date("H:i");
    }
    //just in case, we're breaking date to two items and using only the first
    $temp='';
    if ($label!='')
    {
       $temp="<div class='form-group'>
        <label for='$id' class='col-sm-2 control-label'>$label</label>
        <div class='col-sm-10'>
        ";
    }
    if($explain_text!=''){$temp.="<small>$explain_text</small><br>";}
    $temp.="
    <div class='input-group date col-xs-3' style='min-width:200px;' id='".$element_name."_tp'>
        <input type='text' class='form-control' name='$element_name' id='$element_name' value='$time'/>
        <span class='input-group-addon'>
            <i class='fa fa-calendar'></i>
        </span>
    </div>
    <script>
    \$('#".$element_name."_tp').datetimepicker({format: 'HH:mm',
    useCurrent: true,
    showTodayButton: true,
    icons: {
        time: 'fa fa-clock-o',
     }
    });
    </script>
    ";
    if ($label!='')
    {
        $temp.="        </div>
        </div>
        ";
        print $temp;
    } else {
        return $temp;
    }
}

function make_datetime($element_name,$datetime,$label='',$explain_text='',$stepMinute=1,$minDate='',$maxDate='')
{
    //just in case date is empty, set to today
    if ($datetime=='')
    {
        $datetime=date("Y-m-d");
    }
    //just in case, we're breaking date to two items and using only the first
    $options="defaultDate: '$datetime'";
    if($minDate!='')
    {
        $options.=",
        minDate: '".date("Y-m-d H:i",strtotime($minDate))."'";
    }
    if($maxDate!='')
    {
        $options.=",
        maxDate: '".date("Y-m-d H:i",strtotime($maxDate))."'";
    }
    $temp='';
    if ($label!='')
    {
       $temp="<div class='form-group'>
        <label for='$id' class='col-sm-2 control-label'>$label</label>
        <div class='col-sm-10'>
        ";
    }

    if($explain_text!=''){$temp.="<small>$explain_text</small><br>";}
    $temp.="
    <div class='input-group date col-xs-3' style='min-width:200px;' id='".$element_name."_tp'>
        <input type='text' class='form-control' name='$element_name' id='$element_name' value='$date'/>
        <span class='input-group-addon'>
            <i class='fa fa-calendar'></i>
        </span>
    </div>
    <script>
    \$('#".$element_name."_tp').datetimepicker({format: 'YYYY-MM-DD HH:mm',
    useCurrent: true,
    showTodayButton: true,
    sideBySide: true,
    icons: {
        time: 'fa fa-clock-o',
     },
     $options
    });
    </script>
    ";
    if ($label!='')
    {
        $temp.="        </div>
        </div>
        ";
        print $temp;
    } else {
        return $temp;
    }
}

function make_color($element_name,$color,$label='',$explain_text='')
{
    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);

    }
    if ($explain_text!=''){print "<small>$explain_text</small><br>\n";}
    $temp="<input type='text' id='$element_name' name='$element_name' value='$color' size='10' style='background-color:$color;'></input>\n";
    $temp.="
    <script type='text/javascript'>
    \$('#$element_name').ColorPicker({
    onSubmit: function(hsb, hex, rgb, el) {
        \$(el).val(hex);
        \$(el).ColorPickerHide();
    },
    onBeforeShow: function () {
        \$(this).ColorPickerSetColor(this.value);
    },
    onChange: function (hsb, hex, rgb) {
        \$('#$element_name').css('backgroundColor', '#' + hex);
        \$('#$element_name').val('#' + hex);
    }
    })
    .bind('keyup', function(){
        \$(this).ColorPickerSetColor(this.value);
    });
</script>
";
    if ($label!='')
    {
       labelEnd($temp);
    } else {
       return $temp;
    }
}

function make_address($element_name,$location_name='',$location_street='',$location_city='',$location_state='ID',$location_zip='',$label='',$explain_text='')
{
    $states=array("AL"=>"ALABAMA",
    "AK"=>"ALASKA",
    "AZ"=>"ARIZONA",
    "AR"=>"ARKANSAS",
    "CA"=>"CALIFORNIA",
    "CO"=>"COLORADO",
    "CT"=>"CONNECTICUT",
    "DE"=>"DELAWARE",
    "DC"=>"DISTRICT OF COLUMBIA",
    "FL"=>"FLORIDA",
    "GA"=>"GEORGIA",
    "GU"=>"GUAM",
    "HI"=>"HAWAII",
    "ID"=>"IDAHO",
    "IL"=>"ILLINOIS",
    "IN"=>"INDIANA",
    "IA"=>"IOWA",
    "KS"=>"KANSAS",
    "KY"=>"KENTUCKY",
    "LA"=>"LOUISIANA",
    "ME"=>"MAINE",
    "MD"=>"MARYLAND",
    "MA"=>"MASSACHUSETTS",
    "MI"=>"MICHIGAN",
    "MN"=>"MINNESOTA",
    "MS"=>"MISSISSIPPI",
    "MO"=>"MISSOURI",
    "MT"=>"MONTANA",
    "NE"=>"NEBRASKA",
    "NV"=>"NEVADA",
    "NH"=>"NEW HAMPSHIRE",
    "NJ"=>"NEW JERSEY",
    "NM"=>"NEW MEXICO",
    "NY"=>"NEW YORK",
    "NC"=>"NORTH CAROLINA",
    "ND"=>"NORTH DAKOTA",
    "OH"=>"OHIO",
    "OK"=>"OKLAHOMA",
    "OR"=>"OREGON",
    "PW"=>"PALAU",
    "PA"=>"PENNSYLVANIA",
    "PR"=>"PUERTO RICO",
    "RI"=>"RHODE ISLAND",
    "SC"=>"SOUTH CAROLINA",
    "SD"=>"SOUTH DAKOTA",
    "TN"=>"TENNESSEE",
    "TX"=>"TEXAS",
    "UT"=>"UTAH",
    "VT"=>"VERMONT",
    "VA"=>"VIRGINIA",
    "VI"=>"VIRGIN ISLANDS",
    "WA"=>"WASHINGTON",
    "WV"=>"WEST VIRGINIA",
    "WI"=>"WISCONSIN",
    "WY"=>"WYOMING"
    );


    $temp="Location Name: ";
    $temp.=input_text($element_name."_name",$location_name,50);
    $temp.="\n<br>Street Address: ";
    $temp.=input_text($element_name."_street",$location_street,50);
    $temp.="\n<br><div style='float:left;margin-right:15px;'>City: ";
    $temp.=input_text($element_name."_city",$location_city,30);
    $temp.="</div>\n<div style='float:left;'>State: ";
    $temp.=input_select($element_name."_state",$states[$location_state],$states);
    $temp.="</div>\n<div style='float:left;margin-left:15px;'>Zip: \n";
    $temp.=input_text($element_name."_zip",$location_zip,10);
    $temp.="</div>\n<div class='clear'></div>\n";

    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);
        labelEnd($temp);
    } else {
        return $temp;
    }
}

function make_phone($element_name,$phonenumber,$label='',$explain_text='')
{
    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);
    }
    $temp.='<input type="text" id="' . $element_name .'" name="' . $element_name .'"';
    if ($disabled) {$temp.= " readonly";}
    $temp.= " value=\"$phonenumber\"";
    $temp.=" />";
    $temp.="<script>\$('#$element_name').mask('(999) 999-9999');</script>";
    if ($label!="")
    {
        labelEnd($temp);
    } else {
       return $temp;
    }
}


function make_slider($element_name,$value,$label,$explain_text,$min=0,$max=100,$increment=1)
{
    if ($label!='')
    {
        labelStart($label,$element_name,'normal',$explain_text);

    }
    $temp="<input type='text' id='".$element_name."' name='".$element_name."' style='border:0; color:#f6931f; font-weight:bold;' />";
    $temp.="<div id='".$element_name."_slider'></div>";
    ?>
    <script>
    $(function() {
        $( "#<?php echo $element_name; ?>_slider" ).slider({
            range: "min",
            value: <?php echo intval($value);?>,
            min: <?php echo intval($min);?>,
            max: <?php echo intval($max);?>,
            step: <?php echo intval($increment);?>,
            slide: function( event, ui ) {
                $( "#<?php echo $element_name; ?>" ).val(ui.value);
            }
        });
        $( "#<?php echo $element_name; ?>" ).val($("#<?php echo $element_name; ?>_slider").slider( "value" ));
    });
    </script>
    <?php
    if ($label!="")
    {
        labelEnd($temp);
    } else {
       return $temp;
    }
}

function make_descriptor($text,$label='Information')
{
    labelStart($label,'','normal','');
    labelEnd($text);
}

function safe_text($text,$dir)
{
    if ($dir=='in') {
        $text=str_replace('\r\n','<br />',$text);
        $text=str_replace('\n','<br />',$text);
        //$text=htmlentities($text,ENT_QUOTES);
        $text=addslashes($text);
    } else {
        $text=stripslashes($text);
        //$text=html_entity_decode ( $text, ENT_QUOTES);
        $text=str_replace('<br />','\n',$text);
    }
    return $text;
}

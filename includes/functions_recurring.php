<?php
  /*
  * recurring functions
  */
  
  
  function findRecurrences($job,$startDate)
  {
      global $test;
      //first step is to figure out the end date
      $endDate = findEndDate($job);
       
      //if we don't have a valid end date, lets just skip this one
      if ($endDate == null) return false;
      $returnDates = array();
      //now we need to start by looking at the different possible frequency types
      
      if($job['specified_date']!=0)
      {
          $newDates=findSpecificDates($job,$startDate,$endDate);
          $returnDates = array_merge($newDates,$returnDates);
      } else {
          $repetitions = explode("|",$job['recur_frequency']);
          if(count($repetitions)>0)
          {
              foreach($repetitions as $freq)
              {
                  switch ($freq)
                  {
                        //$recurFrequencies=array("Every Week","Every Other Week","Every 3rd Week","Every 4th Week", 
                        //"On the first","On the second", "On the third", "On the fourth", "On the last");
                      case "0":
                          //every week
                          $newDates=findWeeklyDates($job,$startDate,$endDate,1);
                      break;
                     
                      case "1":
                          //every other week
                          $newDates=findWeeklyDates($job,$startDate,$endDate,2);
                      break;
                        
                      case "2":
                          //every 3rd week
                          $newDates=findWeeklyDates($job,$startDate,$endDate,3); 
                      break;
                        
                      case "3":
                         //every 4th week
                         $newDates=findWeeklyDates($job,$startDate,$endDate,4);  
                      break;
                      
                      case "4":
                          //first occurrence of that day
                          $newDates=findMonthlyDates($job,$startDate,$endDate,1); 
                      break;
                        
                      case "5":
                          //second occurrence of that day
                          $newDates=findMonthlyDates($job,$startDate,$endDate,2); 
                      break;
                      
                      case "6":
                          //third occurrence of that day
                          $newDates=findMonthlyDates($job,$startDate,$endDate,3); 
                      break;
                        
                      case "7":
                          //fourth occurrence of that day
                          $newDates=findMonthlyDates($job,$startDate,$endDate,4); 
                      break;
                        
                      case "8":
                          //last occurrence of that day
                          $newDates=findMonthlyDates($job,$startDate,$endDate,'last');   
                      break;
                        
                  }
                  $returnDates = array_merge($newDates,$returnDates);
              }
          }
      }
      
      return $returnDates;      
  }
  
  function findEndDate($job)
  {
      global $test;
      if($job['end_date_checked']) 
      { 
          //set the end date to the specified end date if that check mark is set
          $endDate = $job['end_date'];
      } else {
          //set end date out the days_out days past today
          $endDate = date("Y-m-d",strtotime("+$job[days_out] days"));
      }
      //if we are past the end date, don't process this one
      if(time()>strtotime($endDate)) return null;
      
      //return the date we calculate
      return $endDate; 
  } 
  
  
  /*
  * This simple function merely advances the months with the specific day of the month and returns all dates between start and end month
  */
  function findSpecificDates($job,$startDate,$endDate)
  {
      $dates = array();
      
      $dom = $job['specified_date'];
      
      $startDate = strtotime($startDate);
      $startYear = date("Y",$startDate);
      $startMonth = date("m",$startDate);
      
      $testDate = $startYear."-".$startMonth."-$dom";
      
      if(strtotime($testDate)>=strtotime($startDate))
      {
          $dates[]=$testDate;
      }
       
      $currentMonth = $startMonth;
      $currentYear = $startYear;
      //loop until we are past the enddate
      while(strtotime($testDate)<=strtotime($endDate))
      {
          //add a month
          $currentDate = strtotime($currentYear."-".$currentMonth."-$dom"." +1 month");
          $currentYear = date("Y",$currentDate);
          $currentMonth = date("m",$currentDate);
         
          $testDate = date("Y-m-d",$currentDate);
           
         
          if(strtotime($testDate)>=strtotime($startDate) && strtotime($testDate)<=strtotime($endDate))
          {
              $dates[]=$testDate;
          }
      }
                    
      return $dates;
  }
  
  
  function findWeeklyDates($job,$startDate,$endDate,$weekSkipper)
  {
      $dates = array();
      
      //we'll start by finding the DoW of the $startDate
      $startDoW = date("w",strtotime($startDate));
             
      //lets get the days of the week we'll be looking for
      $days = explode("|",$job['days_of_week']);
      if(count($days)>0)
      {
          foreach($days as $day)
          {
               
              //if $day == $startDoW then we immediately have a winner
               if($day == $startDoW)
               {
                   $initialDate=$startDate;
               } else {
                   //we'll need to add days until we get to the day
                   //if $day == 3 && $startDate = 2 for example, we'll need to 6 (3 to 6, then 0 to 2 for 3 more)
                   if($day>$startDoW){
                       $daysToAdd = $day-$startDoW;
                   } else {
                       $daysToAdd = (6-$startDoW)+($day+1);
                   }
                   $initialDate=date("Y-m-d",strtotime($startDate." +$daysToAdd days"));
               }
               if(strtotime($initialDate)<=strtotime($endDate))
               {
                   $dates[]=$initialDate;
               }
               //now that we have an initial date, we'll need to loop adding 7*weekSkipper days until we exceed endDate
               $checkDate = $initialDate;
               while(strtotime($checkDate)<=strtotime($endDate))
               {
                   $factor = 7*$weekSkipper;                                                                                                                        
                   $checkDate = date("Y-m-d",strtotime($checkDate."+$factor days"));
                   if(strtotime($checkDate)<=strtotime($endDate))
                   {
                       $dates[]=$checkDate;
                   }                       
               }
          }
      }
      return $dates;
  }
                 
  
  /*
  * This function will return the 1st, 2nd, 3rd, 4th, or last specified days of the month
  */
  function findMonthlyDates($job,$startDate,$endDate,$nthDay)
  {
      $dates = array();
      $daysOfWeek = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
      
      $nthDayText = array(1=>'first', 2=>'second', 3=>'third', 4=>'fourth', 'last'=>'last');
      
      $nthDay = $nthDayText[$nthDay];       
      //lets get the days of the week we'll be looking for
      $days = explode("|",$job['days_of_week']);
      if(count($days)>0)
      {
          foreach($days as $day)
          {
               $textDay = $daysOfWeek[$day];
               
               $startYear = date("Y",strtotime($startDate));
               $startMonth = date("F",strtotime($startDate));
               $startMonthNum = date("m",strtotime($startDate));
               
               
               $testDate = date("Y-m-d",strtotime("$nthDay $textDay $startMonth $startYear"));
               
               if(strtotime($testDate)>=strtotime($startDate))
               {
                   //print "Should have added $testDate<br>";
                   $dates[]=$testDate;
               }  else {
                   //print "$testDate was less than $startDate<br>";
               }
               
               //print "We should now have a startDate = $startDate, testDate= $testDate<br>";
               $currentMonthNum = $startMonthNum;
               $currentYear = $startYear;
               //loop until we are past the enddate
               while(strtotime($testDate)<=strtotime($endDate))
               {
                   //add a month
                   $currentDate = strtotime($currentYear."-".$currentMonthNum."-1"." +1 month");
                   $currentYear = date("Y",$currentDate);
                   $currentMonthNum = date("m",$currentDate);
                   $currentMonth = date("F",$currentDate);
                   
                   $testDate = date("Y-m-d",strtotime("$nthDay $textDay $currentMonth $currentYear"));
                   
                   //print "We should now have a currentDate = $currentDate, testDate= $testDate - nthDay = $nthDay textDay = $textDay<br>";
                   
                   if(strtotime($testDate)>=strtotime($startDate) && strtotime($testDate)<=strtotime($endDate))
                   {
                       $dates[]=$testDate;
                   } else {
                       //print "$testDate was less than $startDate and greater than $endDate<br>";
                   }
               }
               
               
          }   
      } 
      
      return $dates;   
  }
  
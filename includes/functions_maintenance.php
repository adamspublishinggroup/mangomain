<?php
  /* maintenance related functions */
  
  
  
function inserterWearTear($packageid)
{
    //stations is an array of all stations actively used
    //jobs_inserter_rundata and jobs_inserter_rundata_stations hold all the information we need
    
    //also need some information from the package
    $sql="SELECT * FROM jobs_inserter_packages WHERE id=$packageid";
    $dbPackage=dbselectsingle($sql);
    $package=$dbPackage['data'];
    $inserterid=$package['inserter_id'];
    
    $sql="SELECT * FROM jobs_inserter_rundata WHERE package_id='$packageid'";
    $dbRunData=dbselectsingle($sql);
    $runData=$dbRunData['data'];
    
    $runningtime=$runData['run_length'];
    $calctotalpieces=$runData['calc_total_pieces'];
    //lets only do this if wear has not been applied
    if(!$runData['wear_applied'])
    {
        //grab all those stations used in this package with an insert quantity>0
        $sql="SELECT * FROM jobs_inserter_rundata_stations WHERE package_id='$packageid' AND quantity>0";
        $dbStationData=dbselectmulti($sql);
        
        $otherequipment=explode("|",$runData['other_equipment']);
        
        //lets apply the proper amount of wear for each station
        if($dbStationData['numrows']>0)
        {
            foreach($dbStationData['data'] as $station)
            {
                //update the parts
                $sql="UPDATE part_instances SET cur_time=cur_time+$runningtime, cur_count=cur_count+$station[quantity] 
                WHERE equipment_id='$inserterid' AND component_id='$station[station_id]' AND replaced=0";
                $dbUpdatePart=dbexecutequery($sql);
                $error.=$dbUpdatePart['error'];
                //update the PM tasks
                $sql="UPDATE pm_instances SET cur_time=cur_time+$runningtime, cur_count=cur_count+$station[quantity] 
                WHERE equipment_id='$inserterid' AND component_id='$station[station_id]' AND completed=0";
                $dbUpdatePart=dbexecutequery($sql);
                $error.=$dbUpdatePart['error'];
                
                //update the station itself
                $sql="UPDATE inserters_hoppers SET running_cycles=running_cycles+$station[quantity], 
                running_time=running_time+$runningtime WHERE id=$station[station_id]";
                $dbUpdateStations=dbexecutequery($sql);
                $error.=$dbUpdateStations['error'];
                     
            }
        }
        
        //now, lets update any other equipment
        if(count($otherequipment)>0)
        {
            foreach($otherequipment as $key=>$id)
            {
                if($id!='')
                {
                    //update the parts
                    $sql="UPDATE part_instances SET cur_time=cur_time+$runningtime, cur_count=cur_count+$calctotalpieces 
                    WHERE equipment_id='$id' AND replaced=0";
                    $dbUpdatePart=dbexecutequery($sql);
                    $error.=$dbUpdatePart['error'];
                    
                    //update the PM tasks
                    $sql="UPDATE pm_instances SET cur_time=cur_time+$runningtime, cur_count=cur_count+$calctotalpieces 
                    WHERE equipment_id='$id' AND completed=0";
                    $dbUpdatePart=dbexecutequery($sql);
                    $error.=$dbUpdatePart['error'];
                    
                } 
            }
        }
    
        //now set wear applied to true
        $sql="UPDATE jobs_inserter_rundata SET wear_applied=1 WHERE id=$runData[id]";
        $dbUpdate=dbexecutequery($sql);
    }
    if($error!='')
    {
        setUserMessage('Problems applying wear and tear to the inserter for this package.<br>'.$error,'error');
    }    
}


function pressWearTear($jobid,$debug=false)
{
    $sql="SELECT * FROM jobs WHERE id=$jobid";
    $dbJob=dbselectsingle($sql);
    $jobinfo=$dbJob['data'];
    
    $layoutid=$jobinfo['layout_id'];
    $pagewidth=$jobinfo['pagewidth'];
    $jobid=$jobinfo['id'];
    
    $sql="SELECT * FROM job_stats WHERE job_id=$jobid";
    $dbStats=dbselectsingle($sql);
    $stats=$dbStats['data'];
    
    if($debug)
    {
        print "Job & stat details<br/><pre>";
        print_r($jobinfo);
        print "<br />";
        print_r($stats);
        print "</pre>\n";
    }
    
    $schedstart=$jobinfo['startdatetime'];
    $schedstop=$jobinfo['enddatetime'];
    $draw=$jobinfo['draw'];     
    $pubid=$jobinfo['pub_id'];
    $runid=$jobinfo['run_id'];
    $pubdate=$jobinfo['pub_date'];
    $folder=$jobinfo['folder'];
    $goodtime=$stats['goodcopy_actual'];
    $actualstart=$stats['startdatetime_actual'];
    $actualstop=$stats['stopdatetime_actual'];
    //math for run length calculation
    //get minutes between actual stop and start
    $start=strtotime($actualstart);
    $stop=strtotime($actualstop);
    
    $startoffset=round(($start-strtotime($schedstart))/60,2);
    $finishoffset=round(($stop-strtotime($schedstop))/60,2);
    $schedruntime=round((strtotime($schedstop)-strtotime($schedstart))/60,2);
    $printdate=date("Y-m-d",strtotime($startD));
    $downtime=$stats['total_downtime'];
    $runningtime=round(($stop-$start)/60,2); //should give us running time in decimal minutes
    $goodrunningtime=$runningtime-$downtime;
    
    $counterstart=$stats['counter_start'];
    $counterstop=$stats['counter_stop'];
    $spoilsstartup=$stats['spoils_startup'];
    
    //calculations for waste, speeds, spoils
    $gross=$counterstop-$counterstart;
    $spoilstotal=$gross-$draw;
    $spoilsrunning=$spoilstotal-$spoilsstartup;
    if($draw>0)
    {
        $wastepercent=round($spoilstotal/$draw*100,2);
    } else {
        $wastepercent=0;
    }
    if($runningtime>0)
    {
        $runspeed=round($gross/($runningtime/60),0);
    } else {
        $runspeed=0;
    }
    if($goodrunningtime>0)
    {
        $goodrunspeed=round($gross/($goodrunningtime/60),0);
    } else {
        $goodrunspeed=0;
    }
    $pressid=$jobinfo['press_id'];
    
    $sql="SELECT * FROM job_paper WHERE job_id='$jobid'";
    $dbPaper=dbselectmulti($sql);
    if($dbPaper['numrows']>0)
    {
        //update the folder and ribbon decks used for this job
        $lsql="SELECT * FROM layout WHERE id=$layoutid";
        $dbLayout=dbselectsingle($sql);
        if ($dbLayout['data']['ribbon1_used']){
            $sql="UPDATE press_towers SET impressions=impressions+$gross, running_time=running_time+".round($runningtime)." WHERE press_id='$pressid' AND tower_name='Ribbon Deck 1'";
            if($debug)
            {
                print "Updating Ribbon Deck 1 with:<br />$sql<br />";
            }
            $dbUpdateRibbon=dbexecutequery($sql);
            $error.=$dbUpdateRibbon['error'];
        }
        if ($dbLayout['data']['ribbon2_used']){
            $sql="UPDATE press_towers SET impressions=impressions+$gross, running_time=running_time+".round($runningtime)." WHERE press_id='$pressid' AND tower_name='Ribbon Deck 2'";
            $dbUpdateRibbon=dbexecutequery($sql);
            $error.=$dbUpdateRibbon['error'];
            if($debug)
            {
                print "Updating Ribbon Deck 2 with:<br />$sql<br />";
            }
            
        }
            
        //grab towers from stats tower_info
        $towers=explode("|",$stats['tower_info']);
        foreach($towers as $ptower)
        {
            $ptower=explode(",",$ptower);
            $towerid=$ptower[0];
            
            //update the towers themselves
            $sql="UPDATE press_towers SET impressions=impressions+$gross, 
            running_time=running_time+".round($runningtime)." WHERE id=$towerid";
            $dbTowerUpdate=dbexecutequery($sql);
            if($debug)
            {
                print "Updating Press Tower id=$towerid with:<br />$sql<br />";
            }
            
            //update all part instances
            $sql="UPDATE part_instances SET cur_time=cur_time+".round($runningtime).", 
            cur_count=cur_count+$gross WHERE equipment_id=$pressid AND component_id=$towerid 
            AND equipment_type='printing' AND replaced=0";
            $dbTowerPartUpdate=dbexecutequery($sql);
            $error.=$dbTowerPartUpdate['error'];
            if($debug)
            {
                print "Updating Part Instances for towerid=$towerid & equipment id=$pressid:<br />$sql<br />";
            }
            
            //update all PM instances
            $sql="UPDATE pm_instances SET cur_time=cur_time+".round($runningtime).", 
            cur_count=cur_count+$gross WHERE equipment_id=$pressid AND component_id=$towerid 
            AND equipment_type='printing' AND replaced=0";
            $dbTowerPartUpdate=dbexecutequery($sql);
            $error.=$dbTowerPartUpdate['error'];
            if($debug)
            {
                print "Updating PM Tasks for towerid=$towerid & equipment id=$pressid:<br />$sql<br />";
            }
            
            //update any equipment "owned" by those towers
            $sql="SELECT * FROM press_towers WHERE id=$towerid";
            $dbTower=dbselectsingle($sql);
            $tower=$dbTower['data'];
            $stackers=explode("|",$tower['stackers']);
            $strappers=explode("|",$tower['strappers']);
            $splicers=explode("|",$tower['splicers']);
            $counterveyors=explode("|",$tower['counterveyors']);
            if(count($stackers)>0)
            {
                foreach($stackers as $key=>$tid)
                {
                    if($tid!='')
                    {
                        $sql="UPDATE part_instances SET cur_time=cur_time+".round($runningtime).", 
                        cur_count=cur_count+$gross WHERE equipment_id=$tid AND equipment_type='generic' 
                        AND replaced=0";
                        $dbTowerPartUpdate=dbexecutequery($sql);
                        $error.=$dbTowerPartUpdate['error'];
                        $sql="UPDATE pm_instances SET cur_time=cur_time+".round($runningtime).", 
                        cur_count=cur_count+$gross WHERE equipment_id=$tid AND equipment_type='generic' 
                        AND replaced=0";
                        $dbTowerPartUpdate=dbexecutequery($sql);
                        $error.=$dbTowerPartUpdate['error'];
                        if($debug)
                        {
                            print "Updating Stakcer for towerid=$towerid & equipment id=$pressid:<br />$sql<br />";
                        }
                    }    
                }    
            }
            if(count($strappers)>0)
            {
                foreach($strappers as $key=>$tid)
                {
                    if($tid!='')
                    {
                        $sql="UPDATE part_instances SET cur_time=cur_time+".round($runningtime).", 
                        cur_count=cur_count+$gross WHERE equipment_id=$tid AND equipment_type='generic' 
                        AND replaced=0";
                        $dbTowerPartUpdate=dbexecutequery($sql);
                        $error.=$dbTowerPartUpdate['error'];
                        $sql="UPDATE pm_instances SET cur_time=cur_time+".round($runningtime).", 
                        cur_count=cur_count+$gross WHERE equipment_id=$tid AND equipment_type='generic' 
                        AND replaced=0";
                        $dbTowerPartUpdate=dbexecutequery($sql);
                        $error.=$dbTowerPartUpdate['error'];
                         if($debug)
                        {
                            print "Updating Strapper for towerid=$towerid & equipment id=$pressid:<br />$sql<br />";
                        }
                    }    
                }    
            }
            if(count($splicers)>0)
            {
                foreach($splicers as $key=>$tid)
                {
                    if($tid!='')
                    {
                        $sql="UPDATE part_instances SET cur_time=cur_time+".round($runningtime).", 
                        cur_count=cur_count+$gross WHERE equipment_id=$tid AND equipment_type='generic' 
                        AND replaced=0";
                        $dbTowerPartUpdate=dbexecutequery($sql);
                        $error.=$dbTowerPartUpdate['error'];
                        $sql="UPDATE pm_instances SET cur_time=cur_time+".round($runningtime).", 
                        cur_count=cur_count+$gross WHERE equipment_id=$tid AND equipment_type='generic' 
                        AND replaced=0";
                        $dbTowerPartUpdate=dbexecutequery($sql);
                        $error.=$dbTowerPartUpdate['error'];
                        if($debug)
                        {
                            print "Updating Splicer for towerid=$towerid & equipment id=$pressid:<br />$sql<br />";
                        }
                    }    
                }    
            }
            
            if(count($counterveyors)>0)
            {
                foreach($counterveyors as $key=>$tid)
                {
                    if($tid!='')
                    {
                        $sql="UPDATE part_instances SET cur_time=cur_time+".round($runningtime).", 
                        cur_count=cur_count+$gross WHERE equipment_id=$tid AND equipment_type='generic' 
                        AND replaced=0";
                        $dbTowerPartUpdate=dbexecutequery($sql);
                        $error.=$dbTowerPartUpdate['error'];
                        $sql="UPDATE pm_instances SET cur_time=cur_time+".round($runningtime).", 
                        cur_count=cur_count+$gross WHERE equipment_id=$tid AND equipment_type='generic' 
                        AND replaced=0";
                        $dbTowerPartUpdate=dbexecutequery($sql);
                        $error.=$dbTowerPartUpdate['error'];
                 
                    }    
                }    
            }
            
        }
        
        foreach($dbPaper['data'] as $jpaper)
        {
            
            $sql="SELECT * FROM paper_types WHERE id=$jpaper[papertype_id]";
            $dbPapertype=dbselectsingle($sql);
            $papertype=$dbPapertype['data'];
            $pricePerTon=$papertype['price_per_ton'];
            $pagelength=$GLOBALS['broadsheetPageHeight'];
            
            $paperdataid=$papertype['paperdataid'];
            $pagesonroll=round($rollwidth/$pagewidth,0);
            
            
            //convert gsm to basisweight
            $basisweight=gsmToBasisweight($papertype['paper_weight'],$paperdataid);
            //get pages per pound
            $factor=newsprintPagesPerPound($basisweight,$pagewidth,$pagelength,$paperdataid);
            $factor=round($factor,5);
            //calculate tonnage
            //pages on roll * gross / factor should give us tonnage
            $tonnage=round($pagesonroll*$gross/$factor,2); //is in pounds right now
            //convert $tonnage to MT
            $tonnage=round(poundsToKilograms($tonnage)/1000,2); //should be in MT now
            $totaltons+=$tonnage;
            $cost=round($tonnage*$pricePerTon,2);
            $sql="UPDATE job_paper SET price_per_ton='$pricePerTon', factor='$factor', 
            calculated_tonnage='$tonnage', calculated_cost='$cost' WHERE id=$jpaper[id]";
            $dbPaper=dbexecutequery($sql);
            if($debug)
            {
                print "Updating Paper usage for towerid=$towerid & equipment id=$pressid:<br />$sql<br />";
            }
        }
    }
} 
<?php
/*
*  This file contains a set of helper functions for inserting & packaging
*/

  /*
  *  This function calculates the total page count for a section
  */
  function calcPackageTotals($params)
  {
      global $json;
      
      $packageIDs = rtrim($params['package_ids'],",");
      
      if($packageIDs!='')
      {
          $ids = explode(",",$packageIDs);
          foreach($ids as $id)
          {
             $data = getInsertData($id);
             $json['packages'][]=array('id'=>$id,'pagecount'=>$data['pagecount'],
             'insertcount'=>$data['insertcount'],'weight'=>$data['weight']);
             
             //update the database record for this package
             $sql="UPDATE jobs_inserter_packages SET standard_pages='$data[pagecount]', total_inserts='$data[insertcount]', total_weight='$data[weight]' WHERE id=$id";
             $dbUpdate=dbexecutequery($sql);
          }
      }
      $json['status']='success';
  }
  
  function getInsertData($id)
  {
      global $json;
      $sql="SELECT A.*, B.insert_type FROM inserts A, jobs_packages_inserts B WHERE A.id = B.insert_id AND B.package_id = $id";
      
      //$json['sqls'][]=$sql;
      
      $dbInserts = dbselectmulti($sql);
      $i=0;
      foreach($dbInserts['data'] as $insert)
      {
          if($insert['insert_type']=='package')
          {
              $json['message']='Found a nested package';
              $calc = getInsertData($insert['id']);
              $totals['pagecount'] = $totals['pagecount'] + $calc['pagecount'];
              $totals['insertcount'] = $totals['insertcount'] + $calc['insertcount'];
              $totals['weight'] = $totals['weight'] + $calc['weight'];
          } else {
              $i++;
              $totals['weight'] = $totals['weight'] + $insert['piece_weight'];
              $totals['pagecount'] = $totals['pagecount'] + $insert['standard_pages'];
              $totals['insertcount'] = $i;
          }
      }
      return $totals;
  }
  
  
function getInsertToolTip($insertid,$pubdate='',$planid=0)
{
    $sql="SELECT A.*, B.account_name FROM inserts A, accounts B WHERE A.id=$insertid AND A.advertiser_id=B.id";
    $dbInsert=dbselectsingle($sql);
    $insert=$dbInsert['data'];
    
    if($pubdate!='')
    {
        $sql="SELECT * FROM inserts_schedule WHERE insert_date = '$pubdate' AND insert_id = '$insertid'";
        $dbInsertSchedule = dbselectsingle($sql);
        $request = $dbInsertSchedule['data']['insert_quantity'];
    } else {
        $request = 0;
    }
    $qtip.="<b>".stripslashes($insert['account_name'])."</b><br>&nbsp;&nbsp;";
    $qtip.="<b>Description: </b>".stripslashes($insert['insert_tagline'])."<br>&nbsp;&nbsp;";
    $qtip.="<b>Request: </b>$request<br>&nbsp;&nbsp;";
    $qtip.="<b>Standard Pages: </b>".stripslashes($insert['standard_pages'])."<br>&nbsp;&nbsp;";
    $qtip.="<b>Status: </b>";
    if($insert['confirmed'])
    {
        $qtip.="Confirmed, "; 
    } else {
        $qtip.="Not confirmed, ";
    }
    if($insert['received'])
    {
        $qtip.="Received<br>"; 
    } else {
        $qtip.="Not received<br>";
    }
    if($insert['weprint_id']>0)
    {
        $qtip.="&nbsp;&nbsp;<b>We printed this job</b>";
    }
    //now get zoning details
    //need to first get the schedule id, then work to the insert_zoning table with it
    $sql="SELECT * FROM jobs_inserter_plans WHERE id=$planid";
    $dbPlan=dbselectsingle($sql);
    $pubid=$dbPlan['data']['pub_id'];
    $pubdate=$dbPlan['data']['pub_date'];
    
    $sql="SELECT * FROM inserts_schedule WHERE insert_id='$insertid' AND pub_id=$pubid AND insert_date='$pubdate'";
    $dbSchedule=dbselectsingle($sql);
    $scheduleid=$dbSchedule['data']['id'];
    //$qtip.="Trying schedule lookup with $sql<br>";
    
    $zones = "<span style='color: black;font-weight:bold;'>Zones: ";
      //get zones for this insert
      $sql="SELECT DISTINCT(A.zone_label) FROM publications_insertzones A, inserts_zoning B, inserts_schedule C, jobs_inserter_packages D 
WHERE A.id=B.zone_id AND B.schedule_id=C.id AND C.pub_id=D.pub_id AND C.insert_date=D.pub_date AND B.insert_id=$insertid AND D.plan_id=$planid";
      $dbZones = dbselectmulti($sql);
      if($dbZones['numrows']>0)
      {
          foreach($dbZones['data'] as $zone)
          {
              $zones.=" $zone[zone_label] |";
          }
      } 
      $zones.="</span>";
    $qtip.=$zones;
   
    return $qtip;
} 



function tie_received($controlNumber,$insertid)
{
    global $Prefs;
    $sql="SELECT * FROM inserts_received WHERE control_number='$controlNumber'";
    $dbReceived=dbselectsingle($sql);
    if($dbReceived['numrows']>0)
    {
        $receivedInsert=$dbReceived['data'];
        if($receivedInsert['matched']==0)
        {
            $dt=date("Y-m-d H:i");
            $sql="UPDATE inserts_received SET matched=1, matched_on='$dt' WHERE id=$receivedInsert[id]";
            $dbUpdate=dbexecutequery($sql);
            
            //now update the insert record with the data from the inserts_received
            
            
            $sql="UPDATE inserts SET received=1, receive_date='$receivedInsert[receive_date]', receive_count='$receivedInsert[receive_count]', receive_datetime='$receivedInsert[receive_datetime]', ship_type='$receivedInsert[ship_type]',
            ship_quantity='$receivedInsert[ship_quantity]', receive_by='$receivedInsert[receive_by]', 
            single_sheet='$receivedInsert[single_sheet]', slick_sheet='$receivedInsert[slick_sheet]',
            tag_color='$receivedInsert[tag_color]', storage_location='$receivedInsert[storage_location]',
            damage='$receivedInsert[damage]', insert_damage='$receivedInsert[insert_damage]',
            sticky_note='$receivedInsert[sticky_note]' WHERE id=$insertid";
            $dbUpdate=dbexecutequery($sql);
        }
        
        $sql="SELECT A.*, B.account_name FROM inserts A, accounts B WHERE A.id=$insertid AND A.advertiser_id=B.id";
        $dbInsert=dbselectsingle($sql);
        $insert=$dbInsert['data'];
        //print "For insert $insert[account_name] we found a receive record with $controlNumber!<br>";
        $message="<p>During the preprint import process, we were able to match an insert for $insert[account_name]</p>";
        $message.="<p>It has a control # of $controlNumber and is for the following Pubs/Days:</p>
        <ul>";
        
        $sql="SELECT A.*, B.pub_name FROM inserts_schedule A, publications B WHERE A.insert_id=$insertid AND A.pub_id=B.id";
        $dbSchedules=dbselectmulti($sql);
        $foundMatch=false;
        if($dbSchedules['numrows']>0)
        {
            foreach($dbSchedules['data'] as $schedule)
            {
                if($schedule['insert_date']==$receivedInsert['scheduled_pubdate'] && $schedule['pub_id']==$receivedInsert['insert_pub_id'])
                {
                    $foundMatch=true;   
                }
                $message.= "<li>".$schedule['pub_name']." for ".date("m/d/Y",strtotime($schedule['insert_date']))." a quantity of ".$schedule['insert_quantity']."</li>";
            }
        } else {
            $message.= "<li>None scheduled yet</li>";
        }   
        $message.="</ul>\n";
        $message.="<p>Please update your records and the pallets/boxes accordingly.</p>";
        if(!$foundMatch)
        {
            $message.="<p><b>PLEASE NOTE!!!</b> The received publication & date did not match any of the booked pub/dates so you will need to make sure the pallets are marked correctly!</p>";
        }
        $sendTo=$Prefs->lateInsertNotification;
        $mail = new htmlMimeMail();
        $mail->setHtml($message);
        $mail->setFrom($Prefs->systemEmailFromAddress);
        $mail->setSubject($subject);
        $mail->setHeader('Sender','Mango');
        if ($sendTo!='')
        {
            $result = $mail->send(array($sendTo),'smtp');
        }
    }
}


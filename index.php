<?php
include("includes/boot.php") ;
if ($_SESSION['accessdenied']==true)
{
    ?>
    <div class="alert alert-danger" role="alert">You do not have access to that function. If you feel you should, please contact your system administrator</div>
    <?php
    $_SESSION['accessdenied']=false;
}
print "<div class='container'>\n";

?>
<style>

    .panel-heading a:after {
        font-family:'Glyphicons Halflings';
        content:"\e114";
        float: right;
        color: white;
    }
    .panel-heading a.collapsed:after {
        content:"\e080";
    }
    .dashboardBox {
        max-height: 500px;
        overflow-y: auto;
    }
</style>
<?php
    print "<div class='row'>\n";
    //see if this user has any modules, otherwise, show the defaults
    $sql="SELECT A.* FROM dashboard_items A, user_dashboard B WHERE B.user_id=".intval($User->id)." AND B.module_id=A.id";

    $dbBlocks=dbselectmulti($sql);
    if($dbBlocks['numrows']>0)
    {
        print "<div id='mainContentHolderCol1' class='col-md-4 col-xs-12 column'>\n";
        show_module('1');
        print "</div>\n";
        print "<div id='mainContentHolderCol2' class='col-md-4 col-xs-12 column'>\n";
        show_module('2');
        print "</div>\n";
        print "<div id='mainContentHolderCol3' class='col-md-4 col-xs-12 column'>\n";
        show_module('3');
        print "</div>\n";
    } else {
        //user has no dashboard items, so move on
    }
    print "</div>\n";
print "</div>\n";

$script = "
\$('.panel-title a').click(function() {
    var blockid=$(this).data('blockid');
    \$.ajax({
        url: \"includes/ajax_handlers/updateDashboard.php\",
        type: \"POST\",
        data: ({action:'toggle',id:blockid,uid: ".$User->id."}),
        dataType: 'json',
        success: function(msg){
           if(msg.error!='')
           {
             alert(msg.error);
           }
           //console.log(msg);
        }
    })
})
\$(function() {
    \$( \".column\" ).sortable({
        connectWith: \".column\",
        stop: function(event, ui)
        {
            var blockorder1=$(\"#mainContentHolderCol1\").sortable(\"serialize\");
            var blockorder2=$(\"#mainContentHolderCol2\").sortable(\"serialize\");
            var blockorder3=$(\"#mainContentHolderCol3\").sortable(\"serialize\");
            \$.ajax({
                  url: \"includes/ajax_handlers/updateDashboard.php\",
                  global: false,
                  type: \"POST\",
                  data: ({uid: ".$User->id.", col1 : blockorder1, col2 : blockorder2, col3 : blockorder3, action:'reorder'}),
                  dataType: 'json',
                  success: function(msg){
                     if(msg.error!='')
                     {
                         alert(msg.error);
                     }
                     //console.log(msg);
                  }
               }
            )
        }
    });
    \$( \".column\" ).disableSelection();
});
    
    function showorder()
    {
        var blockorder1=$(\"#mainContentHolderCol1\").sortable(\"serialize\");
        alert(blockorder1);
        var blockorder2=$(\"#mainContentHolderCol2\").sortable(\"serialize\");
        alert(blockorder2);
        var blockorder3=$(\"#mainContentHolderCol3\").sortable(\"serialize\");
        alert(blockorder3);
    }
";
$GLOBALS['scripts'][]=$script;



function show_module($column)
{
    global $User;
    $sql="SELECT A.*, B.collapsed FROM dashboard_items A, user_dashboard B WHERE B.user_id=".$User->id." AND B.module_id=A.id AND B.module_column='$column' ORDER BY B.module_order";
    $dbModules=dbselectmulti($sql);
    if ($dbModules['numrows']>0){
        foreach ($dbModules['data'] as $module){
           $moduleid=$module['id'];
           $collapsed=$module['collapsed'];
           $function=stripslashes($module['function_name']);
           $blockname=stripslashes($module['dashboard_name']);
            ?>
            <div id='item_<?php echo $moduleid; ?>' class="panel panel-primary dragBox">
              <!-- Default panel contents -->

                <div class="panel-heading">
                    <h4 class="panel-title" data-blockID='<?php echo $moduleid ?>'>
                        <a class="<?php ($collapsed ? '': 'collapsed') ?>" data-blockid='<?php echo $moduleid ?>' data-toggle="collapse" data-target="#box_<?php echo $moduleid ?>" href="#box_<?php echo $moduleid ?>">
                            <?php echo $blockname ?>
                        </a>
                    </h4>
                </div>
                <?php
                if($collapsed)
                {
                    print "<div id='box_$moduleid' class='dashboardBox collapse'>\n";
                } else {
                    print "<div id='box_$moduleid' class='dashboardBox collapse in'>\n";
                }
                $function(); ?>
                </div>
            </div>
           <?php

        }
    }
    
}

function mango_news()
{
    $dt=date("Y-m-d H:i");
    $sql="SELECT * FROM mango_news WHERE archive_datetime>='$dt' OR sticky=1 AND site_id=".SITE_ID." ORDER BY urgent DESC, post_datetime DESC";
    if($GLOBALS['debug']){
        print "Looking for news items with $sql<br>";
    }
    $dbNews=dbselectmulti($sql);
    if ($dbNews['numrows']>0)
    {
        print '<ul class="list-group">';
        foreach($dbNews['data'] as $news)
        {
            //who wrote it?
            $sql="SELECT firstname,lastname FROM users WHERE id=$news[post_by]";
            $dbAuthor=dbselectsingle($sql);
            $author="By: ".$dbAuthor['data']['firstname'].' '.$dbAuthor['data']['lastname'];
            if ($news['urgent'])
            {
                print "<li class='list-group-item text-warning'><h4>$news[headline]</h4><br>\n";
            } else {
                print "<li class='list-group-item'><h4>x$news[headline]</h4><br>\n";
            }
            print "<p style='font-size:10px;font-weight:normal'>Author $author<br>\n";
            print "Posted ".date("D m/d @ H:i",strtotime($news['post_datetime']))."</p>\n";
            print "<p class='dashboardItem'>$news[message]</p>\n";
            print "</li>";
        }
        print "</ul>\n";
    } else {
        print "No news today.";
    }
}

function upcoming_special_sections()
{
    global $pubids, $folders;

    $start=date("Y-m-d");
    print "<div class='panel-body'><p>The following are upcoming special sections.</p></div>\n";

    $sql="SELECT A.*, B.pub_name FROM special_sections A, publications B WHERE A.site_id=".SITE_ID." AND A.pub_id=B.id AND A.insert_date>='$start' AND A.pub_id IN ($pubids) ORDER BY A.insert_date ASC";
    $dbJobs=dbselectmulti($sql);
    $i=0;
    if ($dbJobs['numrows']>0)
    {
        print "<table class='table table-striped table-bordered table-condensed'>\n";
        print "<tr><th>Section Name</th><th>Pub Date</th><th>Print Time</th></tr>\n";
        foreach($dbJobs['data'] as $job)
        {
            print "<tr>\n";
            print "<td><a href='specialSections.php?action=edit&sectionid=$job[id]'>$job[pub_name] - $job[section_name]</a></td>\n";
            print "<td>$job[insert_date]</td>\n";
            print "<td>";
            print date("D m/d @ H:i",strtotime($job['startdatetime']));
            print "</td>";
            print "</tr>\n";
        }
        print "</table>\n";
    } else {
        print "<p class='dashboardHeadline'>There are no special sections planned at this time.</p>";
    }
}


function upcoming_pressjobs()
{
    global $pubids, $folders;
    $start=date("Y-m-d H:i");
    $end=date("Y-m-d H:i",strtotime("+24 hours"));
    print "<div class='panel-body'><p>The following are press jobs for the next 24 hours.</p></div>\n";

    $sql="SELECT A.*, B.pub_name, C.run_name FROM jobs A, publications B, publications_runs C WHERE A.site_id=".SITE_ID." AND A.pub_id=B.id AND A.run_id=C.id AND A.continue_id=0 AND A.status<>99 AND A.startdatetime>='$start' AND A.startdatetime<='$end' AND A.pub_id IN ($pubids) ORDER BY A.startdatetime ASC";
    $dbJobs=dbselectmulti($sql);
    if ($dbJobs['numrows']>0)
    {
        print "<table class='table table-striped table-bordered table-condensed'>\n";
        print "<tr><th>Job Name</th><th>Pub Date</th><th>Draw</th><th>Folder</th><th>Print Time</th></tr>\n";
        foreach($dbJobs['data'] as $job)
        {
            print "<tr>\n";
            print "<td><a href='#' onclick=\"window.open('jobPressPopup.php?id=$job[id]','Edit Press Job','width=800,height=900,status=no,location=no,toolbar=no,menubar=no,navigation=no')\">$job[pub_name] - $job[run_name]</a></td>\n";
            print "<td>$job[pub_date]</td><td>$job[draw]</td>\n";
            print "<td>".$folders[$job['folder']]."</td>";
            print "<td>".date("D m/d @ H:i",strtotime($job['startdatetime']))."</td>";
            print "</tr>\n";
        }
        print "</table>\n";
    } else {
        print "<p class='dashboardHeadline'>Somehow, there are no jobs in the next 24 hours. Must be a time for a party!</p>";
    }
}

function upcoming_insertpackages()
{
    global $pubids, $inserters;

    $opened=false;
    
    $start=date("Y-m-d H:i");
    $end=date("Y-m-d H:i",strtotime("+96 hours"));

    $sql="SELECT A.*, B.pub_name FROM jobs_inserter_packages A, publications B WHERE A.package_startdatetime>='$start' AND A.package_startdatetime<='$end' AND A.pub_id IN ($pubids) AND A.pub_id=B.id AND B.site_id=".SITE_ID." ORDER BY A.package_startdatetime ASC";
    $dbPackages=dbselectmulti($sql);
    print "<div class='panel-body'><p>Insert packages for the next 96 hours:</p></div>\n";

    if ($dbPackages['numrows']>0)
    {
        print "<table class='table table-striped table-bordered table-condensed'>\n";
        print "<tr><th>Package</th><th>Date/Count</th><th>Inserter/Run Time</th></tr>\n";
        foreach($dbPackages['data'] as $job)
        {
            print "<tr>\n";
            print "<td><a href='buildInsertPackage.php?planid=$job[plan_id]&packageid=$job[id]&pubid=$job[pub_id]'>$job[pub_name] - $job[package_name]</a></td>";
            print "<td>$job[pub_date] and we need $job[inserter_request]</td>";
            print "<td>".$inserters[$job['inserter_id']]." at ";
            print date("D m/d \@ H:i",strtotime($job['package_startdatetime']))."</td>\n";
            print "</tr>\n";
        }
        print "</table>\n";
    } else {
        print "<p class='dashboardHeadline'>No packages scheduled for the next 48 hours.</p>\n";
    }

}

function missing_inserts()
{
    global $pubids;
    $dt=date("Y-m-d",strtotime('-1 month'));
    $sql="SELECT A.*, B.account_name FROM inserts A, accounts B, inserts_schedule C 
    WHERE A.received=0 AND C.insert_id=A.id AND C.insert_date>='$dt' AND A.advertiser_id=B.id AND A.site_id=".SITE_ID." ORDER BY C.insert_date LIMIT 50";
    $dbInserts=dbselectmulti($sql);
    print "<div class='panel-body'><p>The following are booked inserts that have not been received yet.</p></div>\n";
    if ($dbInserts['numrows']>0)
    {
        print "<table class='table table-striped table-bordered table-condensed'>\n";
        print "<tr><th>Insert</th><th>Publishes on</th><th>Package</th></tr>\n";
        foreach ($dbInserts['data'] as $insert)
        {
            print "<tr>\n";
            print "<td><a href='inserts.php?action=edit&insertid=$insert[id]'>$insert[account_name]</a></td>";
            print "<td>".date("D m/d",strtotime($insert['insert_date']))."</td>\n";
            $sql="SELECT * FROM jobs_inserter_packages WHERE id=$insert[package_id]";
            $dbPackage=dbselectsingle($sql);
            if ($dbPackage['numrows']>0)
            {
                $package=$dbPackage['data'];
                print "<td>";
                print date("D m/d \@ H:i",strtotime($package['package_startdatetime']));
                print "</td>\n";
            } else {
                print "<td>Has not been included in a package yet.</td>\n";
            }
            print "</tr>\n";
        }
        print "</table>\n";
    } else {
        print "<div class='panel-body'>No missing inserts!</p></div>\n";
    }
    

}



function unbooked_inserts()
{
    global $pubids;
    $sql="SELECT A.*, B.account_name, C.pub_name FROM inserts_received A, accounts B, publications C 
    WHERE A.matched=0 
    AND A.advertiser_id=B.id 
    AND A.insert_pub_id=C.id
    AND C.site_id=".SITE_ID." 
    ORDER BY A.scheduled_pubdate LIMIT 50";
    print "<div class='panel-body'><p>The following are inserts that have been received but have no matching booking record.</p></div>\n";
    $dt=date("Y-m-d",strtotime('-1 month'));
    $dbInserts=dbselectmulti($sql);
    if ($dbInserts['numrows']>0)
    {
        print "<table class='table table-striped table-bordered table-condensed'>\n";
        foreach ($dbInserts['data'] as $insert)
        {
            print "<tr>";
            print "<td><a href='insertsReceived.php?action=edit&insertid=$insert[id]'>".stripslashes($insert['account_name'])."</a></td>\n";
            print "<td>".date("D m/d",strtotime($insert['scheduled_pubdate']))."</td>";
            print "<td>$insert[pub_name]</td>\n";
            print "</tr>\n";
        }
        print "</table>\n";
    } else {
        print "<p class='dashboardHeadline'>No unbooked inserts!</p>\n";
    }

}

function press_maintenance()
{
    $helpStatuses=array();
    $sql="SELECT * FROM helpdesk_statuses WHERE site_id=".SITE_ID." ORDER BY status_order";
    $dbStatuses=dbselectmulti($sql);
    if ($dbStatuses['numrows']>0)
    {
      foreach($dbStatuses['data'] as $status)
      {
          $helpStatuses[$status['id']]=$status['status_name'];
      }
    } else {
      $helpStatuses[0]="None set!";
    }
    $helpPriorities=array();
    $sql="SELECT * FROM helpdesk_priorities WHERE site_id=$siteID ORDER BY priority_order";
    $dbPriorities=dbselectmulti($sql);
    if ($dbPriorities['numrows']>0)
    {
      foreach($dbPriorities['data'] as $priority)
      {
          $helpPriorities[$priority['id']]=$priority['priority_name'];
      }
    } else {
      $helpPriorities[0]=="None set!";
    }

    $helpTypes=array();
    $sql="SELECT * FROM helpdesk_types WHERE site_id=$siteID AND production_specific=1 ORDER BY type_name";
    $dbTypes=dbselectmulti($sql);
    if ($dbTypes['numrows']>0)
    {
      foreach($dbTypes['data'] as $type)
      {
          $helpTypes[$type['id']]=$type['type_name'];
      }
    } else {
      $helpTypes[0]=="None set!";
    }   

    $sql="SELECT * FROM maintenance_tickets WHERE status_id<>'$GLOBALS[helpdeskCompleteStatus]' ORDER BY priority_id DESC, submitted_datetime DESC";
    $dbTickets=dbselectmulti($sql);
    print "<div class='panel-body'>\n";
    print "<a href='#' onclick=\"window.open('helpdeskSubmit.php?action=submit&type=1&source=press','Help Desk','width=580,height=600,toolbar=no,status=no,location=no,scrollbars=no');return false;\">Click here to open a new trouble ticket.</a>\n";
    print "</div>\n";
    if ($dbTickets['numrows']>0)
    {
        print "<table class='table table-striped table-bordered table-condensed'>\n";
        print "<tr><th>Ticket ID</th><th>Priority</th><th>Type</th><th>Brief</th></tr>\n";
        foreach($dbTickets['data'] as $ticket)
        {
            $priority=$helpPriorities[$ticket['priority_id']];
            $type=$helpTypes[$ticket['type_id']];
            $id=$ticket['id'];
            $brief=$ticket['problem'];
            print "<tr>";
            print "<td><a href='maintenanceTickets.php?action=edit&id=$id'>#$id</a></td>\n";
            print "<td>$priority</td>\n";
            print "<td>$type</td>\n";
            print "<td>$brief</td>\n";
            print "</tr>\n";
        }
        print "</table>\n";
    } else {
        print "No current maintenance tickets are open.";
    }   

}

function short_inventory()
{
    $sql="SELECT * FROM equipment_part WHERE part_inventory_quantity<=part_reorder_quantity AND site_id=".SITE_ID." ORDER BY part_name";
    $dbParts=dbselectmulti($sql);
    if ($dbParts['numrows']>0)
    {
        print "<div class='panel-body'><p><a href='purchaseOrders.php?action=add'>Click here to create a purchase order</a></p></div>\n";
        print "<table class='table table-striped table-bordered table-condensed'>\n";
        print "<tr><th>Part</th><th>Current Count</th></tr>\n";
        foreach($dbParts['data'] as $part)
        {
            print "<tr>\n";
            print "<td>".stripslashes($part['part_name'])."</td>\n";
            print "<td>$part[part_inventory_quantity]</td>\n";
            print "</tr>";
        }
        print "</table>\n";
    } else {
        print "<div class='panel-body'><p>Inventory looks up to date.</p></div>\n";
    }

}

$Page->footer();
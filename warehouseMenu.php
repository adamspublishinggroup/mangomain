<?php
  include("includes/boot.php");
  
  /*
  * this is a menu for the warehouse - built specifically for mobile devices
  */
  
  // display a list of menu options for the warehouse folks
  
  if($User->canAccess('insertQuickAdd.php'))
  {
      print "<a href='insertQuickAdd.php' class='btn btn-block btn-dark'>Receive New Insert</a>";
  }
  
  
  //other function
  //ship & receive inserts
  //receive ink & plates
  //move pallets
  //look up insert
  //inserter staging
  
  
  $Page->footer();
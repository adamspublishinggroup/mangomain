<?php
//<!--VERSION: .9 **||**-->

include("includes/boot.php");

if ($_POST)
{
    $action=$_POST['submit'];
} else {
    $action=$_GET['action'];
}

switch ($action)
{
    case "Save Publication":
    save_pub('insert');
    break;
    
    case "Update Publication":
    save_pub('update');
    break;
    
    case "addpub":
    pubs('add');
    break;
    
    case "editpub":
    pubs('edit');
    break;
    
    case "deletepub":
    deletepub();
    break; 
    
    default:
    list_pubs();
    break;
      
}
function list_pubs()
{
//show all the pubs
   $sql="SELECT * FROM publications ORDER by sort_order, pub_name";
   $dbPubs=dbselectmulti($sql);
   tableStart("<a href='?action=addpub'>Add new publication</a>","Publication Name",5);
   if ($dbPubs['numrows']>0)
   {
        foreach($dbPubs['data'] as $pub)
        {
            $pubid=$pub['id'];
            $pubname=stripslashes($pub['pub_name']);
            print "<tr><td>$pubname</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=editpub&pubid=$pubid' class='btn btn-dark'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='publications_pressruns.php?action=listruns&pubid=$pubid'>Press Runs</a></li>
            <li><a href='publications_packaging.php?action=listzones&pubid=$pubid'>Zoning</a></li>
            <li><a href='?action=deletepub&pubid=$pubid' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
   }
   tableEnd($dbPubs);    
}


function pubs($action)
{
    $sql="SELECT * FROM accounts ORDER BY account_name";
    $dbCustomers=dbselectmulti($sql);
    $customers[0]="Select customer for this publication";
    if($dbCustomers['numrows']>0)
    {
        foreach($dbCustomers['data'] as $customer)
        {
            $customers[$customer['id']]=$customer['account_name'];
        }
    }
    if ($action=='add')
    {
        $button="Save Publication";
        $insertrun=0;
        $sort=1;
        $customerid=0;
        global $prefs;
        $pubcolor=$prefs['default_pub_color'];
    } else {
        $button="Update Publication";
        $pubid=$_GET['pubid'];
        $sql="SELECT * FROM publications WHERE id=$pubid";
        $dbPub=dbselectsingle($sql);
        $pub=$dbPub['data'];
        $pubname=stripslashes($pub['pub_name']);
        $pubcode=stripslashes($pub['pub_code']);
        $altpubcode=stripslashes($pub['alt_pub_code']);
        $circcode=stripslashes($pub['circulation_code']);
        $reversetext=stripslashes($pub['reverse_text']);
        $pubmessage=stripslashes($pub['pub_message']);
        $pubcolor=stripslashes($pub['pub_color']);
        $bookingPub=stripslashes($pub['booking_pub']);
        $bookinEdition=stripslashes($pub['booking_edition']);
        $bookingSticky=stripslashes($pub['booking_sticky']);
        $insertReceiveEmail=stripslashes($pub['insert_receive_email']);
        $insertrun=$pub['insert_run'];
        $sort=$pub['sort_order'];
        $customerid=$pub['customer_id'];
        
    }
    print "<form method=post class='form-horizontal'>\n";
    make_text('pubname',$pubname,'Publication Name','',50);
    make_text('pubcode',$pubcode,'Publication Code','Default 2-3 character pub code',3);
    make_text('altpubcode',$altpubcode,'Alternate Publication Codes','Alternate publication codes (ex: ET, EB) separated by commas. Be sure these are unique!',50);
    make_select('customer_id',$customers[$customerid],$customers,'Customer','If this publication belongs to an external customer, please select if from the list.');
    make_textarea('pubmessage',$pubmessage,'Pub Message','This message displays on the press monitor window when a job with this publication comes up',60,3,false);
    make_checkbox('insertrun',$insertrun,'Insert run','By default, create an insert run for this publication for each pub date');
    make_text('insert_receive_email',$insertReceiveEmail,'Insert Receive Alert Email','What email address should be alerted when an insert for this publication is received?',50);
    make_text('circcode',$circcode,'Circulation Code','Circulation/Inserter pub code',10);
    make_text('bookingPub',$bookingPub,'Ad System Pub Code','Advertising System Publication Code(s) (use commas to separate multiple valid publication codes)',10);
    make_text('bookingEdition',$bookinEdition,'Ad System Edition/Secondary Code','Ad System secondary code (edition, zone, etc that designates inserts in the feed, separate multiple entries with comma)',10);
    make_text('bookingSticky',$bookingSticky,'Ad System Edition/Secondary for sticky notes','Ad System Edition Code for sticky notes (if different)',10);
    make_color('pubcolor',$pubcolor,'Publication color');
    make_checkbox('reversetext',$reversetext,'Reverse','Make text white on this color');
    make_number('sort_order',$sort,'Sort Order','Order to display publications (defaults to alphabetic)');
    make_submit('submit',$button);
    print "<input type='hidden' name='pubid' value='$pubid'>\n";
    print "</form>\n";
     
}

function deletepub()
{
    $pubid=intval($_GET['pubid']);
    $sql="DELETE FROM publications WHERE id=$pubid";
    $dbDelete=dbexecutequery($sql);
    $sql="DELETE FROM publications_runs WHERE pub_id=$pubid";
    $dbDelete=dbexecutequery($sql);
    $sql="DELETE FROM publications_inserttrucks WHERE pub_id=$pubid";
    $dbDelete=dbexecutequery($sql);
    $sql="DELETE FROM publications_insertzones WHERE pub_id=$pubid";
    $dbDelete=dbexecutequery($sql);
    $sql="DELETE FROM publications_insertroutes WHERE pub_id=$pubid";
    $dbDelete=dbexecutequery($sql);
    if ($error!='')
    {
        setUserMessage('There was a problem deleting the publication.<br />'.$error,'error');
    } else {
        setUserMessage('The publication and all associated items have been successfully deleted.','success');
    }
    redirect("?action=list");
}

function save_pub($action)
{
    global $siteID;
    $pubid=$_POST['pubid'];
    $pubname=addslashes($_POST['pubname']);
    $pubcode=addslashes($_POST['pubcode']);
    $circcode=addslashes($_POST['circcode']);
    $sorder=addslashes($_POST['sort_order']);
    $pubcolor=addslashes($_POST['pubcolor']);
    $altpubcode=addslashes($_POST['altpubcode']);
    $pubmessage=addslashes($_POST['pubmessage']);
    $bookingPub=addslashes($_POST['bookingPub']);
    $bookingEdition=addslashes($_POST['bookingEdition']);
    $bookingSticky=addslashes($_POST['bookingSticky']);
    $customerid=addslashes($_POST['customer_id']);
    $insertReceiveEmail=addslashes($_POST['insert_receive_email']);
    if($_POST['sort_order']==''){$sorder=1;}
    if($_POST['reversetext']){$reversetext=1;}else{$reversetext=0;}
    if ($_POST['insertrun']){$insertrun=1;}else{$insertrun=0;}
    if ($action=='insert')
    {
        $sql="INSERT INTO publications (pub_name, pub_code, pub_color,reverse_text, circulation_code, insert_run, site_id, sort_order, alt_pub_code, 
        pub_message, booking_pub, booking_edition, booking_sticky, customer_id, insert_receive_email)
         VALUES ('$pubname', '$pubcode','$pubcolor','$reversetext', '$circcode', '$insertrun', '$siteID', '$sorder', '$altpubcode', '$pubmessage', 
         '$bookingPub', '$bookingEdition', '$bookingSticky', '$customerid', '$insertReceiveEmail')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $pubid=$dbInsert['insertid'];
        //add a default 'Main' run for pressrun
        $sql="INSERT INTO publications_runs (pub_id, run_name) VALUES ('$pubid', 'Main')";
        $dbInsert=dbinsertquery($sql);
        $runid=$dbInsert['insertid'];
        //add a default 'MAIN' run for inserter run
        $sql="INSERT INTO publications_insertruns (pub_id, run_name) VALUES ('$pubid', 'MAIN')";
        $dbInsert=dbinsertquery($sql);
        $runid=$dbInsert['insertid'];
        //now, set default benchmarks for this default run
        $sql="SELECT * FROM benchmarks ORDER BY benchmark_category, benchmark_name";
        $dbBenchmarks=dbselectmulti($sql);
        if ($dbBenchmarks['numrows']>0)
        {
            foreach($dbBenchmarks['data'] as $benchmark)
            {
                if ($benchmark['benchmark_type']=='time')
                {
                    $value="12:00";
                } else {
                    $value="0";
                }
                $sql="INSERT INTO run_benchmarks (run_id, benchmark_id,sunday, monday, tuesday, wednesday, thursday, friday, saturday)
                 VALUES ($runid,$benchmark[id],'$value','$value','$value','$value','$value','$value','$value')";
                $dbInsert=dbinsertquery($sql);
            }
        }
        //now, add this pub to user_publications where user has "allpubs" enabled
        $sql="SELECT id FROM users WHERE allpubs=1";
        $dbUsers=dbselectmulti($sql);
        if ($dbUsers['numrows']>0)
        {
            foreach($dbUsers['data'] as $user)
            {
                $sql="INSERT INTO user_publications (user_id, pub_id, value) VALUES ($user[id], $pubid, 1)";
                $dbInsert=dbinsertquery($sql);
            }
            
        }
        
        //create a default zone as well called "Default"
        $sql="INSERT INTO publications_insertzones (pub_id, zone_name, system_code, zone_order, zone_zip, dow_0, dow_1, dow_2, dow_3, dow_4, dow_5, dow_6, zone_label)
         VALUES ('$pubid', 'Default', '',  '1', '', '0', '0', '0', '0', '0', '0', '0', 'Default' )";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        
    } else {
        $sql="UPDATE publications SET sort_order='$sorder', insert_run='$insertrun', circulation_code='$circcode', pub_name='$pubname', 
        alt_pub_code='$altpubcode', pub_code='$pubcode', pub_color='$pubcolor', reverse_text='$reversetext', pub_message='$pubmessage', 
        booking_pub='$bookingPub', booking_edition='$bookingEdition', booking_sticky='$bookingSticky', customer_id='$customerid', insert_receive_email='$insertReceiveEmail' WHERE id=$pubid";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
        
        
        
        //now, add this pub to user_publications where user has "allpubs" enabled
        $sql="SELECT id FROM users WHERE allpubs=1 AND site_id=".SITE_ID;
        $dbUsers=dbselectmulti($sql);
        if ($dbUsers['numrows']>0)
        {
            foreach($dbUsers['data'] as $user)
            {
                //see if this user has this publication, if not, add it
                $sql="SELECT * FROM user_publications WHERE user_id=$user[id] AND pub_id=$pubid";
                $dbCheck=dbselectsingle($sql);
                if($dbCheck['numrows']==0)
                {
                    $sql="INSERT INTO user_publications (user_id, pub_id, value) VALUES ($user[id], $pubid, 1)";
                    $dbInsert=dbinsertquery($sql);
                }
                
            }
            
        }
        
        
    }
    
    if ($error!='')
    {
        setUserMessage('There was a problem saving the publication.<br />'.$error,'error');
    } else {
        setUserMessage('The publication has been successfully saved.','success');
    }
    redirect("?action=list");
    
}

$Page->footer();
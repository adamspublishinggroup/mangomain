<?php
$pageFluid=true;
include("includes/boot.php") ;
global $pubs, $vendors;


if($_POST)
{
    //this is where we handle the creation of a new package.
    addPackage();
}
if($_GET['action']=='deletePackage')
{
    deletePackage();
}

?>
<style type="text/css">
    .insert, .jacket, .insert-placeholder {
        width: 45%;
        margin: 2px 2.5%;
        float:left;
        height: 120px;
        border: 2px solid green;
        cursor: move;
        padding:2px;
        overflow: hidden;
        font-size:10px !important;
        font-weight:bold;
        background-color: white;
    }
    .package {
        height: 24px;
        margin-bottom: 4px;
    }
    .notconfirmed {
        border: 2px solid red;
    }
    .notreceived {
        color: red;
    }
    
    .killed {
        text-decoration: strikethrough;
        color: red;
    }
    .station {
        font-weight:bold !important;
        font-size: 12px !important;
        overflow: hidden;
        height:24px;
        padding: 6px;
    }
    .sticky {
        border-style: dashed;
    }
    .cloned {
        background-color:#99CCFF;
    }

    .used {
        text-decoration: line-through;
        color: #00ff00 !important;
    }
    .packageList {
        padding-bottom:4px;
        margin-bottom:4px;
        border-bottom: thick solid black;
    }
    .stickylabel {
        color: black;
    }
    .col-md-1 {
        width: 12% !important;
        padding: 2px !important;
    }
    .col-xs-6 {
        width: 45%;
        margin: 2px 2.5%;
    }
    .panel-heading {
        padding:4px;
    }
    .panel-heading p {
        margin: 0 4px;
    }
    .station-hover {
        border: 2px solid green;
    }
</style>
<?php

$planid=intval($_GET['planid']);
$sql="SELECT * FROM jobs_inserter_plans WHERE id=$planid";
$dbPlan=dbselectsingle($sql);
$plan=$dbPlan['data'];

$pubid=$plan['pub_id'];
$runid=$plan['run_id'];
$pressrunid=$plan['pressrun_id'];
$pubdate=$plan['pub_date'];
print "<input type='hidden' id='plan_id' value='$planid' />\n";
print "<input type='hidden' id='pub_id' value='$pubid' />\n";
print "<input type='hidden' id='pub_date' value='$pubdate' />\n";
$pubname=$pubs[$pubid];
$displaydate=date("m/d/Y",strtotime($pubdate));

if($plan['pressrun_id']!=0)
{
    $pressrun="AND A.package_run_id=$plan[pressrun_id]";
} else {
    $pressrun="AND A.package_run_id=0";
}
$sql="SELECT B.*, A.pressrun_id, A.insert_quantity, A.id AS schedule_id FROM inserts_schedule A, inserts B 
    WHERE A.insert_id=B.id AND A.pub_id=$pubid $pressrun AND A.insert_date='$pubdate' 
    ORDER BY B.confirmed DESC";

$dbInserts=dbselectmulti($sql);

$sql="SELECT * FROM jobs_inserter_packages WHERE plan_id='$planid' ORDER BY package_name";
$dbPackages=dbselectmulti($sql);
        

//get the inserts. They will then be displayed in a box at the top of the area. 800px for inserts, 200px for packages & jacket placeholder
$totalpages=0;
$totalinserts=0;
$totalSticky=0;
if($dbInserts['numrows']>0)
{
    
    foreach($dbInserts['data'] as $insert)
    {
        if($insert['sticky_note'])
        {
            $totalSticky++;
        } else {
            $totalpages+=$insert['standard_pages'];
            $totalinserts++;
        }
    }
    //build an array of all used inserts
    $sql="SELECT * FROM jobs_packages_inserts WHERE plan_id=$planid";
    $dbScheduled=dbselectmulti($sql);
    $scheduledInserts['insert']=array();
    $scheduledInserts['package']=array();
    if($dbScheduled['numrows']>0)
    {
        foreach($dbScheduled['data'] as $scheduled)
        {
            $scheduledInserts[$scheduled['insert_type']][]=$scheduled['insert_id']; 
        }
    }
    
    //do the same with sticky notes
    if($dbPackages['numrows']>0)
    {
        foreach($dbPackages['data'] as $package)
        {
            if($package['sticky_note_id']!=0)
            {
                $scheduledInserts['insert'][]=$package['sticky_note_id']; 
            }
        }
    }
    
    
}

if($pressrunid!=0)
{
    $sql="SELECT * FROM publications_runs WHERE id=$pressrunid";
    $dbRun = dbselectsingle($sql);
    $pressrun = stripslashes($dbRun['data']['run_name']);
} else {
    $pressrun = "N/A";
}

$stickynotes[0]='None';
print "<div class='row'>\n";
print "<div class='col-xs-12 col-sm-2' >\n"; //style='position:fixed;height:80%;overflow-y:scroll;'

    print "<div id='packageList' class='packageList row'>\n";
       //add the generic jacket
        print "<div id='jacket_0' class='jacket package col-xs-12' data-type='jacket'  data-id='0'style='color:white;background-color:#006600 !important'>\n";
            print '<b>Generic Jacket</b>';
        print "</div>\n";
        if($dbPackages['numrows']>0 && count($scheduledInserts)>0)
        {
            foreach($dbPackages['data'] as $package)
            {
                if(!in_array($package['id'],$scheduledInserts['package']))
                {
                    $pname=stripslashes($package['package_name']);
                    print "<div id='package_$package[id]' class='insert package col-xs-12' data-type='package' data-id='$package[id]' >\n";
                        print $pname;
                    print "</div>\n";
                } 
            }
           
        }
    print "</div><!-- closing package as insert list -->\n";

    print "<div id='insertList' class='insertList row'>\n";
        if($dbInserts['numrows']>0)
        {
            foreach($dbInserts['data'] as $insert)
            {
                if($insert['weprint_id']>0)
                {
                    $sql="SELECT A.pub_id, B.run_name FROM jobs A, publications_runs B WHERE A.id=$insert[weprint_id] AND A.run_id=B.id";
                    $dbJobs=dbselectsingle($sql);
                    $accountname=stripslashes($dbJobs['data']['run_name']);
                } else {
                    $sql="SELECT * FROM accounts WHERE id=$insert[advertiser_id]";
                    $dbAccount=dbselectsingle($sql);
                    $accountname=str_replace("&amp;","&",stripslashes($dbAccount['data']['account_name']));
                }
                
                
                if($insert['clone_id']!=0){$accountname.=" CLONED";}
                //each insert holder will be 120px wide
                
                if(!in_array($insert['id'],$scheduledInserts['insert']))
                {
                    $request=$insert['insert_quantity'];
                    $insertname=htmlentities($accountname." ".stripslashes($insert['insert_tagline']));
                    $insertname=str_replace("'","",$insertname);
                    
                    if((!$insert['confirmed'] && $GLOBALS['allowScheduleUnconfirmedInserts']) || $insert['confirmed'])
                    {
                        $notconfirmed='';
                    } else {
                        $notconfirmed='notconfirmed';
                    }
                    if($insert['killed'])
                    {
                        $killed='killed';
                    }  else {
                        $killed='';
                    }
                    $zones = "<span style='color: black;font-weight:bold;'>Zones: ";
                    //get zones for this insert
                    $sql="SELECT B.zone_label FROM inserts_zoning A, publications_insertzones B WHERE A.zone_id=B.id AND A.schedule_id=$insert[schedule_id] AND A.insert_id=$insert[id]";
                    $dbZones = dbselectmulti($sql);
                    if($dbZones['numrows']>0)
                    {
                        foreach($dbZones['data'] as $zone)
                        {
                            $zones.=" $zone[zone_label] |";
                        }
                    } 
                    $zones.="</span>";
                    if($insert['sticky_note']){$sticky='sticky';$type='sticky';}else{$sticky='';$type='insert';}
                    if($insert['clone_id']>0){$cloned='cloned';}else{$cloned='';}
                    if(!$insert['received']){$notreceived="notreceived";}else{$notreceived='';}
                    $insertpages=$insert['standard_pages'];
                    $insertinfo=$insertname." | Pages: $insertpages | Request: $request | $zones";
                    print "<div id=\"insert_$insert[id]\" data-type=\"$type\" data-id=\"$insert[id]\" class=\"insert $notconfirmed $notreceived $sticky $cloned $killed \">"; //col-xs-6
                    print $insertinfo;
                    print "</div>\n";
                }
                  
                    
            }
        }
    print "</div><!--closing insert list -->\n";
print "</div><!-- closing the insert area -->\n";



print "<div class='col-xs-12 col-sm-10' >\n"; //style='position:relative;right:0;width:83%;float:right;'
    print "<div class='alert alert-info' role='alert'><b>Publication:</b> $pubname | <b>Press Run:</b> $pressrun | <b>Publishing:</b> $displaydate | Total Inserts: $totalinserts | Standard Pages: $totalpages | Sticky Notes: $totalSticky <button class='btn btn-primary pull-right' data-toggle='modal' data-target='#packageModal'>Create a new package</button><br>
    <b>Legend:</b> Green border = confirmed, Red Border = not confirmed, Red Text = not received, dashed border = sticky note, blue background = cloned insert, Double-click on insert to get details.</div>\n";
    
    packageWindow($dbPackages);
    
    print "</div> <!-- closing package building side -->\n";
print "</div><!-- closing row -->\n";

function packageWindow($dbPackages)
{
    //get the packages
    //this query is called up in the jackets & packages area
    print "<div id='packagewindow' class='container-fluid'>\n";
    if($dbPackages['numrows']>0)
    {
        foreach($dbPackages['data'] as $package)
        {
            $sql="SELECT * FROM inserters WHERE id=$package[inserter_id]";
            $dbInserter=dbselectsingle($sql);
            $inserter=$dbInserter['data'];
            $inserterturn=$inserter['inserter_turn'];
            
            print "<div id='packagearea_$package[id]' class='row packageWrapper' style='margin-top:4px;padding-top:4px;border-top: thick solid black'>\n";
                print "<div class='col-xs-12 col-sm-2'>
                <div class='panel panel-primary'>
                    <div class='panel-heading'>
                    <p><span id='package_$package[id]_name'>".stripslashes($package['package_name'])."</span><span class='pull-right'><i class='fa fa-gear' onClick='expandSettings($package[id]);'> </i></span></p>
                    <input id='package_$package[id]_nameedit' value='".stripslashes($package['package_name'])."' style='display:none;width:80%;color:black;' />";
                    print "
                    </div>\n";
                    print "<div class='panel-body' style='padding:2px;'>\n";
                    print "<div id='settings_$package[id]' style='display:none;'>\n";
                        $pname = 'package_'.$package['id'].'_dt';
                        //print "Run time: ".make_datetime('',date("Y-m-d H:i",strtotime($package['package_startdatetime'])));
                        ?>
                        Scheduled start<br>
                        <div id="<?php echo $pname ?>_tp" class="input-group date col-xs-3" style="width:95%;">
                        <input id="<?php echo $pname ?>" class="form-control" name="<?php echo $pname ?>_tp" value="" type="text">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        </div>
                        <script>
                        $('#<?php echo $pname ?>_tp').datetimepicker({format: 'M/DD/YY HH:mm',
                            useCurrent: true,
                            showTodayButton: true,
                            sideBySide: true,
                            icons: {
                                time: 'fa fa-clock-o',
                             },
                             defaultDate: '<?php echo date("Y-m-d H:i",strtotime($package['package_startdatetime'])) ?>'
                            });
                        </script>
                        <?php
                        print "<br>Production draw request:<br>
                        <input type='text' id='package_$package[id]_request' class='col-xs-12' style='width:95%' value='$package[inserter_request]'/>\n";
                        
                        print "<div class='col-xs-12 btn-group' style='margin: 4px 0'>
                        <a class='btn btn-danger delete pull-left' href='?action=deletePackage&planid=$planid&packageid=$package[id]'><i class='fa fa-trash'></i> Delete </a>";
                        print "<button class='btn btn-primary pull-right' onClick='saveSettings($package[id]);'>Save</button>
                        </div>";
                        print "<input type='hidden' id='package_$package[id]_originalrequest' value='$package[inserter_request]' />\n";
                        print "<input type='hidden' id='package_$package[id]_open' value='false' />\n";
                    print "</div><!-- closes settings panel for package $package[id] -->\n";
                    
                    //add a package stats area
                    print "<div class='col-xs-12'>\n";
                        print "Page count: <span id='package_$package[id]_pagecount'>$package[standard_pages]</span><br>\n";
                        print "# Inserts: <span id='package_$package[id]_insertcount'>$package[total_inserts]</span><br>\n";
                        print "Package Weight: <span id='package_$package[id]_weight'>$package[total_weight]</span><br>\n";
                    print "</div><!--closing the package stats area -->\n";
                    
                    
                    if($package['sticky_note_id']!=0)
                    {
                        $sql="SELECT * FROM inserts WHERE id=$package[sticky_note_id]";
                        $dbSN=dbselectsingle($sql);
                        $sn=$dbSN['data'];
                        
                        
                        
                        
                        if($sn['weprint_id']>0)
                        {
                            $sql="SELECT A.pub_id, B.run_name FROM jobs A, publications_runs B WHERE A.id=$sn[weprint_id] AND A.run_id=B.id";
                            $dbJobs=dbselectsingle($sql);
                            $accountname=stripslashes($dbJobs['data']['run_name']);
                        } else {
                            $sql="SELECT * FROM accounts WHERE id=$sn[advertiser_id]";
                            $dbAccount=dbselectsingle($sql);
                            $accountname=stripslashes($dbAccount['data']['account_name']);
                        }
                        
                        $request=$sn['insert_quantity'];
                        $insertname=$accountname." ".stripslashes($sn['insert_tagline']);
                        $insertname=str_replace("'","",$insertname);
                        $insertpages=$insert['standard_pages'];
                        $sinfo=$insertname."<br><b>Pages:</b> $insertpages <b>Request: </b>$request";
                    
                    
                    
                        if((!$sn['confirmed'] && $GLOBALS['allowScheduleUnconfirmedInserts']) || $sn['confirmed'])
                        {
                            $notconfirmed='';
                        } else {
                            $notconfirmed='notconfirmed';
                        }
                        if($sn['sticky_note']){$sticky='sticky';$type='sticky';}else{$sticky='';$type='insert';}
                        if($sn['clone_id']>0){$cloned='cloned';}else{$cloned='';}
                        $sclasses='insert '.$notconfirmed.' '.$sticky.' '.$cloned;
                        $sname=$insertname;
                        $sclone=$sn['clone_id'];
                        $sid=$sn['id'];
                    } else {
                        $accountname='Drop sticky here';
                        $sid=0;
                        $sclasses='sticky';
                    }
                    print "<div class='col-xs-12' style='margin-top:4px;padding-top:4px;border-top: thin solid black;'>\n";
                    print "<p><b>Sticky Note</b><span><i class='fa fa-trash pull-right' onclick=\"removeInsert($package[id],'sticky',0);\"></i></span></h4>
                    <div id='sticky_$package[id]' data-type=\"sticky\" data-handler=\"sticky\" data-id=\"$sid\" data-packageid=\"$package[id]\" data-stationid='0' class='station bg-info' style='height:40px;'>$accountname</div>
                    </div>";
                print "</div><!-- closing panel -->
                </div> <!-- closing col -->
                </div><!--closing left col -->\n";     
                    
                
                print "<div id='package_$package[id]_stations' class='col-xs-12 col-sm-10'>\n";
                    //here we create a series of drop containers for inserts. One for each station (double-out uses same);
                    //we need to know the numbers of stations in the specified inserter
                    //get the stations
                    $sql="SELECT * FROM inserters_hoppers WHERE inserter_id=$package[inserter_id] ORDER BY hopper_number";
                    $dbStations=dbselectmulti($sql);
                    if($dbStations['numrows']>0)
                    {
                        //run through them fast to get counts
                        $minDoubleHopper=$inserterturn+1; //one more than where the turn is
                        $i=0;
                        $stations[0]=0;
                        foreach($dbStations['data'] as $station)
                        {
                            if($i==0)
                            {
                                $minHopper=$station['hopper_number'];
                                $i++;
                            }
                            $stations[$station['hopper_number']]=$station['id'];
                            $maxDoubleHopper=$station['hopper_number']; //keep setting in, the last value will be the largest
                        }
                        
                        
                        foreach($dbStations['data'] as $station)
                        {
                            $stationNumber=$station['hopper_number'];
                            //if(($stationNumber<=$inserterturn && $candoubleout) || $singleout)
                            
                            //ok, lets see if we have an insert for this slot
                            $sql="SELECT * FROM jobs_packages_inserts WHERE package_id='$package[id]' AND hopper_id='$station[id]'";
                            $dbCheckInsert=dbselectsingle($sql);
                            if($dbCheckInsert['numrows']>0)
                            {
                                //woohoo! there is an insert booked for this package and station
                                $insertid=$dbCheckInsert['data']['insert_id'];
                                $inserttype=$dbCheckInsert['data']['insert_type'];
                                
                                //now we need a little detail about the insert
                                if($inserttype=='insert')
                                {
                                    $sql="SELECT A.*, B.account_name FROM inserts A, accounts B WHERE A.id=$insertid AND A.advertiser_id=B.id";
                                    $dbInsertInfo=dbselectsingle($sql);
                                    $fullinsertinfo=$dbInsertInfo['data'];
                                    $insertname=stripslashes($fullinsertinfo['account_name'])." ".stripslashes($fullinsertinfo['insert_tagline']);
                                    $insertname=str_replace("'","",$insertname);
                            
                                    $insertpages=$fullinsertinfo['standard_pages'];
                                    $request=$insert['insert_quantity'];
                                    $insertname="<b>$insertname</b>";
                                    
                                    $zones = "<span style='color: black;font-weight:bold;'>Zones: ";
                                      //get zones for this insert
                                      $sql="SELECT A.zone_label FROM publications_insertzones A, inserts_zoning B, inserts_schedule C, 
                                      jobs_inserter_packages D 
                                        WHERE A.id=B.zone_id AND B.schedule_id=C.id AND C.pub_id=D.pub_id 
                                        AND C.insert_date=D.pub_date AND B.insert_id=$insertid AND D.id=$package[id]";
                                      $dbZones = dbselectmulti($sql);
                                      if($dbZones['numrows']>0)
                                      {
                                          foreach($dbZones['data'] as $zone)
                                          {
                                              $zones.=" $zone[zone_label] |";
                                          }
                                      } 
                                      $zones.="</span>";
                                    
                                    
                                    
                                    
                                    $insertinfo=$insertname."<br><b>Pages: </b>".$insertpages." <b>Request: </b>$request".$zones;    
                                    $cloneid=$fullinsertinfo['clone_id'];
                                    $insertclasses='insert';
                                    if($fullinsertinfo['sticky_note']){$insertclasses.=' sticky';}
                                    if($cloneid>0){$insertclasses.=' cloned';}
                                } elseif ($inserttype=='package')
                                {
                                    print "<!-- package found for this station with $insertid as id -->\n";
                                    $sql="SELECT * FROM jobs_inserter_packages WHERE id=$insertid";
                                    $dbInsertInfo=dbselectsingle($sql);
                                    $fullinsertinfo=$dbInsertInfo['data'];
                                    $insertname=stripslashes($fullinsertinfo['package_name']);
                                    $insertname=str_replace("'","",$insertname);
                                    $insertpages=$fullinsertinfo['standard_pages'];
                                    $insertinfo=$insertname;
                                    $insertclasses='package'; 
                                } elseif ($inserttype=='jacket')
                                {
                                    $insertname="Generic Jacket";
                                    $insertpages=4;
                                    $insertinfo="Generic Jacket";
                                    $insertclasses='jacket';
                                }
                                
                            } else {
                                $insertid='0';
                                $inserttype='';
                                $insertinfo='';
                                $insertpages='0';
                                $insertname='';
                                $insertclasses='insert';
                                $cloneid=0;
                                $zones='';
                            }
                            
                            print "<div class='col-xs-12 col-md-1'>
                            <div class='panel panel-primary'  style='margin-bottom:2px;'>\n";
                                print "<div class='panel-heading'>\n";
                                
                                
                                
                                
                                if($station['jacket_station'])
                                {
                                    $jacket=1;
                                    print 'J-';
                                } else {
                                    $jacket=0;
                                }
                                print $stationNumber;
                                
                                
                                
                                
                                print "<span class='pull-right'>
                                  <i class='fa fa-trash' onclick='removeInsert($package[id],$station[id]);'> </i>
                                  </span>\n";
                                print "</div>\n";
                                //print "<div class='panel-body'>\n";
                                
                                
                                //$insertinfo=addslashes($insertinfo);
                                print "<div id='pack_$package[id]-station_$station[id]' data-jacket='$jacket' data-packageid='$package[id]' data-stationid='$station[id]' data-handler='insert' data-id=\"$insertid\" data-type=\"$inserttype\" class='station bg-info panel-body' style='height:86px;padding:2px;'>\n";
                                print $insertname.$zones;
                                //print "</div><!-- closes panel body -->\n";
                                print "</div><!-- closes panel -->\n";
                                print "</div><!-- closes col -->\n";
                                    
                            print "</div><!--closes the station $station[id] in package $package[id] -->\n";
                                
                        }
                    } else {
                        print "Inserter is not configured.";
                    }
                print "</div><!-- closes the package stations area -->\n";
            
            
            print "</div><!-- closes the package row -->\n";
        }
        
    } else {
        print "There are no packages set up yet.";
    }
    print "</div><!--closing package window -->\n";
}        

?>
<script>
    var planID=$('#plan_id').val();
    
   
    
    /* 
    initialize all the dropper / draggable stuff.
    */
    $(function()
    {
         //qtip functionality to display more details about the insert
        $(".insert, .station, .notconfirmed").each(function(index, item){
            attachQtip(item);
            
        });
        
        //attach context menu 
        $(".insert").each(function(index, item){
            attachMenu(item);
        }); 
        
        $("#insertList").sortable();
        $("#packageList").sortable();
               
        $(".station" ).droppable({
            accept: ".insert, .jacket",
            hoverClass: "statiom-hover",
            drop: function( event, ui ) {
                handleInsertDrop(event,ui); 
            }
        });
        
    })
    
    
    function handleInsertDrop(event, ui)
    {
        var insert=$(ui.draggable);
        var insertType=insert.data('type');
        var insertID=insert.data('id');
        
        var dropper=$(event.target);
        var dropType=dropper.data('handler');
        var dropperID=dropper.prop('id');
        var existingID=dropper.data('id');
        var packageID=dropper.data('packageid');
        var stationID=dropper.data('stationid');
        var jacket=dropper.data('jacket');
        
        console.log('dropping '+insertType+' on a handler for '+dropType + " insert_id: "+insertID+" package_id: "+packageID+" existing id: "+existingID);
        
        if(existingID != "0") {console.log('has a thing');return false;}
        if(dropType=='sticky' && (insertType=='insert' || insertType=='jacket' || insertType=='package')){return false;}
        if(dropType=='insert' && insertType=='sticky'){return false;}
        if(insertType=='package' && dropType=='insert' && jacket==1){return false;} //return on trying to put package in jacket spot
        
        
        if(insertType=='package' && packageID==insertID)
        {
            alert('You can not put a package back into itself');
            return false;
        } else if(insertType=='sticky' && dropType=='insert') {
            alert('You can only place a sticky note into the sticky note slot.');
            return false;
        } 
        
        if(insertType=='insert')
        {
            addInsertToStation(packageID,stationID,insertID,jacket);
            $(ui.draggable).remove();
        }
        
        if(insertType=='package')
        {
            addPackageToStation(packageID,stationID,insertID);
            $(ui.draggable).remove();
        }
        
        if(insertType=='sticky')
        {
            addStickyToPackage(packageID,stationID,insertID);
            $(ui.draggable).remove();
        }
        
        if(insertType=='jacket')
        {
            addJacketToStation(packageID,stationID,jacket);
        }
        
        
    }
    
    
    /*
    * This function adds a sticky note to the specified package and station
    */
    function addStickyToPackage(packageID,stationID,insertID)
    {
        console.log("Processing the addition of sticky "+insertID+" to package "+packageID);
        //handle stuff like updating page counts, insert counts and database update
        $.ajax({
            type: "POST",
            url: "includes/ajax_handlers/handlePackageChanges.php",
            data:{
                action: 'addStickyToPackage',
                plan_id: planID,
                pack_id: packageID,
                insert_id: insertID,
                insert_type: 'sticky',
                station_id: stationID
            },
            dataType: 'json',
            success: function(response){
                //do stuff
                //console.log(response);
                if(response.status=='success')
                {
                    var station = '#sticky_'+packageID;
                    //add insert name text
                    $(station).html(response.account_name);
                    $(station).data('id',insertID);
                    $(station).data('type','sticky');
                    //remove "droppable" state from this station
                    //$(station).droppable("disable");
                    
                    //add qtip
                    attachQtip(station);
                    
                    //finally update package page, count and weight totals
                    calcPackageTotals();
                    
                    return true;            
                } else {
                    alert(response.message);
                    return false;
                }
            }  
        });
        
    }
    
    /*
    * This function adds an insert to the specified package and station
    */
    function addInsertToStation(packageID,stationID,insertID,jacket)
    {
        console.log("Processing the addition of insert "+insertID+" to package "+packageID);
        //handle stuff like updating page counts, insert counts and database update
        $.ajax({
            type: "POST",
            url: "includes/ajax_handlers/handlePackageChanges.php",
            data:{
                action: 'addInsertToStation',
                plan_id: planID,
                pack_id: packageID,
                insert_id: insertID,
                insert_type: 'insert',
                station_id: stationID,
                jacket: jacket
            },
            dataType: 'json',
            success: function(response){
                //do stuff
                //console.log(response);
                if(response.status=='success')
                {
                    var station = '#pack_'+packageID+'-station_'+stationID;
                    //add insert name text
                    $(station).html(response.account_name);
                    $(station).data('id',insertID);
                    $(station).data('type','insert');
                    //remove "droppable" state from this station
                    $(station).droppable("disable");
                    
                    //add qtip
                    attachQtip(station);
                    
                    //finally update package page, count and weight totals
                    calcPackageTotals();
                    
                    return true;            
                } else {
                    alert(response.message);
                    return false;
                }
            }  
        });
        
    }
    
    /*
    * This function adds an insert to the specified package and station
    */
    function addPackageToStation(destPackageID,stationID,sourcePackageID)
    {
        console.log("Processing the addition of package "+sourcePackageID+" to package "+destPackageID);
        //handle stuff like updating page counts, insert counts and database update
        $.ajax({
            type: "POST",
            url: "includes/ajax_handlers/handlePackageChanges.php",
            data:{
                action: 'addPackageToStation',
                plan_id: planID,
                pack_id: destPackageID,
                insert_id: sourcePackageID,
                insert_type: 'package',
                station_id: stationID
            },
            dataType: 'json',
            success: function(response){
                //do stuff
                //console.log(response);
                if(response.status=='success')
                {
                    var station = '#pack_'+destPackageID+'-station_'+stationID;
                    //add insert name text
                    $(station).html(response.account_name);
                    $(station).data('id',sourcePackageID);
                    $(station).data('type','package');
                    
                    //remove "droppable" state from this station
                    $(station).droppable("disable");
                    
                    //add qtip
                    attachQtip(station);
                    
                    //finally update package page, count and weight totals
                    calcPackageTotals();
                    
                    return true;            
                } else {
                    alert(response.message);
                    return false;
                }
            }  
        });
        
    }
    
    /*
    * This function adds an jacket to the specified package and station
    */
    function addJacketToStation(packageID,stationID,jacket)
    {
        console.log("Processing the addition of jacket to package "+packageID);
        //handle stuff like updating page counts, insert counts and database update
        $.ajax({
            type: "POST",
            url: "includes/ajax_handlers/handlePackageChanges.php",
            data:{
                action: 'addJacketToStation',
                plan_id: planID,
                pack_id: packageID,
                insert_type: 'jacket',
                station_id: stationID
            },
            dataType: 'json',
            success: function(response){
                //do stuff
                console.log(response);
                if(response.status=='success')
                {
                    var station = '#pack_'+packageID+'-station_'+stationID;
                    //add insert name text
                    $(station).html("Generic Jacket");
                    $(station).data('id',0);
                    $(station).data('type','jacket');
                    
                    //remove "droppable" state from this station
                    $(station).droppable("disable");
                    
                    //add qtip
                    attachQtip(station);
                    
                    //finally update package page, count and weight totals
                    calcPackageTotals();
                    
                    return true;            
                } else {
                    alert(response.message);
                    return false;
                }
            }  
        });
        
    }
    
    
    /*
    *   This function loops through each package defined and totals up the current total insert count, page count and weight
    */
    function calcPackageTotals()
    {
        //loop through each packageWrapper class on the page
        var packageIDs = '';
        var packageID = '';
            
        $('.packageWrapper').each(function()
        {
            packageID = $(this).prop('id');
            packageIDs = packageIDs+packageID.replace('packagearea_','')+",";
        })
        
        if(packageIDs != '')
        {
            $.ajax({
                type: "POST",
                url: "includes/ajax_handlers/handlePackageChanges.php",
                data:{
                    action: 'calcPackage',
                    plan_id: planID,
                    package_ids: packageIDs
                },
                dataType: 'json',
                success: function(response){
                    //do stuff
                    console.log(response);
                    if(response.status=='success')
                    {
                        $.each(response.packages, function (j,package){
                            $('#package_'+package.id+'_pagecount').html(package.pagecount);
                            $('#package_'+package.id+'_insertcount').html(package.insertcount);
                            $('#package_'+package.id+'_weight').html(package.weight);
                        })
                        
                        return true;            
                    } else {
                        alert(response.message);
                        return false;
                    }
                }  
            });
        }
    }
    
    function expandSettings(packID)
    {
        if($('#package_'+packID+'_open').val()=='false')
        {
            $('#settings_'+packID).slideDown();
            $('#package_'+packID+'_name').hide();   
            $('#package_'+packID+'_nameedit').show();
            $('#package_'+packID+'_open').val('true');
        } else {
            $('#package_'+packID+'_open').val('false');
            $('#settings_'+packID).slideUp();
            $('#package_'+packID+'_name').show();   
            $('#package_'+packID+'_nameedit').hide(); 
        }
           
    }
    
    function saveSettings(packID)
    {
        //do an ajax call to save any changes. This may also mean enabling secondary 
        //(double-out) stations or a whole new setup
        
        var packdate=$('#package_'+packID+'_dt').val();
        var packname=$('#package_'+packID+'_nameedit').val();
        var request=$('#package_'+packID+'_request').val();
        var orequest=$('#package_'+packID+'_originalrequest').val();
        var stickyid=$('#package_'+packID+'_sticky').val();
        
        
        $.ajax({
            type: "POST",
            url: "includes/ajax_handlers/handlePackageChanges.php",
            data:{action: 'saveSettings',
                planid: planID,
                packid: packID,
                packdate: packdate,
                packname: packname,
                request: request,
                orequest: orequest,
                stickyid:stickyid
            },
            dataType: 'json',
            success: function(response){
                //do stuff
                if(response.status=='success')
                {
                    $('#settings_'+packID).hide();
                    $('#package_'+packID+'_nameedit').hide();
                    $('#package_'+packID+'_name').html(packname);
                    $('#package_'+packID+'_name').show();
                    
                    //update the name of the package in the package holding area
                    $('#package_'+packID).html(packname+"<span class='pull-right'><i class='fa fa-gear' onclick='expandSettings("+packID+");'> </i></span>");
                    $('#package_'+packID).attr("rel",packname);
                    
                    //update the name of the package if it's in another package
                    if(response.update_package_id>0)
                    {
                        $('#pack_'+response.update_package_id+'-station_'+response.update_station_id).html(packname);   
                        $('#pack_'+response.update_package_id+'-station_'+response.update_station_id+'-insert_info').val(packname);   
                    }
                    
                    //see if there are any events that we need to attach
                    if(response.process_attachments)
                    {
                        $.each(response.attach_events,function(j,attach){
                            <?php if($GLOBALS['debug']){ print "console.log('for id='+attach.id+' on -'+attach.action+'- we will be attaching '+attach.afunction);\n";}?>
                            if(attach.action=='direct')
                            {
                                eval(attach.afunction);
                            } else {
                                $('#'+attach.id).live(attach.action, function(){
                                    eval(attach.afunction);
                                });
                            }
                        })
                        
                    }
                    
                } else {
                    alert(response.error_message);
                }
            }  
        });
        
        
    }

    function removeInsert(packageID,stationID)
    {
        //grab the station to work with it
        if(stationID=='sticky')
        {
            var station = '#sticky_'+packageID;
        } else {
            var station = '#pack_'+packageID+'-station_'+stationID;
        }
        //get the "type" of the current item in the station
        //also return false if there isn't an existing insert
        if($(station).html()=='') return false;
        
        var insertType = $(station).data('type');
        var insertID = $(station).data('id');
        var insertText = $(station).html();
        
        //remove insert from database
        //console.log("Processing removal | data: planid="+planID+", packid="+packageID+", insertid="+insertID+", type="+insertType);
        //handle stuff like updating page counts, insert counts and database update
        $.ajax({
            type: "POST",
            url: "includes/ajax_handlers/handlePackageChanges.php",
            data:{
                action: 'removeInsert',
                plan_id: planID,
                pack_id: packageID,
                insert_id: insertID,
                insert_type: insertType,
                station_id: stationID
            },
            dataType: 'json',
            success: function(response){
                //do stuff
                if(response.status=='success')
                {
                    //clear the station holder
                    $(station).html('');
                    
                    //re-enable station as a droppable
                    //add "droppable" state from this station
                    $(station).droppable( "enable" );
                    $(station).data('type','');
                    $(station).data('id','0');
                    $(station).qtip('disable');
                    
                    
                    //create new insert block in insert list (or package list depending)
                    if(insertType=='insert' || insertType=='sticky')
                    {
                        var $newInsertHTML = $("<div id='insert_"+insertID+"' data-type='"+insertType+"' data-id='"+insertID+"' class='"+response.insert_classes+"'>"+insertText+"</div>");
                        $('#insertList').append($newInsertHTML);
                        var newInsert = $('#insert_'+insertID);
                    } else if(insertType=='package')
                    {
                        var $newPackageHTML = "<div id='package_"+insertID+"' class='insert package col-xs-12' data-type='package' data-id='"+insertID+"' >"+insertText+"</div>";
                        $('#packageList').append($newPackageHTML);
                        
                        var newInsert = $('#package_'+insertID);
                    }
                    //make new insert block draggable
                    $(newInsert).sortable();
                    
                    //attach qtip to new insert block
                    attachQtip(newInsert);
                    
                    //update page counts on all packages
                    calcPackageTotals();
                    
                    
                    return true;            
                } else {
                    alert(response.message);
                    return false;
                }
            }  
        });
    }

    
    function attachQtip(item)
    {
        $(item).qtip({
            content: {
             title: 'Insert Details',
             button: true,
             text: "<i class='fa fa-spinner></i>", // The text to use while the AJAX request is loading
             ajax: {
                url: 'includes/ajax_handlers/handlePackageChanges.php',
                data: { action: 'insertDetails', id: $(item).attr('id'), planid: planID },
                type: 'POST',
                dataType: 'json',
                once: false,
                success: function(response) {
                    if(response.status=='success')
                    {
                        this.set('content.text', response.qtip);
                    }
                }
             }
           },
           
            position: {
                    target: $(item),
                    my: 'left center',
                    at: 'right center'
                },
            style: {
                widget: true, // Optional shadow...
                def: false,
                tip: 'left center' // Tips work nicely with the styles too!
            },
            show: {
                event: 'dblclick',
                solo: true // Only show one tooltip at a time
            },
            hide: {
                event: 'click mouseleave unfocus'
            }
            
        });
       
    }
    
    function attachMenu(item)
    {
        $(item).contextMenu('jobCmenu_'+$(item).data('id'),{
            'Print Job Ticket': {
                click: function(element){ // element is the jquery obj clicked on when context menu launched
                    window.open('jobPressTicket.php?action=print&jobid='+event.id,'Press Job Ticket',"scrollbars=0, resizeable=1, width=750, height=640");
                             
                }
            }
        })
    }
    
    
</script>

<!-- add the create new package form modal -->
<!-- Modal -->
<div class="modal fade" id="packageModal" tabindex="-1" role="dialog" aria-labelledby=packageLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="packageLabel">Create new package</h4>
      </div>
      <form method=post class='form-horizontal'>
          <div class="modal-body">
            <?php
            make_text('package_name','','Package Name');
            make_number('package_request',0,'Quantity to produce');
            make_datetime('run_time','','Run time');
            ?>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <input type="submit" id="submit" class="btn btn-primary" value="Create package" />
          </div>
      </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php

function addPackage()
{
    global $planid, $siteID;
    
    $request = intval($_POST['package_request']);
    $packdate = addslashes($_POST['run_time']);
    $pname = addslashes($_POST['package_name']);
        
    $sql="SELECT * FROM jobs_inserter_plans WHERE id=$planid";
    $dbPlan=dbselectsingle($sql);
    $plan=$dbPlan['data'];
    $pubdate=$plan['pub_date']; 
    $request=$plan['inserter_request'];
    $inserterid=$plan['inserter_id'];
    $numpackages=$plan['num_packages']; 
    $pubid=$plan['pub_id']; 
    
    $sql="SELECT * FROM inserters WHERE id=$inserterid";
    $dbInserter=dbselectsingle($sql);
    $inserter=$dbInserter['data'];
    $candoubleout=$inserter['can_double_out'];
    $inserterturn=$inserter['inserter_turn'];
    $singleout=false;

    $singleoutspeed=$inserter['single_out_speed'];
    
    //we will default to single out to leave a larger window by default
    $speed=$singleoutspeed; 
    if($speed>0)
    {
        $runminutes=round(($request/$speed),0)+30; //pad by 30 because... :)
    } else {
        $runminutes=120;
    }
    $packstop=date("Y-m-d H:i",strtotime($packdate."+$runminutes minutes"));
     
    $sql="INSERT INTO jobs_inserter_packages (pub_id, pub_date, package_date, plan_id, inserter_id, 
    package_name, package_startdatetime, package_stopdatetime, inserter_request, site_id) VALUES ('$pubid', '$pubdate', 
    '$packdate', '$planid', '$inserterid', '$pname', '$packdate', '$packstop', '$request', '$siteID')";
    $dbAddPackage=dbinsertquery($sql);
    if($dbAddPackage['error']=='')
    {
        $response['status']='success';
        $packageid=$dbAddPackage['insertid'];
        //update the count for number of packages for the plan
        $sql="UPDATE jobs_inserter_plans SET num_packages=num_packages+1 WHERE id=$planid";
        $dbUpdate=dbexecutequery($sql);
        if($dbUpdate['error']=='')
        {
            
        } else {
            //badness...
        }
    } else {
        //more badness
    }
}

function deletePackage()
{
    $planid=intval($_GET['planid']);
    $packid=intval($_GET['packid']);
    $sql="DELETE FROM jobs_packages_inserts WHERE plan_id=$planid AND package_id=$packid";
    $dbInserts=dbexecutequery($sql);
    
    //subtract one from the number of package in the jobs_inserter_plans
    $sql="UPDATE jobs_inserter_plans SET num_packages=num_packages-1 WHERE id=$planid";
    $dbUpdate=dbexecutequery($sql);
    //delete the actual package
    $sql="DELETE FROM jobs_inserter_packages WHERE id=$packid";
    $dbDelete=dbexecutequery($sql);
    
}

$Page->footer();             
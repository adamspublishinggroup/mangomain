<?php
$popup=true;
$systemMode = true;
include 'includes/boot.php';

//$temp=password_hash('slcrbt5',PASSWORD_DEFAULT);
//$sql="UPDATE users SET password = '$temp' WHERE id = 1";
///$dbUpdate=dbexecutequery($sql);    
         
if($_POST['submit']=='Reset your password')
{
    $pReset = "Now you can use your new password.<br />";
    $token = addslashes($_POST['token']);
    
    $temp=password_hash($_POST['password'],PASSWORD_DEFAULT);
    
    $sql="UPDATE users SET password = '$temp', password_old=0 WHERE token='$token'";
    $dbUpdate=dbexecutequery($sql);
    
}

if ($_GET['nopermission'])
{
    if ($_COOKIE["mango-".SITE_ID]){
        //descript with the given key
        $userToken=$_COOKIE["mango-".SITE_ID];
        $sql="SELECT * FROM users WHERE token='$userToken'"; 
        //for right now we are disabling the other intranet app
        $dbresult=dbselectsingle($sql);
        if($dbresult['numrows']>0)
        {
            $record=$dbresult['data'];
            if (isset($_GET['r'])){$refer=$_GET['r'];}else{$r='';}
            populate_session($record,$refer);
        } else {
            show_login();
        }
    } else {
        setcookie ("mango-".SITE_ID, "", time() - 3600,'/');
        show_login('You do not have permission to log in.');
    }
    
} else if ($_POST['submit']=='Log In') {
    $record=check_user();
    if ($record['errors']!='') {
        show_login($record['errors']);
    } else {
        if (isset($_GET['r'])){$refer=$_GET['r'];}else{$r='';}
        populate_session($record,$refer);
    }
} else {
    //check for cookie first
    if ($_COOKIE["mango-".SITE_ID]){
        //descript with the given key
        $userToken=$_COOKIE["mango-".SITE_ID];
        $sql="SELECT * FROM users WHERE token='$userToken'"; 
        //for right now we are disabling the other intranet app
        $dbresult=dbselectsingle($sql);
        if($dbresult['numrows']>0)
        {
            $record=$dbresult['data'];
            if (isset($_GET['r'])){$refer=$_GET['r'];}else{$r='';}
            populate_session($record,$refer);
        } else {
            show_login();
        } 
    } else {
        show_login();
    }
}


function show_login($errors = '')
{
?>
    <!DOCTYPE html> 
    <html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mango - Newspaper Management Made Simple</title>
    <META name="author" content="Joe Hansen <jhansen@pioneernewsgroup.com>">
    <META name="copyright" content="<?php echo date("Y"); ?> Pioneer News Group Inc - Joe Hansen">
    <META name="robots" content="none">
    <META http-equiv="cache-control" content="no-cache">
    <META http-equiv="pragma" content="no-cache">
    <META http-equiv="content-type" content="text/html; charset=UTF-8">
    <META http-equiv="expires" content="<?php echo date("r") ?>">
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" /> 
    <!-- loading core libraries -->
    <!-- jquery -->
    <script src="/includes/core_libraries/jquery-3.2.0.min.js" type="text/javascript"></script>
    <script src="/includes/core_libraries/jquery-ui-1.12.1.custom/jquery-ui.min.js" type="text/javascript"></script>
    <script>
        // Change JQueryUI plugin names to fix name collision with Bootstrap.
        $.widget.bridge('jquitooltip', $.ui.tooltip);
        $.widget.bridge('jquibutton', $.ui.button);
    </script>
    <!-- load a CDN version of jquery-migrate -->
    <script src="https://code.jquery.com/jquery-migrate-1.2.1.min.js" type="text/javascript"></script> 
    
    <!-- Bootstrap JavaScript -->
    <script src="/includes/core_libraries/bootstrap-3.3.7-dist/js/bootstrap.min.js"></script>
    <script src="/includes/mango_scripts/common.js"></script>
    <script src="/includes/js_libraries/bootbox.min.js"></script>
    <script src="/includes/js_libraries/jquery.validate.min.js"></script>
    <script src="/includes/js_libraries/pwstrength-bootstrap.min.js"></script>
    
    <!-- font awesome -->
    <link rel='stylesheet' type='text/css'  href="/includes/core_libraries/font-awesome-4.7.0/css/font-awesome.min.css" />
    <!-- jquery ui core css -->
    <link rel='stylesheet' type='text/css'  href="/includes/core_libraries/jquery-ui-1.12.1.custom/jquery-ui.css" />

    <!-- bootstrap CSS -->
    <link rel="stylesheet" href="/includes/core_libraries/bootstrap-3.3.7-dist/css/bootstrap.min.css" />

    <!-- Optional Bootstrap theme -->
    <link rel="stylesheet" href="/includes/core_libraries/bootstrap-3.3.7-dist/css/bootstrap-theme.min.css" />
    
    
    <style>
        html {
          position: relative;
          min-height: 100%;
        }
        body {
          /* Margin bottom by footer height */
          margin-bottom: 60px;
        }
        .footer {
          position: absolute;
          bottom: 0;
          width: 100%;
          /* Set the fixed height of the footer here */
          height: 60px;
          padding-top:20px;
          text-align:center;
          background-color: #dedede;
        }
        .carousel-caption h1, .carousel-caption p {
            color: black;
            text-shadow: 1px 1px 1px #ffffff;
        }
    </style>
    </head>
    <body onload="getLocation()">
    <div class='container'>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-7 col-lg-8 hidden-xs hidden-sm">
                <h1 class="txt-color-red login-header-big">MANGO</h1>
                <h4 class="paragraph-header">Newspaper Management Made Simple</h4>
                
                    
                <div id="myCarousel" class="carousel slide " data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                    <li data-target="#myCarousel" data-slide-to="4"></li>
                  </ol>
                  <div class="carousel-inner" role="listbox">
                    <div class="item active">
                      <img class="first-slide" src="artwork/general/inserter.jpg" alt="Inserter">
                      <div class="container">
                        <div class="carousel-caption">
                          <h1>Inserting and Packaging</h1>
                          <p>Tagline here</p>
                        </div>
                      </div>
                    </div>
                    <div class="item">
                      <img class="second-slide" src="artwork/general/papers.jpg" alt="Job Management">
                      <div class="container">
                        <div class="carousel-caption">
                          <h1>Job Management</h1>
                          <p>Tagline here</p>
                        </div>
                      </div>
                    </div>
                    <div class="item">
                      <img class="third-slide" src="artwork/general/storage.jpg" alt="Storage">
                      <div class="container">
                        <div class="carousel-caption">
                          <h1>Newsprint and Preprint Tracking</h1>
                          <p>Tagline here</p>
                        </div>
                      </div>
                    </div>
                    <div class="item">
                      <img class="fourth-slide" src="artwork/general/press.jpg" alt="Press Maintenance">
                      <div class="container">
                        <div class="carousel-caption">
                          <h1>Equipment and Maintenance</h1>
                          <p>Tagline here</p>
                        </div>
                      </div>
                    </div>
                    <div class="item">
                      <img class="fifth-slide" src="artwork/general/folder.jpg" alt="Tracking">
                      <div class="container">
                        <div class="carousel-caption">
                          <h1>Tracking from contract to delivery</h1>
                          <p>Tagline here</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div><!-- /.carousel -->
                  

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h4>Complete Job Tracking</h4>
                        <p>
                            From commercial account management to job statistics, Mango captures it all.
                        </p>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                        <h4>Full-featured Management Suite</h4>
                        <ul>
                        <li>Press Jobs</li>
                        <li>Preprints</li>
                        <li>Bindery</li>
                        <li>Delivery</li>
                        <li>Newsprint</li>
                        <li>Reporting</li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-4" style='padding-top:100px;'>
                <div class="well no-padding">
                    <?php 
                    if($_GET['a']=='reset')
                    {
                        ?>
                       <h4>You are resetting your password</h4>
                        <form action='/login.php' id='resetform' method=post class='form'>
                            <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name='password' placeholder="">
                          </div>
                          <div class="form-group">
                            <label for="password_confirm">Confirm</label>
                            <input type="password" class="form-control" id="password_confirm" name='password_confirm' placeholder="">
                          </div>
                         <input type='hidden' name='token' value='<?= $_GET['t'] ?>' />         
                         <input type='hidden' name='r' value='<?= $_GET['r'] ?>' />
                        <input name='submit' type=submit class='btn btn-primary' value='Reset your password' /> <a href='/login.php'>Oops! I remember now!</a>
                        </form>
                        <script>
                        $('#password').pwstrength({
                            ui: { showVerdictsInsideProgressBar: true }
                        });
                        $( "#resetform" ).validate({
                              rules: {
                                password: "required",
                                password_confirm: {
                                  equalTo: "#password"
                                }
                              }
                            });
                        </script>
                        <?php
                    } else {
                        ?>
                    <form action="/login.php" id="login-form" class="form" method=post>
                        <header>
                            <?= $pReset ?>Sign In to <?= SITE_URL ?>
                        </header>

                        <fieldset>
                          <?php
                          if ($_SESSION['error']<>'') {
                            print "<div class='alert alert-danger' role='alert'>$_SESSION[error]</div>";
                            $_SESSION['error']='';
                          }
                          if ($errors<>'') {
                            print "<div class='alert alert-danger' role='alert'>$errors</div>";
                          }
                          ?>
                          <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" class="form-control" id="username" name='username' placeholder="Username">
                          </div>
                          <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name='password' placeholder="">
                          </div>
                          <div class="checkbox">
                            <label>
                              <input name="remember" id="remember" type="checkbox" checked> Remember Me
                            </label>
                          </div>
                          <input type=submit name="submit" type="submit" class="btn btn-primary" value='Log In'>
                          <a id='passForgot'>Forgot your password?</a>
                        </fieldset>  
                        <input type=hidden id='lat' name='lat' value='0' />
                        <input type=hidden id='lng' name='lng' value='0' />
                    </form>
                    
                    <script>
                         $(document).ready(function(){
                            $('#passForgot').on('click',function(){
                                bootbox.prompt({
                                    title: "Enter your account email to start recovery",
                                    inputType: 'email',
                                    callback: function (result) {
                                        if(result)
                                        {
                                            var dataObj = {email: result};
                                            genericAjaxHandler("sendPasswordReset.php",dataObj,userUpdate)
                                        }
                                    }
                                });
                             })
                            
                         })
                         
                         
                         
                         function userUpdate(response)
                         {
                             bootbox.alert({
                                message: response.message,
                                callback: function () {
                                    
                                }
                            })
                         }
                         
                        function getLocation() {
                                if (navigator.geolocation) {
                                    navigator.geolocation.getCurrentPosition(showPosition,showError);
                                }
                            }
                            function showPosition(position) {
                                $('#lat').val(position.coords.latitude);
                                $('#lng').val(position.coords.longitude);
                            }
                            function showError(error) {
                                switch(error.code) {
                                    case error.PERMISSION_DENIED:
                                        console.log("User denied the request for Geolocation.");
                                        break;
                                    case error.POSITION_UNAVAILABLE:
                                        console.log("Location information is unavailable.");
                                        break;
                                    case error.TIMEOUT:
                                        console.log("The request to get user location timed out.");
                                        break;
                                    case error.UNKNOWN_ERROR:
                                        console.log("An unknown error occurred.");
                                        break;
                                }
                            }
                        </script>
                    
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    
    <footer class="footer">
      <div class="container">
        <p class="text-muted">&copy Copyright <?php echo date("Y"); ?> Adams Publishing Group</p>
      </div>
    </footer>
    
     
    </body>
    </html>
    <?php
    dbclose();
    
}
  
function populate_session($record,$refer='')
{
    
    //update login time & location
    $dt = date("Y-m-d H:i:s");
    $lat = $record['current_lat'];
    $lng = $record['current_lng'];
    $sql = "UPDATE users SET last_login = '$dt', last_login_lat = '$lat', last_login_lng='$lng' WHERE id=$record[id]";
    $dbUpdateLogin = dbexecutequery($sql);
    
    if($_POST['remember'])
    {
        setcookie("mango-".SITE_ID, $record['token'], time()+3600*24*30, '/');  
    }
    
    $_SESSION['loggedin']=true;
    $_SESSION['username']=$record['username'];
    $_SESSION['firstname']=$record['firstname'];
    if ($record['department_id']=='')
    {
        $dept=0;
    } else {
        $dept=$record['department_id'];
    }
    $_SESSION['departmentid']=$dept;
    $_SESSION['userid']=$record['id'];
    $_SESSION['admin']=$record['admin'];
    $_SESSION['simpletables']=$record['simple_tables'];
    $_SESSION['simplemenu']=$record['simple_menu'];
    $_SESSION['email']=$record['email'];
    $_SESSION['app']=$app;
    $_SESSION['accessdenied']=false;
    $dest='index.php';
    //now load the permissions that are true ie value=1
        
    if($record['id']=='999999')
    {
         $_SESSION['permissions']=array(1);
    } else {
        $sql="SELECT permissionID FROM user_permissions WHERE user_id=$record[id] AND value=1";
        $dbPermissions=dbselectmulti($sql);
        if ($dbPermissions['numrows']>0)
        {
            $perms=array();
            foreach($dbPermissions['data'] as $perm)
            {
                array_push($perms,$perm['permissionID']);
            }
            $_SESSION['permissions']=$perms;
        } else {
            $_SESSION['error']="No permissions have been defined for this account.";
            
            redirect("/login.php?r=".$refer);
        }
    }
   
    
    if ($refer!=''){$dest=$refer;}
    echo '<script type="text/javascript">';
    echo 'window.location.href="'.$dest.'";';
    echo '</script>';
    echo '<noscript>';
    echo '<meta http-equiv="refresh" content="0;url='.$dest.'" />';
    echo '</noscript>';
    
    die();
}

function check_user()
{
    $errors = array();
    $testuser=addslashes($_POST['username']);
    $sql="SELECT * FROM users WHERE username='$testuser'"; 
    $dbresult=dbselectsingle($sql);
    $record=$dbresult['data'];
    $record['current_lat']=$_POST['lat'];
    $record['current_lng']=$_POST['lng'];
    $namecheck=$record['firstname'];
    $password=$record['password'];
    
    if($testuser=='superadmin' && $passcheck=='Hsp@33K6')
    {
        $record=array("id"=>'999999',
                      "username"=>"superadmin",
                      "firstname"=>"Super",
                      "lastname"=>"Admin",
                      "department_id"=>0,
                      "admin"=>1,
                      "email"=>'jhansen69@gmail.com',
                      "simple_tables"=>0,
                      "app"=>'mango');
    } else if ($dbresult['numrows']==1) {
        //this is a valid user, so...
        //now to check password validity
        if($record['password_old']) //if it's an old format password, update the user record to the new format
        {
            if ($password==md5(addslashes($_POST['password'])))
            {
                $newpassword=password_hash($_POST['password'],PASSWORD_DEFAULT);
                $sql="UPDATE users SET password='$newpassword', password_old=0 WHERE id=$record[id]";
                $dbUpdate=dbexecutequery($sql);
                populate_session($record);
            } else {
                $error="Please enter a valid username and/or password!";
            }
        } else {
            if( password_verify( $_POST['password'], $record['password'] ) ) {
                populate_session($record);
            } else {
                $error="Please enter a valid username and/or password! no pass";
            }
        }
    } else {
        $error='Please enter a valid username and/or password.';
    }
    
    show_login($error);
} 
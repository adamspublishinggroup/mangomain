<?php
  define("ABS_PATH", $_SERVER['DOCUMENT_ROOT']);

  if(!file_exists(ABS_PATH.'/includes/config.json'))
  {
      die("Config file not present. Aborting bootstrap");
  } else {
      $baseConfig = json_decode(file_get_contents(ABS_PATH.'/includes/config.json'),true);
      $baseConfig = $baseConfig['config'];
      $debugger[]=$baseConfig;
      define( 'DB_HOST', $baseConfig['database']['host'] ); // set database host
      define( 'DB_USER', $baseConfig['database']['username'] ); // set database user
      define( 'DB_PASS', $baseConfig['database']['password'] ); // set database password
      define( 'DB_NAME', $baseConfig['database']['database'] ); // set database name
      define( 'SEND_ERRORS_TO', $baseConfig['send_errors_to'] ); // set database name
      define( 'MODE', $baseConfig['mode'] ); // set database name
      define( 'SITE_ID', $baseConfig['site_id'] ); // set database name 
  }
  session_start();
  unset($_SESSION['mango']);
  session_unset();
  session_destroy();
  setcookie("mango-".SITE_ID, 0,time()-360000,'/');
  redirect('/login.php');
  function redirect($url) {
   if (!headers_sent())
       header('Location: '.$url);
   else {
       echo '<script type="text/javascript">';
       echo 'window.location.href="'.$url.'";';
       echo '</script>';
       echo '<noscript>';
       echo '<meta http-equiv="refresh" content="0;url='.$url.'" />';
       echo '</noscript>';
   }
}